package br.gov.ce.semace.rh.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.semace.base.model.Pessoa;


public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

}
