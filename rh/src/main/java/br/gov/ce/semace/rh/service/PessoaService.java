package br.gov.ce.semace.rh.service;

import br.gov.ce.semace.base.generic.GenericService;
import br.gov.ce.semace.base.model.Pessoa;

public interface PessoaService extends GenericService<Pessoa, Long> {

}
