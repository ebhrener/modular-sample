package br.gov.ce.semace.rh.enable;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import br.gov.ce.semace.base.BaseModuleConfiguration;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
@Documented
@Import({ BaseModuleConfiguration.class })
@Configuration
public @interface EnableRhModule {

}
