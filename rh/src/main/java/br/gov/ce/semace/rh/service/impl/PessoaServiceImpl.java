package br.gov.ce.semace.rh.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.gov.ce.semace.base.exception.ValidacaoException;
import br.gov.ce.semace.base.generic.GenericServiceImpl;
import br.gov.ce.semace.base.model.Pessoa;
import br.gov.ce.semace.rh.repository.PessoaRepository;
import br.gov.ce.semace.rh.service.PessoaService;

@Service
public class PessoaServiceImpl extends GenericServiceImpl<Pessoa, Long> implements PessoaService {

	public PessoaServiceImpl(JpaRepository<Pessoa, Long> repository) {
		super(repository);
	}
	
	@Autowired
	private PessoaRepository repository;

	
	@Override
	public Optional<Pessoa> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public Pessoa save(Pessoa t) throws ValidacaoException {
		return repository.save(t);
	}

	@Override
	public void delete(Pessoa t) throws ValidacaoException {
		super.delete(t);
	}
	
}
