package br.gov.ce.semace.rh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.semace.base.generic.GenericController;
import br.gov.ce.semace.base.model.Pessoa;
import br.gov.ce.semace.rh.service.PessoaService;

@RestController
@RequestMapping("/write/pessoa")
@RefreshScope
public class PessoaController extends GenericController<Pessoa, Long> {

	@Autowired
	public PessoaController(PessoaService service) {
		super(service);
	}

}
