package br.gov.ce.semace.erp.helpers;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe com operações para listas.
 *
 * @author robson.carvalho
 *
 * @param <T>
 */
public class CollectionsHelper<T> {

	private List<T> sourceList;

	public CollectionsHelper(List<T> sourceList) {
		super();
		this.sourceList = sourceList;
	}

	public CollectionsHelper(T[] sourceArray) {
		super();
		this.sourceList = asList(sourceArray);
	}

	public interface Predicate<T> {
		boolean test(T t) throws Exception;
	}

	public List<T> filter(Predicate<T> predicate) {
		try {
			List<T> targetList = new ArrayList<>();
			for (T t : this.sourceList) {
				if (predicate.test(t)) {
					targetList.add(t);
				}
			}
			return targetList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Collections.<T> emptyList();
	}

	public T find(Predicate<T> predicate) {
		try {
			for (T t : this.sourceList) {
				if (predicate.test(t)) {
					return t;
				}
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String aggregate(String delimiter) {
		try {
			if (this.sourceList != null && !this.sourceList.isEmpty()) {
				String result = "";
				for (int i = 0; i < sourceList.size(); i++) {
					if (delimiter != null && !delimiter.trim().isEmpty() && i > 0) {
						result += delimiter;
					}
					result += sourceList.get(i);
				}
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
}
