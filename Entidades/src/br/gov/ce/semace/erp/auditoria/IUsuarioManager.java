package br.gov.ce.semace.erp.auditoria;

import java.io.Serializable;

import br.gov.ce.semace.erp.entity.seguranca.IUsuario;

/**
 * Representa interface para gerenciadores de usuário logados  
 */
public interface IUsuarioManager extends Serializable {

	/**
	 * @return Usuário logado
	 */
	IUsuario getUsuarioLogado();
}
