package br.gov.ce.semace.erp.auditoria;

import org.hibernate.envers.RevisionListener;

import br.gov.ce.semace.erp.entity.seguranca.Auditoria;
import br.gov.ce.semace.erp.entity.seguranca.IUsuario;

/**
 * Esse Listener intercepta o objeto de auditoria e adiciona outras informações
 */
public class AuditoriaListener implements RevisionListener {

	/**
	 * @see RevisionListener
	 *
	 * Os managers dos sistemas auditados devem herdar UsuarioManager
	 */
	@Override
	public void newRevision(final Object revisionEntity) {
		Auditoria auditoria = (Auditoria)revisionEntity;
		try {

			//obter manager de usuário
			IUsuarioManager manager = new IUsuarioManager() {

				/**
				 * Serial
				 */
				private static final long serialVersionUID = 1002748392122742027L;

				@Override
				public IUsuario getUsuarioLogado() {
					//TODO:
					return null;
				}
			};

			//alterar objeto auditoria antes de persisti-lo
			auditoria.setUsuarioId(manager.getUsuarioLogado().getId());
			auditoria.setUsuarioLogin(manager.getUsuarioLogado().getLogin());
			auditoria.setUsuarioTipo(manager.getUsuarioLogado().toString());
			auditoria.setIpSolicitacao(getIpSolicitacao());

		} catch (Exception e) {
			// TODO: Verificar a possibilidade de buscar as informações do sistema do banco de dados.
			auditoria.setUsuarioId(428L);
			auditoria.setUsuarioLogin("sistema");
			auditoria.setUsuarioTipo("INTERNO");
			auditoria.setIpSolicitacao("sistema");
		}

		StringBuilder stack = new StringBuilder();
		for (StackTraceElement s : Thread.currentThread().getStackTrace()){
			stack.append(s.toString());
			stack.append('\n');
		}
		auditoria.setStackTrace(stack.toString());


	}

	private String getIpSolicitacao() {
		String remoteAddr = "";
//		final Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
//		if (request instanceof HttpServletRequest) {
//			final HttpServletRequest req = (HttpServletRequest) request;
//			remoteAddr = req.getHeader("X-FORWARDED-FOR");
//			if(remoteAddr == null){
//				remoteAddr = req.getRemoteAddr();
//			}
//		}
		return remoteAddr;
	}


}
