package br.gov.ce.semace.erp.auditoria;

import java.lang.reflect.InvocationTargetException;

import br.gov.ce.semace.erp.entity.seguranca.IUsuario;
import br.gov.ce.semace.erp.entity.seguranca.Usuario;

public class UsuarioManager implements IUsuarioManager {

	private static final long serialVersionUID = 8584669085563396888L;

	private Usuario usuarioLogado;

	public void parseUsuarioLogado(Object source) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		usuarioLogado = new Usuario();
		usuarioLogado.setId(getMethodValue(source, "getId", Long.class));
		usuarioLogado.setLogin(getMethodValue(source, "getLogin", String.class));
		usuarioLogado.setTipo(getMethodValue(source, "toString", String.class));
	}

	private <T> T getMethodValue(Object source, String methodName, Class<T> typeResult) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Object result = source.getClass().getMethod(methodName).invoke(source);
		return typeResult.cast(result);
	}

	@Override
	public IUsuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}
}
