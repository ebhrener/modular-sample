package br.gov.ce.semace.erp.auditoria;

import java.io.Serializable;

import org.hibernate.envers.RevisionType;

import br.gov.ce.semace.erp.entity.seguranca.Auditoria;

public class HistoricoAuditoria<T> implements Serializable {

	private static final long serialVersionUID = 5357497550458065467L;

	public T entity;

	public Auditoria auditoria;

	public RevisionType revisionType;

	public HistoricoAuditoria(T entity, Auditoria auditoria, RevisionType revisionType) {
		super();
		this.entity = entity;
		this.auditoria = auditoria;
		this.revisionType = revisionType;
	}

	public T getEntity() {
		return entity;
	}

	public void setEntity(T entity) {
		this.entity = entity;
	}

	public Auditoria getAuditoria() {
		return auditoria;
	}

	public void setAuditoria(Auditoria auditoria) {
		this.auditoria = auditoria;
	}

	public String getRevisionType() {
		switch (revisionType) {
		case ADD:
			return "Criação";
		case MOD:
			return "Atualização";
		case DEL:
			return "Remoção";
		default:
			break;
		}
		return null;
	}

	public void setRevisionType(RevisionType revisionType) {
		this.revisionType = revisionType;
	}
}