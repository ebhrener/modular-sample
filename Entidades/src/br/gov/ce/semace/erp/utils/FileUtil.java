package br.gov.ce.semace.erp.utils;

public class FileUtil {

	public static String getNameWithoutExtension(String nome) {
		int index = nome.lastIndexOf('.');
		return index == -1 ? nome : nome.substring(0, index);
	}

}
