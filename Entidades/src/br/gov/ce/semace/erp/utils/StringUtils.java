package br.gov.ce.semace.erp.utils;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class StringUtils {

	/**
	 * Máscara Padrão para o CNPJ
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/01/2016 15:12:32
	 */
	private static final String CNPJ_PATTERN = "##.###.###/####-##";

	/**
	 * Máscara Padrão para o CPF
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/01/2016 15:12:54
	 */
	private static final String CPF_PATTERN = "###.###.###-##";

	/**
	 * Máscara Padrão para o CEP
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/01/2016 15:13:22
	 */
	private static final String CEP_PATTERN = "#####-###";

	/**
	 * Transforma uma {@link String} em um CNPJ formatado
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/01/2016 15:09:56
	 *
	 * @param cnpj Número do documento a ser formatado
	 *
	 * @return Retorna o CNPJ formatado
	 */
	public static String getCNPJFormatado(final String cnpj){
		return format(CNPJ_PATTERN, cnpj);
	}

	/**
	 * Transforma uma {@link String} em um CPF formatado
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/01/2016 15:13:44
	 *
	 * @param cpf Número do documento a ser formatado
	 *
	 * @return Retorna o CPF formatado
	 */
	public static String getCPFFormatado(final String cpf){
		return format(CPF_PATTERN, cpf);
	}

	/**
	 * Transforma uma {@link String} em um CEP formatado
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/01/2016 15:14:33
	 *
	 * @param cep Número a ser formatado
	 *
	 * @return Retorna um CEP formatado
	 */
	public static String getCEPFormatado(final String cep){
		return format(CEP_PATTERN, cep);
	}

	/**
	 * Formata uma String de acordo com um determinado padrão
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/01/2016 15:15:52
	 *
	 * @param padrao
	 * 		Padrão da {@link String} a ser formatada
	 * @param texto
	 * 		Texto a ser formatado
	 *
	 * @return
	 * 		Retorna uma {@link String} formatada de acordo com o padrão informado. Caso um dos parametros seja nulo, retorna <code>null</code>
	 */
	private static String format(final String padrao, final  String texto) {

		if (isBlank(padrao) || isBlank(texto)){
			return null;
		}

        MaskFormatter maskFormatter;

        try {

            maskFormatter = new MaskFormatter(padrao);
            maskFormatter.setValueContainsLiteralCharacters(false);

            return maskFormatter.valueToString(texto);

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

	public static String decapitalize(String string){
		if (string == null || string.length() == 0) {
			return string;
		}
		char c[] = string.toCharArray();
		c[0] = Character.toLowerCase(c[0]);
		return new String(c);
	}

	/**
	 * retorna o index da string encontrada ignorando o case sensitive
	 *
	 * @param key
	 * @param fullText
	 * @return
	 */
	public static int indexOfIgnoreCase(String key, String fullString) {
		return fullString.toLowerCase().indexOf(key);
	}

	public static String insertAtIndex(String insertString, int index, String fullString) {
		return fullString.substring(0,index) + insertString + fullString.substring(index, fullString.length());
	}


	public static boolean isNotBlank(String s) {
		return s != null && !s.trim().equals("");
	}

	public static boolean isBlank(String s) {
		return s == null || s.trim().equals("");
	}

}
