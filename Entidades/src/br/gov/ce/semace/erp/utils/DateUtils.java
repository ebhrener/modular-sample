package br.gov.ce.semace.erp.utils;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Tomaz Lavieri
 */
public class DateUtils {
    /**
     * Formatação "dd/MM/yyyy".
     */
    public static DateFormat BRAZIL_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    /**
     * Formatação "dd/MM/yy".
     */
    public static DateFormat BRAZIL_SHORT_FORMAT = new SimpleDateFormat("dd/MM/yy");

    /**
     * {@link DateFormat} com a formatação utilizando o {@link SimpleDateFormat} e padrão HH:mm:ss
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jul 22, 2013 1:37:59 PM
     */
    public static DateFormat BRAZIL_TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");

    /**
     * {@link DateFormat} com a formatação utilizando o {@link SimpleDateFormat} e padrão HH:mm
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jul 22, 2013 1:37:12 PM
     */
    public static DateFormat BRAZIL_SHORT_TIME_FORMAT = new SimpleDateFormat("HH:mm");

    /**
     * Não é possivel instanciar DateUtils.
     */
    private DateUtils(){}

    /**
     * Retorna o valor do horário minimo para a data de referencia passada.
     * <BR>
     * <BR> Por exemplo se a data for "30/01/2009 as 17h:33m:12s e 299ms" a data
     * retornada por este metodo será "30/01/2009 as 00h:00m:00s e 000ms".
     * @param date de referencia.
     * @return {@link Date} que representa o horário minimo para dia informado.
     */
    public static Date lowDateTime(Date date) {
        Calendar aux = Calendar.getInstance();
        aux.setTime(date);
        toOnlyDate(aux); //zera os parametros de hour,min,sec,milisec
        return aux.getTime();
    }

    /**
     * Retorna o valor do horário maximo para a data de referencia passada.
     * <BR>
     * <BR> Por exemplo se a data for "30/01/2009 as 17h:33m:12s e 299ms" a data
     * retornada por este metodo será "30/01/2009 as 23h:59m:59s e 999ms".
     * @param date de referencia.
     * @return {@link Date} que representa o horário maximo para dia informado.
     */
    public static Date highDateTime(Date date) {
        Calendar aux = Calendar.getInstance();
        aux.setTime(date);
        toOnlyDate(aux); //zera os parametros de hour,min,sec,milisec
        aux.add(Calendar.DAY_OF_MONTH, 1); //vai para o dia seguinte
        aux.add(Calendar.MILLISECOND, -1); //reduz 1 milisegundo
        return aux.getTime();
    }

    /**
     * Zera todas as referencias de hora, minuto, segundo e milesegundo do
     * {@link Calendar}.
     * @param date a ser modificado.
     */
    public static void toOnlyDate(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
    }


    /**
	 * Método que atribui o valor zero para as propriedades HORA, MINUTE, SEGUNDOS e MILISEGUNDOS
	 * da data passada como parâmetro.
	 *
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 28/05/2013 16:29:45
	 *
	 * @version 1.0
	 *
	 * @param date
	 * 		Data que terá a hora, minuto, segundo e milisegundo zerados.
	 *
	 * @return
	 * 		retorna a data recebida como parâmetro com a hora, minuto, segundo e milisegundo zerados.
	 */
    public static Date toOnlyDate(Date date) {
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

    /**
     * Converte uma {@link String} no formato "dd/MM/yyyy" ou "dd/MM/yy" em um
     * {@link Date}.
     * @param date a {@link String} contendo a data.
     * @return o objeto {@link Date} para a string passada.
     * @throws  IllegalArgumentException caso a string não seja em dos dois
     *          formatos permitidos.
     */
    public static Date toDate(String date) throws IllegalArgumentException {
        if (date == null) {
			return null;
		}
        try {
            return date.length() > 8 ?
                BRAZIL_FORMAT.parse(date) : BRAZIL_SHORT_FORMAT.parse(date);
        } catch (ParseException ex) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Formata a data passada em uma {@link String} com o formato "dd/MM/yyyy".
     * @param date a data a ser formatada.
     * @return {@link String} com a data formatada.
     */
    public static String toString(Date date) {
        if (date == null) {
			return null;
		}
        return BRAZIL_FORMAT.format(date);
    }
    /**
     * Formata a data passada em uma {@link String} com <tt>shortFormat</tt>
     * "dd/MM/yy", ou com o formato longo "dd/MM/yyyy".
     * @param date a data a ser formatada.
     * @param shortFormat <tt>true</tt> se o formato é o curto, ou
     *          <tt>false</tt> caso o formato seja o longo.
     * @return {@link String} com a data formatada.
     */
    public static String toString(Date date, boolean shortFormat) {
        if (date == null) {
			return null;
		}
        return shortFormat ?
            BRAZIL_SHORT_FORMAT.format(date) : BRAZIL_FORMAT.format(date);
    }

    /**
     * Formata o tempo de uma determinada data com o padrão {@link #BRAZIL_TIME_FORMAT}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jul 22, 2013 1:39:37 PM
     *
     * @param data
     * 		{@link Date} representando a data a ser formatada
     *
     * @return
     * 		Retorna uma {@link String} formatada, caso o parametro seja diferente de <code>null</code>.
     *  Caso contrário, retorna <code>null</code>
     */
    public static String getTime(final Date data){

    	// verifica se a data informada é nula
    	if (data == null) {
			return null;
		}

    	 //retorna a data formatada
    	 return BRAZIL_TIME_FORMAT.format(data);
    }

    /**
     * Formata o tempo de uma determinada data com o padrão {@link #BRAZIL_SHORT_TIME_FORMAT}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jul 22, 2013 1:34:46 PM
     *
     * @param data
     * 		{@link Date} representando a data a ser formatada
     *
     * @return
     * 		Retorna uma {@link String} formatada, caso o parametro seja diferente de <code>null</code>.
     *  Caso contrário, retorna <code>null</code>
     */
    public static String getShortTime(final Date data){

    	// verifica se a data informada é nula
    	if (data == null) {
			return null;
    	}

    	// retorna a data formatada
    	return BRAZIL_SHORT_TIME_FORMAT.format(data);
   }

    /**
     * Este método retorna a quantidade de dias entre o intervalo de datas.
     * Data Final - Data Inicial
     *
     *
     * @param dataInicial
     * @param dataFinal
     * @return
     */
    public static Integer contaDias(Date dataInicial, Date dataFinal){
		Long time = dataFinal.getTime()-dataInicial.getTime();
		time = TimeUnit.MILLISECONDS.toDays(time);
		return time.intValue();
    }
	/**
	 * Calcula a diferença de meses, considerando 1 mes o mesmo dia do mes posterior.
	 *
	 * EX: 29/01/2009 - 28/02/2009  =  0
	 * EX: 29/01/2009 - 01/02/2009  =  1
	 * EX: 29/01/2009 - 29/03/2009  =  2
	 * EX: 05/02/2009 - 05/03/2009  =  1
	 *
	 *
	 * @param dataInicial
	 * @param dataFinal
	 * @return int
	 *
	 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 15/04/2014
	 */
	public static int diferencaDeMeses(Date dataInicial, Date dataFinal) {

		Calendar inicio = Calendar.getInstance();
		inicio.setTime(dataInicial);
		Calendar fim = Calendar.getInstance();
		fim.setTime(dataFinal);

		int mesInicio = inicio.get(Calendar.MONTH);
		int anoInicio = inicio.get(Calendar.YEAR);
		int diaInicio = inicio.get(Calendar.DAY_OF_MONTH);

		int mesFim = fim.get(Calendar.MONTH);
		int anoFim = fim.get(Calendar.YEAR);
		int diaFim = fim.get(Calendar.DAY_OF_MONTH);

		int total = ((anoFim-anoInicio)*12)-mesInicio+mesFim;

		if (diaFim-diaInicio < 0) {
			total -= 1;
		}

		return total;
	}

	/**
	 * Calcula a quantidade de anos entre duas datas passadas.
	 *
	 * @param dataInicial
	 * @param dataFinal
	 * @return int
	 *
	 * @author Antonio Menezes de Araújo - antonio.menezes@semace.ce.gov.br - 15/11/2015
	 */
	public static int determinaQuantidadeAnos(Date dataInicial, Date dataFinal){

		Calendar inicio = Calendar.getInstance();
		inicio.setTime(dataInicial);

		Calendar fim = Calendar.getInstance();
		fim.setTime(dataFinal);

		return fim.get(Calendar.YEAR) - inicio.get(Calendar.YEAR);
	}
	/**
	 * Este método retorna a quantidade de dias entre as datas considerando as
	 * datas e despresando os horários.
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 02/12/2014 16:44:48
	 *
	 * @param dataInicial
	 * @param dataFinal
	 * @return
	 */
//	public static Integer contaSomenteDias(Date dataInicial, Date dataFinal) {
//		return contaDias(org.apache.commons.lang.time.DateUtils.truncate(dataInicial, Calendar.DATE),
//				org.apache.commons.lang.time.DateUtils.truncate(dataFinal, Calendar.DATE));
//	}


    /**
     * A dataInicial é referente a data que será usada como base para o inicio do calculo.
     * A dataComparacao será utilizada como data limite
     *
     * Exemplo:
     *     dataInicia = Data da Alteração: 01/12/2013
     *     dataCompracao = Data do Contrato:  01/12/2012
     *
     *
     * O prazo máximo refere-se ao limite máximo permitido. Exemplo: No contrato o prazo máximo dar-se-á pelo limite do tipo de contrato.
     * O prazo sugerido refere-se a quantidade de dias desejados pelo sistema. Exemplo, renovação de contrato com 365 dias.
     *
     * Será calculado se o prazo sugerido é superiro ao prazo máximo informado.
     *
     * @param dataInicial
     * @param dataComparacao
     * @param prazoMaximo - Em Meses
     * @param prazoSugerido - Em Dias
     * @return
     */
    public static Date dataMaximaSugerida(Date dataInicial, Date dataComparacao, Integer prazoMaximo, Integer prazoSugerido){
    	Calendar prazoMaximoContrato = Calendar.getInstance();
		prazoMaximoContrato.setTime(dataComparacao);
		prazoMaximoContrato.add(Calendar.MONTH, prazoMaximo);

		Calendar dataSugerida = Calendar.getInstance();
		dataSugerida.setTime(dataInicial);
		dataSugerida.add(Calendar.DATE, prazoSugerido);

		if(dataSugerida.after(prazoMaximoContrato)){
			dataSugerida = prazoMaximoContrato;
		}

		return dataSugerida.getTime();
    }

    /**
     * Verifica se a data atual do sistema está entre ou é uma das datas passadas.
     *
     *@author Antonio Menezes de Araújo [antonio.menezes@semace.ce.gov.br]
     *
     * @param dt1
     * @param dt2
     *
     * @return boolean
     */
    public static boolean isDataAtualEstaPeriodo(Date dtInicialPeriodo, Date dtFinalPeriodo){
    	Calendar dataCalendar1 = Calendar.getInstance();
    	dataCalendar1.setTime(lowDateTime(dtInicialPeriodo));

    	Calendar dataCalendar2 = Calendar.getInstance();
    	dataCalendar2.setTime(lowDateTime(dtFinalPeriodo));

    	Calendar dataAtual = Calendar.getInstance();

    	dataAtual.setTime(lowDateTime(new Date()));

    	return (dataAtual.after(dataCalendar1) && dataAtual.before(dataCalendar2)) ||
    		   ((!dataAtual.after(dataCalendar1) && !dataAtual.before(dataCalendar1) ||
   			   (!dataAtual.after(dataCalendar2) && !dataAtual.before(dataCalendar2))));
    }

    /**
     * Verifica se a data atual do sistema é maior que a data passada(vem depois, desconsiderando o horario).
     *
     * @author Antonio Menezes de Araújo [antonio.menezes@semace.ce.gov.br]
     *
     * @param dt1
     *
     * @return boolean
     */
    public static boolean verificaSeDataAtualEMaior(Date data){
    	Calendar dataCalendar1 = Calendar.getInstance();
    	dataCalendar1.setTime(lowDateTime(data));
    	Calendar dataPassada =
				new GregorianCalendar(dataCalendar1.get(Calendar.YEAR),
						dataCalendar1.get(Calendar.MONTH), dataCalendar1.get(Calendar.DAY_OF_MONTH));

    	Calendar dataAtual = Calendar.getInstance();
    	dataAtual.setTime(lowDateTime(new Date()));

    	return (dataAtual.after(dataPassada));
    }
    /**
     * @author marcus-mazzo
     * Retorna a data por extenso
     *      * 		Exemplo:
     * 			Data = 2014-07-01
     * 			Retorno = 01 de Julho de 2014
     * @param data
     * @return String
     * 			Data Formatada por Extenso
     */
    public static String dataFormatadaExtenso(Date data){
    	Locale locale = new Locale("PT", "BR");
		DateFormat dfmt = new SimpleDateFormat("dd MMMM yyyy", locale);
		String[] dataSplit = dfmt.format(data).split(" ");
		String dataFormatada = dataSplit[0]+" de "+dataSplit[1]+" de "+dataSplit[2];
		return  dataFormatada;
	}

    public static Boolean isFinalDeSemana(Date data){
    	Calendar dataVinda = Calendar.getInstance();
    	dataVinda.setTime(data);
		if (dataVinda.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || dataVinda.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
			return true;
		}
		return false;
	}

    /**
     * Soma uma quantidade de Dias a uma data informada por parametro.
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 09/11/2015 08:17:39
     *
     * @param data
     * 		Data a ser acrescida a quantidade de dias
     *
     * @param quantidadeDias
     * 		Quantidade de dias a ser acrescido a data
     *
     * @return
     * 	Retorna a data informada como parametro somada a quantidade de dias informado.
     */
	public static Date somarDiasAData(final Date data, final int quantidadeDias){
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(data);

		Calendar dataRetorno =
				new GregorianCalendar(dataCalendar.get(Calendar.YEAR), dataCalendar.get(Calendar.MONTH), dataCalendar.get(Calendar.DAY_OF_MONTH));

		dataRetorno.add(Calendar.DAY_OF_MONTH, quantidadeDias);

		return dataRetorno.getTime();

	}


	public static Date somarMesAData(final Date data, final int quantidadeMes){
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(data);

		Calendar dataRetorno =
				new GregorianCalendar(dataCalendar.get(Calendar.YEAR), dataCalendar.get(Calendar.MONTH), dataCalendar.get(Calendar.DAY_OF_MONTH));

		dataRetorno.add(Calendar.MONTH, quantidadeMes);

		return dataRetorno.getTime();

	}

	/**
     * Converte uma data passada via {@link String} para o tipo {@link Date}, levando em consideração apenas o tempo.
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/10/2015 14:20:17
     *
     * @see #BRAZIL_SHORT_TIME_FORMAT
     *
     * @param time
     * 		Data a ser convertida
     *
     * @return
     * 		Retorna <code>null</code>, caso a data seja <code>null</code> ou vazia ( {@link String#isEmpty()} ). Caso contrário, retorna a data formatada de acordo com o padrão: {@link #BRAZIL_SHORT_TIME_FORMAT}
     *
     * @throws ParseException
     */
    public static Date getDatebByShortTimeString(final String time) throws ParseException {

    	if (time == null || time.trim().isEmpty()) {
			return null;
    	}

    	return BRAZIL_SHORT_TIME_FORMAT.parse(time);
    }


	/**
	 * Informa se as datas estão no mesmo ano.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 02/12/2015 12:12:27
	 *
	 * @param data1
	 * 		Data a ser utilizada para comparação
	 *
	 * @param data2
	 * 		Data a ser utilizada para comparação
	 *
	 * @return
	 * 		Retorna <code>true</code> caso as datas estejam no mesmo ano. Caso contrário, <code>false</code>
	 */
	public static boolean isDataMesmoAno(final Date data1, final Date data2){
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(data1);

		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(data2);

		if (calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)){
			return true;
		}

		return false;
	}

	/**
	 * Retorna uma data com ultimo dia do ano.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 02/12/2015 12:11:44
	 *
	 * @return
	 *  Retorna a data com o ultimo dia do ano.
	 */
	public static Date getUltimoDiaAno(){

		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(DateUtils.toDate("31/12/" + calendar1.get(Calendar.YEAR)));

		return calendar1.getTime();

	}

//	public static Integer contaSomenteMinutos(final Date dataInicial, final Date dataFinal) {
//		return contaDias(org.apache.commons.lang.time.DateUtils.truncate(dataInicial, Calendar.MINUTE),
//				org.apache.commons.lang.time.DateUtils.truncate(dataFinal, Calendar.MINUTE));
//	}

	public static int contaMinutos(Date dataInicial, Date dataFinal){

	    long diferencaMinutos = (dataFinal.getTime() - dataInicial.getTime()) / (1000*60);

	    return (int) diferencaMinutos;

	}

	/**
	 * Verifica se existe alguma sobreposição de horários entre os intervalos
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 05/11/2015 14:27:26
	 *
	 * @param listInterval
	 * @return
	 */
//	public static boolean overlapIntervals(List<Interval> listInterval) {
//		boolean existe = false;
//
//		for (int i = 0; i < listInterval.size() - 1; i++) {
//			for (int j = i + 1; j < listInterval.size(); j++) {
//				if (overlapIntervals(listInterval.get(i), listInterval.get(j))) {
//					existe = true;
//
//					break;
//				}
//			}
//
//			if (existe) {
//				break;
//			}
//		}
//
//		return existe;
//	}

	/**
	 * Retorna se existe sobreposição de horários nos intervalos
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 05/11/2015 14:21:40
	 *
	 * @param intervalo1
	 * @param intervalo2
	 * @return
	 */
//	public static boolean overlapIntervals(Interval intervalo1, Interval intervalo2) {
//		return intervalo1.overlaps(intervalo2);
//	}

	/**
	 * Obtem uma data e retorna a sua data com a hora formatada. Ex: 01/01/2016 as 08:30:15
	 *
	 * @author Rodrigo S. Oliveira - 19/12/2016
	 *
	 * @param Data para ser formatada
	 * @return data e hora em formatada para String
	 */
	public static String toStringDateAndTime(final Date data){
	    return toString(data) + " as " + getTime(data);
	}

    /**
    * Método que retorna a quantidade de horas entre um intervalo de datas.
    * Data Final - Data Inicial
    *
    * @param dataInicial
    * @param dataFinal
    * @return inteiro da quantidade de horas
    */
   public static Integer contaHoras(Date dataInicial, Date dataFinal) {
	   Long time = dataFinal.getTime()-dataInicial.getTime();
	   time = TimeUnit.MILLISECONDS.toHours(time);
	   return time.intValue();
   }

   /**
    * Método que retorna o nome do dia da semana de uma data.
    *
    * @param data
    * @return nome do dia da semana
    */
   public static String getDiaSemana(Date data) {
	   Calendar cal = new GregorianCalendar();
	   cal.setTime(data);
	   String nomeDia = "";
	   int dia = cal.get(cal.DAY_OF_WEEK);
	   switch(dia){
	   		case Calendar.SUNDAY: nomeDia = "Domingo";break;
	   		case Calendar.MONDAY: nomeDia = "Segunda-Feira";break;
	   		case Calendar.TUESDAY: nomeDia = "Terça-Feira";break;
	   		case Calendar.WEDNESDAY: nomeDia = "Quarta-Feira";break;
	   		case Calendar.THURSDAY: nomeDia = "Quinta-Feira";break;
	   		case Calendar.FRIDAY: nomeDia = "Sexta-Feira";break;
	   		case Calendar.SATURDAY: nomeDia = "Sábado";break;
		}
	   return nomeDia;
   }

   /**
    * Método que verifica se determinadas datas são iguais sem considerar o tempo, somente a data.
    *
    * @author rodrigo.silva - Rodrigo Silva Oliveira - 20/10/2017 09:38:06
    *
    * @param dataInicial
    * @param dataFinal
    * @return TRUE se datas são iguais, FALSE caso não sejam.
    */
   public static boolean equalsOnlyDate(Date dataInicial, Date dataFinal) {
	   return toOnlyDate(dataInicial).equals(toOnlyDate(dataFinal));
   }
}