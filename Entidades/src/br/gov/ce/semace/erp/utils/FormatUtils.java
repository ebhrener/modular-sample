package br.gov.ce.semace.erp.utils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.licenciamento.Modalidade;
import br.gov.ce.semace.erp.entity.licenciamento.SubtipoProcesso;
import br.gov.ce.semace.erp.entity.licenciamento.TipoProcesso;

public class FormatUtils {

	// Método que formata uma string em uma data.
	public static Date formatarDataSemBarras(String registro)
			throws ParseException {
		Locale locale = new Locale("PT", "BR");
		DateFormat dt = new SimpleDateFormat("dd/MM/yyyy", locale);
		String dataNaoformatada = registro;
		String dataFormatada = dataNaoformatada.substring(0, 2) + "/"
				+ dataNaoformatada.substring(2, 4) + "/"
				+ dataNaoformatada.substring(4, 8);
		return dt.parse(dataFormatada);
	}

	public static String compararData(String data) {
		String dataTrim = data.trim();
		if (dataTrim != null) {
			if (dataTrim.length() == 8) {
				StringBuffer stringBuffer = new StringBuffer();
				stringBuffer.append(dataTrim.substring(0, 6));
				stringBuffer.append("20");
				stringBuffer.append(dataTrim.substring(6));
				dataTrim = stringBuffer.toString();
				System.out.println("ano com 2 digitos" + "" + "" + dataTrim);
			} else {

			}

		}
		return dataTrim;
	}

	public static Date formatarDataComBarras(String data) throws ParseException {
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");

		Date dataNova = null;

		if (data != null) {
			dataNova = sd.parse(data);
		}
		return dataNova;

	}

	public static Double converterValor(String valor) {
		Double d = null;
		if (valor != null) {
			String valorNovo = valor.replace(".", "").replace(",", ".");
			StringBuilder sb = new StringBuilder(valorNovo);
			sb.insert(13, ".");
			d = new Double(valorNovo);
		}
		return d;

	}

	public static String converterValorParaMoeda(BigDecimal valor) {
		return converterValorParaMoeda(valor.doubleValue());
	}

	public static String converterValorParaMoeda(Double valor) {
		Locale locale = new Locale("PT", "BR");
		NumberFormat format = NumberFormat.getNumberInstance(locale);
		format.setMinimumFractionDigits(2);
		String s = format.format(valor);
		return s;
	}

	public static String converterValorParaMoeda(Object valor) {
		return converterValorParaMoeda(Double.parseDouble(valor.toString()));
	}

	public static String retirarAcentos(final String texto) {
		String normalizedText = Normalizer.normalize(texto, Normalizer.Form.NFD);
		return normalizedText.replaceAll("[^\\p{ASCII}]", "");
	}

	/**
	 * Método para tratar as ocorrências de Apostrofos ( ' ) dentro das strings
	 * utilizadas nas consultas, onde poderiam ocasionar erros de SQL Injection,
	 * fechando as aspas (') que são utilizadas nos selects.
	 *
	 * Ex: .. ILIKE '% Joana D'arc Silva%' .. -> o Apostrofos do D'arc irá causar
	 * 		erro de SQL Injection e o sistema não conseguirar um retorno para a consulta.
	 *
	 * Logo, para toda ocorrência de um Apostrofo dentro da string, ele irá
	 * substituir por dois Apostrofos ( '' ), sempre fechando uma possivel aspas (')
	 * e não ocasionando erro de SQL Injection.
	 *
	 * @author Rodrigo Silva
	 * @param texto - a ser tratada os possiveis Apostrofos.
	 * @return String com todos os possiveis Apostrofos tratados.
	 */
	public static String tratarApostrofo(final String texto) {
		String normalizedText = Normalizer.normalize(texto, Normalizer.Form.NFD);
		return normalizedText.replace("'", "''");
	}

	public static String getDescricaoTipoModalidadeSubtipoProcesso(TipoProcesso tipoProcesso, SubtipoProcesso subtipoProcesso, Modalidade modalidade ) {
		if (modalidade != null && tipoProcesso != null && subtipoProcesso != null) {
			return modalidade.getDescricao() +" de " +tipoProcesso.getDescricao() + " / " + subtipoProcesso.getDescricao();
		} else if (modalidade != null && tipoProcesso != null) {
			return modalidade.getDescricao() +" de " +tipoProcesso.getDescricao();
		} else if (tipoProcesso != null && subtipoProcesso != null) {
			return tipoProcesso.getDescricao() + " / " + subtipoProcesso.getDescricao();
		}	else if (tipoProcesso != null) {
			return tipoProcesso.getDescricao();
		}

		return null;
	}

	public static String getDescricaoSiglaTipoModalidadeSubtipoProcesso(TipoProcesso tipoProcesso, SubtipoProcesso subtipoProcesso, Modalidade modalidade) {
		String retorno = null;
		if (modalidade != null && tipoProcesso != null && subtipoProcesso != null) {
			retorno = modalidade.getDescricao() +" de "+tipoProcesso.getDescricao() + " - " + tipoProcesso.getSigla() + " / " + subtipoProcesso.getDescricao();
			if(subtipoProcesso.getSigla() != null && !subtipoProcesso.getSigla().isEmpty()){
				retorno += " - " + subtipoProcesso.getSigla();
			}
		} else if (modalidade != null && tipoProcesso != null) {
			retorno =  modalidade.getDescricao() +" de "+tipoProcesso.getDescricao();
			if(tipoProcesso.getSigla() != null && !tipoProcesso.getSigla().isEmpty()){
				retorno +=  " - " + tipoProcesso.getSigla();
			}
		} else if (tipoProcesso != null && subtipoProcesso != null) {
			retorno = tipoProcesso.getDescricao() + " - " + tipoProcesso.getSigla() + " / " + subtipoProcesso.getDescricao();
			if(subtipoProcesso.getSigla() != null && !subtipoProcesso.getSigla().isEmpty()){
				retorno +=  " - " + subtipoProcesso.getSigla();
			}
		}else if (tipoProcesso != null) {
			retorno = tipoProcesso.getDescricao();
			if(tipoProcesso.getSigla() != null && !tipoProcesso.getSigla().isEmpty()){
				retorno +=  " - " + tipoProcesso.getSigla();
			}
		}

		return retorno;
	}

	public static String getDescricaoAtividadeGrupoAtividade(List<Atividade> atividades) {
		String retorno = atividades != null && !atividades.isEmpty()?" - ":"";
		Integer index = 0;
		for(Atividade at: atividades){
			retorno += at.getGrupoAtividade().getCodigo()+"-"+at.getCodigo()+" / ";
			index++;
			if(index == 3){
				Integer ultimoIndice = retorno.lastIndexOf(" / ");
				if(ultimoIndice < 0){
					ultimoIndice = 0;
				}
				retorno = retorno.substring(0, ultimoIndice)+" ...";
				break;
			}

		}


		return  retorno;
	}

	public static String getDocumentoFormatado(String documento) {
   	 if (documento == null) {
         return null;
     }

     String txt = documento.toString();

     if (txt.length() == 11) {
    	 StringBuilder sb = new StringBuilder();
         sb.append(txt.substring(0, 3));
         sb.append('.');
         sb.append(txt.substring(3, 6));
         sb.append('.');
         sb.append(txt.substring(6, 9));
         sb.append('-');
         sb.append(txt.substring(9, 11));

         return sb.toString();
     } else if (txt.length() == 14) {
    	 StringBuilder sb = new StringBuilder();
    	 sb.append(txt.substring(0, 2));
    	 sb.append('.');
    	 sb.append(txt.substring(2, 5));
    	 sb.append('.');
    	 sb.append(txt.substring(5, 8));
    	 sb.append('/');
    	 sb.append(txt.substring(8, 12));
    	 sb.append('-');
    	 sb.append(txt.substring(12, 14));


    	 return sb.toString();
     }

     return txt;
	}

	public String getNomeMesCalendar(int mes) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, mes - 1);
    	return calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
	}

	public static String getDataFormatada(Date data){
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");

		return sd.format(data);
	}
}