package br.gov.ce.semace.erp.enuns;

public enum StatusMonitoramento {

	ABERTO("Em aberto"),
	FINALIZADO("Finalizado"),
	CANCELADO("Cancelado");

	String descricao;

	private StatusMonitoramento(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
