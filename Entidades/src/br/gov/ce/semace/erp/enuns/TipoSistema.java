package br.gov.ce.semace.erp.enuns;

public enum TipoSistema {

	SIGA	(1,"Siga"),
	NATUUR	(2,"Natuur"),
	ERP	(3,"ERP");

	private String descricao;
	private Integer value;

	private TipoSistema(Integer value, String desc) {
		this.descricao 	= desc;
		this.value 		= value;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #SIGA}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira - 08/02/2017 17:02:50
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #SIGA}, caso contrário retorna <code>false</code>
     */
    public boolean isSiga() {
        if (name().equals(SIGA.name())){
            return true;
        }
        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #NATUUR}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira - 08/02/2017 17:02:50
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #NATUUR}, caso contrário retorna <code>false</code>
     */
    public boolean isNatuur() {
        if (name().equals(NATUUR.name())){
            return true;
        }
        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ERP}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira - 08/02/2017 17:02:50
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ERP}, caso contrário retorna <code>false</code>
     */
    public boolean isERP() {
        if (name().equals(ERP.name())){
            return true;
        }
        return false;
    }

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
