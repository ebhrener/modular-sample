package br.gov.ce.semace.erp.enuns;

public enum PadraoUploadShape {

	KMZ, KML, SHAPE;

	public boolean isKMZ() {
		return KMZ.name().equals(this.name());
	}

	public boolean isKML() {
		return KML.name().equals(this.name());
	}

	public boolean isShape() {
		return SHAPE.name().equals(this.name());
	}
}
