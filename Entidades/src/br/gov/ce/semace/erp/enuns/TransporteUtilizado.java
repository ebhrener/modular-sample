package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tiago
 *
 */

public enum TransporteUtilizado {

	PROPRIO("SEMACE"),
	TERCEIROS("Terceiros");

	String descricao;

	private TransporteUtilizado(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public boolean isTerceiros() {
		if (this.name().equals(TERCEIROS.name())) {
			return true;
		}

		return false;
	}

	public boolean isProprio() {
		if (this.name().equals(PROPRIO.name())) {
			return true;
		}

		return false;
	}

	public static List<TransporteUtilizado> getListTransporteUtilizadoOrdenada(){
		ArrayList<TransporteUtilizado> listOrdenada = new ArrayList<TransporteUtilizado>();
		listOrdenada.add(TransporteUtilizado.PROPRIO);
		listOrdenada.add(TransporteUtilizado.TERCEIROS);
		return listOrdenada;
	}
}