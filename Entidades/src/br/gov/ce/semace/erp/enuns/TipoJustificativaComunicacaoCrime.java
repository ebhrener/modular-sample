package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.entity.fiscalizacao.ComunicacaoCrime;

/**
 * Enum que representa o tipo de justificativa para a alteração de um {@link ComunicacaoCrime}
 *
 * @author rodrigo.silva - Rodrigo Silva Oliveira - 07/12/2017 15:53:45
 */
public enum TipoJustificativaComunicacaoCrime {

	CANCELAR("Cancelar"),
	EDITAR("Editar"),
	MUDAR_STATUS("Mudança de Status"),
	ASSINAR("Assinar");

	private String descricao;

	private TipoJustificativaComunicacaoCrime(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELAR}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELAR}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelar() {

    	if (name().equals(CANCELAR.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EDITAR}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EDITAR}, caso contrário retorna <code>false</code>
     */
    public boolean isEditar() {

        if (name().equals(EDITAR.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #MUDAR_STATUS}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #MUDAR_STATUS}, caso contrário retorna <code>false</code>
     */
    public boolean isMudarStatus() {

        if (name().equals(MUDAR_STATUS.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ASSINAR}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ASSINAR}, caso contrário retorna <code>false</code>
     */
    public boolean isAssinar() {

        if (name().equals(ASSINAR.name())){
            return true;
        }

        return false;
    }

    public static TipoJustificativaComunicacaoCrime fromInt(int valor) {

		for (TipoJustificativaComunicacaoCrime tipoJustificativaComunicacaoCrime : TipoJustificativaComunicacaoCrime.values()) {
			if (tipoJustificativaComunicacaoCrime.ordinal() == valor){
				return tipoJustificativaComunicacaoCrime;
			}
		}

		return null;
	}

	public static List<TipoJustificativaComunicacaoCrime> valuesOrderedByDescricao(){
		List<TipoJustificativaComunicacaoCrime> list =  Arrays.asList(TipoJustificativaComunicacaoCrime.values());
		Collections.sort(list, new Comparator<TipoJustificativaComunicacaoCrime>() {

			@Override
			public int compare(TipoJustificativaComunicacaoCrime o1, TipoJustificativaComunicacaoCrime o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}
}