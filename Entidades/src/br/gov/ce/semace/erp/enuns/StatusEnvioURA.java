package br.gov.ce.semace.erp.enuns;

public enum StatusEnvioURA {

	PENDENTE,
	ENVIADO,
	ERRO_TELEFONE,
	ERRO_DDI,
	ERRO_INTERESSADO,
	ERRO_ENVIO;

}
