package br.gov.ce.semace.erp.enuns;

/**
 * Tipo de Calculo que é utilizado para fazer a correção monetária de um valor
 *
 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br
 *
 */
public enum IndiceAtualizacaoMonetaria {

	IGPDI("IGP-DI"),
	SELIC("Selic");

	private String descricao;

	private IndiceAtualizacaoMonetaria(String descricao) {
		this.setDescricao(descricao) ;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
