package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

import br.gov.ce.semace.erp.entity.financeiro.Dae;

/**
 * {@link Enum} referente aos Status do {@link Dae}
 * <p>
 * Composto por: <br>
 * <br>
 * 0 - {@link #EM_ABERTO} <br>
 * 1 - {@link #PAGO} <br>
 * 2 - {@link #ERRO_PROCESSAMENTO} <br>
 * 3 - {@link #VENCIDO} <br>
 * 4 - {@link #EXCLUIDO} <br>
 * 5 - {@link #AGUARDANDO_EMISSAO} <br>
 * 6 - {@link #VINCULADO_VIA_DEPOSITO} <br>
 * 7 - {@link #PAGO_VENCIDO} <br>
 * 8 - {@link #RESSARCIDO} <br>
 * 9 - {@link #AGUARDANDO_CONFIRMACAO_PAGAMENTO} <br>
 *
 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:39:52
 */
public enum StatusDae {

	/**
	 * <b>Descrição: </b>Em Aberto<br>
	 * <b>Valor:</b> 0
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:40:49
	 */
	EM_ABERTO("Em Aberto"),


	/**
	 * <b>Descrição: </b>Pago<br>
	 * <b>Valor:</b> 1
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:41:56
	 */
	PAGO("Pago"),


	/**
	 * <b>Descrição: </b>Erro no Processamento<br>
	 * <b>Valor:</b> 2
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:41:30
	 */
	ERRO_PROCESSAMENTO("Erro no Processamento"),


	/**
	 * <b>Descrição: </b>Vencido<br>
	 * <b>Valor:</b> 3
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:41:22
	 */
	VENCIDO("Vencido"),


	/**
	 * <b>Descrição: </b>Excluído<br>
	 * <b>Valor:</b> 4
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:41:17
	 */
	EXCLUIDO("Cancelado"),


	/**
	 * <b>Descrição: </b>Aguardando Emissão<br>
	 * <b>Valor:</b> 5
	 *
	 * @author Saulo Fernandes [saulofernandes.ti@gmail.com] - 07/11/2013 08:45:34
	 */
	AGUARDANDO_EMISSAO("Aguardando Emissão"),


	/**
	 * <b>Descrição: </b>Vinculado via depósito<br>
	 * <b>Valor:</b> 6
	 *
	 * @author Antonio Menezes de Araújo [antoniomenezes20@gmail.com] - 04/09/2015 08:45:34
	 */
	VINCULADO_VIA_DEPOSITO("Vinculado via depósito"),

	/**
	 * <b>Descrição: </b>Pago após o vencimento<br>
	 * <b>Valor:</b> 7
	 *
	 * @author Joerlan Oliveira Lima [joerlan.joe@gmail.com] 04/04/2017
	 */
	@Deprecated
	PAGO_VENCIDO("Pago após o vencimento"),

	/**
	 * <b>Descrição: </b>Ressarcido<br>
	 * <b>Valor:</b> 8
	 *
	 * @author Rômulo Augusto da Silva [romuloaugusto.silva@gmail.com] 11/04/2017 15:41:21
	 */
	RESSARCIDO("Ressarcido"),

	/**
	 * <b>Descrição: </b>Aguardando confirmação de pagamento<br>
	 * <b>Valor:</b> 9
	 */
	AGUARDANDO_CONFIRMACAO_PAGAMENTO("Aguardando confirmação de pagamento");

    /**
     * Descrição do {@link Enum}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:42:37
     */
	private String descricao;

	 /**
     * Construtor default com o parâmetro {@link #descricao}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:43:41
     *
     * @param descricao
     *    Parâmetro a ser setado no campo <code>{@link #descricao} </code> do {@link Enum}
     */
	private StatusDae(String descricao) {
		this.descricao = descricao;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EM_ABERTO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:32:29
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EM_ABERTO}, caso contrário retorna <code>false</code>
     */
    public boolean isEmAberto() {

        if (this.name().equals(EM_ABERTO.name())){
            return true;
        }

        return false;
    }

    public boolean isVinculadoViaDeposito() {

        if (this.name().equals(VINCULADO_VIA_DEPOSITO.name())){
            return true;
        }
        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PAGO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:33:08
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PAGO}, caso contrário retorna <code>false</code>
     */
    public boolean isPago() {

        if (this.name().equals(PAGO.name())){
            return true;
        }

        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PAGO_VENCIDO}
     *
     * @author Joerlan Oliveira Lima
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PAGO_VENCIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isPagoVencido() {

        if (this.name().equals(PAGO_VENCIDO.name())){
            return true;
        }

        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ERRO_PROCESSAMENTO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:34:14
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ERRO_PROCESSAMENTO}, caso contrário retorna <code>false</code>
     */
    public boolean isErroProcessamento() {

        if (this.name().equals(ERRO_PROCESSAMENTO.name())){
            return true;
        }

        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #VENCIDO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:38:23
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #VENCIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isVencido() {

        if (this.name().equals(VENCIDO.name())){
            return true;
        }

        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EXCLUIDO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:38:57
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EXCLUIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isExcluido() {

        if (this.name().equals(EXCLUIDO.name())){
            return true;
        }

        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_EMISSAO}
     *
     * @author  @author Rômulo Augusto da Silva [romuloaugusto.silva@gmail.com] 11/04/2017 15:42:55
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #RESSARCIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isRessarcido() {

        if (this.name().equals(RESSARCIDO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #RESSARCIDO}
     *
     * @author Saulo Fernandes [saulofernandes.ti@gmail.com] - 07/11/2013 08:47:49
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #RESSARCIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoEmissao() {

        if (this.name().equals(AGUARDANDO_EMISSAO.name())){
            return true;
        }

        return false;
    }

    public boolean isAguardandoConfirmacaoPagamento() {

        if (this.name().equals(AGUARDANDO_CONFIRMACAO_PAGAMENTO.name())){
            return true;
        }

        return false;
    }

    /**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * {@link List}a de {@link StatusDae} permitidos para finalizar a fase de Geração de Dae, do atendimento presencial
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 11/12/2013 10:13:48
	 *
	 * @return
	 * 		Retorna uma {@link List}a com os {@link StatusDae} configurados.
	 */
    public static List<StatusDae> statusPermitidosFaseGeracaoDae(){

    	List<StatusDae> retorno = new ArrayList<StatusDae>();

    	retorno.add(PAGO);
    	retorno.add(EM_ABERTO);
    	retorno.add(AGUARDANDO_EMISSAO);
    	retorno.add(PAGO_VENCIDO);

    	return retorno;
    }

}