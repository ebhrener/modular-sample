package br.gov.ce.semace.erp.enuns;

public enum TipoParecer {
	
	INSTRUTORIO("Instrutório"),
	RECURSAL("Recursal");
	
	private String descricao;
	
	private TipoParecer(String descricao){
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
