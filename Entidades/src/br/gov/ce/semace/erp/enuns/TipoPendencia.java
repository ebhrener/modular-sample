package br.gov.ce.semace.erp.enuns;

public enum TipoPendencia {
	
	DAE("DAE"), 
	DOCUMENTACAO("Documentação"), 
	ARQUIVO_SHAPE("Arquivo Shape");
	
	private TipoPendencia(String descricao) {
		this.descricao = descricao;
	}
	
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
