package br.gov.ce.semace.erp.enuns;

public enum OperacaoDocumento {
	CRIAR,
	ASSINAR;
}
