package br.gov.ce.semace.erp.enuns;

public enum TipoParametroLaboratorial {
	AGUA ("Água de poço, cacimba, açude, rio, lagoa"),
	DESPEJO ("Despejos Domésticos e Industriais"),
	COLETA ("Serviços de Coleta");
	
	private TipoParametroLaboratorial(String descricao) {
		this.descricao = descricao;
	}
	
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
