package br.gov.ce.semace.erp.enuns;

public enum TipoRelatorio {

	MENSAL("Mensal"),
	SEMANAL("Semanal");

	String descricao;

	private TipoRelatorio(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public boolean isMensal() {
		if (this.name().equals(MENSAL.name())) {
			return true;
		}

		return false;
	}

	public boolean isSemanal() {
		if (this.name().equals(SEMANAL.name())) {
			return true;
		}

		return false;
	}
}