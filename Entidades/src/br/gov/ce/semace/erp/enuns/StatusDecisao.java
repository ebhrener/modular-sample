package br.gov.ce.semace.erp.enuns;

public enum StatusDecisao {
	
	PROCEDENTE("Procedente"), 
	IMPROCEDENTE("Improcedente"), 
	CANCELADO("Cancelado"), 
	ANULADO("Anulado");
	
	private String descricao;
	
	private StatusDecisao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}