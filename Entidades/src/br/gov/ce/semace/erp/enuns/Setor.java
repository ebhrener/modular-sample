package br.gov.ce.semace.erp.enuns;

public enum Setor {
	
	LESTE("Leste"),
	CENTRO("Centro"),
	OESTE("Oeste");
	
	private final String descricao;
	
	private Setor(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao(){
		return descricao;
	}
	
	public static Setor getEnumByDescricao(String descricao){
		
		for (Setor setor : values()) {
			if(setor.getDescricao().equals(descricao))
				return setor;
		}
		
		return null;
	}

}
