package br.gov.ce.semace.erp.enuns;

public enum StatusResultadoAnalise {
	
	AGUARDANDO_COLETA("Aguardando Coleta"),
	AGUARDANDO_ANALISE("Aguardando Análise"),
	ANALISADO("Analisado"),
	SEM_RESULTADO("Sem Resultado");
	
	private String descricao;
	
	private StatusResultadoAnalise(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
