package br.gov.ce.semace.erp.enuns;

public enum TipoFuncionarioBoletim {

	COLETA("Coleta"),
	ANALISE("Analise"),
	PUBLICACAO("Publicação");

	private String descricao;

	private TipoFuncionarioBoletim(String descricao){
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
