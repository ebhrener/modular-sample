package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.entity.fiscalizacao.Raia;

/**
 * {@link Enum} referente aos Status do {@link Raia}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #CONCLUIDO} <br>
 * {@link #RASCUNHO} <br>
 * {@link #CANCELADO} <br>
 * {@link #AGUARDANDO_ASSINATURA} <br>
 * {@link #AGUARDANDO_APROVACAO} <br>
 *
 * @author rodrigo.silva - Rodrigo Silva Oliveira - 20/11/17 08:33:19
 */
public enum StatusRaia {

	/**
	 * <b>Descrição: </b>Concluído<br>
	 * <b>Valor:</b> 0
	 */
	CONCLUIDO("Concluído"),

	/**
	 * <b>Descrição: </b>Rascunho<br>
	 * <b>Valor:</b> 1
	 */
	RASCUNHO("Rascunho"),

	/**
	 * <b>Descrição: </b>Cancelado<br>
	 * <b>Valor:</b> 2
	 */
	CANCELADO("Cancelado"),

	/**
	 * <b>Descrição: </b>Aguardando Assinatura<br>
	 * <b>Valor:</b> 3
	 */
	AGUARDANDO_ASSINATURA("Aguardando Assinatura"),

	/**
	 * <b>Descrição: </b>Aguardando Aprovação<br>
	 * <b>Valor:</b> 4
	 */
	AGUARDANDO_APROVACAO("Aguardando Aprovação");

	/**
     * Descrição do {@link Enum}
     */
	private String descricao;

	 /**
     * Construtor com o parâmetro descricao.
     *
     * @param descricao - Parâmetro a ser setado no campo <code>{@link #descricao} </code> do {@link Enum}
     */
	private StatusRaia(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Método get da propriedade Descricao do {@link Enum}
	 *
	 * @return Retorna a descricao do {@link Enum}
	 */
	public String getDescricao() {
		return this.descricao;
	}

	/**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #CONCLUIDO}
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CONCLUIDO}, caso contrário retorna <code>false</code>
	 */
    public boolean isConcluido() {
    	if (name().equals(CONCLUIDO.name())) {
            return true;
    	}
        return false;
    }

	/**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #RASCUNHO}
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #RASCUNHO}, caso contrário retorna <code>false</code>
	 */
    public boolean isRascunho() {
    	if (name().equals(RASCUNHO.name())) {
            return true;
    	}
        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADO}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADO}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelado() {
        if (name().equals(CANCELADO.name())) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_ASSINATURA}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_ASSINATURA}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoAssinatura() {
        if (name().equals(AGUARDANDO_ASSINATURA.name())) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_APROVACAO}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_APROVACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoAprovacao() {
        if (name().equals(AGUARDANDO_APROVACAO.name())) {
            return true;
        }
        return false;
    }

	public static List<StatusRaia> valuesOrderedByDescricao() {
		List<StatusRaia> list =  Arrays.asList(StatusRaia.values());
		Collections.sort(list, new Comparator<StatusRaia>() {

			@Override
			public int compare(StatusRaia o1, StatusRaia o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

	/**
     * Retorna o StatusRaia correspondente do número passado por parâmetro.
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira - 22/11/2017 08:46:23
     */
	public static StatusRaia fromInt(int valor) {
		for (StatusRaia statusRaia : StatusRaia.values()) {
			if (statusRaia.ordinal() == valor){
				return statusRaia;
			}
		}

		return null;
	}

	/**
     * Retorna uma lista com os Status que representam que o Raia está ativo.
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira - 22/11/2017 08:48:59
     */
	public static List<StatusRaia> getListStatusRaiasAtivos() {
		List<StatusRaia> listRaias = new ArrayList<>();

		listRaias.add(CONCLUIDO);
		listRaias.add(RASCUNHO);
		listRaias.add(AGUARDANDO_ASSINATURA);
		listRaias.add(AGUARDANDO_APROVACAO);

		return listRaias;
	}
}