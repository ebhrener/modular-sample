package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

public enum StatusBuscaProcesso {

	TODOS("Todos"), POS_GED("Pós-GED"), PRE_GED("Pré-GED");

	private String descricao;

	private StatusBuscaProcesso(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public static List<StatusBuscaProcesso> listStatusBuscaProcesso(){
		List<StatusBuscaProcesso> listStatusBusca = new ArrayList<StatusBuscaProcesso>();
		listStatusBusca.add(TODOS);
		listStatusBusca.add(PRE_GED);
		listStatusBusca.add(POS_GED);

		return listStatusBusca;
	}

}
