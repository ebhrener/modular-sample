package br.gov.ce.semace.erp.enuns;

public enum ClassificacaoShape {

	/*0*/AREA("Área", "processos"),
	/*1*/EQUIPAMENTO("Equipamento", "equipamentos"),
	/*2*/SECUNDARIO("Secundário", "extras");

	private String descricao;

	private String schema;

	private ClassificacaoShape(String descricao, String schema) {
		this.descricao = descricao;
		this.schema = schema;
	}

	public String getDescricao() {
		return descricao;
	}

	public boolean isArea() {
		return AREA.equals(this);
	}

	public boolean isEquipamento() {
		return EQUIPAMENTO.equals(this);
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}
}
