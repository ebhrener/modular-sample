package br.gov.ce.semace.erp.enuns;

import br.gov.ce.semace.erp.entity.contrato.Contrato;

/**
 * {@link Enum} referente ao Tipo de {@link Contrato}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #REGULAR} <br>
 * {@link #EMERGENCIAL} <br>
 * 
 * @author renearaujo
 */
public enum TipoContrato {

	/**
	 * <b>Descrição: </b>Aguardando Envio do Auto de Infração <br>
	 * <b>Valor:</b> 0
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 1, 2013 4:57:33 PM
	 */
	REGULAR("Regular"),

	/**
	 * <b>Descrição: </b>Aguardando AR do Auto de Infração Defesa <br>
	 * <b>Valor:</b> 1
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 8:30:48 AM
	 */
	EMERGENCIAL("Emergencial");
	
    /**
     * Descrição do {@link Enum}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:40:20 PM
     */
	private String descricao;
	

	 /**
     * Construtor com os parâmetros descricao e valor.
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:40:49 PM
     * 
     * @param description
     *            Parâmetro a ser setado no campo <code>{@link #descricao} </code> do {@link Enum}
     * 
     * @param value
     *            Parâmetro a ser setado no campo <code> {@link #value} </code> do {@link Enum}
     */
	private TipoContrato(String description) {
		this.setDescricao(description);
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isEmergencial(){
		if(this.name().equals(EMERGENCIAL)){
			return true;
		}
		
		return false;
	}
	
	public boolean isRegular(){
		if(this.name().equals(REGULAR)){
			return true;
		}
		
		return false;
	}
	
	
}
