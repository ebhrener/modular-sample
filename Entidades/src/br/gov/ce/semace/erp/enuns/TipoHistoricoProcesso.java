package br.gov.ce.semace.erp.enuns;

import br.gov.ce.semace.erp.entity.protocolo.HistoricoProcesso;

/**
 * {@link Enum} referente ao Tipo de Histórico do Processo (ver {@link HistoricoProcesso})
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #JULGAMENTO} <br>
 * {@link #PROCESSO} <br>
 *
 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/07/2013 17:01:03
 */
public enum TipoHistoricoProcesso {

	/**
	 * <b>Descrição: </b>Julgamento<br>
	 * <b>Valor Ordinal:</b> 0
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/07/2013 17:01:20
	 */
	JULGAMENTO("Julgamento"),
	/**
	 * <b>Descrição: </b>Processo<br>
	 * <b>Valor Ordinal:</b> 1
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 09:40:26
	 */
	PROCESSO("Processo");

    /**
     * Descrição do tipo do {@link Enum} {@link TipoHistoricoProcesso}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/07/2013 17:02:18
     */
	private String descricao;

	 /**
     * Construtor com os parâmetros descricao e valor.
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/07/2013 17:02:44
     *
     * @param description
     *            Parâmetro a ser setado no campo <code>{@link #descricao} </code> do {@link Enum}
     *
     * @param value
     *            Parâmetro a ser setado no campo <code> {@link #value} </code> do {@link Enum}
     */
	private TipoHistoricoProcesso(String descricao) {
		this.descricao = descricao;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #JULGAMENTO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/07/2013 17:00:00
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #JULGAMENTO}, caso contrário retorna <code>false</code>
     */
	public boolean isTipoJulgamento() {

		// verifica se o name é igual ao do tipo JULGAMENTO
		if (name().equals(JULGAMENTO.name())) {
			return true;
		}

		return false;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PROCESSO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 09:41:24
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PROCESSO}, caso contrário retorna <code>false</code>
     */
	public boolean isTipoProcesso() {

		// verifica se o name é igual ao do tipo PROCESSO
		if (name().equals(PROCESSO.name())) {
			return true;
		}

		return false;
	}

	/**
	 * Gets the {@link #descricao} property
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 04/07/2013 09:24:58
	 *
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Set the {@link #descricao} property
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 04/07/2013 09:23:58
	 *
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
