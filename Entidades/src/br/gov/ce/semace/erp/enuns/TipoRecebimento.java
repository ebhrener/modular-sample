package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

public enum TipoRecebimento {

	AGUARDANDO_RECEBIMENTO("Aguardando Recebimento"),
	RECEBIDO ("Recebido"),
	NAO_LOCALIZADO ("Não Localizado"),
	NAO_RECEBIDO ("Não Recebido"),
	EDITAL ("Edital"),
	RECUSADO ("Recusado"),
	MUDOU_SE ("Mudou-se");

	String descricao;

	private TipoRecebimento(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #RECEBIDO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:37:25 PM
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #RECEBIDO}, caso contrário retorna <code>false</code>
     */
	public boolean isRecebido() {

		if (name().equals(RECEBIDO.name())) {
			return true;
		}

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_RECEBIMENTO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:38:24 PM
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_RECEBIMENTO}, caso contrário retorna <code>false</code>
     */
	public boolean isAguardandoRecebimento() {

		if (name().equals(AGUARDANDO_RECEBIMENTO.name())) {
			return true;
		}

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #NAO_RECEBIDO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:39:03 PM
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #NAO_RECEBIDO}, caso contrário retorna <code>false</code>
     */
	public boolean isNaoRecebido() {

		if (name().equals(NAO_RECEBIDO.name())) {
			return true;
		}

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #NAO_LOCALIZADO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:39:56 PM
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #NAO_LOCALIZADO}, caso contrário retorna <code>false</code>
     */
	public boolean isNaoLocalizado() {

		if (name().equals(NAO_LOCALIZADO.name())) {
			return true;
		}

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EDITAL}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:40:33 PM
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EDITAL}, caso contrário retorna <code>false</code>
     */
	public boolean isEdital() {

		if (name().equals(EDITAL.name())) {
			return true;
		}

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #RECUSADO}
     *
     * @author Saulo Fernandes [saulofernandes.ti@gmail.com] - 04/07/2013 16:02:28
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #RECUSADO}, caso contrário retorna <code>false</code>
     */
	public boolean isRecusado() {

		if (name().equals(RECUSADO.name())) {
			return true;
		}

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #MUDOU_SE}
     *
     * @author Saulo Fernandes [saulofernandes.ti@gmail.com] - 04/07/2013 16:03:19
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #MUDOU_SE}, caso contrário retorna <code>false</code>
     */
	public boolean isMudouse() {

		if (name().equals(MUDOU_SE.name())) {
			return true;
		}

		return false;
	}


	public static final List<TipoRecebimento> getListRetornoAR() {

		List<TipoRecebimento> listTipoRecebimento = new ArrayList<>();

		listTipoRecebimento.add(TipoRecebimento.MUDOU_SE);
		listTipoRecebimento.add(TipoRecebimento.NAO_LOCALIZADO);
		listTipoRecebimento.add(TipoRecebimento.NAO_RECEBIDO);
		listTipoRecebimento.add(TipoRecebimento.RECEBIDO);
		listTipoRecebimento.add(TipoRecebimento.RECUSADO);

		return listTipoRecebimento;
	}

}
