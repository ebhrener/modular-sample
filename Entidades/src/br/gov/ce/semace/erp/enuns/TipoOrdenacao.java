package br.gov.ce.semace.erp.enuns;

public enum TipoOrdenacao {

	ASC, DESC;

	public boolean isAscending() {
		return this.name().equals(ASC.name());
	}

	public boolean isDescending() {
		return this.name().equals(DESC.name());
	}

	public static TipoOrdenacao get(String tipo) {
		return (tipo != null && !tipo.trim().equals("")) ? valueOf(tipo.trim().toUpperCase()) : null;
	}
}
