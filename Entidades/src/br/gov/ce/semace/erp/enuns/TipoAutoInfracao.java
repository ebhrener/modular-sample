package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

import br.gov.ce.semace.erp.entity.fiscalizacao.AutoInfracao;

/**
 * {@link Enum} referente aos Tipos de {@link AutoInfracao}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #ADVERTENCIA} <br>
 * {@link #FUMACA} <br>
 * {@link #MULTA} <br>
 *
 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 22/04/2016 10:41:23
 */
public enum TipoAutoInfracao {

	/**
	 * <b>Descrição: </b>Multa<br>
	 * <b>Valor:</b> 0
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 22/04/2016 10:42:32
	 */
	MULTA("Multa"),

	/**
	 * <b>Descrição: </b>Advertência<br>
	 * <b>Valor:</b> 1
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 22/04/2016 10:43:04
	 */
	ADVERTENCIA("Advertência"),

	/**
	 * <b>Descrição: </b>Poluição Automotiva<br>
	 * <b>Valor:</b> 2
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 22/04/2016 10:43:21
	 */
	FUMACA("Poluição Automotiva");


	private final String descricao;

	private TipoAutoInfracao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoAutoInfracao getEnumByDescricao(String descricao){

		for (TipoAutoInfracao tipoAuto : values()) {
			if(tipoAuto.getDescricao().equals(descricao)) {
				return tipoAuto;
			}
		}

		return null;
	}

	/**
	 * {@link List}a com os {@link TipoAutoInfracao} que podem ser utilizados
	 * no cadastro/alteração de um {@link AutoInfracao}:
	 *
	 *  <ul>{@link TipoAutoInfracao#ADVERTENCIA}</ul>
	 *  <ul>{@link TipoAutoInfracao#MULTA}</ul>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 22/04/2016 10:39:11
	 *
	 * @return
	 * 		Retorna uma {@link List}a de {@link TipoAutoInfracao}
	 */
	public static List<TipoAutoInfracao> getlistTipoAutoInfracaoNovo(){

		List<TipoAutoInfracao> list = new ArrayList<>();

		list.add(ADVERTENCIA);
		list.add(MULTA);

		return list;
	}

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #MULTA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 22/04/2016 10:53:12
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #MULTA}, caso contrário retorna <code>false</code>
     */
    public boolean isMulta() {

        if (name().equals(MULTA.name())){
            return true;
        }

        return false;
    }
    
    public static TipoAutoInfracao get(int ordinal) {
		for (TipoAutoInfracao tipoAutoInfracao : values()) {
			if (ordinal == tipoAutoInfracao.ordinal()) {
				return tipoAutoInfracao;
			}
		}

		return null;
	}

}
