package br.gov.ce.semace.erp.enuns;

public enum LocalizacaoSetor {
	
	LESTE("Leste"),
	CENTRO("Centro"),
	OESTE("Oeste");
	
	private final String descricao;
	
	private LocalizacaoSetor(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao(){
		return descricao;
	}
	
	public static LocalizacaoSetor getEnumByDescricao(String descricao){
		
		for (LocalizacaoSetor setor : values()) {
			if(setor.getDescricao().equals(descricao))
				return setor;
		}
		
		return null;
	}

}
