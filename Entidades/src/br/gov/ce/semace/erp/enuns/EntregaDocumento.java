package br.gov.ce.semace.erp.enuns;

public enum EntregaDocumento {

	SEMACE("na Semace"),
    CORREIOS("via Correios");

	private String descricao;

	private EntregaDocumento(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
