package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Status da Divida Ativa
 * 
 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 10/04/2014
 *
 *	Status SIGA
 *	<option value="0">Devedor</option>
 *	<option value="1">Acordo - Parcelamento</option>
 *	<option value="5">Acordo - Proj. de Criação de Horto</option>
 *	<option value="6">Acordo - Proj. de Recup. de Área degradada (PRAD)</option>
 *	<option value="7">Sub Judice - suspensão por decisão</option>
 *	<option value="2">Pendente - Execução Fiscal não ajuizada</option>
 *	<option value="3">Extinta</option>
 *	<option value="4">Cancelada</option>
 *	<option value="11">Extinta - RPV/Precatório</option>
 *	<option value="12">Extinta - Precatório</option>
 *	<option value="13">Extinta - RPV</option>
 *	<option value="14">RPV</option>
 *	<option value="15">Precatório</option>
 *
 */
public enum StatusDivida {
	
	//TODO: DIVIDA verificar quais status devem ser exibidos na edição
	DEVEDOR("Devedor", false, 0, 2),
	ACORDO("Acordo", true, 1, 5, 6),
	EXTINTA("Extinta", true, 3, 11, 12, 13),
	SUSPENSO("Suspenso por decisão", true, 7),
	CANCELADO("Cancelada", true, 4),
	RPV("RPV", true, 14),
	PRECATORIO("Precatório", true, 15)
	;

	private String descricao;
	
	private boolean efeito;
	
	private Integer[] statusSiga;
	
	private StatusDivida(String descricao, boolean efeito, Integer...statuSiga) {
		this.descricao = descricao;
		this.setEfeito(efeito);
		this.setStatusSiga(statuSiga);
	}

	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isEfeito() {
		return efeito;
	}

	public void setEfeito(boolean efeito) {
		this.efeito = efeito;
	}

	public Integer[] getStatusSiga() {
		return statusSiga;
	}

	public void setStatusSiga(Integer[] statusSiga) {
		this.statusSiga = statusSiga;
	}
	
	public static StatusDivida getStatus(Integer statusSiga){
		for (StatusDivida statusDivida : values()) {
			if (Arrays.asList(statusDivida.getStatusSiga()).contains(statusSiga)) {
				return statusDivida;
			}
		}
		return null;
	}
	
	public static List<StatusDivida> getValuesOrdenados(){
		List<StatusDivida> list =  Arrays.asList(StatusDivida.values());
		Collections.sort(list, new Comparator<StatusDivida>() {

			@Override
			public int compare(StatusDivida o1, StatusDivida o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}
}
