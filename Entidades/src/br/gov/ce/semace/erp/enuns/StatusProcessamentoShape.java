package br.gov.ce.semace.erp.enuns;

public enum StatusProcessamentoShape {

	A_PROCESSAR("À Processar"),
	PROCESSADO("Processado"),
	ERRO("Erro no processamento");

	private StatusProcessamentoShape(String descricao) {
		this.setDescricao(descricao);
	}

	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
