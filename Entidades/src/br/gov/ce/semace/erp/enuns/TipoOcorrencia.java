package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.entity.fiscalizacao.Ocorrencia;
import br.gov.ce.semace.erp.entity.fiscalizacao.OrdemFiscalizacao;
import br.gov.ce.semace.erp.entity.fiscalizacao.Resposta;

/**
 * {@link Enum} referente aos Tipos do {@link Ocorrencia}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #PRESENCIAL} <br>
 * {@link #INTERNO} <br>
 * {@link #TELEFONE} <br>
 * {@link #ANONIMO} <br>
 * {@link #CAMPO} <br>
 * {@link #OUVIDORIA} <br>
 * {@link #MP} <br>
 * {@link #PROCESSO} <br>
 * {@link #DOF} <br>
 * {@link #SEDFAUNA} <br>
 * {@link #SISFAUNA} <br>
 * {@link #SISSPASS} <br>
 * {@link #BENS_APREENDIDOS} <br>
 * {@link #CETAS_TCO} <br>
 *
 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/04/2016 08:55:24
 */
public enum TipoOcorrencia {

	/**
	 * Indica se a {@link Ocorrencia} foi disparada de algo que foi presenciado em campo pelos fiscais
	 * <p>
	 * <b>Descrição: </b>Presencial<br>
	 * <b>Valor:</b> 0
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/04/2016 08:55:49
	 */
	PRESENCIAL("Presencial", true),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada de algum sistema ou algo similar
	 * <p>
	 * <b>Descrição: </b>Interno<br>
	 * <b>Valor:</b> 1
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/04/2016 09:01:49
	 */
	INTERNO("Interno", true),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através de uma denuncia por telefone
	 * <p>
	 * <b>Descrição: </b>Telefone<br>
	 * <b>Valor:</b> 2
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/04/2016 09:01:44
	 */
	TELEFONE("Telefone", true),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através de uma denuncia Anônima.
	 * <p>
	 * <b>Descrição: </b>Anônimo<br>
	 * <b>Valor:</b> 3
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/04/2016 09:05:29
	 */
	ANONIMO("Anônimo", true),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através de um trabalho em Campo.
	 * <p>
	 * <b>Descrição: </b>Em Campo<br>
	 * <b>Valor:</b> 4
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/04/2016 09:04:21
	 */
	CAMPO("Em Campo", false),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através da Ouvidoria.
	 * <p>
	 * <b>Descrição: </b>Ouvidoria<br>
	 * <b>Valor:</b> 5
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/04/2016 09:05:25
	 */
	OUVIDORIA("Ouvidoria", false),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através de um Processo do MP.
	 * <p>
	 * <b>Descrição: </b>Processos Judiciais e MP<br>
	 * <b>Valor:</b> 6
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/04/2016 09:06:42
	 */
	MP("Processos Judicias e MP", false),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através de um Processo.
	 * <p>
	 * <b>Descrição: </b>Processo<br>
	 * <b>Valor:</b> 7
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/04/2016 09:06:17
	 */
	PROCESSO("Processo", false),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através de DOF.
	 * <p>
	 * <b>Descrição: </b>DOF<br>
	 * <b>Valor:</b> 8
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/04/2016 09:05:56
	 */
	DOF("DOF", false),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através de um SEDFAUNA.
	 * <p>
	 * <b>Descrição: </b>SEDFAUNA<br>
	 * <b>Valor:</b> 9
	 *
	 * @author Rodrigo Silva Oliveira
	 */
	SEDFAUNA("SEDFAUNA", false),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através de um SISFAUNA.
	 * <p>
	 * <b>Descrição: </b>SISFAUNA<br>
	 * <b>Valor:</b> 10
	 *
	 * @author Rodrigo Silva Oliveira
	 */
	SISFAUNA("SISFAUNA", false),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através de um SISSPASS.
	 * <p>
	 * <b>Descrição: </b>SISSPASS<br>
	 * <b>Valor:</b> 11
	 *
	 * @author Rodrigo Silva Oliveira
	 */
	SISSPASS("SISSPASS", false),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através de BENS APREENDIDOS.
	 * <p>
	 * <b>Descrição: </b>Bens Apreendidos<br>
	 * <b>Valor:</b> 12
	 *
	 * @author Rodrigo Silva Oliveira - 10/01/2017 14:31:22
	 */
	BENS_APREENDIDOS("Bens Apreendidos", false),

	/**
	 * Indica se a {@link Ocorrencia} foi disparada através do CETAS/TCO.
	 * <p>
	 * <b>Descrição: </b>CETAS/TCO<br>
	 * <b>Valor:</b> 13
	 *
	 * @author Rodrigo Silva Oliveira - 10/01/2017 14:32:10
	 */
	CETAS_TCO("CETAS/TCO", false);

	private String descricao;
	private Boolean isVisible;

	private TipoOcorrencia(String descricao, Boolean isVisible) {
		this.descricao = descricao;
		this.isVisible = isVisible;
	}

	public String getDescricao() {
		return descricao;
	}

	@Override
	public String toString() {
		return descricao;
	}

	public Boolean getIsVisible() {
		return isVisible;
	}

	public static List<TipoOcorrencia> isVinculoDocumentoLiberada() {
		List<TipoOcorrencia> list = new ArrayList<TipoOcorrencia>();
		for(TipoOcorrencia tipo: TipoOcorrencia.values()){
			list.add(tipo);
		}
		return list;
	}

	/**
	 * Informa a lista de {@link TipoOcorrencia} que requerem um processo no cadastro da {@link Ocorrencia}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 13/10/2015 16:40:43
	 *
	 * @return
	 * 		Retorna uma lista com os {@link TipoOcorrencia} configurados:
     *
     *  <br>{@link #INTERNO} <br>
     * 		{@link #MP} <br>
     * 		{@link #PROCESSO} <br>
     * 		{@link #DOF} <br>
     * 		{@link #BENS_APREENDIDOS} <br>
	 */
	public static List<TipoOcorrencia> listTipoOcorrenciaRequeremProcesso(){
		List<TipoOcorrencia> list = new ArrayList<>();

		list.add(INTERNO);
		list.add(MP);
		list.add(PROCESSO);
		list.add(DOF);
		list.add(BENS_APREENDIDOS);

		return list;
	}

	/**
	 * Informa a lista de {@link TipoOcorrencia} que as coordenadas são opcionais para a resposta da {@link Ocorrencia}
	 *
	 * @author rodrigo.silva - Rodrigo Silva Oliveira
	 *
	 * @return
	 * 		Retorna uma lista com os {@link TipoOcorrencia} configurados:
     *
     *  <br>{@link #INTERNO} <br>
     * 		{@link #MP} <br>
     * 		{@link #PROCESSO} <br>
     * 		{@link #DOF} <br>
     * 		{@link #BENS_APREENDIDOS} <br>
	 */
	public static List<TipoOcorrencia> listTipoOcorrenciaCoordenadasOpcionais(){
		List<TipoOcorrencia> list = new ArrayList<>();

		list.add(DOF);
		list.add(SEDFAUNA);
		list.add(SISFAUNA);
		list.add(SISSPASS);
		list.add(BENS_APREENDIDOS);

		return list;
	}

	/**
	 * Verifica se o {@link #name()} do {@link Enum} é
	 * {@link #DOF}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 01/09/2015 14:41:18
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #DOF}, caso contrário retorna <code>false</code>
	 */
    public boolean isDOF() {

    	if (name().equals(DOF.name())){
            return true;
    	}

        return false;
    }

	/**
	 * Verifica se o {@link #name()} do {@link Enum} é
	 * {@link #CAMPO}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/11/2015 11:06:15
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CAMPO}, caso contrário retorna <code>false</code>
	 */
    public boolean isCampo() {

    	if (name().equals(CAMPO.name())){
            return true;
    	}

        return false;
    }

	/**
	 * Verifica se o {@link #name()} do {@link Enum} é
	 * {@link #BENS_APREENDIDOS}
	 *
	 * @author Rodrigo Silva Oliveira - 10/01/2017 14:34:25
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #BENS_APREENDIDOS}, caso contrário retorna <code>false</code>
	 */
    public boolean isBensApreendidos() {

    	if (name().equals(BENS_APREENDIDOS.name())){
            return true;
    	}

        return false;
    }

	/**
	 * Verifica se o {@link #name()} do {@link Enum} é
	 * {@link #CETAS_TCO}
	 *
	 * @author Rodrigo Silva Oliveira - 10/01/2017 14:35:13
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CETAS_TCO}, caso contrário retorna <code>false</code>
	 */
    public boolean isCetasTco() {

    	if (name().equals(CETAS_TCO.name())){
            return true;
    	}

        return false;
    }

    /**
	 * Verifica se o {@link #name()} do {@link Enum} é
	 * {@link #OUVIDORIA}
	 *
	 * @author Rodrigo Silva Oliveira - 05/05/2017 08:04:29
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #OUVIDORIA}, caso contrário retorna <code>false</code>
	 */
    public boolean isOuvidoria() {

    	if (name().equals(OUVIDORIA.name())){
            return true;
    	}

        return false;
    }

    /**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #TELEFONE}
	 *
	 * @author Rodrigo Silva Oliveira - 24/08/2018 16:39:37
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #TELEFONE}, caso contrário retorna <code>false</code>
	 */
    public boolean isTelefone() {
    	if (name().equals(TELEFONE.name())){
            return true;
    	}
        return false;
    }

    /**
	 * Informa a lista de {@link TipoOcorrencia} que exigem denunciante ao se cadastrar/alterar uma {@link Resposta} de {@link Ocorrencia}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/04/2016 08:39:32
	 *
	 * @return
	 * 		Retorna uma lista com os {@link TipoOcorrencia} configurados:
     *
     * 		{@link #MP} <br>
	 */
	public static List<TipoOcorrencia> listTipoOcorrenciaExigeDenunciante(){
		List<TipoOcorrencia> list = new ArrayList<>();

		list.add(PRESENCIAL);
		list.add(TELEFONE);
		list.add(CAMPO);
		list.add(ANONIMO);

		return list;
	}

	/**
	 * Informa a lista de {@link TipoOcorrencia} de uma {@link Ocorrencia} que serão prioritárias automaticamente no cadastro da {@link OrdemFiscalizacao}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 01/03/2016 11:15:55
	 *
	 * @return
	 * 		Retorna uma lista com os {@link TipoOcorrencia} configurados:
     *
     * 		{@link #MP} <br>
     * 		{@link #PROCESSO} <br>
	 */
	public static List<TipoOcorrencia> listTipoOcorrenciaPrioritariaOrdemFiscalizacao(){
		List<TipoOcorrencia> list = new ArrayList<>();

		list.add(MP);

		return list;
	}

	public static List<TipoOcorrencia> valuesOrderedByDescricao(){
		List<TipoOcorrencia> list =  Arrays.asList(TipoOcorrencia.values());
		Collections.sort(list, new Comparator<TipoOcorrencia>() {

			@Override
			public int compare(TipoOcorrencia o1, TipoOcorrencia o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

	/**
	 * Informa a lista de {@link TipoOcorrencia} em que os fiscais tem permissão para cadastrar no ERP, as demais somente gerência.
	 *
	 * @author rodrigo.silva - Rodrigo Silva Oliveira - 06/03/2017 10:33:51
	 *
	 * @return - Retorna uma lista com os {@link TipoOcorrencia} configurados:
     *
     *  <br> {@link #CAMPO} <br>
     * 		{@link #CETAS_TCO} <br>
	 */
	public static List<TipoOcorrencia> listTipoOcorrenciaFiscalCadastro(){
		List<TipoOcorrencia> list = new ArrayList<>();

		list.add(CAMPO);
		list.add(CETAS_TCO);

		return list;
	}
}