package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.entity.fiscalizacao.Ocorrencia;
import br.gov.ce.semace.erp.entity.fiscalizacao.Resposta;

public enum TipoVinculoTermo {

	TERMO_COM_AUTO("Termo para Auto de Infração"),
	TERMO_COM_NOTIFICACAO("Termo para Notificação"),
	TERMO_SEM_VINCULO("Termo sem Vínculo");

	private final String descricao;

	private TipoVinculoTermo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoVinculoTermo getEnumByDescricao(String descricao){

		for (TipoVinculoTermo tipo : values()) {
			if(tipo.getDescricao().contains(descricao)) {
				return tipo;
			}
		}

		return null;
	}

	public static List<TipoVinculoTermo> valuesOrderedByDescricao(){

		List<TipoVinculoTermo> list =  Arrays.asList(TipoVinculoTermo.values());

		Collections.sort(list, new Comparator<TipoVinculoTermo>() {

			@Override
			public int compare(TipoVinculoTermo o1, TipoVinculoTermo o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});

		return list;
	}

	/**
	 * Informa a lista de {@link TipoOcorrencia} que exigem denunciante ao se cadastrar/alterar uma {@link Resposta} de {@link Ocorrencia}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 23/05/2016 08:55:15
	 *
	 * @return
	 * 		Retorna uma lista com os {@link TipoOcorrencia} configurados:
     *
     * 		{@link #MP} <br>
	 */
	public static List<TipoVinculoTermo> listTipoVinculoTermoExigeNumeroDocumento(){
		List<TipoVinculoTermo> list = new ArrayList<>();

		list.add(TERMO_COM_AUTO);
		list.add(TERMO_COM_NOTIFICACAO);

		return list;
	}


    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #TERMO_COM_AUTO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/05/2016 13:35:16
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #TERMO_COM_AUTO}, caso contrário retorna <code>false</code>
     */
    public boolean isAutoInfracao() {

        if (name().equals(TERMO_COM_AUTO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #TERMO_COM_NOTIFICACAO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/05/2016 13:35:07
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #TERMO_COM_NOTIFICACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isNotificacao() {

        if (name().equals(TERMO_COM_NOTIFICACAO.name())){
            return true;
        }

        return false;
    }
    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #TERMO_SEM_VINCULO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/05/2016 15:02:17
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #TERMO_SEM_VINCULO}, caso contrário retorna <code>false</code>
     */
    public boolean isSemVinculo() {

        if (name().equals(TERMO_SEM_VINCULO.name())){
            return true;
        }

        return false;
    }
}