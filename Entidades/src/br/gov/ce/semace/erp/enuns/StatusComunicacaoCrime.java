package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.entity.fiscalizacao.ComunicacaoCrime;

/**
 * {@link Enum} referente aos Status da {@link ComunicacaoCrime}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #ATIVO} <br>
 * {@link #CANCELADO} <br>
 * {@link #RASCUNHO} <br>
 * {@link #AGUARDANDO_ASSINATURA} <br>
 * {@link #AGUARDANDO_APROVACAO} <br>
 *
 * @author rodrigo.silva - Rodrigo Silva Oliveira - 06/12/2017 15:06:07
 */
public enum StatusComunicacaoCrime {

	/**
	 * <b>Descrição: </b>Ativo<br>
	 * <b>Valor:</b> 0
	 */
	ATIVO("Ativo"),

	/**
	 * <b>Descrição: </b>Cancelado<br>
	 * <b>Valor:</b> 1
	 */
	CANCELADO("Cancelado"),

	/**
	 * <b>Descrição: </b>Rascunho<br>
	 * <b>Valor:</b> 2
	 */
	RASCUNHO("Rascunho"),

	/**
	 * <b>Descrição: </b>Aguardando Assinatura<br>
	 * <b>Valor:</b> 3
	 */
	AGUARDANDO_ASSINATURA("Aguardando Assinatura"),

	/**
	 * <b>Descrição: </b>Aguardando Aprovação<br>
	 * <b>Valor:</b> 4
	 */
	AGUARDANDO_APROVACAO("Aguardando Aprovação");

	/**
     * Descrição da {@link Enum}
     */
	private String descricao;

	 /**
     * Construtor com o parâmetro descricao.
     *
     * @param descricao - Parâmetro a ser setado no campo <code>{@link #descricao} </code> do {@link Enum}
     */
	private StatusComunicacaoCrime(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Método get da propriedade Descricao da {@link Enum}
	 *
	 * @return Retorna a descricao da {@link Enum}
	 */
	public String getDescricao() {
		return this.descricao;
	}

	/**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #ATIVO}
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ATIVO}, caso contrário retorna <code>false</code>
	 */
    public boolean isAtivo() {
    	if (name().equals(ATIVO.name())) {
            return true;
    	}

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADO}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADO}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelado() {
        if (name().equals(CANCELADO.name())) {
            return true;
        }

        return false;
    }

    /**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #RASCUNHO}
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #RASCUNHO}, caso contrário retorna <code>false</code>
	 */
    public boolean isRascunho() {
    	if (name().equals(RASCUNHO.name())) {
            return true;
    	}
        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_ASSINATURA}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_ASSINATURA}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoAssinatura() {
        if (name().equals(AGUARDANDO_ASSINATURA.name())) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_APROVACAO}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_APROVACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoAprovacao() {
        if (name().equals(AGUARDANDO_APROVACAO.name())) {
            return true;
        }
        return false;
    }

    /**
     * Retorna uma lista com os Status que representam que a CCR está ativa.
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira - 20/04/2018 14:51:17
     */
	public static List<StatusComunicacaoCrime> getListStatusComunicacaoCrimeAtivos() {
		List<StatusComunicacaoCrime> listComunicacaoCrime = new ArrayList<>();

		listComunicacaoCrime.add(ATIVO);
		listComunicacaoCrime.add(RASCUNHO);
		listComunicacaoCrime.add(AGUARDANDO_ASSINATURA);
		listComunicacaoCrime.add(AGUARDANDO_APROVACAO);

		return listComunicacaoCrime;
	}

	public static List<StatusComunicacaoCrime> valuesOrderedByDescricao() {
		List<StatusComunicacaoCrime> list =  Arrays.asList(StatusComunicacaoCrime.values());
		Collections.sort(list, new Comparator<StatusComunicacaoCrime>() {

			@Override
			public int compare(StatusComunicacaoCrime o1, StatusComunicacaoCrime o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

	/**
     * Retorna o StatusComunicacaoCrime correspondente do número passado por parâmetro.
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira - 06/12/2017 15:17:25
     */
	public static StatusComunicacaoCrime fromInt(int valor) {
		for (StatusComunicacaoCrime statusComunicacaoCrime : StatusComunicacaoCrime.values()) {
			if (statusComunicacaoCrime.ordinal() == valor){
				return statusComunicacaoCrime;
			}
		}

		return null;
	}
}