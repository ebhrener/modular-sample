package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ModeloDocumentoDivida {

	/*0*/PETICAO_INICIAL_FORTALEZA(TipoDocumento.EXECUCAO_FISCAL, "Petição Inicial Fortaleza - Execução Fiscal","peticao_inicial.html", "PI"),
	/*1*/PETICAO_INICIAL_INTERIOR(TipoDocumento.EXECUCAO_FISCAL, "Petição Inicial Interior - Execução Fiscal","peticao_inicial.html", "PI"),
	/*2*/CDA_LT(TipoDocumento.CDA, "Certidão de Dívida Ativa com Auto de Infração lavrado até 04/01/2011", "cda.html", "CDA"),
	/*3*/CDA_GT(TipoDocumento.CDA, "Certidão de Dívida com Auto de Infração lavrado a partir de 05/01/2011", "cda.html", "CDA"),
	/*4*/CDA_LT_COM_ACORDO(TipoDocumento.CDA, "Certidão de Dívida com Auto de Infração lavrado até 04/01/2011 com descumprimento de parcelamento", "cda.html", "CDA"),
	/*5*/CDA_GT_COM_ACORDO(TipoDocumento.CDA, "Certidão de Dívida com Auto de Infração lavrado a partir de 05/01/2011 com descumprimento de parcelamento", "cda.html", "CDA"),
	/*6*/TIDA_LT(TipoDocumento.TIDA, "Termo de Inscrição em Dívida Ativa com Auto de Infração lavrado até 04/01/2011", "tida_trida.html", "TIDA"),
	/*7*/TIDA_GT(TipoDocumento.TIDA, "Termo de Inscrição em Dívida Ativa com Auto de Infração lavrado a partir de 05/01/2011", "tida_trida.html", "TIDA"),
	/*8*/TIDA_LT_COM_ACORDO(TipoDocumento.TIDA, "Termo de Inscrição em Dívida Ativa com Auto de Infração lavrado até 04/01/2011 com descumprimento de parcelamento", "tida_trida.html", "TIDA"),
	/*9*/TIDA_GT_COM_ACORDO(TipoDocumento.TIDA, "Termo de Inscrição em Dívida Ativa com Auto de Infração lavrado a partir de 05/01/2011 com descumprimento de parcelamento", "tida_trida.html", "TIDA"),
	/*10*/TRIDA_LT(TipoDocumento.TRIDA, "Termo de Retificação de Inscrição em Dívida Ativa com Auto de Infração lavrado até 04/01/2011", "tida_trida.html", "TRIDA"),
	/*11*/TRIDA_GT(TipoDocumento.TRIDA, "Termo de Retificação de Inscrição em Dívida Ativa com Auto de Infração lavrado a partir de 05/01/2011", "tida_trida.html", "TRIDA"),
	/*12*/TRIDA_LT_COM_ACORDO(TipoDocumento.TRIDA, "Termo de Retificação de Inscrição em Dívida Ativa com Auto de Infração lavrado até 04/01/2011 com descumprimento de parcelamento", "tida_trida.html", "TRIDA"),
	/*13*/TRIDA_GT_COM_ACORDO(TipoDocumento.TRIDA, "Termo de Retificação de Inscrição em Dívida Ativa com Auto de Infração lavrado a partir de 05/01/2011 com descumprimento de parcelamento", "tida_trida.html", "TRIDA"),
	/*14*/TCPDADM(TipoDocumento.TCPDADM, "Termo de Compromisso de Parcelamento de Débito Administrativo", "tcpdadm.html", "TCPDADM"),
	;

	private ModeloDocumentoDivida(TipoDocumento tipoDocumento, String descricao, String htmlFile, String simpleName) {
		this.tipoDocumento = tipoDocumento;
		this.descricao = descricao;
		this.setHtmlFile(htmlFile);
		this.simpleName = simpleName;
	}

	private String simpleName;

	private String htmlFile;

	private TipoDocumento tipoDocumento;

	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<ModeloDocumentoDivida> getValues() {
		return Arrays.asList(values());
	}

	public List<ModeloDocumentoDivida> listByTipoDocumento(TipoDocumento tipoDocumento) {
		List<ModeloDocumentoDivida> list = new ArrayList<>();
		for (ModeloDocumentoDivida modelo : values()) {
			if (modelo.getTipoDocumento() == tipoDocumento) {
				list.add(modelo);
			}
		}
		return list;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getHtmlFile() {
		return htmlFile;
	}

	public void setHtmlFile(String htmlFile) {
		this.htmlFile = htmlFile;
	}

	public String getSimpleName() {
		return simpleName;
	}

	public void setSimpleName(String simpleName) {
		this.simpleName = simpleName;
	}

	public boolean hasAcordo() {
		return this == CDA_GT_COM_ACORDO || this == CDA_LT_COM_ACORDO
				|| this == TIDA_GT_COM_ACORDO || this == TIDA_LT_COM_ACORDO
				|| this == TRIDA_GT_COM_ACORDO || this == TRIDA_LT_COM_ACORDO;
	}

	public boolean isCDA() {
		return this == CDA_GT || this == CDA_LT || this == CDA_GT_COM_ACORDO
				|| this == CDA_LT_COM_ACORDO;
	}

	public boolean isTIDA() {
		return this == TIDA_GT || this == TIDA_LT || this == TIDA_GT_COM_ACORDO
				|| this == TIDA_LT_COM_ACORDO;
	}

	public boolean isTRIDA() {
		return this == TRIDA_GT || this == TRIDA_LT || this == TRIDA_GT_COM_ACORDO
				|| this == TRIDA_LT_COM_ACORDO;
	}

	public boolean isGT() {
		return this == CDA_GT || this == CDA_GT_COM_ACORDO
				|| this == TIDA_GT || this == TIDA_GT_COM_ACORDO
				|| this == TRIDA_GT || this == TRIDA_GT_COM_ACORDO;
	}

	public boolean isLT() {
		return this == CDA_LT || this == CDA_LT_COM_ACORDO
				|| this == TIDA_LT || this == TIDA_LT_COM_ACORDO
				|| this == TRIDA_LT || this == TRIDA_LT_COM_ACORDO;
	}

	public boolean isTCPDADM() {
		return this == TCPDADM;
	}

	/**
	 * Retorna os modelos que devem aparecer no combo da tela de geração de documentos
	 */
	public static List<ModeloDocumentoDivida> getValuesComboGeracaoDocumento() {
		return Arrays.asList(PETICAO_INICIAL_FORTALEZA,
				PETICAO_INICIAL_INTERIOR, CDA_LT, CDA_GT, CDA_LT_COM_ACORDO,
				CDA_GT_COM_ACORDO, TIDA_LT, TIDA_GT, TIDA_LT_COM_ACORDO,
				TIDA_GT_COM_ACORDO, TRIDA_LT, TRIDA_GT, TRIDA_LT_COM_ACORDO,
				TRIDA_GT_COM_ACORDO);
	}
}
