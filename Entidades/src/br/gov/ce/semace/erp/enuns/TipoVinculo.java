package br.gov.ce.semace.erp.enuns;

public enum TipoVinculo {
	
	VINCULADO("Vinculado"), 
	DESVINCULADO("Desvinculado");

	private String descricao;

	private TipoVinculo(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
