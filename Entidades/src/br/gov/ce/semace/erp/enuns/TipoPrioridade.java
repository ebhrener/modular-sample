package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum TipoPrioridade {

	ALTA("Alta"), MEDIA("Média"), BAIXA("Baixa");

	private String displayName;

	private TipoPrioridade(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public static List<TipoPrioridade> valuesOrderedByDescricao(){
		List<TipoPrioridade> list =  Arrays.asList(TipoPrioridade.values());
		Collections.sort(list, new Comparator<TipoPrioridade>() {

			@Override
			public int compare(TipoPrioridade o1, TipoPrioridade o2) {
				return o1.getDisplayName().compareTo(o2.getDisplayName());
			}
		});
		return list;
	}
}
