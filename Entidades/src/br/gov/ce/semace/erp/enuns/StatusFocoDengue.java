package br.gov.ce.semace.erp.enuns;

import br.gov.ce.semace.erp.entity.fiscalizacao.Ocorrencia;
import br.gov.ce.semace.erp.entity.fiscalizacao.Resposta;

/**
 * {@link Enum} referente aos Status do Foco da Degue na {@link Resposta} de uma {@link Ocorrencia}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #SIM} <br>
 * {@link #NAO} <br>
 * {@link #NAO_HOUVE_VISTORIA} <br>
 *
 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 07/01/2016 09:36:27
 */
public enum StatusFocoDengue {

	/**
	 * <b>Descrição: </b>Sim<br>
	 * <b>Valor:</b> 0
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 07/01/2016 09:40:42
	 */
	SIM("Sim"),

	/**
	 * <b>Descrição: </b>Não<br>
	 * <b>Valor:</b> 1
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 07/01/2016 09:40:21
	 */
	NAO("Não"),

	/**
	 * <b>Descrição: </b>Não Houve Vistoria<br>
	 * <b>Valor:</b> 2
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 07/01/2016 09:40:12
	 */
	NAO_HOUVE_VISTORIA("Não Houve Vistoria");

	/**
     * Descrição do {@link Enum}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 07/01/2016 09:39:55
     */
	private String descricao;

	 /**
     * Construtor com o parâmetro descricao.
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 07/01/2016 09:39:07
     *
     * @param descricao
     *            Parâmetro a ser setado no campo <code>{@link #descricao} </code> do {@link Enum}
     */
	private StatusFocoDengue(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Método get da propriedade Descricao do {@link Enum}
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 07/01/2016 09:38:58
	 *
	 * @return Retorna a descricao do {@link Enum}
	 */
	public String getDescricao() {
		return this.descricao;
	}

	/**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #SIM}
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 07/01/2016 09:38:04
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #SIM}, caso contrário retorna <code>false</code>
	 */
    public boolean isSim() {

    	if (name().equals(SIM.name())){
            return true;
    	}

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #NAO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 07/01/2016 09:37:54
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #NAO}, caso contrário retorna <code>false</code>
     */
    public boolean isNao() {

        if (name().equals(NAO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #NAO_HOUVE_VISTORIA}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 07/01/2016 09:37:41
     *
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #NAO_HOUVE_VISTORIA}, caso contrário retorna <code>false</code>
     */
    public boolean isNaoHouveVistoria() {

        if (name().equals(NAO_HOUVE_VISTORIA.name())){
            return true;
        }

        return false;
    }
}