package br.gov.ce.semace.erp.enuns;

public enum TipoEnvioTramite {
	
	AREA("Enviar para Área"), 
	TECNICO("Enviar para Técnico"),
	REDIRECIONAR("Redirecionar");
	
	private String descricao;
	
	private TipoEnvioTramite(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
