package br.gov.ce.semace.erp.enuns;

public enum IReportUtil {
	
	REQUERIMENTO_PESSOA_FISICA("/reports/requerimento/requerimento_pessoa_fisica.jasper"),
	REQUERIMENTO_PESSOA_JURIDICA("/reports/requerimento/requerimento_pessoa_juridica.jasper"),
	REQUERIMENTO_PDF("requerimento.pdf");
	
	private final String descricao;
	
	private IReportUtil(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}
