package br.gov.ce.semace.erp.enuns;

public enum TipoProdutoConsumidor {
	
	CONSUMACAO("Consumação"),
	PRODUCAO("Produção");
	
	private String descricao;
	
	private TipoProdutoConsumidor(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}
