package br.gov.ce.semace.erp.enuns;

public enum StatusSolicitacaoServicoLaboratorial {
	
	ABERTO("Aberto"), CANCELADO("Cancelado");
	
	private String descricao;
	
	private StatusSolicitacaoServicoLaboratorial(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
