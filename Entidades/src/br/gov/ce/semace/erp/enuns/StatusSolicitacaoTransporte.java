package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

public enum StatusSolicitacaoTransporte {

	RASCUNHO("Rascunho"),
	AGUARDANDO_MOTORISTA("Aguardando Motorista"),
	AGUARDANDO_TRANSPORTE("Aguardando Transporte"),
	AGUARDANDO_APROVACAO("Aguardando Aprovação"),
	APROVADA("Aprovada"),
	EM_VIAGEM("Em Viagem"),
	REALIZADA("Realizada"),
	NAO_REALIZADA("Não Realizada"),
	CANCELADA("Cancelada");

	String descricao;

	private StatusSolicitacaoTransporte(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public boolean isRascunho() {
		if (this.name().equals(RASCUNHO.name())) {
			return true;
		}

		return false;
	}

	public boolean isAguardandoMotorista() {
		if (this.name().equals(AGUARDANDO_MOTORISTA.name())) {
			return true;
		}

		return false;
	}

	public boolean isAguardandoTransporte() {
		if (this.name().equals(AGUARDANDO_TRANSPORTE.name())) {
			return true;
		}

		return false;
	}

	public boolean isAguardandoAprovacao() {
		if (this.name().equals(AGUARDANDO_APROVACAO.name())) {
			return true;
		}

		return false;
	}

	public boolean isAprovada() {
		if (this.name().equals(APROVADA.name())) {
			return true;
		}

		return false;
	}

	public boolean isEmViagem() {
		if (this.name().equals(EM_VIAGEM.name())) {
			return true;
		}

		return false;
	}

	public boolean isRealizada() {
		if (this.name().equals(REALIZADA.name())) {
			return true;
		}

		return false;
	}

	public boolean isNaoRealizada() {
		if (this.name().equals(NAO_REALIZADA.name())) {
			return true;
		}

		return false;
	}

	public boolean isCancelada() {
		if (this.name().equals(CANCELADA.name())) {
			return true;
		}

		return false;
	}

	public boolean isPermitidoRetornarStatus() {
		if (this.name().equals(AGUARDANDO_MOTORISTA.name())
				|| this.name().equals(AGUARDANDO_TRANSPORTE.name())
				|| this.name().equals(AGUARDANDO_APROVACAO.name())
				|| this.name().equals(APROVADA.name())) {
			return true;
		}

		return false;
	}

	public static List<StatusSolicitacaoTransporte> getListStatusSolicitacaoTransporteOrdenada(){
		ArrayList<StatusSolicitacaoTransporte> listOrdenada = new ArrayList<StatusSolicitacaoTransporte>();
		listOrdenada.add(StatusSolicitacaoTransporte.AGUARDANDO_APROVACAO);
		listOrdenada.add(StatusSolicitacaoTransporte.AGUARDANDO_MOTORISTA);
		listOrdenada.add(StatusSolicitacaoTransporte.AGUARDANDO_TRANSPORTE);
		listOrdenada.add(StatusSolicitacaoTransporte.APROVADA);
		listOrdenada.add(StatusSolicitacaoTransporte.CANCELADA);
		listOrdenada.add(StatusSolicitacaoTransporte.EM_VIAGEM);
		listOrdenada.add(StatusSolicitacaoTransporte.NAO_REALIZADA);
		listOrdenada.add(StatusSolicitacaoTransporte.RASCUNHO);
		listOrdenada.add(StatusSolicitacaoTransporte.REALIZADA);
		return listOrdenada;
	}
}