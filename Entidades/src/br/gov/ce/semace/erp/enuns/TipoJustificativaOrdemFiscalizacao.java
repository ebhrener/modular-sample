package br.gov.ce.semace.erp.enuns;

public enum TipoJustificativaOrdemFiscalizacao {

	CANCELAR("Cancelar"),
	MUDAR_STATUS("Mudança de Status");

	private String descricao;

	private TipoJustificativaOrdemFiscalizacao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
