package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

public enum TipoOrdemColeta {

	/*0*/FORTALEZA("Fortaleza", Boolean.TRUE),
	/*1*/RMF("RMF", Boolean.FALSE),
	/*2*/ESTADO("Estado", Boolean.FALSE),
	/*3*/LITORAL_LESTE("Litoral Leste", Boolean.TRUE),
	/*4*/LITORAL_OESTE("Litoral Oeste", Boolean.TRUE);

	private String descricao;

	private Boolean ativo;

	private TipoOrdemColeta(String descricao, boolean ativo) {
		this.descricao = descricao;
		this.ativo = ativo;
	}

	public String getDescricao() {
		return descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public static List<TipoOrdemColeta> listAtivos() {
		List<TipoOrdemColeta> list = new ArrayList<>();
		for (TipoOrdemColeta tipoOrdemColeta : values()) {
			if (tipoOrdemColeta.getAtivo()) {
				list.add(tipoOrdemColeta);
			}
		}
		return list;
	}


}
