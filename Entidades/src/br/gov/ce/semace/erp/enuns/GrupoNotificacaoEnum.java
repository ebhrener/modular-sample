package br.gov.ce.semace.erp.enuns;

public enum GrupoNotificacaoEnum {

	CONTROL("Controle"),
	TREATMENT("Tratamento");

	private String descricao;

	private GrupoNotificacaoEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
