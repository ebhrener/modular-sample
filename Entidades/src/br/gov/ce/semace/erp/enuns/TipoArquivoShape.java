package br.gov.ce.semace.erp.enuns;

public enum TipoArquivoShape {

	PRJ(".prj"),SHP(".shp"),SHX(".shx"),DBF(".dbf"),KML(".kml"),KMZ(".kmz");

	private String extensao;

	private TipoArquivoShape(String extensao) {
		this.extensao = extensao;
	}

	public static TipoArquivoShape getTipoArquivoShapeByExtensao(String extensao){
		for(TipoArquivoShape t: TipoArquivoShape.values()){
			if(t.getExtensao().endsWith(extensao)){
				return t;
			}
		}
		return null;
	}

	public String getExtensao() {
		return extensao;
	}

	public boolean isPRJ() {
		return PRJ.name().equals(this.name());
	}

	public boolean isSHP() {
		return SHP.name().equals(this.name());
	}

	public boolean isSHX() {
		return SHX.name().equals(this.name());
	}

	public boolean isDBF() {
		return DBF.name().equals(this.name());
	}

	public boolean isKML() {
		return KML.name().equals(this.name());
	}

	public boolean isKMZ() {
		return KMZ.name().equals(this.name());
	}

	public static boolean isPRJ(String extensaoParam) {
		return PRJ.getExtensao().endsWith(extensaoParam);
	}

	public static boolean isSHP(String extensaoParam) {
		return SHP.getExtensao().endsWith(extensaoParam);
	}

	public static boolean isSHX(String extensaoParam) {
		return SHX.getExtensao().endsWith(extensaoParam);
	}

	public static boolean isDBF(String extensaoParam) {
		return DBF.getExtensao().endsWith(extensaoParam);
	}

	public static boolean isKML(String extensaoParam) {
		return KML.getExtensao().endsWith(extensaoParam);
	}

	public static boolean isKMZ(String extensaoParam) {
		return KMZ.getExtensao().endsWith(extensaoParam);
	}

}
