package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.gov.ce.semace.erp.entity.geral.Documento;

/**
 * {@link Enum} referente ao Tipo de {@link Documento}
 *
 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 14:20:23
 */
public enum TipoDocumento {
	/*0*/TERMO("Termo","TRM","termo.jasper", true, false, true, "Processo de Termo", true, OrigemSolicitacaoProcesso.TERMO),
	/*1*/NOTIFICACAO("Notificação","NTF","notificacao.jasper", true, false, false, null, true, OrigemSolicitacaoProcesso.NOTIFICACAO),
	/*2*/AUTOINFRACAO("Auto de Infração","AIF","autoInfracao.jasper", true, false, true, "Processo de Auto de Infração", true, OrigemSolicitacaoProcesso.AUTO_INFRACAO),
	/*3*/OCORRENCIA("Ocorrência","DEN","ocorrencia.jasper", false, false, false, null, true, null),
	/*4*/OCORRENCIA_OF("Ocorrência","DEN","ocorrencia_of.jasper", false, false, false, null, true, null),
	/*5*/AUTOINFRACAO_PREENCHIDO("Auto de Infração","AIF","auto_Infracao_preenchido.jasper", false, false, false, null, true, null),
	/*6*/NOTIFICACAO_PREENCHIDO("Notificação","NTF","notificacao_preenchida.jasper", false, false, false, null, true, null),
	/*7*/OF("Ordem de Fiscalização","OF", "ordem_fiscalizacao.jasper",false, false, false, null, true, null),
	/*8*/TERMO_PREENCHIDO("Termo","TRM","termo_preenchido.jasper", false, false, false, null, true, null),
	/*9*/TERMO_COMPROMISSO_COMPENSACAO_AMBIENTAL("Termo de Compromisso de Compensação Ambiental","TCCA", null, false, true, false, null, true, null),
	/*10*/RESPOSTAS_OF("Respostas na Ordem de Fiscalização","RESP_OF","resposta_ordem_fiscalizacao.jasper", false, false, false, null, true, null),
	/*11*/TERMO_PARCELAMENTO_AUTO_INFRACAO("Termo de Parcelamento de Débito de Auto de Infração","TPDAIF",null, false, true, false, null, false, null),
	/*12*/ACORDO_EXTRAJUDICIAL("Termo de Compromisso de Parcelamento e Confissão de Dívida.", "TCPCD", null, false, true, false, null, false, null),
	/*13*/ORDEM_FISCALIZACAO_COMPLETA("Ordem de Fiscalização Completa", "OF", "ordem_fiscalizacao_ocorrencias.jasper", false, false, false, null, true, null),
	/*14*/CADASTRO_CONSUMIDOR_MATERIA_PRIMA("Cadastro do Consumidor de Materia Prima","CCMPOF", "cadastro_consumidor_materia_prima.jasper", false, false, false, null, false, OrigemSolicitacaoProcesso.REQUERIMENTO),
	/*15*/OCORRENCIA_COMPLETA("Ocorrência com todos os documentos gerados","DEN_COMPLETA", "ocorrencia_preenchida.jasper", false, false, false, null, true, null),
	/*16*/DOC_ANEXO("Doc Anexo", "DAN",null,false,false, false, null, false, OrigemSolicitacaoProcesso.DOC_ANEXO),
	/*17*/PARECER("Parecer","PRC",null, false,false, false, null, false, null),
	/*18*/JULGAMENTO("Julgamento", "JULG", null, false, false, false, null, false, null),
	/*19*/DECISAO("Decisao", "DCS", null, true, true, false, null,false, null),
	/*20*/PRODUTIVIDADE("Produtividade", "PRODUTIVIDADE", "produtividade.jasper", false, false, false, null, false, null),
	/*21*/DOC_AVULSO("Doc Avulso", "DAV", null, false, false, true, "Administrativo - Doc Avulso", false, OrigemSolicitacaoProcesso.DOC_AVULSO),
	/*22*/CI("Comunicação Interna", "CI", "comunicado_interno.jasper", false, false, true, "Administrativo - CI", false, null),
	/*23*/PROCESSO("Processo","PROC", null, false, false, false, "Processo",false , null),
	/*24*/OFICIO("Ofício","OFI", "oficio.jasper", false, false, true, "Administrativo - Ofício",false, OrigemSolicitacaoProcesso.OFICIO),
	/*25*/RAIA("Raia","RAIA","impressao_raia.jasper", false,false, false, null, false, null),
	/*26*/AR("Aviso de Recebimento","AR", null, false, false, true, "Protocolo - AR",false, null),
	/*27*/TERMO_PROVISORIO("Termo Provisório","TRMP", null, false, false, true, "Processo de Termo Provisório",false, OrigemSolicitacaoProcesso.TERMO),
	/*28*/REQUERIMENTO("Requerimento","REQ", null, false, false, true, "Processo de Requerimento",false, OrigemSolicitacaoProcesso.REQUERIMENTO),
	/*29*/REQUERIMENTO_ANALISE("Requerimento de Análise","REQ", null, false, false, true, "Processo de Requerimento de Análise",false, OrigemSolicitacaoProcesso.REQUERIMENTO),
	/*30*/CONTRATO("Contrato","CRT", null, false, false, true, "Processo de Aquisição",false, OrigemSolicitacaoProcesso.CI),
	/*31*/CONTRATO_ADITIVO("Aditivo","ADT", null, false, false, true, "Processo de Aquisição",false, OrigemSolicitacaoProcesso.CI),
	/*32*/CONTRATO_APOSTILAMENTO("Apostilamento","APT", null, false, false, true, "Processo de Aquisição",false, OrigemSolicitacaoProcesso.CI),
	/*33*/ORDEM_COLETA("Ordem de Coleta","OCL","RelatorioOrdemColetaCheckList.jasper", false, false, false, null, true, null),
	/*34*/ANALISE_COLETA("Analise","ACL","RelatorioAnalise.jasper", false, false, false, null, true, null),
	/*35*/BOLETIM("Boletim","BOL","RelatorioBoletim.jasper", false, false, false, null, true, null),
	/*36*/RENOVAÇÃO_CREDITO_ORÇAMENTÁRIO("Aditivo com Renovação de Créditos Orçamentários","RCO", null, false, false, true, "Processo de Aquisição",false, OrigemSolicitacaoProcesso.CI),
	/*37*/DIVIDA_ATIVA("Dívida Ativa","DA", null, false, false, true, "Dívida Ativa",false, OrigemSolicitacaoProcesso.AUTO_INFRACAO),
	/*38*/SOLICITACAO_VIAGEM("Solicitação de Viagem","SV", null, false, false, false, null, true, null),
	/*39*/ORDEM_TRANSPORTE("Ordem de Transporte","OT", null, false, false, false, null, true, null),
	/*40*/RELATORIO_GERAL_OF("Relatório Geral OF","RGOF", null, false, false, true, "Relatório Geral",false, null),
	/*41*/ARQUIVO_PROCESSO("Arquivos Iniciais do Processo","APROC", null, false, false, false, null,false, null),
	/*42*/DESPACHO("Despacho do Processo","desp", null, false, false, false, null,false, null),
	/*43*/ARQUIVO_DOC_ANEXO("Arquivos DocAnexo","adan", null, false, false, false, null,false, null),
	/*44*/DESPACHO_SANEADOR("Despacho Saneador","DSPSAN", null, false, false, false, null,false, null),
	/*45*/ARQUIVO_DOC_AVULSO("Arquivos DocAvulso","adav", null, false, false, false, null,false, null),
	/*46*/ARQUIVO_AVISO_RECEBIMENTO("Arquivos Aviso Recebimento","aar", null, false, false, false, null,false, null),
	/*47*/DAE("DAE","DAE", null, false, false, false, null,false, null),
	/*48*/ARQUIVO_RAIA("Arquivos do Raia","ARAIA", null, false, false, false, null,false, null),
	/*49*/IMAGEM_DOC_QUESTIONARIO("Imagem do Questionário","idocq", null, false, false, false, null,false, null),
	/*50*/RELATORIO_VIAGEM_TERCEIRIZADO("Relatório de Viagem de Funcionários","RV",null, false, false, false, null, true, null),
	/*51*/TCFA("Taxa de Controle e Fiscalização Ambiental","TCFA", null, false, false, false, null, false, null),
	/*52*/CADASTRO_CONSUMIDOR_FLORESTAL("Cadastro de Consumidor Florestal","CCF", null, false, false, false, null, false, null),
	/*53*/RAMA("Rama","RAMA","impressao_rama.jasper", false,false, false, null, false, null),
	/*54*/TIDA("Termo de Inscrição em Dívida Ativa","TIDA",null, false,false, false, null, false, null),
	/*55*/TRIDA("Termo de Retificação de Inscrição em Dívida Ativa","TRIDA",null, false,false, false, null, false, null),
	/*56*/EXECUCAO_FISCAL("Execução Fiscal de Dívida Ativa não Tributária - Petição Inicial","EFDA",null, false,false, false, null, false, null),
	/*57*/CDA("Certidão de Dívida Ativa","CDA",null, false,false, false, null, false, null),
	/*58*/RELATORIO_ANALISE_RAMA("Relatório de Análise de RAMA","RAR",null, false,false, false, null, false, null),
	/*59*/TCPDADM("Termo de Compromisso de Parcelamento de Débito Administrativo","TCPDADM",null, false,false, false, null, false, null),
	/*60*/CAPA_PROCESSO("Capa do Processo","CAPA", null, false, false, true, null, false, null),
	/*61*/COMUNICACAO_CRIME("Comunicação de Crime", "CCR", null, false, false, false, null, false, null),
	/*62*/ARQUIVO_COMUNICACAO_CRIME("Arquivos da Comunicação de Crime","ACCR", null, false, false, false, null,false, null),
	/*63*/DECLARACAO("Declaração", "DEC", null, false, false, false, null, false, null),
	/*64*/AUTORIZACAO("Autorização", "AUT", null, false, false, false, null, false, null),
	/*65*/PARECER_TECNICO("Parecer Técnico", "PARTEC", null, false, false, false, null, false, null),
	/*66*/RELATORIO_TECNICO("Relatório Técnico", "RELTEC", null, false, false, false, null, false, null),
	/*67*/TERMO_COMPROMISSO("Termo de Compromisso", "TERCOM", null, false, false, false, null, false, null),
	/*68*/TERMO_REFERENCIA("Termo de Referência", "TERREF", null, false, false, false, null, false, null),
	/*69*/NOTIFICACAO_JURIDICA("Notificação Jurídica", "NOTJUR", null, false, false, false, null, false, null),
	/*70*/NOTICIA_CRIME("Notícia Crime", "NOTCRI", null, false, false, false, null, false, null),
	/*71*/PARECER_JURIDICO("Parecer Jurídico", "PARJUR", null, false, false, false, null, false, null),
	/*72*/TERMO_ACORDO_EXTRAJUDICIAL("Termo de Acordo Extrajudicial", "TERACOEXT", null, false, false, false, null, false, null),
	/*73*/TERMO_ADITIVO_CONTRATO("Termo Aditivo ao Contrato", "TERADCONT", null, false, false, false, null, false, null),
	/*74*/TERMO_ADITIVO_CONVENIO("Termo Aditivo ao Convênio", "TERADCONV", null, false, false, false, null, false, null),
	/*75*/TERMO_AJUSTAMENTO_CONDUTA_TAC("Termo de Ajustamento de Conduta - TAC", "TERAJUCON", null, false, false, false, null, false, null),
	/*76*/TERMO_AUDIENCIA("Termo de Audiência", "TERAUD", null, false, false, false, null, false, null),
	/*77*/TERMO_CESSAO_USO("Termo de Cessão de Uso", "TERCESUSO", null, false, false, false, null, false, null),
	/*78*/TERMO_COOPERACAO_TECNICA("Termo de Cooperação Técnica", "TERCOOPTEC", null, false, false, false, null, false, null),
	/*79*/TERMO_PARCELAMENTO_DIVIDA_ATIVA_NAO_TRIBUTARIA_NAO_EXECUTADA("Termo de Parcelamento de Dívida Ativa Não-Tributária (Não Executada)", "TPDANT", null, false, false, false, null, false, null),
	/*80*/TERMO_RETIFICACAO("Termo de Retificação", "TERRET", null, false, false, false, null, false, null),
	/*81*/ANALISE_JURIDICA_LICENCIAMENTOS_E_AUTORIZACOES_AMBIENTAIS("Análise Jurídica dos Licenciamentos e Autorizações Ambientais", "AJLAAMB", null, false, false, false, null, false, null),
	/*82*/CERTIDAO_NEGATIVA_DEBITO_AMBIENTAL("Certidão Negativa de Débito Ambiental", "CNDAMB", null, false, false, false, null, false, null),
	/*83*/TERMO_DOACAO("Termo de Doação", "TERDOA", null, false, false, false, null, false, null),
	/*84*/CERTIDAO_POSITIVA_COM_EFEITOS_NEGATIVA("Certidão Positiva com Efeitos de Negativa", "CPEN", null, false, false, false, null, false, null),
	/*85*/TERMO_PARCERIA("Termo de Parceria", "TERPAR", null, false, false, false, null, false, null),
	/*86*/TERMO_AUTORIZACAO_USO("Termo de Autorização de Uso", "TERAUTUSO", null, false, false, false, null, false, null),
	/*87*/TERMO_AJUSTAMENTO_CONDUTA_PARA_AVERBACAO("Termo de Ajustamento de Conduta para Averbação", "TACAVER", null, false, false, false, null, false, null),
	/*88*/TERMO_AVERBACAO("Termo de Averbação", "TERAVER", null, false, false, false, null, false, null),
	/*89*/PECAS_PROJUR("Peças da Projur", "PECPROJ", null, false, false, false, null, false, null),
	/*90*/RELATORIO_JURIDICO("Relatório Jurídico", "RELJUR", null, false, false, false, null, false, null),
	/*91*/CERTIDAO_QUITACAO_COMPENSACOES_AMBIENTAIS("Certidão de Quitação das Compensações Ambientais", "CQCAMB", null, false, false, false, null, false, null),
	/*92*/AUTO_INFRACAO_BLITZ("Auto de Infração de Blitz", "AIFBLITZ", null, false, false, false, null, false, null),
	/*93*/PARECER_INSTRUTORIO("Parecer Instrutório", "PARINST", null, false, false, false, null, false, null),
	/*94*/NOTIFICACAO_PROJU("Notificação Proju", "NOTPROJ", null, false, false, false, null, false, null),
	/*95*/VISTORIA("Vistoria", "VIST", null, false, false, false, null, false, null),
	/*96*/JULGAMENTO_ADMINISTRATIVO_PRIMEIRA_INSTANCIA("Julgamento Administrativo de 1ª instância", "JAPINST", null, false, false, false, null, false, null),
	/*97*/MAPA("Mapa", "MAP", null, false, false, false, null, false, null),
	/*98*/TERMO_EMBARGO("Termo de Embargo", "TEREMB", null, false, false, false, null, false, null),
	/*99*/TERMO_APREENSAO("Termo de Apreensão", "TERAPR", null, false, false, false, null, false, null),
	/*100*/DECLARACAO_TRAMITACAO_PROCESSUAL("Declaração de Tramitação Processual", "DECTRAPRO", null, false, false, false, null, false, null),
	/*101*/MANIFESTACAO_TECNICA_EMBARGO_ADMINISTRATIVO("Manifestação Técnica de Embargo Administrativo", "MTEADM", null, false, false, false, null, false, null),
	/*102*/PARECER_TECNICO_RECURSAL("Parecer Técnico Recursal", "PARTECREC", null, false, false, false, null, false, null),
	/*103*/CONTRADITA("Contradita", "CONT", null, false, false, false, null, false, null),
	/*104*/JULGAMENTO_AUTO_INFRACAO_SEGUNDA_INSTANCIA("Julgamento de auto de infração de 2° Instância", "JAIFSINST", null, false, false, false, null, false, null),
	/*105*/TRANSFERENCIA_BENS_APREENDIDOS("Transferência de Bens Apreendidos", "TRABENAPR", null, false, false, false, null, false, null),
	/*106*/DESPACHO_INSTRUCAO("Despacho de  Instrução", "DESINST", null, false, false, false, null, false, null),
	/*107*/DESPACHO_JURIDICO("Despacho Jurídico", "DESJUR", null, false, false, false, null, false, null),
	/*108*/NOTIFICACAO_COMPENSACAO("Notificação Compensação", "NOTCOMP", null, false, false, false, null, false, null),
	/*109*/ATA_SESSAO_CAMARA_RECURSAL("Ata da Sessão Câmara Recursal", "ASCR", null, false, false, false, null, false, null),
	/*110*/TERMO_REFERENCIA_ESPECIAL_PARA_REPARACAO_DANOS_AMBIENTAIS("Termo de Referência Especial para Reparação de Danos Ambientais", "TRERDAMB", null, false, false, false, null, false, null),
	/*111*/TERMO_APOSTILAMENTO("Termo de Apostilamento", "TERAPOST", null, false, false, false, null, false, null),
	/*112*/JUSTIFICATIVA_TECNICA("Justificativa Técnica", "JUSTEC", null, false, false, false, null, false, null),
	/*113*/LICENCA("Licença", "LIC", null, false, false, false, null, false, null),
	/*114*/TERMO_COMPROMISSO_RECUPERACAO_AREA_DEGRADADA("Termo de Compromisso de Recuperação de Área Degradada", "TCRAD", null, false, false, false, null, false, null),
	/*115*/CERTIFICADO("Certificado", "CERT", null, false, false, false, null, false, null),
	/*116*/DECLARACAO_ISENCAO("Declaração de isenção", "DECISEN", null, false, false, false, null, false, null),
	/*117*/AUXILIAR("Documento Auxiliar", "AUX", null, true, false, false, null, false, null),
	/*118*/TABELA("Tabela", null, null, true, false, false, null, false, null),
	/*119*/CERTIFICADO_CADASTRO_TECNICO_ESTADUAL("Certificado de Cadastro Técnico Estadual", "CCTE", null, true, false, false, null, false, null),
	/*120*/DOCUMENTO_RETIFICACAO("Documento de Retificação", "DOCRET", null, true, false, false, null, false, null),
	/*121*/DOCUMENTO_COMPLEMENTAR("Documento Complementar", "DOCCOMP", null, true, false, false, null, false, null),
	/*122*/DOCUMENTO_DESENTRANHAMENTO("Documento de Desentranhamento", "DOCDES", null, true, false, false, null, false, null),
	/*123*/LAUDO_LABORATORIAL("Laudo Laboratorial", "LAULAB", null, true, false, false, null, false, null),
	;


	private final String descricao;
	private final String index;
	private final String arquivoJasper;
	private Boolean visible;
	private Boolean hasTemplate;
	private Boolean isProcesso;
	private String nomeTipoProcesso;
	private Boolean isDocumentoFiscal;
	private OrigemSolicitacaoProcesso origemSolicitacaoProcesso;

	private TipoDocumento(String descricao, String index, String arquivoJasper, Boolean visible, Boolean hasTemplate, Boolean isProcesso, String nomeTipoProcesso, Boolean isDocumentoFiscal, OrigemSolicitacaoProcesso origemSolicitacao) {
		this.descricao = descricao;
		this.index = index;
		this.arquivoJasper = arquivoJasper;
		this.visible = visible;
		this.hasTemplate = hasTemplate;
		this.isProcesso = isProcesso;
		this.nomeTipoProcesso = nomeTipoProcesso;
		this.isDocumentoFiscal = isDocumentoFiscal;
		this.origemSolicitacaoProcesso = origemSolicitacao;
	}

	public String getTipoDocumento() {
		return this.index;
	}

	public String getArquivoJasper() {
		return arquivoJasper;
	}

	public String getDescricao() {
		return descricao;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	public Boolean getHasTemplate() {
		return hasTemplate;
	}
	public void setHasTemplate(Boolean hasTemplate) {
		this.hasTemplate = hasTemplate;
	}
	public String getIndex() {
		return index;
	}
	public Boolean getIsProcesso() {
		return isProcesso;
	}

	public String getNomeTipoProcesso() {
		return nomeTipoProcesso;
	}

	public void setNomeTipoProcesso(String nomeTipoProcesso) {
		this.nomeTipoProcesso = nomeTipoProcesso;
	}

	public Boolean getIsDocumentoFiscal() {
		return isDocumentoFiscal;
	}

	public void setIsDocumentoFiscal(Boolean isDocumentoFiscal) {
		this.isDocumentoFiscal = isDocumentoFiscal;
	}

	public void setIsProcesso(Boolean isProcesso) {
		this.isProcesso = isProcesso;
	}

	public OrigemSolicitacaoProcesso getOrigemSolicitacaoProcesso() {
		return origemSolicitacaoProcesso;
	}

	public void setOrigemSolicitacaoProcesso(
			OrigemSolicitacaoProcesso origemSolicitacaoProcesso) {
		this.origemSolicitacaoProcesso = origemSolicitacaoProcesso;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PROCESSO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 14:10:14
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PROCESSO}, caso contrário retorna <code>false</code>
     */
	public boolean isTipoProcesso() {

		// verifica se o name é igual ao do tipo PROCESSO
		if (name().equals(PROCESSO.name())) {
			return true;
		}

		return false;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #OFICIO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 14:11:29
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #OFICIO}, caso contrário retorna <code>false</code>
     */
	public boolean isTipoOficio() {

		// verifica se o name é igual ao do tipo OFICIO
		if (name().equals(OFICIO.name())) {
			return true;
		}

		return false;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #DOC_AVULSO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 14:13:46
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #DOC_AVULSO}, caso contrário retorna <code>false</code>
     */
	public boolean isTipoDocAvulso() {

		// verifica se o name é igual ao do tipo DOC_AVULSO
		if (name().equals(DOC_AVULSO.name())) {
			return true;
		}

		return false;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #DOC_ANEXO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 14:14:17
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #DOC_ANEXO}, caso contrário retorna <code>false</code>
     */
	public boolean isTipoDocAnexo() {

		// verifica se o name é igual ao do tipo DOC_ANEXO
		if (name().equals(DOC_ANEXO.name())) {
			return true;
		}

		return false;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CI}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 14:15:07
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CI}, caso contrário retorna <code>false</code>
     */
	public boolean isTipoCI() {

		// verifica se o name é igual ao do tipo CI
		if (name().equals(CI.name())) {
			return true;
		}

		return false;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AUTOINFRACAO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28/10/2015 16:28:02
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AUTOINFRACAO}, caso contrário retorna <code>false</code>
     */
	public boolean isTipoAutoInfracao() {

		if (name().equals(AUTOINFRACAO.name())) {
			return true;
		}

		return false;
	}

	/**
	 * {@link List}a contendo os {@link TipoDocumento} que podem gerar {@link Documento}s, na fiscalização
	 *
	 *  {@link TipoDocumento} que fazem parte:
	 *
	 * <li>{@link #AUTOINFRACAO}</li>
	 * <li>{@link #NOTIFICACAO}</li>
	 * <li>{@link #TERMO}</li>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/04/2016 16:56:49
	 *
	 * @return
	 * 	{@link List}a com os {@link TipoDocumento} configurados.
	 */
	public static List<TipoDocumento> getListTipoDocumentoGeradorFiscalizacao(){

		List<TipoDocumento> listTipoDocumento = new ArrayList<>();

		listTipoDocumento.add(AUTOINFRACAO);
		listTipoDocumento.add(NOTIFICACAO);
		listTipoDocumento.add(TERMO);

		return listTipoDocumento;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #NOTIFICACAO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 17/05/2016 14:52:42
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #NOTIFICACAO}, caso contrário retorna <code>false</code>
     */
	public boolean isTipoNotificacao() {

		if (name().equals(NOTIFICACAO.name())) {
			return true;
		}

		return false;
	}

	public boolean isTipoTermo() {

		if (name().equals(TERMO.name())) {
			return true;
		}

		return false;
	}

	public boolean isTipoCapaProcesso() {
		return CAPA_PROCESSO.equals(this);
	}

	public Integer getOrdinal() {
		return this.ordinal();
	}

	public static TipoDocumento fromInt(int valor) {

		for (TipoDocumento tipoDocumento : TipoDocumento.values()) {
			if (tipoDocumento.ordinal() == valor){
				return tipoDocumento;
			}
		}

		return null;
	}

	/**
	 * Método que informa se é permitido incluir o QRCode para o {@link TipoDocumento} passado por parâmetro
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 28/03/2018 11:25:08
	 *
	 * @param tipoDocumento
	 * @return
	 */
	@Deprecated
	public static boolean permiteCriarQRCOde(TipoDocumento tipoDocumento) {
		List<TipoDocumento> listBloqueados = Arrays.asList(TipoDocumento.CAPA_PROCESSO, TipoDocumento.ARQUIVO_PROCESSO, TipoDocumento.DAE,
				TipoDocumento.ARQUIVO_DOC_ANEXO, TipoDocumento.AUXILIAR);

		return !listBloqueados.contains(tipoDocumento);
	}

	/**
	 * Método que informa se é permitido incluir o QRCode para o {@link TipoDocumento} obtido do idTipoDocumento passado por parâmetro
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 28/03/2018 11:29:42
	 *
	 * @param idTipoDocumento
	 * @return
	 */
	@Deprecated
	public static boolean permiteCriarQRCOde(int idTipoDocumento) {
		return permiteCriarQRCOde(TipoDocumento.values()[idTipoDocumento]);
	}

	public boolean isTipoRaia() {
		if (name().equals(RAIA.name())) {
			return true;
		}

		return false;
	}

	public boolean isTipoComunicacaoCrime() {
		if (name().equals(COMUNICACAO_CRIME.name())) {
			return true;
		}

		return false;
	}

}