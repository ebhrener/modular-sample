package br.gov.ce.semace.erp.enuns;

public class ConstantesMovimetacao {

	public final static String DISPONIBILIZA_ENVIO = "Disponibilizou para envio";
	public final static String SISTEMA = "Sistema";
	public final static String INTERESSADO = "(Interessado)";
	public final static String BLOQUEAR_FALTA_ENVIO = "Bloqueou por falta de envio no período correto.";
	public final static String BLOQUEAR_FALTA_PAGAMENTO = "Bloqueou por falta de pagamento no período correto.";
	public final static String IDENTIFICACAO_PAGAMENTO = "Identificou pagamento";
	public final static String RESPONDER = "Respondeu RAMA.";
	public final static String SALVAR = "Salvou RAMA.";
	public final static String FINALIZAR = "Finalizou RAMA.";
	public final static String RETIFICAR = "Retificou RAMA.";
	public final static String DESVINCULAR = "Desvinculou RAMA.";
	public final static String PROTOCOLAR_VINCULO = "Protocolou RAMA com vínculo.";
	public final static String DISPENSAR = "Dispensou RAMA.";
	public final static String CONCLUIR_ANALISE = "Concluiu análise do RAMA";
	public final static String ANALISE_RAMA = "Analisando RAMA";
	public final static String ENVIAR_APROVACAO = "Enviar Rama Para Aprovação Gerencial.";
	public final static String CADASTRO_PENDENCIA = "Adicionou pendencia ao RAMA: ";
	public final static String REMOCAO_PENDENCIA = "Remoção da pendencia : ";
	public final static String RAMA_PENDENTE = "RAMA colocado como PENDENTE";
	public final static String RELATORIO_SALVO = "Salvou relatório do RAMA.";
	public final static String REMANEJADO = "RAMA remanejado para ";
	public final static String PROTOCOLAR = "Protocolou RAMA. ";
	public final static String INICIO_GERACAO_DAE = "Iniciou a etapa de geração do DAE ";
	public final static String FIM_GERACAO_DAE = "Finalizou a etapa de geração do DAE ";
	public final static String INICIO_VALIDACAO_ISENCAO = "Iniciou a etapa de validação de isenção do RAMA ";
	public final static String ENVIAR_ATENDENTE = "Enviou RAMA para atendente fazer o cálculo ";
	public final static String ENVIAR_GERENTE = "Enviou RAMA para gerente validar isenção ";
	public final static String FINALIZAR_VALIDACAO_ISENCAO = "Finalizou validação de isenção do RAMA ";
	public final static String GERAR_DAE = "Gerou DAE do RAMA ";
	public final static String PROTOCOLAR_RAMA = "Protocolou RAMA ";
	public final static String SOLICITAR_FIC = "Solicitou o envio da FIC ";

}
