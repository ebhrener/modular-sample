package br.gov.ce.semace.erp.enuns;

public enum StatusRegistro {
	
	ATIVO,
	INATIVO,
	BLOQUEADO;

}
