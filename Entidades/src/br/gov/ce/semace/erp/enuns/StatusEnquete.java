package br.gov.ce.semace.erp.enuns;


public enum StatusEnquete {

	AGUARDANDO_INICIO("Aguardando Início"), // 0
	EM_ANDAMENTO("Em Andamento"), // 1
	ENCERRADO("Encerrado"), // 2
	CANCELADO("Cancelado"); // 3

	private String descricao;

	private StatusEnquete(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public String toString() {
		return descricao;
	}

	public static StatusEnquete fromInt(int valor) {

		for (StatusEnquete statusEnquete : StatusEnquete.values()) {
			if (statusEnquete.ordinal() == valor){
				return statusEnquete;
			}
		}

		return null;
	}

	public boolean isAguardandoInicio() {

        if (name().equals(AGUARDANDO_INICIO.name())){
            return true;
        }

        return false;
    }

	public boolean isEmAndamento() {

        if (name().equals(EM_ANDAMENTO.name())){
            return true;
        }

        return false;
    }

	public boolean isEncerrada() {

        if (name().equals(ENCERRADO.name())){
            return true;
        }

        return false;
    }

	public boolean isCancelada() {

        if (name().equals(CANCELADO.name())){
            return true;
        }

        return false;
    }
}