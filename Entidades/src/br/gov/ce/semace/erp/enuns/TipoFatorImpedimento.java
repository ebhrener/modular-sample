package br.gov.ce.semace.erp.enuns;

public enum TipoFatorImpedimento {

	ORDEM_COLETA("Ordem de Coleta"),
	ANALISE("Análise");
	
	private String descricao;

	private TipoFatorImpedimento(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
