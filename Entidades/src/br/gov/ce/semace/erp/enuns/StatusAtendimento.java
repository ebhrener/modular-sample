package br.gov.ce.semace.erp.enuns;

import static java.util.Arrays.asList;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.helpers.CollectionsHelper;
import br.gov.ce.semace.erp.helpers.CollectionsHelper.Predicate;

public enum StatusAtendimento {

	/*0*/ @Deprecated TODOS("Todos", false),
	/*1*/ AGENDADO("Agendado", false),
	/*2*/ EM_ATENDIMENTO("Em Atendimento", true),
	/*3*/ VALIDACAO_DOCUMENTO_SHAPE("Validação Documentos/Shape", true),
	/*4*/ @Deprecated GERAR_PROCESSO_PROVISORIO("Geração Processo Provisório", true),
	/*5*/ ATENDIDO("Atendido", false),
	/*6*/ CANCELADO("Cancelado", false),
	/*7*/ EXCLUIDO("Excluído", false),
	/*8*/ GERAR_DAE("Geração de DAE", true),
	/*9*/ ATENDIMENTO_COM_PENDENCIA("Documentação Pendente", true),
	/*10*/ @Deprecated ATENDIMENTO_INICIADO("Atendimento Iniciado", true),
	/*11*/ AGUARDANDO_SHAPE("Aguardando Validação do Shape", true),
	/*12*/ AGUARDANDO_VALIDACAO_DOCUMENTO("Aguardando Validação dos Documentos", true),
	/*13*/ TRIAGEM("Triagem",true),
	/*14*/ AGUARDANDO_PAGAMENTO("Aguardando Pagamento",true),
	/*15*/ PRE_REQUERIMENTO("Pré-requerimento", true),
	/*16*/ AGUARDANDO_GERACAO_PROCESSO("Aguardando Geração de Processo", true)

	;

	private String descricao;
	private boolean visible;


	public static List<StatusAtendimento> listStatusAtendimentoFiltro(){
		List<StatusAtendimento> listStatusAtendimento = new ArrayList<StatusAtendimento>();
		listStatusAtendimento.add(StatusAtendimento.AGENDADO);
		listStatusAtendimento.add(StatusAtendimento.EM_ATENDIMENTO);
		listStatusAtendimento.add(StatusAtendimento.VALIDACAO_DOCUMENTO_SHAPE);
		listStatusAtendimento.add(StatusAtendimento.ATENDIDO);
		listStatusAtendimento.add(StatusAtendimento.GERAR_DAE);
		listStatusAtendimento.add(StatusAtendimento.ATENDIMENTO_COM_PENDENCIA);
		listStatusAtendimento.add(StatusAtendimento.AGUARDANDO_SHAPE);
		listStatusAtendimento.add(StatusAtendimento.AGUARDANDO_VALIDACAO_DOCUMENTO);
		listStatusAtendimento.add(StatusAtendimento.TRIAGEM);
		listStatusAtendimento.add(StatusAtendimento.AGUARDANDO_PAGAMENTO);
		listStatusAtendimento.add(StatusAtendimento.AGUARDANDO_GERACAO_PROCESSO);
//		listStatusAtendimento.add(StatusAtendimento.PRE_AGENDADO);

		Collections.sort(listStatusAtendimento, new Comparator<StatusAtendimento>() {
			@Override
			public int compare(StatusAtendimento o1, StatusAtendimento o2) {
				return  o1.getDescricao().compareTo(o2.getDescricao());
			}
		});

		return listStatusAtendimento;
	}

	private StatusAtendimento(String descricao, boolean visible) {
		this.descricao = descricao;
		this.visible = visible;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public boolean isVisible() {
		return this.visible;
	}

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #TODOS}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:42:10
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #TODOS}, caso contrário retorna <code>false</code>
     */
    public boolean isTodos() {

        if (this.name().equals(TODOS.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGENDADO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:42:55
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGENDADO}, caso contrário retorna <code>false</code>
     */
    public boolean isAgendado() {

        if (this.name().equals(AGENDADO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EM_ATENDIMENTO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:43:28
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EM_ATENDIMENTO}, caso contrário retorna <code>false</code>
     */
    public boolean isEmAtendimento() {

        if (this.name().equals(EM_ATENDIMENTO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #VALIDACAO_DOCUMENTO_SHAPE}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:44:08
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #VALIDACAO_DOCUMENTO_SHAPE}, caso contrário retorna <code>false</code>
     */
    public boolean isValidacaoDocumentoShape() {

        if (this.name().equals(VALIDACAO_DOCUMENTO_SHAPE.name())
        		|| this.name().equals(AGUARDANDO_VALIDACAO_DOCUMENTO.name())
        		|| this.name().equals(AGUARDANDO_SHAPE.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #GERAR_PROCESSO_PROVISORIO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:44:45
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #GERAR_PROCESSO_PROVISORIO}, caso contrário retorna <code>false</code>
     */
    public boolean isGeraProcessoProvisorio() {

        if (this.name().equals(GERAR_PROCESSO_PROVISORIO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ATENDIDO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:45:34
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ATENDIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isAtendido() {

        if (this.name().equals(ATENDIDO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:45:56
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADO}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelado() {

        if (this.name().equals(CANCELADO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EXCLUIDO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:46:17
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EXCLUIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isExcluido() {

        if (this.name().equals(EXCLUIDO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #GERAR_DAE}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:46:55
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #GERAR_DAE}, caso contrário retorna <code>false</code>
     */
    public boolean isGerarDae() {

        if (this.name().equals(GERAR_DAE.name())){
            return true;
        }

        return false;
    }


    public boolean isAguardandoPagmentoDae() {

        if (this.name().equals(AGUARDANDO_PAGAMENTO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ATENDIMENTO_INICIADO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:47:59
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ATENDIMENTO_INICIADO}, caso contrário retorna <code>false</code>
     */
    public boolean isAtendimentoIniciado() {

        if (this.name().equals(ATENDIMENTO_INICIADO.name())){
            return true;
        }

        return false;
    }

	public boolean isTriagem() {
		return TRIAGEM.equals(this);
	}

    public boolean isAguardandoShape() {
    	return AGUARDANDO_SHAPE.equals(this);
    }

    public boolean isAguardandoValidacaoDocumento() {
    	return AGUARDANDO_VALIDACAO_DOCUMENTO.equals(this);
    }

    public boolean isAguardandoGeracaoProcesso() {
    	return AGUARDANDO_GERACAO_PROCESSO.equals(this);
    }

    public boolean isDocumentacaoPendente() {
		if (this.name().equals(ATENDIMENTO_COM_PENDENCIA.name())){
            return true;
        }
        return false;
	}

    public boolean isPreAgendado() {
    	return PRE_REQUERIMENTO.equals(this);
    }

    public static List<StatusAtendimento> getListStatusLiberarHora(){
		List<StatusAtendimento> status = new ArrayList<>();
		status.add(StatusAtendimento.ATENDIDO);
		status.add(StatusAtendimento.CANCELADO);
		status.add(StatusAtendimento.EXCLUIDO);
		return status;
	}

	public static List<Integer> getListStatusLiberarHoraAsInteger() {
		return Arrays.asList(new Integer[] {
				StatusAtendimento.ATENDIDO.ordinal(),
				StatusAtendimento.CANCELADO.ordinal(),
				StatusAtendimento.EXCLUIDO.ordinal() });
	}

	public static StatusAtendimento get(int ordinal){
		return values()[ordinal];
	}

	/**
	 * Retorna a lista de {@link StatusAtendimento} em que a SEMACE está atendendo o requerimento.
	 */
	public static List<StatusAtendimento> getListStatusAtendimentoSemace() {
		return asList(AGENDADO, EM_ATENDIMENTO, VALIDACAO_DOCUMENTO_SHAPE, GERAR_DAE, AGUARDANDO_SHAPE, AGUARDANDO_VALIDACAO_DOCUMENTO, TRIAGEM,
				AGUARDANDO_GERACAO_PROCESSO);
	}

	public static List<StatusAtendimento> getNonDeprecatedValues() {

		return new CollectionsHelper<>(values()).filter(new Predicate<StatusAtendimento>() {

			@Override
			public boolean test(StatusAtendimento status) throws Exception {
				Field field = StatusAtendimento.class.getField(status.name());
				return !field.isAnnotationPresent(Deprecated.class);
			}

		});
	}

	public static List<StatusAtendimento> getNonAvailableForDiscardPolicy(){
		return asList(ATENDIDO, CANCELADO, EXCLUIDO);
	}

	public static List<StatusAtendimento> getListStatusValidacaoDocumentoShape() {
		return asList(VALIDACAO_DOCUMENTO_SHAPE, AGUARDANDO_VALIDACAO_DOCUMENTO, AGUARDANDO_SHAPE);
	}

	public static List<StatusAtendimento> getListNotAvailableForPrioritize() {
		return asList(CANCELADO, EXCLUIDO, ATENDIDO, AGUARDANDO_PAGAMENTO, PRE_REQUERIMENTO, AGUARDANDO_GERACAO_PROCESSO);
	}

	public static List<StatusAtendimento> getListStatusAtrasoRequerimento() {
		return asList(AGENDADO, EM_ATENDIMENTO, TRIAGEM);
	}

	public static StatusAtendimento getStatusByDescricao(String descricao){
		switch (descricao) {
		case "AGENDADO":
			return StatusAtendimento.AGENDADO;
		case "EM_ATENDIMENTO":
			return StatusAtendimento.EM_ATENDIMENTO;
		case "VALIDACAO_DOCUMENTO_SHAPE":
			return StatusAtendimento.VALIDACAO_DOCUMENTO_SHAPE;
		case "ATENDIDO":
			return StatusAtendimento.ATENDIDO;
		case "GERAR_DAE":
			return StatusAtendimento.GERAR_DAE;
		case "ATENDIMENTO_COM_PENDENCIA":
			return StatusAtendimento.ATENDIMENTO_COM_PENDENCIA;
		case "AGUARDANDO_SHAPE":
			return StatusAtendimento.AGUARDANDO_SHAPE;
		case "AGUARDANDO_VALIDACAO_DOCUMENTO":
			return StatusAtendimento.AGUARDANDO_VALIDACAO_DOCUMENTO;
		case "TRIAGEM":
			return StatusAtendimento.TRIAGEM;
		case "AGUARDANDO_PAGAMENTO":
			return StatusAtendimento.AGUARDANDO_PAGAMENTO;
		case "AGUARDANDO_GERACAO_PROCESSO":
			return StatusAtendimento.AGUARDANDO_GERACAO_PROCESSO;
		default:
			return null;
		}
	}

}
