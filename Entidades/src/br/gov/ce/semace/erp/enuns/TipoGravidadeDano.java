package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * {@link Enum} referente aos Tipos de Gravidade do Dano
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #INEXISTENTE} <br>
 * {@link #LEVE} <br>
 * {@link #MEDIO} <br>
 * {@link #GRAVE} <br>
 *
 * @author rodrigo.silva - Rodrigo Silva Oliveira - 06/12/2017 15:56:33
 */
public enum TipoGravidadeDano {

	/**
	 * <b>Descrição: </b>Inexistente<br>
	 * <b>Valor:</b> 0
	 */
	INEXISTENTE("Inexistente"),

	/**
	 * <b>Descrição: </b>Leve<br>
	 * <b>Valor:</b> 1
	 */
	LEVE("Leve"),

	/**
	 * <b>Descrição: </b>Médio<br>
	 * <b>Valor:</b> 2
	 */
	MEDIO("Médio"),

	/**
	 * <b>Descrição: </b>Grave<br>
	 * <b>Valor:</b> 3
	 */
	GRAVE("Grave");


	private final String descricao;

	private TipoGravidadeDano(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #INEXISTENTE}
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #INEXISTENTE}, caso contrário retorna <code>false</code>
	 */
    public boolean isInexistente() {
    	if (name().equals(INEXISTENTE.name())) {
            return true;
    	}

        return false;
    }

    /**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #LEVE}
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #LEVE}, caso contrário retorna <code>false</code>
	 */
    public boolean isLeve() {
    	if (name().equals(LEVE.name())) {
            return true;
    	}

        return false;
    }

    /**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #MEDIO}
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #MEDIO}, caso contrário retorna <code>false</code>
	 */
    public boolean isMedio() {
    	if (name().equals(MEDIO.name())) {
            return true;
    	}

        return false;
    }

    /**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #GRAVE}
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #GRAVE}, caso contrário retorna <code>false</code>
	 */
    public boolean isGrave() {
    	if (name().equals(GRAVE.name())) {
            return true;
    	}

        return false;
    }

	public static List<TipoGravidadeDano> valuesOrderedByDescricao() {
		List<TipoGravidadeDano> list =  Arrays.asList(TipoGravidadeDano.values());
		Collections.sort(list, new Comparator<TipoGravidadeDano>() {

			@Override
			public int compare(TipoGravidadeDano o1, TipoGravidadeDano o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

	/**
     * Retorna a Enum TipoGravidadeDano correspondente do número passado por parâmetro.
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira - 06/12/2017 15:59:28
     */
	public static TipoGravidadeDano fromInt(int valor) {
		for (TipoGravidadeDano tipoGravidadeDano : TipoGravidadeDano.values()) {
			if (tipoGravidadeDano.ordinal() == valor){
				return tipoGravidadeDano;
			}
		}

		return null;
	}
}