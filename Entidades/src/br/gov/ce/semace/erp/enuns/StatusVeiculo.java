package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tiago
 *
 */

public enum StatusVeiculo {

	DISPONIVEL("Disponível"),
	INDISPONIVEL("Indisponível");

	String descricao;

	private StatusVeiculo(String descricao) {
		this.descricao = descricao;
	}


	public String getDescricao() {
		return this.descricao;
	}

	public boolean isIndisponivel(){
		if(this.name().equals(INDISPONIVEL)){
			return true;
		}

		return false;
	}

	public boolean isDisponivel(){
		if(this.name().equals(DISPONIVEL)){
			return true;
		}

		return false;
	}

	public static List<StatusVeiculo> getListStatusVeiculoOrdenada(){
		ArrayList<StatusVeiculo> listOrdenada = new ArrayList<StatusVeiculo>();
		listOrdenada.add(StatusVeiculo.DISPONIVEL);
		listOrdenada.add(StatusVeiculo.INDISPONIVEL);
		return listOrdenada;
	}
}