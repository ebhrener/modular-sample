package br.gov.ce.semace.erp.enuns;

/**
 * 
 * @author tiago
 *
 */

public enum TipoLancamentoViagem {
	
	DIARIA("Diária"),
	HORA_EXTRA("Hora Extra"),
	AJUDA_CUSTO("Ajuda de Custo"),
	ACRESCIMO("Acréscimo");
	
	String descricao;
	
	private TipoLancamentoViagem(String descricao){
		this.descricao = descricao;	
	}
	
	public boolean isAcrescimo(){
		if(this.name().equals(ACRESCIMO)){
			return true;
		}
		
		return false;
	}
	
	public boolean isAjudaCusto(){
		if(this.name().equals(AJUDA_CUSTO)){
			return true;
		}
		
		return false;
	}
	
	public boolean isDiaria(){
		if(this.name().equals(DIARIA)){
			return true;
		}
		
		return false;
	}
	
	public boolean isHoraExtra(){
		if(this.name().equals(HORA_EXTRA)){
			return true;
		}
		
		return false;
	}
}
