package br.gov.ce.semace.erp.enuns;

public enum TipoCriacaoArquivo {

	DIGITAL, DIGITALIZADO;

	public boolean isDigital() {
		return DIGITAL.equals(this);
	}

	public boolean isDigitalizado() {
		return DIGITALIZADO.equals(this);
	}
}
