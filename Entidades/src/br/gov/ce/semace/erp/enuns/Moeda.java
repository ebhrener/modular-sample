package br.gov.ce.semace.erp.enuns;

/**
 * 
 * @author tiago
 *
 */

public enum Moeda {

	REAL("Real"),
	DOLAR("Dolar"),
	EURO("Euro"),
	LIBRA("Libra"),
	IENES("Ienes");
	
	String descricao;
	
	private Moeda(String descricao) {
		this.descricao = descricao;
	}


	public String getDescricao() {
		return this.descricao;
	}
	
	public boolean isIenes(){
		if(this.name().equals(IENES.name())){
			return true;
		}
		
		return false;
	}
	
	public boolean isLibra(){
		if(this.name().equals(LIBRA.name())){
			return true;
		}
		
		return false;
	}
	
	public boolean isEuro(){
		if(this.name().equals(EURO.name())){
			return true;
		}
		
		return false;
	}
	
	public boolean isDolar(){
		if(this.name().equals(DOLAR.name())){
			return true;
		}
		
		return false;
	}
	
	public boolean isReal(){
		if(this.name().equals(REAL.name())){
			return true;
		}
		
		return false;
	}
	
}
