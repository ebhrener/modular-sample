package br.gov.ce.semace.erp.enuns;

public enum TipoLaboratorioSistema {
	
	BALNEABILIDADE("Balneabilidade"),
	HIDROGRAFIA("Hidrografia");
	
	private String descricao;
	
	private TipoLaboratorioSistema(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
