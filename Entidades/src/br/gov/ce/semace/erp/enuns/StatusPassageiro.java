package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tiago #RM #2393
 *
 */

public enum StatusPassageiro {

	COMPARECEU("Compareceu"),
	NAO_COMPARECEU("Não Compareceu"),
	REMOVIDO("Removido"),
	CANCELADO("Cancelado");

	String descricao;

	private StatusPassageiro(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public boolean isCompareceu(){
		if(this.name().equals(COMPARECEU.name())){
			return true;
		}

		return false;
	}

	public boolean isNaoCompareceu(){
		if(this.name().equals(NAO_COMPARECEU.name())){
			return true;
		}

		return false;
	}

	public boolean isRemovido(){
		if(this.name().equals(REMOVIDO.name())){
			return true;
		}

		return false;
	}

	public boolean isCancelado(){
		if(this.name().equals(CANCELADO.name())){
			return true;
		}

		return false;
	}

	public static List<StatusPassageiro> getListStatusPassageiroOrdenada() {
		ArrayList<StatusPassageiro> listOrdenada = new ArrayList<StatusPassageiro>();
		listOrdenada.add(StatusPassageiro.CANCELADO);
		listOrdenada.add(StatusPassageiro.COMPARECEU);
		listOrdenada.add(StatusPassageiro.NAO_COMPARECEU);
		listOrdenada.add(StatusPassageiro.REMOVIDO);
		return listOrdenada;
	}
}