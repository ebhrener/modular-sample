package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

public enum TipoRepeticaoAgendamento {

	SEMANAL("Semanal"),
	MENSAL("Mensal");

	private String descricao;

	private TipoRepeticaoAgendamento(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public static List<TipoRepeticaoAgendamento> listTipoRepeticaoAgendamento(){
		List<TipoRepeticaoAgendamento> listTipoRepeticaoAgendamento = new ArrayList<>();
		listTipoRepeticaoAgendamento.add(SEMANAL);
		listTipoRepeticaoAgendamento.add(MENSAL);
		return listTipoRepeticaoAgendamento;
	}

}
