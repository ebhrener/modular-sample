package br.gov.ce.semace.erp.enuns;

public enum StatusRecebimentoNotificacao {

	DESEJA_RECEBER,
	IGNORA_PARCIAL,
	IGNORA_TUDO;

	public static StatusRecebimentoNotificacao get(int orinal) {
		for (StatusRecebimentoNotificacao statusRecebimentoNotificacao : values()) {
			if (orinal == statusRecebimentoNotificacao.ordinal()) {
				return statusRecebimentoNotificacao;
			}
		}
		return null;
	}

}
