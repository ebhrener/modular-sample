package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

public enum StatusTramitacao {

	AGUARDANDO_RECEBIMENTO("Aguardando Recebimento"),
	RECEBIDO("Recebido"),
	RESGATADO("Resgatado"),
	REDIRECIONADO("Redirecionado"),
	FINALIZADO("Encerrado"),
	CANCELADO("Cancelado"),
	RETORNADO("Retornado"),
	VINCULADO("Vinculado"),
	REVERTIDO("Revertido"),
	DISTRIBUIDO("Aguardando Recebimento")
	;

	private String descricao;

	private StatusTramitacao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public static List<StatusTramitacao> getStatusTramitacaoLiberada(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(RECEBIDO);
		list.add(RESGATADO);
		list.add(RETORNADO);
		list.add(REVERTIDO);

		return list;
	}

	public static List<StatusTramitacao> getStatusReceberTramitacao(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(AGUARDANDO_RECEBIMENTO);
		list.add(REDIRECIONADO);
		list.add(DISTRIBUIDO);
		return list;
	}

	public static List<StatusTramitacao> getStatusResgatarTramitacao(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(AGUARDANDO_RECEBIMENTO);
		list.add(RECEBIDO);
		list.add(REDIRECIONADO);
		list.add(RETORNADO);
		list.add(REVERTIDO);
		list.add(DISTRIBUIDO);
		return list;
	}

	public static List<StatusTramitacao> getStatusRedirecionarTramitacao() {
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(AGUARDANDO_RECEBIMENTO);
		list.add(RECEBIDO);
		list.add(RETORNADO);
		list.add(RESGATADO);
		list.add(REVERTIDO);
		list.add(DISTRIBUIDO);
		return list;
	}

	public static List<StatusTramitacao> getStatusReverterTramitacao() {
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(AGUARDANDO_RECEBIMENTO);
		list.add(DISTRIBUIDO);
		return list;
	}

	public static List<StatusTramitacao> getStatusVisualizarCITramitacao() {
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(RECEBIDO);
		list.add(RESGATADO);
		list.add(FINALIZADO);
		list.add(CANCELADO);
		list.add(RETORNADO);
		list.add(REVERTIDO);
		list.add(REDIRECIONADO);
		return list;
	}

	public static List<StatusTramitacao> getStatusImpressaoCITramitacao(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(RECEBIDO);
		list.add(RESGATADO);
		list.add(REVERTIDO);
		list.add(REDIRECIONADO);
		list.add(RETORNADO);
		return list;
	}

	public static List<StatusTramitacao> getStatusFinalizarCITramitacao(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(RECEBIDO);
		list.add(RESGATADO);
		list.add(REVERTIDO);
		list.add(REDIRECIONADO);
		list.add(RETORNADO);
		return list;
	}

	public static List<StatusTramitacao> getStatusVisualizarDocAvulsoTramitacao(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(RECEBIDO);
		list.add(RESGATADO);
		list.add(REVERTIDO);
		list.add(RETORNADO);
		list.add(REDIRECIONADO);
		list.add(FINALIZADO);
		return list;
	}

	public static List<StatusTramitacao> getStatusFinalizarDocAvulsoTramitacao(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(RECEBIDO);
		list.add(RESGATADO);
		list.add(REVERTIDO);
		list.add(RETORNADO);
		list.add(REDIRECIONADO);
		return list;
	}

	public static List<StatusTramitacao> getStatusVisualizarOficioTramitacao(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(RECEBIDO);
		list.add(RESGATADO);
		list.add(REVERTIDO);
		list.add(RETORNADO);
		list.add(REDIRECIONADO);
		list.add(FINALIZADO);
		list.add(CANCELADO);
		return list;
	}

	public static List<StatusTramitacao> getStatusFinalizarOficioTramitacao(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(RECEBIDO);
		list.add(RESGATADO);
		list.add(REVERTIDO);
		list.add(RETORNADO);
		list.add(REDIRECIONADO);
		return list;
	}

	public static List<StatusTramitacao> getStatusImpressaoOficioTramitacao(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(FINALIZADO);
		return list;
	}

	public static List<StatusTramitacao> getStatusVisualizarProcessoTramitacao(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(RECEBIDO);
		list.add(REVERTIDO);
		list.add(RESGATADO);
		list.add(RETORNADO);
		list.add(REDIRECIONADO);
		list.add(FINALIZADO);
		return list;
	}

	public static List<StatusTramitacao> getStatusVisualizarVinculosTramitacao(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(RECEBIDO);
		list.add(REVERTIDO);
		list.add(RESGATADO);
		list.add(RETORNADO);
		list.add(REDIRECIONADO);
		list.add(FINALIZADO);
		return list;
	}

	public static List<StatusTramitacao> getStatusTramitado(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(AGUARDANDO_RECEBIMENTO);
		list.add(DISTRIBUIDO);
		return list;
	}

	public static List<StatusTramitacao> getStatusArquivar(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(FINALIZADO);
		list.add(DISTRIBUIDO);
		list.add(AGUARDANDO_RECEBIMENTO);
		list.add(CANCELADO);
		return list;
	}

	public static List<StatusTramitacao> getStatusDesarquivar(){
		List<StatusTramitacao> list = new ArrayList<StatusTramitacao>();
		list.add(FINALIZADO);
		return list;
	}
}
