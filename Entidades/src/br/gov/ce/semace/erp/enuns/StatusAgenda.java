package br.gov.ce.semace.erp.enuns;

public enum StatusAgenda {
	
	AGENDADO("Agendado"),
	EM_PROCESSAMENTO("Em Processamento"),
	PROCESSADO("Processado"),
	ERRO("erro");
	
	private final String descricao;
	
	private StatusAgenda(String descricao){
		this.descricao 	= descricao;
	}
	
	public String getDescricao(){
		return descricao;
	}
	
	public static StatusAgenda getEnumByDescricao(String descricao){
		
		for (StatusAgenda setor : values()) {
			if(setor.getDescricao().equals(descricao))
				return setor;
		}
		
		return null;
	}
	
}
