package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.entity.fiscalizacao.Notificacao;

/**
 * {@link Enum} referente aos Status da {@link Notificacao}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #ATIVO} <br>
 * {@link #CANCELADO} <br>
 * {@link #PENDENTE_SINCRONIA} <br>
 *
 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 4 de jul de 2016 11:12:43
 */
public enum StatusNotificacao {

	/**
	 * <b>Descrição: </b>Ativo<br>
	 * <b>Valor:</b> 0
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 4 de jul de 2016 11:12:49
	 */
	ATIVO("Ativo"),

	/**
	 * <b>Descrição: </b>Cancelado<br>
	 * <b>Valor:</b> 1
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 4 de jul de 2016 11:12:54
	 */
	CANCELADO("Cancelado"),

	/**
	 * <b>Descrição: </b>Pendência Sincronia<br>
	 * <b>Valor:</b> 2
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 4 de jul de 2016 11:12:57
	 */
	PENDENTE_SINCRONIA("Pendência Sincronia");

	/**
     * Descrição do {@link Enum}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 4 de jul de 2016 11:13:02
     */
	private String descricao;

	 /**
     * Construtor com o parâmetro descricao.
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 4 de jul de 2016 11:13:06
     *
     * @param descricao
     *            Parâmetro a ser setado no campo <code>{@link #descricao} </code> do {@link Enum}
     */
	private StatusNotificacao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Método get da propriedade Descricao do {@link Enum}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 4 de jul de 2016 11:13:14
	 *
	 * @return Retorna a descricao do {@link Enum}
	 */
	public String getDescricao() {
		return this.descricao;
	}

	/**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #ATIVO}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 4 de jul de 2016 11:13:16
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ATIVO}, caso contrário retorna <code>false</code>
	 */
    public boolean isAtivo() {

    	if (name().equals(ATIVO.name())){
            return true;
    	}

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 4 de jul de 2016 11:13:19
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADO}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelado() {

        if (name().equals(CANCELADO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PENDENTE_SINCRONIA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 4 de jul de 2016 11:13:21
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PENDENTE_SINCRONIA}, caso contrário retorna <code>false</code>
     */
    public boolean isPendenteSincronia() {

        if (name().equals(PENDENTE_SINCRONIA.name())){
            return true;
        }

        return false;
    }

	public static List<StatusNotificacao> valuesOrderedByDescricao(){
		List<StatusNotificacao> list =  Arrays.asList(StatusNotificacao.values());
		Collections.sort(list, new Comparator<StatusNotificacao>() {

			@Override
			public int compare(StatusNotificacao o1, StatusNotificacao o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

}