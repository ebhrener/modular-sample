package br.gov.ce.semace.erp.enuns;

import br.gov.ce.semace.erp.entity.juridico.AcordoDivida;

/**
 * Status do Acordo da Divida {@link AcordoDivida}
 *
 * Composto por: <br>
 * <br>
 * 0 - {@link #CANCELADO} <br>
 * 1 - {@link #ANDAMENTO} <br>
 * 2 - {@link #CONCLUIDO} <br>
 * 3 - {@link #PRE_ACORDO} <br>
 * 4 - {@link #CANCELADO_PRE_ACORDO} <br>
 * 5 - {@link #ACORDO_EM_ATRASO} <br>
 *
 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 04/04/2014
 *
 */
public enum StatusAcordoDivida {

	/**
	 * <b>Descrição: </b>Cancelado<br>
	 * <b>Valor:</b> 0
	 *
	 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 11/07/2014
	 */
	CANCELADO("Cancelado"),
	/**
	 * <b>Descrição: </b>Em Andamento<br>
	 * <b>Valor:</b> 1
	 *
	 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 11/07/2014
	 */
	ANDAMENTO("Em Andamento"),
	/**
	 * <b>Descrição: </b>Concluído<br>
	 * <b>Valor:</b> 2
	 *
	 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 11/07/2014
	 */
	CONCLUIDO("Concluído"),
	/**
	 * <b>Descrição: </b>Pre-Acordo<br>
	 * <b>Valor:</b> 3
	 *
	 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 11/07/2014
	 */
	PRE_ACORDO("Pre-Acordo"),
	/**
	 * <b>Descrição: </b>Cancelado Pre-Acordo<br>
	 * <b>Valor:</b> 4
	 *
	 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 11/07/2014
	 */
	CANCELADO_PRE_ACORDO("Cancelado Pre-Acordo"),
	/**
	 * <b>Descrição: </b>Acordo em atraso<br>
	 * <b>Valor:</b> 5
	 *
	 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 20/03/2017
	 */
	ACORDO_EM_ATRASO("Acordo em atraso");

	private String descricao;

	private StatusAcordoDivida(String descricao) {
		this.descricao = descricao;
	}

	public boolean isCancelado() {
        return this.name().equals(CANCELADO.name());
    }

	public boolean isEmAndamento() {
		return this.name().equals(ANDAMENTO.name());
	}

	public boolean isConcluido() {
		return this.name().equals(CONCLUIDO.name());
	}

	public boolean isPreAcordo() {
		return this.name().equals(PRE_ACORDO.name());
	}

	public boolean isCanceladoPreAcordo() {
		return this.name().equals(CANCELADO_PRE_ACORDO.name());
	}

	public boolean isAcordoEmAtraso() {
		return this.name().equals(ACORDO_EM_ATRASO.name());
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
