package br.gov.ce.semace.erp.enuns;

public enum TipoQuestionario {

	PARECER_INSTRUTORIO("Parecer Instrutório"),
	PARECER_RECURSAL("Parecer Recursal"),
	RAIA_FISCALIZACAO("RAIA - Fiscalização"),
	RAIA_FUMACA_NEGRA("RAIA - Fumaça Negra"),
	RAMA("Rama");

	private String descricao;

	private TipoQuestionario(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}