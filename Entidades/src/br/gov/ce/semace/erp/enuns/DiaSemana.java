package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

public enum DiaSemana {

	DOMINGO(1L, "Domingo"), SEGUNDA(2L, "Segunda"), TERCA(3L, "Terça"),
	QUARTA(4L, "Quarta"), QUINTA(5L, "Quinta"), SEXTA(6L, "Sexta"),
	SABADO(7L,"Sábado");

	private DiaSemana(Long identificador, String descricao) {
		this.identificador = identificador;
		this.descricao = descricao;
	}

	private Long identificador;
	private String descricao;

	public Long getIdentificador() {
		return identificador;
	}

	public void setIdentificador(Long identificador) {
		this.identificador = identificador;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public static List<DiaSemana> listDias(){
		List<DiaSemana> list = new ArrayList<DiaSemana>();
		list.add(SEGUNDA);
		list.add(TERCA);
		list.add(QUARTA);
		list.add(QUINTA);
		list.add(SEXTA);
		return list;
	}

	public static DiaSemana findDiaByIdentificador(String id){
		switch (id) {
		case "2":
			return SEGUNDA;
		case "3":
			return TERCA;
		case "4":
			return QUARTA;
		case "5":
			return QUINTA;
		case "6":
			return SEXTA;
		}
		return null;
	}

}
