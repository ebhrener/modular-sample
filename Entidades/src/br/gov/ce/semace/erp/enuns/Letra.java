package br.gov.ce.semace.erp.enuns;

public enum Letra {
	A, B, C, D, E, F, G, H, I, J, L, M, N, O, P, Q, R, S, T, U;

	public static String getValue(Letra letra) {
		switch (letra) {
			case A:
				return "A";
			case B:
				return "B";
			case C:
				return "C";
			case D:
				return "D";
			case E:
				return "E";
			case F:
				return "F";
			case G:
				return "G";
			case H:
				return "H";
			case I:
				return "I";
			case J:
				return "J";
			case L:
				return "L";
			case M:
				return "M";
			case N:
				return "N";
			case O:
				return "O";
			case P:
				return "P";
			case Q:
				return "Q";
			case R:
				return "R";
			case S:
				return "S";
			case T:
				return "T";
			case U:
				return "U";
		}
		return null;
	}
	
}
