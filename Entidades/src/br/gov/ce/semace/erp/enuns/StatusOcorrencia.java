package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.entity.fiscalizacao.AutoInfracao;
import br.gov.ce.semace.erp.entity.fiscalizacao.Ocorrencia;
import br.gov.ce.semace.erp.entity.fiscalizacao.OrdemFiscalizacao;
import br.gov.ce.semace.erp.entity.fiscalizacao.Resposta;

public enum StatusOcorrencia {
	REGISTRADA("Aguardando Atendimento"), // 0
	EM_FISCALIZACAO("Em Fiscalização"), // 1
	FINALIZADA("Procedente"), // 2
	NAO_ATENDIDA("Não Atendida"), // 3
	NAO_LOCALIZADA("Não Localizada"), // 4
	REMOVIDA("Removida"), // 5
	CANCELADA("Cancelada"), // 6
	IMPROCEDENTE("Improcedente"), // 7
	NAO_CONCLUIDA("Não Concluída"), // 8
	ENCAMINHADA("Encaminhada"); // 9

	private String descricao;

	private StatusOcorrencia(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
	* Lista de Status ocorrencia que eram chamados atraves do campo que foi removido, visible.
	*/
	public static List<StatusOcorrencia> listVisible(){
		List<StatusOcorrencia> list = new ArrayList<>();
		list.add(ENCAMINHADA);
		list.add(IMPROCEDENTE);
		list.add(NAO_ATENDIDA);
		list.add(NAO_CONCLUIDA);
		list.add(NAO_LOCALIZADA);
		list.add(FINALIZADA);
		return list;
	}

	@Deprecated
	public static List<StatusOcorrencia> isVinculoDocumentoLiberada() {
		List<StatusOcorrencia> list = new ArrayList<>();
		list.add(REGISTRADA);
		list.add(ENCAMINHADA);
		list.add(FINALIZADA);
		list.add(NAO_CONCLUIDA);
		return list;
	}

	public static List<StatusOcorrencia> permitirResponderOcorrenciaRespondida() {
		List<StatusOcorrencia> list = new ArrayList<>();
		list.add(ENCAMINHADA);
		list.add(NAO_CONCLUIDA);
		list.add(FINALIZADA);
		return list;
	}

	/**
	 * {@link List}a de {@link StatusOcorrencia} referente aos tipos de {@link Resposta}s
	 * que requerem ao usuário informar as coordenadas.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 02/09/2015 09:17:02
	 *
	 * @return
	 * 	Retorna uma {@link List}a de {@link StatusOcorrencia}
	 */
	public static List<StatusOcorrencia> listRespostasComCoordenadas(){

		List<StatusOcorrencia> list = new ArrayList<>();

		list.add(NAO_CONCLUIDA);
		list.add(FINALIZADA);
		list.add(IMPROCEDENTE);

		return list;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #REGISTRADA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/09/2015 11:03:07
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #REGISTRADA}, caso contrário retorna <code>false</code>
     */
    public boolean isRegistrada() {

        if (name().equals(REGISTRADA.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EM_FISCALIZACAO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/09/2015 11:09:43
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EM_FISCALIZACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isEmFiscalizacao() {

        if (name().equals(EM_FISCALIZACAO.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #FINALIZADA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/09/2015 11:10:14
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #FINALIZADA}, caso contrário retorna <code>false</code>
     */
    public boolean isFinalizada() {

        if (name().equals(FINALIZADA.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #NAO_ATENDIDA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/09/2015 11:11:59
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #NAO_ATENDIDA}, caso contrário retorna <code>false</code>
     */
    public boolean isNaoAtendida() {

        if (name().equals(NAO_ATENDIDA.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #NAO_LOCALIZADA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/09/2015 11:13:01
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #NAO_LOCALIZADA}, caso contrário retorna <code>false</code>
     */
    public boolean isNaoLocalizada() {

        if (name().equals(NAO_LOCALIZADA.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #REMOVIDA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/09/2015 11:13:28
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #REMOVIDA}, caso contrário retorna <code>false</code>
     */
    public boolean isRemovida() {

        if (name().equals(REMOVIDA.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/09/2015 11:15:52
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADA}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelada() {

        if (name().equals(CANCELADA.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #IMPROCEDENTE}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/09/2015 11:16:29
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #IMPROCEDENTE}, caso contrário retorna <code>false</code>
     */
    public boolean isImprocedente() {

        if (name().equals(IMPROCEDENTE.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #NAO_CONCLUIDA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/09/2015 11:17:03
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #NAO_CONCLUIDA}, caso contrário retorna <code>false</code>
     */
    public boolean isNaoConcluida() {

        if (name().equals(NAO_CONCLUIDA.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ENCAMINHADA}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ENCAMINHADA}, caso contrário retorna <code>false</code>
     */
    public boolean isEncaminhada() {

        if (name().equals(ENCAMINHADA.name())){
            return true;
        }

        return false;
    }

    /**
     * {@link List}a de {@link StatusOcorrencia} que permitem uma {@link OrdemFiscalizacao} ser Finalizada:
     *
     * <ul>{@value #FINALIZADA}</ul>
     * <ul>{@value #NAO_CONCLUIDA}</ul>
     * <ul>{@value #NAO_LOCALIZADA}</ul>
     * <ul>{@value #IMPROCEDENTE}</ul>
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 15/02/2016 09:07:49
     *
     * @return
     * 	Retorna uma {@link List}a de {@link StatusOcorrencia}
     */
    public static List<StatusOcorrencia> getListStatusOcorrenciaFinalizaOrdemFiscalizacao(){

    	List<StatusOcorrencia> list = new ArrayList<>();

    	list.add(ENCAMINHADA);
		list.add(FINALIZADA);
		list.add(NAO_CONCLUIDA);
		list.add(NAO_LOCALIZADA);
		list.add(IMPROCEDENTE);
		list.add(NAO_ATENDIDA);

		return list;

    }

    public static List<StatusOcorrencia> getListStatusOcorrenciaDisponivelNovaOrdemFiscalizacao(){

    	List<StatusOcorrencia> list = new ArrayList<>();

		list.add(REGISTRADA);
		list.add(ENCAMINHADA);
		list.add(NAO_ATENDIDA);
		list.add(NAO_CONCLUIDA);

		return list;
    }


	public static StatusOcorrencia fromInt(int valor) {

		for (StatusOcorrencia statusOcorrencia : StatusOcorrencia.values()) {
			if (statusOcorrencia.ordinal() == valor){
				return statusOcorrencia;
			}
		}

		return null;
	}

	public static List<StatusOcorrencia> valuesOrderedByDescricao(){
		List<StatusOcorrencia> list =  Arrays.asList(StatusOcorrencia.values());
		Collections.sort(list, new Comparator<StatusOcorrencia>() {

			@Override
			public int compare(StatusOcorrencia o1, StatusOcorrencia o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

	/**
	 * Retorna uma lista de {@link StatusOcorrencia} que podem ser vinculados ao {@link AutoInfracao}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 10/05/2016 10:28:54
	 *
	 * @return
	 * 		Retorna uma {@link List}a de {@link StatusOcorrencia}
	 */
	public static List<StatusOcorrencia> getListPodemVincularAutoInfracao(){
		List<StatusOcorrencia> list = new ArrayList<>();

		list.add(REGISTRADA);
		list.add(ENCAMINHADA);
		list.add(FINALIZADA);
		list.add(NAO_CONCLUIDA);
		list.add(EM_FISCALIZACAO);

		return list;
	}


	/**
	 * Informa uma lista de {@link StatusOcorrencia} que requerem resposta referente ao foco de dengue.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 11/01/2016 14:51:09
	 *
	 * @return
	 * 		Retorna uma {@link List}a com os {@link StatusOcorrencia}
	 */
	public static List<StatusOcorrencia> getListObrigatorioResponderFocoDengue() {
		return listStatusFinalizamOcorrencia();
	}

	/**
	 * Informa uma lista de {@link StatusOcorrencia} que requerem resposta sobre se foi feita vistoria em campo ou não (analise de processo).
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 13/01/2016 08:54:50
	 *
	 * @return
	 * 		Retorna uma {@link List}a com os {@link StatusOcorrencia}
	 */
	public static List<StatusOcorrencia> getListObrigatorioResponderVistoriaCampo() {
		return listStatusFinalizamOcorrencia();
	}

	/**
	 * {@link List}a de {@link StatusOcorrencia} que finalizam uma {@link Ocorrencia}.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 13/01/2016 08:56:19
	 *
	 * @return
	 * 		Retorna uma {@link List}a com os {@link StatusOcorrencia}
	 */
	private static List<StatusOcorrencia> listStatusFinalizamOcorrencia(){
		List<StatusOcorrencia> list = new ArrayList<>();
		list.add(ENCAMINHADA);
		list.add(FINALIZADA);
		list.add(IMPROCEDENTE);
		list.add(NAO_CONCLUIDA);
		return list;
	}

	public static List<StatusOcorrencia> getListStatusOcorrenciaAtendida(){

		List<StatusOcorrencia> list = new ArrayList<>();
		list.add(ENCAMINHADA);
		list.add(FINALIZADA);
		list.add(IMPROCEDENTE);
		list.add(NAO_CONCLUIDA);
		list.add(NAO_LOCALIZADA);

		return list;

	}

	public static List<StatusOcorrencia> getListStatusOcorrenciaNaoAtendida(){

		List<StatusOcorrencia> list = new ArrayList<>();
		list.add(EM_FISCALIZACAO);
		list.add(REGISTRADA);
		list.add(NAO_ATENDIDA);

		return list;

	}

	public static int[] getArrayOrdinal(List<StatusOcorrencia> list){

		int[] retorno = new int[list.size()];
		int i = 0;
		for (StatusOcorrencia statusOcorrencia : list) {
			retorno[i] = statusOcorrencia.ordinal();
			i++;
		}
		return retorno;
	}
}