package br.gov.ce.semace.erp.enuns;

public enum StatusComunicadoInterno {
	
	PENDENTE("Pendente"),
	ASSINADO("Assinado"),
	ENVIADO("Enviado"),
	RECEBIDO("Recebido"),
	CANCELADO("Cancelado");
	
	private String descricao;
	
	
	
	private StatusComunicadoInterno(String descricao) {
		
		this.descricao = descricao;
		
	}

	public String getDescricao() {
		return descricao;
	}

	
	
}
