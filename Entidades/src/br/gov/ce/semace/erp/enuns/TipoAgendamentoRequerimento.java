package br.gov.ce.semace.erp.enuns;

public enum TipoAgendamentoRequerimento {
	
	REQUERIMENTO("Requerimento"),
	ANALISE("Análise Laboratorial");
	
	private String descricao;
	
	private TipoAgendamentoRequerimento(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
