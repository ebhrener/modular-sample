package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.entity.fiscalizacao.Raia;

/**
 * Enum que representa o tipo de justificativa para a alteração de um {@link Raia}
 *
 * @author rodrigo.silva - Rodrigo Silva Oliveira - 27/11/2017 11:52:28
 */
public enum TipoJustificativaRaia {

	CANCELAR("Cancelar"),
	EDITAR("Editar"),
	MUDAR_STATUS("Mudança de Status"),
	ASSINAR("Assinar");

	private String descricao;

	private TipoJustificativaRaia(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELAR}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELAR}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelar() {

        if (name().equals(CANCELAR.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EDITAR}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EDITAR}, caso contrário retorna <code>false</code>
     */
    public boolean isEditar() {

        if (name().equals(EDITAR.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #MUDAR_STATUS}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #MUDAR_STATUS}, caso contrário retorna <code>false</code>
     */
    public boolean isMudarStatus() {

        if (name().equals(MUDAR_STATUS.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ASSINAR}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ASSINAR}, caso contrário retorna <code>false</code>
     */
    public boolean isAssinar() {

        if (name().equals(ASSINAR.name())){
            return true;
        }

        return false;
    }

    public static TipoJustificativaRaia fromInt(int valor) {

		for (TipoJustificativaRaia tipoJustificativaRaia : TipoJustificativaRaia.values()) {
			if (tipoJustificativaRaia.ordinal() == valor){
				return tipoJustificativaRaia;
			}
		}

		return null;
	}

	public static List<TipoJustificativaRaia> valuesOrderedByDescricao(){
		List<TipoJustificativaRaia> list =  Arrays.asList(TipoJustificativaRaia.values());
		Collections.sort(list, new Comparator<TipoJustificativaRaia>() {

			@Override
			public int compare(TipoJustificativaRaia o1, TipoJustificativaRaia o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}
}