package br.gov.ce.semace.erp.enuns;

public enum StatusAssinatura {

	REJEITADA("Rejeitada"),
	PENDENTE("Pendente"),
	ASSINADO("Assinado");

	private StatusAssinatura(String descricao) {
		this.descricao = descricao;
	}

	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isRejeitada() {
		return REJEITADA.name().equals(this.name());
	}
}
