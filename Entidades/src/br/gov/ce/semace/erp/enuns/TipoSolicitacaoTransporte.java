package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago Nascimento - vilard@gmail.com - 25/04/2014 - 09:17:49
 *
 */

public enum TipoSolicitacaoTransporte {

	LOCAL("Local"),
	REGIONAL("Regional"),
	ESTADUAL("Estadual"),
	NACIONAL("Nacional"),
	INTERNACIONAL("Internacional");

	private String descricao;

	private TipoSolicitacaoTransporte(String descricao) {
		this.descricao = descricao;
	}

	public boolean isInternacional(){
		if(this.name().equals(INTERNACIONAL.name())){
			return true;
		}

		return false;
	}

	public boolean isNacional(){
		if(this.name().equals(NACIONAL.name())){
			return true;
		}

		return false;
	}

	public boolean isEstadual(){
		if(this.name().equals(ESTADUAL.name())){
			return true;
		}

		return false;
	}

	public boolean isRegional(){
		if(this.name().equals(REGIONAL.name())){
			return true;
		}

		return false;
	}

	public boolean isLocal(){
		if(this.name().equals(LOCAL.name())){
			return true;
		}

		return false;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 *
	 * Lista dos tipos de solicitações que podem ser pagas diária.
	 *
	 * @author Tiago Nascimento - vilard@gmail.com - 25/04/2014 - 09:16:39
	 *
	 * @return Lista de tipos de solicitações
	 *
	 */
    public static List<TipoSolicitacaoTransporte> getTipoSolicitacaoPagaDiaria(){

    	List<TipoSolicitacaoTransporte> retorno = new ArrayList<>();
    	retorno.add(REGIONAL);
    	retorno.add(ESTADUAL);
    	retorno.add(NACIONAL);
    	retorno.add(INTERNACIONAL);

    	return retorno;
    }

	/**
	 *
	 * Lista dos tipos de solicitações que podem ser pagas ajuda de custo.
	 *
	 * @author Tiago Nascimento - vilard@gmail.com - 25/04/2014 - 09:16:39
	 *
	 * @return Lista de tipos de solicitações
	 *
	 */
    public static List<TipoSolicitacaoTransporte> getTipoSolicitacaoPagaAjudaCusto(){

    	List<TipoSolicitacaoTransporte> retorno = new ArrayList<>();

    	retorno.add(NACIONAL);
    	retorno.add(INTERNACIONAL);

    	return retorno;
    }

	public static List<TipoSolicitacaoTransporte> getListTipoSolicitacaoTransporteOrdenada(){
		ArrayList<TipoSolicitacaoTransporte> listOrdenada = new ArrayList<TipoSolicitacaoTransporte>();
		listOrdenada.add(TipoSolicitacaoTransporte.ESTADUAL);
		listOrdenada.add(TipoSolicitacaoTransporte.INTERNACIONAL);
		listOrdenada.add(TipoSolicitacaoTransporte.LOCAL);
		listOrdenada.add(TipoSolicitacaoTransporte.NACIONAL);
		listOrdenada.add(TipoSolicitacaoTransporte.REGIONAL);
		return listOrdenada;
	}
}