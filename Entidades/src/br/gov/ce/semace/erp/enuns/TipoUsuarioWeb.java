package br.gov.ce.semace.erp.enuns;


public enum TipoUsuarioWeb {
	
	PROPRIO("Próprio"),
	CONSULTOR("Consultor");
	
	private final String descricao;
	
	private TipoUsuarioWeb(String descricao){
		this.descricao=descricao;
	}
	
	public String getDescricao(){
		return descricao;
	}

}
