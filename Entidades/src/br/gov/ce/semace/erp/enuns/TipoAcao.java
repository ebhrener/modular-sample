package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum TipoAcao {

	INCLUSAO (1, "Inclusão"),
	EDICAO (2, "Edição"),
	EXCLUSAO (3, "Exclusão"),
	CANCELAMENTO (4, "Cancelamento"),
	SOLICITACAO_CANCELAMENTO (5, "Solicitacão Cancelamento");

	private String descricao;
	private Integer value;

	private TipoAcao(Integer value, String desc) {
		this.descricao 	= desc;
		this.value 		= value;
	}

    public boolean isInclusao() {
    	if (name().equals(INCLUSAO.name())){
            return true;
    	}
        return false;
    }

    public boolean isEdicao() {
    	if (name().equals(EDICAO.name())){
            return true;
    	}
        return false;
    }

    public boolean isExclusao() {
    	if (name().equals(EXCLUSAO.name())){
            return true;
    	}
        return false;
    }

    public boolean isCancelamento() {
    	if (name().equals(CANCELAMENTO.name())){
            return true;
    	}
        return false;
    }

    public boolean isSolicitacaoCancelamento() {
    	if (name().equals(SOLICITACAO_CANCELAMENTO.name())){
            return true;
    	}
        return false;
    }

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public static TipoAcao fromInt(int valor) {
		for (TipoAcao tipoAcao : TipoAcao.values()) {
			if (tipoAcao.ordinal() == valor){
				return tipoAcao;
			}
		}
		return null;
	}

	public static List<TipoAcao> valuesOrderedByDescricao(){
		List<TipoAcao> list =  Arrays.asList(TipoAcao.values());
		Collections.sort(list, new Comparator<TipoAcao>() {
			@Override
			public int compare(TipoAcao o1, TipoAcao o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}
}