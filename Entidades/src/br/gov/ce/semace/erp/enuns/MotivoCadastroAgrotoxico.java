package br.gov.ce.semace.erp.enuns;

public enum MotivoCadastroAgrotoxico {

	REGISTRO_COMERCIALIZADOR("Solitação de registro de estabelecimento comercializador de produtos agrotóxicos"),
    RENOVACAO_COMERCIALIZADOR("Solitação de renovação de registro de estabelecimento comercializador de produtos agrotóxicos"),
	REGISTRO_PRESTADOR_UTILIZADOR("Solitação de registro de estabelecimento prestador de serviços com utilização de agrotóxicos"),
	RENOVACAO_PRESTADOR_UTILIZADOR("Solitação de renovação de registro de estabelecimento prestador de serviços com utilização de agrotóxicos"),
	REGISTRO_UTILIZADOR("Solitação de registro de estabelecimento utilizador de agrotóxicos"),
	RENOVACAO_UTILIZADOR("Solitação de registro de registro de estabelecimento utilizador de agrotóxicos");

	private String descricao;

	private MotivoCadastroAgrotoxico(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
