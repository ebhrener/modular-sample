package br.gov.ce.semace.erp.enuns;

public enum TipoCoordenada {
	
	UTM("UTM"), GEOGRAFICA("Geográfica");
	
	private String descricao;
	
	private TipoCoordenada(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public boolean isUTM() {
		return UTM.name().equals(this.name());
	}

	public boolean isGeografica() {
		return GEOGRAFICA.name().equals(this.name());
	}
}
