package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.entity.geral.Questao;
import br.gov.ce.semace.erp.entity.geral.Questionario;

public enum TipoResposta {

	TEXTO_CURTO("Texto Curto"),
	TEXTO_LONGO("Texto Longo"),
	MULTIPLA_ESCOLHA("Multipla Escolha"),
	CAIXA_SELECAO("Caixa de Seleção"),
	OUTROS("Outros"),
	UPLOAD_IMAGEM("Upload de Imagens"),
	MAPA_RESPOSTA("Mapa Resposta"),
	MAPA_CABECALHO_RESPOSTA("Mapa Cabeçalho Resposta");

	private String descricao;

	private TipoResposta(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #TEXTO_CURTO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 10/06/2013 11:21:55
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #TEXTO_CURTO}, caso contrário retorna <code>false</code>
     */
	public boolean isTextoCurto() {

		if (name().equals(TEXTO_CURTO.name())){
			return true;
		}

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #TEXTO_LONGO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 10/06/2013 11:23:04
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #TEXTO_LONGO}, caso contrário retorna <code>false</code>
     */
	public boolean isTextoLongo() {

		if (name().equals(TEXTO_LONGO.name())){
			return true;
		}

		return false;
	}


	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #MULTIPLA_ESCOLHA}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 10/06/2013 11:23:33
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #MULTIPLA_ESCOLHA}, caso contrário retorna <code>false</code>
     */
	public boolean isMultiplaEscolha() {

		if (name().equals(MULTIPLA_ESCOLHA.name())){
			return true;
		}

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CAIXA_SELECAO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 10/06/2013 11:23:33
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CAIXA_SELECAO}, caso contrário retorna <code>false</code>
     */
	public boolean isCaixaSelecao() {

		if (name().equals(CAIXA_SELECAO.name())){
			return true;
		}

		return false;
	}

	public boolean isUploadImagem() {

		if (name().equals(UPLOAD_IMAGEM.name())){
			return true;
		}

		return false;
	}

	public boolean isMapaResposta() {

		if (name().equals(MAPA_RESPOSTA.name())){
			return true;
		}

		return false;
	}

	public boolean isMapaCabecalhoResposta() {

		if (name().equals(MAPA_CABECALHO_RESPOSTA.name())) {
			return true;
		}

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #OUTROS}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 10/06/2013 11:23:33
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #OUTROS}, caso contrário retorna <code>false</code>
     */
	public boolean isOutros() {

		if (name().equals(OUTROS.name())){
			return true;
		}

		return false;
	}

	/**
     * Informa os {@link TipoResposta} que devem ser descritos ao adicionar o Tipo de resposta a uma {@link Questao} no {@link Questionario}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 23/10/2014 14:58:43
     *
     * @return
     * 		Retorna uma {@link List}a com os Tipos de Resposta:
     *
     *  <br>{@link #MULTIPLA_ESCOLHA} <br>
     * 		{@link #CAIXA_SELECAO}
     */
    public static List<TipoResposta> getListTipoRespostaExigeDescricaoOpcao(){

    	List<TipoResposta> retorno = new ArrayList<>();

    	retorno.add(MULTIPLA_ESCOLHA);
    	retorno.add(CAIXA_SELECAO);

    	return retorno;
    }

	public static List<TipoResposta> valuesOrderedByDescricao(){
		List<TipoResposta> list =  Arrays.asList(TipoResposta.values());
		Collections.sort(list, new Comparator<TipoResposta>() {

			@Override
			public int compare(TipoResposta o1, TipoResposta o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

}
