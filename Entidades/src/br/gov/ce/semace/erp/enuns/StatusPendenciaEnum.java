package br.gov.ce.semace.erp.enuns;

public enum StatusPendenciaEnum {

	/*0*/ABERTO("Em Aberto"),
	/*1*/AGUARDANDO_ANALISE("Aguardando análise"),
	/*2*/RESOLVIDO("Resolvida"),
	/*3*/LIBERADO("Liberada"),
	/*4*/CANCELAR("Cancelada");

	private String descricao;

	private StatusPendenciaEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isAberto() {
		return ABERTO.equals(this);
	}

	public boolean isAguardandoAnalise() {
		return AGUARDANDO_ANALISE.equals(this);
	}

	public boolean isResolvido() {
		return RESOLVIDO.equals(this);
	}

	public boolean isLiberado() {
		return LIBERADO.equals(this);
	}

	public boolean isCancelado() {
		return CANCELAR.equals(this);
	}

	public static StatusPendenciaEnum[] getListStatusDisponiveisParaInteressado() {
		return new StatusPendenciaEnum[] {ABERTO, LIBERADO};
	}

	public static StatusPendenciaEnum[] getListStatusVisualizarPendenciaAtendimento() {
		return new StatusPendenciaEnum[] {ABERTO, LIBERADO,AGUARDANDO_ANALISE};
	}
}
