package br.gov.ce.semace.erp.enuns;

public enum StatusArquivoTemporario {

	/*0*/ CRIADO("Criado"),
	/*1*/ SINCRONIZADO("Sincronizado"),
	/*2*/ EXPURGADO("Expurgado");

	private String descricao;

	private StatusArquivoTemporario(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public static StatusArquivoTemporario fromInt(int valor) {
		for (StatusArquivoTemporario status : StatusArquivoTemporario.values()) {
			if (status.ordinal() == valor){
				return status;
			}
		}
		return null;
	}

	public boolean isCriado() {
        return this.name().equals(CRIADO.name());
    }

	public boolean isSincronizado() {
        return this.name().equals(SINCRONIZADO.name());
    }

	public boolean isExpurgado() {
        return this.name().equals(EXPURGADO.name());
    }

}