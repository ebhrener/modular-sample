package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum BensApreendidosTermo {

    BENS_APREENDIDOS_PRODUTOS_FLORESTAIS("Produtos Florestais"),
    BENS_APREENDIDOS_PRODUTOS_PESQUEIROS("Produtos Pesqueiros"),
    BENS_APREENDIDOS_ANIMAIS_SILVESTRES("Animais Silvestres"),
    BENS_APREENDIDOS_ARMAS("Armas"),
    BENS_APREENDIDOS_PETRECHOS_PESCA("Petrechos de Caça e Pesca"),
    BENS_APREENDIDOS_OUTROS("Outros");

	private String descricao;

	private BensApreendidosTermo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public static BensApreendidosTermo getEnumByDescricao(String descricao){

		for (BensApreendidosTermo bens : values()) {
			if(bens.getDescricao().equals(descricao)) {
				return bens;
			}
		}

		return null;
	}


	public static List<BensApreendidosTermo> valuesOrderedByDescricao(){
		List<BensApreendidosTermo> list =  Arrays.asList(BensApreendidosTermo.values());
		Collections.sort(list, new Comparator<BensApreendidosTermo>() {

			@Override
			public int compare(BensApreendidosTermo o1, BensApreendidosTermo o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #BENS_APREENDIDOS_OUTROS}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 02/06/2016 09:27:56
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #BENS_APREENDIDOS_OUTROS}, caso contrário retorna <code>false</code>
     */
    public boolean isOutros() {

        if (name().equals(BENS_APREENDIDOS_OUTROS.name())){
            return true;
        }

        return false;
    }

}
