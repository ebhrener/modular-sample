package br.gov.ce.semace.erp.enuns;

public enum StatusRequerimento {

	/*0*/ AGENDADO("Agendado"),
	/*1*/ VALIDACAO_PREVIA("Validação Prévia"),
	/*2*/ GERACAO_BOLETO("Geração de DAE"),
	/*3*/ AGUARDANDO_PAGAMENTO("Aguardando Pagamento"),
	/*4*/ VALIDACAO_DOCUMENTO_SHAPE("Validação Documentos/Shape"),
	/*5*/ VALIDACAO_DOC("Validação Documentos"),
	/*6*/ VALIDACAO_CONCLUIDA("Validação Concluída"),
	/*7*/ VALIDACAO_CONCLUIDA_PENDENCIA("Validação Concluída com Pendência"),
	/*8*/ ATENDIDO("Atendido"),
	/*9*/ CANCELADO("Cancelado"),
	/*10*/ EXCLUIDO("Excluído"),
	/*11*/ ANEXAR_DOCUMENTOS("Anexar Documentos");

	private String descricao;

	private StatusRequerimento(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ATENDIDO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 29/03/2016 11:55:48
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ATENDIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isAtendido() {

        if (this.name().equals(ATENDIDO.name())){
            return true;
        }

        return false;
    }
}
