package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.entity.fiscalizacao.AutoInfracao;

/**
 * {@link Enum} referente aos Status do {@link AutoInfracao}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #ATIVO} <br>
 * {@link #CANCELADO} <br>
 * {@link #PENDENTE_SINCRONIA} <br>
 *
 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28 de jun de 2016 09:51:30
 */
public enum StatusAutoInfracao {

	/**
	 * <b>Descrição: </b>Ativo<br>
	 * <b>Valor:</b> 0
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28 de jun de 2016 09:51:42
	 */
	ATIVO("Ativo"),

	/**
	 * <b>Descrição: </b>Cancelado<br>
	 * <b>Valor:</b> 1
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28 de jun de 2016 09:51:55
	 */
	CANCELADO("Cancelado"),

	/**
	 * <b>Descrição: </b>Pendência Sincronia<br>
	 * <b>Valor:</b> 2
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28 de jun de 2016 09:52:13
	 */
	PENDENTE_SINCRONIA("Pendência Sincronia");

	/**
     * Descrição do {@link Enum}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28 de jun de 2016 09:53:59
     */
	private String descricao;

	 /**
     * Construtor com o parâmetro descricao.
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28 de jun de 2016 09:54:05
     *
     * @param descricao
     *            Parâmetro a ser setado no campo <code>{@link #descricao} </code> do {@link Enum}
     */
	private StatusAutoInfracao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Método get da propriedade Descricao do {@link Enum}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28 de jun de 2016 09:54:15
	 *
	 * @return Retorna a descricao do {@link Enum}
	 */
	public String getDescricao() {
		return this.descricao;
	}

	/**
	 * Verifica se o {@link #name()} do {@link Enum} é {@link #ATIVO}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28 de jun de 2016 09:54:30
	 *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ATIVO}, caso contrário retorna <code>false</code>
	 */
    public boolean isAtivo() {

    	if (name().equals(ATIVO.name())){
            return true;
    	}

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28 de jun de 2016 09:54:46
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADO}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelado() {

        if (name().equals(CANCELADO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PENDENTE_SINCRONIA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28 de jun de 2016 09:55:11
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PENDENTE_SINCRONIA}, caso contrário retorna <code>false</code>
     */
    public boolean isPendenteSincronia() {

        if (name().equals(PENDENTE_SINCRONIA.name())){
            return true;
        }

        return false;
    }

	public static List<StatusAutoInfracao> valuesOrderedByDescricao(){
		List<StatusAutoInfracao> list =  Arrays.asList(StatusAutoInfracao.values());
		Collections.sort(list, new Comparator<StatusAutoInfracao>() {

			@Override
			public int compare(StatusAutoInfracao o1, StatusAutoInfracao o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

}