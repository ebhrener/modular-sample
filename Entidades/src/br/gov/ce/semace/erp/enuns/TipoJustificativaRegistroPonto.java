 package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Harisson Rafael [harissonrafael@gmail.com] - 15/01/2017 10:12:07
 *
 * @version 1.0
 */
public enum TipoJustificativaRegistroPonto {

	/* 0 */ANIVERSARIO("Aniversário"),
	/* 1 */ATESTADO_MEDICO("Atestado Médico"),
	/* 2 */ATRASO("Atraso/Estagiário"),
	/* 3 */CONSULTA_MEDICA("Consulta Médica"),
	/* 4 */CURSO_PALESTRA_SEMINARIO("Curso, Palestra ou Seminário"),
	/* 5 */FOLGA_TRE("Folga TRE"),
	/* 6 */OUTROS("Outros"),
	/* 7 */SERVICO_EXTERNO("Serviço Externo"),
	/* 8 */CANCELAR("Cancelar Registro"),
	/* 9 */REATIVAR("Reativar Registro"),
	/* 10 */VIAGEM_A_SERVICO("Viagem a Serviço"),
	/* 11 */FERIAS("Férias"),
	/* 12 */LICENCA_SAUDE("Licença Saúde"),
	/* 13 */LICENCA_ESPECIAL("Licença Especial"),
	/* 14 */LICENCA_MATERNIDADE("Licença Maternidade"),
	/* 15 */LICENCA_PATERNIDADE("Licença Paternidade"),
	/* 16 */DOACAO_SANGUE("Doação de Sangue"),
	/* 17 */LUTO("Luto"),
	/* 18 */VIAGEM_FORA_ESTADO("Viagem fora do estado"),
	/* 19 */MESTRADO("Mestrado"),
	/* 20 */DOUTORADO("Doutorado"),
	/* 21 */RECESSO("Recesso"),
	/* 22 */BANCO_HORA("Banco de Horas"),
	/* 23 */HORA_COMPENSAR("Horas a compensar"),
	/* 24 */ESQUECIMENTO("Esquecimento"),
	/* 25 */TRIBUNAL_JURI("Tribunal do Júri"),
	/* 26 */LICENCA_INTERESSE_PARTICULAR("Licença interesse particular"),
	/* 27 */IMPOSSIBILIDADE_COMPENSACAO("Impossibilidade de Compensação"),
	/* 28 */CANCELAMENTO("Cancelamento"),
	/* 29 */LICENCA_CASAMENTO("Licença Casamento"),
	/* 30 */BANCO_HORA_AUTOMATICO("Banco de Horas automatico"),
	/* 31 */DESCONTO_FOLHA("Desconto em folha"),
	/* 32 */SISTEMA_INDISPONIVEL("Sistema indisponível"),
	/* 33 */FALTA("Falta"),
	/* 34 */FOLGA("Folga"),
	/* 35 */PROVA_ESTAGIARIO("Prova/Estagiário"),
	/* 36 */COMPENSACAO_ESTAGIARIO("Compensação/Estagiário"),
	/* 37 */LIBERADO_ORDEM_JUDICIAL("Liberado/Ordem judicial"),
	/* 38 */AULA_CAMPO_ESTAGIARIO("Aula de Campo/Estagiário");

	private String descricao;

	private TipoJustificativaRegistroPonto(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 *
	 * @author Erick Bhrener [erick.bhb@gmail.com] 31/01/2017 10:53:58
	 * @return List<TipoJustificativaRegistroPonto>
	 * @param
	 * Descrição:
	 */
	public static List<TipoJustificativaRegistroPonto> getListTipoJustificativaRegistroPontoExistente(){

		List<TipoJustificativaRegistroPonto> list = new ArrayList<>();

		list.add(CANCELAR);
		list.add(REATIVAR);

		return list;
	}
	public static List<TipoJustificativaRegistroPonto> getListTipoJustificativa(){

		List<TipoJustificativaRegistroPonto> list = new ArrayList<>();
		for (TipoJustificativaRegistroPonto tipo : TipoJustificativaRegistroPonto.values()) {
			if(tipo != CANCELAR && tipo != REATIVAR && tipo != OUTROS && tipo != IMPOSSIBILIDADE_COMPENSACAO && tipo != BANCO_HORA_AUTOMATICO){
				list.add(tipo);
			}
		}
		Collections.sort(list, new Comparator<TipoJustificativaRegistroPonto>() {

			@Override
			public int compare(TipoJustificativaRegistroPonto o1,
					TipoJustificativaRegistroPonto o2) {
				return o1.name().compareTo(o2.name());
			}

		});
		return list;
	}
	public static List<TipoJustificativaRegistroPonto> getListTipoJustificativaEmLote(){

		List<TipoJustificativaRegistroPonto> list = new ArrayList<>();

		list.add(ATESTADO_MEDICO);
		list.add(CURSO_PALESTRA_SEMINARIO);
		list.add(FERIAS);
		list.add(FOLGA_TRE);
		list.add(LICENCA_CASAMENTO);
		list.add(LICENCA_ESPECIAL);
		list.add(LICENCA_INTERESSE_PARTICULAR);
		list.add(LICENCA_MATERNIDADE);
		list.add(LICENCA_PATERNIDADE);
		list.add(LICENCA_SAUDE);
		list.add(LUTO);
		list.add(RECESSO);
		list.add(SERVICO_EXTERNO);
		list.add(SISTEMA_INDISPONIVEL);
		list.add(VIAGEM_A_SERVICO);
		list.add(VIAGEM_FORA_ESTADO);

		return list;
	}
	public static List<String> getListTipoJustificativaPorDescricao(){

		List<String> list = new ArrayList<>();

		for(TipoJustificativaRegistroPonto t : getListTipoJustificativa()){
			list.add(t.getDescricao().toUpperCase());
		}
		list.add(ATRASO.getDescricao().toUpperCase());
		list.add(OUTROS.getDescricao().toUpperCase());

		return list;
	}
	public static List<TipoJustificativaRegistroPonto> getListTipoJustificativaParaGestor(){

		List<TipoJustificativaRegistroPonto> list = new ArrayList<>();
		list.add(BANCO_HORA);
		list.add(ESQUECIMENTO);
		return list;
	}

	/**
	 * Método onde é verificado se o tipo de justificativa preenche toda a jornada do funcionário.
	 * @return boolean
	 */
	public boolean isJustificaVariosDias(){

		if(name().equals(ATESTADO_MEDICO.name()) || name().equals(FERIAS.name()) || name().equals(LICENCA_ESPECIAL.name()) ||
				name().equals(LICENCA_INTERESSE_PARTICULAR.name()) || name().equals(LICENCA_MATERNIDADE.name()) || name().equals(LICENCA_PATERNIDADE.name())
				|| name().equals(LICENCA_SAUDE.name()) || name().equals(LUTO.name())){
			return true;
		}

		return false;
	}

	public static List<TipoJustificativaRegistroPonto> getListJustificativaEmLoteNaoGeraHoraExtraFeriadoFinalDeSemana(){
		List<TipoJustificativaRegistroPonto> list = new ArrayList<>();

		list.add(FERIAS);
		list.add(LICENCA_CASAMENTO);
		list.add(LICENCA_ESPECIAL);
		list.add(LICENCA_INTERESSE_PARTICULAR);
		list.add(LICENCA_MATERNIDADE);
		list.add(LICENCA_PATERNIDADE);
		list.add(LICENCA_SAUDE);
		list.add(LUTO);

		return list;
	}
}