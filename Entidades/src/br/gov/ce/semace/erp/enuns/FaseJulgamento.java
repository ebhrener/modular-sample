package br.gov.ce.semace.erp.enuns;

public enum FaseJulgamento {
	
	DEFESA("Defesa"),
	ALEGACAO("Alegação"),
	DECISAO_1_INSTANCIA("Decisão"),
	RETRATACAO("Reconsideração"),
	TRANSITADO_EM_JULGADO("Finalizado"),
	INSTRUCAO("Instrução"),
	DECISAO_2_INSTANCIA("Decisão"),
	SUSPENSO("Suspenso"),
	RECURSO("Recurso")
	;
	
	private String descricao;

	private FaseJulgamento(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #DEFESA}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:10:27 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #DEFESA}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseDefesa() {

        if (name().equals(DEFESA.name())){
            return true;
        }

        return false;
    }
	
	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ALEGACAO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:10:27 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ALEGACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseAlegacao() {

        if (name().equals(ALEGACAO.name())){
            return true;
        }

        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #DECISAO_1_INSTANCIA}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:10:18 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #DECISAO_1_INSTANCIA}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseDecisao1Instancia() {

        if (name().equals(DECISAO_1_INSTANCIA.name())){
            return true;
        }

        return false;
    }
    
	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #RETRATACAO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:09:13 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #RETRATACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseRetratacao() {

        if (name().equals(RETRATACAO.name())){
            return true;
        }

        return false;
    }
    
	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #TRANSITADO_EM_JULGADO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:09:13 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #TRANSITADO_EM_JULGADO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseTransitadoEmJulgado() {

        if (name().equals(TRANSITADO_EM_JULGADO.name())){
            return true;
        }

        return false;
    }
    
	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #INSTRUCAO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:08:58 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #INSTRUCAO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseInstrucao() {

        if (name().equals(INSTRUCAO.name())){
            return true;
        }

        return false;
    }
    
	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #DECISAO_2_INSTANCIA}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:08:42 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #DECISAO_2_INSTANCIA}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseDecisao2Instancia() {

        if (name().equals(DECISAO_2_INSTANCIA.name())){
            return true;
        }

        return false;
    }
    
	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #SUSPENSO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:08:20 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #SUSPENSO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseSuspenso() {

        if (name().equals(SUSPENSO.name())){
            return true;
        }

        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #RECURSO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18/06/2013 16:15:37
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #RECURSO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseRecurso() {

        if (name().equals(RECURSO.name())){
            return true;
        }

        return false;
    }
}