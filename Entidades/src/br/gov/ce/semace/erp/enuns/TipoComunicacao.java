package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

public enum TipoComunicacao {
	
	PUBLICAR_SITE("Publicação no Site"),
	ENVIAR_OFICIO("Envio de Ofício"),
	ENTREGAR_PESSOALMENTE("Entrega de Ofício Pessoalmente"),
	PUBLICAR_EDITAL("Publicação em Edital"),
	ENTREGAR_AUTO_CAMPO("Entrega de Auto em Campo");
	
	private String descricao;

	private TipoComunicacao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public static List<TipoComunicacao> validarDocumentoOrigem() {
		List<TipoComunicacao> list = new ArrayList<TipoComunicacao>();
		list.add(ENTREGAR_PESSOALMENTE);
		list.add(ENVIAR_OFICIO);
		return list;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PUBLICAR_SITE}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:35:05 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PUBLICAR_SITE}, caso contrário retorna <code>false</code>
     */
	public boolean isPublicarSite() {

		if (name().equals(PUBLICAR_SITE.name()))
			return true;

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PUBLICAR_EDITAL}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 10:29:25 AM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PUBLICAR_EDITAL}, caso contrário retorna <code>false</code>
     */
	public boolean isPublicarEdital() {

		if (name().equals(PUBLICAR_EDITAL.name()))
			return true;

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ENTREGAR_PESSOALMENTE}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 10:29:04 AM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ENTREGAR_PESSOALMENTE}, caso contrário retorna <code>false</code>
     */
	public boolean isEntregarPessoalmente() {

		if (name().equals(ENTREGAR_PESSOALMENTE.name()))
			return true;

		return false;
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ENVIAR_OFICIO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 10:29:14 AM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ENVIAR_OFICIO}, caso contrário retorna <code>false</code>
     */
	public boolean isEnviarOficio() {

		if (name().equals(ENVIAR_OFICIO.name()))
			return true;

		return false;
	}
	
	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ENTREGAR_AUTO_CAMPO}
     * 
     * @author romulo
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ENTREGAR_AUTO_CAMPO}, caso contrário retorna <code>false</code>
     */
	public boolean isAutoEntregueCampo() {
		if (name().equals(ENTREGAR_AUTO_CAMPO.name())) {
			return true;
		}

		return false;
	}
	
}
