package br.gov.ce.semace.erp.enuns;

public enum TipoArquivo {

	FOTO("Foto"), DOCUMENTO("Documento"), PROGRAMA("Programa"), PLANO("Plano"), DECLARACAO("Declaração"),COMPROVANTE("Comprovante");

	private String descricao;

	private TipoArquivo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
