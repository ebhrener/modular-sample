package br.gov.ce.semace.erp.enuns;

/**
 * {@link Enum} referente aos Relatorios do Atendimento.
 *
 * 	<pre><b>Composto por:</b>
 *		<li> {@link #TIPO_INTERESSADO}
 *		<li> {@link #TIPO_LICENCA}
 *		<li> {@link #TIPO_LICENCA_AUTO}
 *		<li> {@link #TIPO_REQUERIMENTO}
 *		<li> {@link #STATUS_ATENDIMENTO}
 *	</pre>
 *
 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/01/2014 13:33:03
 */
public enum RelatorioAtendimento {

	/**
	 * <b>Descrição:</b> Relatório por Tipo de Interessado<br>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/01/2014 13:33:26
	 */
	TIPO_INTERESSADO("Relatório por Tipo de Interessado"),
	/**
	 * <b>Descrição:</b> Relatório por Tipo de Requerimento<br>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/01/2014 13:33:26
	 */
	TIPO_REQUERIMENTO("Relatório por Tipo de Requerimento"),
	/**
	 * <b>Descrição:</b> Relatório por Tipo de Licença<br>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/01/2014 13:33:26
	 */
	TIPO_LICENCA("Relatório por Tipo de Licença"),
	/**
	 * <b>Descrição:</b> Relatório por Tipo de Licença - Auto<br>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/01/2014 13:33:26
	 */
	TIPO_LICENCA_AUTO("Relatório por Tipo de Licença - Auto"),
	/**
	 * <b>Descrição:</b> Relatório por Status de Atendimento<br>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/01/2014 13:33:26
	 */
	STATUS_ATENDIMENTO("Relatório por Status de Atendimento"),
	/**
	 * <b>Descrição:</b> Relatório de Atendimentos com Pendência<br>
	 *
	 * @author elizandra - Elizandra Amarante [elizandra.ferreira@gmail.com] - 07/12/2014
	 */
	ATENDIMENTO_PENDENTE("Relatório de Atendimentos com Pendência");

    /**
     * Descrição do {@link Enum}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 08/01/2014 13:34:43
     */
	private String descricao;

	private RelatorioAtendimento(String descricao){
		this.descricao = descricao;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
}