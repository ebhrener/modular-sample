package br.gov.ce.semace.erp.enuns;

public enum StatusColeta {
	
	ABERTA("Aberta"),
	COLETADO("Coletado"),
	NAO_COLETADO("Não Coletado");
	
	private StatusColeta(String descricao) {
		this.descricao = descricao;
	}

	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
