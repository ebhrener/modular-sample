package br.gov.ce.semace.erp.enuns;

/**
 * 
 * @author tiago
 *
 */

public enum TipoLancamentoFinanceiro {

	CREDITO("Crédito"),
	DEBITO("Débito");
	
	String descricao;
	
	private TipoLancamentoFinanceiro(String descricao){
		this.descricao = descricao;	
	}
	
	public boolean isCredito(){
		if(this.name().equals(CREDITO.name())){
			return true;
		}
		
		return false;
	}
	
	public boolean isDebito(){
		if(this.name().equals(DEBITO.name())){
			return true;
		}
		
		return false;
	}
	
}
