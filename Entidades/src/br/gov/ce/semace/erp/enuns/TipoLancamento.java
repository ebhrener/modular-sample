package br.gov.ce.semace.erp.enuns;

public enum TipoLancamento {

	CREDITO("Crédito"),
	DEBITO("Débito");
	
	String descricao;
	
	public String getDescricao() {
		return descricao;
	}

	private TipoLancamento(String descricao){
		this.descricao = descricao;	
	}
	
	public boolean isCredito(){
		if(this.name().equals(CREDITO.name())){
			return true;
		}
		
		return false;
	}
	
	public boolean isDebito(){
		if(this.name().equals(DEBITO.name())){
			return true;
		}
		
		return false;
	}

}
