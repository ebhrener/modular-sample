package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

public enum StatusContrato {
	
	FINALIZADO("Finalizado"), 
	CANCELADO("Cancelado"), 
	VIGENTE("Vigente"), 
	;
	
	private String descricao;
	
	private StatusContrato(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public static List<StatusContrato> getStatusContratoAtivo() {
		List<StatusContrato> status = new ArrayList<>();
		status.add(VIGENTE);
		return status;
	}
	
	public static boolean isAtivo(StatusContrato statusContrato){
		return getStatusContratoAtivo().contains(statusContrato);
	}
}