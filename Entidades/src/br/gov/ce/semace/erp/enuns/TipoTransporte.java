package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tiago
 *
 */

public enum TipoTransporte {

	AEREO("Aéreo"),
	MARITIMO("Marítimo"),
	RODOVIARIO("Rodoviário"),
	FERROVIARIO("Ferroviário");

	String descricao;

	private TipoTransporte(String descricao) {
		this.descricao = descricao;
	}


	public String getDescricao() {
		return this.descricao;
	}

	public boolean isFerroviario(){
		if(this.name().equals(FERROVIARIO)){
			return true;
		}

		return false;
	}

	public boolean isRodoviario(){
		if(this.name().equals(RODOVIARIO)){
			return true;
		}

		return false;
	}

	public boolean isMaritimo(){
		if(this.name().equals(MARITIMO)){
			return true;
		}

		return false;
	}

	public boolean isAereo(){
		if(this.name().equals(AEREO)){
			return true;
		}

		return false;
	}

	public static List<TipoTransporte> getListTipoTransporteOrdenada(){
		ArrayList<TipoTransporte> listOrdenada = new ArrayList<TipoTransporte>();
		listOrdenada.add(TipoTransporte.AEREO);
		listOrdenada.add(TipoTransporte.FERROVIARIO);
		listOrdenada.add(TipoTransporte.MARITIMO);
		listOrdenada.add(TipoTransporte.RODOVIARIO);
		return listOrdenada;
	}
}