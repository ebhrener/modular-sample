package br.gov.ce.semace.erp.enuns;

public enum TipoShape {

	NULL_SHAPE("Null Shape",0),
	POINT("Point",1),
	POLYLINE("PolyLine", 3),
	POLYGON("Polygon", 5),
	MULTIPOINT("MultiPoint", 8),
	POINT_Z("PointZ", 11),
	POLYLINE_Z("PolyLineZ", 13),
	POLYGON_Z("PolygonZ", 15),
	MULTIPOINT_Z("MultiPointZ", 18),
	POINT_M("PointM", 21),
	POLYLINE_M("PolyLineM", 23),
	POLYGON_M("PolygonM", 25),
	MULTIPOINT_M("MultiPointM", 28),
	MULTIPATCH("MultiPatch", 31);

	// AREA("Área"),
	// EQUIPAMENTO("Equipamento");

	private String descricao;

	private Integer value;

	private TipoShape(String descricao, Integer value) {
		this.descricao = descricao;
		this.value = value;
	}

	public String getDescricao() {
		return descricao;
	}

	public Integer getValue() {
		return value;
	}
	
	public static TipoShape get(Integer value){
		for (TipoShape tipoShape : TipoShape.values()) {
			if (tipoShape.value == value) {
				return tipoShape;
			}
		}
		return null;
	}
	
	public static TipoShape get(String value){
		for (TipoShape tipoShape : TipoShape.values()) {
			if (tipoShape.descricao.toLowerCase().equals(value.toLowerCase()) || tipoShape.toString().equals(value.toLowerCase())) {
				return tipoShape;
			}
		}
		return null;
	}
}
