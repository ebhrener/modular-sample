package br.gov.ce.semace.erp.enuns;

public enum TipoArquivoContent {
	IMAGEM_QUESTIONARIO("imagem_questionario");
	
	private String descricao;
	
	private TipoArquivoContent(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
