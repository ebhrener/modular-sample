package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum TipoTermo {

	APREENSAO_DEPOSITO("Apreensão/Depósito"),
	EMBARGO_INTERDICAO("Embargo/Interdição");
	/*TERMO_COM_AUTO("Termo para Auto de Infração"),
	TERMO_COM_NOTIFICACAO("Termo para Notificação"),
	TERMO_SEM_DOCUMENTO("Termo sem Auto ou Notificação");*/

	private final String descricao;

	private TipoTermo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoTermo getEnumByDescricao(String descricao){

		for (TipoTermo tipo : values()) {
			if(tipo.getDescricao().contains(descricao)) {
				return tipo;
			}
		}

		return null;
	}


	   /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EMBARGO_INTERDICAO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 10/11/2015 11:48:16
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EMBARGO_INTERDICAO}, caso contrário retorna <code>false</code>
     */
    public boolean isApreensaoDeposito() {

        if (name().equals(APREENSAO_DEPOSITO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EMBARGO_INTERDICAO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 10/11/2015 11:48:16
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EMBARGO_INTERDICAO}, caso contrário retorna <code>false</code>
     */
    public boolean isEmbargoInterdicao() {

        if (name().equals(EMBARGO_INTERDICAO.name())){
            return true;
        }

        return false;
    }

	public static List<TipoTermo> valuesOrderedByDescricao(){

		List<TipoTermo> list =  Arrays.asList(TipoTermo.values());

		Collections.sort(list, new Comparator<TipoTermo>() {

			@Override
			public int compare(TipoTermo o1, TipoTermo o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});

		return list;
	}

}
