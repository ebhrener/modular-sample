package br.gov.ce.semace.erp.enuns;

public enum TipoImagemEditor {

	RELATORIO_GERAL_OF("br.gov.ce.semace.erp.entity.imagens.editor.ImagemRelatorioGeralOf"),
	RAIA("br.gov.ce.semace.erp.entity.imagens.editor.ImagemRaia"),
	PARECER_INSTRUTORIO("br.gov.ce.semace.erp.entity.imagens.editor.ImagemParecerInstrutorio");
	
	private String classeImagem;
	
	private TipoImagemEditor(String classeImagem) {
		this.classeImagem = classeImagem;
	}

	public String getClasseImagem() {
		return classeImagem;
	}

	public void setClasseImagem(String classeImagem) {
		this.classeImagem = classeImagem;
	}
	
}
