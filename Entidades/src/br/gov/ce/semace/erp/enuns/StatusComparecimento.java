package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tiago #RM #2393
 *
 */

public enum StatusComparecimento {

	COMPARECEU("Compareceu"),
	NAO_COMPARECEU("Não Compareceu"),
	SUBSTITUIDO("Substituido"),
	CANCELADO("Cancelado");

	String descricao;

	private StatusComparecimento(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public boolean isCompareceu(){
		if(this.name().equals(COMPARECEU.name())){
			return true;
		}

		return false;
	}

	public boolean isNaoCompareceu(){
		if(this.name().equals(NAO_COMPARECEU.name())){
			return true;
		}

		return false;
	}

	public boolean isSubstituido(){
		if(this.name().equals(SUBSTITUIDO.name())){
			return true;
		}

		return false;
	}

	public boolean isCancelado(){
		if(this.name().equals(CANCELADO.name())){
			return true;
		}

		return false;
	}

	public static List<StatusComparecimento> getListStatusComparecimentoOrdenada() {
		ArrayList<StatusComparecimento> listOrdenada = new ArrayList<StatusComparecimento>();
		listOrdenada.add(StatusComparecimento.CANCELADO);
		listOrdenada.add(StatusComparecimento.COMPARECEU);
		listOrdenada.add(StatusComparecimento.NAO_COMPARECEU);
		listOrdenada.add(StatusComparecimento.SUBSTITUIDO);
		return listOrdenada;
	}
}