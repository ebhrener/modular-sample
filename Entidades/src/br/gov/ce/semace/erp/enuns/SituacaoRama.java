package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum SituacaoRama {

/*0	 */  AGUARDANDO_PERIODO_ENTREGA("Aguardando Período de Entrega"),
/*1	 */	 DISPONIVEL_ENVIO("Disponível Para Envio"),
/*2	 */	 PENDENTE_ENVIO("Pendente de Envio"),
/*3	 */	 IRREGULAR_ATRASO_ENTREGA("Irregular por Atraso de Entrega"),
/*4	 */	 AGUARDANDO_PAGAMENTO("Aguardando Pagamento"),
/*5	 */	 PROTOCOLADO_ONLINE("Protocolado Online"),
/*6	 */	 PROTOCOLADO_PRESENCIAL("Protocolado Presencial"),
/*7	 */	 DISPENSADO("Dispensado"),
/*8	 */	 SEM_STATUS("Anterior ao RAMA Online"),
/*9  */	 IRREGULAR_FALTA_PAGAMENTO("Irregular por Falta de Pagamento"),
/*10 */  AGUARDANDO_VINCULO("Aguardando Vínculo"),
/*11 */  PROTOCOLADO_COM_VINCULO("Protocolado por Vínculo"),
/*12 */  PENDENTE("Pendente"),
/*13 */  AGUARDANDO_ANALISE("Aguardando Análise"),
/*14 */  RETIFICADO("Retificado"),
/*15 */  EM_ANALISE("Em Análise"),
/*16 */  AGUARDANDO_APROVACAO("Aguardando Aprovação do Gerente"),
/*17 */  REGULAR("Atende"),
/*18 */  IRREGULAR("Não Atende"),
/*19 */  AGUARDANDO_VALIDACAO_ISENCAO("Aguardando Validação de Isenção"),
/*20 */  AGUARDANDO_GERACAO_DAE("Aguardando Geração de DAE");


	private String descricao;

	private SituacaoRama(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public static List<SituacaoRama> listSituacaoHabilitaCriacaoAgendamento() {
		return new ArrayList<>(Arrays.asList(new SituacaoRama[] {DISPONIVEL_ENVIO, PENDENTE_ENVIO, IRREGULAR_ATRASO_ENTREGA, SEM_STATUS }));
	}

	public static List<SituacaoRama> listSituacaoHabilitaResponder(){
		return new ArrayList<>(Arrays.asList(new SituacaoRama[] {DISPONIVEL_ENVIO,PENDENTE_ENVIO,IRREGULAR_ATRASO_ENTREGA,SEM_STATUS,PENDENTE}));
	}

	public static List<SituacaoRama> listSituacaoHabilitaPreenchimentoFormulario(){
		return new ArrayList<>(Arrays.asList(new SituacaoRama[] {DISPONIVEL_ENVIO,PENDENTE_ENVIO,IRREGULAR_ATRASO_ENTREGA,SEM_STATUS}));
	}

	public static List<SituacaoRama> listSituacaoDesabilitaOpcao(){
		return new ArrayList<>(Arrays.asList(new SituacaoRama[] {AGUARDANDO_PERIODO_ENTREGA, PROTOCOLADO_PRESENCIAL, PROTOCOLADO_COM_VINCULO}));
	}

	public static List<SituacaoRama> listSituacaoHabilitaVisualizarDadosAnalise(){
		return new ArrayList<>(Arrays.asList(new SituacaoRama[] {PENDENTE,AGUARDANDO_ANALISE,RETIFICADO,EM_ANALISE,AGUARDANDO_APROVACAO,REGULAR,IRREGULAR}));
	}

	public  Integer retornaValorNumericoStatus(){
		switch(this){
		case AGUARDANDO_PERIODO_ENTREGA:
			return 0;
		case DISPONIVEL_ENVIO:
			return 1;
		case PENDENTE_ENVIO:
			return 2;
		case IRREGULAR_ATRASO_ENTREGA:
			return 3;
		case AGUARDANDO_PAGAMENTO:
			return 4;
		case PROTOCOLADO_ONLINE:
			return 5;
		case PROTOCOLADO_PRESENCIAL:
			return 6;
		case DISPENSADO:
			return 7;
		case SEM_STATUS:
			return 8;
		case IRREGULAR_FALTA_PAGAMENTO:
			return 9;
		case AGUARDANDO_VINCULO:
			return 10;
		case PROTOCOLADO_COM_VINCULO:
			return 11;
		case PENDENTE:
			return 12;
		case AGUARDANDO_ANALISE:
			return 13;
		case RETIFICADO:
			return 14;
		case EM_ANALISE:
			return 15;
		case AGUARDANDO_APROVACAO:
			return 16;
		case REGULAR:
			return 17;
		case IRREGULAR:
			return 18;
		}
		return null;
	}

	public static SituacaoRama findByDescricao(String descricao){
		switch(descricao){
		case "AGUARDANDO_PERIODO_ENTREGA":
			return AGUARDANDO_PERIODO_ENTREGA;
		case "DISPONIVEL_ENVIO":
			return DISPONIVEL_ENVIO;
		case "PENDENTE_ENVIO":
			return PENDENTE_ENVIO;
		case "IRREGULAR_ATRASO_ENTREGA":
			return IRREGULAR_ATRASO_ENTREGA;
		case "AGUARDANDO_PAGAMENTO":
			return AGUARDANDO_PAGAMENTO;
		case "PROTOCOLADO_ONLINE":
			return PROTOCOLADO_ONLINE;
		case "PROTOCOLADO_PRESENCIAL":
			return PROTOCOLADO_PRESENCIAL;
		case "DISPENSADO":
			return DISPENSADO;
		case "SEM_STATUS":
			return SEM_STATUS;
		case "IRREGULAR_FALTA_PAGAMENTO":
			return IRREGULAR_FALTA_PAGAMENTO;
		case "AGUARDANDO_VINCULO":
			return AGUARDANDO_VINCULO;
		case "PROTOCOLADO_COM_VINCULO":
			return PROTOCOLADO_COM_VINCULO;
		case "PENDENTE":
			return PENDENTE;
		case "AGUARDANDO_ANALISE":
			return AGUARDANDO_ANALISE;
		case "RETIFICADO":
			return RETIFICADO;
		case "EM_ANALISE":
			return EM_ANALISE;
		case "AGUARDANDO_APROVACAO":
			return AGUARDANDO_APROVACAO;
		case "REGULAR":
			return REGULAR;
		case "IRREGULAR":
			return IRREGULAR;
		}
		return null;
	}
}
