package br.gov.ce.semace.erp.enuns;

public enum OrigemSolicitacaoProcesso {
	
	REQUERIMENTO,
	DOC_ANEXO,
	DOC_AVULSO,
	AUTO_INFRACAO,
	TERMO,
	NOTIFICACAO,
	OFICIO,
	CI;

}
