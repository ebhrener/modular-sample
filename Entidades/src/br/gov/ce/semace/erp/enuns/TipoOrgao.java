package br.gov.ce.semace.erp.enuns;

public enum TipoOrgao {
	
	FEDERAL("Federal"), ESTADUAL("Estadual"), MUNICIPAL("Municipal"), PARTICULAR("Particular");
	
	private String descricao;

	private TipoOrgao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public boolean isFederal() {
		return this.name().equals(FEDERAL.name());
	}

	public boolean isEstadual() {
		return this.name().equals(ESTADUAL.name());
	}

	public boolean isMunicipal() {
		return this.name().equals(MUNICIPAL.name());
	}

	public boolean isParticular() {
		return this.name().equals(PARTICULAR.name());
	}
}
