package br.gov.ce.semace.erp.enuns;

public enum CallbackResponse {

	FINALIZAR,
	ASSINAR,
	ERROR;

}
