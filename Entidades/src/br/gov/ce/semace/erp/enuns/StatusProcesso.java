package br.gov.ce.semace.erp.enuns;

public enum StatusProcesso {

	CANCELADO("Cancelado"),
	ATIVO("Ativo");

	private String descricao;

	public String toString() {
		return descricao; 
	}
	
	private StatusProcesso(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 2:02:06 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADO}, caso contrário retorna <code>false</code>
     */
	public boolean isCancelado() {

		if (name().equals(CANCELADO.name()))
			return true;

		return false;
	}
	
	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ATIVO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 2:05:51 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ATIVO}, caso contrário retorna <code>false</code>
     */
	public boolean isAtivo() {

		if (name().equals(ATIVO.name()))
			return true;

		return false;
	}
}
