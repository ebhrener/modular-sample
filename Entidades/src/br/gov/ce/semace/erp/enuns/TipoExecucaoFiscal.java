package br.gov.ce.semace.erp.enuns;

public enum TipoExecucaoFiscal {

    CAPITAL("Capital"),
	PREFEITURAS("Prefeituras"),
    INTERIOR("Interior");

	private String descricao;

	private TipoExecucaoFiscal(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public static TipoExecucaoFiscal getEnumByDescricao(String descricao){
		
		for (TipoExecucaoFiscal bens : values()) {
			if(bens.getDescricao().equals(descricao))
				return bens;
		}
		
		return null;
	}
	
}
