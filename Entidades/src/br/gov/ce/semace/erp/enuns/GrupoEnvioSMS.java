package br.gov.ce.semace.erp.enuns;


/**
 * GRUPO-SEMACE-TESTE ID 215
 * SEMACE SMS 1 ID 294 (1a notificação de SMS),
 * SEMACE SMS 2 ID 295 (2a notificação),
 * SEMACE SMS 3 ID 296 (3a),
 * SEMACE URA 1 ID 297 (1a ligação de URA) e
 * SEMACE URA 2 ID 298 (2a ligação)
 *
 *	@author joerlan.lima
 *
 */
public enum GrupoEnvioSMS {

	GRUPO_SMS_1(294L),
	GRUPO_SMS_2(295L),
	GRUPO_SMS_3(296L);

	private Long remoteID;

	private GrupoEnvioSMS(Long remoteID) {
		this.remoteID = remoteID;
	}

	public Long getRemoteID() {
		return remoteID;
	}

	public void setRemoteID(Long remoteID) {
		this.remoteID = remoteID;
	}

}
