package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

public enum OrigemSolicitacaoDae {

	REQUERIMENTO("Requerimento"),
	PROCESSO("Processo"),
	PROCESSO_SIGA("Processo do Siga"),
	ANALISE_LABORATORIAL("Análise Laboratorial"),
	AUTO_INFRACAO("Auto de Infração"),
	PRODUTOS_AGROTOXICOS("Produtos Agrotóxicos"),
	TERMO_COMPENSACAO("Termo de Compromisso de Compensação Ambiental"),
	DECLARACAO_CONSUMIDOR_MATERIA_PRIMA("Declaração de Consumidor de Matéria Prima de Origem Florestal"),
	PARCELAMENTO_AUTO_INFRACAO("Parcelamento de Débito de Auto de Infração"),
	ACORDO_DIVIDA_ATIVA("Acordo Extrajudicial de Dívida Ativa"),
	JULGAMENTO("Julgamento"),
	QUITACAO_DIVIDA_ATIVA("Quitação da Dívida Ativa"),
	PROCESSO_COMPENSACAO_SIGA("Processo Compensação do Siga"),
	TCFA("Taxa de Controle e Fiscalização Ambiental"),
	TERMO_FISCALIZACAO("Termo "),
	AUTO_INFRACAO_BLITZ_FUMACA_NEGRA("Auto de Infração Bliz Fumaça Negra"),
	ESTUDO_AMBIENTAL("Estudo Ambiental"),
	CADASTRO_CONSUMIDOR_FLORESTAL("Cadastro de consumidor de matéria-prima Florestal"),
	MULTA_CONTRATUAL("Multa Contratual"),
	PARCELAMENTO_TERMO_APREENSAO_DEPOSITO("Termo de apreensão e depósito"),
	RAMA("Rama"),
	MEDIDA_COMPENSATORIA("Medida Compensatória"),
	TAC("TAC")
	;

	private String descricao;

	OrigemSolicitacaoDae(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Informa as {@link OrigemSolicitacaoDae} que não requerem Sede para gerar Dae
	 *
	 * @author antoniomenezes
	 *
	 * @return
	 * 		Retorna uma {@link List}a com as Origens Solicitação:
	 *
	 *  <br>{@link #RAMA} <br>
	 */
	public static List<OrigemSolicitacaoDae> listaSolicitacaoOrigemNaoRequerSede(){

		List<OrigemSolicitacaoDae> retorno = new ArrayList<OrigemSolicitacaoDae>();

		retorno.add(RAMA);

		return retorno;
	}

	public String getDescricao(int quantidadeCaracteres) {
		if (this.descricao.length() < quantidadeCaracteres) {
			return this.descricao;
		}
		return descricao.substring(0, quantidadeCaracteres) + " ...";
	}
}
