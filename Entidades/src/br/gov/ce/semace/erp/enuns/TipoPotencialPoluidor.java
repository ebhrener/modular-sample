package br.gov.ce.semace.erp.enuns;

import br.gov.ce.semace.erp.entity.geral.Documento;

/**
 * {@link Enum} referente aos Status do {@link Documento}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #PROTOCOLADO} <br>
 * {@link #CANCELADO} <br>
 * {@link #ARQUIVADO} <br>
 * {@link #ENCERRADO} <br>
 * {@link #CRIADO} <br>
 *
 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/11/2013 13:43:01
 */
public enum TipoPotencialPoluidor {

	/**
	 * Informa se o {@link Documento} foi protocolado
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/09/2013 14:44:06
	 */
	BAIXO("Baixo", 'B'),
	MEDIO("Médio", 'M'),
	ALTO("Alto", 'A');

	private String descricao;
	private char sigla;

	private TipoPotencialPoluidor(String descricao, char sigla) {
		this.descricao = descricao;
		this.sigla = sigla;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #BAIXO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/11/2013 13:51:12
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #BAIXO}, caso contrário retorna <code>false</code>
     */
    public boolean isBaixo() {

        if (name().equals(BAIXO.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #MEDIO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/11/2013 13:51:42
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #MEDIO}, caso contrário retorna <code>false</code>
     */
    public boolean isMedio() {

        if (name().equals(MEDIO.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ALTO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/11/2013 13:54:02
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ALTO}, caso contrário retorna <code>false</code>
     */
    public boolean isAlto() {

        if (name().equals(ALTO.name())){
            return true;
        }

        return false;
    }

    /**
     * Informa a {@link #descricao} de acordo com a sigla informada.
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/11/2013 13:58:17
     *
     * @param sigla
     * 		Caracter referente a sigla do Potencial Poluidor
     *
     * @return
     * 		Retorna a {@link #descricao} do {@link Enum}
     */
    public static String getDescricaoBySigla(char sigla){

    	for (TipoPotencialPoluidor potencialPoluidor : values()) {
			if (potencialPoluidor.sigla == sigla){
				return potencialPoluidor.descricao;
			}
		}

    	return "";
    }

}