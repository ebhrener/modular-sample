package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

import br.gov.ce.semace.erp.entity.julgamento.DespachoSaneador;
import br.gov.ce.semace.erp.entity.julgamento.Julgamento;

/**
 * {@link Enum} referente aos Status do {@link Julgamento}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #AGUARDANDO_ENVIO_AIF} <br>
 * {@link #AGUARDANDO_AR_AIF} <br>
 * {@link #AGUARDANDO_DEFESA} <br>
 * {@link #AGUARDANDO_DISTRIBUICAO_PARECER_INSTRUTORIO} <br>
 * {@link #AGUARDANDO_PARECER_INSTRUTORIO} <br>
 * {@link #ELABORANDO_PARECER_INSTRUTORIO} <br>
 * {@link #INSTRUCAO_CONCLUIDA} <br>
 * {@link #AGUARDANDO_AR_ALEGACAO} <br>
 * {@link #PERIODO_ALEGACAO} <br>
 * {@link #AGUARDANDO_DISTRIBUICAO_DECISAO_1_GRAU} <br>
 * {@link #AGUARDANDO_DECISAO_1_GRAU} <br>
 * {@link #ELABORANDO_DECISAO_1_GRAU} <br>
 * {@link #DECISAO_1_GRAU_CONCLUIDA} <br>
 * {@link #AGUARDANDO_AR_DECISAO_1_GRAU} <br>
 * {@link #JULGADO_E_TRANSITADO} <br>
 * {@link #SUSPENSO} <br>
 * {@link #AGUARDANDO_RECURSO} <br>
 * {@link #AGUARDANDO_DISTRIBUICAO_PARECER_RECURSAL} <br>
 * {@link #AGUARDANDO_PARECER_RECURSAL} <br>
 * {@link #ELABORANDO_PARECER_RECURSAL} <br>
 * {@link #PARECER_RECURSAL_FINALIZADO} <br>
 * {@link #ELABORANDO_DECISAO_2_GRAU} <br>
 * {@link #DECISAO_2_GRAU_CONCLUIDA} <br>
 * {@link #AGUARDANDO_AR_DECISAO_2_GRAU} <br>
 * {@link #JULGADO_EM_2_GRAU} <br>
 * {@link #AGUARDANDO_AIF_ENTREGUE_PESSOALMENTE} <br>
 * {@link #AGUARDANDO_PARECER_INSTRUTORIO_ENTREGUE_PESSOALMENTE} <br>
 * {@link #AGUARDANDO_DECISAO_ENTREGUE_PESSOALMENTE} <br>
 * {@link #AGUARDANDO_AIF_PUBLICADO_EDITAL} <br>
 * {@link #AGUARDANDO_PARECER_INSTRUTORIO_PUBLICADO_EDITAL} <br>
 * {@link #AGUARDANDO_DECISAO_PUBLICADA_EDITAL} <br>
 * {@link #PERIODO_RETRATACAO} <br>
 * {@link #ELABORANDO_RETRATACAO_DECISAO} <br>
 * 
 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 1, 2013 4:57:38 PM
 */
public enum StatusJulgamento {

	/**
	 * <b>Descrição: </b>Aguardando Envio do Auto de Infração <br>
	 * <b>Valor:</b> 0
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 1, 2013 4:57:33 PM
	 */
	AGUARDANDO_ENVIO_AIF("Aguardando Envio do Auto de Infração", 0, FaseJulgamento.DEFESA),

	/**
	 * <b>Descrição: </b>Aguardando AR do Auto de Infração Defesa <br>
	 * <b>Valor:</b> 1
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 8:30:48 AM
	 */
	AGUARDANDO_AR_AIF("Aguardando AR do Auto de Infração Defesa", 1, FaseJulgamento.DEFESA),

	/**
	 * <b>Descrição: </b>Aguardando Apresentação de Defesa<br>
	 * <b>Valor:</b> 2
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 1:32:43 PM
	 */
	AGUARDANDO_DEFESA("Aguardando Apresentação de Defesa", 2, FaseJulgamento.DEFESA),

	/**
	 * <b>Descrição: </b>Aguardando Distribuição - Parecer Instrutório <br>
	 * <b>Valor:</b> 3
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 1:36:42 PM
	 */
	AGUARDANDO_DISTRIBUICAO_PARECER_INSTRUTORIO("Aguardando Distribuição - Parecer Instrutório", 3, FaseJulgamento.INSTRUCAO),

	/**
	 * <b>Descrição: </b>Aguardando Elaboração do Parecer Instrutório <br>
	 * <b>Valor:</b> 4
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 1:37:07 PM
	 */
	AGUARDANDO_PARECER_INSTRUTORIO("Aguardando Elaboração do Parecer Instrutório", 4, FaseJulgamento.INSTRUCAO),

	/**
	 * <b>Descrição: </b>Elaborando Parecer Instrutório <br>
	 * <b>Valor:</b> 5
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:12:43 PM
	 */
	ELABORANDO_PARECER_INSTRUTORIO("Elaborando Parecer Instrutório", 5, FaseJulgamento.INSTRUCAO),

	/**
	 * <b>Descrição: </b>Instrução Concluída <br>
	 * <b>Valor:</b> 6
	 * 
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 13/05/2013 16:18:17
	 */
	AGUARDANDO_COMUNICACAO_INSTRUCAO("Aguardando Comunicação - Parecer Instrutório", 6, FaseJulgamento.INSTRUCAO),

	/**
	 * <b>Descrição: </b>Aguardando AR - Alegação <br>
	 * <b>Valor:</b> 7
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:13:28 PM
	 */
	AGUARDANDO_AR_INSTRUCAO("Aguardando AR - Parecer Instrutório", 7, FaseJulgamento.INSTRUCAO),

	/**
	 * <b>Descrição: </b>Período de Alegação <br>DEFESA
	 * <b>Valor:</b> 8
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:14:14 PM
	 */
	PERIODO_ALEGACAO("Período de Alegação", 8, FaseJulgamento.ALEGACAO),

	/**
	 * <b>Descrição: </b>Aguardando Distribuição - Decisão de 1º Grau <br>
	 * <b>Valor:</b> 9
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:14:39 PM
	 */	
	AGUARDANDO_DISTRIBUICAO_DECISAO_1_GRAU("Aguardando Distribuição - Decisão de 1ª Instância", 9, FaseJulgamento.DECISAO_1_INSTANCIA),

	/**
	 * <b>Descrição: </b>Aguardando Decisão de 1º Grau <br>
	 * <b>Valor:</b> 10
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:16:16 PM
	 */	
	AGUARDANDO_DECISAO_1_GRAU("Aguardando Decisão de 1ª Instância", 10, FaseJulgamento.DECISAO_1_INSTANCIA),

	/**
	 * <b>Descrição: </b>Elaborando Decisão de 1º Grau <br>
	 * <b>Valor:</b> 11
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:16:19 PM
	 */	
	ELABORANDO_DECISAO_1_GRAU("Elaborando Decisão de 1ª Instância", 11, FaseJulgamento.DECISAO_1_INSTANCIA),

	/**
	 * <b>Descrição: </b>Decisão de 1º Grau Concluída <br>
	 * <b>Valor:</b> 12
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:16:23 PM
	 */	
	DECISAO_1_GRAU_CONCLUIDA("Decisão de 1ª Instância Concluída", 12, FaseJulgamento.DECISAO_1_INSTANCIA),

	/**
	 * <b>Descrição: </b>Aguardando AR - Decisão 1º Grau <br>
	 * <b>Valor:</b> 13
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:16:27 PM
	 */	
	AGUARDANDO_AR_DECISAO_1_GRAU("Aguardando AR - Decisão 1ª Instância", 13, FaseJulgamento.DECISAO_1_INSTANCIA),

	/**
	 * <b>Descrição: </b>Julgado e Transitado <br>
	 * <b>Valor:</b> 14
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:16:50 PM
	 */	
	TRANSITADO_EM_JULGADO("Transitado em Julgado", 14, FaseJulgamento.TRANSITADO_EM_JULGADO),

	/**
	 * <b>Descrição: </b>Suspenso<br>
	 * <b>Valor:</b> 15
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:16:50 PM
	 */	
	SUSPENSO("Suspenso", 15, FaseJulgamento.SUSPENSO),
	
	/**
	 * <b>Descrição: </b>Aguardando Recurso <br>
	 * <b>Valor:</b> 16
	 *
	 * <br><b>TODO:</b> Avaliar os status de Recurso quando a instrução normativa for publicada
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:22:13 PM
	 */
	AGUARDANDO_RECURSO("Aguardando Apresentação de Recurso", 16, FaseJulgamento.RECURSO),

	/**
	 * <b>Descrição: </b>Aguardando Auto de Infração Entregue Pessoalmente <br>
	 * <b>Valor:</b> 17
	 * 
	 * @author Saulo Fernandes
	 */	
	AGUARDANDO_AIF_ENTREGUE_PESSOALMENTE("Aguardando Auto de Infração Entregue Pessoalmente", 17, FaseJulgamento.DEFESA),

	/**
	 * <b>Descrição: </b>Aguardando Parecer Instrutorio Entregue Pessoalmente <br>
	 * <b>Valor:</b> 18
	 * 
	 * @author Saulo Fernandes
	 */
	AGUARDANDO_PARECER_INSTRUTORIO_ENTREGUE_PESSOALMENTE("Aguardando Confirmação do Parecer Instrutório Entregue Pessoalmente", 18, FaseJulgamento.INSTRUCAO),

	/**
	 * <b>Descrição: </b>Aguardando Decisão de Julgamento Entregue Pessoalmente <br>
	 * <b>Valor:</b> 19
	 * 
	 * @author Saulo Fernandes
	 */
	AGUARDANDO_DECISAO_ENTREGUE_PESSOALMENTE("Aguardando Decisão de Julgamento Entregue Pessoalmente", 19, FaseJulgamento.DECISAO_1_INSTANCIA),

	/**
	 * <b>Descrição: </b>Aguardando Auto de Infração Publicado em Edital <br>
	 * <b>Valor:</b> 20
	 * 
	 * @author Saulo Fernandes
	 */
	AGUARDANDO_AIF_PUBLICADO_EDITAL("Aguardando Auto de Infração Publicado em Edital", 20, FaseJulgamento.DEFESA),

	/**
	 * <b>Descrição: </b>Aguardando Parecer Instrutório Publicado em Edital <br>
	 * <b>Valor:</b> 21
	 * 
	 * @author Saulo Fernandes
	 */
	AGUARDANDO_PARECER_INSTRUTORIO_PUBLICADO_EDITAL("Aguardando Parecer Instrutório Publicado em Edital", 21, FaseJulgamento.INSTRUCAO),

	/**
	 * <b>Descrição: </b>Aguardando Decisão Publicada em Edital <br>
	 * <b>Valor:</b> 22
	 * 
	 * @author Saulo Fernandes
	 */
	AGUARDANDO_DECISAO_PUBLICADA_EDITAL("Aguardando Decisão Publicada em Edital", 22, FaseJulgamento.DECISAO_1_INSTANCIA),
	
	/**
	 * <b>Descrição: </b>Período de Retratação<br>
	 * <b>Valor:</b> 23
	 * 
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 09/05/2013 08:20:20
	 */
	PERIODO_RETRATACAO("Período de Reconsideração", 23, FaseJulgamento.RETRATACAO),
	
	/**
	 * <b>Descrição: </b>Elaborando Retratação<br>
	 * <b>Valor:</b> 24
	 * 
	 * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 09/05/2013 13:58:12
	 */
	ELABORANDO_RETRATACAO("Elaborando Reconsideração", 24, FaseJulgamento.RETRATACAO),

	/**
	 * <b>Descrição: </b>Aguardando AR Retratação<br>
	 * <b>Valor:</b> 25
	 * 
	 * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 15/05/2013 16:49:34
	 */
	AGUARDANDO_AR_RETRATACAO("Aguardando AR de Reconsideração", 25, FaseJulgamento.RETRATACAO),
	
	/**
	 * <b>Descrição: </b>Retratação concluída<br>
	 * <b>Valor:</b> 26
	 * 
	 * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 16/05/2013 08:36:05
	 */
	RETRATACAO_CONCLUIDA("Reconsideração Concluída", 26, FaseJulgamento.RETRATACAO),
	
	/**
	 * <b>Descrição: </b>Retratação entregue pessoalmente<br>
	 * <b>Valor:</b> 27
	 * 
	 * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 16/05/2013 15:11:51
	 */
	AGUARDANDO_RETRATACAO_ENTREGUE_PESSOALMENTE("Aguardando Reconsideração Entregue Pessoalmente", 27, FaseJulgamento.RETRATACAO),
	
	/**
	 * <b>Descrição: </b>Retratação publicada em edital <br>
	 * <b>Valor:</b> 28
	 * 
	 * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 16/05/2013 15:12:07
	 */
	AGUARDANDO_RETRATACAO_PUBLICADA_EDITAL("Reconsideração Publicada em Edital", 28, FaseJulgamento.RETRATACAO),
	
	/**
	 * <b>Descrição: </b>Parecer Instrutório Concluído<br>
	 * <b>Valor:</b> 29
	 * 
	 */
	PARECER_CONCLUIDO("Parecer Instrutório Concluído", 29, FaseJulgamento.INSTRUCAO),
	
	/**
	 * <b>Descrição: </b>Aguardando Comunicação - Decisão Em 1ª Instância<br>
	 * <b>Valor:</b> 30
	 * 
	 */
	AGUARDANDO_COMUNICACAO_DECISAO_1_INSTANCIA("Aguardando Comunicação - Decisão Em 1ª Instância", 30, FaseJulgamento.DECISAO_1_INSTANCIA),

	/**
	 * <b>Descrição: </b>Julgado em 1ª Instância<br>
	 * <b>Valor:</b> 31
	 * 
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 15/06/2013 13:15:07
	 */
	JULGADO_1_INSTANCIA("Julgado em 1ª Instância", 31, FaseJulgamento.DECISAO_1_INSTANCIA),
	
	/**
	 * <b>Descrição: </b>Aguardando Comunicação da Retratação<br>
	 * <b>Valor:</b> 32
	 * 
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 15/06/2013 13:37:55
	 */
	AGUARDANDO_COMUNICACAO_RETRATACAO("Aguardando Comunicação da Reconsideração", 32, FaseJulgamento.RETRATACAO);
	
    /**
     * Descrição do {@link Enum}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:40:20 PM
     */
	private String descricao;
	
	/**
	 * Valor do {@link Enum}
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:39:59 PM
	 */
	private Integer value;
	
	private FaseJulgamento faseJulgamento;

	 /**
     * Construtor com os parâmetros descricao e valor.
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 2:40:49 PM
     * 
     * @param description
     *            Parâmetro a ser setado no campo <code>{@link #descricao} </code> do {@link Enum}
     * 
     * @param value
     *            Parâmetro a ser setado no campo <code> {@link #value} </code> do {@link Enum}
     */
	private StatusJulgamento(String description, Integer value, FaseJulgamento faseJulgamento) {
		this.descricao = description;
		this.value = value;
		this.faseJulgamento = faseJulgamento;
	}

	public String getDescricao() {
		return this.descricao;
	}
	
	public Integer getValue() {
		return this.value;
	}
	
	public FaseJulgamento getFaseJulgamento() {
		return faseJulgamento;
	}

    public static StatusJulgamento fromName(final String name){
    	return valueOf(StatusJulgamento.class, name);
    }
	
	/**
	 * Verifica se o {@link #name()} do {@link Enum} é
	 * {@link #AGUARDANDO_ENVIO_AIF}
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 1, 2013
	 *         4:32:07 PM
	 * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_ENVIO_AIF}, caso contrário retorna <code>false</code>
	 */
    public boolean isAguardandoEnvioAIF() {

    	if (name().equals(AGUARDANDO_ENVIO_AIF.name())){
            return true;
    	}

        return false;
    }
    
    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_AR_AIF}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 1, 2013 4:32:07 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_AR_AIF}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoArAifDefesa() {

        if (name().equals(AGUARDANDO_AR_AIF.name())){
            return true;
        }

        return false;
    }
    
    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_DEFESA}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:29:26 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_DEFESA}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoApresentacaoDefesa() {

        if (name().equals(AGUARDANDO_DEFESA.name())){
            return true;
        }

        return false;
    }
    
    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_DISTRIBUICAO_PARECER_INSTRUTORIO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:30:47 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_DISTRIBUICAO_PARECER_INSTRUTORIO}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoDistribuicaoParecerInstrutorio() {

        if (name().equals(AGUARDANDO_DISTRIBUICAO_PARECER_INSTRUTORIO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_PARECER_INSTRUTORIO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:31:30 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_PARECER_INSTRUTORIO}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoElaboracaoParecerInstrutorio() {

        if (name().equals(AGUARDANDO_PARECER_INSTRUTORIO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ELABORANDO_PARECER_INSTRUTORIO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:32:38 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ELABORANDO_PARECER_INSTRUTORIO}, caso contrário retorna <code>false</code>
     */
    public boolean isElaborandoParecerInstrutorio() {

        if (name().equals(ELABORANDO_PARECER_INSTRUTORIO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_COMUNICACAO__ALEGACAO}
     * 
     * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 13/05/2013 14:47:59
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_COMUNICACAO__ALEGACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoComunicacaoAlegacao() {

        if (name().equals(AGUARDANDO_COMUNICACAO_INSTRUCAO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_AR_ALEGACAO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:34:02 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_AR_ALEGACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoArAlegacao() {

        if (name().equals(AGUARDANDO_AR_INSTRUCAO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PERIODO_ALEGACAO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:35:05 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PERIODO_ALEGACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isPeriodoAlegacao() {

        if (name().equals(PERIODO_ALEGACAO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_DISTRIBUICAO_DECISAO_1_GRAU}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:35:35 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_DISTRIBUICAO_DECISAO_1_GRAU}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoDistribuicaoDecisao() {

        if (name().equals(AGUARDANDO_DISTRIBUICAO_DECISAO_1_GRAU.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_DECISAO_1_GRAU}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:36:43 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_DECISAO_1_GRAU}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoDecisao1Grau() {

        if (name().equals(AGUARDANDO_DECISAO_1_GRAU.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ELABORANDO_DECISAO_1_GRAU}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:37:50 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ELABORANDO_DECISAO_1_GRAU}, caso contrário retorna <code>false</code>
     */
    public boolean isElaborandoDecisao1Grau() {

        if (name().equals(ELABORANDO_DECISAO_1_GRAU.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #DECISAO_1_GRAU_CONCLUIDA}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:38:31 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #DECISAO_1_GRAU_CONCLUIDA}, caso contrário retorna <code>false</code>
     */
    public boolean isDecisao1GrauConcluida() {

        if (name().equals(DECISAO_1_GRAU_CONCLUIDA.name())){
            return true;
        }

        return false;
    }
    
    /**
     * Verifica se o {@link #name()} do {@link Enum}} é {@link #JULGADO_1_INSTANCIA}
     * 
     * @return
     * 	Verdadeiro caso seja {@link #JULGADO_1_INSTANCIA}
     */
    public boolean isDecisao1Grau() {

        if (name().equals(JULGADO_1_INSTANCIA.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_AR_DECISAO_1_GRAU}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:42:35 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_AR_DECISAO_1_GRAU}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoArDecisao1Grau() {

        if (name().equals(AGUARDANDO_AR_DECISAO_1_GRAU.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #JULGADO_E_TRANSITADO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:43:29 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #JULGADO_E_TRANSITADO}, caso contrário retorna <code>false</code>
     */
    public boolean isJulgadoETransitado() {

        if (name().equals(TRANSITADO_EM_JULGADO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #SUSPENSO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:43:52 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #SUSPENSO}, caso contrário retorna <code>false</code>
     */
    public boolean isSuspenso() {

        if (name().equals(SUSPENSO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_RECURSO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 2, 2013 4:44:23 PM
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_RECURSO}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoRecurso() {

        if (name().equals(AGUARDANDO_RECURSO.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_AIF_ENTREGUE_PESSOALMENTE}
     * 
     * @author Saulo Fernandes
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_AIF_ENTREGUE_PESSOALMENTE}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoAifEntreguePessoalmente() {

        if (name().equals(AGUARDANDO_AIF_ENTREGUE_PESSOALMENTE.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_PARECER_INSTRUTORIO_ENTREGUE_PESSOALMENTE}
     * 
     * @author Saulo Fernandes
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_PARECER_INSTRUTORIO_ENTREGUE_PESSOALMENTE}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoParecerInstrutorioEntreguePessoalmente() {

        if (name().equals(AGUARDANDO_PARECER_INSTRUTORIO_ENTREGUE_PESSOALMENTE.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_DECISAO_ENTREGUE_PESSOALMENTE}
     * 
     * @author Saulo Fernandes
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_DECISAO_ENTREGUE_PESSOALMENTE}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoDecisaoEntreguePessoalmente() {

        if (name().equals(AGUARDANDO_DECISAO_ENTREGUE_PESSOALMENTE.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_AIF_PUBLICADO_EDITAL}
     * 
     * @author Saulo Fernandes
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_AIF_PUBLICADO_EDITAL}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoAifPublicadoEdital() {

        if (name().equals(AGUARDANDO_AIF_PUBLICADO_EDITAL.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_PARECER_INSTRUTORIO_PUBLICADO_EDITAL}
     * 
     * @author Saulo Fernandes
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_PARECER_INSTRUTORIO_PUBLICADO_EDITAL}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoParecerInstrutorioPublicadoEdital() {

        if (name().equals(AGUARDANDO_PARECER_INSTRUTORIO_PUBLICADO_EDITAL.name())){
            return true;
        }

        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_DECISAO_PUBLICADA_EDITAL}
     * 
     * @author Saulo Fernandes
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_DECISAO_PUBLICADA_EDITAL}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoDecisaoPublicadaEdital() {

        if (name().equals(AGUARDANDO_DECISAO_PUBLICADA_EDITAL.name())){
            return true;
        }

        return false;
    }
    
    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PERIODO_RETRATACAO}
     * 
     * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 09/05/2013 08:25:54
     * 
     * @return
     * 		retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PERIODO_RETRATACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isPeriodoRetratacao() {
    	
        if (name().equals(PERIODO_RETRATACAO.name())) {
            return true;
        }

        return false;
    }
    
    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ELABORANDO_RETRATACAO_DECISAO}
     * 
     * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 09/05/2013 14:01:04
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ELABORANDO_RETRATACAO_DECISAO}, caso contrário retorna <code>false</code>
     */
    public boolean isElaborandoRetratacao() {

        if (name().equals(ELABORANDO_RETRATACAO.name())){
            return true;
        }

        return false;
    }
    
    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_AR_RETRATACAO}
     * 
     * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 15/05/2013 16:51:28
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_AR_RETRATACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoArRetratacao() {

        if (name().equals(AGUARDANDO_AR_RETRATACAO.name())){
            return true;
        }

        return false;
    }
    
    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #RETRATACAO_CONCLUIDA}
     * 
     * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 16/05/2013 08:38:20
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #RETRATACAO_CONCLUIDA}, caso contrário retorna <code>false</code>
     */
    public boolean isRetratacaoConcluida() {

        if (name().equals(RETRATACAO_CONCLUIDA.name())){
            return true;
        }

        return false;
    }
    
    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #RETRATACAO_CONCLUIDA}
     * 
     * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 16/05/2013 08:38:20
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #RETRATACAO_CONCLUIDA}, caso contrário retorna <code>false</code>
     */
    public boolean isParecerConcluido() {

        if (name().equals(PARECER_CONCLUIDO.name())){
            return true;
        }

        return false;
    }
    
    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_RETRATACAO_ENTREGUE_PESSOALMENTE}
     * 
     * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 15/06/2013 13:31:15
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_RETRATACAO_ENTREGUE_PESSOALMENTE}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoRetratacaoEntreguePessoalmente() {
        if (name().equals(AGUARDANDO_RETRATACAO_ENTREGUE_PESSOALMENTE.name())) {
            return true;
        }
        return false;
    }
    
	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_RETRATACAO_PUBLICADA_EDITAL}
     * 
     * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 15/06/2013 13:35:57
     * 
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_RETRATACAO_PUBLICADA_EDITAL}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoRetratacaoPublicadaEdital() {

        if (name().equals(AGUARDANDO_RETRATACAO_PUBLICADA_EDITAL.name())){
            return true;
        }

        return false;
    }
    
    /**
     * Informa os {@link StatusJulgamento} configurados para a ação de <b>Comunicação</b> do {@link Julgamento}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 11, 2013 11:03:53 AM
     * 
     * @return
     * 		Retorna uma {@link List}a com os Status:
     * 
     *  <br>{@link #AGUARDANDO_ENVIO_AIF} <br>
     *  <br>{@link #AGUARDANDO_COMUNICACAO_ALEGACAO} <br>
     * 	<br>{@link #DECISAO_1_GRAU_CONCLUIDA} <br>
     */
    public static List<StatusJulgamento> getStatusAcaoComunicacao(){

    	List<StatusJulgamento> retorno = new ArrayList<StatusJulgamento>();

    	retorno.add(AGUARDANDO_ENVIO_AIF);
    	retorno.add(AGUARDANDO_COMUNICACAO_INSTRUCAO);
    	retorno.add(DECISAO_1_GRAU_CONCLUIDA);

    	return retorno; 
    }

    /**
     * Informa os {@link StatusJulgamento} configurados para a ação de <b>Estender Prazo de Defesa</b> do {@link Julgamento}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 11, 2013 11:02:11 AM
     * 
     * @return
     * 		Retorna uma {@link List}a com os Status:
     * 
     *  <br>{@link #AGUARDANDO_DEFESA} <br>
     * 		{@link #AGUARDANDO_DISTRIBUICAO_PARECER_INSTRUTORIO} 
     */
    public static List<StatusJulgamento> getStatusAcaoEstenderPrazoDefesa(){

    	List<StatusJulgamento> retorno = new ArrayList<StatusJulgamento>();

    	retorno.add(AGUARDANDO_DEFESA);
    	retorno.add(AGUARDANDO_DISTRIBUICAO_PARECER_INSTRUTORIO);

    	return retorno;
    }

    /**
     * Informa os {@link StatusJulgamento} configurados para a ação de <b>Estender Prazo de Alegação</b> do {@link Julgamento}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 11, 2013 11:01:42 AM
     * 
     * @return
     * 		Retorna uma {@link List}a com os Status:
     * 
     *  <br>{@link #PERIODO_ALEGACAO} <br>
     * 		{@link #AGUARDANDO_DISTRIBUICAO_DECISAO_1_GRAU} 
     */
    public static List<StatusJulgamento> getStatusAcaoEstenderPrazoAlegacao(){
    	
    	List<StatusJulgamento> retorno = new ArrayList<StatusJulgamento>();
    	
    	retorno.add(PERIODO_ALEGACAO);
    	retorno.add(AGUARDANDO_DISTRIBUICAO_DECISAO_1_GRAU);
    	
    	return retorno;
    }

    /**
     * Informa os {@link StatusJulgamento} configurados para a ação de <b>Finalizar Fase Decisão</b> do {@link Julgamento}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 11, 2013 11:00:58 AM
     * 
     * @author Saulo Fernandes [saulofernandes.ti@gmail.com] - 09/05/2013 13:10:13
     * 
     * @return
     * 		Retorna uma {@link List}a com os Status:
     * 
     *  <br>{@link #ELABORANDO_DECISAO_1_GRAU} <br>
     */
    public static List<StatusJulgamento> getStatusAcaoFinalizarFaseDecisao(){
    	
    	List<StatusJulgamento> retorno = new ArrayList<StatusJulgamento>();
    	
    	retorno.add(ELABORANDO_DECISAO_1_GRAU);
    	
    	return retorno;
    }

    /**
     * Informa os {@link StatusJulgamento} configurados para a ação de <b>Nova Decisão</b> do {@link Julgamento}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 11, 2013 10:58:45 AM
     * 
     * @author Saulo Fernandes [saulofernandes.ti@gmail.com] - 09/05/2013 11:37:29
     * 
     * @return
     * 		Retorna uma {@link List}a com os Status:
     * 
     *  <br>{@link #AGUARDANDO_DECISAO_1_GRAU} <br>
     */
    public static List<StatusJulgamento> getStatusAcaoNovaDecisao(){
    	
    	List<StatusJulgamento> retorno = new ArrayList<StatusJulgamento>();
    	
    	retorno.add(AGUARDANDO_DECISAO_1_GRAU);
    	
    	return retorno;
    }

    /**
     * Informa os {@link StatusJulgamento} configurados para a ação de <b>Despacho Saneador</b> (ver {@link DespachoSaneador}) do {@link Julgamento}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - May 3, 2013 11:54:06 AM
     * 
     * @return
     * 		Retorna uma {@link List}a com os Status:
     * 
     *  <br>{@link #AGUARDANDO_DECISAO_1_GRAU} <br>
     * 		{@link #ELABORANDO_DECISAO_1_GRAU}
     */
    public static List<StatusJulgamento> getStatusAcaoDespachoSaneador(){
    	
    	List<StatusJulgamento> retorno = new ArrayList<StatusJulgamento>();
    	
    	retorno.add(AGUARDANDO_DECISAO_1_GRAU);
    	retorno.add(ELABORANDO_DECISAO_1_GRAU);
    	
    	return retorno;
    }
    
	public static List<StatusJulgamento> acaoComunicacaoLiberada() {
		List<StatusJulgamento> retorno = new ArrayList<StatusJulgamento>();
		retorno.add(StatusJulgamento.AGUARDANDO_ENVIO_AIF);
		retorno.add(StatusJulgamento.PARECER_CONCLUIDO);
		retorno.add(StatusJulgamento.AGUARDANDO_COMUNICACAO_INSTRUCAO);
		retorno.add(StatusJulgamento.DECISAO_1_GRAU_CONCLUIDA);
		retorno.add(StatusJulgamento.AGUARDANDO_COMUNICACAO_DECISAO_1_INSTANCIA);
		retorno.add(StatusJulgamento.RETRATACAO_CONCLUIDA);
		retorno.add(StatusJulgamento.AGUARDANDO_COMUNICACAO_RETRATACAO);
		return retorno;
	}

	 /**
     * Verifica se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é {@link FaseJulgamento#DEFESA}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18/06/2013 16:03:17
     * 
     * @see FaseJulgamento#isFaseDefesa()
     * 
	 * @return Retorna <code>true</code> se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é
	 *         {@link FaseJulgamento#DEFESA}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseDefesa() {
        return this.faseJulgamento.isFaseDefesa();
    }

	 /**
     * Verifica se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é {@link FaseJulgamento#ALEGACAO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18/06/2013 16:10:06
     * 
     * @see FaseJulgamento#isFaseAlegacao()
     * 
	 * @return Retorna <code>true</code> se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é
	 *         {@link FaseJulgamento#ALEGACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseAlegacao() {
        return this.faseJulgamento.isFaseAlegacao();
    }

	 /**
     * Verifica se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é {@link FaseJulgamento#DECISAO_1_INSTANCIA}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18/06/2013 16:10:49
     * 
     * @see FaseJulgamento#isFaseDecisao1Instancia()
     * 
	 * @return Retorna <code>true</code> se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é
	 *         {@link FaseJulgamento#DECISAO_1_INSTANCIA}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseDecisao1Instancia() {
        return this.faseJulgamento.isFaseDecisao1Instancia();
    }

	 /**
     * Verifica se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é {@link FaseJulgamento#DECISAO_2_INSTANCIA}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18/06/2013 16:12:13
     * 
     * @see FaseJulgamento#isFaseDecisao2Instancia()
     * 
	 * @return Retorna <code>true</code> se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é
	 *         {@link FaseJulgamento#DECISAO_2_INSTANCIA}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseDecisao2Instancia() {
        return this.faseJulgamento.isFaseDecisao2Instancia();
    }

	 /**
     * Verifica se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é {@link FaseJulgamento#INSTRUCAO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18/06/2013 16:12:47
     * 
     * @see FaseJulgamento#isFaseInstrucao()
     * 
	 * @return Retorna <code>true</code> se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é
	 *         {@link FaseJulgamento#INSTRUCAO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseInstrucao() {
        return this.faseJulgamento.isFaseInstrucao();
    }

	 /**
     * Verifica se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é {@link FaseJulgamento#RETRATACAO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18/06/2013 16:13:33
     * 
     * @see FaseJulgamento#isFaseRetratacao()
     * 
	 * @return Retorna <code>true</code> se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é
	 *         {@link FaseJulgamento#RETRATACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseRetratacao() {
        return this.faseJulgamento.isFaseRetratacao();
    }

	 /**
     * Verifica se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é {@link FaseJulgamento#SUSPENSO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18/06/2013 16:13:57
     * 
     * @see FaseJulgamento#isFaseSuspenso()
     * 
	 * @return Retorna <code>true</code> se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é
	 *         {@link FaseJulgamento#SUSPENSO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseSuspenso() {
        return this.faseJulgamento.isFaseSuspenso();
    }

	 /**
     * Verifica se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é {@link FaseJulgamento#TRANSITADO_EM_JULGADO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18/06/2013 16:14:22
     * 
     * @see FaseJulgamento#isFaseTransitadoEmJulgado()
     * 
	 * @return Retorna <code>true</code> se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é
	 *         {@link FaseJulgamento#TRANSITADO_EM_JULGADO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseTransitadoEmJulgado() {
        return this.faseJulgamento.isFaseTransitadoEmJulgado();
    }

	 /**
     * Verifica se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é {@link FaseJulgamento#RECURSO}
     * 
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18/06/2013 16:17:11
     * 
     * @see FaseJulgamento#isFaseRecurso()
     * 
	 * @return Retorna <code>true</code> se a propriedade {@link #faseJulgamento} do {@link Enum} {@link StatusJulgamento} é
	 *         {@link FaseJulgamento#RECURSO}, caso contrário retorna <code>false</code>
     */
    public boolean isFaseRecurso() {
        return this.faseJulgamento.isFaseRecurso();
    }
    
}
