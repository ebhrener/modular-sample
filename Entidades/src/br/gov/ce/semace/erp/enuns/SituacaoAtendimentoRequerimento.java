package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.AgendamentoRequerimento;

/**
 * {@link Enum} referente as fases dos Status de Atendimento do {@link AgendamentoRequerimento}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #ATENDIDO} <br>
 * {@link #CANCELADO} <br>
 * {@link #EM_ATENDIMENTO} <br>
 * {@link #PENDENTE} <br>
 *
 * @author rodrigo.silva - Rodrigo Silva Oliveira - 31/07/2018 10:25:59
 */
public enum SituacaoAtendimentoRequerimento {

	/*0*/ ATENDIDO("Atendido"),
	/*1*/ CANCELADO("Cancelado"),
	/*2*/ EM_ATENDIMENTO("Em Atendimento"),
	/*3*/ PENDENTE("Pendente");

	private String descricao;

	private SituacaoAtendimentoRequerimento(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ATENDIDO}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ATENDIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isAtendido() {
        if (name().equals(ATENDIDO.name())) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADO}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADO}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelado() {
        if (name().equals(CANCELADO.name())) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EM_ATENDIMENTO}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EM_ATENDIMENTO}, caso contrário retorna <code>false</code>
     */
    public boolean isEmAtendimento() {
        if (name().equals(EM_ATENDIMENTO.name())) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PENDENTE}
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PENDENTE}, caso contrário retorna <code>false</code>
     */
    public boolean isPendente() {
        if (name().equals(PENDENTE.name())) {
            return true;
        }
        return false;
    }

    public static SituacaoAtendimentoRequerimento fromInt(int valor) {
		for (SituacaoAtendimentoRequerimento situacao : SituacaoAtendimentoRequerimento.values()) {
			if (situacao.ordinal() == valor) {
				return situacao;
			}
		}
		return null;
	}

	public static List<SituacaoAtendimentoRequerimento> valuesOrderedByDescricao() {
		List<SituacaoAtendimentoRequerimento> list =  Arrays.asList(SituacaoAtendimentoRequerimento.values());
		Collections.sort(list, new Comparator<SituacaoAtendimentoRequerimento>() {

			@Override
			public int compare(SituacaoAtendimentoRequerimento o1, SituacaoAtendimentoRequerimento o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}
}