package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum StatusOrdemFiscalizacao {
	REGISTRADO("Registrado"),
	EM_FISCALIZACAO("Em Fiscalização"),
	PROCESSO_CONCLUSAO("Processo de Conclusão"),
	CONCLUIDO("Concluído"),
	CANCELADA("Cancelada");

	private String descricao;

	StatusOrdemFiscalizacao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	@Override
	public String toString() {
		return getDescricao();
	}

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 06/01/2016 16:52:23
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADA}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelada() {

        if (this.name().equals(CANCELADA.name())){
            return true;
        }

        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #REGISTRADO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 26/01/2016 11:53:07
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #REGISTRADO}, caso contrário retorna <code>false</code>
     */
    public boolean isRegistrado() {

        if (this.name().equals(REGISTRADO.name())){
            return true;
        }

        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CONCLUIDO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 01/02/2016 10:45:42
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CONCLUIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isConcluido() {

        if (this.name().equals(CONCLUIDO.name())){
            return true;
        }

        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PROCESSO_CONCLUSAO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 12/02/2016 14:49:49
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PROCESSO_CONCLUSAO}, caso contrário retorna <code>false</code>
     */
    public boolean isProcessoConclusao() {

        if (this.name().equals(PROCESSO_CONCLUSAO.name())){
            return true;
        }

        return false;
    }

	 /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #EM_FISCALIZACAO}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 01/06/2016 20:49:44
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #EM_FISCALIZACAO}, caso contrário retorna <code>false</code>
     */
    public boolean isEmFiscalizacao() {

        if (this.name().equals(EM_FISCALIZACAO.name())){
            return true;
        }

        return false;
    }

    /**
     * {@link List}a com os possíveis {@link StatusOrdemFiscalizacao} para o qual o usuário poderá
     * alterar:
     * 	<ul>{@link #REGISTRADO}</ul>
     * 	<ul>{@link #EM_FISCALIZACAO}</ul>
     * 	<ul>{@link #PROCESSO_CONCLUSAO}</ul>
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 04/02/2016 13:45:29
     *
     * @return
     * 		{@link List}a de {@link StatusOrdemFiscalizacao}
     */
   public static List<StatusOrdemFiscalizacao> getListStatusModificarStatusOrdemFiscalizacao(){
	   List<StatusOrdemFiscalizacao> retorno = new ArrayList<StatusOrdemFiscalizacao>();

	   retorno.add(REGISTRADO);
	   retorno.add(EM_FISCALIZACAO);
	   retorno.add(PROCESSO_CONCLUSAO);

	   return retorno;
    }

	public static List<StatusOrdemFiscalizacao> valuesOrderedByDescricao(){
		List<StatusOrdemFiscalizacao> list =  Arrays.asList(StatusOrdemFiscalizacao.values());
		Collections.sort(list, new Comparator<StatusOrdemFiscalizacao>() {

			@Override
			public int compare(StatusOrdemFiscalizacao o1, StatusOrdemFiscalizacao o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

   public static List<StatusOrdemFiscalizacao> getListStatusValidosOrdemFiscalizacao(){
	   List<StatusOrdemFiscalizacao> retorno = new ArrayList<StatusOrdemFiscalizacao>();

	   retorno.add(REGISTRADO);
	   retorno.add(EM_FISCALIZACAO);
	   retorno.add(PROCESSO_CONCLUSAO);
	   retorno.add(CONCLUIDO);

	   return retorno;
    }

	public static int[] getArrayOrdinal(List<StatusOrdemFiscalizacao> list){

		int[] retorno = new int[list.size()];
		int i = 0;
		for (StatusOrdemFiscalizacao statusOrdemFiscalizacao : list) {
			retorno[i] = statusOrdemFiscalizacao.ordinal();
			i++;
		}
		return retorno;
	}

	public static StatusOrdemFiscalizacao fromInt(int valor) {
		for (StatusOrdemFiscalizacao statusOrdemFiscalizacao : StatusOrdemFiscalizacao.values()) {
			if (statusOrdemFiscalizacao.ordinal() == valor){
				return statusOrdemFiscalizacao;
			}
		}
		return null;
	}
}