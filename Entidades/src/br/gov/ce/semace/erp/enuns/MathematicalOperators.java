package br.gov.ce.semace.erp.enuns;


public enum MathematicalOperators {

	LESS_THAN("Menor", "<","60", "003C", "/images/60.png"),
	GREATER_THAN("Maior", ">","62", "003E", "/images/62.png"),
	LESS_THAN_OR_EQUAL_TO("Menor ou Igual", "≤","8804", "2264", "/images/8804.png"),
	GREATER_THAN_OR_EQUAL_TO("Maior ou Igual", "≥","8805", "2265", "/images/8805.png");

	private String name;
	private String symbol;
	private String decimal;
	private String hexaDecimal;
	private String img;
	
	private MathematicalOperators(String name, String symbol, String decimal,
			String hexaDecimal, String img) {
		this.name = name;
		this.symbol = symbol;
		this.decimal = decimal;
		this.hexaDecimal = hexaDecimal;
		this.img = img;
	}

	public String getName() {
		return name;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getDecimal() {
		return decimal;
	}

	public String getHexaDecimal() {
		return hexaDecimal;
	}

	public String getImg() {
		return img;
	}

}
