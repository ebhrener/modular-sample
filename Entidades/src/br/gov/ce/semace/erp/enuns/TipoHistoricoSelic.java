package br.gov.ce.semace.erp.enuns;

/**
 * Tipos de Histórico IGP-DI
 * 
 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 28/03/2014
 *
 */
public enum TipoHistoricoSelic {
	
	INSERT("Cadastro"),
	UPDATE("Atualização"),
	DELETE("Removido");
	
	private String descricao;
	
	private TipoHistoricoSelic(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
