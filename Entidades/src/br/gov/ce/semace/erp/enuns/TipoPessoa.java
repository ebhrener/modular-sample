package br.gov.ce.semace.erp.enuns;

import br.gov.ce.semace.erp.enuns.TipoPessoa;


public enum TipoPessoa {

	SELECIONE (-1),FISICA(0), JURIDICA(1), TECNICO(2);

	Integer id;


	TipoPessoa(Integer id){
		this.id = id;
	}

	public static TipoPessoa fromInt(int valor) {
		switch (valor) {
			case -1: return SELECIONE;
			case 0: return FISICA;
			case 1: return JURIDICA;
			default: return null;
		}
	}


	@Override
	public String toString() {
		switch (this) {
			case SELECIONE: return "Selecione";
			case FISICA: return "Física";
			case JURIDICA: return "Jurídica";
			default: return null;
		}
	}

	public Integer getId() {
		return id;
	}

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #FISICA}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 29/07/2013 11:42:19
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #FISICA}, caso contrário retorna <code>false</code>
     */
	public boolean isPessoaFisica() {

		if (name().equals(FISICA.name())) {
			return true;
		}

		return false;
	}

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #JURIDICA}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 29/07/2013 11:45:38
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #JURIDICA}, caso contrário retorna <code>false</code>
     */
	public boolean isPessoaJuridica() {

		if (name().equals(JURIDICA.name())) {
			return true;
		}

		return false;
	}

}
