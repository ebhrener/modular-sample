package br.gov.ce.semace.erp.enuns;

public enum TipoTermoParcelamento {
	
	PARCELAMENTO_COMPENSACAO_AMBIENTAL,
	PARCELAMENTO_AUTO_INFRACAO,
	PARCELAMENTO_DIVIDA_ATIVA;

}
