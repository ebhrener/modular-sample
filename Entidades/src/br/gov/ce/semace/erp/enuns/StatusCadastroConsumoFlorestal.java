package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum StatusCadastroConsumoFlorestal {

	VIGENTE("Vigente"), // #0
	VENCIDO("Vencido"), // #1
	AGUARDANDO_PAGAMENTO("Aguardando Pagamento"), // #2
	CANCELADO_FALTA_PAGAMENTO("Cancelado por Falta de Pagamento"), // #3
	CANCELADO("Cancelado"), // #4
	PAGO("Pago"), // #5
	AGUARDANDO_EMISSAO_DAE("Aguardando Emissão do DAE"), // #6
	CADASTRADO("Cadastrado"), // #7
	AGUARDANDO_CANCELAMENTO("Aguardando Cancelamento"), // #8
	NAO_CONCLUIDO("Não Concluído"); // #9

	private StatusCadastroConsumoFlorestal(String descricao) {
		this.descricao = descricao;
	}

	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #VIGENTE}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #VIGENTE}, caso contrário retorna <code>false</code>
     */
    public boolean isVigente() {
        if (name().equals(VIGENTE.name())){
            return true;
        }
        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #VENCIDO}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #VENCIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isVencido() {
        if (name().equals(VENCIDO.name())){
            return true;
        }
        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_PAGAMENTO}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_PAGAMENTO}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoPagamento() {
        if (name().equals(AGUARDANDO_PAGAMENTO.name())){
            return true;
        }
        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADO_FALTA_PAGAMENTO}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADO_FALTA_PAGAMENTO}, caso contrário retorna <code>false</code>
     */
    public boolean isCanceladoFaltaPagamento() {
        if (name().equals(CANCELADO_FALTA_PAGAMENTO.name())){
            return true;
        }
        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADO}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADO}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelado() {
        if (name().equals(CANCELADO.name())){
            return true;
        }
        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PAGO}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PAGO}, caso contrário retorna <code>false</code>
     */
    public boolean isPago() {
        if (name().equals(PAGO.name())){
            return true;
        }
        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_EMISSAO_DAE}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_EMISSAO_DAE}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoEmissaoDAE() {
        if (name().equals(AGUARDANDO_EMISSAO_DAE.name())){
            return true;
        }
        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CADASTRADO}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CADASTRADO}, caso contrário retorna <code>false</code>
     */
    public boolean isCadastrado() {
        if (name().equals(CADASTRADO.name())){
            return true;
        }
        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #AGUARDANDO_CANCELAMENTO}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #AGUARDANDO_CANCELAMENTO}, caso contrário retorna <code>false</code>
     */
    public boolean isAguardandoCancelamento() {
        if (name().equals(AGUARDANDO_CANCELAMENTO.name())){
            return true;
        }
        return false;
    }

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #NAO_CONCLUIDO}
     *
     * @author rodrigo.silva - Rodrigo Silva Oliveira
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #NAO_CONCLUIDO}, caso contrário retorna <code>false</code>
     */
    public boolean isNaoConcluido() {
        if (name().equals(NAO_CONCLUIDO.name())){
            return true;
        }
        return false;
    }

	public static List<StatusCadastroConsumoFlorestal> valuesOrderedByDescricao(){
		List<StatusCadastroConsumoFlorestal> list =  Arrays.asList(StatusCadastroConsumoFlorestal.values());
		Collections.sort(list, new Comparator<StatusCadastroConsumoFlorestal>() {

			@Override
			public int compare(StatusCadastroConsumoFlorestal o1, StatusCadastroConsumoFlorestal o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

	public static StatusCadastroConsumoFlorestal fromInt(int valor) {
		for (StatusCadastroConsumoFlorestal statusCadastroConsumo : StatusCadastroConsumoFlorestal.values()) {
			if (statusCadastroConsumo.ordinal() == valor){
				return statusCadastroConsumo;
			}
		}
		return null;
	}
}