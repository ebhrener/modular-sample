package br.gov.ce.semace.erp.enuns;


/**
 * GRUPO-SEMACE-TESTE ID 215
 * SEMACE SMS 1 ID 294 (1a notificação de SMS),
 * SEMACE SMS 2 ID 295 (2a notificação),
 * SEMACE SMS 3 ID 296 (3a),
 * SEMACE URA 1 ID 297 (1a ligação de URA) e
 * SEMACE URA 2 ID 298 (2a ligação)
 *
 *	@author joerlan.lima
 *
 */
public enum GrupoEnvioURA {

	GRUPO_URA_1(297L, 7),
	GRUPO_URA_2(298L, 2);

	private Long remoteID;
	private Integer plusDays;

	private GrupoEnvioURA(Long remoteID, Integer plusDays) {
		this.remoteID = remoteID;
		this.setPlusDays(plusDays);
	}

	public Long getRemoteID() {
		return remoteID;
	}

	public void setRemoteID(Long remoteID) {
		this.remoteID = remoteID;
	}

	public Integer getPlusDays() {
		return plusDays;
	}

	public void setPlusDays(Integer plusDays) {
		this.plusDays = plusDays;
	}

}
