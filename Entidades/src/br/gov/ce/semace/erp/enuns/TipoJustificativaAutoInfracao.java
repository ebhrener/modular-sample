package br.gov.ce.semace.erp.enuns;

public enum TipoJustificativaAutoInfracao {

	CANCELAR("Cancelar"),
	EDITAR("Editar"),
	PENDENTE_SINCRONIA("Pendência Sincronia");

	private String descricao;

	private TipoJustificativaAutoInfracao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PENDENTE_SINCRONIA}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28 de jun de 2016 13:32:37
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PENDENTE_SINCRONIA}, caso contrário retorna <code>false</code>
     */
    public boolean isPendenteSincronia() {

        if (name().equals(PENDENTE_SINCRONIA.name())){
            return true;
        }

        return false;
    }

}
