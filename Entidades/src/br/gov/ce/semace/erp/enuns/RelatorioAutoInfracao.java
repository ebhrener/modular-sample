/**
 *
 */
package br.gov.ce.semace.erp.enuns;

/**
 * @author renearaujo
 *
 */
public enum RelatorioAutoInfracao {

	TIPO_AUTUADO("Relatório por Tipo de Autuado"),
	TIPO_AUTO_INFRACAO("Relatório por Tipo de Infração"),
	TIPO_MULTA("Relatório por Valores de Multa");

	private String descricao;

	private RelatorioAutoInfracao(String descricao) {
		this.descricao = descricao;
	}
}
