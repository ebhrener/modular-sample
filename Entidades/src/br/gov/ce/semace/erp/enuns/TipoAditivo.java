package br.gov.ce.semace.erp.enuns;



/**
 * {@link Enum} referente ao Tipo de {@link Aditivo}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #PRAZO} <br>
 * {@link #VALOR} <br>
 * {@link #PRAZO_VALOR} <br>
 * 
 * @author Tiago Freire (tiago.freire@semace.ce.gov.br) 18/10/2013 10:07:42
 */

public enum TipoAditivo {

	PRAZO("Prazo"),
	VALOR("Valor"),
	PRAZO_VALOR("Prazo e Valor");
	
    /**
     * Descrição do {@link Enum}
     * 
     * @author Tiago Freire (tiago.freire@semace.ce.gov.br) 18/10/2013 10:09:18
     */
	private String descricao;
	

	 /**
     * Construtor com os parâmetros descricao e valor.
     * 
     * @author Tiago Freire (tiago.freire@semace.ce.gov.br) 18/10/2013 10:09:33
     * 
     * @param description
     *            Parâmetro a ser setado no campo <code>{@link #descricao} </code> do {@link Enum}
     * 
     * @param value
     *            Parâmetro a ser setado no campo <code> {@link #value} </code> do {@link Enum}
     */
	private TipoAditivo(String description) {
		this.setDescricao(description);
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isPrazo(){
		if(this.name().equals(PRAZO)){
			return true;
		}
		
		return false;
	}
	
	public boolean isValor(){
		if(this.name().equals(VALOR)){
			return true;
		}
		
		return false;
	}
	
	public boolean isPrazoValor(){
		if(this.name().equals(PRAZO_VALOR)){
			return true;
		}
		
		return false;
	}
	
}
