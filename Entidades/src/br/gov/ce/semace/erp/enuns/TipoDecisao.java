package br.gov.ce.semace.erp.enuns;

public enum TipoDecisao {
	
	PRIMEIRA_INSTANCIA("1ª Instância"),
	SEGUNDA_INSTANCIA("2ª Instância"),
	RETRATACAO_PRIMEIRA_INSTANCIA("Reconsideração de 1ª Instância");
	
	private String descricao;

	
	private TipoDecisao(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
