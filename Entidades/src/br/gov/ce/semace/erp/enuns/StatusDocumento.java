package br.gov.ce.semace.erp.enuns;


import java.util.ArrayList;
import java.util.List;

import br.gov.ce.semace.erp.entity.geral.Documento;

/**
 * {@link Enum} referente aos Status do {@link Documento}
 * <p>
 * Composto por: <br>
 * <br>
 * {@link #PROTOCOLADO} <br>
 * {@link #CANCELADO} <br>
 * {@link #ARQUIVADO} <br>
 * {@link #ENCERRADO} <br>
 * {@link #CRIADO} <br>
 *
 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/09/2013 14:42:21
 */
public enum StatusDocumento {

	/**
	 * Informa se o {@link Documento} foi protocolado
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/09/2013 14:44:06
	 */
	PROTOCOLADO("Protocolado"),

	/**
	 * Informa se o {@link Documento} foi cancelado
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/09/2013 14:44:57
	 */
	CANCELADO("Cancelado"),

	/**
	 * Informa se o {@link Documento} foi arquivado
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/09/2013 14:45:14
	 */
	ARQUIVADO("Arquivado"),

	/**
	 * Informa se o {@link Documento} foi encerrado
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/09/2013 14:45:35
	 */
	ENCERRADO("Encerrado"),

	/**
	 * Informa se o {@link Documento} foi criado
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/09/2013 14:46:02
	 */
	CRIADO("Criado");

    /**
     * Descrição do {@link Enum}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/09/2013 14:46:26
     */
	private String descricao;

	/**
     * Construtor default com o parâmetro descricao.
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 03/09/2013 14:47:05
     *
     * @param descricao
     * 		Parâmetro a ser setado no campo <code>{@link #descricao} </code> do {@link Enum}
     */
	private StatusDocumento(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return this.descricao;
	}

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #PROTOCOLADO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 20/06/2013 11:49:19
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #PROTOCOLADO}, caso contrário retorna <code>false</code>
     */
    public boolean isProtocolado() {

        if (name().equals(PROTOCOLADO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CANCELADO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 20/06/2013 14:19:45
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CANCELADO}, caso contrário retorna <code>false</code>
     */
    public boolean isCancelado() {

        if (name().equals(CANCELADO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ARQUIVADO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 20/06/2013 14:30:28
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ARQUIVADO}, caso contrário retorna <code>false</code>
     */
    public boolean isArquivado(){

        if (name().equals(ARQUIVADO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #ENCERRADO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 20/06/2013 14:34:52
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #ENCERRADO}, caso contrário retorna <code>false</code>
     */
    public boolean isEncerrado(){

        if (name().equals(ENCERRADO.name())){
            return true;
        }

        return false;
    }

    /**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #CRIADO}
     *
     * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 20/06/2013 14:35:26
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #CRIADO}, caso contrário retorna <code>false</code>
     */
    public boolean isCriado(){

        if (name().equals(CRIADO.name())){
            return true;
        }

        return false;
    }

    public static List<StatusDocumento> getListaStatusDocAvulso(){
    	List<StatusDocumento> retorno = new ArrayList<>();

    	retorno.add(CANCELADO);
    	retorno.add(CRIADO);
    	retorno.add(ENCERRADO);

    	return retorno;
    }

    /**
     * {@link List}a dos {@link StatusDocumento} para {@link br.gov.ce.semace.erp.entity.protocolo.DocAnexo} utilizados
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 9, 2014 11:24:30 AM
     *
     * @return {@link List}a com os {@link StatusDocumento} setados
     */
    public static List<StatusDocumento> getListaStatusDocAnexo(){
    	List<StatusDocumento> retorno = new ArrayList<>();

    	retorno.add(CANCELADO);
    	retorno.add(CRIADO);

    	return retorno;
    }
}