package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tiago
 *
 */

public enum StatusOrdemTransporte {

	AGUARDANDO_MOTORISTA("Aguardando Motorista"),
	AGUARDANDO_VEICULO("Aguardando Veículo"),
	AGUARDANDO_APROVACAO("Aguardando Aprovação"),
	APROVADA("Aprovada"),
	INICIADA("Iniciada"),
	FINALIZADA("Finalizada"),
	CANCELADA("Cancelada");

	String descricao;

	private StatusOrdemTransporte(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public boolean isAguardandoVeiculo() {
		if (this.name().equals(AGUARDANDO_VEICULO.name())) {
			return true;
		}

		return false;
	}

	public boolean isAguardandoMotorista() {
		if (this.name().equals(AGUARDANDO_MOTORISTA.name())) {
			return true;
		}

		return false;
	}

	public boolean isAguardandoAprovacao() {
		if (this.name().equals(AGUARDANDO_APROVACAO.name())) {
			return true;
		}

		return false;
	}

	public boolean isAprovada() {
		if (this.name().equals(APROVADA.name())) {
			return true;
		}

		return false;
	}

	public boolean isIniciada() {
		if (this.name().equals(INICIADA.name())) {
			return true;
		}

		return false;
	}

	public boolean isFinalizada() {
		if (this.name().equals(FINALIZADA.name())) {
			return true;
		}

		return false;
	}

	public boolean isCancelada() {
		if (this.name().equals(CANCELADA.name())) {
			return true;
		}

		return false;
	}

	public static List<StatusOrdemTransporte> getListStatusOrdemTransporteOrdenada(){
		ArrayList<StatusOrdemTransporte> listOrdenada = new ArrayList<StatusOrdemTransporte>();
		listOrdenada.add(StatusOrdemTransporte.AGUARDANDO_APROVACAO);
		listOrdenada.add(StatusOrdemTransporte.AGUARDANDO_MOTORISTA);
		listOrdenada.add(StatusOrdemTransporte.AGUARDANDO_VEICULO);
		listOrdenada.add(StatusOrdemTransporte.APROVADA);
		listOrdenada.add(StatusOrdemTransporte.CANCELADA);
		listOrdenada.add(StatusOrdemTransporte.FINALIZADA);
		listOrdenada.add(StatusOrdemTransporte.INICIADA);
		return listOrdenada;
	}
}