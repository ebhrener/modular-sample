package br.gov.ce.semace.erp.enuns;

public enum TipoMajoracao {
	
	AGRAVANTE("Agravante"), 
	ATENUANTE("Atenuante"); 
	
	private String descricao;
	
	private TipoMajoracao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
