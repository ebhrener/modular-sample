package br.gov.ce.semace.erp.enuns;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum NaturezaTermo {

	NATUREZA_FLORESTAL("Florestal"),
    NATUREZA_COMERCIAL("Comercial"),
    NATUREZA_INDUSTRIAL("Industrial"),
    NATUREZA_OUTROS("Outros");

	private String descricao;

	private NaturezaTermo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public static NaturezaTermo getEnumByDescricao(String descricao){

		for (NaturezaTermo natureza : values()) {
			if(natureza.getDescricao().equals(descricao)) {
				return natureza;
			}
		}

		return null;
	}

	public static List<NaturezaTermo> valuesOrderedByDescricao(){
		List<NaturezaTermo> list =  Arrays.asList(NaturezaTermo.values());
		Collections.sort(list, new Comparator<NaturezaTermo>() {

			@Override
			public int compare(NaturezaTermo o1, NaturezaTermo o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return list;
	}

	/**
     * Verifica se o {@link #name()} do {@link Enum} é {@link #NATUREZA_OUTROS}
     *
     * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 24/05/2016 22:57:35
     *
	 * @return Retorna <code>true</code> se o {@link #name()} do {@link Enum} é
	 *         {@link #NATUREZA_OUTROS}, caso contrário retorna <code>false</code>
     */
    public boolean isOutros() {

        if (name().equals(NATUREZA_OUTROS.name())){
            return true;
        }

        return false;
    }

}
