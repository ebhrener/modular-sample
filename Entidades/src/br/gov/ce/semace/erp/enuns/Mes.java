package br.gov.ce.semace.erp.enuns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Erick Bhrener
 *
 */
public enum Mes {
	JANEIRO(0,"Janeiro"),
	FEVEREIRO(1,"Fevereiro"),
	MARCO(2,"Março"),
	ABRIL(3,"Abril"),
	MAIO(4,"Maio"),
	JUNHO(5,"Junho"),
	JULHO(6,"Julho"),
	AGOSTO(7,"Agosto"),
	SETEMBRO(8,"Setembro"),
	OUTUBRO(9,"Outubro"),
	NOVEMBRO(10,"Novembro"),
	DEZEMBRO(11,"Dezembro");

	private Mes(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}

	private Integer id;
	private String nome;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public static List<Mes> getMeses(){

		List<Mes> meses = new ArrayList<>();

		meses.addAll(Arrays.asList(Mes.values()));

		return meses;
	}
	public static Mes findById(Integer id){

		switch(id){
			case 0:
				return JANEIRO;
			case 1:
				return FEVEREIRO;
			case 2:
				return MARCO;
			case 3:
				return ABRIL;
			case 4:
				return MAIO;
			case 5:
				return JUNHO;
			case 6:
				return JULHO;
			case 7:
				return AGOSTO;
			case 8:
				return SETEMBRO;
			case 9:
				return OUTUBRO;
			case 10:
				return NOVEMBRO;
			case 11:
				return DEZEMBRO;
			default:
				break;
		}
		return null;
	}
}
