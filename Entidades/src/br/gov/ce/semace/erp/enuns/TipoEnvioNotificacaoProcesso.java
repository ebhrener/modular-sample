package br.gov.ce.semace.erp.enuns;

/**
 * Tipo de envio da Notificação (EMAIL, SMS ou Chamda por VOZ)
 *
 * @author joerlan.lima
 *
 */
public enum TipoEnvioNotificacaoProcesso {

	EMAIL,
	SMS;

}
