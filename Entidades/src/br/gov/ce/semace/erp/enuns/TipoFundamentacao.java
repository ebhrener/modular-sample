package br.gov.ce.semace.erp.enuns;

public enum TipoFundamentacao {
	
	DECRETO_FEDERAL ("Decreto Federal nº", "do"),
	DECRETO_ESTADUAL("Decreto Estadual nº", "do"),
	DECRETO_MUNICIPAL("Decreto Municipal nº", "do"),
	INSTRUCAO_NORMATIVA("Instrução Normativa", "da"),
	LEI_FEDERAL("Lei Federal nº", "da"),
	LEI_ESTADUAL("Lei Estadual nº", "da"),
	LEI_MUNICIPAL("Lei Municipal nº", "da"),
	PORTARIA("Portaria", "da"),
	RESOLUCAO_CONAMA("Resolução CONAMA nº", "da"),
	RESOLUCAO_COEMA("Resolução COEMA nº", "da"),
	RESOLUCAO_CONTRAN("Resolução CONTRAN nº", "da");
	
	private String descricao;
	private String conector;
	
	private TipoFundamentacao(String descricao, String conector) {
		this.descricao = descricao;
		this.conector = conector;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
		
	}
	public String getConector() {
		return conector;
	}

	public void setConector(String conector) {
		this.conector = conector;
	}

	public static TipoFundamentacao getEnumByDescricao(String descricao) {
		for (TipoFundamentacao tipo : values()) {
			if (tipo.getDescricao().equals(descricao)) {
				return tipo;
			}
		}
		return null;
	}

}
