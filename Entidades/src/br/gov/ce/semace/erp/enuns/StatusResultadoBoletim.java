package br.gov.ce.semace.erp.enuns;

public enum StatusResultadoBoletim {

	PROPRIA("Própria", true, "label-success", "P"),
	IMPROPRIA("Imprópria", false, "label-important", "I"),
	IMPROPRIA_JUSTIFICADA("Imprópria Justificada", false, "label-important", "I*"),
	SEM_RESULTADO("Sem Resultado", false, "label-warning", "ND");

	private String descricao;
	private Boolean isAltera;
	private String label;
	private String symbol;

	private StatusResultadoBoletim(String descricao, boolean isAltera, String label, String symbol) {
		this.descricao = descricao;
		this.isAltera = isAltera;
		this.label = label;
		this.setSymbol(symbol);
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getIsAltera() {
		return isAltera;
	}

	public void setIsAltera(Boolean isAltera) {
		this.isAltera = isAltera;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getOrdinal(){
		return ordinal();
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

}
