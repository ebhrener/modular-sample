package br.gov.ce.semace.erp.enuns;

/**
 * Tipo de Histórico da Divida Ativa
 *
 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 04/04/2014
 *
 */
public enum TipoHistoricoDivida {

	CADASTRO("Cadastro"),
	ALTERACAO("Alteração"),
	ATUALIZACAO_MONETARIA("Atualização Monetária"),
	ALTERACAO_IGPDI("Alteração IGP-DI"),
	ALTERACAO_SELIC("Alteração Selic"),
	ALTERACAO_MULTA("Alteração Multa"),
//	TODO: DIVIDA atualizar dividas apos a alteração dos Juros?
	ALTERACAO_JUROS("Alteração Juros"),
	CANCELAMENTO_ACORDO("Cancelamento do Acordo"),
	CANCELAMENTO_PRE_ACORDO("Cancelamento do Pre-Acordo"),
	FINALIZACAO_ACORDO("Finalização do Acordo"),
	CANCELAMENTO_DAE("Cancelamento do Dae de Quitação"),
	MIGRACAO("Migração"),
	ATUALIZACAO_SALDO_DEVEDOR("Atualização do Saldo Devedor");

	private String descricao;

	private TipoHistoricoDivida(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
