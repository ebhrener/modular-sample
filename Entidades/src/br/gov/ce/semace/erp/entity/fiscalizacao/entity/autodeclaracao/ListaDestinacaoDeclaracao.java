package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListaDestinacaoDeclaracao", propOrder = {
    "listaDestinacaoDeclaracao"
})
public class ListaDestinacaoDeclaracao {
	
	private List<DestinacaoDeclaracao> listaDestinacaoDeclaracao;

	public List<DestinacaoDeclaracao> getListaDestinacaoDeclaracao() {
		return listaDestinacaoDeclaracao;
	}

	public void setListaDestinacaoDeclaracao(
			List<DestinacaoDeclaracao> listaDestinacaoDeclaracao) {
		this.listaDestinacaoDeclaracao = listaDestinacaoDeclaracao;
	}
	
	

}
