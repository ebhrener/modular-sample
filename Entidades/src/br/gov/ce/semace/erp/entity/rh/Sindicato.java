package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

/**
 * Entidade {@link Sindicato} do esquema RH.
 *
 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 16/04/2014 10:59:39
 */
@Entity
@Audited
@Table(schema="rh", name="sindicato")
public class Sindicato implements Serializable{

	private static final long serialVersionUID = 3544450798009256978L;

	@Id
	@SequenceGenerator(sequenceName="rh.seq_sindicato", name="SEQSINDICATO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQSINDICATO")
	private Long id;

	@NotNull(message="Campo Descrição é Obrigatorio")
	@Column(nullable=false)
	private String descricao;

	@Column(name="ativo")
	private boolean ativo = true;

	//================================== GETERS and SETERS ====================================//

	@Transient
	public String getDetalheAtivo(){

		if (this.ativo){
			return "Sim";
		}

		return "Não";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
