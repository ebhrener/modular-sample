package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.utils.DateUtils;

@Entity
@Audited
@Table(name="consultoria_interessado", schema = "geral")
public class ConsultoriaInteressado implements Serializable{

	private static final long serialVersionUID = 7018040291751423025L;


	@Id @Column(name="consultoria_interessado_id")
	@SequenceGenerator(sequenceName="geral.seq_consultoria_interessado", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="consultoria_id")
	private Consultoria consultoria = new Consultoria();


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="interessado_id")
	private Interessado interessado = new Interessado();

	@OneToMany(mappedBy = "consultoriaInteressado", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<DadosProcuracao> dadosProcuracoes = new ArrayList<>();

	@Column(name = "consultoria_ativa")
	private Boolean consultoriaAtiva;

	/**
	 * Método {@link Transient}e que informa os detalhes do status/validade da procuração.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 25/09/2013 16:36:27
	 *
	 * @return
	 *  etorna a {@link String} <code>Inativa</code> quando a última procuração está com a propriedade
	 *  {@link DadosProcuracao#getAtivo()} <code>false</code> ou não foi encontrada procuração.
	 *  Caso a última procuração tenha valor <code>null</code> no campo {@link DadosProcuracao#getDataValidade()},
	 *  retorna a String <code>Sem Informação</code>. Por fim, caso a última procuração tenha data de validade
	 *  e o status seja ativo, retorna a data de validade formatada.
	 */
	@Transient
	public String getDetalheProcuracaoFormatado(){

		if(getDadosProcuracoes() != null && !getDadosProcuracoes().isEmpty()){

			final DadosProcuracao dadosUltimaProcuracao = getDadosProcuracoes().get(getDadosProcuracoes().size()-1);

			if(!dadosUltimaProcuracao.getAtivo()){
				return "Inválida";
			}

			if(dadosUltimaProcuracao.getDataValidade() != null && dadosUltimaProcuracao.getAtivo()) {
				return " Até " + DateUtils.toString(dadosUltimaProcuracao.getDataValidade());
			}

			if (dadosUltimaProcuracao.getDataValidade() == null ){
				return "Sem Informação";
			}
		}

		return "Inativa";
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the consultoria
	 */
	public Consultoria getConsultoria() {
		return this.consultoria;
	}

	/**
	 * @param consultoria the consultoria to set
	 */
	public void setConsultoria(Consultoria consultoria) {
		this.consultoria = consultoria;
	}

	/**
	 * @return the interessado
	 */
	public Interessado getInteressado() {
		return this.interessado;
	}

	/**
	 * @param interessado the interessado to set
	 */
	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}

	/**
	 * @return the dadosProcuracoes
	 */
	public List<DadosProcuracao> getDadosProcuracoes() {
		return this.dadosProcuracoes;
	}

	/**
	 * @param dadosProcuracoes the dadosProcuracoes to set
	 */
	public void setDadosProcuracoes(List<DadosProcuracao> dadosProcuracoes) {
		this.dadosProcuracoes = dadosProcuracoes;
	}

	/**
	 * @return the consultoriaAtiva
	 */
	public Boolean getConsultoriaAtiva() {
		return this.consultoriaAtiva;
	}

	/**
	 * @param consultoriaAtiva the consultoriaAtiva to set
	 */
	public void setConsultoriaAtiva(Boolean consultoriaAtiva) {
		this.consultoriaAtiva = consultoriaAtiva;
	}


}
