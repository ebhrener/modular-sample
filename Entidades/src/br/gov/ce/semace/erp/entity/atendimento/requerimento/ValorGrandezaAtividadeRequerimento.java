package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.AtividadeClassificacaoGrandeza;

@Entity
@Audited
@Table(schema = "atendimento", name = "valor_grandeza_atividade_requerimento")
public class ValorGrandezaAtividadeRequerimento implements Serializable {

	private static final long serialVersionUID = -5424417047632093450L;

	@Id
	@SequenceGenerator(sequenceName = "atendimento.seq_valor_grandeza_atv_req", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="atividade_requerimento_checklist_id")
	private AtividadeRequerimentoChecklist atividadeRequerimentoChecklist;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="atividade_classificacao_grandeza_id")
	private AtividadeClassificacaoGrandeza atividadeClassificacaoGrandeza;

	@Column(name="valor_numerico")
	private BigDecimal valorNumerico;

	@Column(name="valor_alfa_numerico")
	private String valorAlfaNumerico;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AtividadeRequerimentoChecklist getAtividadeRequerimentoChecklist() {
		return atividadeRequerimentoChecklist;
	}

	public void setAtividadeRequerimentoChecklist(AtividadeRequerimentoChecklist atividadeRequerimentoChecklist) {
		this.atividadeRequerimentoChecklist = atividadeRequerimentoChecklist;
	}

	public AtividadeClassificacaoGrandeza getAtividadeClassificacaoGrandeza() {
		return atividadeClassificacaoGrandeza;
	}

	public void setAtividadeClassificacaoGrandeza(AtividadeClassificacaoGrandeza atividadeClassificacaoGrandeza) {
		this.atividadeClassificacaoGrandeza = atividadeClassificacaoGrandeza;
	}

	public BigDecimal getValorNumerico() {
		return valorNumerico;
	}

	public void setValorNumerico(BigDecimal valorNumerico) {
		this.valorNumerico = valorNumerico;
	}

	public String getValorAlfaNumerico() {
		return valorAlfaNumerico;
	}

	public void setValorAlfaNumerico(String valorAlfaNumerico) {
		this.valorAlfaNumerico = valorAlfaNumerico;
	}

}
