package br.gov.ce.semace.erp.entity.protocolo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Pendencia;

@Entity
@Audited
@Table(schema = "protocolo", name = "pendencia_processo")
public class PendenciaProcesso extends Pendencia {

	private static final long serialVersionUID = -699304408421576052L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "processo_id", nullable = true)
	private Processo processo;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "tipo_anexo_id")
	private TipoAnexo tipoAnexo;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinTable(name = "pendencia_processo_arquivo_doc_anexo",
		schema = "protocolo",
		joinColumns = @JoinColumn(name = "pendencia_id"),
		inverseJoinColumns = @JoinColumn(name = "arquivo_documento_id"))
	private List<ArquivoDocAnexo> listArquivoDocAnexo;

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public TipoAnexo getTipoAnexo() {
		return tipoAnexo;
	}

	public void setTipoAnexo(TipoAnexo tipoAnexo) {
		this.tipoAnexo = tipoAnexo;
	}

	public List<ArquivoDocAnexo> getListArquivoDocAnexo() {
		return listArquivoDocAnexo;
	}

	public void setListArquivoDocAnexo(List<ArquivoDocAnexo> listArquivoDocAnexo) {
		this.listArquivoDocAnexo = listArquivoDocAnexo;
	}

}
