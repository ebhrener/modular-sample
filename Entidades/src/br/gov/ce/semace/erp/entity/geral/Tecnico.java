package br.gov.ce.semace.erp.entity.geral;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="geral", name="tecnico")
@PrimaryKeyJoinColumn(name="pessoa_fisica_id")
public class Tecnico extends PessoaFisica{

	private static final long serialVersionUID = -178902527012023891L;
	
	@OneToMany(mappedBy = "tecnico", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<TecnicoFormacao> tecnicosFormacoes = new ArrayList<TecnicoFormacao>();

	public List<TecnicoFormacao> getTecnicosFormacoes() {
		return tecnicosFormacoes;
	}

	public void setTecnicosFormacoes(List<TecnicoFormacao> tecnicosFormacoes) {
		this.tecnicosFormacoes = tecnicosFormacoes;
	}

}
