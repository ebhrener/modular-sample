package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "configuracao_agendamento_rama", schema = "licenciamento")
public class ConfiguracaoAgendamentoRama implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8704292600686898754L;

	@Id
	@SequenceGenerator(sequenceName = "licenciamento.seq_configuracao_agendamento_rama", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(name = "descricao")
	private String descricao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_processo_id", nullable = false)
	private TipoProcesso tipoProcesso = new TipoProcesso();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modalidade_processo_id")
	private Modalidade modalidade = new Modalidade();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subtipo_processo_id")
	private SubtipoProcesso subtipoProcesso = new SubtipoProcesso();

	@Column(name = "valida_agendamento")
	private boolean validaAgendemento = false;

	@ManyToOne
	@JoinColumn(name = "configuracao_licenciamento_id")
	private ConfiguracaoLicenciamento configuracaoLicenciamento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoProcesso getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(TipoProcesso tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public Modalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}

	public SubtipoProcesso getSubtipoProcesso() {
		return subtipoProcesso;
	}

	public void setSubtipoProcesso(SubtipoProcesso subtipoProcesso) {
		this.subtipoProcesso = subtipoProcesso;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isValidaAgendemento() {
		return validaAgendemento;
	}

	public void setValidaAgendemento(boolean validaAgendemento) {
		this.validaAgendemento = validaAgendemento;
	}

	public ConfiguracaoLicenciamento getConfiguracaoLicenciamento() {
		return configuracaoLicenciamento;
	}

	public void setConfiguracaoLicenciamento(ConfiguracaoLicenciamento configuracaoLicenciamento) {
		this.configuracaoLicenciamento = configuracaoLicenciamento;
	}
}
