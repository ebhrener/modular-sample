package br.gov.ce.semace.erp.entity.florestal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.financeiro.Dae;
import br.gov.ce.semace.erp.entity.geral.Consultoria;
import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.geral.Endereco;
import br.gov.ce.semace.erp.entity.geral.Interessado;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.StatusCadastroConsumoFlorestal;
import br.gov.ce.semace.erp.enuns.TipoAcao;
import br.gov.ce.semace.erp.enuns.TipoSistema;

@Entity
@Audited
@Table(name = "historico_cadastro_consumo_florestal", schema = "florestal")
public class HistoricoCadastroConsumoFlorestal implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -8758533128450998365L;

	@Id
	@SequenceGenerator(sequenceName = "florestal.seq_historico_cadastro_consumo_florestal", name = "florestal.seq_historico_cadastro_consumo_florestal", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "florestal.seq_historico_cadastro_consumo_florestal")
	private Long id;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "interessado_id")
	private Interessado interessado;

	@OneToOne(optional = true)
	@JoinColumn(name = "documento_id")
	private Documento documento;

	@Column(name = "data_cadastro")
	private Date dataCadastro;

	@Column(name = "data_inicio_validade")
	private Date dataInicioValidade;

	@Column(name = "inscricao_estadual")
	private String inscricaoEstadual;

	@OneToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "dae_id")
	private Dae dae;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "consultoria_id")
	private Consultoria consultoria;

	@Column(name = "data_validade")
	private Date dataFinalValidade;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "consumo_id")
	private Consumo consumo = new Consumo();

	@Column(name="is_isento", nullable = false)
	private Boolean isento;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	@Column(name = "status_cad_consumo_florestal")
	private StatusCadastroConsumoFlorestal statusCadConsumoFlorestal;

	// Dados referentes ao histórico
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data")
    private Date data;

	@Column(name="tipo_acao")
    private TipoAcao tipoAcao;

	@Column(name="sistema")
    private TipoSistema sistema;

	@Column(name="justificativa")
    private String justificativa;

    @Column(name="cadastro_consumo_florestal_id")
	private Long cadastroConsumoFlorestalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario;

	@Temporal(TemporalType.DATE)
	@Column(name="data_cancelamento")
    private Date dataCancelamento;

	@Temporal(TemporalType.DATE)
	@Column(name="data_solicitacao_cancelamento")
    private Date dataSolicitacaoCancelamento;

	@Column(name="numeroDoc")
	private String numeroDoc;

	@Column(name="confirmar_cancelamento")
	private boolean confirmarCancelamento;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "historicoCadastroConsumoFlorestal")
	private List<ArquivosHistoricoCadastroConsumoFlorestal> arquivosHistoricoCadastroConsumoFlorestal = new ArrayList<>();

//	@Transient
//	private List<UploadedFile> listAnexos = new ArrayList<UploadedFile>();

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Interessado getInteressado() {
		return interessado;
	}


	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}


	public Documento getDocumento() {
		return documento;
	}


	public void setDocumento(Documento documento) {
		this.documento = documento;
	}


	public Date getDataCadastro() {
		return dataCadastro;
	}


	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}


	public Date getDataInicioValidade() {
		return dataInicioValidade;
	}


	public void setDataInicioValidade(Date dataInicioValidade) {
		this.dataInicioValidade = dataInicioValidade;
	}


	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}


	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}


	public Dae getDae() {
		return dae;
	}


	public void setDae(Dae dae) {
		this.dae = dae;
	}


	public Consultoria getConsultoria() {
		return consultoria;
	}


	public void setConsultoria(Consultoria consultoria) {
		this.consultoria = consultoria;
	}


	public Date getDataFinalValidade() {
		return dataFinalValidade;
	}


	public void setDataFinalValidade(Date dataFinalValidade) {
		this.dataFinalValidade = dataFinalValidade;
	}


	public Consumo getConsumo() {
		return consumo;
	}


	public void setConsumo(Consumo consumo) {
		this.consumo = consumo;
	}

	public Boolean getIsento() {
		return isento;
	}


	public void setIsento(Boolean isento) {
		this.isento = isento;
	}


	public Endereco getEndereco() {
		return endereco;
	}


	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}


	public StatusCadastroConsumoFlorestal getStatusCadConsumoFlorestal() {
		return statusCadConsumoFlorestal;
	}


	public void setStatusCadConsumoFlorestal(
			StatusCadastroConsumoFlorestal statusCadConsumoFlorestal) {
		this.statusCadConsumoFlorestal = statusCadConsumoFlorestal;
	}


	public Date getData() {
		return data;
	}


	public void setData(Date data) {
		this.data = data;
	}

	public String getJustificativa() {
		return justificativa;
	}

	/**
	 * Informa o valor do campo {@link #justificativa} até o limite que for passado por parâmetro.
	 *
	 * @author rodrigo.silva - Rodrigo Silva Oliveira - 13/03/2017 11:57:58
	 *
	 * @param quantidadeCaracteres - Valor que representa a quantidade de caracteres a ser repassado
	 *
	 * @return - Retorna o valor do campo justificativa, caso o tamanho da {@link String} da justificativa for
	 * menor que a quantidade de caracteres passada por parametro. Caso contrário, retorna uma {@link String}
	 * com o valor da justificativa até o limite de quantidade de caracteres requisitado, acrescido de <i>...</i>
	 */
	@Transient
	public String getJustificativa(int quantidadeCaracteres) {
		if (this.justificativa.length() < quantidadeCaracteres) {
			return this.justificativa;
		}
		return justificativa.substring(0, quantidadeCaracteres) + " <bold>...</bold>";
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public Long getCadastroConsumoFlorestalId() {
		return cadastroConsumoFlorestalId;
	}


	public void setCadastroConsumoFlorestalId(Long cadastroConsumoFlorestalId) {
		this.cadastroConsumoFlorestalId = cadastroConsumoFlorestalId;
	}


	public Funcionario getFuncionario() {
		return funcionario;
	}


	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}


	public Date getDataCancelamento() {
		return dataCancelamento;
	}


	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}


	public TipoAcao getTipoAcao() {
		return tipoAcao;
	}


	public void setTipoAcao(TipoAcao tipoAcao) {
		this.tipoAcao = tipoAcao;
	}


	public TipoSistema getSistema() {
		return sistema;
	}


	public void setSistema(TipoSistema sistema) {
		this.sistema = sistema;
	}


	public String getNumeroDoc() {
		return numeroDoc;
	}


	public void setNumeroDoc(String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}


	public boolean isConfirmarCancelamento() {
		return confirmarCancelamento;
	}


	public void setConfirmarCancelamento(boolean confirmarCancelamento) {
		this.confirmarCancelamento = confirmarCancelamento;
	}


	public Date getDataSolicitacaoCancelamento() {
		return dataSolicitacaoCancelamento;
	}


	public void setDataSolicitacaoCancelamento(Date dataSolicitacaoCancelamento) {
		this.dataSolicitacaoCancelamento = dataSolicitacaoCancelamento;
	}


	public List<ArquivosHistoricoCadastroConsumoFlorestal> getArquivosHistoricoCadastroConsumoFlorestal() {
		return arquivosHistoricoCadastroConsumoFlorestal;
	}


	public void setArquivosHistoricoCadastroConsumoFlorestal(
			List<ArquivosHistoricoCadastroConsumoFlorestal> arquivosHistoricoCadastroConsumoFlorestal) {
		this.arquivosHistoricoCadastroConsumoFlorestal = arquivosHistoricoCadastroConsumoFlorestal;
	}


//	public List<UploadedFile> getListAnexos() {
//		return listAnexos;
//	}
//
//
//	public void setListAnexos(List<UploadedFile> listAnexos) {
//		this.listAnexos = listAnexos;
//	}
}