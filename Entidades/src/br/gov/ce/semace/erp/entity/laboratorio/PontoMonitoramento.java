package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="ponto_monitoramento", schema="laboratorio")
public class PontoMonitoramento implements Serializable{
	
	private static final long serialVersionUID = 3294740826711110686L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_ponto_monitoramento", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
		
	private String nome;
	private String latitude;
	private String longitude;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="praia_id")
	private Praia praia = new Praia();
	
	@ManyToMany(mappedBy="pontos", fetch = FetchType.LAZY)
	private List<TipoAnalise> tipoAnalises;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public List<TipoAnalise> getTipoAnalises() {
		return tipoAnalises;
	}
	public void setTipoAnalises(List<TipoAnalise> tipoAnalises) {
		this.tipoAnalises = tipoAnalises;
	}
	public Praia getPraia() {
		return praia;
	}
	public void setPraia(Praia praia) {
		this.praia = praia;
	}	

}
