package br.gov.ce.semace.erp.entity.atendimento.vo;

import java.math.BigInteger;

import br.gov.ce.semace.erp.enuns.TipoPendencia;


public class PendenciaChecklistVO {
	
	private BigInteger id;
	private String descricao;
	private String titulo;
	private Boolean pendente;
	private Boolean liberado;
	private Integer tipo;
	private TipoPendencia tipoPendencia;
	
	public PendenciaChecklistVO() {
		
	}
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Boolean getPendente() {
		return pendente;
	}

	public void setPendente(Boolean pendente) {
		this.pendente = pendente;
	}

	public Boolean getLiberado() {
		return liberado;
	}

	public void setLiberado(Boolean liberado) {
		this.liberado = liberado;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
		for(TipoPendencia tp: TipoPendencia.values()){
			if(tp.ordinal() == tipo && !id.equals(0)){
				setTipoPendencia(tp);
				break;
			}
		}
	}

	public TipoPendencia getTipoPendencia() {
		return tipoPendencia;
	}

	public void setTipoPendencia(TipoPendencia tipoPendencia) {
		this.tipoPendencia = tipoPendencia;
	}
}
