package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "tipo_processo", schema="adm_siga")
public class TipoProcessoSiga implements Serializable{

	private static final long serialVersionUID = 5212691003039657904L;

	@Id
	@Column(name="tipo_processo_id", length=4, nullable=false)
	private String id;

	@Column(name="familia_processo_id", length=4)
	private String familiaProcesso;

	@Column(name="grupo_tipo_processo_id", length=4)
	private String grupoTipoProcesso;

	@Column(name="grupo_condicionante_id", length=4)
	private String grupoCondicionante;

	@Column(name="sigla_tipo", length=10)
	private String siglaTipo;

	@Column(name="tipo_processo", length=80)
	private String descricao;

	private boolean excluido;

	@Column(name="requer_documentacao")
	private boolean requerDocumentacao;

	@Column(name="requer_planilha")
	private boolean requerPlanilha;

	@Column(nullable=false)
	private int contador;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFamiliaProcesso() {
		return familiaProcesso;
	}

	public void setFamiliaProcesso(String familiaProcesso) {
		this.familiaProcesso = familiaProcesso;
	}

	public String getGrupoTipoProcesso() {
		return grupoTipoProcesso;
	}

	public void setGrupoTipoProcesso(String grupoTipoProcesso) {
		this.grupoTipoProcesso = grupoTipoProcesso;
	}

	public String getGrupoCondicionante() {
		return grupoCondicionante;
	}

	public void setGrupoCondicionante(String grupoCondicionante) {
		this.grupoCondicionante = grupoCondicionante;
	}

	public String getSiglaTipo() {
		return siglaTipo;
	}

	public void setSiglaTipo(String siglaTipo) {
		this.siglaTipo = siglaTipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isExcluido() {
		return excluido;
	}

	public void setExcluido(boolean excluido) {
		this.excluido = excluido;
	}

	public boolean isRequerDocumentacao() {
		return requerDocumentacao;
	}

	public void setRequerDocumentacao(boolean requerDocumentacao) {
		this.requerDocumentacao = requerDocumentacao;
	}

	public boolean isRequerPlanilha() {
		return requerPlanilha;
	}

	public void setRequerPlanilha(boolean requerPlanilha) {
		this.requerPlanilha = requerPlanilha;
	}

	public int getContador() {
		return contador;
	}

	public void setContador(int contador) {
		this.contador = contador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + contador;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + (excluido ? 1231 : 1237);
		result = prime * result
				+ ((familiaProcesso == null) ? 0 : familiaProcesso.hashCode());
		result = prime
				* result
				+ ((grupoCondicionante == null) ? 0 : grupoCondicionante
						.hashCode());
		result = prime
				* result
				+ ((grupoTipoProcesso == null) ? 0 : grupoTipoProcesso
						.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (requerDocumentacao ? 1231 : 1237);
		result = prime * result + (requerPlanilha ? 1231 : 1237);
		result = prime * result
				+ ((siglaTipo == null) ? 0 : siglaTipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TipoProcessoSiga other = (TipoProcessoSiga) obj;
		if (contador != other.contador) {
			return false;
		}
		if (descricao == null) {
			if (other.descricao != null) {
				return false;
			}
		} else if (!descricao.equals(other.descricao)) {
			return false;
		}
		if (excluido != other.excluido) {
			return false;
		}
		if (familiaProcesso == null) {
			if (other.familiaProcesso != null) {
				return false;
			}
		} else if (!familiaProcesso.equals(other.familiaProcesso)) {
			return false;
		}
		if (grupoCondicionante == null) {
			if (other.grupoCondicionante != null) {
				return false;
			}
		} else if (!grupoCondicionante.equals(other.grupoCondicionante)) {
			return false;
		}
		if (grupoTipoProcesso == null) {
			if (other.grupoTipoProcesso != null) {
				return false;
			}
		} else if (!grupoTipoProcesso.equals(other.grupoTipoProcesso)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (requerDocumentacao != other.requerDocumentacao) {
			return false;
		}
		if (requerPlanilha != other.requerPlanilha) {
			return false;
		}
		if (siglaTipo == null) {
			if (other.siglaTipo != null) {
				return false;
			}
		} else if (!siglaTipo.equals(other.siglaTipo)) {
			return false;
		}
		return true;
	}



}
