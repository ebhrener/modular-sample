package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.seguranca.Usuario;
import br.gov.ce.semace.erp.enuns.TipoHistoricoDivida;

@Entity
@Audited
@Table(name="historico_divida", schema="juridico")
public class HistoricoDivida implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -8829558923717347523L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_historico_divida", name="juridico.seq_historico_divida", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_historico_divida")
	private Long id;

	@ManyToOne(optional=false,fetch=FetchType.EAGER)
	@JoinColumn(name="divida_ativa_id")
	private DividaAtiva dividaAtiva;

	@Column(name="data_cadastro", nullable=false)
	private Date dataCadastro;

	@ManyToOne(fetch=FetchType.LAZY,optional=true)
	@JoinColumn(name="usuario_id")
	private Usuario usuario;

	@Enumerated(EnumType.ORDINAL)
	@Column(nullable=false, name= "tipo_historico_divida")
	private TipoHistoricoDivida tipoHistoricoDivida; 

	@Column(nullable=false, scale=2, precision=12)
	private BigDecimal valor;

	private String observacao;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "org.hibernate.type.BinaryType")
	private byte[] arquivo;
	
	@Column(name ="file_name")
	private String fileName;
	
	@OneToOne(mappedBy="historicoDivida", cascade=CascadeType.ALL)
	private CalculoDivida calculoDivida;
	
	//------------------------------------------------Gets e Setters---------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DividaAtiva getDividaAtiva() {
		return dividaAtiva;
	}

	public void setDividaAtiva(DividaAtiva dividaAtiva) {
		this.dividaAtiva = dividaAtiva;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public TipoHistoricoDivida getTipoHistoricoDivida() {
		return tipoHistoricoDivida;
	}

	public void setTipoHistoricoDivida(TipoHistoricoDivida tipoHistoricoDivida) {
		this.tipoHistoricoDivida = tipoHistoricoDivida;
	}

	public CalculoDivida getCalculoDivida() {
		return calculoDivida;
	}

	public void setCalculoDivida(CalculoDivida calculoDivida) {
		this.calculoDivida = calculoDivida;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public byte[] getArquivo() {
		return arquivo;
	}

	public void setArquivo(byte[] arquivo) {
		this.arquivo = arquivo;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
