package br.gov.ce.semace.erp.entity.notificacaoprocesso;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Pessoa;

@Audited
@Entity
@Table(name="recebimento_notificacoes_pessoa", schema="notificacao_processo")
public class RecebimentoNotificacoesPessoa implements Serializable{


	/**
	 * Serial
	 */
	private static final long serialVersionUID = -5472740484556600387L;

	@Id
	@SequenceGenerator(sequenceName="notificacao_processo.seq_recebimento_notificacoes_pessoa", name="notificacao_processo.seq_recebimento_notificacoes_pessoa", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="notificacao_processo.seq_recebimento_notificacoes_pessoa")
	private Long id;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(nullable=false, name="recebimento_notificacao_id")
	private RecebimentoNotificacoes recebimentoNotificacoes;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(nullable=false, name="pessoa_id")
	private Pessoa pessoa;

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public RecebimentoNotificacoes getRecebimentoNotificacoes() {
		return recebimentoNotificacoes;
	}

	public void setRecebimentoNotificacoes(RecebimentoNotificacoes recebimentoNotificacoes) {
		this.recebimentoNotificacoes = recebimentoNotificacoes;
	}

}
