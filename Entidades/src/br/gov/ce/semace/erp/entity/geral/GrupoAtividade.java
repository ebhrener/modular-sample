package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="grupo_atividade", schema="geral" )
public class GrupoAtividade implements Serializable {

	private static final long serialVersionUID = 3145940149552728374L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_grupoatividade", name="SEQGRUPO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQGRUPO")
	private Long id;

	@NotEmpty(message="Campo Código do Grupo é Obrigatório")
	@OrderBy
	@Column(nullable=false)
	private String codigo;


	@NotEmpty(message="Campo Descrição é Obrigatório")
	@Column(name="grupo_descricao", nullable=false)
	private String grupoDescricao;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="coema_id")
	private Coema coema = new Coema();


	@OneToMany(mappedBy = "grupoAtividade", fetch=FetchType.LAZY)
	private List<Atividade> atividades = new ArrayList<>();

	private Boolean ativo;


	//------------------------------------------Gets e Setters---------------------------------------------


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getGrupoDescricao() {
		return grupoDescricao;
	}


	public void setGrupoDescricao(String grupoDescricao) {
		this.grupoDescricao = grupoDescricao;
	}

	public List<Atividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}

	public Boolean getAtivo() {
		return ativo;
	}


	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}


	//--------------------------------------------------Métodos Auxiliares----------------------------------------------------------


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GrupoAtividade other = (GrupoAtividade) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}


	public Coema getCoema() {
		return coema;
	}


	public void setCoema(Coema coema) {
		this.coema = coema;
	}


}