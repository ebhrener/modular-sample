package br.gov.ce.semace.erp.entity.atendimento.visita;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.atendimento.CartaoVisita;
import br.gov.ce.semace.erp.entity.geral.PessoaFisica;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.entity.rh.Setor;

@Entity
@Table(name="visita", schema="atendimento")
@Audited
public class Visita implements Serializable{

	private static final long serialVersionUID = 3554840986589959709L;

	@Id
	@SequenceGenerator(sequenceName = "atendimento.seq_visita", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_visita")
	private Date dataVisita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_visita_final")
	private Date dataVisitaFinal;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="visitante_id")
	private PessoaFisica visitante;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="setor_id")
	private Setor setor = new Setor();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="contato_setor_id")
	private Funcionario contatoSetor = new Funcionario();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cartao_visita_id", nullable = false)
	private CartaoVisita cartaoVisita = new CartaoVisita();

	@Column(columnDefinition = "boolean default false", nullable = false)
	private Boolean finalizado = Boolean.FALSE;

	public String getDisplayFinalizado() {
		return finalizado == null ? "" : finalizado ? "Sim" : "Não";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PessoaFisica getVisitante() {
		return visitante;
	}

	public void setVisitante(PessoaFisica visitante) {
		this.visitante = visitante;
	}

	public Funcionario getContatoSetor() {
		return contatoSetor;
	}

	public void setContatoSetor(Funcionario contatoSetor) {
		this.contatoSetor = contatoSetor;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public Date getDataVisita() {
		return dataVisita;
	}

	public void setDataVisita(Date dataVisita) {
		this.dataVisita = dataVisita;
	}

	public CartaoVisita getCartaoVisita() {
		return cartaoVisita;
	}

	public void setCartaoVisita(CartaoVisita cartaoVisita) {
		this.cartaoVisita = cartaoVisita;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public Date getDataVisitaFinal() {
		return dataVisitaFinal;
	}

	public void setDataVisitaFinal(Date dataVisitaFinal) {
		this.dataVisitaFinal = dataVisitaFinal;
	}
}