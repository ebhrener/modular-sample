package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Pessoa;

@Entity
@Audited
@Table(name="assinante_divida", schema="juridico")
public class AssinanteDocumentoDivida implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -7761593171038141947L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_assinante_divida", name="juridico.seq_assinante_divida", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_assinante_divida")
	private Long id;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(name="documento_divida_id", nullable=false)
	private DocumentoDivida documentoDivida;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(name="pessoa_id", nullable=false)
	private Pessoa pessoa;

	public DocumentoDivida getDocumentoDivida() {
		return documentoDivida;
	}

	public void setDocumentoDivida(DocumentoDivida documentoDivida) {
		this.documentoDivida = documentoDivida;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
