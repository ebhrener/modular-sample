package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="rh", name="ferias")
public class Ferias implements Serializable{

	private static final long serialVersionUID = -2036864157887825595L;

	@Id
	@SequenceGenerator(sequenceName="rh.seq_ferias", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@NotNull(message="Campo Exercício é obrigatório.")
	@Column(nullable=false)
	private Integer exercicio;

	@Column(name="dias_disponiveis")
	private Integer diasDisponiveis = 0;

	@Column(name="dias_utilizados")
	private Integer diasUtilizados = 0;

	private Boolean abono = false;

	@Temporal(TemporalType.DATE)
	@Column(name="data_cadastro")
	private Date dataCadastro = new Date();

	//------------------------------------------------Relacionamentos---------------------------------------------

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario;

	@OneToMany(mappedBy = "ferias", fetch = FetchType.LAZY)
	private List<UsufruirFerias> usufruirFerias = new ArrayList<>();

	//------------------------------------------------Transient---------------------------------------------

	@Transient
	private boolean habilitaUsufruir = false;

	//------------------------------------------------Gets e Setters---------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getExercicio() {
		return exercicio;
	}

	public void setExercicio(Integer exercicio) {
		this.exercicio = exercicio;
	}

	public Integer getDiasDisponiveis() {
		return diasDisponiveis;
	}

	public void setDiasDisponiveis(Integer diasDisponiveis) {
		this.diasDisponiveis = diasDisponiveis;
	}

	public Boolean getAbono() {
		return abono;
	}

	public void setAbono(Boolean abono) {
		this.abono = abono;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public List<UsufruirFerias> getUsufruirFerias() {
		return usufruirFerias;
	}

	public void setUsufruirFerias(List<UsufruirFerias> usufruirFerias) {
		this.usufruirFerias = usufruirFerias;
	}

	public boolean isHabilitaUsufruir() {
		return habilitaUsufruir;
	}

	public void setHabilitaUsufruir(boolean habilitaUsufruir) {
		this.habilitaUsufruir = habilitaUsufruir;
	}

	public Integer getDiasUtilizados() {
		return diasUtilizados;
	}

	public void setDiasUtilizados(Integer diasUtilizados) {
		this.diasUtilizados = diasUtilizados;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((exercicio == null) ? 0 : exercicio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Ferias other = (Ferias) obj;
		if (exercicio == null) {
			if (other.exercicio != null) {
				return false;
			}
		} else if (!exercicio.equals(other.exercicio)) {
			return false;
		}
		return true;
	}



}