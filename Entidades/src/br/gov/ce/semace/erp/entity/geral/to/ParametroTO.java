package br.gov.ce.semace.erp.entity.geral.to;

public class ParametroTO {

	private Integer prazoEnvioArquivosRequerimento;
	private Integer prazoSolucaoPendenciaDocumentacaoRequerimento;
	private Integer prazoSegurancaPagamentoTaxa;
	private Integer prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED;
	private Integer prazoAtendimentoRequerimentoPreGED;
	private Integer tamanhoPaginaDescarteRequerimento;

	public ParametroTO() {
		super();
	}

	public ParametroTO(Integer prazoEnvioArquivosRequerimento, Integer prazoSolucaoPendenciaDocumentacaoRequerimento, Integer prazoSegurancaPagamentoTaxa,
			Integer prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED, Integer prazoAtendimentoRequerimentoPreGED, Integer tamanhoPaginaDescarteRequerimento) {
		super();
		this.prazoEnvioArquivosRequerimento = prazoEnvioArquivosRequerimento;
		this.prazoSolucaoPendenciaDocumentacaoRequerimento = prazoSolucaoPendenciaDocumentacaoRequerimento;
		this.prazoSegurancaPagamentoTaxa = prazoSegurancaPagamentoTaxa;
		this.prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED = prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED;
		this.prazoAtendimentoRequerimentoPreGED = prazoAtendimentoRequerimentoPreGED;
		this.tamanhoPaginaDescarteRequerimento = tamanhoPaginaDescarteRequerimento;
	}

	public Integer getPrazoEnvioArquivosRequerimento() {
		return prazoEnvioArquivosRequerimento;
	}

	public void setPrazoEnvioArquivosRequerimento(Integer prazoEnvioArquivosRequerimento) {
		this.prazoEnvioArquivosRequerimento = prazoEnvioArquivosRequerimento;
	}

	public Integer getPrazoSolucaoPendenciaDocumentacaoRequerimento() {
		return prazoSolucaoPendenciaDocumentacaoRequerimento;
	}

	public void setPrazoSolucaoPendenciaDocumentacaoRequerimento(Integer prazoSolucaoPendenciaDocumentacaoRequerimento) {
		this.prazoSolucaoPendenciaDocumentacaoRequerimento = prazoSolucaoPendenciaDocumentacaoRequerimento;
	}

	public Integer getPrazoSegurancaPagamentoTaxa() {
		return prazoSegurancaPagamentoTaxa;
	}

	public void setPrazoSegurancaPagamentoTaxa(Integer prazoSegurancaPagamentoTaxa) {
		this.prazoSegurancaPagamentoTaxa = prazoSegurancaPagamentoTaxa;
	}

	public Integer getPrazoSolucaoPendenciaDocumentacaoRequerimentoPreGED() {
		return prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED;
	}

	public void setPrazoSolucaoPendenciaDocumentacaoRequerimentoPreGED(Integer prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED) {
		this.prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED = prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED;
	}

	public Integer getPrazoAtendimentoRequerimentoPreGED() {
		return prazoAtendimentoRequerimentoPreGED;
	}

	public void setPrazoAtendimentoRequerimentoPreGED(Integer prazoAtendimentoRequerimentoPreGED) {
		this.prazoAtendimentoRequerimentoPreGED = prazoAtendimentoRequerimentoPreGED;
	}

	public Integer getTamanhoPaginaDescarteRequerimento() {
		return tamanhoPaginaDescarteRequerimento;
	}

	public void setTamanhoPaginaDescarteRequerimento(Integer tamanhoPaginaDescarteRequerimento) {
		this.tamanhoPaginaDescarteRequerimento = tamanhoPaginaDescarteRequerimento;
	}
}
