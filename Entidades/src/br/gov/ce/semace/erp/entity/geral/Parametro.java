package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.financeiro.TipoTaxa;
import br.gov.ce.semace.erp.entity.geral.entity.cep.Pais;
import br.gov.ce.semace.erp.entity.geral.entity.cep.UF;
import br.gov.ce.semace.erp.entity.rh.Advogado;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.entity.rh.NivelHierarquico;
import br.gov.ce.semace.erp.entity.rh.Setor;
import br.gov.ce.semace.erp.entity.rh.SituacaoFuncionario;

@Entity
@Audited
@Table(schema = "geral", name="parametro")
public class Parametro implements Serializable {

	private static final long serialVersionUID = 4039892393917582334L;

	@Id
	public Long id;

	@Column(name="qtd_max_atendentes")
	private Long qtdMaxAtendentes;

	@Column(name="caminho_arquivo_upload_shape")
	private String caminhoArquivoUploadShape;

	@Column(name="usuario_arquivos_shape")
	private String usuarioArquivosShape;

	@Column(name="senha_usuario_arquivos_shape")
	private String senhaUsuarioArquivosShape;

	@Column(name="usuario_ssh")
	private String usuarioSSH;

	@Column(name="senha_usuario_ssh")
	private String senhaUsuarioSSH;

	@Column(name="caminho_pasta_compartilhamento_ssh")
	private String caminhoPastaCompartilhamentoSSH;

	@Column(name="dominio_arquivos_shape")
	private String dominioArquivosShape;

	@Column(name="endereco_arquivos_shape")
	private String enderecoArquivosShape;

	@Column(name="pasta_compartilhamento_arquivos_shape")
	private String pastaCompartilhamentoArquivosShape;

	@Column(name="pasta_atendimento_arquivos_shape")
	private String pastaAtendimentoArquivosShape;

	@Column(name="pasta_geo_arquivos_shape")
	private String pastaGeoArquivosShape;

	@Column(name="pasta_arquivo_arquivos_shape")
	private String pastaArquivoArquivosShape;

	@Column(name="pasta_base_cartografica_shape")
	private String pastaBaseCartograficaShape;

	@Column(name="pasta_upload_imagens")
	private String pastaUploadImagens;

	@Column(name="pasta_temp")
	private String pastaTemp;

	@Column(name="pasta_importacao_base_cartografica_shape")
	private String pastaImportacaoBaseCartograficaShape;

	@Column(name="usuario_ftp")
	private String usuarioFtp;

	@Column(name="senha_ftp")
	private String senhaFtp;

	@Column(name="path_ftp")
	private String pathFtp;

	@Column(name="ip_ftp")
	private String ipFtp;

	@Column(name="pasta_mapa_tematico")
	private String pastaMapaTematico;

	@Column(name="pasta_compartilhamento_imagens")
	private String pastaCompartilhamentoImagens;

	@Column(name="pasta_imagens_mapa_tematico")
	private String pastaImagensMapaTematico;

	@Column(name="email_atendimento")
    private String emailAtendimento;

    @Column(name="senha_email_atendimento")
    private String senhaEmailAtendimento;

    @Column(name="servidor_email")
    private String servidorEmail;

    @Column(name="valor_faturamento_ponto_corte")
    private BigDecimal valorFaturamentoPontoCorte;

    @Column(name="valor_area_ponto_corte")
    private BigDecimal valorAreaPontoCorte;

    @Column(name="quantidade_funcionario_ponto_corte")
    private Integer quantidadeFuncionarioPontoCorte;

    @Column(name="quantidade_dias_atualizacao_senha")
    private Long quantidadeDiasAtualizacaoSenha;

    @Column(name="validacao_shape_ativa")
    private Boolean validacaoShapeAtiva;

    @Column(name="geracao_dae_ativa")
    private Boolean geracaoDaeAtiva;

    @Column(name="percentual_desconto_auto_infracao_fumaca")
    private Double percentualDescontoAutoInfracaoFumaca;

    @Column(name="qntd_dias_desconto_auto_infracao_fumaca")
    private Integer qntdDiasDescontoAutoInfracaoFumaca;

    @Column(name="qntd_dias_dae_declaracao_consumidor_origem_florestal")
    private Integer qntdDiasDaeDeclaracaoConsumidorOrigemFlorestal;

    @Column(name = "acesso_liberado_produtividade")
    private Boolean acessoLiberadoProdutividade;

    @Column(name = "pasta_temporaria_sistemas")
    private String pastaTemporariaSistemas;

    @Column(name="geo_webservice_protocol", length = 10)
    private String geoWebserviceProtocol;

    @Column(name="geo_webservice_host")
    private String geoWebserviceHost;

    @Column(name="geo_webservice_port", length = 10)
    private String geoWebservicePort;

    @Column(name="geo_webservice_host_assets")
    private String geoWebserviceHostAssets;

    @Column(name="geo_token_google_maps")
    private String geoTokenGoogleMaps;

    @Column(name="geo_client_id_google_maps")
    private String geoClientIdGoogleMaps;

    @Column(name="geo_limite_upload_shape")
    private Integer geoLimiteUploadShape;

    @Column(name="limite_numero_imagens")
    private Integer limiteNumeroImagens;

    @Column(name="resize_width")
    private Integer resizeWidth;

    @Column(name="resize_height")
    private Integer resizeHeight;

    @Column(name="usuario_web_service_viproc")
    private String usuarioWebServiceViproc;

    @Column(name="senha_web_service_viproc")
    private String senhaWebServiceViproc;

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coema_id")
	private Coema coema = new Coema();

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setor_superintendencia_id")
	private Setor superintendencia = new Setor();

    @ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "parametro_funcionario_oficio",
			schema = "geral",
			joinColumns = @JoinColumn(name = "parametro_id"),
			inverseJoinColumns = @JoinColumn(name = "funcionario_id"))
    private List<Funcionario> listFuncionarioAssinarOficio = new ArrayList<>();

    @Column(name = "liberar_visualizacao_processo_fora_pauta")
    private Boolean liberarVisualizacaoProcessoForaPauta;

    @Column(name = "validar_pendencia_financeira")
    private Boolean validarPendenciaFinanceira;

    @Column(name = "inserir_registros_siga")
    private Boolean inserirRegistrosSiga;
    @Column(name = "limite_vigencia_contrato")
    private Integer limiteVigenciaContrato;


    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcionario_id")
    private Funcionario responsavelSetorContrato;

    @Column(name = "liberar_contrato_pessoa_fisica")
    private Boolean liberarContratoPessoaFisica;

    @Column(name = "permitir_inserir_numero_contrato_manual")
    private Boolean permitirInserirNumeroContratoManual;


    @Column(name = "email_suporte")
    private String emailSuporte;

    @Column(name = "validar_email_unico_interessado")
    private Boolean validarEmailUnicoInteressado;

    @OneToMany(mappedBy = "parametro", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FaseAtendimento> fasesAtendimento;

    @Column(name = "data_inicio_processo_geo")
    @Temporal(TemporalType.DATE)
    private Date dataInicioProcessoGeo;

    @Column(name = "divida_honorarios", scale=5, precision=12)
    private BigDecimal dividaHonorarios;

    @Column(name = "divida_juros", scale=5, precision=12)
    private BigDecimal dividaJuros;

	@Column(name = "min_igpdi", scale=5, precision=12)
    private BigDecimal minIGPDI;

    @Column(name = "max_igpdi", scale=5, precision=12)
    private BigDecimal maxIGPDI;

    @ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "param_advog_assin_exec_fiscal",
			schema = "geral",
			joinColumns = @JoinColumn(name = "parametro_id"),
			inverseJoinColumns = @JoinColumn(name = "funcionario_id"))
    private List<Advogado> listAdvoAssinExecucaoFiscal = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "param_func_assin_exec_fiscal",
			schema = "geral",
			joinColumns = @JoinColumn(name = "parametro_id"),
			inverseJoinColumns = @JoinColumn(name = "funcionario_id"))
    private List<Funcionario> listFuncAssinExecucaoFiscal = new ArrayList<>();

    @OneToOne
    @JoinColumn(name="usuario_sistema_id")
    private Funcionario usuarioSistema = new Funcionario();

    @Column(name = "enviar_msg_divida_dae_pago")
    private Boolean enviarMsgDividaDaePago;

    @Column(name = "enviar_msg_divida_dae_vencido")
    private Boolean enviarMsgDividaDaeVencido;

    @Column(name = "enviar_msg_acordo_dae_entrada_pago")
    private Boolean enviarMsgAcordoDaeEntradaPago;

    @Column(name = "enviar_msg_acordo_dae_entrada_vencido")
    private Boolean enviarMsgAcordoDaeEntradaVencido;

    @Column(name = "enviar_msg_acordo_cancelado")
    private Boolean enviarMsgAcordoCancelado;

    @Column(name = "enviar_msg_acordo_finalizado")
    private Boolean enviarMsgAcordoFinalizado;

    @Column(name = "enviar_msg_acordo_em_atraso")
    private Boolean enviarMsgAcordoEmAtraso;

    @Column(name = "enviar_msg_acordo_regularizado")
    private Boolean enviarMsgAcordoRegularizado;

    @Column(name = "qtd_lim_parcelas_acordo_divida")
    private Integer qtdLimiteParcelasAcordoDivida;

    @Column(name = "valor_limite_parcela_pessoa_fisica", scale=2, precision=12)
    private BigDecimal valorLimiteParcelaPessoaFisica;

    @Column(name = "valor_limite_parcela_pessoa_juridica", scale=2, precision=12)
    private BigDecimal valorLimiteParcelaPessoaJuridica;

    @JoinColumn(name = "situacao_funcionario_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SituacaoFuncionario situacaoFuncionario;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="pais_brasil_id", referencedColumnName="id")
	private Pais paisBrasil = new Pais();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="estado_ceara_id", referencedColumnName="cd_uf")
	private UF estadoCeara = new UF();

    @Column(name="tamanho_imagem_editor_texto")
    private Long tamanhoImagemEditorTexto;

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nivel_hierarquico_maximo_id")
    private NivelHierarquico nivelHierarquicoMaximo = new NivelHierarquico();

    @Column(name = "liberar_regra_tramitacao")
    private Boolean liberarRegraTramitacao;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "parametro")
    private List<SetorTramitacao> listSetorTramitacao = new ArrayList<>();

    @Column(name="url_assinatura_digital")
    private String urlAssinaturaDigital;

    @Column(name="data_inicio_notificacao_processo")
    private Date dataInicioNotificacaoProcesso;

    @OneToOne
    @JoinColumn(name="tipo_taxa_id")
    private TipoTaxa tipoTaxa = new TipoTaxa();

    @OneToOne
    @JoinColumn(name="questionario_id")
    private Questionario questionario = new Questionario();

    @OneToOne
    @JoinColumn(name="usuario_natuur_web")
    private Funcionario funcioarioWeb = new Funcionario();

    @Column(name = "data_inicio_funcionamento_rama")
    @Temporal(TemporalType.DATE)
    private Date dataInicioFuncionamentoRama;

    @Column(name="libera_edicao_divida_paga")
    private Boolean liberaEdicaoDividaPaga;

    @Column(name="libera_geracao_dae_divida_paga")
    private Boolean liberaGeracaoDaeDividaPaga;

    @Column(name = "ged_ativo")
    private Boolean gedAtivo;

    @Column(name = "quantidade_tabela_maxima_coema")
    private Integer quantidadeTabelaMaximaCoema;

    @Column(name="endereco_producao_natuur_web")
	private String enderecoProducaoNatuurWeb;

	@Column(name = "prazo_envio_arquivos_requerimento")
	private Integer prazoEnvioArquivosRequerimento;

	@Column(name = "prazo_solucao_pendencia_documentacao_requerimento")
	private Integer prazoSolucaoPendenciaDocumentacaoRequerimento;

	@Column(name = "prazo_seguranca_pagamento_taxa")
	private Integer prazoSegurancaPagamentoTaxa;

	@Column(name = "prazo_solucao_pendencia_documentacao_requerimento_pre_ged")
	private Integer prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED;

	@Column(name = "prazo_atendimento_requerimento_pre_ged")
	private Integer prazoAtendimentoRequerimentoPreGED;

	@Column(name = "tamanho_pagina_descarte_requerimento")
	private Integer tamanhoPaginaDescarteRequerimento;

    @Column(name = "hostname_ftp_charge")
    private String hostnameFtpCharge;

    @Column(name="username_ftp_charge")
    private String usernameFtpCharge;

    @Column(name="password_ftp_charge")
    private String passwordFtpCharge;

    @Column(name="path_ftp_charge")
    private String pathFtpCharge;

    /************************
     *  Getters and Setters *
     ************************/

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Especifica a quantidade máxima de atendentes do atendimento e usado na
	 * classe AgendamentoRequerimentoSession nos metodos findHorasDisponiveis e verificaQtdAgendamento
	 */
	public Long getQtdMaxAtendentes() {
		return qtdMaxAtendentes;
	}

	public void setQtdMaxAtendentes(Long qtdMaxAtendentes) {
		this.qtdMaxAtendentes = qtdMaxAtendentes;
	}

	/**
	 * Caminho da máquina local para salvar os arquivos temporarios do shape
	 * no momento do upload.
	 * @return
	 */
	public String getCaminhoArquivoUploadShape() {
		return caminhoArquivoUploadShape;
	}

	public void setCaminhoArquivoUploadShape(String caminhoArquivoUploadShape) {
		this.caminhoArquivoUploadShape = caminhoArquivoUploadShape;
	}

	/**
	 * Usuario FTP
	 * @return
	 */
	public String getUsuarioFtp() {
		return usuarioFtp;
	}

	public void setUsuarioFtp(String usuarioFtp) {
		this.usuarioFtp = usuarioFtp;
	}

	/**
	 * Senha para acesso do FTP
	 * @return
	 */
	public String getSenhaFtp() {
		return senhaFtp;
	}

	public void setSenhaFtp(String senhaFtp) {
		this.senhaFtp = senhaFtp;
	}

	/**
	 * Path onde ira salvar os arquivos dos clientes no FTP
	 * @return
	 */
	public String getPathFtp() {
		return pathFtp;
	}

	public void setPathFtp(String pathFtp) {
		this.pathFtp = pathFtp;
	}

	/**
	 * Ip do servidor de FTP do upload arquivo shape
	 * @return
	 */
	public String getIpFtp() {
		return ipFtp;
	}

	public void setIpFtp(String ipFtp) {
		this.ipFtp = ipFtp;
	}

	/**
	 * Eemail natuur.atendimento@semace.ce.gov.br
	 * utilizado para envio de email para os clientes da semace
	 * @return
	 */
	public String getEmailAtendimento() {
		return emailAtendimento;
	}

	public void setEmailAtendimento(String emailAtendimento) {
		this.emailAtendimento = emailAtendimento;
	}

	/**
	 * Senha do email natuur.atendimento@semace.ce.gov.br
	 * utilizado para envio de email para os clientes da semace
	 * @return
	 */
	public String getSenhaEmailAtendimento() {
		return senhaEmailAtendimento;
	}

	public void setSenhaEmailAtendimento(String senhaEmailAtendimento) {
		this.senhaEmailAtendimento = senhaEmailAtendimento;
	}

	/**
	 * Configuração do servidor de email da semace
	 * @return
	 */
	public String getServidorEmail() {
		return servidorEmail;
	}

	public void setServidorEmail(String servidorEmail) {
		this.servidorEmail = servidorEmail;
	}

	public BigDecimal getValorFaturamentoPontoCorte() {
		return valorFaturamentoPontoCorte;
	}

	public void setValorFaturamentoPontoCorte(BigDecimal valorFaturamentoPontoCorte) {
		this.valorFaturamentoPontoCorte = valorFaturamentoPontoCorte;
	}

	public BigDecimal getValorAreaPontoCorte() {
		return valorAreaPontoCorte;
	}

	public void setValorAreaPontoCorte(BigDecimal valorAreaPontoCorte) {
		this.valorAreaPontoCorte = valorAreaPontoCorte;
	}

	public Integer getQuantidadeFuncionarioPontoCorte() {
		return quantidadeFuncionarioPontoCorte;
	}

	public void setQuantidadeFuncionarioPontoCorte(
			Integer quantidadeFuncionarioPontoCorte) {
		this.quantidadeFuncionarioPontoCorte = quantidadeFuncionarioPontoCorte;
	}

	public Boolean getValidacaoShapeAtiva() {
		return validacaoShapeAtiva;
	}

	public void setValidacaoShapeAtiva(Boolean validacaoShapeAtiva) {
		this.validacaoShapeAtiva = validacaoShapeAtiva;
	}

	public String getSenhaUsuarioArquivosShape() {
		return senhaUsuarioArquivosShape;
	}

	public void setSenhaUsuarioArquivosShape(String senhaUsuarioArquivosShape) {
		this.senhaUsuarioArquivosShape = senhaUsuarioArquivosShape;
	}

	public String getDominioArquivosShape() {
		return dominioArquivosShape;
	}

	public void setDominioArquivosShape(String dominioArquivosShape) {
		this.dominioArquivosShape = dominioArquivosShape;
	}

	public String getEnderecoArquivosShape() {
		return enderecoArquivosShape;
	}

	public void setEnderecoArquivosShape(String enderecoArquivosShape) {
		this.enderecoArquivosShape = enderecoArquivosShape;
	}

	public String getUsuarioArquivosShape() {
		return usuarioArquivosShape;
	}

	public void setUsuarioArquivosShape(String usuarioArquivosShape) {
		this.usuarioArquivosShape = usuarioArquivosShape;
	}

	public Long getQuantidadeDiasAtualizacaoSenha() {
		return quantidadeDiasAtualizacaoSenha;
	}

	public void setQuantidadeDiasAtualizacaoSenha(
			Long quantidadeDiasAtualizacaoSenha) {
		this.quantidadeDiasAtualizacaoSenha = quantidadeDiasAtualizacaoSenha;
	}

	public Boolean getGeracaoDaeAtiva() {
		return geracaoDaeAtiva;
	}

	public void setGeracaoDaeAtiva(Boolean geracaoDaeAtiva) {
		this.geracaoDaeAtiva = geracaoDaeAtiva;
	}

	public Double getPercentualDescontoAutoInfracaoFumaca() {
		return percentualDescontoAutoInfracaoFumaca;
	}

	public void setPercentualDescontoAutoInfracaoFumaca(
			Double percentualDescontoAutoInfracaoFumaca) {
		this.percentualDescontoAutoInfracaoFumaca = percentualDescontoAutoInfracaoFumaca;
	}

	public Integer getQntdDiasDescontoAutoInfracaoFumaca() {
		return qntdDiasDescontoAutoInfracaoFumaca;
	}

	public void setQntdDiasDescontoAutoInfracaoFumaca(
			Integer qntdDiasDescontoAutoInfracaoFumaca) {
		this.qntdDiasDescontoAutoInfracaoFumaca = qntdDiasDescontoAutoInfracaoFumaca;
	}

	/**
	 * Quantidade de dias para o vencimento de um dae de declaração
	 * de consumidor de origem florestal
	 * @return
	 */
	public Integer getQntdDiasDaeDeclaracaoConsumidorOrigemFlorestal() {
		return qntdDiasDaeDeclaracaoConsumidorOrigemFlorestal;
	}

	public void setQntdDiasDaeDeclaracaoConsumidorOrigemFlorestal(
			Integer qntdDiasDaeDeclaracaoConsumidorOrigemFlorestal) {
		this.qntdDiasDaeDeclaracaoConsumidorOrigemFlorestal = qntdDiasDaeDeclaracaoConsumidorOrigemFlorestal;
	}

	public Coema getCoema() {
		return coema;
	}

	public void setCoema(Coema coema) {
		this.coema = coema;
	}

	public Boolean getAcessoLiberadoProdutividade() {
		return acessoLiberadoProdutividade;
	}

	public void setAcessoLiberadoProdutividade(Boolean acessoLiberadoProdutividade) {
		this.acessoLiberadoProdutividade = acessoLiberadoProdutividade;
	}

	public Setor getSuperintendencia() {
		return superintendencia;
	}

	public void setSuperintendencia(Setor superintendencia) {
		this.superintendencia = superintendencia;
	}

	public String getPastaCompartilhamentoArquivosShape() {
		return pastaCompartilhamentoArquivosShape;
	}

	public void setPastaCompartilhamentoArquivosShape(String pastaCompartilhamentoArquivosShape) {
		this.pastaCompartilhamentoArquivosShape = pastaCompartilhamentoArquivosShape;
	}

	public String getPastaAtendimentoArquivosShape() {
		return pastaAtendimentoArquivosShape;
	}

	public void setPastaAtendimentoArquivosShape(String pastaAtendimentoArquivosShape) {
		this.pastaAtendimentoArquivosShape = pastaAtendimentoArquivosShape;
	}

	public String getPastaGeoArquivosShape() {
		return pastaGeoArquivosShape;
	}

	public void setPastaGeoArquivosShape(String pastaGeoArquivosShape) {
		this.pastaGeoArquivosShape = pastaGeoArquivosShape;
	}

	public String getPastaArquivoArquivosShape() {
		return pastaArquivoArquivosShape;
	}

	public void setPastaArquivoArquivosShape(String pastaArquivoArquivosShape) {
		this.pastaArquivoArquivosShape = pastaArquivoArquivosShape;
	}

	public String getPastaTemporariaSistemas() {
		return pastaTemporariaSistemas;
	}

	public void setPastaTemporariaSistemas(String pastaTemporariaSistemas) {
		this.pastaTemporariaSistemas = pastaTemporariaSistemas;
	}

	public String getPastaBaseCartograficaShape() {
		return pastaBaseCartograficaShape;
	}

	public void setPastaBaseCartograficaShape(String pastaBaseCartograficaShape) {
		this.pastaBaseCartograficaShape = pastaBaseCartograficaShape;
	}

	public List<Funcionario> getListFuncionarioAssinarOficio() {
		return listFuncionarioAssinarOficio;
	}

	public void setListFuncionarioAssinarOficio(
			List<Funcionario> listFuncionarioAssinarOficio) {
		this.listFuncionarioAssinarOficio = listFuncionarioAssinarOficio;
	}

	public String getSenhaUsuarioSSH() {
		return senhaUsuarioSSH;
	}

	public void setSenhaUsuarioSSH(String senhaUsuarioSSH) {
		this.senhaUsuarioSSH = senhaUsuarioSSH;
	}

	public String getUsuarioSSH() {
		return usuarioSSH;
	}

	public void setUsuarioSSH(String usuarioSSH) {
		this.usuarioSSH = usuarioSSH;
	}

	public String getGeoWebservicePort() {
		return geoWebservicePort;
	}

	public void setGeoWebservicePort(String geoWebservicePort) {
		this.geoWebservicePort = geoWebservicePort;
	}

	public String getGeoTokenGoogleMaps() {
		return geoTokenGoogleMaps;
	}

	public void setGeoTokenGoogleMaps(String geoTokenGoogleMaps) {
		this.geoTokenGoogleMaps = geoTokenGoogleMaps;
	}

	public String getGeoWebserviceProtocol() {
		return geoWebserviceProtocol;
	}

	public void setGeoWebserviceProtocol(String geoWebserviceProtocol) {
		this.geoWebserviceProtocol = geoWebserviceProtocol;
	}

	public String getGeoWebserviceHost() {
		return geoWebserviceHost;
	}

	public void setGeoWebserviceHost(String geoWebserviceHost) {
		this.geoWebserviceHost = geoWebserviceHost;
	}

	public String getPastaTemp() {
		return pastaTemp;
	}

	public void setPastaTemp(String pastaTemp) {
		this.pastaTemp = pastaTemp;
	}

	public String getPastaCompartilhamentoImagens() {
		return pastaCompartilhamentoImagens;
	}

	public void setPastaCompartilhamentoImagens(String pastaCompartilhamentoImagens) {
		this.pastaCompartilhamentoImagens = pastaCompartilhamentoImagens;
	}

	public String getPastaMapaTematico() {
		return pastaMapaTematico;
	}

	public void setPastaMapaTematico(String pastaMapaTematico) {
		this.pastaMapaTematico = pastaMapaTematico;
	}

	public String getPastaImagensMapaTematico() {
		return pastaImagensMapaTematico;
	}

	public void setPastaImagensMapaTematico(String pastaImagensMapaTematico) {
		this.pastaImagensMapaTematico = pastaImagensMapaTematico;
	}

	public String getGeoWebserviceHostAssets() {
		return geoWebserviceHostAssets;
	}

	public void setGeoWebserviceHostAssets(String geoWebserviceHostAssets) {
		this.geoWebserviceHostAssets = geoWebserviceHostAssets;
	}

	public String getPastaImportacaoBaseCartograficaShape() {
		return pastaImportacaoBaseCartograficaShape;
	}

	public void setPastaImportacaoBaseCartograficaShape(
			String pastaImportacaoBaseCartograficaShape) {
		this.pastaImportacaoBaseCartograficaShape = pastaImportacaoBaseCartograficaShape;
	}

	public String getCaminhoPastaCompartilhamentoSSH() {
		return caminhoPastaCompartilhamentoSSH;
	}

	public void setCaminhoPastaCompartilhamentoSSH(
			String caminhoPastaCompartilhamentoSSH) {
		this.caminhoPastaCompartilhamentoSSH = caminhoPastaCompartilhamentoSSH;
	}

	public Boolean getLiberarVisualizacaoProcessoForaPauta() {
		return liberarVisualizacaoProcessoForaPauta;
	}

	public void setLiberarVisualizacaoProcessoForaPauta(
			Boolean liberarVisualizacaoProcessoForaPauta) {
		this.liberarVisualizacaoProcessoForaPauta = liberarVisualizacaoProcessoForaPauta;
	}

	public Boolean getValidarPendenciaFinanceira() {
		return validarPendenciaFinanceira;
	}

	public void setValidarPendenciaFinanceira(Boolean validarPendenciaFinanceira) {
		this.validarPendenciaFinanceira = validarPendenciaFinanceira;
	}

	public String getUsuarioWebServiceViproc() {
		return usuarioWebServiceViproc;
	}

	public void setUsuarioWebServiceViproc(String usuarioWebServiceViproc) {
		this.usuarioWebServiceViproc = usuarioWebServiceViproc;
	}

	public String getSenhaWebServiceViproc() {
		return senhaWebServiceViproc;
	}

	public void setSenhaWebServiceViproc(String senhaWebServiceViproc) {
		this.senhaWebServiceViproc = senhaWebServiceViproc;
	}

	public Boolean getInserirRegistrosSiga() {
		return inserirRegistrosSiga;
	}

	public void setInserirRegistrosSiga(Boolean inserirRegistrosSiga) {
		this.inserirRegistrosSiga = inserirRegistrosSiga;
	}

	public String getEmailSuporte() {
		return emailSuporte;
	}

	public void setEmailSuporte(String emailSuporte) {
		this.emailSuporte = emailSuporte;
	}

	public Boolean getValidarEmailUnicoInteressado() {
		return validarEmailUnicoInteressado;
	}

	public void setValidarEmailUnicoInteressado(Boolean validarEmailUnicoInteressado) {
		this.validarEmailUnicoInteressado = validarEmailUnicoInteressado;
	}

	public Integer getLimiteVigenciaContrato() {
		return limiteVigenciaContrato;
	}

	public void setLimiteVigenciaContrato(Integer limiteVigenciaContrato) {
		this.limiteVigenciaContrato = limiteVigenciaContrato;
	}

	public Boolean getLiberarContratoPessoaFisica() {
		return liberarContratoPessoaFisica;
	}

	public void setLiberarContratoPessoaFisica(Boolean liberarContratoPessoaFisica) {
		this.liberarContratoPessoaFisica = liberarContratoPessoaFisica;
	}

	public Boolean getPermitirInserirNumeroContratoManual() {
		return permitirInserirNumeroContratoManual;
	}

	public void setPermitirInserirNumeroContratoManual(
			Boolean permitirInserirNumeroContratoManual) {
		this.permitirInserirNumeroContratoManual = permitirInserirNumeroContratoManual;
	}

	public Funcionario getResponsavelSetorContrato() {
		return responsavelSetorContrato;
	}

	public void setResponsavelSetorContrato(Funcionario responsavelSetorContrato) {
		this.responsavelSetorContrato = responsavelSetorContrato;
	}

	public String getGeoClientIdGoogleMaps() {
		return geoClientIdGoogleMaps;
	}

	public void setGeoClientIdGoogleMaps(String geoClientIdGoogleMaps) {
		this.geoClientIdGoogleMaps = geoClientIdGoogleMaps;
	}

	public Integer getGeoLimiteUploadShapeMemoria() {
		Integer posicaoMemoria = 1048576;
		Integer totalMegabytes = 10;

		if (geoLimiteUploadShape == null || geoLimiteUploadShape == 0) {
			return posicaoMemoria;
		}

		totalMegabytes = geoLimiteUploadShape;
		posicaoMemoria = (totalMegabytes * posicaoMemoria);

		return posicaoMemoria;
	}

	public Integer getGeoLimiteUploadShape() {
		return geoLimiteUploadShape;
	}

	public void setGeoLimiteUploadShape(Integer geoLimiteUploadShape) {
		this.geoLimiteUploadShape = geoLimiteUploadShape;
	}

	public List<FaseAtendimento> getFasesAtendimento() {
		return fasesAtendimento;
	}

	public void setFasesAtendimento(List<FaseAtendimento> fasesAtendimento) {
		this.fasesAtendimento = fasesAtendimento;
	}

	public Date getDataInicioProcessoGeo() {
		return dataInicioProcessoGeo;
	}

	public void setDataInicioProcessoGeo(Date dataInicioProcessoGeo) {
		this.dataInicioProcessoGeo = dataInicioProcessoGeo;
	}

	public String getPastaUploadImagens() {
		return pastaUploadImagens;
	}

	public void setPastaUploadImagens(String pastaUploadImagens) {
		this.pastaUploadImagens = pastaUploadImagens;
	}

	public Integer getLimiteNumeroImagens() {
		return limiteNumeroImagens;
	}

	public void setLimiteNumeroImagens(Integer limiteNumeroImagens) {
		this.limiteNumeroImagens = limiteNumeroImagens;
	}

	public Integer getResizeWidth() {
		return resizeWidth;
	}

	public void setResizeWidth(Integer resizeWidth) {
		this.resizeWidth = resizeWidth;
	}

	public Integer getResizeHeight() {
		return resizeHeight;
	}

	public void setResizeHeight(Integer resizeHeight) {
		this.resizeHeight = resizeHeight;
	}

	public BigDecimal getDividaJurosMensal() {
		return dividaJuros;
	}

	public BigDecimal getDividaJuros() {
		return dividaJuros;
	}

	public void setDividaJuros(BigDecimal dividaJuros) {
		this.dividaJuros = dividaJuros;
	}

	public BigDecimal getMinIGPDI() {
		return minIGPDI;
	}

	public void setMinIGPDI(BigDecimal minIGPDI) {
		this.minIGPDI = minIGPDI;
	}

	public BigDecimal getMaxIGPDI() {
		return maxIGPDI;
	}

	public void setMaxIGPDI(BigDecimal maxIGPDI) {
		this.maxIGPDI = maxIGPDI;
	}

	public List<Advogado> getListAdvoAssinExecucaoFiscal() {
		return listAdvoAssinExecucaoFiscal;
	}

	public void setListAdvoAssinExecucaoFiscal(
			List<Advogado> listAdvoAssinExecucaoFiscal) {
		this.listAdvoAssinExecucaoFiscal = listAdvoAssinExecucaoFiscal;
	}

	public List<Funcionario> getListFuncAssinExecucaoFiscal() {
		return listFuncAssinExecucaoFiscal;
	}

	public void setListFuncAssinExecucaoFiscal(List<Funcionario> listFuncAssinExecucaoFiscal) {
		this.listFuncAssinExecucaoFiscal = listFuncAssinExecucaoFiscal;
	}

	public Funcionario getUsuarioSistema() {
		return usuarioSistema;
	}

	public void setUsuarioSistema(Funcionario usuarioSistema) {
		this.usuarioSistema = usuarioSistema;
	}

	public Boolean getEnviarMsgDividaDaePago() {
		return enviarMsgDividaDaePago;
	}

	public void setEnviarMsgDividaDaePago(Boolean enviarMsgDividaDaePago) {
		this.enviarMsgDividaDaePago = enviarMsgDividaDaePago;
	}

	public Boolean getEnviarMsgDividaDaeVencido() {
		return enviarMsgDividaDaeVencido;
	}

	public void setEnviarMsgDividaDaeVencido(Boolean enviarMsgDividaDaeVencido) {
		this.enviarMsgDividaDaeVencido = enviarMsgDividaDaeVencido;
	}

	public Boolean getEnviarMsgAcordoDaeEntradaPago() {
		return enviarMsgAcordoDaeEntradaPago;
	}

	public void setEnviarMsgAcordoDaeEntradaPago(
			Boolean enviarMsgAcordoDaeEntradaPago) {
		this.enviarMsgAcordoDaeEntradaPago = enviarMsgAcordoDaeEntradaPago;
	}

	public Boolean getEnviarMsgAcordoDaeEntradaVencido() {
		return enviarMsgAcordoDaeEntradaVencido;
	}

	public void setEnviarMsgAcordoDaeEntradaVencido(
			Boolean enviarMsgAcordoDaeEntradaVencido) {
		this.enviarMsgAcordoDaeEntradaVencido = enviarMsgAcordoDaeEntradaVencido;
	}

	public Boolean getEnviarMsgAcordoCancelado() {
		return enviarMsgAcordoCancelado;
	}

	public void setEnviarMsgAcordoCancelado(Boolean enviarMsgAcordoCancelado) {
		this.enviarMsgAcordoCancelado = enviarMsgAcordoCancelado;
	}

	public Boolean getEnviarMsgAcordoFinalizado() {
		return enviarMsgAcordoFinalizado;
	}

	public void setEnviarMsgAcordoFinalizado(Boolean enviarMsgAcordoFinalizado) {
		this.enviarMsgAcordoFinalizado = enviarMsgAcordoFinalizado;
	}

	public Integer getQtdLimiteParcelasAcordoDivida() {
		return qtdLimiteParcelasAcordoDivida;
	}

	public void setQtdLimiteParcelasAcordoDivida(
			Integer qtdLimiteParcelasAcordoDivida) {
		this.qtdLimiteParcelasAcordoDivida = qtdLimiteParcelasAcordoDivida;
	}

	public BigDecimal getDividaHonorarios() {
		return dividaHonorarios;
	}

	public void setDividaHonorarios(BigDecimal dividaHonorarios) {
		this.dividaHonorarios = dividaHonorarios;
	}

	public BigDecimal getValorLimiteParcelaPessoaFisica() {
		return valorLimiteParcelaPessoaFisica;
	}

	public void setValorLimiteParcelaPessoaFisica(
			BigDecimal valorLimiteParcelaPessoaFisica) {
		this.valorLimiteParcelaPessoaFisica = valorLimiteParcelaPessoaFisica;
	}

	public BigDecimal getValorLimiteParcelaPessoaJuridica() {
		return valorLimiteParcelaPessoaJuridica;
	}

	public void setValorLimiteParcelaPessoaJuridica(
			BigDecimal valorLimiteParcelaPessoaJuridica) {
		this.valorLimiteParcelaPessoaJuridica = valorLimiteParcelaPessoaJuridica;
	}

	public SituacaoFuncionario getSituacaoFuncionario() {
		return situacaoFuncionario;
	}

	public void setSituacaoFuncionario(SituacaoFuncionario situacaoFuncionario) {
		this.situacaoFuncionario = situacaoFuncionario;
	}
	public Pais getPaisBrasil() {
		return paisBrasil;
	}

	public void setPaisBrasil(Pais paisBrasil) {
		this.paisBrasil = paisBrasil;
	}

	public UF getEstadoCeara() {
		return estadoCeara;
	}

	public void setEstadoCeara(UF estadoCeara) {
		this.estadoCeara = estadoCeara;
	}

	public Long getTamanhoImagemEditorTexto() {
		return tamanhoImagemEditorTexto;
	}

	public void setTamanhoImagemEditorTexto(Long tamanhoImagemEditorTexto) {
		this.tamanhoImagemEditorTexto = tamanhoImagemEditorTexto;
	}

	public NivelHierarquico getNivelHierarquicoMaximo() {
		return nivelHierarquicoMaximo;
	}

	public void setNivelHierarquicoMaximo(NivelHierarquico nivelHierarquicoMaximo) {
		this.nivelHierarquicoMaximo = nivelHierarquicoMaximo;
	}

	public Boolean getLiberarRegraTramitacao() {
		return liberarRegraTramitacao;
	}

	public void setLiberarRegraTramitacao(Boolean liberarRegraTramitacao) {
		this.liberarRegraTramitacao = liberarRegraTramitacao;
	}

	public List<SetorTramitacao> getListSetorTramitacao() {
		return listSetorTramitacao;
	}

	public void setListSetorTramitacao(List<SetorTramitacao> listSetorTramitacao) {
		this.listSetorTramitacao = listSetorTramitacao;
	}

	public String getUrlAssinaturaDigital() {
		return urlAssinaturaDigital;
	}

	public void setUrlAssinaturaDigital(String urlAssinaturaDigital) {
		this.urlAssinaturaDigital = urlAssinaturaDigital;
	}

	public Date getDataInicioNotificacaoProcesso() {
		return dataInicioNotificacaoProcesso;
	}

	public void setDataInicioNotificacaoProcesso(
			Date dataInicioNotificacaoProcesso) {
		this.dataInicioNotificacaoProcesso = dataInicioNotificacaoProcesso;
	}

	public TipoTaxa getTipoTaxa() {
		return tipoTaxa;
	}

	public void setTipoTaxa(TipoTaxa tipoTaxa) {
		this.tipoTaxa = tipoTaxa;
	}

	public Questionario getQuestionario() {
		return questionario;
	}

	public void setQuestionario(Questionario questionario) {
		this.questionario = questionario;
	}

	public Funcionario getFuncioarioWeb() {
		return funcioarioWeb;
	}

	public void setFuncioarioWeb(Funcionario funcioarioWeb) {
		this.funcioarioWeb = funcioarioWeb;
	}

	public Date getDataInicioFuncionamentoRama() {
		return dataInicioFuncionamentoRama;
	}

	public void setDataInicioFuncionamentoRama(Date dataInicioFuncionamentoRama) {
		this.dataInicioFuncionamentoRama = dataInicioFuncionamentoRama;
	}

	public Boolean getLiberaEdicaoDividaPaga() {
		return liberaEdicaoDividaPaga;
	}

	public void setLiberaEdicaoDividaPaga(Boolean liberaEdicaoDividaPaga) {
		this.liberaEdicaoDividaPaga = liberaEdicaoDividaPaga;
	}

	public Boolean getLiberaGeracaoDaeDividaPaga() {
		return liberaGeracaoDaeDividaPaga;
	}

	public void setLiberaGeracaoDaeDividaPaga(Boolean liberaGeracaoDaeDividaPaga) {
		this.liberaGeracaoDaeDividaPaga = liberaGeracaoDaeDividaPaga;
	}

	public Boolean getEnviarMsgAcordoEmAtraso() {
		return enviarMsgAcordoEmAtraso;
	}

	public void setEnviarMsgAcordoEmAtraso(Boolean enviarMsgAcordoEmAtraso) {
		this.enviarMsgAcordoEmAtraso = enviarMsgAcordoEmAtraso;
	}

	public Boolean getEnviarMsgAcordoRegularizado() {
		return enviarMsgAcordoRegularizado;
	}

	public void setEnviarMsgAcordoRegularizado(
			Boolean enviarMsgAcordoRegularizado) {
		this.enviarMsgAcordoRegularizado = enviarMsgAcordoRegularizado;
	}

	// O GED ficará inativo no Natuur-Online durante a fase de teste em produção.
	// O "false" será removido do "if" apenas quando o GED for definitivamente implantado em produção.
	public Boolean isGedAtivo() {
		return gedAtivo;
	}

	public void setGedAtivo(Boolean gedAtivo) {
		this.gedAtivo = gedAtivo;
	}

	public Integer getQuantidadeTabelaMaximaCoema() {
		return quantidadeTabelaMaximaCoema;
	}

	public void setQuantidadeTabelaMaximaCoema(Integer quantidadeTabelaMaximaCoema) {
		this.quantidadeTabelaMaximaCoema = quantidadeTabelaMaximaCoema;
	}

		public String getEnderecoProducaoNatuurWeb() {
		return enderecoProducaoNatuurWeb;
	}

	public void setEnderecoProducaoNatuurWeb(String enderecoProducaoNatuurWeb) {
		this.enderecoProducaoNatuurWeb = enderecoProducaoNatuurWeb;
	}

	public Integer getPrazoEnvioArquivosRequerimento() {
		return prazoEnvioArquivosRequerimento;
	}

	public void setPrazoEnvioArquivosRequerimento(Integer prazoEnvioArquivosRequerimento) {
		this.prazoEnvioArquivosRequerimento = prazoEnvioArquivosRequerimento;
	}

	public Integer getPrazoSolucaoPendenciaDocumentacaoRequerimento() {
		return prazoSolucaoPendenciaDocumentacaoRequerimento;
	}

	public void setPrazoSolucaoPendenciaDocumentacaoRequerimento(Integer prazoSolucaoPendenciaDocumentacaoRequerimento) {
		this.prazoSolucaoPendenciaDocumentacaoRequerimento = prazoSolucaoPendenciaDocumentacaoRequerimento;
	}

	public Integer getPrazoSegurancaPagamentoTaxa() {
		return prazoSegurancaPagamentoTaxa;
	}

	public void setPrazoSegurancaPagamentoTaxa(Integer prazoSegurancaPagamentoTaxa) {
		this.prazoSegurancaPagamentoTaxa = prazoSegurancaPagamentoTaxa;
	}

	public Integer getPrazoSolucaoPendenciaDocumentacaoRequerimentoPreGED() {
		return prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED;
	}

	public void setPrazoSolucaoPendenciaDocumentacaoRequerimentoPreGED(Integer prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED) {
		this.prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED = prazoSolucaoPendenciaDocumentacaoRequerimentoPreGED;
	}

	public Integer getPrazoAtendimentoRequerimentoPreGED() {
		return prazoAtendimentoRequerimentoPreGED;
	}

	public void setPrazoAtendimentoRequerimentoPreGED(Integer prazoAtendimentoRequerimentoPreGED) {
		this.prazoAtendimentoRequerimentoPreGED = prazoAtendimentoRequerimentoPreGED;
	}

	public Integer getTamanhoPaginaDescarteRequerimento() {
		return tamanhoPaginaDescarteRequerimento;
	}

	public void setTamanhoPaginaDescarteRequerimento(Integer tamanhoPaginaDescarteRequerimento) {
		this.tamanhoPaginaDescarteRequerimento = tamanhoPaginaDescarteRequerimento;
	}

	public String getHostnameFtpCharge() {
		return hostnameFtpCharge;
	}

	public void setHostnameFtpCharge(String hostnameFtpCharge) {
		this.hostnameFtpCharge = hostnameFtpCharge;
	}

	public String getUsernameFtpCharge() {
		return usernameFtpCharge;
	}

	public void setUsernameFtpCharge(String usernameFtpCharge) {
		this.usernameFtpCharge = usernameFtpCharge;
	}

	public String getPasswordFtpCharge() {
		return passwordFtpCharge;
	}

	public void setPasswordFtpCharge(String passwordFtpCharge) {
		this.passwordFtpCharge = passwordFtpCharge;
	}

	public String getPathFtpCharge() {
		return pathFtpCharge;
	}

	public void setPathFtpCharge(String pathFtpCharge) {
		this.pathFtpCharge = pathFtpCharge;
	}
}