package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "historico_shape_processo")
public class HistoricoShapeProcesso implements Serializable{

	private static final long serialVersionUID = 8139288341984186709L;

	@Id
	@SequenceGenerator(sequenceName="seq_historio_shape_processo", name="seq_historio_shape_processo", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_historio_shape_processo")
	private Long id;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="shape_processo_id")
	private ShapeProcesso shapeProcesso;
	
	@Column(name="dt_alteracao")
	private Date data;
	
	private Boolean ativo;

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public ShapeProcesso getShapeProcesso() {
		return shapeProcesso;
	}

	public void setShapeProcesso(ShapeProcesso shapeProcesso) {
		this.shapeProcesso = shapeProcesso;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
