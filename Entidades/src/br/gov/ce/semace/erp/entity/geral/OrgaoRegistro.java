package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="geral", name="orgao_registro")
public class OrgaoRegistro implements Serializable{

	private static final long serialVersionUID = -8429467018166112163L;
	
	@Id @SequenceGenerator(sequenceName="geral.seq_orgao_registro", name="SEQORGAO_REGISTRO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQORGAO_REGISTRO")
	private Long id;
	
	private String descricao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
