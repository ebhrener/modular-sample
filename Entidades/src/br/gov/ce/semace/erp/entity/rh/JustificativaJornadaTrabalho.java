package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoJustificativaRegistroPonto;

@Entity
@Table(schema="rh", name="justificativa_jornada_trabalho")
@Audited
//@TypeDef(name="period", typeClass= JodaTypePeriodParser.class)
public class JustificativaJornadaTrabalho implements Serializable {

	private static final long serialVersionUID = -858392123598913777L;

	@Id
	@SequenceGenerator(sequenceName = "rh.seq_justificativa_jornada_trabalho", name = "seq_justificativa_jornada_trabalho", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_justificativa_jornada_trabalho")
	private long id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;

	@ManyToOne(optional = false)
	@JoinColumn(name = "cadastrante_id")
	private Funcionario cadastrante;

	@Column(name = "data_hora_cadastro")
	private Date dataHoraCadastro;

	@Column(name = "dia_justificado")
	private Date diaJustificado;

//	@Column(name = "horas")
//	@Type(type="period")
//	private Period horas;

	@Column(name = "tipo_justificativa")
	private TipoJustificativaRegistroPonto tipoJustificativa;

	@Column(name = "justificativa" )
//	@Length(max=255, message="Texto deve conter menos de 255 caracteres.")
	private String justificativa;

	@Column(name= "ativo")
	private boolean ativo = true;

	public JustificativaJornadaTrabalho() {
		super();
	}

//	public JustificativaJornadaTrabalho(Period horas, TipoJustificativaRegistroPonto tipoJustificativa, String justificativa) {
//		this.horas = horas;
//		this.tipoJustificativa = tipoJustificativa;
//		this.justificativa = justificativa;
//	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public Funcionario getCadastrante() {
		return cadastrante;
	}
	public void setCadastrante(Funcionario cadastrante) {
		this.cadastrante = cadastrante;
	}
	public Date getDataHoraCadastro() {
		return dataHoraCadastro;
	}
	public void setDataHoraCadastro(Date dataHoraCadastro) {
		this.dataHoraCadastro = dataHoraCadastro;
	}
	public Date getDiaJustificado() {
		return diaJustificado;
	}
	public void setDiaJustificado(Date diaJustificado) {
		this.diaJustificado = diaJustificado;
	}
//	public Period getHoras() {
//		return horas;
//	}
//	public void setHoras(Period horas) {
//		this.horas = horas;
//	}
	public TipoJustificativaRegistroPonto getTipoJustificativa() {
		return tipoJustificativa;
	}
	public void setTipoJustificativa(TipoJustificativaRegistroPonto tipoJustificativa) {
		this.tipoJustificativa = tipoJustificativa;
	}
	public String getJustificativa() {
		return justificativa;
	}
	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

//	public String getPeriod(){
//		PeriodFormatter formater = new PeriodFormatterBuilder().printZeroAlways().minimumPrintedDigits(1).appendHours().appendLiteral(":").minimumPrintedDigits(2).appendMinutes().toFormatter();
//		if(this.getHoras()== null){
//			return new String();
//		}
//		return formater.print(this.horas.normalizedStandard(PeriodType.time()));
//	}
//
//	public void setPeriod(String hora){
//		if (StringUtils.isNotBlank(hora)) {
//			if(this.horas == null){
//				this.horas = new Period();
//			}
//			if (StringUtils.isNotBlank(hora.split(":")[0])) {
//				this.horas = this.horas.withHours(Integer.parseInt(hora.split(":")[0]));
//			}
//			if (hora.length() > 1 && StringUtils.isNotBlank(hora.split(":")[1])) {
//				this.horas = this.horas.withMinutes(Integer.parseInt(hora.split(":")[1]));
//			}
//		}
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		JustificativaJornadaTrabalho other = (JustificativaJornadaTrabalho) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

}
