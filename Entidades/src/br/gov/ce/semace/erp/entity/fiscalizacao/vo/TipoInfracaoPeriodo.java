package br.gov.ce.semace.erp.entity.fiscalizacao.vo;

public class TipoInfracaoPeriodo {
	private String nomeTipoInfracao;
	private Integer mes;
	private Integer ano;
	private Long quantidade;
	
	public TipoInfracaoPeriodo() {
		super();
	}
	
	public TipoInfracaoPeriodo(String nomeTipoInfracao, Integer mes,
			Integer ano, Long quantidade) {
		super();
		this.nomeTipoInfracao = nomeTipoInfracao;
		this.mes = mes;
		this.ano = ano;
		this.quantidade = quantidade;
	}
	public String getNomeTipoInfracao() {
		return nomeTipoInfracao;
	}
	public void setNomeTipoInfracao(String nomeTipoInfracao) {
		this.nomeTipoInfracao = nomeTipoInfracao;
	}
	public Integer getMes() {
		return mes;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public Long getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}
	
}
