package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;


/**
 *
 * @author saulo
 *
 */

@Entity
@Audited
@Table(name="documento_questionario", schema="geral")
@Inheritance(strategy = InheritanceType.JOINED)
@SequenceGenerator(sequenceName = DocumentoQuestionario.SEQUENCE_NAME, name = DocumentoQuestionario.SEQUENCE_NAME, allocationSize=1)
public class DocumentoQuestionario implements Serializable{

	private static final long serialVersionUID = 2272042505056541437L;

	/**
	 * Nome da Sequence que existirá no banco, também que será  <i>conhecida</i> dentro da aplicação
	 * e nome do gerador para onde deve apontar o tipo de geração {@link GeneratedType#}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/03/2016 15:06:57
	 */
	public static final String SEQUENCE_NAME = "geral.seq_documento_questionario";



	@Id @Column(name="documento_questionario_id")
	@SequenceGenerator(sequenceName = "geral.seq_documento_questionario", name = "SEQ_DOCUMENTO_QUESTIONARIO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DOCUMENTO_QUESTIONARIO")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="questionario_id")
	private Questionario questionario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="documento_id",nullable = true)
	private Documento documento;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "documentoQuestionario", cascade = CascadeType.ALL, orphanRemoval=true)
	private List<RespostaQuestionario> respostasQuestionario = new ArrayList<RespostaQuestionario>();

	/** Métodos getters e setters */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Questionario getQuestionario() {
		return questionario;
	}

	public void setQuestionario(Questionario questionario) {
		this.questionario = questionario;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public List<RespostaQuestionario> getRespostasQuestionario() {
		return respostasQuestionario;
	}

	public void setRespostasQuestionario(
			List<RespostaQuestionario> respostasQuestionario) {
		this.respostasQuestionario = respostasQuestionario;
	}

}
