package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoArquivoShape;

/**
 * Não há registros na tabela nem idícios de que esta tabela está sendo utilizada
 */
@Deprecated
@Entity
@Audited
@Table(name="arquivo_shape", schema = "atendimento")
public class ArquivoShape implements Serializable{

	private static final long serialVersionUID = -5148901181181533445L;

	@Id @SequenceGenerator(sequenceName="atendimento.seq_arquivo_shape", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="agendamento_requerimento_id")
	private AgendamentoRequerimento agendamentoRequerimento;

	@Column(name="nome_arquivo")
	private String nomeArquivo;

	@Column(name="tipo_arquivo_shape")
	private TipoArquivoShape tipoArquivoShape;

	@Column(name="tipo_shape")
	private String tipoShape;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_envio")
	private Date dataEnvio;

	private Boolean ativo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AgendamentoRequerimento getAgendamentoRequerimento() {
		return agendamentoRequerimento;
	}

	public void setAgendamentoRequerimento(
			AgendamentoRequerimento agendamentoRequerimento) {
		this.agendamentoRequerimento = agendamentoRequerimento;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public Date getDataEnvio() {
		return dataEnvio;
	}

	public void setDataEnvio(Date dataEnvio) {
		this.dataEnvio = dataEnvio;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public TipoArquivoShape getTipoArquivoShape() {
		return tipoArquivoShape;
	}

	public void setTipoArquivoShape(TipoArquivoShape tipoArquivoShape) {
		this.tipoArquivoShape = tipoArquivoShape;
	}

	public String getTipoShape() {
		return tipoShape;
	}

	public void setTipoShape(String tipoShape) {
		this.tipoShape = tipoShape;
	}
}
