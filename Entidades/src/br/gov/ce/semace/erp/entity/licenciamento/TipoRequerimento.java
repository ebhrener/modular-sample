package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;


@Entity
@Audited
@Table(name = "tipo_requerimento", schema="atendimento")
public class TipoRequerimento implements Serializable{
	
	
	private static final long serialVersionUID = -7156296289880222015L;
	
	@Id
	@SequenceGenerator(sequenceName = "atendimento.seq_tipo_requerimento", name = "SEQREQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQREQ")
	private Long id;

	private String descricao;
	
	private Boolean publico;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tipoRequerimento")
	private List<TipoProcesso> tipoProcessos;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<TipoProcesso> getTipoProcessos() {
		return tipoProcessos;
	}

	public void setTipoProcessos(List<TipoProcesso> tipoProcessos) {
		this.tipoProcessos = tipoProcessos;
	}

	public Boolean getPublico() {
		return publico;
	}

	public void setPublico(Boolean publico) {
		this.publico = publico;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoRequerimento other = (TipoRequerimento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
