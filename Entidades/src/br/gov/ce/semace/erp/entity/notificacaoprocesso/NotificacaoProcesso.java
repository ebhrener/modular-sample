package br.gov.ce.semace.erp.entity.notificacaoprocesso;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.StatusNotificacaoProcesso;

/**
 * Esta tabela contém as informações referentes a notificação que será enviada ao usuário quando
 * houver uma movimentação do processo.
 *
 * @author joerlan.lima
 *
 */
@Audited
@Entity
@Table(name="notificacao_processo", schema="notificacao_processo")
public class NotificacaoProcesso implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 8483938866492705169L;

	@Id
	@SequenceGenerator(sequenceName="notificacao_processo.seq_notificacao_processo", name="notificacao_processo.seq_notificacao_processo", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="notificacao_processo.seq_notificacao_processo")
	private Long id;

	@Column(nullable= false, name="status_processo_id")
	private String statusProcessoID;

	@Column(nullable= false, name="movimento_processo_id")
	private String movimentoProcessoID;

	@Column(nullable= false, name="spu")
	private String spu;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(nullable=false, name="mapeamento_status_notificacoes_id")
	private MapeamentoStatusNotificacoes mapeamentoStatusNotificacoes;

	@Column(nullable= false, name="processo_id")
	private String processoID;

	@Column(nullable=false)
	private Boolean processado;

	@Column(nullable=false, name="data_cadastro")
	private Date dataCadastro;

	@Column(name="data_processamento")
	private Date dataProcessamento;

	private String log;

	@Column(name="status")
	@Enumerated(EnumType.ORDINAL)
	private StatusNotificacaoProcesso statusNotificacaoProcesso;

	private String email;
	private String telefone;

	//------------------------------------------------Gets e Setters---------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProcessoID() {
		return processoID;
	}

	public void setProcessoID(String processoID) {
		this.processoID = processoID;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Boolean getProcessado() {
		return processado;
	}

	public void setProcessado(Boolean processado) {
		this.processado = processado;
	}

	public Date getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public String getStatusProcessoID() {
		return statusProcessoID;
	}

	public void setStatusProcessoID(String statusProcessoID) {
		this.statusProcessoID = statusProcessoID;
	}

	public String getMovimentoProcessoID() {
		return movimentoProcessoID;
	}

	public void setMovimentoProcessoID(String movimentoProcessoID) {
		this.movimentoProcessoID = movimentoProcessoID;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public MapeamentoStatusNotificacoes getMapeamentoStatusNotificacoes() {
		return mapeamentoStatusNotificacoes;
	}

	public void setMapeamentoStatusNotificacoes(
			MapeamentoStatusNotificacoes mapeamentoStatusNotificacoes) {
		this.mapeamentoStatusNotificacoes = mapeamentoStatusNotificacoes;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public StatusNotificacaoProcesso getStatusNotificacaoProcesso() {
		return statusNotificacaoProcesso;
	}

	public void setStatusNotificacaoProcesso(StatusNotificacaoProcesso statusNotificacaoProcesso) {
		this.statusNotificacaoProcesso = statusNotificacaoProcesso;
	}

}