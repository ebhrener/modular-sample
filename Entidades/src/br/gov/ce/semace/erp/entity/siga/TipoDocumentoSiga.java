package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;

public class TipoDocumentoSiga implements Serializable {

	private static final long serialVersionUID = 3093699550643506834L;

	private Integer tipoDocumentoId;
	private String tipoDocumento;
	private Integer numAtual;
	private Boolean excluido = Boolean.FALSE;

	public TipoDocumentoSiga() {
		super();
	}

	public TipoDocumentoSiga(Integer tipoDocumentoId, String tipoDocumento, Integer numAtual, Boolean excluido) {
		this.tipoDocumentoId = tipoDocumentoId;
		this.tipoDocumento = tipoDocumento;
		this.numAtual = numAtual;
		this.excluido = excluido;
	}

	public Integer getTipoDocumentoId() {
		return tipoDocumentoId;
	}

	public void setTipoDocumentoId(Integer tipoDocumentoId) {
		this.tipoDocumentoId = tipoDocumentoId;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Integer getNumAtual() {
		return numAtual;
	}

	public void setNumAtual(Integer numAtual) {
		this.numAtual = numAtual;
	}

	public Boolean getExcluido() {
		return excluido;
	}

	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}
}