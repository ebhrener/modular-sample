package br.gov.ce.semace.erp.entity.atendimento.to;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import br.gov.ce.semace.erp.enuns.StatusAtendimento;

public class AgendamentoRequerimentoPainelTO implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -4127235435916843810L;

	private Long idAgendamentoRequerimento;

	private String numeroRequerimento;

	private String nomeInteressado;

	private String documentoInteressado;

	private Date dataAgendamento;

	private Date dataCadastro;

	private StatusAtendimento statusAtendimento;

	private Date dataAuditoria;

	private String nomeFuncionario;

	private String descricaoTipoProcesso;

	private String descricaoModalidade;

	private String descricaoSubTipoProcesso;

	private String displayTipoRequerimento;



	public AgendamentoRequerimentoPainelTO() {
		super();
	}

	public String getDisplayTipoRequerimentoCompleto(){
		StringBuilder builder = new StringBuilder();
		if(descricaoModalidade != null && !descricaoModalidade.trim().equals("")){
			builder.append(descricaoModalidade + " / ");
		}

		if(descricaoTipoProcesso != null && !descricaoTipoProcesso.trim().equals("")){
			builder.append(descricaoTipoProcesso + " / ");
		}

		if(descricaoSubTipoProcesso != null && !descricaoSubTipoProcesso.trim().equals("")){
			builder.append(descricaoSubTipoProcesso);
		}

		return builder.toString();
	}

	public AgendamentoRequerimentoPainelTO(BigInteger idAgendamentoRequerimento, String numeroRequerimento, String nomeInteressado,
			String documentoInteressado, Timestamp dataAuditoria, String nomeFuncionario,String descricaoTipoProcesso,String descricaoModalidade, String descricaoSubTipoProcesso) {
		super();
		this.idAgendamentoRequerimento = idAgendamentoRequerimento != null ? idAgendamentoRequerimento.longValue() : null;
		this.numeroRequerimento = numeroRequerimento;
		this.nomeInteressado = nomeInteressado;
		this.documentoInteressado = documentoInteressado;
		this.dataAuditoria = dataAuditoria;
		this.nomeFuncionario = nomeFuncionario;
		this.descricaoTipoProcesso = descricaoTipoProcesso;
		this.descricaoModalidade = descricaoModalidade;
		this.descricaoSubTipoProcesso = descricaoSubTipoProcesso;
	}

	public Long getIdAgendamentoRequerimento() {
		return idAgendamentoRequerimento;
	}

	public void setIdAgendamentoRequerimento(Long idAgendamentoRequerimento) {
		this.idAgendamentoRequerimento = idAgendamentoRequerimento;
	}

	public String getNumeroRequerimento() {
		return numeroRequerimento;
	}

	public void setNumeroRequerimento(String numeroRequerimento) {
		this.numeroRequerimento = numeroRequerimento;
	}

	public String getNomeInteressado() {
		return nomeInteressado;
	}

	public void setNomeInteressado(String nomeInteressado) {
		this.nomeInteressado = nomeInteressado;
	}

	public String getDocumentoInteressado() {
		return documentoInteressado;
	}

	public void setDocumentoInteressado(String documentoInteressado) {
		this.documentoInteressado = documentoInteressado;
	}

	public Date getDataAgendamento() {
		return dataAgendamento;
	}

	public void setDataAgendamento(Date dataAgendamento) {
		this.dataAgendamento = dataAgendamento;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getDisplayCutNome(int length) {
		String nome = nomeInteressado;
		if (nome != null && nome.length() > length) {
			return nome.substring(0, length - 3) + "...";
		}
		return nome;
	}

	public String getDisplayCutNomeFuncionario(int length) {
		String nome = nomeFuncionario;
		if (nome != null && nome.length() > length) {
			return nome.substring(0, length - 3) + "...";
		}
		return nome;
	}

	public StatusAtendimento getStatusAtendimento() {
		return statusAtendimento;
	}

	public void setStatusAtendimento(StatusAtendimento statusAtendimento) {
		this.statusAtendimento = statusAtendimento;
	}

	public Date getDataAuditoria() {
		return dataAuditoria;
	}

	public void setDataAuditoria(Date dataAuditoria) {
		this.dataAuditoria = dataAuditoria;
	}

	public String getNomeFuncionario() {
		return nomeFuncionario;
	}

	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}

	public String getDisplayTipoRequerimento() {
		return displayTipoRequerimento;
	}

	public void setDisplayTipoRequerimento(String displayTipoRequerimento) {
		this.displayTipoRequerimento = displayTipoRequerimento;
	}

	public String getDescricaoTipoProcesso() {
		return descricaoTipoProcesso;
	}

	public void setDescricaoTipoProcesso(String descricaoTipoProcesso) {
		this.descricaoTipoProcesso = descricaoTipoProcesso;
	}

	public String getDescricaoModalidade() {
		return descricaoModalidade;
	}

	public void setDescricaoModalidade(String descricaoModalidade) {
		this.descricaoModalidade = descricaoModalidade;
	}

	public String getDescricaoSubTipoProcesso() {
		return descricaoSubTipoProcesso;
	}

	public void setDescricaoSubTipoProcesso(String descricaoSubTipoProcesso) {
		this.descricaoSubTipoProcesso = descricaoSubTipoProcesso;
	}



}
