package br.gov.ce.semace.erp.entity.fiscalizacao.vo;

public class GrupoPeriodo {
	
	private String nomeGrupo;
	
	private Integer mes;
	
	private Integer ano;
	
	private Long quantidade;

	public GrupoPeriodo() {
		super();
	}
	
	public GrupoPeriodo(String nomeGrupo, Integer mes, Integer ano, Long quantidade) {
		super();
		this.nomeGrupo = nomeGrupo;
		this.mes = mes;
		this.ano = ano;
		this.quantidade = quantidade;
	}

	public String getNomeGrupo() {
		return nomeGrupo;
	}

	public void setNomeGrupo(String nomeGrupo) {
		this.nomeGrupo = nomeGrupo;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}
	
	

}