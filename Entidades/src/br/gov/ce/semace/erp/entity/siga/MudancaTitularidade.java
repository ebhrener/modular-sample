package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.util.Date;

public class MudancaTitularidade implements Serializable {

	private static final long serialVersionUID = 1511312370457485345L;

	private String spuId;
	private String mudanca;
	private Date data;

	public String getSpuId() {
		return spuId;
	}

	public void setSpuId(String spuId) {
		this.spuId = spuId;
	}

	public String getMudanca() {
		return mudanca;
	}

	public void setMudanca(String mudanca) {
		this.mudanca = mudanca;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

}
