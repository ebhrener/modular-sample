package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import br.gov.ce.semace.erp.entity.financeiro.TipoTaxa;
import br.gov.ce.semace.erp.enuns.IndiceAtualizacaoMonetaria;

@Entity
@Audited
@Table(name="origem_divida", schema="juridico")
public class OrigemDivida implements Serializable{

	private static final long serialVersionUID = 2347775145334788457L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_origem_divida", name="juridico.seq_origem_divida", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_origem_divida")
	private Long id;

	@Column(nullable=false)
	private String descricao;

	@ManyToOne(optional=false)
	@JoinColumn(name="tipo_taxa_id")
	private TipoTaxa tipoTaxa = new TipoTaxa();

	@Enumerated(EnumType.ORDINAL)
	@Column(nullable=false, name= "indice_atualizacao_monetaria")
	private	IndiceAtualizacaoMonetaria indiceAtualizacaoMonetaria;

	@Column(name = "valor_multa", scale=5, precision=12)
    private BigDecimal valorMulta;

	@OneToMany(mappedBy="origemDivida", fetch=FetchType.LAZY)
	@NotAudited
	private List<DividaAtiva> dividas = new ArrayList<>();

	public OrigemDivida() {
		super();
	}

	public OrigemDivida(String descricao) {
		super();
		this.descricao = descricao;
	}

	public OrigemDivida(Long id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}

	//------------------------------------------------Gets e Setters---------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<DividaAtiva> getDividas() {
		return dividas;
	}

	public void setDividas(List<DividaAtiva> dividas) {
		this.dividas = dividas;
	}

	public TipoTaxa getTipoTaxa() {
		return tipoTaxa;
	}

	public void setTipoTaxa(TipoTaxa tipoTaxa) {
		this.tipoTaxa = tipoTaxa;
	}

	public IndiceAtualizacaoMonetaria getIndiceAtualizacaoMonetaria() {
		return indiceAtualizacaoMonetaria;
	}

	public void setIndiceAtualizacaoMonetaria(IndiceAtualizacaoMonetaria indiceAtualizacaoMonetaria) {
		this.indiceAtualizacaoMonetaria = indiceAtualizacaoMonetaria;
	}

	public BigDecimal getValorMulta() {
		return valorMulta;
	}

	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}

}
