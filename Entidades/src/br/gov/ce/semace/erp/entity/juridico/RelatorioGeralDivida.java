package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * DTO do relatório Geral da Dívida Ativa
 * 
 * @author Joerlan Lima
 *
 */
public class RelatorioGeralDivida implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -799448435294943515L;
	
	private String numeroCDA;
	
	private Date dataInscricao;
	
	private String cpf;
	
	private String cnpj;
	
	private Boolean siga;
	
	private BigDecimal valorAtualizado;
	
	private String execucaoFiscal;
	
	private String nome;
	
	public RelatorioGeralDivida(String numeroCDA, Date dataInscricao,
			String cpf, String cnpj, Boolean siga, BigDecimal valorAtualizado,
			String execucaoFiscal, String nome) {
		super();
		this.numeroCDA = numeroCDA;
		this.dataInscricao = dataInscricao;
		this.cpf = cpf;
		this.cnpj = cnpj;
		this.siga = siga;
		this.valorAtualizado = valorAtualizado;
		this.execucaoFiscal = execucaoFiscal;
		this.nome = nome;
	}

	public String getDocumento() {
		if (cpf != null && !cpf.trim().isEmpty()) {
			return cpf;
		}
		if (cnpj != null && !cnpj.trim().isEmpty()) {
			return cnpj;
		}
		return "";
	}

	public String getNumeroCDA() {
		return numeroCDA;
	}

	public void setNumeroCDA(String numeroCDA) {
		this.numeroCDA = numeroCDA;
	}

	public Date getDataInscricao() {
		return dataInscricao;
	}

	public void setDataInscricao(Date dataInscricao) {
		this.dataInscricao = dataInscricao;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Boolean getSiga() {
		return siga;
	}

	public void setSiga(Boolean siga) {
		this.siga = siga;
	}

	public BigDecimal getValorAtualizado() {
		return valorAtualizado;
	}

	public void setValorAtualizado(BigDecimal valorAtualizado) {
		this.valorAtualizado = valorAtualizado;
	}

	public String getExecucaoFiscal() {
		return execucaoFiscal;
	}

	public void setExecucaoFiscal(String execucaoFiscal) {
		this.execucaoFiscal = execucaoFiscal;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
