package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="shape_base_cartografica")
public class ShapeBaseCartografica implements Serializable {

	private static final long serialVersionUID = 4245888436832382729L;

	@Id
	@SequenceGenerator(sequenceName="seq_shape_base_cartografica", name="seq_shape_base_cartografica", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_shape_base_cartografica")
	private Long id;

	@Column(nullable = false,length = 20)
	private String versao;

	@Column(unique = true)
	private String tabela;

	@Column(nullable= false)
	private String descricao;

	@Column(nullable= false)
	private Boolean ativo;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = BaseCartografica.class)
	@JoinColumn(name="base_cartografica_id")
	private BaseCartografica baseCartografica = new BaseCartografica();

	@OneToOne(optional=true, mappedBy="shapeBaseCartografica")
	private AgendaBaseCartografica agendaBaseCartografica;

	@Column(name = "latitude_maxima")
	private Double latitudeMaxima;

	@Column(name = "latitude_minima")
	private Double latitudeMinima;

	@Column(name = "longitude_maxima")
	private Double longitudeMaxima;

	@Column(name = "longitude_minima")
	private Double longitudeMinima;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public String getTabela() {
		return tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	public BaseCartografica getBaseCartografica() {
		return baseCartografica;
	}

	public void setBaseCartografica(BaseCartografica baseCartografica) {
		this.baseCartografica = baseCartografica;
	}

	public AgendaBaseCartografica getAgendaBaseCartografica() {
		return agendaBaseCartografica;
	}

	public void setAgendaBaseCartografica(AgendaBaseCartografica agendaBaseCartografica) {
		this.agendaBaseCartografica = agendaBaseCartografica;
	}

	public Double getLatitudeMaxima() {
		return latitudeMaxima;
	}

	public void setLatitudeMaxima(Double latitudeMaxima) {
		this.latitudeMaxima = latitudeMaxima;
	}

	public Double getLatitudeMinima() {
		return latitudeMinima;
	}

	public void setLatitudeMinima(Double latitudeMinima) {
		this.latitudeMinima = latitudeMinima;
	}

	public Double getLongitudeMaxima() {
		return longitudeMaxima;
	}

	public void setLongitudeMaxima(Double longitudeMaxima) {
		this.longitudeMaxima = longitudeMaxima;
	}

	public Double getLongitudeMinima() {
		return longitudeMinima;
	}

	public void setLongitudeMinima(Double longitudeMinima) {
		this.longitudeMinima = longitudeMinima;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

}
