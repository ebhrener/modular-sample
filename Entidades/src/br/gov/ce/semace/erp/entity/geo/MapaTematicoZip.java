package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;

/**
 * Classe de tratamento de mapas temáticos como zip na pasta de compartilhamento
 */
public class MapaTematicoZip implements Serializable{

	private static final long serialVersionUID = 4174777151349488563L;

	private String nome;
	
	private String canonicalPath;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCanonicalPath() {
		return canonicalPath;
	}

	public void setCanonicalPath(String canonicalPath) {
		this.canonicalPath = canonicalPath;
	}

}

