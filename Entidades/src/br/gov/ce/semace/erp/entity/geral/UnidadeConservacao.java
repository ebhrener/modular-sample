package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoOrgao;

@Entity
@Audited
@Table(name="unidade_conservacao", schema="geral")
public class UnidadeConservacao implements Serializable{

	private static final long serialVersionUID = -2351641673122445842L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_unidade_conservacao", name="SEQ", allocationSize=1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ")
	private Long id;

	@Column(name="nome_unidade_conservacao")
	private String nomeUnidadeConservacao;

	@Column(name="tipo_orgao")
	private TipoOrgao tipoOrgao;

	@Column(name = "codigo_unidade")
	private String codigoUnidade;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeUnidadeConservacao() {
		return nomeUnidadeConservacao;
	}

	public void setNomeUnidadeConservacao(String nomeUnidadeConservacao) {
		this.nomeUnidadeConservacao = nomeUnidadeConservacao;
	}

	public TipoOrgao getTipoOrgao() {
		return tipoOrgao;
	}

	public void setTipoOrgao(TipoOrgao tipoOrgao) {
		this.tipoOrgao = tipoOrgao;
	}

	public String getCodigoUnidade() {
		return codigoUnidade;
	}

	public void setCodigoUnidade(String codigoUnidade) {
		this.codigoUnidade = codigoUnidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UnidadeConservacao other = (UnidadeConservacao) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return codigoUnidade + " - " + nomeUnidadeConservacao;
	}

}
