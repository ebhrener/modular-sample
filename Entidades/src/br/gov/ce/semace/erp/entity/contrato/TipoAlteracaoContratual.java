package br.gov.ce.semace.erp.entity.contrato;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoDocumento;

@Entity
@Audited
@Table(name="tipo_alteracao_contratual", schema="contrato")
public class TipoAlteracaoContratual implements Serializable{

	private static final long serialVersionUID = 7079900775683556015L;

	@Id
	@SequenceGenerator(sequenceName="contrato.seq_tipo_alteracao_contrato", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	private String descricao;
	
	@Column(name = "tipo_documento")
	private TipoDocumento tipoDocumento;
	
	@Column(name = "prazo_alteracao_sugestao")
	private Integer prazoAlteracaoSugestao;
	
	@Column(name = "permitir_correcao_monetaria")
	private Boolean permitirCorrecaoMonetaria;
	
	@Column(name="permitir_renovar_valor_contrato_por_prazo")
	private Boolean permitirRenovarValorContratoPorPrazo;
	
	
	/**
	 * Getters and Setters
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Integer getPrazoAlteracaoSugestao() {
		return prazoAlteracaoSugestao;
	}

	public void setPrazoAlteracaoSugestao(Integer prazoAlteracaoSugestao) {
		this.prazoAlteracaoSugestao = prazoAlteracaoSugestao;
	}

	public Boolean getPermitirCorrecaoMonetaria() {
		return permitirCorrecaoMonetaria;
	}

	public void setPermitirCorrecaoMonetaria(Boolean permitirCorrecaoMonetaria) {
		this.permitirCorrecaoMonetaria = permitirCorrecaoMonetaria;
	}

	public Boolean getPermitirRenovarValorContratoPorPrazo() {
		return permitirRenovarValorContratoPorPrazo;
	}

	public void setPermitirRenovarValorContratoPorPrazo(
			Boolean permitirRenovarValorContratoPorPrazo) {
		this.permitirRenovarValorContratoPorPrazo = permitirRenovarValorContratoPorPrazo;
	}
}
