package br.gov.ce.semace.erp.entity.protocolo.to;

import java.io.Serializable;

import br.gov.ce.semace.erp.entity.tramite.to.TramiteTO;

public class CapaTO implements Serializable {

	private static final long serialVersionUID = -6456667637963101202L;

	private String spu;

	private String data;

	private String hora;

	private String origem;

	private String assunto;

	private String observacoes;

	private String autor;

	private String criadoPor;

	private String cpf;

	private String dataHoraCriacao;

	private TramiteTO tramite;

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getCriadoPor() {
		return criadoPor;
	}

	public void setCriadoPor(String criadoPor) {
		this.criadoPor = criadoPor;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDataHoraCriacao() {
		return dataHoraCriacao;
	}

	public void setDataHoraCriacao(String dataHoraCriacao) {
		this.dataHoraCriacao = dataHoraCriacao;
	}

	public TramiteTO getTramite() {
		return tramite;
	}

	public void setTramite(TramiteTO tramite) {
		this.tramite = tramite;
	}

}
