package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;
import br.gov.ce.semace.erp.entity.geral.entity.cep.UF;
import br.gov.ce.semace.erp.utils.StringUtils;

@Entity
@Audited
@Table(name="endereco", schema="geral")
public class Endereco implements Serializable, Cloneable {

	private static final long serialVersionUID = -5624844588824452681L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_endereco", name="geral.seq_endereco", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="geral.seq_endereco")
	@Index(name="end_pk_i1")
	private Long id;

	private String logradouro;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cidade_id")
	private Cidade cidade = new Cidade();

	private String bairro;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="uf_id")
	private UF uf = new UF();

	private String numero;

	private String complemento;

	@Column(name="caixa_postal")
	private String caixaPostal;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="pessoa_id")
	@JsonBackReference
	private Pessoa pessoa = new Pessoa();

	@Column(name="endereco_padrao")
	private Boolean isEnderecoPadrao = false;

	private String cep;

	private String latitude;

	private String longitude;

	/**
	 * Método {@link Transient} para informar se o endereço é o de correspondência.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 25/09/2013 11:12:59
	 *
	 * @return
	 * 	Retorna a {@link String} <code>Sim</code>, se o {@link Endereco} for o padrão, no caso utilizado para a correspondência.
	 * Caso contrário, retorna <code>Não</code>.
	 */
	@Transient
	public String getEnderecoCorrespondencia(){
		if(this.isEnderecoPadrao != null && this.isEnderecoPadrao){
			return "Sim";
		}

		return "Não";
	}

	@Override
	public String toString() {
		/** Rua Jaime Benevolo, 1400 - Fátima, Fortaleza - CE, 60050-081 */

		StringBuilder sb = new StringBuilder();

		if(StringUtils.isNotBlank(this.logradouro)) {
			sb.append(this.logradouro.trim());
		}

		if(StringUtils.isNotBlank(this.numero)) {
			if(this.numero.equalsIgnoreCase("s/n")) {
				sb.append(", " + getNumero());
			} else {
				sb.append(", nº " + getNumero());
			}
		}
		if (StringUtils.isNotBlank(this.complemento)) {
			sb.append(", " + getComplemento());
		}

		if(StringUtils.isNotBlank(this.bairro)) {
			sb.append(" - " + getBairro().trim());
		}

		if(this.cidade != null && StringUtils.isNotBlank(this.cidade.getDescricao())) {
			sb.append(", " + this.cidade.getDescricao().trim());
			if(this.uf != null && StringUtils.isNotBlank(this.uf.getSigla())) {
				sb.append(" - " + this.uf.getSigla().trim());
			}
		}

		if(StringUtils.isNotBlank(this.cep) && this.cep.length() >= 8) {
			sb.append(", " + this.cep.subSequence(0, 5));
			sb.append("-");
			sb.append(this.cep.subSequence(5, 8));
		}

		return sb.toString();
	}

	public String toString(boolean logradouro, boolean numero, boolean complemento, boolean bairro, boolean cidade, boolean uf, boolean cep) {
		/** Rua Jaime Benevolo, 1400 - Fátima, Fortaleza - CE, 60050-081 */

		StringBuilder sb = new StringBuilder();

		if(StringUtils.isNotBlank(this.logradouro) && logradouro) {
			sb.append(this.logradouro.trim());
		}

		if(StringUtils.isNotBlank(this.numero) && numero) {
			if(this.numero.equalsIgnoreCase("s/n")) {
				sb.append(", " + getNumero());
			} else {
				sb.append(", nº " + getNumero());
			}
		}
		if (StringUtils.isNotBlank(this.complemento) && complemento) {
			sb.append(", " + getComplemento());
		}

		if(StringUtils.isNotBlank(this.bairro) && bairro) {
			sb.append(" - " + getBairro().trim());
		}

		if(this.cidade != null && StringUtils.isNotBlank(this.cidade.getDescricao()) && cidade) {
			sb.append(", " + this.cidade.getDescricao().trim());
			if(this.uf != null && StringUtils.isNotBlank(this.uf.getSigla()) && uf) {
				sb.append(" - " + this.uf.getSigla().trim());
			}
		}

		if(StringUtils.isNotBlank(this.cep) && this.cep.length() >= 8 && cep) {
			sb.append(", " + this.cep.subSequence(0, 5));
			sb.append("-");
			sb.append(this.cep.subSequence(5, 8));
		}

		return sb.toString();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the logradouro
	 */
	public String getLogradouro() {
		return this.logradouro;
	}

	/**
	 * @param logradouro the logradouro to set
	 */
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	/**
	 * @return the cidade
	 */
	public Cidade getCidade() {
		return this.cidade;
	}

	/**
	 * @param cidade the cidade to set
	 */
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	/**
	 * @return the bairro
	 */
	public String getBairro() {
		return this.bairro;
	}

	/**
	 * @param bairro the bairro to set
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	/**
	 * @return the uf
	 */
	public UF getUf() {
		return this.uf;
	}

	/**
	 * @param uf the uf to set
	 */
	public void setUf(UF uf) {
		this.uf = uf;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return this.numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the complemento
	 */
	public String getComplemento() {
		return this.complemento;
	}

	/**
	 * @param complemento the complemento to set
	 */
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	/**
	 * @return the caixaPostal
	 */
	public String getCaixaPostal() {
		return this.caixaPostal;
	}

	/**
	 * @param caixaPostal the caixaPostal to set
	 */
	public void setCaixaPostal(String caixaPostal) {
		this.caixaPostal = caixaPostal;
	}

	/**
	 * @return the pessoa
	 */
	public Pessoa getPessoa() {
		return this.pessoa;
	}

	/**
	 * @param pessoa the pessoa to set
	 */
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	/**
	 * @return the isEnderecoPadrao
	 */
	public Boolean getIsEnderecoPadrao() {
		return this.isEnderecoPadrao;
	}

	/**
	 * @param isEnderecoPadrao the isEnderecoPadrao to set
	 */
	public void setIsEnderecoPadrao(Boolean isEnderecoPadrao) {
		this.isEnderecoPadrao = isEnderecoPadrao;
	}

	/**
	 * @return the cep
	 */
	public String getCep() {
		return this.cep;
	}

	/**
	 * @param cep the cep to set
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bairro == null) ? 0 : bairro.hashCode());
		result = prime * result + ((cep == null) ? 0 : cep.hashCode());
		result = prime * result
				+ ((logradouro == null) ? 0 : logradouro.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Endereco other = (Endereco) obj;
		if (bairro == null) {
			if (other.bairro != null) {
				return false;
			}
		} else if (!bairro.equals(other.bairro)) {
			return false;
		}
		if (cep == null) {
			if (other.cep != null) {
				return false;
			}
		} else if (!cep.equals(other.cep)) {
			return false;
		}
		if (logradouro == null) {
			if (other.logradouro != null) {
				return false;
			}
		} else if (!logradouro.equals(other.logradouro)) {
			return false;
		}
		if (numero == null) {
			if (other.numero != null) {
				return false;
			}
		} else if (!numero.equals(other.numero)) {
			return false;
		}
		return true;
	}

}