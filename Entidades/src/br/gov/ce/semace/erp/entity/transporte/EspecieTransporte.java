package br.gov.ce.semace.erp.entity.transporte;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoTransporte;
/**
 *
 * @author Tiago Nascimento - vilard@gmail.com - 14/05/2014 - 13:28:27
 *
 */
@Entity
@Audited
@Table(name="especie_transporte", schema="transporte")
public class EspecieTransporte implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 154836423606487614L;

	@Id
	@SequenceGenerator(sequenceName="transporte.seq_especie_transporte", name="SEQ_ESPECIE_TRANSPORTE", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ_ESPECIE_TRANSPORTE")
	private Long id;

	private String descricao;

	@Column(name="tipo_transporte")
	private TipoTransporte tipoTransporte;

	@Column(nullable = false)
	private boolean ativo = true;

	@Transient
	public String getDetalheAtivo(){

		if (this.ativo){
			return "Sim";
		}

		return "Não";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoTransporte getTipoTransporte() {
		return tipoTransporte;
	}

	public void setTipoTransporte(TipoTransporte tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
