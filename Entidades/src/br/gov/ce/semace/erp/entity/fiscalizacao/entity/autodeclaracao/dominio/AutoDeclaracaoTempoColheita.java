package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio;

public enum AutoDeclaracaoTempoColheita {
	
	PERMANENTE(0,"Permanente"),
	SEIS_MESES(1,"Seis Meses"),
	ANO(2,"Um Ano"),
	DEZOITO_MESES(3,"Dezoito Meses"),
	DOIS_ANOS(4,"Dois Anos"),
	DEZ_ANOS(5,"Dez Anos"),
	CINCO_ANOS(6,"Cinco Anos");
	
	private Integer codigo;
	private String descricao;
	
	private AutoDeclaracaoTempoColheita(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static String showDescricaoByCodigo(Integer codigo){
		for(AutoDeclaracaoTempoColheita auto: AutoDeclaracaoTempoColheita.values()){
			if(auto.getCodigo().equals(codigo)){
				return auto.descricao;
			}
		}
		return "Não Informado";	}
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
}
