package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.atendimento.agrotoxico.ProdutoAgrotoxico;
import br.gov.ce.semace.erp.entity.geo.ArquivoShapeGeo;
import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.geral.Consultoria;
import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.geral.Empreendimento;
import br.gov.ce.semace.erp.entity.geral.EmpreendimentoAtividade;
import br.gov.ce.semace.erp.entity.geral.Interessado;
import br.gov.ce.semace.erp.entity.geral.ProjetoEstrategico;
import br.gov.ce.semace.erp.entity.geral.Sede;
import br.gov.ce.semace.erp.entity.geral.UnidadeConservacao;
import br.gov.ce.semace.erp.entity.licenciamento.CheckList;
import br.gov.ce.semace.erp.entity.licenciamento.Cnae;
import br.gov.ce.semace.erp.entity.licenciamento.Modalidade;
import br.gov.ce.semace.erp.entity.licenciamento.Rama;
import br.gov.ce.semace.erp.entity.licenciamento.SubtipoProcesso;
import br.gov.ce.semace.erp.entity.licenciamento.TipoProcesso;
import br.gov.ce.semace.erp.entity.rh.Setor;
import br.gov.ce.semace.erp.entity.seguranca.Usuario;
import br.gov.ce.semace.erp.entity.siga.Autorizacao;
import br.gov.ce.semace.erp.entity.siga.LicencaSiga;
import br.gov.ce.semace.erp.enuns.EntregaDocumento;
import br.gov.ce.semace.erp.enuns.MotivoCadastroAgrotoxico;
import br.gov.ce.semace.erp.enuns.SituacaoAtendimentoRequerimento;
import br.gov.ce.semace.erp.enuns.StatusAtendimento;
import br.gov.ce.semace.erp.enuns.StatusBuscaProcesso;
import br.gov.ce.semace.erp.enuns.StatusRequerimento;
import br.gov.ce.semace.erp.utils.FormatUtils;

@Entity
@Audited
@Table(name="agendamento_requerimento", schema="atendimento")
@Inheritance(strategy = InheritanceType.JOINED)
public class AgendamentoRequerimento implements Serializable {

	private static final long serialVersionUID = 5607246441697742318L;

	@Id @SequenceGenerator(sequenceName="atendimento.seq_requerimento", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="interessado_id")
	private Interessado interessado = new Interessado();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="empreendimento_id")
	private Empreendimento empreendimento = new Empreendimento();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="sede_id")
	private Sede sede = new Sede();

	@Temporal(TemporalType.DATE)
	private Date data;

	private Time hora;

	private String justificativa;

	@Temporal(TemporalType.DATE)
	@Column(name="data_cancelamento")
	private Date dataCancelamento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_entrada")
	private Date dataEntrada;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tipo_processo_id")
	private TipoProcesso tipoProcesso = new TipoProcesso();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="modalidade_processo_id")
	private Modalidade modalidade = new Modalidade();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="subtipo_processo_id")
	private SubtipoProcesso subtipoProcesso = new SubtipoProcesso();

	@Column(name="numero_documento")
//	@Index(name="req_numero_documento_idx")
	private String numeroDocumento;

	@Column(name="enviou_arquivo_shape", nullable=false)
	private Boolean enviouArquivoShape = false;

	@Column(name="arquivo_shape_valido", nullable=false)
	private Boolean arquivoShapeValido = false;

	@Column(name="documentacao_valida", nullable=false)
	private Boolean documentacaoValida = false;

	@Column(name = "nao_gera_spu")
	private Boolean naoGeraSPU;

	@Column(columnDefinition="text")
	private String observacao;

	@Column(name="observacao_validacao_documento", columnDefinition="text")
	private String observacaoValidacaoDocumento;

	@Column(columnDefinition="text", name="observacao_pre_processo")
	private String observacaoPreProcesso;

	@Column(precision=15, scale=2)
	private BigDecimal hectare;

	@Column(columnDefinition="text")
	private String objetivo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao")
	private Date dataCriacao;

	private Integer sequencial;

	//SPU ANTIGO
	@Column(name="numero_spu")
	private String numeroSpu;

	@Column(name="status_requerimento")
	private StatusRequerimento statusRequerimento;

	@Column(name="status_atendimento")
	private StatusAtendimento statusAtendimento;

	@Column(name="motivo_cadastro_agrotoxico")
	private MotivoCadastroAgrotoxico motivoCadastroAgrotoxico;

	@Column(name="entrega_documento")
	private EntregaDocumento entregaDocumento;

	@Column(name="destinacao_materia_prima", columnDefinition="text")
	private String destinacaoMateriaPrima;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy="agendamentoRequerimento")
	private List<AtividadeRequerimentoChecklist> atividadeRequerimentoChecklist = new ArrayList<>();

	/**
	 * {@link Deprecated} Subistido por atividadeRequerimentoChecklist (Lista de Atividades)
	 */
	@Deprecated
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="atividade_id")
	private Atividade atividade = new Atividade();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="usuario_id")
	private Usuario usuario = new Usuario();

	@ManyToOne(fetch = FetchType.LAZY )
	private Requerimento requerimento;

	//NOVO SPU
	@Column(name="spu_processo")
	private String spuProcesso;

	@Column(name="liberado_pendencia")
	private Boolean liberadoPendencia;

	@Column(name="requerimento_avulso")
	private Boolean requerimentoAvulso;


	// CAMPOS NECESSÁRIOS PARA PENDENCIA

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="checklist_id")
	private CheckList checkList = new CheckList();

	@Column(name="validou_shape")
	private Boolean validouShape = false;

	@Column(name="validou_documentacao")
	private Boolean validouDocumentacao = false;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "agendamento_requerimento_projeto_estrategico",
		schema = "atendimento",
		joinColumns = @JoinColumn(name = "agendamento_requerimento_id"),
		inverseJoinColumns = @JoinColumn(name = "projeto_estrategico_id"))
	private List<ProjetoEstrategico> projetosEstrategicos = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="consultoria_id")
	private Consultoria consultoria = new Consultoria();


	@JoinColumn(name="empreendimento_atividade_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private EmpreendimentoAtividade empreendimentoAtividade = new EmpreendimentoAtividade();

	/**
	 * Não há registros na tabela nem idícios de que esta tabela está sendo utilizada
	 */
	@Deprecated
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "agendamentoRequerimento")
	private List<ArquivoShape> arquivosShape = new ArrayList<>();

	@Column(name="documentacao_financeira_valida")
	private Boolean documentacaoFinanceiraValida;

	/**
	 * Informa se o {@link AgendamentoRequerimento} é passível de isenção de taxas.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 23/12/2013 11:49:18
	 */
	@Column(name="isento_taxa", nullable = false)
	private boolean isentoTaxa = false;

	@Column(name = "is_processo")
	private Boolean isProcesso;

	@OneToMany(mappedBy = "agendamentoRequerimento", fetch = FetchType.LAZY)
	private List<AgendamentoAnaliseLaboratorial> analises;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "documento_id")
	private Documento documento = new Documento();

	@Transient
	private Boolean excluido;

	@Transient
	private Long idEndereco;

	@Transient
	private String telefone;

	@Transient
	private Boolean isAgendamentoAnalise = Boolean.FALSE;

	@Transient
	private Date dataInicial;

	@Transient
	private Date dataFinal;

	@Column(name = "valor_investimento")
	private BigDecimal valorInvestimento;

	@Column(name = "numero_empregos_direto")
	private Integer numeroEmpregosDireto;

	private BigDecimal faturamento;

	@Column(name = "numero_empregadores_operacao")
	private Integer numeroEmpregadoresOperacao;

	@Column(name="nome_produto_selo_verde")
	private String nomeProdutoSeloVerde;

	@Column(name="modelo_selo_verde")
	private String modeloSeloVerde;

	@Column(name="descricao_selo_verde")
	private String descricaoSeloVerde;

	@Column(name="materia_prima_selo_verde")
	private String materiaPrimaUtilizadaSeloVerde;

	@Column(name = "percentual_materia_prima_selo_verde")
	private Double percentualMateriaPrimaSeloVerde;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cnae_id")
	private Cnae cnae;

	@Column(name="quantidade_produtos_alteracao_lote")
	private Integer quantidadeProdutosAlteracaoLote;

	@Column(name="numero_car")
	private String numeroCar;

	@Column(name="preencheu_numero_car")
	private Boolean preencheuNumeroCar;

	@Column(name="numero_veiculos")
	private Integer numeroVeiculos;

	@Column(name="numero_licenca")
	private String numeroLicenca;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="rama_id")
	private Rama rama;

	/**
	 * Informa se o {@link AgendamentoRequerimento} possui
	 * um empreendimento localizado em uma unidade de conservação.
	 *
	 * @author robson.carvalho - Robson Rocha Carvalho [robson01jc@gmail.com] - 23/01/2017 10:27:18
	 */
	@Column(name="localizado_unidade_conservacao")
	private Boolean localizadoUnidadeConservacao;

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "agendamento_requerimento_unidade_conservacao", schema = "atendimento",
		joinColumns = @JoinColumn(name = "agendamento_requerimento_id"),
		inverseJoinColumns = @JoinColumn(name = "unidade_conservacao_id"))
	private List<UnidadeConservacao> unidadesConservacao = new ArrayList<>();

	@OneToMany(mappedBy="requerimento", fetch=FetchType.LAZY, orphanRemoval = true)
	private List<RequerimentoAtividadeCheckListItemCheckList> listRACI = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="setor_id")
	private Setor setorDestino;

	@OneToMany(mappedBy="agendamentoRequerimento", fetch=FetchType.LAZY, orphanRemoval = false)
	private List<ArquivoShapeGeo> listArquivoShapeGeo = new ArrayList<>();

	/**
	 * Informa se o {@link AgendamentoRequerimento} possui prioridade de atendimento.
	 *
	 * @author robson.carvalho - Robson Rocha Carvalho [robson01jc@gmail.com] - 19/06/2018 09:15:18
	 */
	@Column
	private Boolean prioritario;

	@Transient
	private List<ProdutoAgrotoxico> listProdutosAgrotoxicos;

	@Transient
	private LicencaSiga licencaSiga;

	@Transient
	private Autorizacao autorizacaoSiga;

	@Transient
	private boolean possuiAtividadeRequerCAR;

	@Transient
	private StatusBuscaProcesso statusRequerimentoBusca;

	@Transient
	private Date dataImplantacaoGED;

	/**
	 * Campo para ser usado como filtro em telas de listagem.
	 */
	@Transient
	private Boolean pendente;

	@Transient
	private String displayUnidadeConservacao;

	/**
	 * Property used for queries projection purposes.
	 */
	@Transient
	private String lastUpdateUsername;

	/**
	 * Campo para ser usado como filtro em telas de listagem.
	 */
	@Transient
	private SituacaoAtendimentoRequerimento situacaoFaseAtendimentoRequerimento;

	@Transient
	private Integer qdtDiasLicencaVencer;

	/**
	 * Informa o Spu anterior para telas de detalhes ou tabela.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 04/12/2013 16:40:32
	 *
	 * @return
	 * 		Retorna uma {@link String} com o {@link #numeroSpu}, porém, caso esteja nulo nulo ou vazio, retorna a {@link String}
	 * <code>Não Informado</code>
	 */
	@Transient
	public String getDetalheSpuAnterior(){
		if(this.numeroSpu == null || this.numeroSpu.trim().isEmpty()){
			return "Não informado";
		}

		return this.numeroSpu;
	}

	@Transient
	public String getDisplayTipoRequerimento(){
		StringBuilder descricaoCompleta = new StringBuilder("");

		if(modalidade != null && modalidade.getId() != null ){
			descricaoCompleta.append(modalidade.getDescricao() + " / ");
		}

		if(tipoProcesso != null){
			descricaoCompleta.append(tipoProcesso.getDescricao() + " / ");
		}

		if(subtipoProcesso != null && subtipoProcesso.getId() != null ){
			descricaoCompleta.append(subtipoProcesso.getDescricao() + " / ");
		}

		return descricaoCompleta.toString();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Interessado getInteressado() {
		return this.interessado;
	}

	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}

	public Empreendimento getEmpreendimento() {
		return this.empreendimento;
	}

	public void setEmpreendimento(Empreendimento empreendimento) {
		this.empreendimento = empreendimento;
	}

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Time getHora() {
		return this.hora;
	}

	public void setHora(Time hora) {
		this.hora = hora;
	}

	public String getJustificativa() {
		return this.justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public Date getDataCancelamento() {
		return this.dataCancelamento;
	}

	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public Date getDataEntrada() {
		return this.dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public TipoProcesso getTipoProcesso() {
		return this.tipoProcesso;
	}

	public void setTipoProcesso(TipoProcesso tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public String getNumeroDocumento() {
		return this.numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public Boolean getEnviouArquivoShape() {
		return this.enviouArquivoShape;
	}

	public void setEnviouArquivoShape(Boolean enviouArquivoShape) {
		this.enviouArquivoShape = enviouArquivoShape;
	}

	public Boolean getArquivoShapeValido() {
		return this.arquivoShapeValido;
	}

	public void setArquivoShapeValido(Boolean arquivoShapeValido) {
		this.arquivoShapeValido = arquivoShapeValido;
	}

	public Boolean getDocumentacaoValida() {
		return this.documentacaoValida;
	}

	public void setDocumentacaoValida(Boolean documentacaoValida) {
		this.documentacaoValida = documentacaoValida;
	}

	public String getObservacao() {
		return this.observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getObservacaoValidacaoDocumento() {
		return this.observacaoValidacaoDocumento;
	}

	public void setObservacaoValidacaoDocumento(String observacaoValidacaoDocumento) {
		this.observacaoValidacaoDocumento = observacaoValidacaoDocumento;
	}

	public BigDecimal getHectare() {
		return this.hectare;
	}

	public void setHectare(BigDecimal hectare) {
		this.hectare = hectare;
	}

	public String getObjetivo() {
		return this.objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Integer getSequencial() {
		return this.sequencial;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public String getNumeroSpu() {
		return this.numeroSpu;
	}

	public void setNumeroSpu(String numeroSpu) {
		this.numeroSpu = numeroSpu;
	}

	public StatusRequerimento getStatusRequerimento() {
		return this.statusRequerimento;
	}

	public void setStatusRequerimento(StatusRequerimento statusRequerimento) {
		this.statusRequerimento = statusRequerimento;
	}

	public StatusAtendimento getStatusAtendimento() {
		return this.statusAtendimento;
	}

	public void setStatusAtendimento(StatusAtendimento statusAtendimento) {
		this.statusAtendimento = statusAtendimento;
	}

	public EntregaDocumento getEntregaDocumento() {
		return this.entregaDocumento;
	}

	public void setEntregaDocumento(EntregaDocumento entregaDocumento) {
		this.entregaDocumento = entregaDocumento;
	}

	public String getDestinacaoMateriaPrima() {
		return this.destinacaoMateriaPrima;
	}

	public void setDestinacaoMateriaPrima(String destinacaoMateriaPrima) {
		this.destinacaoMateriaPrima = destinacaoMateriaPrima;
	}

	public List<AtividadeRequerimentoChecklist> getAtividadeRequerimentoChecklist() {
		return atividadeRequerimentoChecklist;
	}

	public void setAtividadeRequerimentoChecklist(List<AtividadeRequerimentoChecklist> atividadeRequerimentoChecklist) {
		this.atividadeRequerimentoChecklist = atividadeRequerimentoChecklist;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getSpuProcesso() {
		return this.spuProcesso;
	}

	public void setSpuProcesso(String spuProcesso) {
		this.spuProcesso = spuProcesso;
	}
	public Boolean getLiberadoPendencia() {
		return this.liberadoPendencia;
	}

	public void setLiberadoPendencia(Boolean liberadoPendencia) {
		this.liberadoPendencia = liberadoPendencia;
	}

	public MotivoCadastroAgrotoxico getMotivoCadastroAgrotoxico() {
		return this.motivoCadastroAgrotoxico;
	}

	public void setMotivoCadastroAgrotoxico(
			MotivoCadastroAgrotoxico motivoCadastroAgrotoxico) {
		this.motivoCadastroAgrotoxico = motivoCadastroAgrotoxico;
	}

	public CheckList getCheckList() {
		return this.checkList;
	}

	public void setCheckList(CheckList checkList) {
		this.checkList = checkList;
	}

	public Boolean getValidouShape() {
		return this.validouShape;
	}

	public void setValidouShape(Boolean validouShape) {
		this.validouShape = validouShape;
	}

	public Boolean getValidouDocumentacao() {
		return this.validouDocumentacao;
	}

	public void setValidouDocumentacao(Boolean validouDocumentacao) {
		this.validouDocumentacao = validouDocumentacao;
	}

	public Consultoria getConsultoria() {
		return this.consultoria;
	}

	public void setConsultoria(Consultoria consultoria) {
		this.consultoria = consultoria;
	}

	public Boolean getRequerimentoAvulso() {
		return this.requerimentoAvulso;
	}

	public void setRequerimentoAvulso(Boolean requerimentoAvulso) {
		this.requerimentoAvulso = requerimentoAvulso;
	}

	public Requerimento getRequerimento() {
		return this.requerimento;
	}

	public void setRequerimento(Requerimento requerimento) {
		this.requerimento = requerimento;
	}

	public EmpreendimentoAtividade getEmpreendimentoAtividade() {
		return this.empreendimentoAtividade;
	}

	public void setEmpreendimentoAtividade(
			EmpreendimentoAtividade empreendimentoAtividade) {
		this.empreendimentoAtividade = empreendimentoAtividade;
	}

	public List<ProjetoEstrategico> getProjetosEstrategicos() {
		return this.projetosEstrategicos;
	}

	public void setProjetosEstrategicos(
			List<ProjetoEstrategico> projetosEstrategicos) {
		this.projetosEstrategicos = projetosEstrategicos;
	}

	public Sede getSede() {
		return this.sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

	public List<ArquivoShape> getArquivosShape() {
		return this.arquivosShape;
	}

	public void setArquivosShape(List<ArquivoShape> arquivosShape) {
		this.arquivosShape = arquivosShape;
	}

	public Boolean getNaoGeraSPU() {
		return this.naoGeraSPU;
	}

	public void setNaoGeraSPU(Boolean naoGeraSPU) {
		this.naoGeraSPU = naoGeraSPU;
	}

	public Long getIdEndereco() {
		return this.idEndereco;
	}

	public void setIdEndereco(Long idEndereco) {
		this.idEndereco = idEndereco;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	public Boolean getDocumentacaoFinanceiraValida() {
		return this.documentacaoFinanceiraValida;
	}

	public void setDocumentacaoFinanceiraValida(Boolean documentacaoFinanceiraValida) {
		this.documentacaoFinanceiraValida = documentacaoFinanceiraValida;
	}

	public Boolean getIsProcesso() {
		return this.isProcesso;
	}

	public void setIsProcesso(Boolean isProcesso) {
		this.isProcesso = isProcesso;
	}

	public Boolean getExcluido() {
		return this.excluido;
	}

	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}

	public SubtipoProcesso getSubtipoProcesso() {
		return this.subtipoProcesso;
	}

	public void setSubtipoProcesso(SubtipoProcesso subtipoProcesso) {
		this.subtipoProcesso = subtipoProcesso;
	}

	@Transient
	public String getDescricaoTipoModalidadeSubtipoProcesso() {
		return FormatUtils.getDescricaoTipoModalidadeSubtipoProcesso(this.tipoProcesso, this.subtipoProcesso, this.modalidade);
	}

	@Transient
	public String getDescricaoSiglaTipoModalidadeSubtipoProcesso() {
		return FormatUtils.getDescricaoSiglaTipoModalidadeSubtipoProcesso(this.tipoProcesso, this.subtipoProcesso, this.modalidade);
	}

	public Modalidade getModalidade() {
		return this.modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}


	public Boolean getIsAgendamentoAnalise() {
		return this.isAgendamentoAnalise;
	}

	public void setIsAgendamentoAnalise(Boolean isAgendamentoAnalise) {
		this.isAgendamentoAnalise = isAgendamentoAnalise;
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusAtendimento} é
	 * 	do tipo <b>TODOS</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:49:48
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusAtendimento} seja
	 * 	<b>TODOS</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusAtendimento#isTodos()
	 */
	@Transient
    public boolean isAtendimentoTodos() {
		return this.statusAtendimento.isTodos();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusAtendimento} é
	 * 	do tipo <b>AGENDADO</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:51:32
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusAtendimento} seja
	 * 	<b>AGENDADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusAtendimento#isAgendado()
	 */
	@Transient
    public boolean isAtendimentoAgendado() {
		return this.statusAtendimento.isAgendado();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusAtendimento} é
	 * 	do tipo <b>EM_ATENDIMENTO</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:51:53
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusAtendimento} seja
	 * 	<b>EM_ATENDIMENTO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusAtendimento#isEmAtendimento()
	 */
	@Transient
    public boolean isEmAtendimento() {
		return this.statusAtendimento.isEmAtendimento();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusAtendimento} é
	 * 	do tipo <b>VALIDACAO_DOCUMENTO_SHAPE</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:53:49
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusAtendimento} seja
	 * 	<b>VALIDACAO_DOCUMENTO_SHAPE</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusAtendimento#isValidacaoDocumentoShape()
	 */
	@Transient
    public boolean isAtendimentoValidacaoDocumentoShape() {
		return this.statusAtendimento.isValidacaoDocumentoShape();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusAtendimento} é
	 * 	do tipo <b>GERAR_PROCESSO_PROVISORIO</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:54:16
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusAtendimento} seja
	 * 	<b>GERAR_PROCESSO_PROVISORIO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusAtendimento#isGeraProcessoProvisorio()
	 */
	@Transient
    public boolean isAtendimentoGeraProcessoProvisorio() {
		return this.statusAtendimento.isGeraProcessoProvisorio();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusAtendimento} é
	 * 	do tipo <b>ATENDIDO</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:54:48
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusAtendimento} seja
	 * 	<b>ATENDIDO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusAtendimento#isAtendido()
	 */
	@Transient
    public boolean isAtendimentoAtendido() {
		return this.statusAtendimento.isAtendido();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusAtendimento} é
	 * 	do tipo <b>CANCELADO</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:55:11
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusAtendimento} seja
	 * 	<b>CANCELADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusAtendimento#isCancelado()
	 */
	@Transient
    public boolean isAtendimentoCancelado() {
		return this.statusAtendimento.isCancelado();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusAtendimento} é
	 * 	do tipo <b>EXCLUIDO</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:55:39
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusAtendimento} seja
	 * 	<b>EXCLUIDO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusAtendimento#isExcluido()
	 */
	@Transient
    public boolean isAtendimentoExcluido() {
		return this.statusAtendimento.isExcluido();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusAtendimento} é
	 * 	do tipo <b>GERAR_DAE</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:56:15
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusAtendimento} seja
	 * 	<b>GERAR_DAE</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusAtendimento#isGerarDae()
	 */
	@Transient
    public boolean isAtendimentoGerarDae() {
		return this.statusAtendimento.isGerarDae();
    }


	@Transient
    public boolean isAtendimentoAguardandoPagamentoDae() {
		return this.statusAtendimento.isAguardandoPagmentoDae();
    }

	@Transient
	public boolean isAtendimentoPreAgendado() {
		return this.statusAtendimento.isPreAgendado();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusAtendimento} é
	 * 	do tipo <b>ATENDIMENTO_INICIADO</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 16/10/2013 11:56:34
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusAtendimento} seja
	 * 	<b>ATENDIMENTO_INICIADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusAtendimento#isAtendimentoIniciado()
	 */
	@Transient
    public boolean isAtendimentoIniciado() {
		return this.statusAtendimento.isAtendimentoIniciado();
    }

	@Transient
    public boolean isAtendimentoEmTriagem() {
		return this.statusAtendimento.isTriagem();
    }

	@Transient
	public boolean isAguardandoValidacaoDocumento() {
    	return this.statusAtendimento.isAguardandoValidacaoDocumento();
    }

	@Transient
	public boolean isAtendimentoAguardandoGeracaoProcesso() {
    	return this.statusAtendimento.isAguardandoGeracaoProcesso();
    }

	@Transient
	public boolean isAtendimentoEmAnaliseDeDocumentos() {
		return this.statusAtendimento.isValidacaoDocumentoShape()
				|| this.statusAtendimento.isAguardandoValidacaoDocumento()
				|| this.statusAtendimento.isAguardandoShape();
	}

	@Transient
	public boolean isEmAtendimentoSemace() {
		return StatusAtendimento.getListStatusAtendimentoSemace().contains(this.statusAtendimento);
	}

	/**
	 * @return the observacaoPreProcesso
	 */
	public String getObservacaoPreProcesso() {
		return observacaoPreProcesso;
	}

	/**
	 * @param observacaoPreProcesso the observacaoPreProcesso to set
	 */
	public void setObservacaoPreProcesso(String observacaoPreProcesso) {
		this.observacaoPreProcesso = observacaoPreProcesso;
	}

	/**
	 * @return the isentoTaxa
	 */
	public boolean isIsentoTaxa() {
		return isentoTaxa;
	}

	/**
	 * @param isentoTaxa the isentoTaxa to set
	 */
	public void setIsentoTaxa(boolean isentoTaxa) {
		this.isentoTaxa = isentoTaxa;
	}

	public boolean isDocumentacaoPendente() {
		return this.statusAtendimento.isDocumentacaoPendente();
	}

	public List<AgendamentoAnaliseLaboratorial> getAnalises() {
		return analises;
	}

	public void setAnalises(List<AgendamentoAnaliseLaboratorial> analises) {
		this.analises = analises;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public BigDecimal getValorInvestimento() {
		return valorInvestimento;
	}

	public void setValorInvestimento(BigDecimal valorInvestimento) {
		this.valorInvestimento = valorInvestimento;
	}

	public Integer getNumeroEmpregosDireto() {
		return numeroEmpregosDireto;
	}

	public void setNumeroEmpregosDireto(Integer numeroEmpregosDireto) {
		this.numeroEmpregosDireto = numeroEmpregosDireto;
	}

	public BigDecimal getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(BigDecimal faturamento) {
		this.faturamento = faturamento;
	}

	public Integer getNumeroEmpregadoresOperacao() {
		return numeroEmpregadoresOperacao;
	}

	public void setNumeroEmpregadoresOperacao(Integer numeroEmpregadoresOperacao) {
		this.numeroEmpregadoresOperacao = numeroEmpregadoresOperacao;
	}

	public String getNomeProdutoSeloVerde() {
		return nomeProdutoSeloVerde;
	}

	public void setNomeProdutoSeloVerde(String nomeProdutoSeloVerde) {
		this.nomeProdutoSeloVerde = nomeProdutoSeloVerde;
	}

	public String getModeloSeloVerde() {
		return modeloSeloVerde;
	}

	public void setModeloSeloVerde(String modeloSeloVerde) {
		this.modeloSeloVerde = modeloSeloVerde;
	}

	public String getDescricaoSeloVerde() {
		return descricaoSeloVerde;
	}

	public void setDescricaoSeloVerde(String descricaoSeloVerde) {
		this.descricaoSeloVerde = descricaoSeloVerde;
	}

	public String getMateriaPrimaUtilizadaSeloVerde() {
		return materiaPrimaUtilizadaSeloVerde;
	}

	public void setMateriaPrimaUtilizadaSeloVerde(
			String materiaPrimaUtilizadaSeloVerde) {
		this.materiaPrimaUtilizadaSeloVerde = materiaPrimaUtilizadaSeloVerde;
	}

	public Double getPercentualMateriaPrimaSeloVerde() {
		return percentualMateriaPrimaSeloVerde;
	}

	public void setPercentualMateriaPrimaSeloVerde(
			Double percentualMateriaPrimaSeloVerde) {
		this.percentualMateriaPrimaSeloVerde = percentualMateriaPrimaSeloVerde;
	}

	public Cnae getCnae() {
		return cnae;
	}

	public void setCnae(Cnae cnae) {
		this.cnae = cnae;
	}

	public List<ProdutoAgrotoxico> getListProdutosAgrotoxicos() {
		return listProdutosAgrotoxicos;
	}

	public void setListProdutosAgrotoxicos(List<ProdutoAgrotoxico> listProdutosAgrotoxicos) {
		this.listProdutosAgrotoxicos = listProdutosAgrotoxicos;
	}

	public Integer getQuantidadeProdutosAlteracaoLote() {
		return quantidadeProdutosAlteracaoLote;
	}

	public void setQuantidadeProdutosAlteracaoLote(Integer quantidadeProdutosAlteracaoLote) {
		this.quantidadeProdutosAlteracaoLote = quantidadeProdutosAlteracaoLote;
	}

	public String getNumeroCar() {
		return numeroCar;
	}

	public void setNumeroCar(String numeroCar) {
		this.numeroCar = numeroCar;
	}

	public Boolean getPreencheuNumeroCar() {
		return preencheuNumeroCar;
	}

	public void setPreencheuNumeroCar(Boolean preencheuNumeroCar) {
		this.preencheuNumeroCar = preencheuNumeroCar;
	}

	public String getNumeroLicenca() {
		return numeroLicenca;
	}

	public void setNumeroLicenca(String numeroLicenca) {
		this.numeroLicenca = numeroLicenca;
	}

	public LicencaSiga getLicencaSiga() {
		return licencaSiga;
	}

	public void setLicencaSiga(LicencaSiga licencaSiga) {
		this.licencaSiga = licencaSiga;
	}

	public Autorizacao getAutorizacaoSiga() {
		return autorizacaoSiga;
	}

	public void setAutorizacaoSiga(Autorizacao autorizacaoSiga) {
		this.autorizacaoSiga = autorizacaoSiga;
	}

	public Rama getRama() {
		return rama;
	}

	public void setRama(Rama rama) {
		this.rama = rama;
	}

	public Boolean getLocalizadoUnidadeConservacao() {
		return localizadoUnidadeConservacao;
	}

	public void setLocalizadoUnidadeConservacao(Boolean localizadoUnidadeConservacao) {
		this.localizadoUnidadeConservacao = localizadoUnidadeConservacao;
	}

	public List<UnidadeConservacao> getUnidadesConservacao() {
		return unidadesConservacao;
	}

	public void setUnidadesConservacao(List<UnidadeConservacao> unidadesConservacao) {
		this.unidadesConservacao = unidadesConservacao;
	}

	public boolean isPossuiAtividadeRequerCAR() {
		return possuiAtividadeRequerCAR;
	}

	public void setPossuiAtividadeRequerCAR(boolean possuiAtividadeRequerCAR) {
		this.possuiAtividadeRequerCAR = possuiAtividadeRequerCAR;
	}

	public Integer getNumeroVeiculos() {
		return numeroVeiculos;
	}

	public void setNumeroVeiculos(Integer numeroVeiculos) {
		this.numeroVeiculos = numeroVeiculos;
	}

	@Transient
	public Boolean getPendente() {
		return pendente;
	}

	public void setPendente(Boolean pendente) {
		this.pendente = pendente;
	}

	public Setor getSetorDestino() {
		return setorDestino;
	}

	public void setSetorDestino(Setor setorDestino) {
		this.setorDestino = setorDestino;
	}

	public StatusBuscaProcesso getStatusRequerimentoBusca() {
		return statusRequerimentoBusca;
	}

	public void setStatusRequerimentoBusca(StatusBuscaProcesso statusRequerimentoBusca) {
		this.statusRequerimentoBusca = statusRequerimentoBusca;
	}

	public Date getDataImplantacaoGED() {
		return dataImplantacaoGED;
	}

	public void setDataImplantacaoGED(Date dataImplantacaoGED) {
		this.dataImplantacaoGED = dataImplantacaoGED;
	}

	/**
	 * Monta uma {@link String} que contém informações de {@link UnidadeConservacao} pertinentes ao {@link AgendamentoRequerimento} para fins de exibição.
	 *
	 *	<pre><b>Referente a geração da {@link String}, vale ressaltar que:</b>
	 *		<li> Caso a localização ainda não tenha sido informada, é retornada a seguinte {@link String}: "Não informado"
	 *		<li> Caso o empreendimento encontre-se em unidade de conservação, e também tenha sido informado qual ou quais, é retornado a descrição das unidades de conservação
	 *		<li> Caso o empreendimento encontre-se em unidade de conservação, e não tenha não sido informado qual ou quais, é retornada a seguinte {@link String}: "Sim"
	 *		<li> Caso o empreendimento não encontre-se em unidade de conservação, é retornada a seguinte {@link String}: "Não"
	 *	</pre>
	 *
	 * @return
	 *  	retorna a {@link String} gerada a partir do {@link AgendamentoRequerimento}
	 */
	@Deprecated
	@Transient
	public String displayUnidadeConservacao() {
		if(localizadoUnidadeConservacao == null) {
			return "Não informado";
		} else if(localizadoUnidadeConservacao){
			if (unidadesConservacao != null && !unidadesConservacao.isEmpty()) {
				return unidadesConservacao.toString().substring(1, unidadesConservacao.toString().length() - 1);
			}
			return "Sim";
		}

		return "Não";
	}

	public AgendamentoAnaliseLaboratorial parseToAgendamentoAnaliseLaboratorial() {
		AgendamentoAnaliseLaboratorial agendamentoAnalise = new AgendamentoAnaliseLaboratorial();
		agendamentoAnalise.setId(this.getId());
		agendamentoAnalise.setEmpreendimento(this.getEmpreendimento());
		agendamentoAnalise.setEmpreendimentoAtividade(this.getEmpreendimentoAtividade());
		agendamentoAnalise.setAtividade(this.getAtividade());
		agendamentoAnalise.setArquivoShapeValido(this.getArquivoShapeValido());
		agendamentoAnalise.setDocumentacaoValida(this.getDocumentacaoValida());
		agendamentoAnalise.setEnviouArquivoShape(this.getEnviouArquivoShape());
		agendamentoAnalise.setModalidade(this.getModalidade());
		agendamentoAnalise.setSubtipoProcesso(this.getSubtipoProcesso());
		agendamentoAnalise.setRequerimentoAvulso(this.getRequerimentoAvulso());
		agendamentoAnalise.setValidouDocumentacao(this.getValidouDocumentacao());
		agendamentoAnalise.setValidouShape(this.getValidouShape());
		agendamentoAnalise.setStatusAtendimento(this.getStatusAtendimento());
		agendamentoAnalise.setNaoGeraSPU(this.getNaoGeraSPU());
		agendamentoAnalise.setIsProcesso(this.getIsProcesso());
		agendamentoAnalise.setInteressado(this.getInteressado());
		agendamentoAnalise.setConsultoria(this.getConsultoria());
		agendamentoAnalise.setData(this.getData());
		agendamentoAnalise.setDataCancelamento(this.getDataCancelamento());
		agendamentoAnalise.setDataCriacao(this.getDataCriacao());
		agendamentoAnalise.setDataEntrada(this.getDataEntrada());
		agendamentoAnalise.setDestinacaoMateriaPrima(this.getDestinacaoMateriaPrima());
		agendamentoAnalise.setEntregaDocumento(this.getEntregaDocumento());
		agendamentoAnalise.setExcluido(this.getExcluido());
		agendamentoAnalise.setHectare(this.getHectare());
		agendamentoAnalise.setHora(this.getHora());
		agendamentoAnalise.setIdEndereco(this.getIdEndereco());
		agendamentoAnalise.setJustificativa(this.getJustificativa());
		agendamentoAnalise.setLiberadoPendencia(this.getLiberadoPendencia());
		agendamentoAnalise.setMotivoCadastroAgrotoxico(this.getMotivoCadastroAgrotoxico());
		agendamentoAnalise.setNaoGeraSPU(this.getNaoGeraSPU());
		agendamentoAnalise.setNumeroSpu(this.getNumeroSpu());
		agendamentoAnalise.setObjetivo(this.getObjetivo());
		agendamentoAnalise.setObservacao(this.getObservacao());
		agendamentoAnalise.setObservacaoValidacaoDocumento(this.getObservacaoValidacaoDocumento());
		agendamentoAnalise.setProjetosEstrategicos(this.getProjetosEstrategicos());
		agendamentoAnalise.setTelefone(this.getTelefone());
		agendamentoAnalise.setTipoProcesso(this.getTipoProcesso());
		agendamentoAnalise.setStatusRequerimento(this.getStatusRequerimento());
		agendamentoAnalise.setSede(this.getSede());
		agendamentoAnalise.setNumeroDocumento(this.getNumeroDocumento());
		agendamentoAnalise.setObservacaoPreProcesso(this.getObservacaoPreProcesso());
		agendamentoAnalise.setSequencial(this.getSequencial());
		agendamentoAnalise.setAtividadeRequerimentoChecklist(this.getAtividadeRequerimentoChecklist());
		agendamentoAnalise.setUsuario(this.getUsuario());
		agendamentoAnalise.setRequerimento(this.getRequerimento());
		agendamentoAnalise.setSpuProcesso(this.getSpuProcesso());
		agendamentoAnalise.setArquivosShape(this.getArquivosShape());
		agendamentoAnalise.setDocumentacaoFinanceiraValida(this.getDocumentacaoFinanceiraValida());
		agendamentoAnalise.setIsentoTaxa(this.isIsentoTaxa());
		agendamentoAnalise.setDocumento(this.getDocumento());
		agendamentoAnalise.setValorInvestimento(this.getValorInvestimento());
		agendamentoAnalise.setNumeroEmpregosDireto(this.getNumeroEmpregosDireto());
		agendamentoAnalise.setFaturamento(this.getFaturamento());

		return agendamentoAnalise;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		AgendamentoRequerimento other = (AgendamentoRequerimento) obj;
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public List<RequerimentoAtividadeCheckListItemCheckList> getListRACI() {
		return listRACI;
	}

	public void setListRACI(List<RequerimentoAtividadeCheckListItemCheckList> listRACI) {
		this.listRACI = listRACI;
	}

	public List<ArquivoShapeGeo> getListArquivoShapeGeo() {
		return listArquivoShapeGeo;
	}

	public void setListArquivoShapeGeo(List<ArquivoShapeGeo> listArquivoShapeGeo) {
		this.listArquivoShapeGeo = listArquivoShapeGeo;
	}

	public String getDisplayUnidadeConservacao() {
		return displayUnidadeConservacao;
	}

	public void setDisplayUnidadeConservacao(String displayUnidadeConservacao) {
		this.displayUnidadeConservacao = displayUnidadeConservacao;
	}

	public Boolean getPrioritario() {
		return prioritario;
	}

	public void setPrioritario(Boolean prioritario) {
		this.prioritario = prioritario;
	}

	public String getLastUpdateUsername() {
		return lastUpdateUsername;
	}

	public void setLastUpdateUsername(String lastUpdateUsername) {
		this.lastUpdateUsername = lastUpdateUsername;
	}

	public String getDisplayLastUpdateUsername(int length) {
		if (lastUpdateUsername != null && lastUpdateUsername.length() > length) {
			return lastUpdateUsername.substring(0, length - 3) + "...";
		}
		return lastUpdateUsername;
	}

	public SituacaoAtendimentoRequerimento getSituacaoFaseAtendimentoRequerimento() {
		return situacaoFaseAtendimentoRequerimento;
	}

	public void setSituacaoFaseAtendimentoRequerimento(SituacaoAtendimentoRequerimento situacaoFaseAtendimentoRequerimento) {
		this.situacaoFaseAtendimentoRequerimento = situacaoFaseAtendimentoRequerimento;
	}

	public Integer getQdtDiasLicencaVencer() {
		return qdtDiasLicencaVencer;
	}

	public void setQdtDiasLicencaVencer(Integer qdtDiasLicencaVencer) {
		this.qdtDiasLicencaVencer = qdtDiasLicencaVencer;
	}

	@Transient
	public String getDescricaoQuantidadeDiasLicencaVencer(){
		if(qdtDiasLicencaVencer == null){
			return "---";
		}

		if(qdtDiasLicencaVencer < 0){
			return "Vencida há " + (qdtDiasLicencaVencer * (-1)) + " dia(s)";
		}

		return  qdtDiasLicencaVencer + " dia(s) restante(s)";

	}


}