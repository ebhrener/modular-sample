package br.gov.ce.semace.erp.entity.protocolo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.ArquivoDocumento;

@Entity
@Audited
@Table(name = "arquivo_processo", schema = "protocolo")
public class ArquivoProcesso extends ArquivoDocumento {

	private static final long serialVersionUID = 8048332227590666052L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "processo_id")
	private Processo processo = new Processo();

	@Column(name = "juntado")
	private boolean juntado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_juntada")
	private Date dataJuntada;

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public boolean isJuntado() {
		return juntado;
	}

	public void setJuntado(boolean juntado) {
		this.juntado = juntado;
	}

	public Date getDataJuntada() {
		return dataJuntada;
	}

	public void setDataJuntada(Date dataJuntada) {
		this.dataJuntada = dataJuntada;
	}

	@Override
	public ArquivoProcesso clone() throws CloneNotSupportedException {
		ArquivoProcesso clone = new ArquivoProcesso();

		clone.setId(this.getId());
		clone.setDocumentoNatuur(this.getDocumentoNatuur() != null && this.getDocumentoNatuur().getId() != null ? this.getDocumentoNatuur() : null);
		clone.setDocumentoIdSiga(this.getDocumentoIdSiga());
		clone.setTipoDocumentoEntity(this.getTipoDocumentoEntity());
		clone.setGedId(this.getGedId());
		clone.setNumeroSpu(this.getNumeroSpu());
		clone.setNome(this.getNome());
		clone.setTipoConteudo(this.getTipoConteudo());
		clone.setTamanho(this.getTamanho());
		clone.setPagina(this.getPagina());
		clone.setNumeroPaginas(this.getNumeroPaginas());
		clone.setExcluido(this.isExcluido());
		clone.setJustificativa(this.getJustificativa());
		clone.setHash(this.getHash());
		clone.setPublico(this.getPublico());
		clone.setDataCadastro(this.getDataCadastro());
		clone.setNumeroDocumento(this.getNumeroDocumento());
		clone.setTipoAnexo(this.getTipoAnexo());
		clone.setBytes(this.getBytes());
		clone.setFile(this.getFile());
		clone.setIsAssinado(this.isAssinado());
		clone.setArquivoTemporario(this.getArquivoTemporario() != null && this.getArquivoTemporario().getId() != null ? this.getArquivoTemporario() : null);
		clone.setSignatarioList(this.getSignatarioList() != null && !this.getSignatarioList().isEmpty() ? this.getSignatarioList() : null);
		clone.setTipoCriacao(this.getTipoCriacao());

		clone.setProcesso(this.getProcesso() != null && this.getProcesso().getId() != null ? this.getProcesso() : null);
		clone.setJuntado(this.isJuntado());
		clone.setDataJuntada(this.getDataJuntada());

		return clone;
	}
}