
package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.StatusColeta;

@Entity
@Audited
@Table(name="coleta", schema="laboratorio")
public class Coleta implements Serializable {
	
	private static final long serialVersionUID = -7597156710244424096L;
	
	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_coleta", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name="coleta_item_risco", schema="laboratorio", 
		joinColumns = @JoinColumn(name="coleta_id"),
		inverseJoinColumns = @JoinColumn(name="item_risco_id"))
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private List<ItemRisco> itensRisco = new ArrayList<ItemRisco>();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ordem_coleta_id", nullable=false)
	private OrdemColeta ordemColeta = new OrdemColeta();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="condicao_climatica_id")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private CondicaoClimatica condicaoClimatica = new CondicaoClimatica();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ponto_amostragem_id", nullable=false)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private PontoAmostragem pontoAmostragem = new PontoAmostragem();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fator_impedimento_id")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private FatorImpedimento fatorImpedimento = new FatorImpedimento();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario = new Funcionario();
	
	@OneToOne(mappedBy="coleta")
	private ResultadoAnalise resultadoAnalise = new ResultadoAnalise();
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_hora_coleta")
	private Date dataHora;
	
	@Column(name="justificativa", columnDefinition="Text")
	private String justificativa;

	@Column(nullable=false)
	private StatusColeta status = StatusColeta.ABERTA;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ItemRisco> getItensRisco() {
		return itensRisco;
	}

	public void setItensRisco(List<ItemRisco> itensRisco) {
		this.itensRisco = itensRisco;
	}

	public OrdemColeta getOrdemColeta() {
		return ordemColeta;
	}

	public void setOrdemColeta(OrdemColeta ordemColeta) {
		this.ordemColeta = ordemColeta;
	}

	public CondicaoClimatica getCondicaoClimatica() {
		return condicaoClimatica;
	}

	public void setCondicaoClimatica(CondicaoClimatica condicaoClimatica) {
		this.condicaoClimatica = condicaoClimatica;
	}

	public PontoAmostragem getPontoAmostragem() {
		return pontoAmostragem;
	}

	public void setPontoAmostragem(PontoAmostragem pontoAmostragem) {
		this.pontoAmostragem = pontoAmostragem;
	}

	public FatorImpedimento getFatorImpedimento() {
		return fatorImpedimento;
	}

	public void setFatorImpedimento(FatorImpedimento fatorImpedimento) {
		this.fatorImpedimento = fatorImpedimento;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public ResultadoAnalise getResultadoAnalise() {
		return resultadoAnalise;
	}

	public void setResultadoAnalise(ResultadoAnalise resultadoAnalise) {
		this.resultadoAnalise = resultadoAnalise;
	}

	public StatusColeta getStatus() {
		return status;
	}

	public void setStatus(StatusColeta status) {
		this.status = status;
	}
	
}
