package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import br.gov.ce.semace.erp.enuns.TipoArquivo;

@Entity
@Audited
@Table(name = "arquivo_rama", schema = "geral")
public class ArquivoRama implements Serializable {

	private static final long serialVersionUID = 3176252517487632765L;

	@Id @Column(name="id")
	@SequenceGenerator(sequenceName = "geral.seq_arquivo_rama", name = "geral.seq_arquivo_rama", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geral.seq_arquivo_rama")
	private Long id;

	@Column(name="nome_arquivo")
	private String nomeAquivo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rama_id")
	private Rama rama = new Rama();

	@Column(name="fileSystem")
	private String fileSystem;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "org.hibernate.type.BinaryType")
	@NotAudited
	private byte[] arquivo;

	@Column(name = "tipo_arquivo")
	private TipoArquivo tipoArquivo;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNomeAquivo() {
		return nomeAquivo;
	}

	public void setNomeAquivo(String nomeAquivo) {
		this.nomeAquivo = nomeAquivo;
	}

	public Rama getRama() {
		return rama;
	}

	public void setRama(Rama rama) {
		this.rama = rama;
	}

	public String getFileSystem() {
		return fileSystem;
	}

	public void setFileSystem(String fileSystem) {
		this.fileSystem = fileSystem;
	}

	public byte[] getArquivo() {
		return arquivo;
	}

	public void setArquivo(byte[] arquivo) {
		this.arquivo = arquivo;
	}

	public TipoArquivo getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(TipoArquivo tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}


}
