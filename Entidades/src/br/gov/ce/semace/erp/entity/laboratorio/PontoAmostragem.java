package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;
import br.gov.ce.semace.erp.enuns.TipoLaboratorioSistema;

@Entity
@Table(name="ponto_amostragem", schema="laboratorio")
public class PontoAmostragem implements Serializable{

	private static final long serialVersionUID = 6093095317573182387L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_ponto_amostragem", name="laboratorio.seq_ponto_amostragem", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="laboratorio.seq_ponto_amostragem")
	private Long id;

	@Column(name = "tipo_laboratorio_sistema")
	private TipoLaboratorioSistema tipoLaboratorioSistema;

	@ManyToOne(fetch = FetchType.LAZY, optional= false)
	@JoinColumn(name="municipio_id")
	private Cidade cidade = new Cidade();

	@ManyToOne(fetch = FetchType.LAZY, optional=false)
	@JoinColumn(name="regiao_id")
	private Regiao regiao = new Regiao();

	@Column(nullable=false)
	private String codigo;

	private String descricao;

	@Column(name="ponto_referencia")
	private String pontoReferencia;

	@Column(nullable=false)
	private Double latitude;

	@Column(nullable=false)
	private Double longitude;

	@Column(nullable=false)
	private Boolean ativo = true;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="pontoAmostragem")
	private List<Coleta> coletas = new ArrayList<Coleta>();

	@ManyToOne(optional=true, fetch=FetchType.LAZY)
	@JoinColumn(name="regiao_zona_id")
	private RegiaoZonaPontoAmostragem regiaoZonaPontoAmostragem;

	//------------------------------------------------Gets e Setters---------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Regiao getRegiao() {
		return regiao;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getPontoReferencia() {
		return pontoReferencia;
	}

	public void setPontoReferencia(String pontoReferencia) {
		this.pontoReferencia = pontoReferencia;
	}

	public List<Coleta> getColetas() {
		return coletas;
	}

	public void setColetas(List<Coleta> coletas) {
		this.coletas = coletas;
	}

	public TipoLaboratorioSistema getTipoLaboratorioSistema() {
		return tipoLaboratorioSistema;
	}

	public void setTipoLaboratorioSistema(
			TipoLaboratorioSistema tipoLaboratorioSistema) {
		this.tipoLaboratorioSistema = tipoLaboratorioSistema;
	}

	public RegiaoZonaPontoAmostragem getRegiaoZonaPontoAmostragem() {
		return regiaoZonaPontoAmostragem;
	}

	public void setRegiaoZonaPontoAmostragem(RegiaoZonaPontoAmostragem regiaoZonaPontoAmostragem) {
		this.regiaoZonaPontoAmostragem = regiaoZonaPontoAmostragem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PontoAmostragem other = (PontoAmostragem) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}