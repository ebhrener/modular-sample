package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.financeiro.Dae;
import br.gov.ce.semace.erp.entity.financeiro.LetraAtividade;
import br.gov.ce.semace.erp.entity.financeiro.TipoTaxa;
import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.geral.GrupoAtividade;
import br.gov.ce.semace.erp.enuns.Letra;

@Entity
@Audited
@Table(name = "rama_atividade_letra", schema = "licenciamento")
public class RamaAtividadeLetra implements Serializable {

	private static final long serialVersionUID = 2778461506162850832L;

	@Id @Column(name="id")
	@SequenceGenerator(sequenceName = "licenciamento.seq_rama_atividade_letra", name = "licenciamento.seq_rama_atividade_letra", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "licenciamento.seq_rama_atividade_letra")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rama_id")
	private Rama rama;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_id")
	private Atividade atividade = new Atividade();

	@Column(name="excluido")
	private Boolean excluido = false;

	@Column(name="data_cadastro")
	private Date dataCadastro;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_calculo_id")
	private TipoTaxa tipoTaxaCalculo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dae_gerado_id")
	private Dae dae;

	private Letra letra;

	@Column(name="spu_base_regularizacao_licenca_vencida")
	private String spuBaseRegularizacaoLicencaVencida;

	@Column(name="distancia")
	private BigDecimal distancia;

	@Transient
	private List<LetraAtividade> listLetraAtividade = new ArrayList<LetraAtividade>();

	@Transient
	private List<GrupoAtividade> listGrupoAtividade = new ArrayList<GrupoAtividade>();

	@Transient
	private List<Atividade> listAtividade = new ArrayList<Atividade>();

	@Transient
	private GrupoAtividade grupoAtividade = new GrupoAtividade();


	public Rama getRama() {
		return rama;
	}

	public void setRama(Rama rama) {
		this.rama = rama;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public Letra getLetra() {
		return letra;
	}

	public void setLetra(Letra letra) {
		this.letra = letra;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<LetraAtividade> getListLetraAtividade() {
		return listLetraAtividade;
	}

	public void setListLetraAtividade(List<LetraAtividade> listLetraAtividade) {
		this.listLetraAtividade = listLetraAtividade;
	}

	public List<GrupoAtividade> getListGrupoAtividade() {
		return listGrupoAtividade;
	}

	public void setListGrupoAtividade(List<GrupoAtividade> listGrupoAtividade) {
		this.listGrupoAtividade = listGrupoAtividade;
	}

	public List<Atividade> getListAtividade() {
		return listAtividade;
	}

	public void setListAtividade(List<Atividade> listAtividade) {
		this.listAtividade = listAtividade;
	}

	public GrupoAtividade getGrupoAtividade() {
		return grupoAtividade;
	}

	public void setGrupoAtividade(GrupoAtividade grupoAtividade) {
		this.grupoAtividade = grupoAtividade;
	}

	public Boolean getExcluido() {
		return excluido;
	}

	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public TipoTaxa getTipoTaxaCalculo() {
		return tipoTaxaCalculo;
	}

	public void setTipoTaxaCalculo(TipoTaxa tipoTaxaCalculo) {
		this.tipoTaxaCalculo = tipoTaxaCalculo;
	}

	public Dae getDae() {
		return dae;
	}

	public void setDae(Dae dae) {
		this.dae = dae;
	}

	public String getSpuBaseRegularizacaoLicencaVencida() {
		return spuBaseRegularizacaoLicencaVencida;
	}

	public void setSpuBaseRegularizacaoLicencaVencida(String spuBaseRegularizacaoLicencaVencida) {
		this.spuBaseRegularizacaoLicencaVencida = spuBaseRegularizacaoLicencaVencida;
	}

	public BigDecimal getDistancia() {
		return distancia;
	}

	public void setDistancia(BigDecimal distancia) {
		this.distancia = distancia;
	}

}
