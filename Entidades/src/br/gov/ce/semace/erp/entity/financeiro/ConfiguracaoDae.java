package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "financeiro", name = "configuracao_dae")
public class ConfiguracaoDae implements Serializable {

	private static final long serialVersionUID = -8989587172198189581L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_configuracao_dae", name = "SEQ_CONFIGURACAO_DAE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CONFIGURACAO_DAE")
	private Long id;

	public TipoTaxa getTipoTaxaJulgamentoAutoInfracao() {
		return tipoTaxaJulgamentoAutoInfracao;
	}

	public void setTipoTaxaJulgamentoAutoInfracao(
			TipoTaxa tipoTaxaJulgamentoAutoInfracao) {
		this.tipoTaxaJulgamentoAutoInfracao = tipoTaxaJulgamentoAutoInfracao;
	}

	@Column(name = "cnpj_cedente")
	private String cnpjCedente;

	@Column(name = "nome_cedente")
	private String nomeCedente;

	@Column(name = "prefixo_nosso_numero")
	private String prefixoNossoNumero;

	@Column(name = "fator_custo_quilometragem", precision=15, scale=4)
	private BigDecimal fatorCustoQuilometragem;

	@Column(name = "fator_distancia", precision=15, scale=4)
	private BigDecimal fatorDistancia;

	@Column(name = "fator_custo_hora_tecnica", precision=15, scale=4)
	private BigDecimal fatorCustoHoraTecnica;

	@Column(name = "fator_analise_tecnica", precision=15, scale=4)
	private BigDecimal fatorAnaliseTecnica;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_auto_infracao_id")
	private TipoTaxa tipoTaxaAutoInfracao = new TipoTaxa();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_julgamento_auto_infracao_id")
	private TipoTaxa tipoTaxaJulgamentoAutoInfracao = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_compensacao_ambiental_id")
	private TipoTaxa tipoTaxaCompensacaoAmbiental = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_tcfa_id")
	private TipoTaxa tipoTaxaTcfa = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_divida_ativa_id")
	private TipoTaxa tipoTaxaDividaAtiva = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_parcelamento_auto_infracao_id")
	private TipoTaxa tipoTaxaParcelamentoAutoInfracao = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_parcelamento_divida_ativa_id")
	private TipoTaxa tipoTaxaParcelamentoDividaAtiva = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_complementar_renovacao_id")
	private TipoTaxa tipoTaxaComplementarRenovacao = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_auto_infracao_btzfm_id")
	private TipoTaxa tipoTaxaAutoInfracaoBlitzFumacaNegra = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_estudo_ambiental_id")
	private TipoTaxa tipoTaxaEstudoAmbiental = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_analise_laboratorial_id")
	private TipoTaxa tipoTaxaAnaliseLaboratorial = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_termo_id")
	private TipoTaxa tipoTaxaTermo = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_cadastro_consumo_florestal_id")
	private TipoTaxa tipoTaxaCadastroConsumoFlorestal = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_multa_contratual")
	private TipoTaxa tipoTaxaMultaContratual = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_compensacao_ambiental_sem_divisao")
	private TipoTaxa tipoTaxaCompensacaoAmbientalSemDivisao = new TipoTaxa();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_parcelamento_termo_apreensao_deposito")
	private TipoTaxa tipoTaxaParcelamentoTermoApreensaoDeposito;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_compensacao_ambiental_indenizacao_ressarcimento")
	private TipoTaxa tipoTaxaCompensacaoAmbientalIndenizacaoRessarcimento;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_medida_compensatoria_decisao_judicial")
	private TipoTaxa tipoTaxaMedidaCompensatoriaDecisaoJudicial;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_multa_descumprimento_tac_id")
	private TipoTaxa tipoTaxaMultaDescumprimentoTAC;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_reanalise_compensacao_ambiental")
	private TipoTaxa tipoTaxaReanaliseCompensacaoAmbiental = new TipoTaxa();

	private Boolean ativo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCnpjCedente() {
		return cnpjCedente;
	}

	public void setCnpjCedente(String cnpjCedente) {
		this.cnpjCedente = cnpjCedente;
	}

	public String getNomeCedente() {
		return nomeCedente;
	}

	public void setNomeCedente(String nomeCedente) {
		this.nomeCedente = nomeCedente;
	}

	public String getPrefixoNossoNumero() {
		return prefixoNossoNumero;
	}

	public void setPrefixoNossoNumero(String prefixoNossoNumero) {
		this.prefixoNossoNumero = prefixoNossoNumero;
	}

	public BigDecimal getFatorCustoQuilometragem() {
		return fatorCustoQuilometragem;
	}

	public void setFatorCustoQuilometragem(BigDecimal fatorCustoQuilometragem) {
		this.fatorCustoQuilometragem = fatorCustoQuilometragem;
	}

	public BigDecimal getFatorDistancia() {
		return fatorDistancia;
	}

	public void setFatorDistancia(BigDecimal fatorDistancia) {
		this.fatorDistancia = fatorDistancia;
	}

	public BigDecimal getFatorCustoHoraTecnica() {
		return fatorCustoHoraTecnica;
	}

	public void setFatorCustoHoraTecnica(BigDecimal fatorCustoHoraTecnica) {
		this.fatorCustoHoraTecnica = fatorCustoHoraTecnica;
	}

	public BigDecimal getFatorAnaliseTecnica() {
		return fatorAnaliseTecnica;
	}

	public void setFatorAnaliseTecnica(BigDecimal fatorAnaliseTecnica) {
		this.fatorAnaliseTecnica = fatorAnaliseTecnica;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public TipoTaxa getTipoTaxaAutoInfracao() {
		return tipoTaxaAutoInfracao;
	}

	public void setTipoTaxaAutoInfracao(TipoTaxa tipoTaxaAutoInfracao) {
		this.tipoTaxaAutoInfracao = tipoTaxaAutoInfracao;
	}

	public TipoTaxa getTipoTaxaCompensacaoAmbiental() {
		return tipoTaxaCompensacaoAmbiental;
	}

	public void setTipoTaxaCompensacaoAmbiental(
			TipoTaxa tipoTaxaCompensacaoAmbiental) {
		this.tipoTaxaCompensacaoAmbiental = tipoTaxaCompensacaoAmbiental;
	}

	public TipoTaxa getTipoTaxaDividaAtiva() {
		return tipoTaxaDividaAtiva;
	}

	public void setTipoTaxaDividaAtiva(TipoTaxa tipoTaxaDividaAtiva) {
		this.tipoTaxaDividaAtiva = tipoTaxaDividaAtiva;
	}

	public TipoTaxa getTipoTaxaParcelamentoAutoInfracao() {
		return tipoTaxaParcelamentoAutoInfracao;
	}

	public void setTipoTaxaParcelamentoAutoInfracao(
			TipoTaxa tipoTaxaParcelamentoAutoInfracao) {
		this.tipoTaxaParcelamentoAutoInfracao = tipoTaxaParcelamentoAutoInfracao;
	}

	public TipoTaxa getTipoTaxaParcelamentoDividaAtiva() {
		return tipoTaxaParcelamentoDividaAtiva;
	}

	public void setTipoTaxaParcelamentoDividaAtiva(
			TipoTaxa tipoTaxaParcelamentoDividaAtiva) {
		this.tipoTaxaParcelamentoDividaAtiva = tipoTaxaParcelamentoDividaAtiva;
	}

	public TipoTaxa getTipoTaxaTcfa() {
		return tipoTaxaTcfa;
	}

	public void setTipoTaxaTcfa(TipoTaxa tipoTaxaTcfa) {
		this.tipoTaxaTcfa = tipoTaxaTcfa;
	}

	public TipoTaxa getTipoTaxaAutoInfracaoBlitzFumacaNegra() {
		return tipoTaxaAutoInfracaoBlitzFumacaNegra;
	}

	public void setTipoTaxaAutoInfracaoBlitzFumacaNegra(
			TipoTaxa tipoTaxaAutoInfracaoBlitzFumacaNegra) {
		this.tipoTaxaAutoInfracaoBlitzFumacaNegra = tipoTaxaAutoInfracaoBlitzFumacaNegra;
	}

	public TipoTaxa getTipoTaxaTermo() {
		return tipoTaxaTermo;
	}

	public void setTipoTaxaTermo(TipoTaxa tipoTaxaTermo) {
		this.tipoTaxaTermo = tipoTaxaTermo;
	}

	public TipoTaxa getTipoTaxaEstudoAmbiental() {
		return tipoTaxaEstudoAmbiental;
	}

	public void setTipoTaxaEstudoAmbiental(TipoTaxa tipoTaxaEstudoAmbiental) {
		this.tipoTaxaEstudoAmbiental = tipoTaxaEstudoAmbiental;
	}

	public TipoTaxa getTipoTaxaAnaliseLaboratorial() {
		return tipoTaxaAnaliseLaboratorial;
	}

	public void setTipoTaxaAnaliseLaboratorial(
			TipoTaxa tipoTaxaAnaliseLaboratorial) {
		this.tipoTaxaAnaliseLaboratorial = tipoTaxaAnaliseLaboratorial;
	}

	public TipoTaxa getTipoTaxaCadastroConsumoFlorestal() {
		return tipoTaxaCadastroConsumoFlorestal;
	}

	public void setTipoTaxaCadastroConsumoFlorestal(
			TipoTaxa tipoTaxaCadastroConsumoFlorestal) {
		this.tipoTaxaCadastroConsumoFlorestal = tipoTaxaCadastroConsumoFlorestal;
	}

	public TipoTaxa getTipoTaxaComplementarRenovacao() {
		return tipoTaxaComplementarRenovacao;
	}

	public void setTipoTaxaComplementarRenovacao(
			TipoTaxa tipoTaxaComplementarRenovacao) {
		this.tipoTaxaComplementarRenovacao = tipoTaxaComplementarRenovacao;
	}

	public TipoTaxa getTipoTaxaMultaContratual() {
		return tipoTaxaMultaContratual;
	}

	public void setTipoTaxaMultaContratual(TipoTaxa tipoTaxaMultaContratual) {
		this.tipoTaxaMultaContratual = tipoTaxaMultaContratual;
	}

	public TipoTaxa getTipoTaxaCompensacaoAmbientalSemDivisao() {
		return tipoTaxaCompensacaoAmbientalSemDivisao;
	}

	public void setTipoTaxaCompensacaoAmbientalSemDivisao(TipoTaxa tipoTaxaCompensacaoAmbientalSemDivisao) {
		this.tipoTaxaCompensacaoAmbientalSemDivisao = tipoTaxaCompensacaoAmbientalSemDivisao;
	}

	public TipoTaxa getTipoTaxaParcelamentoTermoApreensaoDeposito() {
		return tipoTaxaParcelamentoTermoApreensaoDeposito;
	}

	public void setTipoTaxaParcelamentoTermoApreensaoDeposito(TipoTaxa tipoTaxaParcelamentoTermoApreensaoDeposito) {
		this.tipoTaxaParcelamentoTermoApreensaoDeposito = tipoTaxaParcelamentoTermoApreensaoDeposito;
	}

	public TipoTaxa getTipoTaxaCompensacaoAmbientalIndenizacaoRessarcimento() {
		return tipoTaxaCompensacaoAmbientalIndenizacaoRessarcimento;
	}

	public void setTipoTaxaCompensacaoAmbientalIndenizacaoRessarcimento(
			TipoTaxa tipoTaxaCompensacaoAmbientalIndenizacaoRessarcimento) {
		this.tipoTaxaCompensacaoAmbientalIndenizacaoRessarcimento = tipoTaxaCompensacaoAmbientalIndenizacaoRessarcimento;
	}

	public TipoTaxa getTipoTaxaMedidaCompensatoriaDecisaoJudicial() {
		return tipoTaxaMedidaCompensatoriaDecisaoJudicial;
	}

	public void setTipoTaxaMedidaCompensatoriaDecisaoJudicial(
			TipoTaxa tipoTaxaMedidaCompensatoriaDecisaoJudicial) {
		this.tipoTaxaMedidaCompensatoriaDecisaoJudicial = tipoTaxaMedidaCompensatoriaDecisaoJudicial;
	}

	public TipoTaxa getTipoTaxaMultaDescumprimentoTAC() {
		return tipoTaxaMultaDescumprimentoTAC;
	}

	public void setTipoTaxaMultaDescumprimentoTAC(TipoTaxa tipoTaxaMultaDescumprimentoTAC) {
		this.tipoTaxaMultaDescumprimentoTAC = tipoTaxaMultaDescumprimentoTAC;
	}

	public TipoTaxa getTipoTaxaReanaliseCompensacaoAmbiental() {
		return tipoTaxaReanaliseCompensacaoAmbiental;
	}

	public void setTipoTaxaReanaliseCompensacaoAmbiental(TipoTaxa tipoTaxaReanaliseCompensacaoAmbiental) {
		this.tipoTaxaReanaliseCompensacaoAmbiental = tipoTaxaReanaliseCompensacaoAmbiental;
	}

}