package br.gov.ce.semace.erp.entity.geral;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "arquivo_pendencia", schema = "geral")
public class ArquivoPendencia extends ArquivoDocumento {

	private static final long serialVersionUID = 3342374545748743745L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pendencia_id")
	private Pendencia pendencia = new Pendencia();

	public ArquivoPendencia() {
		super();
	}

	public ArquivoPendencia(ArquivoDocumento arquivoDocumento, Pendencia pendencia) {
		this.setId(arquivoDocumento.getId());
		this.setDocumentoNatuur(arquivoDocumento.getDocumentoNatuur());
		this.setDocumentoIdSiga(arquivoDocumento.getDocumentoIdSiga());
		this.setGedId(arquivoDocumento.getGedId());
		this.setNumeroSpu(arquivoDocumento.getNumeroSpu());
		this.setTipoCriacao(arquivoDocumento.getTipoCriacao());
		this.setNome(arquivoDocumento.getNome());
		this.setTipoDocumentoEntity(arquivoDocumento.getTipoDocumentoEntity());
		this.setTipoConteudo(arquivoDocumento.getTipoConteudo());
		this.setTamanho(arquivoDocumento.getTamanho());
		this.setArquivoTemporario(arquivoDocumento.getArquivoTemporario());
		this.setPagina(arquivoDocumento.getPagina());
		this.setNumeroPaginas(arquivoDocumento.getNumeroPaginas());
		this.setPublico(arquivoDocumento.getPublico());
		this.setExcluido(arquivoDocumento.isExcluido());
		this.setJustificativa(arquivoDocumento.getJustificativa());
		this.setHash(arquivoDocumento.getHash());
		this.pendencia = pendencia;
	}

	public Pendencia getPendencia() {
		return pendencia;
	}

	public void setPendencia(Pendencia pendencia) {
		this.pendencia = pendencia;
	}
}
