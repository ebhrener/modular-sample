package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "arquivo_digitalizado_tci", schema = "protocolo")
public class ArquivoDigitalizadoTci implements Serializable, Comparable<ArquivoDigitalizadoTci> {

	/**
	 *
	 */
	private static final long serialVersionUID = 5086060719830054775L;

	@Id
	@SequenceGenerator(sequenceName = "protocolo.seq_arquivo_digitalizado_tci", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(name = "ordem")
	private Integer ordem;

	@Column(name = "spu")
	private String spu;

	@Column(name = "indice_ged")
	private Long indiceGED;

	@Column(name = "caminho")
	private String caminho;

	@Column(name = "possui_arquivo")
	private boolean possuiArquivo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public Long getIndiceGED() {
		return indiceGED;
	}

	public void setIndiceGED(Long indiceGED) {
		this.indiceGED = indiceGED;
	}

	@Override
	public int compareTo(ArquivoDigitalizadoTci o) {
		// TODO Auto-generated method stub
		return this.getOrdem().compareTo(o.getOrdem());
	}

	public String getCaminho() {
		return caminho;
	}

	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}

	public boolean isPossuiArquivo() {
		return possuiArquivo;
	}

	public void setPossuiArquivo(boolean possuiArquivo) {
		this.possuiArquivo = possuiArquivo;
	}

}
