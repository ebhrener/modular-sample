package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="rh", name="tipo_comissao")
public class TipoComissao implements Serializable{

	private static final long serialVersionUID = 5891778974031858073L;

	@Id
	@SequenceGenerator(sequenceName="rh.seq_tipo_comissao", name="SEQCOMISSAO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQCOMISSAO")
	private Long id;

	@NotNull(message="Campo Sigla é obrigatorio")
	@Column(nullable=false)
	private String sigla;

	@NotNull(message="Campo Descrição é obrigatorio")
	@Column(nullable=false)
	private String descricao;

	//------------------------------------------------Gets e Setters---------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
