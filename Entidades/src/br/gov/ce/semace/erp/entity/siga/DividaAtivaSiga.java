package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import br.gov.ce.semace.erp.entity.juridico.OrigemDivida;

public class DividaAtivaSiga implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -998904785167722634L;

	private Integer numCDA;
	private String devedor;
	private String cpf;
	private String cnpj;
	private String endereco;
	private String numeroAIF;
	private Date dataCadastro;
	private String valorDebito;
	private Integer ano;
	private String municipio;
	private String numeroProcesso;
	private String dispositivoInfrigido;
	private String dataQuitacao;
	private String numeroExecucaoFiscal;
	private Integer status;
	private String motivoAuto;
	private BigDecimal valorDivida;
	private String origemDividaText;
	private OrigemDivida origemDivida;
	private String dataVencimento;
	private String cep;
	private String atividade;
	private String fone;
	private String bairro;
	private String periodo;
	private BigInteger codigoIbge;

	public DividaAtivaSiga() {
		super();
	}

	public DividaAtivaSiga(Integer numCDA, String devedor, String cpf,
			String cnpj, String endereco, String numeroAIF, Date dataCadastro,
			String valorDebito, Integer ano, String municipio,
			String numeroProcesso, String dispositivoInfrigido,
			String dataQuitacao, OrigemDivida origemDivida,
			String numeroExecucaoFiscal, Integer status, String motivoAuto,
			BigDecimal valorDivida, String dataVencimento,
			String origemDividaText, String cep, String atividade, String fone, String bairro, String periodo) {
		super();
		this.numCDA = numCDA;
		this.devedor = devedor;
		this.setCpf(cpf);
		this.setCnpj(cnpj);
		this.endereco = endereco;
		this.numeroAIF = numeroAIF;
		this.dataCadastro = dataCadastro;
		this.valorDebito = valorDebito;
		this.ano = ano;
		this.municipio = municipio;
		this.numeroProcesso = numeroProcesso;
		this.dispositivoInfrigido = dispositivoInfrigido;
		this.dataQuitacao = dataQuitacao;
		this.origemDivida = origemDivida;
		this.numeroExecucaoFiscal = numeroExecucaoFiscal;
		this.status = status;
		this.motivoAuto = motivoAuto;
		this.valorDivida = valorDivida;
		this.dataVencimento = dataVencimento;
		this.origemDividaText = origemDividaText;
		this.cep = cep;
		this.atividade = atividade;
		this.fone = fone;
		this.bairro = bairro;
		this.periodo = periodo;
	}

	public String getDevedor() {
		return devedor;
	}

	public void setDevedor(String devedor) {
		this.devedor = devedor;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumeroAIF() {
		return numeroAIF;
	}

	public void setNumeroAIF(String numeroAIF) {
		this.numeroAIF = numeroAIF;
	}

	public String getValorDebito() {
		return valorDebito;
	}

	public void setValorDebito(String valorDebito) {
		this.valorDebito = valorDebito;
	}

	public Integer getNumCDA() {
		return numCDA;
	}

	public void setNumCDA(Integer numCDA) {
		this.numCDA = numCDA;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getDocumento() {
		if (cpf != null && !cpf.trim().isEmpty()) {
			return cpf;
		}
		if (cnpj != null && !cnpj.trim().isEmpty()) {
			return cnpj;
		}
		return null;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public String getDispositivoInfrigido() {
		return dispositivoInfrigido;
	}

	public void setDispositivoInfrigido(String dispositivoInfrigido) {
		this.dispositivoInfrigido = dispositivoInfrigido;
	}

	public String getDataQuitacao() {
		return dataQuitacao;
	}

	public void setDataQuitacao(String dataQuitacao) {
		this.dataQuitacao = dataQuitacao;
	}

	public OrigemDivida getOrigemDivida() {
		return origemDivida;
	}

	public void setOrigemDivida(OrigemDivida origemDivida) {
		this.origemDivida = origemDivida;
	}

	public String getNumeroExecucaoFiscal() {
		return numeroExecucaoFiscal;
	}

	public void setNumeroExecucaoFiscal(String numeroExecucaoFiscal) {
		this.numeroExecucaoFiscal = numeroExecucaoFiscal;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMotivoAuto() {
		return motivoAuto;
	}

	public void setMotivoAuto(String motivoAuto) {
		this.motivoAuto = motivoAuto;
	}

	public BigDecimal getValorDivida() {
		return valorDivida;
	}

	public void setValorDivida(BigDecimal valorDivida) {
		this.valorDivida = valorDivida;
	}

	public String getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getOrigemDividaText() {
		return origemDividaText;
	}

	public void setOrigemDividaText(String origemDividaText) {
		this.origemDividaText = origemDividaText;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getAtividade() {
		return atividade;
	}

	public void setAtividade(String atividade) {
		this.atividade = atividade;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public BigInteger getCodigoIbge() {
		return codigoIbge;
	}

	public void setCodigoIbge(BigInteger codigoIbge) {
		this.codigoIbge = codigoIbge;
	}
	
	public String getNumeroDocumento() {
		return numCDA+"/"+ano;
	}
}
