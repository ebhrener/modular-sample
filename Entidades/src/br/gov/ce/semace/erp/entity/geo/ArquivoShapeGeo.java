package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.AgendamentoRequerimento;
import br.gov.ce.semace.erp.entity.atendimento.requerimento.RequerimentoAtividadeCheckListItemCheckList;
import br.gov.ce.semace.erp.enuns.ClassificacaoShape;
import br.gov.ce.semace.erp.enuns.StatusProcessamentoShape;
import br.gov.ce.semace.erp.enuns.TipoArquivoShape;


@Entity
@Audited
@Table(name="arquivo_shape_geo", schema = "geo")
public class ArquivoShapeGeo implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -8443909129767590984L;

	@Id
	@SequenceGenerator(sequenceName="geo.seq_arquivo_shape_geo", name="geo.seq_arquivo_shape_geo", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="geo.seq_arquivo_shape_geo")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_cadastro", nullable=false)
	private Date dataCadastro;

	@JoinColumn(name = "requerimento_id")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private AgendamentoRequerimento agendamentoRequerimento;

	@Column(name = "spu")
	private String spu;

	@JoinColumn(name = "raci_id")
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private RequerimentoAtividadeCheckListItemCheckList requerimentoAtividadeCheckListItemCheckList;

	@Enumerated(EnumType.ORDINAL)
	@Column(name="classificacao_shape", nullable = true)
	private ClassificacaoShape classificacaoShape;

	@Enumerated(EnumType.ORDINAL)
	@Column(name="tipo_arquivo_shape")
	private TipoArquivoShape tipoArquivoShape;

	@Enumerated(EnumType.ORDINAL)
	@Column(name="status_processamento_shape")
	private StatusProcessamentoShape statusProcessamentoShape;

	@NotAudited
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "bytes")
	@Type(type = "org.hibernate.type.BinaryType")
	private byte[] bytes;

	@Column(nullable=false)
	private String nome;

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public RequerimentoAtividadeCheckListItemCheckList getRequerimentoAtividadeCheckListItemCheckList() {
		return requerimentoAtividadeCheckListItemCheckList;
	}

	public void setRequerimentoAtividadeCheckListItemCheckList(
			RequerimentoAtividadeCheckListItemCheckList requerimentoAtividadeCheckListItemCheckList) {
		this.requerimentoAtividadeCheckListItemCheckList = requerimentoAtividadeCheckListItemCheckList;
	}

	public ClassificacaoShape getClassificacaoShape() {
		return classificacaoShape;
	}

	public void setClassificacaoShape(ClassificacaoShape classificacaoShape) {
		this.classificacaoShape = classificacaoShape;
	}

	public TipoArquivoShape getTipoArquivoShape() {
		return tipoArquivoShape;
	}

	public void setTipoArquivoShape(TipoArquivoShape tipoArquivoShape) {
		this.tipoArquivoShape = tipoArquivoShape;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public StatusProcessamentoShape getStatusProcessamentoShape() {
		return statusProcessamentoShape;
	}

	public void setStatusProcessamentoShape(StatusProcessamentoShape statusProcessamentoShape) {
		this.statusProcessamentoShape = statusProcessamentoShape;
	}

	public AgendamentoRequerimento getAgendamentoRequerimento() {
		return agendamentoRequerimento;
	}

	public void setAgendamentoRequerimento(AgendamentoRequerimento agendamentoRequerimento) {
		this.agendamentoRequerimento = agendamentoRequerimento;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public String getNumeroRequerimento() {
		if (agendamentoRequerimento != null && agendamentoRequerimento.getNumeroDocumento() != null && !agendamentoRequerimento.getNumeroDocumento().trim().isEmpty()) {
			return agendamentoRequerimento.getNumeroDocumento();
		}

		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
