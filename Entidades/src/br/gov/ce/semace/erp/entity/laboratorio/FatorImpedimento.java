package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.semace.erp.enuns.TipoFatorImpedimento;

@Entity
@Table(name="fator_impedimento", schema="laboratorio")
public class FatorImpedimento implements Serializable {
	
	private static final long serialVersionUID = -7594925739202149148L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_fator_impedimento", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@Column(name = "tipo_fator_impedimento")
	private TipoFatorImpedimento tipoFatorImpedimento;
	
	private String descricao;
	
	@OneToMany(mappedBy="fatorImpedimento")
	private List<ResultadoAnalise> resultadosAnalise = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoFatorImpedimento getTipoFatorImpedimento() {
		return tipoFatorImpedimento;
	}

	public void setTipoFatorImpedimento(TipoFatorImpedimento tipoFatorImpedimento) {
		this.tipoFatorImpedimento = tipoFatorImpedimento;
	}

	public List<ResultadoAnalise> getResultadosAnalise() {
		return resultadosAnalise;
	}

	public void setResultadosAnalise(List<ResultadoAnalise> resultadosAnalise) {
		this.resultadosAnalise = resultadosAnalise;
	}
	
}
