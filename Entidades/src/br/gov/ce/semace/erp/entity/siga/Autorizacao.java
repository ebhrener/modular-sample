package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Autorizacao implements Serializable {

	private static final long serialVersionUID = -7487219819835067183L;

	private String autorizacaoId;
	private String spuProcesso;
	private String tipoProcesso;
	private Date dataValidade;
	private String processoID;
	private Long empreendimentoID;
	private Boolean apa;
	private BigDecimal taxaLicenciamento;
	private String textoAutorizacao;
	private Boolean renovacao;
	private Boolean regularizacao;
	private Date dataEmissao;
	private String coordAssinouID;
	private String usuarioAssinouID;
	private String areaID;
	private String condicionaantesEspecifica;
	private Integer talhao;
	private String parecer;
	private String localidade;
	private String municipio;
	private BigDecimal areaTotal;
	private BigDecimal areaReserva;
	private BigDecimal areaDesmatamento;
	private String coordenadas;
	private BigDecimal rendimentoTotal;
	private String undLenha;
	private BigDecimal lenha;
	private BigDecimal carvao;
	private String contrato;
	private BigDecimal saldoGuias;
	private Date entregue;
	private Date dataAssinatura;
	private String segViaID;
	private boolean vegetacao;
	private String relatorio;
	private BigDecimal reposicaoFlorestal;
	private BigDecimal numeroArvores;
	private String spuOriginaouID;
	private BigDecimal volumeMadeira;
	private String objetivo;
	private String descricaoEmpreendimento;
	private String observacao;
	private Boolean vara;
	private Double mourao;
	private Double areaAPP;
	private Double areaTotalPlano;
	private Double escora;
	private Integer tipoEspecieID;
	private Double talhao2;
	private Double coordenadaX;
	private Double coordenadaY;
	private Double areaEfetivoManejo;
	private String descricaoAcesso;
	private String numeroAutorizacaoRepflor;
	private String spuOriginouAETID;
	private Integer zonaID;
	private Integer empreendimentoNatuurID;
	private String unidadeMedidadeArea;
	private String usuarioCoordId;
	private String areaNucleoId;
	private String estaca;
	private String tora;

	public String getAutorizacaoId() {
		return autorizacaoId;
	}

	public void setAutorizacaoId(String autorizacaoId) {
		this.autorizacaoId = autorizacaoId;
	}

	public String getSpuProcesso() {
		return spuProcesso;
	}

	public void setSpuProcesso(String spuProcesso) {
		this.spuProcesso = spuProcesso;
	}

	public String getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(String tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}

	public String getProcessoID() {
		return processoID;
	}

	public void setProcessoID(String processoID) {
		this.processoID = processoID;
	}

	public Long getEmpreendimentoID() {
		return empreendimentoID;
	}

	public void setEmpreendimentoID(Long empreendimentoID) {
		this.empreendimentoID = empreendimentoID;
	}

	public Boolean getApa() {
		return apa;
	}

	public void setApa(Boolean apa) {
		this.apa = apa;
	}

	public BigDecimal getTaxaLicenciamento() {
		return taxaLicenciamento;
	}

	public void setTaxaLicenciamento(BigDecimal taxaLicenciamento) {
		this.taxaLicenciamento = taxaLicenciamento;
	}

	public String getTextoAutorizacao() {
		return textoAutorizacao;
	}

	public void setTextoAutorizacao(String textoAutorizacao) {
		this.textoAutorizacao = textoAutorizacao;
	}

	public Boolean getRenovacao() {
		return renovacao;
	}

	public void setRenovacao(Boolean renovacao) {
		this.renovacao = renovacao;
	}

	public Boolean getRegularizacao() {
		return regularizacao;
	}

	public void setRegularizacao(Boolean regularizacao) {
		this.regularizacao = regularizacao;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getCoordAssinouID() {
		return coordAssinouID;
	}

	public void setCoordAssinouID(String coordAssinouID) {
		this.coordAssinouID = coordAssinouID;
	}

	public String getUsuarioAssinouID() {
		return usuarioAssinouID;
	}

	public void setUsuarioAssinouID(String usuarioAssinouID) {
		this.usuarioAssinouID = usuarioAssinouID;
	}

	public String getAreaID() {
		return areaID;
	}

	public void setAreaID(String areaID) {
		this.areaID = areaID;
	}

	public String getCondicionaantesEspecifica() {
		return condicionaantesEspecifica;
	}

	public void setCondicionaantesEspecifica(String condicionaantesEspecifica) {
		this.condicionaantesEspecifica = condicionaantesEspecifica;
	}

	public Integer getTalhao() {
		return talhao;
	}

	public void setTalhao(Integer talhao) {
		this.talhao = talhao;
	}

	public String getParecer() {
		return parecer;
	}

	public void setParecer(String parecer) {
		this.parecer = parecer;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public BigDecimal getAreaTotal() {
		return areaTotal;
	}

	public void setAreaTotal(BigDecimal areaTotal) {
		this.areaTotal = areaTotal;
	}

	public BigDecimal getAreaReserva() {
		return areaReserva;
	}

	public void setAreaReserva(BigDecimal areaReserva) {
		this.areaReserva = areaReserva;
	}

	public BigDecimal getAreaDesmatamento() {
		return areaDesmatamento;
	}

	public void setAreaDesmatamento(BigDecimal areaDesmatamento) {
		this.areaDesmatamento = areaDesmatamento;
	}

	public String getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}

	public BigDecimal getRendimentoTotal() {
		return rendimentoTotal;
	}

	public void setRendimentoTotal(BigDecimal rendimentoTotal) {
		this.rendimentoTotal = rendimentoTotal;
	}

	public String getUndLenha() {
		return undLenha;
	}

	public void setUndLenha(String undLenha) {
		this.undLenha = undLenha;
	}

	public BigDecimal getLenha() {
		return lenha;
	}

	public void setLenha(BigDecimal lenha) {
		this.lenha = lenha;
	}

	public BigDecimal getCarvao() {
		return carvao;
	}

	public void setCarvao(BigDecimal carvao) {
		this.carvao = carvao;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public BigDecimal getSaldoGuias() {
		return saldoGuias;
	}

	public void setSaldoGuias(BigDecimal saldoGuias) {
		this.saldoGuias = saldoGuias;
	}

	public Date getEntregue() {
		return entregue;
	}

	public void setEntregue(Date entregue) {
		this.entregue = entregue;
	}

	public Date getDataAssinatura() {
		return dataAssinatura;
	}

	public void setDataAssinatura(Date dataAssinatura) {
		this.dataAssinatura = dataAssinatura;
	}

	public String getSegViaID() {
		return segViaID;
	}

	public void setSegViaID(String segViaID) {
		this.segViaID = segViaID;
	}

	public boolean isVegetacao() {
		return vegetacao;
	}

	public void setVegetacao(boolean vegetacao) {
		this.vegetacao = vegetacao;
	}

	public String getRelatorio() {
		return relatorio;
	}

	public void setRelatorio(String relatorio) {
		this.relatorio = relatorio;
	}

	public BigDecimal getReposicaoFlorestal() {
		return reposicaoFlorestal;
	}

	public void setReposicaoFlorestal(BigDecimal reposicaoFlorestal) {
		this.reposicaoFlorestal = reposicaoFlorestal;
	}

	public BigDecimal getNumeroArvores() {
		return numeroArvores;
	}

	public void setNumeroArvores(BigDecimal numeroArvores) {
		this.numeroArvores = numeroArvores;
	}

	public String getSpuOriginaouID() {
		return spuOriginaouID;
	}

	public void setSpuOriginaouID(String spuOriginaouID) {
		this.spuOriginaouID = spuOriginaouID;
	}

	public BigDecimal getVolumeMadeira() {
		return volumeMadeira;
	}

	public void setVolumeMadeira(BigDecimal volumeMadeira) {
		this.volumeMadeira = volumeMadeira;
	}

	public String getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public String getDescricaoEmpreendimento() {
		return descricaoEmpreendimento;
	}

	public void setDescricaoEmpreendimento(String descricaoEmpreendimento) {
		this.descricaoEmpreendimento = descricaoEmpreendimento;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Boolean getVara() {
		return vara;
	}

	public void setVara(Boolean vara) {
		this.vara = vara;
	}

	public Double getMourao() {
		return mourao;
	}

	public void setMourao(Double mourao) {
		this.mourao = mourao;
	}

	public Double getAreaAPP() {
		return areaAPP;
	}

	public void setAreaAPP(Double areaAPP) {
		this.areaAPP = areaAPP;
	}

	public Double getAreaTotalPlano() {
		return areaTotalPlano;
	}

	public void setAreaTotalPlano(Double areaTotalPlano) {
		this.areaTotalPlano = areaTotalPlano;
	}

	public Double getEscora() {
		return escora;
	}

	public void setEscora(Double escora) {
		this.escora = escora;
	}

	public Integer getTipoEspecieID() {
		return tipoEspecieID;
	}

	public void setTipoEspecieID(Integer tipoEspecieID) {
		this.tipoEspecieID = tipoEspecieID;
	}

	public Double getTalhao2() {
		return talhao2;
	}

	public void setTalhao2(Double talhao2) {
		this.talhao2 = talhao2;
	}

	public Double getCoordenadaX() {
		return coordenadaX;
	}

	public void setCoordenadaX(Double coordenadaX) {
		this.coordenadaX = coordenadaX;
	}

	public Double getCoordenadaY() {
		return coordenadaY;
	}

	public void setCoordenadaY(Double coordenadaY) {
		this.coordenadaY = coordenadaY;
	}

	public Double getAreaEfetivoManejo() {
		return areaEfetivoManejo;
	}

	public void setAreaEfetivoManejo(Double areaEfetivoManejo) {
		this.areaEfetivoManejo = areaEfetivoManejo;
	}

	public String getDescricaoAcesso() {
		return descricaoAcesso;
	}

	public void setDescricaoAcesso(String descricaoAcesso) {
		this.descricaoAcesso = descricaoAcesso;
	}

	public String getNumeroAutorizacaoRepflor() {
		return numeroAutorizacaoRepflor;
	}

	public void setNumeroAutorizacaoRepflor(String numeroAutorizacaoRepflor) {
		this.numeroAutorizacaoRepflor = numeroAutorizacaoRepflor;
	}

	public String getSpuOriginouAETID() {
		return spuOriginouAETID;
	}

	public void setSpuOriginouAETID(String spuOriginouAETID) {
		this.spuOriginouAETID = spuOriginouAETID;
	}

	public Integer getZonaID() {
		return zonaID;
	}

	public void setZonaID(Integer zonaID) {
		this.zonaID = zonaID;
	}

	public Integer getEmpreendimentoNatuurID() {
		return empreendimentoNatuurID;
	}

	public void setEmpreendimentoNatuurID(Integer empreendimentoNatuurID) {
		this.empreendimentoNatuurID = empreendimentoNatuurID;
	}

	public String getUnidadeMedidadeArea() {
		return unidadeMedidadeArea;
	}

	public void setUnidadeMedidadeArea(String unidadeMedidadeArea) {
		this.unidadeMedidadeArea = unidadeMedidadeArea;
	}

	public String getUsuarioCoordId() {
		return usuarioCoordId;
	}

	public void setUsuarioCoordId(String usuarioCoordId) {
		this.usuarioCoordId = usuarioCoordId;
	}

	public String getAreaNucleoId() {
		return areaNucleoId;
	}

	public void setAreaNucleoId(String areaNucleoId) {
		this.areaNucleoId = areaNucleoId;
	}

	public String getEstaca() {
		return estaca;
	}

	public void setEstaca(String estaca) {
		this.estaca = estaca;
	}

	public String getTora() {
		return tora;
	}

	public void setTora(String tora) {
		this.tora = tora;
	}

}
