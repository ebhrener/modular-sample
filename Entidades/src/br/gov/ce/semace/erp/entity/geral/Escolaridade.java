package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "escolaridade", schema="geral")
public class Escolaridade implements Serializable{
	
	
	private static final long serialVersionUID = -1541071908588898429L;

//=========================================Atributos=========================================
	
	@Id
	@SequenceGenerator(sequenceName ="geral.seq_escolaridade", name = "SEQESCOL", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQESCOL")
	private Long id;
	
	private String descricao;


	
	//=========================================Getters and setters=========================================
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	
	
	

}
