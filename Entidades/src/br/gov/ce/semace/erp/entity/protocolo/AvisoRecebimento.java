package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.TipoDocumento;
import br.gov.ce.semace.erp.enuns.TipoRecebimento;
import br.gov.ce.semace.erp.utils.DateUtils;

@Entity
@Audited
@Table(name = "aviso_recebimento", schema = "protocolo")
public class AvisoRecebimento implements Serializable {

	private static final long serialVersionUID = 7070237707161164303L;

	@Id
	@SequenceGenerator(sequenceName = "protocolo.seq_aviso_recebimento", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documento_id")
	private Documento documento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documento_origem_id")
	private Documento documentoOrigem;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_recebimento_ar")
	private Date dataRecebimentoAr;

	@Temporal(TemporalType.DATE)
	private Date data;

	@Temporal(TemporalType.TIME)
	private Date hora;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;

	@Column(nullable=false)
	private String rastreamento;

	@Column(name = "tipo_recebimento")
	private TipoRecebimento tipoRecebimento;

	@Column(name = "numero_ar", unique = true)
	private String numeroAR;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "avisoRecebimento")
	private List<ArquivoAvisoRecebimento> arquivos = new ArrayList<>();

	private Boolean retornou;

	@Transient
	private TipoDocumento tipoDocumento;

	/**
	 * Propriedade {@link Transient}e apenas para apresentação na tela do arquivo para download.
	 *
	 * TODO: Deve ser removida a lista de arquivos para deixar apenas uma propriedade, pois
	 * não só é possível adicionar 1 unico arquivo.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 16, 2013 11:46:44 AM
	 */
	@Transient
	private ArquivoAvisoRecebimento arquivoAvisoRecebimento;

	/**
	 * Fornece os campos {@link #data} e {@link #hora} formatados.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 06/08/2013 11:09:49
	 *
	 * @return retorna uma {@link String} com de acordo com o padrão {@link DateUtils#BRAZIL_FORMAT} para a {@link #data} e
	 * {@link DateUtils#BRAZIL_SHORT_TIME_FORMAT} para a {@link #hora}.
	 */
	@Transient
	public String getDataHoraFormatada(){
		// formata a data
		String dataFormatada = DateUtils.toString(this.data);
		// formata o tempo
		String horaFormatada = DateUtils.getShortTime(this.hora);

		// concatena e separa por espaços a string a ser retornada
		return dataFormatada + " às " + horaFormatada;
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoDocumento} é
	 * 	do tipo <b>OFICIO</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - May 22, 2014 10:45:58 AM
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoDocumento} seja
	 * 	<b>OFICIO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoDocumento#isTipoOficio()
	 */
	@Transient
	public boolean isTipoOficio(){
		return this.tipoDocumento.isTipoOficio();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoDocumento} é
	 * 	do tipo <b>PROCESSO</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - May 22, 2014 10:46:04 AM
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoDocumento} seja
	 * 	<b>PROCESSO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoDocumento#isTipoProcesso()
	 */
	@Transient
	public boolean isTipoProcesso(){
		return this.tipoDocumento.isTipoProcesso();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoRecebimento} é
	 * 	do tipo <b> {@link TipoRecebimento#RECEBIDO} </b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - May 22, 2014 10:46:10 AM
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoRecebimento} seja
	 * 	<b> {@link TipoRecebimento#RECEBIDO} </b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoRecebimento#isRecebido()
	 */
	@Transient
	public boolean isRecebido() {
		if (this.tipoRecebimento != null && this.tipoRecebimento.isRecebido()){
			return true;
		}
		return false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataRecebimentoAr() {
		return dataRecebimentoAr;
	}

	public void setDataRecebimentoAr(Date dataRecebimentoAr) {
		this.dataRecebimentoAr = dataRecebimentoAr;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public String getRastreamento() {
		return rastreamento;
	}

	public void setRastreamento(String rastreamento) {
		this.rastreamento = rastreamento;
	}

	public List<ArquivoAvisoRecebimento> getArquivos() {
		return arquivos;
	}

	public void setArquivos(List<ArquivoAvisoRecebimento> arquivos) {
		this.arquivos = arquivos;
	}

	public TipoRecebimento getTipoRecebimento() {
		return tipoRecebimento;
	}

	public void setTipoRecebimento(TipoRecebimento tipoRecebimento) {
		this.tipoRecebimento = tipoRecebimento;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroAR() {
		return numeroAR;
	}

	public void setNumeroAR(String numeroAR) {
		this.numeroAR = numeroAR;
	}

	public Boolean getRetornou() {
		return retornou;
	}

	public void setRetornou(Boolean retornou) {
		this.retornou = retornou;
	}

	/**
	 * @return the arquivoAvisoRecebimento
	 */
	public ArquivoAvisoRecebimento getArquivoAvisoRecebimento() {

		if (this.getArquivos() !=null && !this.getArquivos().isEmpty()){
			this.arquivoAvisoRecebimento = this.getArquivos().iterator().next();
		}

		return this.arquivoAvisoRecebimento;
	}

	/**
	 * @param arquivoAvisoRecebimento the arquivoAvisoRecebimento to set
	 */
	public void setArquivoAvisoRecebimento(
			ArquivoAvisoRecebimento arquivoAvisoRecebimento) {
		this.arquivoAvisoRecebimento = arquivoAvisoRecebimento;
	}

	/**
	 * @return the documento
	 */
	public Documento getDocumento() {
		return documento;
	}


	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}


	/**
	 * @return the documentoOrigem
	 */
	public Documento getDocumentoOrigem() {
		return documentoOrigem;
	}


	/**
	 * @param documentoOrigem the documentoOrigem to set
	 */
	public void setDocumentoOrigem(Documento documentoOrigem) {
		this.documentoOrigem = documentoOrigem;
	}

}
