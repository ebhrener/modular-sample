package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.Date;

public class InteressadoLicencaSigaTO implements Serializable{

	private static final long serialVersionUID = -865800856827472569L;

	private String nomeInteressado;
	private String documentoInteressado;
	private String descricaoEmpreendimento;
	private String spuLicenca;
	private String descricaoProcesso;
	private String descricaoCompletaAtividade;
	private Date dataCadastroProcesso;

	public String getNomeInteressado() {
		return nomeInteressado;
	}

	public void setNomeInteressado(String nomeInteressado) {
		this.nomeInteressado = nomeInteressado;
	}

	public String getDocumentoInteressado() {
		return documentoInteressado;
	}

	public void setDocumentoInteressado(String documentoInteressado) {
		this.documentoInteressado = documentoInteressado;
	}

	public String getDescricaoEmpreendimento() {
		return descricaoEmpreendimento;
	}

	public void setDescricaoEmpreendimento(String descricaoEmpreendimento) {
		this.descricaoEmpreendimento = descricaoEmpreendimento;
	}

	public String getSpuLicenca() {
		return spuLicenca;
	}

	public void setSpuLicenca(String spuLicenca) {
		this.spuLicenca = spuLicenca;
	}

	public String getDescricaoProcesso() {
		return descricaoProcesso;
	}

	public void setDescricaoProcesso(String descricaoProcesso) {
		this.descricaoProcesso = descricaoProcesso;
	}

	public String getDescricaoCompletaAtividade() {
		return descricaoCompletaAtividade;
	}

	public void setDescricaoCompletaAtividade(String descricaoCompletaAtividade) {
		this.descricaoCompletaAtividade = descricaoCompletaAtividade;
	}

	public Date getDataCadastroProcesso() {
		return dataCadastroProcesso;
	}

	public void setDataCadastroProcesso(Date dataCadastroProcesso) {
		this.dataCadastroProcesso = dataCadastroProcesso;
	}

}
