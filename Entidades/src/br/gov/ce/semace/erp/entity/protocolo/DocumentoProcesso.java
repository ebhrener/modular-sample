	package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;

@Entity
@Audited
@Table(schema = "protocolo", name = "documento_processo")
public class DocumentoProcesso implements Serializable{

	private static final long serialVersionUID = 8947939497169163182L;

	@Id
	@SequenceGenerator(sequenceName = "protocolo.seq_documento_processo", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "documento_id")
	private Documento documento = new Documento();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "processo_id")
	private Processo processo = new Processo();

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_vinculacao")
	private Date dataHoraVinculacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public Date getDataHoraVinculacao() {
		return dataHoraVinculacao;
	}

	public void setDataHoraVinculacao(Date dataHoraVinculacao) {
		this.dataHoraVinculacao = dataHoraVinculacao;
	}
}
