package br.gov.ce.semace.erp.entity.atendimento.to;

import java.io.Serializable;
import java.util.Date;

public class FuncionarioPendenciaLiberadaTO implements Serializable{

	private static final long serialVersionUID = -3724509070511826785L;

	private String nome;
	private Date dataAtualizacao;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

}
