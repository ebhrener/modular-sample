package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.math.BigInteger;

public class MunicipioSiga implements Serializable {

	private static final long serialVersionUID = -2696752559257699793L;

	private String ufId;
	private String municipioId;
	private String municipio;
	private Boolean excluido;
	private Integer regiaoId;
	private Integer regiaoPef;
	private Boolean conveniado;
	private Integer distancia;
	private String areaId;
	private Integer id;
	private Boolean regiaoMetropolitana = Boolean.FALSE;
	private BigInteger codigoIbge;
	private String municipioNovoId;

	public MunicipioSiga() {
		super();
	}

	public MunicipioSiga(String ufId, String municipioId, String municipio, Boolean excluido, Integer regiaoId, Integer regiaoPef, Boolean conveniado,
			Integer distancia, String areaId, Integer id, Boolean regiaoMetropolitana, BigInteger codigoIbge, String municipioNovoId) {
		this.ufId = ufId;
		this.municipioId = municipioId;
		this.municipio = municipio;
		this.excluido = excluido;
		this.regiaoId = regiaoId;
		this.regiaoPef = regiaoPef;
		this.conveniado = conveniado;
		this.distancia = distancia;
		this.areaId = areaId;
		this.id = id;
		this.regiaoMetropolitana = regiaoMetropolitana;
		this.codigoIbge = codigoIbge;
		this.municipioNovoId = municipioNovoId;
	}

	public String getUfId() {
		return ufId;
	}

	public void setUfId(String ufId) {
		this.ufId = ufId;
	}

	public String getMunicipioId() {
		return municipioId;
	}

	public void setMunicipioId(String municipioId) {
		this.municipioId = municipioId;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public Boolean getExcluido() {
		return excluido;
	}

	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}

	public Integer getRegiaoId() {
		return regiaoId;
	}

	public void setRegiaoId(Integer regiaoId) {
		this.regiaoId = regiaoId;
	}

	public Integer getRegiaoPef() {
		return regiaoPef;
	}

	public void setRegiaoPef(Integer regiaoPef) {
		this.regiaoPef = regiaoPef;
	}

	public Boolean getConveniado() {
		return conveniado;
	}

	public void setConveniado(Boolean conveniado) {
		this.conveniado = conveniado;
	}

	public Integer getDistancia() {
		return distancia;
	}

	public void setDistancia(Integer distancia) {
		this.distancia = distancia;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getRegiaoMetropolitana() {
		return regiaoMetropolitana;
	}

	public void setRegiaoMetropolitana(Boolean regiaoMetropolitana) {
		this.regiaoMetropolitana = regiaoMetropolitana;
	}

	public BigInteger getCodigoIbge() {
		return codigoIbge;
	}

	public void setCodigoIbge(BigInteger codigoIbge) {
		this.codigoIbge = codigoIbge;
	}

	public String getMunicipioNovoId() {
		return municipioNovoId;
	}

	public void setMunicipioNovoId(String municipioNovoId) {
		this.municipioNovoId = municipioNovoId;
	}
}