package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;

/**
 *
 * @author Elizandra Amarante [elizandra.ferreira@gmail.com] - 11/05/2016
 *
 */
@Entity
@Audited
@Table(schema="geral", name="regiao_metropolitana")
public class RegiaoMetropolitana implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 6292178804704575653L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_regiao_metropolitana", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@Column(nullable = false)
	private String descricao;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE, mappedBy = "regiaoMetropolitana")
	@JsonBackReference
	private List<Cidade> cidades = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}
}
