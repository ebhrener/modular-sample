package br.gov.ce.semace.erp.entity.transporte;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Habilitacao;
import br.gov.ce.semace.erp.enuns.StatusVeiculo;
/**
 *
 * @author Tiago Nascimento - vilard@gmail.com - 14/05/2014 - 13:28:45
 *
 */
@Entity
@Audited
@Table(name="veiculo", schema="transporte")
public class Veiculo implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -7103083644930083172L;

	@Id
	@SequenceGenerator(sequenceName="transporte.seq_veiculo", name="SEQ_VEICULO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ_VEICULO")
	private Long id;

	@Column(unique = true)
	private String identificador;

	private String modelo;

	@Column(unique = true)
	private String tombo;


	@Column(name="exige_motorista")
	private boolean exigeMotorista;

	@Column(name="status_veiculo")
	private StatusVeiculo statusVeiculo;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="especie_transporte_id")
	private EspecieTransporte especieTransporte = new EspecieTransporte();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@NotFound(action=NotFoundAction.IGNORE)
	@JoinColumn(name="habilitacao_id", nullable=true)
	private Habilitacao habilitacao = new Habilitacao();

	@Column(nullable = false)
	private boolean ativo = true;

	@Transient
	public String getDetalheAtivo(){

		if (this.ativo){
			return "Sim";
		}

		return "Não";
	}

	@Transient
	public String getDetalheExigeMotorista(){

		if (this.exigeMotorista){
			return "Sim";
		}

		return "Não";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Veiculo other = (Veiculo) obj;
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getTombo() {
		return tombo;
	}

	public void setTombo(String tombo) {
		this.tombo = tombo;
	}

	public boolean isExigeMotorista() {
		return exigeMotorista;
	}

	public void setExigeMotorista(boolean exigeMotorista) {
		this.exigeMotorista = exigeMotorista;
	}

	public StatusVeiculo getStatusVeiculo() {
		return statusVeiculo;
	}

	public void setStatusVeiculo(StatusVeiculo statusVeiculo) {
		this.statusVeiculo = statusVeiculo;
	}

	public EspecieTransporte getEspecieTransporte() {
		return especieTransporte;
	}

	public void setEspecieTransporte(EspecieTransporte especieTransporte) {
		this.especieTransporte = especieTransporte;
	}

	public Habilitacao getHabilitacao() {
		return habilitacao;
	}

	public void setHabilitacao(Habilitacao habilitacao) {
		this.habilitacao = habilitacao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
