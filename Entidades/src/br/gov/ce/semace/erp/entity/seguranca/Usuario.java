package br.gov.ce.semace.erp.entity.seguranca;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.permissao.Role;

@Entity
@Audited
@Table(name = "usuario", schema = "seguranca")
public class Usuario implements IUsuario {

	private static final long serialVersionUID = 5329378700973426382L;

	@Id
	@SequenceGenerator(sequenceName = "seguranca.seq_usuario", name = "SEQUSER", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUSER")
	private Long id;

	@Column(unique = true, nullable = false)
	private String login;

	@Column(nullable = false)
	private String senha;

	private String assinatura;

	@Column(name="is_super_usuario")
	private Boolean isSuperUsuario;

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "usuario_perfil", schema = "seguranca",
		joinColumns = @JoinColumn(name = "usuario_id"),
		inverseJoinColumns = @JoinColumn(name = "perfil_id"))
	private List<Perfil> perfis = new ArrayList<Perfil>();

	@OneToOne(fetch = FetchType.LAZY, mappedBy="usuario", cascade = CascadeType.MERGE)
	private Funcionario funcionario = new Funcionario();

	@Column(name="alterou_senha")
	private Boolean alterouSenha;

	@Column(name="is_usuario_logado")
	private Boolean isUsuarioLogado;

	@Column(name="aceitou_termos")
	private Boolean aceitouTermos;

	@Temporal(TemporalType.DATE)
	@Column(name="data_alteracao_senha")
	private Date dataAlteracaoSenha;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
	private List<UltimaSenha> ultimasSenha = new ArrayList<UltimaSenha>();

	private String token;

	@Column(name = "validade_acesso_externo")
	@Temporal(TemporalType.DATE)
	private Date validadeAcessoExterno;

	@Transient
	private Perfil perfilBusca = new Perfil();

	@Transient
	private String usuarioIdSiga;

	@Transient
	private boolean filtroTemAcessoExterno = false;

	@Transient
	private String tipo = "INTERNO";

	@Transient
	private Long pessoaId;

	/**
	 * 	Método {@link Transient} que retorna uma {@link String} com o nome do funcionário com a
	 * mátricula para ser utilizado em mensagens de erro e informação
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 04/11/2014 11:54:09
	 *
	 * @return
	 * 		Retorna a {@link String} com nome do {@link Funcionario} e matricula
	 */
	@Transient
	public String getNomeMatriculaUsuario(){
		return this.funcionario.getNome() + " - " + this.funcionario.getMatricula();
	}

	/**
	 * Método {@link Transient} que retorna uma {@link String} informando se o {@link Funcionario} está ativo ou não.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 04/11/2014 11:55:36
	 *
	 * @return
	 * 		Retorna a {@link String} Sim, se o {@link Funcionario} estiver Ativo. Caso contrário, Não
	 */
	@Transient
	public String getDetalheAtivo(){

		if(this.funcionario.getAtivo() == null || !this.funcionario.getAtivo()){
			return "Não";
		}

		return "Sim";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((funcionario == null) ? 0 : funcionario.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Usuario other = (Usuario) obj;
		if (funcionario == null) {
			if (other.funcionario != null) {
				return false;
			}
		} else if (!funcionario.equals(other.funcionario)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public Boolean hasPermissao(Role role) {
		for (Perfil perfil : this.perfis) {
			if(perfil.getPermissoes().contains(role.name()) || this.isSuperUsuario()){
				return true;
			}
		}
		return false;
	}

	/**
	 * O retorno desse método é utilizado para identificar o tipo de usuário na engine de auditoria
	 * @return
	 */
	@Override
	public String toString() {
		return tipo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Boolean isSuperUsuario() {
		return isSuperUsuario;
	}

	public void setSuperUsuario(Boolean isSuperUsuario) {
		this.isSuperUsuario = isSuperUsuario;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Boolean getAlterouSenha() {
		return alterouSenha;
	}

	public void setAlterouSenha(Boolean alterouSenha) {
		this.alterouSenha = alterouSenha;
	}

	public Boolean getIsUsuarioLogado() {
		return isUsuarioLogado;
	}

	public void setIsUsuarioLogado(Boolean isUsuarioLogado) {
		this.isUsuarioLogado = isUsuarioLogado;
	}
	public Boolean getAceitouTermos() {
		return aceitouTermos;
	}

	public void setAceitouTermos(Boolean aceitouTermos) {
		this.aceitouTermos = aceitouTermos;
	}


	public Date getDataAlteracaoSenha() {
		return dataAlteracaoSenha;
	}

	public void setDataAlteracaoSenha(Date dataAlteracaoSenha) {
		this.dataAlteracaoSenha = dataAlteracaoSenha;
	}

	public List<UltimaSenha> getUltimasSenha() {
		return ultimasSenha;
	}

	public void setUltimasSenha(List<UltimaSenha> ultimasSenha) {
		this.ultimasSenha = ultimasSenha;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAssinatura() {
		return assinatura;
	}

	public void setAssinatura(String assinatura) {
		this.assinatura = assinatura;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	public Perfil getPerfilBusca() {
		return perfilBusca;
	}

	public void setPerfilBusca(Perfil perfilBusca) {
		this.perfilBusca = perfilBusca;
	}

	public String getUsuarioIdSiga() {
		return usuarioIdSiga;
	}

	public void setUsuarioIdSiga(String usuarioIdSiga) {
		this.usuarioIdSiga = usuarioIdSiga;
	}

		public Date getValidadeAcessoExterno() {
		return validadeAcessoExterno;
	}

	public void setValidadeAcessoExterno(Date validadeAcessoExterno) {
		this.validadeAcessoExterno = validadeAcessoExterno;
	}

	public boolean isFiltroTemAcessoExterno() {
		return filtroTemAcessoExterno;
	}

	public void setFiltroTemAcessoExterno(boolean filtroTemAcessoExterno) {
		this.filtroTemAcessoExterno = filtroTemAcessoExterno;
	}

	@Transient
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Long getPessoaId() {
		return pessoaId;
	}

	public void setPessoaId(Long pessoaId) {
		this.pessoaId = pessoaId;
	}
}