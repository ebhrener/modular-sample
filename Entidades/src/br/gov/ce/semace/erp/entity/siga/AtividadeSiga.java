package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "atividade", schema = "adm_siga")
public class AtividadeSiga implements Serializable{

	private static final long serialVersionUID = -3282305303150075266L;

	@Id
	@GeneratedValue
	private int id;

	@Column(name="atividade_id")
	private String atividadeId;

	@Column(name="grupo_atividade_idantiga")
	private String grupoAtividadeAntigo;

	@Column(name="atividade")
	private String atividade;

	@Column(name="atividade_id2")
	private String atividadeId2;

	@Column(name = "excluido")
	private boolean excluido;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAtividadeId() {
		return atividadeId;
	}

	public void setAtividadeId(String atividadeId) {
		this.atividadeId = atividadeId;
	}

	public String getGrupoAtividadeAntigo() {
		return grupoAtividadeAntigo;
	}

	public void setGrupoAtividadeAntigo(String grupoAtividadeAntigo) {
		this.grupoAtividadeAntigo = grupoAtividadeAntigo;
	}

	public String getAtividade() {
		return atividade;
	}

	public void setAtividade(String atividade) {
		this.atividade = atividade;
	}

	public boolean isExcluido() {
		return excluido;
	}

	public void setExcluido(boolean excluido) {
		this.excluido = excluido;
	}

	public String getAtividadeId2() {
		return atividadeId2;
	}

	public void setAtividadeId2(String atividadeId2) {
		this.atividadeId2 = atividadeId2;
	}



}
