package br.gov.ce.semace.erp.entity.produtividade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Setor;

@Entity
@Audited
@Table(name = "atividade_produtividade", schema = "produtividade")
public class AtividadeProdutividade implements Serializable{

	private static final long serialVersionUID = 2805533069206586743L;
	
	@Id @SequenceGenerator(sequenceName="produtividade.seq_atividade_produtividade", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	private String titulo;
	
	@Column(name = "nome_documento")
	private String nomeDocumento;
	
	@Column(name = "permite_mais_resposta")
	private Boolean permiteMaisResposta;
	
	@Column(name = "validar_spu_resposta")
	private Boolean validarSpuResposta;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "atividadeProdutividade", orphanRemoval = true)
	private List<PesoSetor> listPesoSetor = new ArrayList<PesoSetor>();
	
	@Transient
	private Boolean possuiTipoResposta;
	
	@Transient
	private Boolean possuiAtividadeCoema;
	
	@Transient
	private Setor setorPeso = new Setor();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getNomeDocumento() {
		return nomeDocumento;
	}

	public void setNomeDocumento(String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}

	public Boolean getPossuiTipoResposta() {
		return possuiTipoResposta;
	}

	public void setPossuiTipoResposta(Boolean possuiTipoResposta) {
		this.possuiTipoResposta = possuiTipoResposta;
	}

	public List<PesoSetor> getListPesoSetor() {
		return listPesoSetor;
	}

	public void setListPesoSetor(List<PesoSetor> listPesoSetor) {
		this.listPesoSetor = listPesoSetor;
	}

	public Boolean getPermiteMaisResposta() {
		return permiteMaisResposta;
	}

	public void setPermiteMaisResposta(Boolean permiteMaisResposta) {
		this.permiteMaisResposta = permiteMaisResposta;
	}

	public Boolean getValidarSpuResposta() {
		return validarSpuResposta;
	}

	public void setValidarSpuResposta(Boolean validarSpuResposta) {
		this.validarSpuResposta = validarSpuResposta;
	}

	public Boolean getPossuiAtividadeCoema() {
		return possuiAtividadeCoema;
	}

	public void setPossuiAtividadeCoema(Boolean possuiAtividadeCoema) {
		this.possuiAtividadeCoema = possuiAtividadeCoema;
	}

	public Setor getSetorPeso() {
		return setorPeso;
	}

	public void setSetorPeso(Setor setorPeso) {
		this.setorPeso = setorPeso;
	}
}
