package br.gov.ce.semace.erp.entity.julgamento;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.fiscalizacao.AutoInfracao;
import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.protocolo.Processo;
import br.gov.ce.semace.erp.enuns.StatusJulgamento;
import br.gov.ce.semace.erp.enuns.StatusProcesso;

@Entity
@Audited
@Table(name = "julgamento", schema="julgamento")
public class Julgamento implements Serializable {

	private static final long serialVersionUID = -1921186214627076628L;
	
	@Id
	@SequenceGenerator(sequenceName ="julgamento.seq_julgamento", name = "SEQJULGAMENTO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQJULGAMENTO")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "processo_id")
	private Processo processo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "auto_infracao_id")
	private AutoInfracao autoInfracao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documento_id")
	private Documento documento;
	
	@OneToMany(mappedBy="julgamento", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ComunicacaoJulgamento> listComunicacao;
	
	@Column(name = "status_julgamento")
	private StatusJulgamento statusJulgamento;
	
	@Column(name = "auto_entregue_campo")
	private Boolean autoEntregueCampo = false;
	
	@Column(name="prazo_defesa_encerrado")
	private Boolean prazoDefesaEncerrado = false;
	
	@Column(name="prazo_alegacao_encerrado")
	private Boolean prazoAlegacaoEncerrado = false;
	
	@Column(name="prazo_recurso_encerrado")
	private Boolean prazoRecursoEncerrado = false;
	
	@Column(name="prazo_retratacao_encerrado")
	private Boolean prazoRetratacaoEncerrado = false;
	
	/**
	 * Propriedade que define se alguma defesa foi entregue até o encerramento do prazo de entrega do defesa.
	 */
	@Column(name="defesa_entregue_prazo")
	private Boolean defesaEntreguePrazo;
	
	/**
	 * Propriedade que define se algum recurso foi entregue até o encerramento do prazo de entrega do recurso.
	 */
	@Column(name="recurso_entregue_prazo")
	private Boolean recursoEntreguePrazo;
	
	/**
	 * Propriedade que define se algum documento de alegação foi entregue até o encerramento do prazo de alegações finais.
	 */
	@Column(name="alegacao_entregue_prazo")
	private Boolean alegacaoEntreguePrazo;
	
	@Column(name="qtd_envios_defesa")
	private Integer qtdEnviosDefesa;
	
	@Column(name="qtd_envios_instrucao")
	private Integer qtdEnviosInstrucao;
	
	@Column(name="qtd_envios_decisao")
	private Integer qtdEnviosDecisao;
	
	@Column(name="qtd_envios_retracao")
	private Integer qtdEnviosRetratacao;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_final_alegacao")
	private Date dataFinalAlegacao;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicial_alegacao")
	private Date dataInicialAlegacao;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_final_defesa")
	private Date dataFinalDefesa;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicial_defesa")
	private Date dataInicialDefesa;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_final_recurso")
	private Date dataFinalRecurso;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicial_recurso")
	private Date dataInicialRecurso;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_final_retratacao")
	private Date dataFinalRetratacao;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicial_retratacao")
	private Date dataInicialRetratacao;

	/**
	 * Informa se o {@link Julgamento} está ativo ou não.
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 23, 2013 1:43:10 PM
	 */
	private Boolean ativo = true;
	
	public Long getId() {
		return this.id;
	}
	
	/////////////////TRANSIENTE/////////////////////////////

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_ENVIO_AIF</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:36:00 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_ENVIO_AIF</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoEnvioAIF()
	 */
	@Transient
	public boolean isAguardandoEnvioAIF(){
		return this.statusJulgamento.isAguardandoEnvioAIF();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_AR_AIF</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:35:42 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_AR_AIF</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoArAifDefesa()
	 */
	@Transient
	public boolean isAguardandoArAifDefesa(){
		return this.statusJulgamento.isAguardandoArAifDefesa();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_DEFESA</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:35:23 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_DEFESA</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoApresentacaoDefesa()
	 */
	@Transient
    public boolean isAguardandoApresentacaoDefesa() {
		return this.statusJulgamento.isAguardandoApresentacaoDefesa();
    }
 
	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_DISTRIBUICAO_PARECER_INSTRUTORIO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:34:47 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_DISTRIBUICAO_PARECER_INSTRUTORIO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoDistribuicaoParecerInstrutorio()
	 */
	@Transient
    public boolean isAguardandoDistribuicaoParecerInstrutorio() {
		return this.statusJulgamento.isAguardandoDistribuicaoParecerInstrutorio();    
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_PARECER_INSTRUTORIO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:34:23 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_PARECER_INSTRUTORIO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoElaboracaoParecerInstrutorio()
	 */
	@Transient
    public boolean isAguardandoElaboracaoParecerInstrutorio() {
		return this.statusJulgamento.isAguardandoElaboracaoParecerInstrutorio();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>ELABORANDO_PARECER_INSTRUTORIO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:33:54 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>ELABORANDO_PARECER_INSTRUTORIO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isElaborandoParecerInstrutorio()
	 */
	@Transient
    public boolean isElaborandoParecerInstrutorio() {
		return this.statusJulgamento.isElaborandoParecerInstrutorio();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_COMUNICACAO__ALEGACAO</b>
	 * 
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 13/05/2013 14:48:37
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_COMUNICACAO__ALEGACAO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoComunicacaoAlegacao()
	 */
	@Transient
    public boolean isAguardandoComunicacaoAlegacao() {
		return this.statusJulgamento.isAguardandoComunicacaoAlegacao();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_AR_ALEGACAO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:33:10 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_AR_ALEGACAO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoArAlegacao()
	 */
	@Transient
    public boolean isAguardandoArAlegacao() {
		return this.statusJulgamento.isAguardandoArAlegacao();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>PRAZO_ALEGACAO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:32:46 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>PRAZO_ALEGACAO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isPeriodoAlegacao()
	 */
	@Transient
    public boolean isPrazoAlegacao() {
		return this.statusJulgamento.isPeriodoAlegacao();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_DISTRIBUICAO_DECISAO_1_GRAU</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:32:27 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_DISTRIBUICAO_DECISAO_1_GRAU</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoDistribuicaoDecisao()
	 */
	@Transient
    public boolean isAguardandoDistribuicaoDecisao() {
		return this.statusJulgamento.isAguardandoDistribuicaoDecisao();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_DECISAO_1_GRAU</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:32:01 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_DECISAO_1_GRAU</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoDecisao1Grau()
	 */
	@Transient
    public boolean isAguardandoDecisao1Grau() {
		return this.statusJulgamento.isAguardandoDecisao1Grau();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>ELABORANDO_DECISAO_1_GRAU</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:31:37 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>ELABORANDO_DECISAO_1_GRAU</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isElaborandoDecisao1Grau()
	 */
	@Transient
    public boolean isElaborandoDecisao1Grau() {
		return this.statusJulgamento.isElaborandoDecisao1Grau();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>DECISAO_1_GRAU_CONCLUIDA</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:31:20 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>DECISAO_1_GRAU_CONCLUIDA</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isDecisao1GrauConcluida()
	 */
	@Transient
    public boolean isDecisao1GrauConcluida() {
		return this.statusJulgamento.isDecisao1GrauConcluida();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_AR_DECISAO_1_GRAU</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:30:46 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_AR_DECISAO_1_GRAU</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoArDecisao1Grau()
	 */
	@Transient
    public boolean isAguardandoArDecisao1Grau() {
		return this.statusJulgamento.isAguardandoArDecisao1Grau();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>JULGADO_E_TRANSITADO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:30:27 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>JULGADO_E_TRANSITADO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isJulgadoETransitado()
	 */
	@Transient
    public boolean isJulgadoETransitado() {
		return this.statusJulgamento.isJulgadoETransitado();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>SUSPENSO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:30:09 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>SUSPENSO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isSuspenso()
	 */
	@Transient
    public boolean isSuspenso() {
		return this.statusJulgamento.isSuspenso();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_RECURSO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 3, 2013 10:29:47 AM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_RECURSO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoRecurso()
	 */
	@Transient
    public boolean isAguardandoRecurso() {
		return this.statusJulgamento.isAguardandoRecurso();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_AIF_ENTREGUE_PESSOALMENTE</b>
	 * 
	 * @author Saulo Fernandes
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_AIF_ENTREGUE_PESSOALMENTE</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoAifEntreguePessoalmente()
	 */
	@Transient
    public boolean isAguardandoAifEntreguePessoalmente() {
		return this.statusJulgamento.isAguardandoAifEntreguePessoalmente();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_PARECER_INSTRUTORIO_ENTREGUE_PESSOALMENTE</b>
	 * 
	 * @author Saulo Fernandes
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_PARECER_INSTRUTORIO_ENTREGUE_PESSOALMENTE</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoParecerInstrutorioEntreguePessoalmente()
	 */
	@Transient
    public boolean isAguardandoParecerInstrutorioEntreguePessoalmente() {
		return this.statusJulgamento.isAguardandoParecerInstrutorioEntreguePessoalmente();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_DECISAO_ENTREGUE_PESSOALMENTE</b>
	 * 
	 * @author Saulo Fernandes
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_DECISAO_ENTREGUE_PESSOALMENTE</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoDecisaoEntreguePessoalmente()
	 */
	@Transient
    public boolean isAguardandoDecisaoEntreguePessoalmente() {
		return this.statusJulgamento.isAguardandoDecisaoEntreguePessoalmente();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_AIF_PUBLICADO_EDITAL</b>
	 * 
	 * @author Saulo Fernandes
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_AIF_PUBLICADO_EDITAL</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoAifPublicadoEdital()
	 */
	@Transient
    public boolean isAguardandoAifPublicadoEdital() {
		return this.statusJulgamento.isAguardandoAifPublicadoEdital();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_PARECER_INSTRUTORIO_PUBLICADO_EDITAL</b>
	 * 
	 * @author Saulo Fernandes
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_PARECER_INSTRUTORIO_PUBLICADO_EDITAL</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoParecerInstrutorioPublicadoEdital()
	 */
	@Transient
    public boolean isAguardandoParecerInstrutorioPublicadoEdital() {
		return this.statusJulgamento.isAguardandoParecerInstrutorioPublicadoEdital();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_DECISAO_PUBLICADA_EDITAL</b>
	 * 
	 * @author Saulo Fernandes
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_DECISAO_PUBLICADA_EDITAL</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoDecisaoPublicadaEdital()
	 */
	@Transient
    public boolean isAguardandoDecisaoPublicadaEdital() {
		return this.statusJulgamento.isAguardandoDecisaoPublicadaEdital();
    }
	
	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>ELABORANDO_RETRACACAO_DECISAO</b>
	 * 
	 * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 10/05/2013 10:03:31
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>ELABORANDO_RETRACACAO_DECISAO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isElaborandoRetratacaoDecisao()
	 */
	@Transient
    public boolean isElaborandoRetratacao() {
		return this.statusJulgamento.isElaborandoRetratacao();
    }
	
	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>RETRACACAO_CONCLUIDA</b>
	 * 
	 * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 16/05/2013 08:40:01
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>RETRACACAO_CONCLUIDA</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isRetratacaoConcluida()
	 */
	@Transient
    public boolean isRetratacaoConcluida() {
		return this.statusJulgamento.isRetratacaoConcluida();
    }
	
	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>PERIODO_RETRACACAO</b>
	 * 
	 * @author Ernesto Janebro [ernesto.camurca@gmail.com] - 10/05/2013 10:07:43
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>PERIODO_RETRACACAO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isPeriodoRetratacao()
	 */
	@Transient
    public boolean isPeriodoRetratacao() {
		return this.statusJulgamento.isPeriodoRetratacao();
    }
	
	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * do tipo <b>AGUARDANDO_AR_RETRATACAO</b>
	 * 
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 15/06/2013 13:12:51
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_AR_RETRATACAO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoArRetratacao()
	 */
	@Transient
    public boolean isAguardandoArRetratacao() {
		return this.statusJulgamento.isAguardandoArRetratacao();
    }
	
	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * do tipo <b>AGUARDANDO_RETRATACAO_ENTREGUE_PESSOALMENTE</b>
	 * 
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 15/06/2013 13:29:54
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_RETRATACAO_ENTREGUE_PESSOALMENTE</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoRetratacaoEntreguePessoalmente()
	 */
	@Transient
    public boolean isAguardandoRetratacaoEntreguePessoalmente() {
		return this.statusJulgamento.isAguardandoRetratacaoEntreguePessoalmente();
    }
	
	/**
	 * Método {@link Transient}e para verificar se o {@link #statusJulgamento} é 
	 * 	do tipo <b>AGUARDANDO_RETRATACAO_PUBLICADA_EDITAL</b>
	 * 
	 * @author Saulo Fernandes
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #statusJulgamento} seja 
	 * 	<b>AGUARDANDO_RETRATACAO_PUBLICADA_EDITAL</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see StatusJulgamento#isAguardandoRetratacaoPublicadaEdital()
	 */
	@Transient
    public boolean isAguardandoRetratacaoPublicadaEdital() {
		return this.statusJulgamento.isAguardandoRetratacaoPublicadaEdital();
    }
	
	@Transient
    public boolean isParecerConcluido() {
		return this.statusJulgamento.isParecerConcluido();
    }
	
	@Transient
	public boolean isProcessoCancelado() {
		return this.getProcesso().getStatusProcesso() == StatusProcesso.CANCELADO;
	}
	
	@Transient
    public boolean isDecisao1Grau() {
		return this.statusJulgamento.isDecisao1Grau();
    }
	

	/////////////////GET E SET /////////////////////////////

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public AutoInfracao getAutoInfracao() {
		return autoInfracao;
	}

	public void setAutoInfracao(AutoInfracao autoInfracao) {
		this.autoInfracao = autoInfracao;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public List<ComunicacaoJulgamento> getListComunicacao() {
		return listComunicacao;
	}

	public void setListComunicacao(List<ComunicacaoJulgamento> listComunicacao) {
		this.listComunicacao = listComunicacao;
	}

	public StatusJulgamento getStatusJulgamento() {
		return statusJulgamento;
	}

	public void setStatusJulgamento(StatusJulgamento statusJulgamento) {
		this.statusJulgamento = statusJulgamento;
	}

	public Boolean getAutoEntregueCampo() {
		return autoEntregueCampo;
	}

	public void setAutoEntregueCampo(Boolean autoEntregueCampo) {
		this.autoEntregueCampo = autoEntregueCampo;
	}

	public Boolean getPrazoDefesaEncerrado() {
		return prazoDefesaEncerrado;
	}

	public void setPrazoDefesaEncerrado(Boolean prazoDefesaEncerrado) {
		this.prazoDefesaEncerrado = prazoDefesaEncerrado;
	}

	public Boolean getPrazoAlegacaoEncerrado() {
		return prazoAlegacaoEncerrado;
	}

	public void setPrazoAlegacaoEncerrado(Boolean prazoAlegacaoEncerrado) {
		this.prazoAlegacaoEncerrado = prazoAlegacaoEncerrado;
	}

	public Boolean getDefesaEntreguePrazo() {
		return defesaEntreguePrazo;
	}

	public void setDefesaEntreguePrazo(Boolean defesaEntreguePrazo) {
		this.defesaEntreguePrazo = defesaEntreguePrazo;
	}

	public Integer getQtdEnviosDefesa() {
		return qtdEnviosDefesa;
	}

	public void setQtdEnviosDefesa(Integer qtdEnviosDefesa) {
		this.qtdEnviosDefesa = qtdEnviosDefesa;
	}

	public Integer getQtdEnviosInstrucao() {
		return qtdEnviosInstrucao;
	}

	public void setQtdEnviosInstrucao(Integer qtdEnviosInstrucao) {
		this.qtdEnviosInstrucao = qtdEnviosInstrucao;
	}

	public Integer getQtdEnviosDecisao() {
		return qtdEnviosDecisao;
	}

	public void setQtdEnviosDecisao(Integer qtdEnviosDecisao) {
		this.qtdEnviosDecisao = qtdEnviosDecisao;
	}

	public Date getDataFinalAlegacao() {
		return dataFinalAlegacao;
	}

	public void setDataFinalAlegacao(Date dataFinalAlegacao) {
		this.dataFinalAlegacao = dataFinalAlegacao;
	}

	public Date getDataInicialAlegacao() {
		return dataInicialAlegacao;
	}

	public void setDataInicialAlegacao(Date dataInicialAlegacao) {
		this.dataInicialAlegacao = dataInicialAlegacao;
	}

	public Date getDataFinalDefesa() {
		return dataFinalDefesa;
	}

	public void setDataFinalDefesa(Date dataFinalDefesa) {
		this.dataFinalDefesa = dataFinalDefesa;
	}

	public Date getDataInicialDefesa() {
		return dataInicialDefesa;
	}

	public void setDataInicialDefesa(Date dataInicialDefesa) {
		this.dataInicialDefesa = dataInicialDefesa;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getPrazoRecursoEncerrado() {
		return prazoRecursoEncerrado;
	}

	public void setPrazoRecursoEncerrado(Boolean prazoRecursoEncerrado) {
		this.prazoRecursoEncerrado = prazoRecursoEncerrado;
	}

	public Date getDataFinalRecurso() {
		return dataFinalRecurso;
	}

	public void setDataFinalRecurso(Date dataFinalRecurso) {
		this.dataFinalRecurso = dataFinalRecurso;
	}

	public Date getDataInicialRecurso() {
		return dataInicialRecurso;
	}

	public void setDataInicialRecurso(Date dataInicialRecurso) {
		this.dataInicialRecurso = dataInicialRecurso;
	}

	public Boolean getRecursoEntreguePrazo() {
		return recursoEntreguePrazo;
	}

	public void setRecursoEntreguePrazo(Boolean recursoEntreguePrazo) {
		this.recursoEntreguePrazo = recursoEntreguePrazo;
	}
	
	public Boolean getPrazoRetratacaoEncerrado() {
		return prazoRetratacaoEncerrado;
	}

	public void setPrazoRetratacaoEncerrado(Boolean prazoRetratacaoEncerrado) {
		this.prazoRetratacaoEncerrado = prazoRetratacaoEncerrado;
	}

	public Date getDataFinalRetratacao() {
		return dataFinalRetratacao;
	}

	public void setDataFinalRetratacao(Date dataFinalRetratacao) {
		this.dataFinalRetratacao = dataFinalRetratacao;
	}

	public Date getDataInicialRetratacao() {
		return dataInicialRetratacao;
	}

	public void setDataInicialRetratacao(Date dataInicialRetratacao) {
		this.dataInicialRetratacao = dataInicialRetratacao;
	}

	public Boolean getAlegacaoEntreguePrazo() {
		return alegacaoEntreguePrazo;
	}

	public void setAlegacaoEntreguePrazo(Boolean alegacaoEntreguePrazo) {
		this.alegacaoEntreguePrazo = alegacaoEntreguePrazo;
	}

	public Integer getQtdEnviosRetratacao() {
		return qtdEnviosRetratacao;
	}

	public void setQtdEnviosRetratacao(Integer qtdEnviosRetratacao) {
		this.qtdEnviosRetratacao = qtdEnviosRetratacao;
	}

}
