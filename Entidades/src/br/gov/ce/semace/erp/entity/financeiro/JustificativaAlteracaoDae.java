package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.seguranca.Usuario;

@Entity
@Audited
@Table(schema = "financeiro", name = "justificativa_alteracao_dae")
public class JustificativaAlteracaoDae implements Serializable {

	private static final long serialVersionUID = 2454239126707781216L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_justificativa_alteracao_dae", name = "SEQ_JUSTIFICATIVA_ALTERACAO_DAE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_JUSTIFICATIVA_ALTERACAO_DAE")
	private Long id;

	@Column(name = "justificativa", columnDefinition = "text")
	private String justificativa;

	@ManyToOne(fetch = FetchType.LAZY)
	private Dae dae;

	@Column(name = "data_deposito")
	private Date dataDeposito;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_logado")
	private Usuario usuarioLogado;

	@Column(name="ativo", nullable = false)
	private boolean ativo = true;

	@Column(name="valor_deposito")
	private Double valorDeposito;

	@Column(name="data_vinculo")
	private Date dataVinculo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public Dae getDae() {
		return dae;
	}

	public void setDae(Dae dae) {
		this.dae = dae;
	}

	public Date getDataDeposito() {
		return dataDeposito;
	}

	public void setDataDeposito(Date dataDeposito) {
		this.dataDeposito = dataDeposito;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Double getValorDeposito() {
		return valorDeposito;
	}

	public void setValorDeposito(Double valorDeposito) {
		this.valorDeposito = valorDeposito;
	}

	public Date getDataVinculo() {
		return dataVinculo;
	}

	public void setDataVinculo(Date dataVinculo) {
		this.dataVinculo = dataVinculo;
	}

}
