package br.gov.ce.semace.erp.entity.atendimento;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.semace.erp.entity.licenciamento.Modalidade;
import br.gov.ce.semace.erp.entity.licenciamento.SubtipoProcesso;
import br.gov.ce.semace.erp.entity.licenciamento.TipoProcesso;

@Entity
@Table(name = "config_req_geracao_taxa_fixa_automatico", schema = "atendimento")
public class ConfigReqGeracaoTaxaFixaAutomatico implements Serializable {

	private static final long serialVersionUID = -1258384440150818164L;

	@Id
	@SequenceGenerator(sequenceName = "atendimento.seq_config_req_geracao_taxa_fixa_automatico", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_processo_id")
	private TipoProcesso tipoProcesso;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sub_tipo_processo_id")
	private SubtipoProcesso subtipoProcesso;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modalidade_id")
	private Modalidade modalidade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "configuracao_atendimento_presencial_id")
	private ConfiguracaoAtendimentoPresencial configuracaoAtendimentoPresencial;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoProcesso getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(TipoProcesso tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public SubtipoProcesso getSubtipoProcesso() {
		return subtipoProcesso;
	}

	public void setSubtipoProcesso(SubtipoProcesso subtipoProcesso) {
		this.subtipoProcesso = subtipoProcesso;
	}

	public Modalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}

	public ConfiguracaoAtendimentoPresencial getConfiguracaoAtendimentoPresencial() {
		return configuracaoAtendimentoPresencial;
	}

	public void setConfiguracaoAtendimentoPresencial(ConfiguracaoAtendimentoPresencial configuracaoAtendimentoPresencial) {
		this.configuracaoAtendimentoPresencial = configuracaoAtendimentoPresencial;
	}


}
