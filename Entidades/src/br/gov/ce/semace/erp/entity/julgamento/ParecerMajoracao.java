package br.gov.ce.semace.erp.entity.julgamento;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="parecer_majoracao", schema="julgamento")
public class ParecerMajoracao implements Serializable{

	private static final long serialVersionUID = 7738874823606765578L;

	@Id
	@SequenceGenerator(sequenceName ="julgamento.seq_parecer_majoracao", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	private BigDecimal valor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="parecer_id")
	private Parecer parecer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="majoracao_id")
	private Majoracao majoracao;

	public ParecerMajoracao clone() throws CloneNotSupportedException {
		ParecerMajoracao parecerMajoracao = new ParecerMajoracao();
		parecerMajoracao.setMajoracao(this.majoracao);
		parecerMajoracao.setValor(this.valor);
		return parecerMajoracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof ParecerMajoracao)) {
			return false;
		}
		if(((ParecerMajoracao)obj).getId() != null){
			return ((ParecerMajoracao)obj).getId().equals(getId());
		}

		ParecerMajoracao other = (ParecerMajoracao) obj;

		return (other.getParecer().getId().equals(this.parecer.getId())
				&& other.getMajoracao().getTipo().equals(this.majoracao.getTipo())
				&& other.getValor().equals(this.valor));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Parecer getParecer() {
		return parecer;
	}

	public void setParecer(Parecer parecer) {
		this.parecer = parecer;
	}

	public Majoracao getMajoracao() {
		return majoracao;
	}

	public void setMajoracao(Majoracao majoracao) {
		this.majoracao = majoracao;
	}
}
