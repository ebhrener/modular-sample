package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoParametroLaboratorial;

@Entity
@Audited
@Table(schema = "laboratorio", name = "analise_laboratorial")
public class AnaliseLaboratorial implements Serializable {

	private static final long serialVersionUID = -1221075827457027275L;

	@Id
	@SequenceGenerator(sequenceName = "laboratorio.seq_analise_laboratorial", name = "SEQ_ANALISE_LABORATORIAL", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ANALISE_LABORATORIAL")
	private Long id;
	
	private String descricao;
	
	private BigDecimal valor;
	
	@Column(name = "tipo_parametro_laboratorial")
	private TipoParametroLaboratorial tipoParametroLaboratorial;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "analise_laboratorial_parametro_laboratorial",
		schema = "laboratorio", 
		joinColumns = @JoinColumn(name = "analise_laboratorial_id"), 
		inverseJoinColumns = @JoinColumn(name = "parametro_laboratorial_id"))
	private List<ParametroLaboratorial> parametrosLaboratoriais = new ArrayList<ParametroLaboratorial>();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="enquadramento_id")
	private Enquadramento enquadramento;
	
	@Column(columnDefinition = "boolean default true")
	private boolean ativo = true;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((parametrosLaboratoriais == null) ? 0
						: parametrosLaboratoriais.hashCode());
		result = prime
				* result
				+ ((tipoParametroLaboratorial == null) ? 0
						: tipoParametroLaboratorial.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		AnaliseLaboratorial other = (AnaliseLaboratorial) obj;
		
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		
		if(descricao == null){
			if (other.getDescricao() != null){
				return false;
			}
		} else if(descricao.equals(other.getDescricao())){
			return true;
		}
		
		return false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoParametroLaboratorial getTipoParametroLaboratorial() {
		return tipoParametroLaboratorial;
	}

	public void setTipoParametroLaboratorial(
			TipoParametroLaboratorial tipoParametroLaboratorial) {
		this.tipoParametroLaboratorial = tipoParametroLaboratorial;
	}

	public List<ParametroLaboratorial> getParametrosLaboratoriais() {
		return parametrosLaboratoriais;
	}

	public void setParametrosLaboratoriais(
			List<ParametroLaboratorial> parametrosLaboratoriais) {
		this.parametrosLaboratoriais = parametrosLaboratoriais;
	}

	public Enquadramento getEnquadramento() {
		return enquadramento;
	}

	public void setEnquadramento(Enquadramento enquadramento) {
		this.enquadramento = enquadramento;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String isAtivoToDisplay() {
		return this.ativo ? "Sim" : "Não";
	}

}
