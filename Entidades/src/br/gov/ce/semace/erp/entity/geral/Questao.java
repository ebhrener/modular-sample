package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoResposta;

@Entity
@Audited
@Table(name="questao", schema = "geral")
public class Questao implements Serializable, Comparable<Questao> {

	private static final long serialVersionUID = 6590622535330468373L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_questao", name = "SEQ_QUESTAO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_QUESTAO")
	private Long id;

	private String descricao;

	private String numero;

	private boolean ativo = true;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "questao_dependencia_id")
	private Questao questaoDependencia;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "opcao_dependencia_id")
	private Opcao opcaoDependencia;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "questionario_id")
	private Questionario questionario;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "questao", cascade = CascadeType.ALL)
	private List<Opcao> opcoes;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "questao")
	private List<RespostaQuestionario> respostasQuestionario;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "questaoDependencia")
	private List<Questao> questoesDependentes;

	@Column(name="questao_obrigatoria")
	private Boolean questaoObrigatoria = Boolean.FALSE;

	@Transient
	private RespostaQuestionario respostaQuestionario = new RespostaQuestionario();

	@Transient
	private List<RespostaQuestionario> listRespostasQuestionario = new ArrayList<RespostaQuestionario>();

	@Transient
	private String numeroOrdenado;

	@Transient
	private Boolean exibe = true;

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Questao)) {
			return false;
		}
		Questao other = (Questao) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Opcao> getOpcoes() {
		return opcoes;
	}

	public void setOpcoes(List<Opcao> opcoes) {
		this.opcoes = opcoes;
	}

	public Questionario getQuestionario() {
		return questionario;
	}

	public void setQuestionario(Questionario questionario) {
		this.questionario = questionario;
	}

	public Questao getQuestaoDependencia() {
		return questaoDependencia;
	}

	public void setQuestaoDependencia(Questao questaoDependencia) {
		this.questaoDependencia = questaoDependencia;
	}

	public Opcao getOpcaoDependencia() {
		return opcaoDependencia;
	}

	public void setOpcaoDependencia(Opcao opcaoDependencia) {
		this.opcaoDependencia = opcaoDependencia;
	}

	public RespostaQuestionario getRespostaQuestionario() {
		return respostaQuestionario;
	}

	public void setRespostaQuestionario(RespostaQuestionario respostaQuestionario) {
		this.respostaQuestionario = respostaQuestionario;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNumeroOrdenado() {
		return numeroOrdenado;
	}

	public void setNumeroOrdenado(String numeroOrdenado) {
		this.numeroOrdenado = numeroOrdenado;
	}

	public List<RespostaQuestionario> getRespostasQuestionario() {
		return respostasQuestionario;
	}

	public void setRespostasQuestionario(
			List<RespostaQuestionario> respostasQuestionario) {
		this.respostasQuestionario = respostasQuestionario;
	}

	public List<Questao> getQuestoesDependentes() {
		return questoesDependentes;
	}

	public void setQuestoesDependentes(List<Questao> questoesDependentes) {
		this.questoesDependentes = questoesDependentes;
	}

	@Override
	public int compareTo(Questao questao) {
		if (questao != null && questao.getNumero() != null && getNumero() != null) {
			if (!this.getNumero().contains(".") && !questao.getNumero().contains(".")) {
				return Integer.valueOf(getNumero()).compareTo(Integer.valueOf(questao.getNumero()));
			}

			String string = "";
			String stringQuestao = "";

			for (int i = 0; i < getNumero().split("\\.").length; i++) {
				string += "0000".substring(0, 4 - getNumero().split("\\.")[i].length()) + getNumero().split("\\.")[i] + ".";
			}
			for (int i = 0; i < questao.getNumero().split("\\.").length; i++) {
				stringQuestao += "0000".substring(0, 4 - questao.getNumero().split("\\.")[i].length()) + questao.getNumero().split("\\.")[i] + ".";
			}

			return string.compareTo(stringQuestao);
		}
		return 0;
	}

	public void montarNumeroQuestao() {
		String [] a = this.getNumero().split("\\.");

		String numero = "";
		for (int i = 0; i < a.length; i++) {
			numero += String.format("%03d", new Integer(a[i]));
			if(i != a.length-1){
				numero +=".";
			}
		}
		this.setNumeroOrdenado(numero);
	}


	/**
	 * Método {@link Transient}e para verificar se o {@link TipoResposta} da {@link Opcao} é
	 * 	do tipo <b>TEXTO_CURTO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 11/06/2013 14:50:59
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link TipoResposta} da {@link Opcao} seja
	 * 	<b>TEXTO_CURTO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see Opcao#isTextoCurto()
	 */
	@Transient
	public boolean isTextoCurto() {

		if(!this.getOpcoes().isEmpty()){
			Opcao opcao = this.getOpcoes().get(0);
			if(opcao !=null){
				return opcao.isTextoCurto();
			}
		}

		return false;
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link TipoResposta} da {@link Opcao} é
	 * 	do tipo <b>TEXTO_LONGO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 11/06/2013 14:53:05
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link TipoResposta} seja
	 * 	<b>TEXTO_LONGO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see Opcao#isTextoLongo()
	 */
	@Transient
	public boolean isTextoLongo() {
		if(!this.getOpcoes().isEmpty()){
			Opcao opcao = this.getOpcoes().get(0);
			if(opcao !=null){
				return opcao.isTextoLongo();
			}
		}

		return false;
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link TipoResposta} da {@link Opcao} é
	 * 	do tipo <b>MULTIPLA_ESCOLHA</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 11/06/2013 14:54:04
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link TipoResposta} seja
	 * 	<b>MULTIPLA_ESCOLHA</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see Opcao#isMultiplaEscolha()
	 */
	@Transient
	public boolean isMultiplaEscolha() {

		if(!this.getOpcoes().isEmpty()){
			Opcao opcao = this.getOpcoes().get(0);
			if(opcao !=null){
				return opcao.isMultiplaEscolha();
			}
		}

		return false;
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link TipoResposta} da {@link Opcao} é
	 * 	do tipo <b>CAIXA_SELECAO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 11/06/2013 14:54:48
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link TipoResposta} seja
	 * 	<b>CAIXA_SELECAO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see Opcao#isCaixaSelecao()
	 */
	@Transient
	public boolean isCaixaSelecao() {

		if(!this.getOpcoes().isEmpty()){
			Opcao opcao = this.getOpcoes().get(0);
			if(opcao !=null){
				return opcao.isCaixaSelecao();
			}
		}

		return false;
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link TipoResposta} da {@link Opcao} é
	 * 	do tipo <b>OUTROS</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 11/06/2013 14:55:39
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link TipoResposta} seja
	 * 	<b>OUTROS</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see Opcao#isOutros()
	 */
	@Transient
	public boolean isOutros() {

		if(!this.getOpcoes().isEmpty()){
			Opcao opcao = this.getOpcoes().get(0);
			if(opcao !=null){
				return opcao.isOutros();
			}
		}

		return false;
	}

	@Transient
	public boolean isUploadImagem() {

		if(!this.getOpcoes().isEmpty()){
			Opcao opcao = this.getOpcoes().get(0);
			if(opcao !=null){
				return opcao.isUploadImagem();
			}
		}

		return false;
	}

	@Transient
	public boolean isMapaResposta() {

		if(!this.getOpcoes().isEmpty()){
			Opcao opcao = this.getOpcoes().get(0);
			if(opcao !=null){
				System.out.println(opcao.getTipoResposta());
				return opcao.isMapaResposta();
			}
		}

		return false;
	}

	@Transient
	public boolean isMapaCabecalhoResposta() {

		if(!this.getOpcoes().isEmpty()){
			Opcao opcao = this.getOpcoes().get(0);
			if(opcao !=null){
				System.out.println(opcao.getTipoResposta());
				return opcao.isMapaCabecalhoResposta();
			}
		}

		return false;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getExibe() {
		return exibe;
	}

	public void setExibe(Boolean exibe) {
		this.exibe = exibe;
	}

	@Transient
	public List<RespostaQuestionario> getListRespostasQuestionario() {
		return listRespostasQuestionario;
	}

	@Transient
	public void setListRespostasQuestionario(List<RespostaQuestionario> listRespostasQuestionario) {
		this.listRespostasQuestionario = listRespostasQuestionario;
	}

	public Boolean getQuestaoObrigatoria() {
		return questaoObrigatoria;
	}

	public void setQuestaoObrigatoria(Boolean questaoObrigatoria) {
		this.questaoObrigatoria = questaoObrigatoria;
	}
}