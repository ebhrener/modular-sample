package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio.AutoDeclaracaoAtividadePrincipal;

@Entity
@Table(schema="servicos",name="atividade_principal")
public class AtividadePrincipal {
	

	@Id
	@SequenceGenerator(sequenceName = "servicos.seq_atividade_principal", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;
	private Integer codigoAtividadePrincipal;
	
	@Transient
	private String descricao;
	
	@ManyToOne
	private AutoDeclaracao autoDeclaracao;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getCodigoAtividadePrincipal() {
		return codigoAtividadePrincipal;
	}
	public void setCodigoAtividadePrincipal(Integer codigoAtividadePrincipal) {
		this.codigoAtividadePrincipal = codigoAtividadePrincipal;
	}
	public AutoDeclaracao getAutoDeclaracao() {
		return autoDeclaracao;
	}
	public void setAutoDeclaracao(AutoDeclaracao autoDeclaracao) {
		this.autoDeclaracao = autoDeclaracao;
	}
	public String getDescricao() {
		descricao = AutoDeclaracaoAtividadePrincipal.showDescricaoByCodigo(getCodigoAtividadePrincipal());
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
