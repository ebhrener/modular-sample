package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.SituacaoRama;

@Entity
@Audited
@Table(name = "analise_tecnico_rama", schema = "licenciamento")
public class AnaliseTecnicoRama implements Serializable, Comparable<AnaliseTecnicoRama> {

	private static final long serialVersionUID = -4730078309977303826L;

	@Id
	@SequenceGenerator(sequenceName = "licenciamento.SEQ_ANALISE_TECNICO_RAMA", name = "SEQ_ANALISE_TECNICO_RAMA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ANALISE_TECNICO_RAMA")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tecnico_id")
	private Funcionario funcionario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rama_id")
	private Rama rama;

	@Column(name = "data_recebimento_demanda")
	private Date dataRecebimentoDemanda;

	@Column(name = "data_inicio_analise")
	private Date dataInicioAnalise;

	@Column(name = "data_fim_analise")
	private Date dataFimAnalise;

	@Column(name = "remanejado")
	private Boolean remanejado;

	@Column(name = "redistribuido")
	private Boolean redistribuido;

	@Column(name = "data_redistribuido")
	private Date dataRedistribuido;

	@Column(name = "data_remanejado")
	private Date dataRemanejado;

	@Column(name = "justificativa",columnDefinition="text")
	private String justificativa;

	@Column(name = "data_pendencia")
	private Date dataPendencia;

	@Column(name = "data_remocao_pendencia")
	private Date dataRemocaoPendencia;

	@Column(name = "finalizado")
	private Boolean finalizado;

	@Column(name = "ativo")
	private Boolean ativo;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "analiseTecnica", cascade = CascadeType.ALL)
	private List<RelatorioTecnico> listRelatorio;

	@Column(name = "resultado_analise")
	private SituacaoRama resultadoAnalise;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "analise", cascade = CascadeType.ALL)
	private List<ItemPendencia> listPendencias = new ArrayList<ItemPendencia>();
	
	@Transient
	private RelatorioTecnico ultimoRelatorio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Rama getRama() {
		return rama;
	}

	public void setRama(Rama rama) {
		this.rama = rama;
	}

	public Date getDataRecebimentoDemanda() {
		return dataRecebimentoDemanda;
	}

	public void setDataRecebimentoDemanda(Date dataRecebimentoDemanda) {
		this.dataRecebimentoDemanda = dataRecebimentoDemanda;
	}

	public Date getDataInicioAnalise() {
		return dataInicioAnalise;
	}

	public void setDataInicioAnalise(Date dataInicioAnalise) {
		this.dataInicioAnalise = dataInicioAnalise;
	}

	public Date getDataFimAnalise() {
		return dataFimAnalise;
	}

	public void setDataFimAnalise(Date dataFimAnalise) {
		this.dataFimAnalise = dataFimAnalise;
	}

	public Boolean getRemanejado() {
		return remanejado;
	}

	public void setRemanejado(Boolean remanejado) {
		this.remanejado = remanejado;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public Date getDataRemanejado() {
		return dataRemanejado;
	}

	public void setDataRemanejado(Date dataRemanejado) {
		this.dataRemanejado = dataRemanejado;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public Boolean getRedistribuido() {
		return redistribuido;
	}

	public List<RelatorioTecnico> getListRelatorio() {
		return listRelatorio;
	}

	public void setListRelatorio(List<RelatorioTecnico> listRelatorio) {
		this.listRelatorio = listRelatorio;
	}

	public void setRedistribuido(Boolean redistribuido) {
		this.redistribuido = redistribuido;
	}

	public Date getDataRedistribuido() {
		return dataRedistribuido;
	}

	public void setDataRedistribuido(Date dataRedistribuido) {
		this.dataRedistribuido = dataRedistribuido;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}


	public SituacaoRama getResultadoAnalise() {
		return resultadoAnalise;
	}

	public void setResultadoAnalise(SituacaoRama resultadoAnalise) {
		this.resultadoAnalise = resultadoAnalise;
	}


	public Date getDataPendencia() {
		return dataPendencia;
	}

	public void setDataPendencia(Date dataPendencia) {
		this.dataPendencia = dataPendencia;
	}

	public Date getDataRemocaoPendencia() {
		return dataRemocaoPendencia;
	}

	public void setDataRemocaoPendencia(Date dataRemocaoPendencia) {
		this.dataRemocaoPendencia = dataRemocaoPendencia;
	}

	public RelatorioTecnico getUltimoRelatorio() {
		return ultimoRelatorio;
	}

	public void setUltimoRelatorio(RelatorioTecnico ultimoRelatorio) {
		this.ultimoRelatorio = ultimoRelatorio;
	}

	public List<ItemPendencia> getListPendencias() {
		return listPendencias;
	}

	public void setListPendencias(List<ItemPendencia> listPendencias) {
		this.listPendencias = listPendencias;
	}

	@Override
	public int compareTo(AnaliseTecnicoRama o) {
		if (o != null) {
			return this.id.compareTo(o.getId());
		}
		return 0;
	}

	@Override
	public AnaliseTecnicoRama clone() throws CloneNotSupportedException {
		AnaliseTecnicoRama analise = new AnaliseTecnicoRama();
		analise.setId(this.id);
		analise.setDataFimAnalise(this.dataFimAnalise);
		analise.setDataInicioAnalise(this.dataInicioAnalise);
		analise.setDataPendencia(this.dataPendencia);
		analise.setDataRecebimentoDemanda(this.dataRecebimentoDemanda);
		analise.setDataRedistribuido(this.dataRedistribuido);
		analise.setDataRemanejado(this.dataRemanejado);
		analise.setDataRemocaoPendencia(this.dataRemocaoPendencia);
		analise.setFinalizado(this.finalizado);
		analise.setFuncionario(this.funcionario);
		analise.setJustificativa(this.justificativa);
		analise.setListRelatorio(this.listRelatorio);
		analise.setRama(this.rama);
		analise.setRedistribuido(this.redistribuido);
		analise.setRemanejado(this.remanejado);
		analise.setResultadoAnalise(this.resultadoAnalise);

		return analise;
	}



}
