package br.gov.ce.semace.erp.entity.viproc;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * Entidade para mapeamento de setor entre siga, natuur e viproc
 * 
 * @author Joerlan Oliveira Lima [joerlan.joe@gmail.com] - 16/07/2013 08:47:18
 *
 */
@Entity
@Table(name="mapeamento_setor", schema="viproc")
public class MapeamentoSetor implements Serializable {	
		
	private static final long serialVersionUID = 2232175806626109487L;

	@Id
	@SequenceGenerator(sequenceName="viproc.seq_mapeamento_setor", name="SEQMAPSET", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQMAPSET")
	private Long id;	
	
	@Column(name="setor_siga_id", length=4)
	private String setorSigaId;
	
	@Column(name="setor_natuur_id")
	private Integer setorNatuurId;
	
	@Column(name="setor_viproc")
	private String setorViproc;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSetorSigaId() {
		return setorSigaId;
	}
	public void setSetorSigaId(String setorSigaId) {
		this.setorSigaId = setorSigaId;
	}
	public Integer getSetorNatuurId() {
		return setorNatuurId;
	}
	public void setSetorNatuurId(Integer setorNatuurId) {
		this.setorNatuurId = setorNatuurId;
	}
	public String getSetorViproc() {
		return setorViproc;
	}
	public void setSetorViproc(String setorViproc) {
		this.setorViproc = setorViproc;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MapeamentoSetor other = (MapeamentoSetor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}