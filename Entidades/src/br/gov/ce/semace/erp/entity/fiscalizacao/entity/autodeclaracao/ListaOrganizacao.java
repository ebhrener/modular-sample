package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listaOrganizacao", propOrder = {
    "organizacoesSociais"
})
public class ListaOrganizacao {
	
	private List<OrganizacaoSocial> organizacoesSociais;

	public List<OrganizacaoSocial> getOrganizacoesSociais() {
		return organizacoesSociais;
	}

	public void setOrganizacoesSociais(List<OrganizacaoSocial> organizacoesSociais) {
		this.organizacoesSociais = organizacoesSociais;
	}
	

}
