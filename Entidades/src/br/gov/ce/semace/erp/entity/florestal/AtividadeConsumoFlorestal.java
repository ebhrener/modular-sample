package br.gov.ce.semace.erp.entity.florestal;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.geral.UnidadeMedida;

@Entity
@Audited
@Table(name = "atividade_consumo_florestal", schema = "florestal")
public class AtividadeConsumoFlorestal implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -8758533128450998365L;

	@Id
	@SequenceGenerator(sequenceName = "florestal.seq_atividade_consumo_florestal", name = "florestal.seq_atividade_consumo_florestal", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "florestal.seq_atividade_consumo_florestal")
	private Long id;

	@Column(name="is_principal")
	private Boolean principal;

	@ManyToOne(fetch=FetchType.LAZY, optional=true)
	@JoinColumn(name="cadastro_consumo_florestal_id")
	private CadastroConsumoFlorestal cadastroConsumoFlorestal;

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="atividade_id")
	private Atividade atividade = new Atividade();

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="atividade_florestal_id")
	private AtividadeFlorestal atividadeFlorestal = new AtividadeFlorestal();

	@ManyToOne(optional=false,fetch=FetchType.LAZY)
	@JoinColumn(name="classificacao_id")
	private Classificacao classificacao = new Classificacao();

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="producao_id")
	private Producao producao = new Producao();

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="dado_consumo_id")
	private DadosConsumo dadosConsumo;

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="produto_id")
	private Produto produto = new Produto();

	@Column(name="producao_media")
	private BigDecimal producaoMedia;

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="unidade_medida_producao_id")
	private UnidadeMedida unidadeMedidaProducao;

	@Column(name="comsumo_medio")
	private BigDecimal consumoMedio;

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="unidade_medida_consumo_id")
	private UnidadeMedida unidadeMedidaConsumo;

	@Column(name="quantidade_meses_consumo")
	private Integer qtdMesesConsumo;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(name="tipologia_id")
	private Tipologia tipologia = new Tipologia();

	@Column(name = "descricao_outra_tipologia")
	private String descricao;

	@Transient
	private int indice;

	@Transient
	private Object selectedAtividade;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public CadastroConsumoFlorestal getCadastroConsumoFlorestal() {
		return cadastroConsumoFlorestal;
	}


	public void setCadastroConsumoFlorestal(CadastroConsumoFlorestal cadastroConsumoFlorestal) {
		this.cadastroConsumoFlorestal = cadastroConsumoFlorestal;
	}


	public Atividade getAtividade() {
		return atividade;
	}


	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}


	public Classificacao getClassificacao() {
		return classificacao;
	}


	public void setClassificacao(Classificacao classificacao) {
		this.classificacao = classificacao;
	}


	public Producao getProducao() {
		return producao;
	}


	public void setProducao(Producao producao) {
		this.producao = producao;
	}


	public Produto getProduto() {
		return produto;
	}


	public void setProduto(Produto produto) {
		this.produto = produto;
	}


	public BigDecimal getProducaoMedia() {
		return producaoMedia;
	}


	public void setProducaoMedia(BigDecimal producaoMedia) {
		this.producaoMedia = producaoMedia;
	}


	public UnidadeMedida getUnidadeMedidaProducao() {
		return unidadeMedidaProducao;
	}


	public void setUnidadeMedidaProducao(UnidadeMedida unidadeMedidaProducao) {
		this.unidadeMedidaProducao = unidadeMedidaProducao;
	}


	public BigDecimal getConsumoMedio() {
		return consumoMedio;
	}


	public void setConsumoMedio(BigDecimal consumoMedio) {
		this.consumoMedio = consumoMedio;
	}


	public UnidadeMedida getUnidadeMedidaConsumo() {
		return unidadeMedidaConsumo;
	}


	public void setUnidadeMedidaConsumo(UnidadeMedida unidadeMedidaConsumo) {
		this.unidadeMedidaConsumo = unidadeMedidaConsumo;
	}


	public Integer getQtdMesesConsumo() {
		return qtdMesesConsumo;
	}


	public void setQtdMesesConsumo(Integer qtdMesesConsumo) {
		this.qtdMesesConsumo = qtdMesesConsumo;
	}


	public Tipologia getTipologia() {
		return tipologia;
	}


	public void setTipologia(Tipologia tipologia) {
		this.tipologia = tipologia;
	}


	public AtividadeFlorestal getAtividadeFlorestal() {
		return atividadeFlorestal;
	}


	public void setAtividadeFlorestal(AtividadeFlorestal atividadeFlorestal) {
		this.atividadeFlorestal = atividadeFlorestal;
	}

	public String getDisplayAtividade() {
		if (atividade != null && atividade.getId() != null) {
			return atividade.getCodigo()+" - "+atividade.getDescricao();
		}else if (atividadeFlorestal != null && atividadeFlorestal.getId() != null){
			if (atividadeFlorestal.getCodigo() != null && !atividadeFlorestal.getCodigo().trim().isEmpty()) {
				return atividadeFlorestal.getCodigo()+" - "+atividadeFlorestal.getDescricao();
			}
			return atividadeFlorestal.getDescricao();
		}
		return null;
	}

	/** Metodo criado para as atividades que estam sendo incluidas no cadastro
	 *  e não foram persistidas no banco de dados, por isso não teem ID .
	 */
	public String getDisplayAtividadeEdit() {
		if(selectedAtividade instanceof Atividade) {
			return ((Atividade)selectedAtividade).getCodigo()+" - "+((Atividade)selectedAtividade).getDescricao();
		}else if(selectedAtividade instanceof AtividadeFlorestal){
			if (((AtividadeFlorestal)selectedAtividade).getCodigo() != null && !((AtividadeFlorestal)selectedAtividade).getCodigo().trim().isEmpty()) {
				return ((AtividadeFlorestal)selectedAtividade).getCodigo()+" - "+((AtividadeFlorestal)selectedAtividade).getDescricao();
			}
			return ((AtividadeFlorestal)selectedAtividade).getDescricao();
		}
		return null;
	}

	public Boolean getPrincipal() {
		return principal;
	}


	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}


	public DadosConsumo getDadosConsumo() {
		return dadosConsumo;
	}


	public void setDadosConsumo(DadosConsumo dadosConsumo) {
		this.dadosConsumo = dadosConsumo;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public int getIndice() {
		return indice;
	}


	public void setIndice(int indice) {
		this.indice = indice;
	}


	public Object getSelectedAtividade() {
		return selectedAtividade;
	}


	public void setSelectedAtividade(Object selectedAtividade) {
		this.selectedAtividade = selectedAtividade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atividade == null) ? 0 : atividade.hashCode());
		result = prime * result + ((atividadeFlorestal == null) ? 0 : atividadeFlorestal.hashCode());
		result = prime * result + ((classificacao == null) ? 0 : classificacao.hashCode());
		result = prime * result + ((producao == null) ? 0 : producao.hashCode());
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		result = prime * result + ((dadosConsumo == null) ? 0 : dadosConsumo.hashCode());
		result = prime * result + ((tipologia == null) ? 0 : tipologia.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		AtividadeConsumoFlorestal other = (AtividadeConsumoFlorestal) obj;

		if (atividade == null || atividade.getId() == null) {
			if (other.atividade != null && other.atividade.getId() != null) {
				return false;
			}
		} else if (!atividade.equals(other.atividade)) {
			return false;
		}

		if (atividadeFlorestal == null || atividadeFlorestal.getId() == null) {
			if (other.atividadeFlorestal != null && other.atividadeFlorestal.getId() != null) {
				return false;
			}
		} else if (!atividadeFlorestal.equals(other.atividadeFlorestal)) {
			return false;
		}

		if (classificacao == null || classificacao.getId() == null) {
			if (other.classificacao != null && other.classificacao.getId() != null) {
				return false;
			}
		} else if (!classificacao.equals(other.classificacao)) {
			return false;
		}

		if (producao == null || producao.getId() == null) {
			if (other.producao != null && other.producao.getId() != null) {
				return false;
			}
		} else if (!producao.equals(other.producao)) {
			return false;
		}

		if (produto == null || produto.getId() == null) {
			if (other.produto != null && other.produto.getId() != null) {
				return false;
			}
		} else if (!produto.equals(other.produto)) {
			return false;
		}

		if (dadosConsumo == null || dadosConsumo.getId() == null) {
			if (other.dadosConsumo != null && other.dadosConsumo.getId() != null) {
				return false;
			}
		} else if (!dadosConsumo.equals(other.dadosConsumo)) {
			return false;
		}

		if (tipologia == null || tipologia.getId() == null) {
			if (other.tipologia != null && other.tipologia.getId() != null) {
				return false;
			}
		} else if (!tipologia.equals(other.tipologia)) {
			return false;
		}

		if (descricao == null) {
			if (other.descricao != null && !other.descricao.trim().isEmpty()) {
				return false;
			}
		} else if (!descricao.equals(other.descricao)) {
			return false;
		}

		return true;
	}

	@Override
	public AtividadeConsumoFlorestal clone() throws CloneNotSupportedException {
		AtividadeConsumoFlorestal atividadeConsumoFlorestal = new AtividadeConsumoFlorestal();
		atividadeConsumoFlorestal.setAtividade(this.atividade == null ? null : this.atividade.clone());
		atividadeConsumoFlorestal.setAtividadeFlorestal(this.atividadeFlorestal == null ? null : this.atividadeFlorestal.clone());
		atividadeConsumoFlorestal.setCadastroConsumoFlorestal(this.cadastroConsumoFlorestal == null ? null : this.cadastroConsumoFlorestal.clone());
		atividadeConsumoFlorestal.setClassificacao(this.classificacao == null ? null : this.classificacao.clone());
		atividadeConsumoFlorestal.setConsumoMedio(this.consumoMedio);
		atividadeConsumoFlorestal.setDadosConsumo(this.dadosConsumo == null ? null : this.dadosConsumo.clone());
		atividadeConsumoFlorestal.setDescricao(this.descricao);
		atividadeConsumoFlorestal.setId(this.id);
		atividadeConsumoFlorestal.setIndice(this.indice);
		atividadeConsumoFlorestal.setPrincipal(this.principal);
		atividadeConsumoFlorestal.setProducao(this.producao == null ? null : this.producao.clone());
		atividadeConsumoFlorestal.setProducaoMedia(this.producaoMedia);
		atividadeConsumoFlorestal.setProduto(this.produto == null ? null : this.produto.clone());
		atividadeConsumoFlorestal.setQtdMesesConsumo(this.qtdMesesConsumo);
		atividadeConsumoFlorestal.setSelectedAtividade(this.selectedAtividade);
		atividadeConsumoFlorestal.setTipologia(this.tipologia == null ? null : this.tipologia.clone());
		atividadeConsumoFlorestal.setUnidadeMedidaConsumo(this.unidadeMedidaConsumo == null ? null : this.unidadeMedidaConsumo.clone());
		atividadeConsumoFlorestal.setUnidadeMedidaProducao(this.unidadeMedidaProducao == null ? null : this.unidadeMedidaProducao.clone());
		return atividadeConsumoFlorestal;
	}
}