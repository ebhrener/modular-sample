package br.gov.ce.semace.erp.entity.planoviagem;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.StatusSolicitacaoTransporte;

@Entity
@Audited
@Table(schema = "plano_viagem", name = "historico_solicitacao_viagem")
public class HistoricoSolicitacaoViagem implements Serializable {
	private static final long serialVersionUID = 5688120054785180903L;

	@Id
	@SequenceGenerator(sequenceName = "plano_viagem.seq_historico_solicitacao_viagem", name = "SEQ_HISTORICO_SOLICITACAO_VIAGEM", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_HISTORICO_SOLICITACAO_VIAGEM")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "solicitacao_viagem_id")
	private SolicitacaoViagem solicitacaoViagem = new SolicitacaoViagem();

	@Column(name = "status_solicitacao_transporte")
	private StatusSolicitacaoTransporte statusSolicitacaoTransporte;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;

	@Column(name = "descricao")
	private String descricao;

	@Column(name = "data_acao")
	private Timestamp dataAcao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SolicitacaoViagem getSolicitacaoViagem() {
		return solicitacaoViagem;
	}

	public void setSolicitacaoViagem(SolicitacaoViagem solicitacaoViagem) {
		this.solicitacaoViagem = solicitacaoViagem;
	}

	public StatusSolicitacaoTransporte getStatusSolicitacaoTransporte() {
		return statusSolicitacaoTransporte;
	}

	public void setStatusSolicitacaoTransporte(
			StatusSolicitacaoTransporte statusSolicitacaoTransporte) {
		this.statusSolicitacaoTransporte = statusSolicitacaoTransporte;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Timestamp getDataAcao() {
		return dataAcao;
	}

	public void setDataAcao(Timestamp dataAcao) {
		this.dataAcao = dataAcao;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}