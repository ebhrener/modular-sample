package br.gov.ce.semace.erp.entity.julgamento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoMajoracao;

@Entity
@Audited
@Table(name="majoracao", schema="julgamento" )
public class Majoracao implements Serializable{

	private static final long serialVersionUID = 7691660320748925902L;

	@Id
	@SequenceGenerator(sequenceName ="julgamento.seq_majoracao", name = "SEQMAJORACAO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMAJORACAO")
	private Long id;

	@Column(name="titulo")
	private String titulo;

	@Column(name="tipo_majoracao")
	private TipoMajoracao tipo;

	@Column(name="min_percentual")
	private Integer minPercentual;

	@Column(name="max_percentual")
	private Integer maxPercentual;

	@Column(name="possui_faixa_percentual", nullable = false)
	private Boolean possuiFaixaPercentual;


	/***********************
	 * GETTERS and SETTERS *
	 ***********************/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public TipoMajoracao getTipo() {
		return tipo;
	}

	public void setTipo(TipoMajoracao tipo) {
		this.tipo = tipo;
	}

	public Integer getMinPercentual() {
		return minPercentual;
	}

	public void setMinPercentual(Integer minPercentual) {
		this.minPercentual = minPercentual;
	}

	public Integer getMaxPercentual() {
		return maxPercentual;
	}

	public void setMaxPercentual(Integer maxPercentual) {
		this.maxPercentual = maxPercentual;
	}

	public Boolean getPossuiFaixaPercentual() {
		return possuiFaixaPercentual;
	}

	public void setPossuiFaixaPercentual(Boolean possuiFaixaPercentual) {
		this.possuiFaixaPercentual = possuiFaixaPercentual;
	}

	public String getTituloListagem() {
		return getTitulo() + " " +
				(getPossuiFaixaPercentual()
						? ": de " + getMinPercentual() + "% a " + getMaxPercentual() + "%"
						: "");
	}

}
