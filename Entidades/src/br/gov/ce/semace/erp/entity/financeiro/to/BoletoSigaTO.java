package br.gov.ce.semace.erp.entity.financeiro.to;

import java.util.Date;

public class BoletoSigaTO {

	private Integer id;

	private String spu;

	private String nomeSacado;

	private String cpfSacado;

	private String cnpjSacado;
	
	private String cidadeSacado;

	private String ufSacado;

	private String cepSacado;

	private String municipioSacado;

	private String enderecoSacado;

	private String complementoSacados;

	private String numeroDocumentoBanco;

	private Date dataVencimento;

	private Date dataProcessamento;

	private Date dataDocumento;

	private Date dataPagamento;

	private String nossoNumero;

	private String numeroDocumento;

	private String valorDocumento;

	private String acrescimoDocumento;

	private Boolean pago;

	private String documentoSacado;

	private String observacao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public String getNomeSacado() {
		return nomeSacado;
	}

	public void setNomeSacado(String nomeSacado) {
		this.nomeSacado = nomeSacado;
	}

	public String getCpfSacado() {
		return cpfSacado;
	}

	public void setCpfSacado(String cpfSacado) {
		this.cpfSacado = cpfSacado;
	}

	public String getCnpjSacado() {
		return cnpjSacado;
	}

	public void setCnpjSacado(String cnpjSacado) {
		this.cnpjSacado = cnpjSacado;
	}

	public String getUfSacado() {
		return ufSacado;
	}

	public void setUfSacado(String ufSacado) {
		this.ufSacado = ufSacado;
	}

	public String getCepSacado() {
		return cepSacado;
	}

	public void setCepSacado(String cepSacado) {
		this.cepSacado = cepSacado;
	}

	public String getMunicipioSacado() {
		return municipioSacado;
	}

	public void setMunicipioSacado(String municipioSacado) {
		this.municipioSacado = municipioSacado;
	}

	public String getEnderecoSacado() {
		return enderecoSacado;
	}

	public void setEnderecoSacado(String enderecoSacado) {
		this.enderecoSacado = enderecoSacado;
	}

	public String getComplementoSacados() {
		return complementoSacados;
	}

	public void setComplementoSacados(String complementoSacados) {
		this.complementoSacados = complementoSacados;
	}

	public String getNumeroDocumentoBanco() {
		return numeroDocumentoBanco;
	}

	public void setNumeroDocumentoBanco(String numeroDocumentoBanco) {
		this.numeroDocumentoBanco = numeroDocumentoBanco;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public Date getDataDocumento() {
		return dataDocumento;
	}

	public void setDataDocumento(Date dataDocumento) {
		this.dataDocumento = dataDocumento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public String getNossoNumero() {
		return nossoNumero;
	}

	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getValorDocumento() {
		return valorDocumento;
	}

	public void setValorDocumento(String valorDocumento) {
		this.valorDocumento = valorDocumento;
	}

	public String getAcrescimoDocumento() {
		return acrescimoDocumento;
	}

	public void setAcrescimoDocumento(String acrescimoDocumento) {
		this.acrescimoDocumento = acrescimoDocumento;
	}

	public Boolean getPago() {
		return pago;
	}

	public void setPago(Boolean pago) {
		this.pago = pago;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getDocumentoSacado() {
		return documentoSacado;
	}

	public void setDocumentoSacado(String documentoSacado) {
		this.documentoSacado = documentoSacado;
	}

	public String getCidadeSacado() {
		return cidadeSacado;
	}

	public void setCidadeSacado(String cidadeSacado) {
		this.cidadeSacado = cidadeSacado;
	}

}
