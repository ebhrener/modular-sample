package br.gov.ce.semace.erp.entity.geo;



import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "imagem", schema = "public")
public class Imagem implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 5179650148005547852L;

	@Id
	@SequenceGenerator(sequenceName="seq_imagem", name="seq_imagem", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_imagem")
	private Long id;
	
	@Column(nullable = false)
	@Lob
	@Basic(fetch = FetchType.EAGER)
	@Type(type = "org.hibernate.type.BinaryType")
	private byte[] dados;
	
	@OneToOne(mappedBy="imagem")
	private ProcessoImagem processoImagem;
	
	
	public Imagem() {
		super();
	}

	public Imagem(byte[] dados) {
		super();
		this.dados = dados;
	}

}

