package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio.AutoDeclaracaoCondicaoUsoPosseTerra;

@Entity
@Table(name="posse_terra", schema="servicos")
public class PosseTerra {
	
	@Id
	@SequenceGenerator(sequenceName = "servicos.seq_servicos", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;
	private Integer codigoCondicaoPosseUsoTerra;
	
	@ManyToOne
	private AutoDeclaracao autoDeclaracao;
	
	@Transient
	private String descricao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AutoDeclaracao getAutoDeclaracao() {
		return autoDeclaracao;
	}

	public void setAutoDeclaracao(AutoDeclaracao autoDeclaracao) {
		this.autoDeclaracao = autoDeclaracao;
	}

	public Integer getCodigoCondicaoPosseUsoTerra() {
		return codigoCondicaoPosseUsoTerra;
	}

	public void setCodigoCondicaoPosseUsoTerra(Integer codigoCondicaoPosseUsoTerra) {
		this.codigoCondicaoPosseUsoTerra = codigoCondicaoPosseUsoTerra;
	}

	public String getDescricao() {
		descricao = AutoDeclaracaoCondicaoUsoPosseTerra.showDescricaoByCodigo(getCodigoCondicaoPosseUsoTerra());
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	

}
