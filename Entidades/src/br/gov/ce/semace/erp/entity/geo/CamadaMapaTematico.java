package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="camada_mapa_tematico")
public class CamadaMapaTematico implements Serializable {

	private static final long serialVersionUID = 7967280849350505844L;

	@Id 
	@SequenceGenerator(sequenceName="seq_camada_mapa_tematico", name="seq_camada_mapa_tematico", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_camada_mapa_tematico")
	private Long id;
	
	@Column
	String nome;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="mapa_tematico_id", nullable=true)
	private MapaTematico mapaTematico;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="base_cartografica_id", nullable=true)
	private BaseCartografica baseCartografica;
	
	@Column(name = "url_imagens")
	private String urlImagens;
	
	@Column(name = "limites_quadrantes")
	private String limitesQuadrantes;
	
	public CamadaMapaTematico() {
		super();
	}
	
	public CamadaMapaTematico(MapaTematico mapaTematico) {
		super();
		this.mapaTematico = mapaTematico;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public MapaTematico getMapaTematico() {
		return mapaTematico;
	}

	public void setMapaTematico(MapaTematico mapaTematico) {
		this.mapaTematico = mapaTematico;
	}

	public String getUrlImagens() {
		return urlImagens;
	}

	public void setUrlImagens(String urlImagens) {
		this.urlImagens = urlImagens;
	}

	public String getLimitesQuadrantes() {
		return limitesQuadrantes;
	}

	public void setLimitesQuadrantes(String limitesQuadrantes) {
		this.limitesQuadrantes = limitesQuadrantes;
	}

	public BaseCartografica getBaseCartografica() {
		return baseCartografica;
	}

	public void setBaseCartografica(BaseCartografica baseCartografica) {
		this.baseCartografica = baseCartografica;
	}
	
}