package br.gov.ce.semace.erp.entity.julgamento;

import java.io.Serializable;
import java.util.Date;

import br.gov.ce.semace.erp.enuns.FaseJulgamento;

public class PrazoJulgamentoVO implements Serializable {

	private static final long serialVersionUID = 8587245341051791840L;
	
	private FaseJulgamento faseJulgamento;
	
	private Date dataInicio;
	
	private Date dataFim;
	
	// Foi criado do tipo String, ao invés de boolean, pois existe o caso 'Não se aplica'
	private String possuiAnexo;

	private Boolean prazoEncerrado;
	

	/**
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 03/06/2013 15:11:34
	 * 
	 * Método construtor default.
	 */
	public PrazoJulgamentoVO() {
		super();
	}
	
	/**
	 * 
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 03/06/2013 15:11:53
	 * 
	 * Método construtor com parâmetros.
	 * 
	 * @param faseJulgamento
	 * 		referente a {@link FaseJulgamento}.
	 * 
	 * @param dataInicio
	 * 		referente a data de início da {@link FaseJulgamento}.
	 * 
	 * @param dataFim
	 * 		referente a data fim da {@link FaseJulgamento}
	 * 
	 * @param possuiAnexo
	 * 		indica se algum anexo foi anexado no prazo da {@link FaseJulgamento}. 
	 */
	public PrazoJulgamentoVO(FaseJulgamento faseJulgamento, Date dataInicio,
			Date dataFim, String possuiAnexo, Boolean prazoEncerrado) {
		super();
		this.faseJulgamento = faseJulgamento;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.possuiAnexo = possuiAnexo;
		this.prazoEncerrado = prazoEncerrado;
	}

	public FaseJulgamento getFaseJulgamento() {
		return faseJulgamento;
	}

	public void setFaseJulgamento(FaseJulgamento faseJulgamento) {
		this.faseJulgamento = faseJulgamento;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public void setPossuiAnexo(String possuiAnexo) {
		this.possuiAnexo = possuiAnexo;
	}

	public String getPossuiAnexo() {
		return possuiAnexo;
	}

	public Boolean getPrazoEncerrado() {
		return prazoEncerrado;
	}

	public void setPrazoEncerrado(Boolean prazoEncerrado) {
		this.prazoEncerrado = prazoEncerrado;
	}

}
