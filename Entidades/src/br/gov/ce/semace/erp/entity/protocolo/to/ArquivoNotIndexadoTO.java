package br.gov.ce.semace.erp.entity.protocolo.to;

import java.io.Serializable;
import java.math.BigInteger;

public class ArquivoNotIndexadoTO implements Serializable {

	private static final long serialVersionUID = 8232975023003638168L;

	private Long id;

	private String nome;

	private Integer pagina;

	private Integer numeroPaginas;

	private boolean selecionado;

	private String justificativa;

	public ArquivoNotIndexadoTO() {
		super();
	}

	public ArquivoNotIndexadoTO(Long id, String nome, Integer numeroPaginas) {
		super();
		this.id = id;
		this.nome = nome;
		this.numeroPaginas = numeroPaginas;
	}

	public ArquivoNotIndexadoTO(BigInteger id, String nome, Integer numeroPaginas) {
		super();
		this.id = id != null ? id.longValue() : null;
		this.nome = nome;
		this.numeroPaginas = numeroPaginas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPagina() {
		return pagina;
	}

	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}

	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}

	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}
}