package br.gov.ce.semace.erp.entity.licenciamento;

public enum TipoRelatorioRama {

	PENDENCIA("Pendencia"),
	REGULAR("Atende"),
	IRREGULAR("Não Atende"),
	AGUARDANDO_APROVACAO("Aguardando Aprovação Gerencial");

	private String descricao;

	private TipoRelatorioRama(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
