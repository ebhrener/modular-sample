package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * Entidade responsável pelo mapeamento das Configurações utilizadas no RH(Recursos Humanos)
 *
 * @author robson.carvalho - Robson Rocha Carvalho [robson01jc@gmail.com] - Apr 4, 2017, 2:17:06 PM
 */
@Entity
@Audited
@Table(schema = "rh", name="configuracao_rh")
public class ConfiguracaoRH implements Serializable {

	private static final long serialVersionUID = 1876604690301801575L;

	@Id
	public Long id;

	/**
	 * Lista de funcionários (ver {@link Funcionario}) aptos a receber notificação de retorno de férias.
	 *
	 * @author robson.carvalho - Robson Rocha Carvalho [robson01jc@gmail.com] - Apr 4, 2017, 2:09:21 PM
	 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "configuracao_rh_funcionario", schema = "rh",
			joinColumns = @JoinColumn(name = "configuracao_id"),
			inverseJoinColumns = @JoinColumn(name = "funcionario_id"))
	private List<Funcionario> listFuncionarioNotificacaoFerias = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Funcionario> getListFuncionarioNotificacaoFerias() {
		return listFuncionarioNotificacaoFerias;
	}

	public void setListFuncionarioNotificacaoFerias(List<Funcionario> listFuncionarioNotificacaoFerias) {
		this.listFuncionarioNotificacaoFerias = listFuncionarioNotificacaoFerias;
	}

}