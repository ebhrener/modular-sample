package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoMare;


@Entity
@Audited
@Table(name="mare", schema="laboratorio")
public class Mare implements Serializable{


	/**
	 * Serial
	 */
	private static final long serialVersionUID = 7626113382829489987L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_mare", name="laboratorio.seq_mare", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="laboratorio.seq_mare")
	private Long id;

	@Column(nullable = false)
	private Date data;

	@Column(nullable=false, scale=2, precision=12)
	private BigDecimal altitude;

	@Enumerated(EnumType.ORDINAL)
	@Column(name="tipo_mare")
	private TipoMare tipoMare;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="boletim_id")
	private Boletim boletim;

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public BigDecimal getAltitude() {
		return altitude;
	}

	public void setAltitude(BigDecimal altitude) {
		this.altitude = altitude;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boletim getBoletim() {
		return boletim;
	}

	public void setBoletim(Boletim boletim) {
		this.boletim = boletim;
	}

	public TipoMare getTipoMare() {
		return tipoMare;
	}

	public void setTipoMare(TipoMare tipoMare) {
		this.tipoMare = tipoMare;
	}

}
