package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.laboratorio.SolicitacaoServicoLaboratorial;


@Entity
@Table(name = "agendamento_analise_laboratorial", schema="atendimento")
@Audited
public class AgendamentoAnaliseLaboratorial extends AgendamentoRequerimento {

	private static final long serialVersionUID = -2097469924678214527L;

	public AgendamentoAnaliseLaboratorial() {
		super();
	}

	public AgendamentoAnaliseLaboratorial(Long id) {
		super();
		this.setId(id);
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pai_agendamento_requerimento_id")
	private AgendamentoRequerimento agendamentoRequerimento = new AgendamentoRequerimento();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "agendamentoAnalise", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<SolicitacaoServicoLaboratorial> solicitacoes = new ArrayList<>();

	/**
	 * Indica se o requerimento de análise foi criado por meio da geração de DAE.
	 */
	@Column(name = "generated_from_dae")
	private boolean generatedFromDae = true;

	@Transient
	private boolean listarTodos;

	@Transient
	public boolean isGeradoFromRequerimento() {
		return (this.getAgendamentoRequerimento() != null && this.getAgendamentoRequerimento().getId() != null);
	}

	public List<SolicitacaoServicoLaboratorial> getSolicitacoes() {
		return this.solicitacoes;
	}

	public void setSolicitacoes(List<SolicitacaoServicoLaboratorial> solicitacoes) {
		this.solicitacoes = solicitacoes;
	}

	public AgendamentoRequerimento getAgendamentoRequerimento() {
		return this.agendamentoRequerimento;
	}

	public void setAgendamentoRequerimento(
			AgendamentoRequerimento agendamentoRequerimento) {
		this.agendamentoRequerimento = agendamentoRequerimento;
	}

	public boolean isListarTodos() {
		return listarTodos;
	}

	public void setListarTodos(boolean listarTodos) {
		this.listarTodos = listarTodos;
	}

	public boolean isGeneratedFromDae() {
		return generatedFromDae;
	}

	public void setGeneratedFromDae(boolean generatedFromDae) {
		this.generatedFromDae = generatedFromDae;
	}

}
