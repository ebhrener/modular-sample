package br.gov.ce.semace.erp.entity.florestal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.financeiro.Dae;
import br.gov.ce.semace.erp.entity.geral.Consultoria;
import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.geral.Endereco;
import br.gov.ce.semace.erp.entity.geral.Interessado;
import br.gov.ce.semace.erp.enuns.StatusCadastroConsumoFlorestal;
import br.gov.ce.semace.erp.enuns.TipoSistema;

/**
 * Entidade CadastroConsumoFlorestal
 *
 * @author joerlan
 */
@Entity
@Audited
@Table(name = "cadastro_consumo_florestal", schema = "florestal")
public class CadastroConsumoFlorestal implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 3800569004983220693L;

	@Id
	@SequenceGenerator(sequenceName = "florestal.seq_cadastro_consumo_florestal", name = "florestal.seq_cadastro_consumo_florestal", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "florestal.seq_cadastro_consumo_florestal")
	private Long id;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "interessado_id")
	private Interessado interessado;

	@OneToOne(optional = false)
	@JoinColumn(name = "documento_id")
	private Documento documento;

	@Column(name = "data_cadastro")
	private Date dataCadastro;

	@Column(name = "data_inicio_validade")
	private Date dataInicioValidade;

	@Column(name = "inscricao_estadual")
	private String inscricaoEstadual;

	@OneToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "dae_id")
	private Dae dae;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "consultoria_id")
	private Consultoria consultoria;

	@Column(name = "data_validade")
	private Date dataFinalValidade;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "consumo_id")
	private Consumo consumo = new Consumo();

	@OneToMany(mappedBy = "cadastroConsumoFlorestal", cascade = CascadeType.ALL)
	private List<AtividadeConsumoFlorestal> listAtividadeConsumoFlorestal;

	@Column(name="is_isento", nullable = false)
	private Boolean isento;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	@Column(name = "status_cad_consumo_florestal")
	private StatusCadastroConsumoFlorestal statusCadConsumoFlorestal;

	/**
	 * Transients usados na edição do CCF e suas atividades.
	 */
	@Transient private List<Map<Integer, AtividadeConsumoFlorestal>> listAcoesAtividadesGeral = new ArrayList<>();
	@Transient private TipoSistema tipoSistemaAcao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Interessado getInteressado() {
		return interessado;
	}

	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Dae getDae() {
		return dae;
	}

	public void setDae(Dae dae) {
		this.dae = dae;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Consultoria getConsultoria() {
		return consultoria;
	}

	public void setConsultoria(Consultoria consultoria) {
		this.consultoria = consultoria;
	}

	public Consumo getConsumo() {
		return consumo;
	}

	public void setConsumo(Consumo consumo) {
		this.consumo = consumo;
	}

	public List<AtividadeConsumoFlorestal> getListAtividadeConsumoFlorestal() {
		return listAtividadeConsumoFlorestal;
	}

	public void setListAtividadeConsumoFlorestal(
			List<AtividadeConsumoFlorestal> listAtividadeConsumoFlorestal) {
		this.listAtividadeConsumoFlorestal = listAtividadeConsumoFlorestal;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	// Regras para o Status do CCF de acordo com o Status do seu DAE, se possuir.
	/** @Transient
	public StatusCadastroConsumoFlorestal getStatusCadastroConsumoFlorestal() {
		if (isento != null && isento) {
			if (isVencido()) {
				return StatusCadastroConsumoFlorestal.VENCIDO;
			}
			if (isPreVigente()) {
				return StatusCadastroConsumoFlorestal.CADASTRADO;
			}
			return StatusCadastroConsumoFlorestal.VIGENTE;
		}
		if (dae.isDaePago() || dae.isDaeVinculadoDeposito()) {
			if (isVencido()) {
				return StatusCadastroConsumoFlorestal.VENCIDO;
			}
			if (isPreVigente()) {
				return StatusCadastroConsumoFlorestal.PAGO;
			}
			return StatusCadastroConsumoFlorestal.VIGENTE;
		}
		if (dae.isDaeEmAberto() && !isVencido()) {
			return StatusCadastroConsumoFlorestal.AGUARDANDO_PAGAMENTO;
		}
		if (dae.isDaeExcluido() || dae.isDaeErroProcessamento()) {
			return StatusCadastroConsumoFlorestal.NAO_CONCLUIDO;
		}
		if (dae.isDaeVencido()) {
			return StatusCadastroConsumoFlorestal.CANCELADO_FALTA_PAGAMENTO;
		}
		if (dae.isDaeAguardandoEmissao()) {
			return StatusCadastroConsumoFlorestal.AGUARDANDO_EMISSAO_DAE;
		}

		return null;
	} */

	public boolean isVencido() {
		return dataFinalValidade.before(new Date());
	}

	public boolean isPreVigente() {
		return new Date().before(dataInicioValidade);
	}

	public Date getDataInicioValidade() {
		return dataInicioValidade;
	}

	public void setDataInicioValidade(Date dataInicioValidade) {
		this.dataInicioValidade = dataInicioValidade;
	}

	public Date getDataFinalValidade() {
		return dataFinalValidade;
	}

	public void setDataFinalValidade(Date dataFinalValidade) {
		this.dataFinalValidade = dataFinalValidade;
	}

	@Transient
	public AtividadeConsumoFlorestal getAtividadePrincipal() {
		if (listAtividadeConsumoFlorestal != null && !listAtividadeConsumoFlorestal.isEmpty()) {
			for (AtividadeConsumoFlorestal atividade : listAtividadeConsumoFlorestal) {
				if (atividade.getPrincipal()) {
					return atividade;
				}
			}
		}
		return null;
	}

	@Transient
	public AtividadeConsumoFlorestal getPrimeiraAtividadeSecundaria() {
		if (listAtividadeConsumoFlorestal != null && !listAtividadeConsumoFlorestal.isEmpty()) {
			for (AtividadeConsumoFlorestal atividade : listAtividadeConsumoFlorestal) {
				if (!atividade.getPrincipal()) {
					return atividade;
				}
			}
		}
		return null;
	}

	public Boolean getIsento() {
		return isento;
	}

	public void setIsento(Boolean isento) {
		this.isento = isento;
	}

	public StatusCadastroConsumoFlorestal getStatusCadConsumoFlorestal() {
		return statusCadConsumoFlorestal;
	}

	public void setStatusCadConsumoFlorestal(StatusCadastroConsumoFlorestal statusCadConsumoFlorestal) {
		this.statusCadConsumoFlorestal = statusCadConsumoFlorestal;
	}

	public TipoSistema getTipoSistemaAcao() {
		return tipoSistemaAcao;
	}

	public void setTipoSistemaAcao(TipoSistema tipoSistemaAcao) {
		this.tipoSistemaAcao = tipoSistemaAcao;
	}

	public List<Map<Integer, AtividadeConsumoFlorestal>> getListAcoesAtividadesGeral() {
		return listAcoesAtividadesGeral;
	}

	public void setListAcoesAtividadesGeral(List<Map<Integer, AtividadeConsumoFlorestal>> listAcoesAtividadesGeral) {
		this.listAcoesAtividadesGeral = listAcoesAtividadesGeral;
	}

	@Override
	public CadastroConsumoFlorestal clone() throws CloneNotSupportedException {
		CadastroConsumoFlorestal cadastroConsumoFlorestal = new CadastroConsumoFlorestal();
		cadastroConsumoFlorestal.setConsultoria(this.consultoria);
		cadastroConsumoFlorestal.setConsumo(this.consumo);
		cadastroConsumoFlorestal.setDae(this.dae);
		cadastroConsumoFlorestal.setDataCadastro(this.dataCadastro);
		cadastroConsumoFlorestal.setDataFinalValidade(this.dataFinalValidade);
		cadastroConsumoFlorestal.setDataInicioValidade(this.dataInicioValidade);
		cadastroConsumoFlorestal.setDocumento(this.documento);
		cadastroConsumoFlorestal.setEndereco(this.endereco);
		cadastroConsumoFlorestal.setId(this.id);
		cadastroConsumoFlorestal.setInscricaoEstadual(this.inscricaoEstadual);
		cadastroConsumoFlorestal.setInteressado(this.interessado);
		cadastroConsumoFlorestal.setIsento(this.isento);
		cadastroConsumoFlorestal.setListAcoesAtividadesGeral(this.listAcoesAtividadesGeral);
		cadastroConsumoFlorestal.setListAtividadeConsumoFlorestal(this.listAtividadeConsumoFlorestal);
		cadastroConsumoFlorestal.setStatusCadConsumoFlorestal(this.statusCadConsumoFlorestal);
		cadastroConsumoFlorestal.setTipoSistemaAcao(this.tipoSistemaAcao);
		return cadastroConsumoFlorestal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CadastroConsumoFlorestal other = (CadastroConsumoFlorestal) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}