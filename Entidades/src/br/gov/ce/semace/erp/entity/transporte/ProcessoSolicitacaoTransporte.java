package br.gov.ce.semace.erp.entity.transporte;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "transporte", name = "processo_solicitacao_transporte")
public class ProcessoSolicitacaoTransporte implements Serializable{

	private static final long serialVersionUID = 8102818384236719765L;

	@Id
	@SequenceGenerator(sequenceName = "transporte.seq_processo_solicitacao_transporte", name = "SEQ_PROCESSO_SOLICITACAO_TRANSPORTE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PROCESSO_SOLICITACAO_TRANSPORTE")
	private Long id;

	@Column(name="spu", columnDefinition="text")
	private String spu = new String();
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="solicitacao_transporte_id")
	private SolicitacaoTransporte solicitacaoTransporte;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		ProcessoSolicitacaoTransporte other = (ProcessoSolicitacaoTransporte) obj;
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}


	//================================== GETERS and SETERS ====================================//

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public SolicitacaoTransporte getSolicitacaoTransporte() {
		return solicitacaoTransporte;
	}

	public void setSolicitacaoTransporte(SolicitacaoTransporte solicitacaoTransporte) {
		this.solicitacaoTransporte = solicitacaoTransporte;
	}

}

