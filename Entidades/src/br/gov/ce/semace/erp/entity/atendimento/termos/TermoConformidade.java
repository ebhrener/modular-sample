package br.gov.ce.semace.erp.entity.atendimento.termos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.EmpreendimentoAtividade;
import br.gov.ce.semace.erp.entity.geral.Interessado;

@Entity
@Audited
@Table(name="termo_conformidade", schema="atendimento")
public class TermoConformidade implements Serializable{

	private static final long serialVersionUID = 77162842126629714L;
	
	@Id @SequenceGenerator(sequenceName="atendimento.seq_termo_conformidade", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="interessado_id")
	private Interessado interessado;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="empreendimento_atividade_id")
	private EmpreendimentoAtividade empreendimentoAtividade;
	
	@Column(name="nome_interessado")
	private String nomeInteressado;
	
	@Column(name="documento_interessado")
	private String documentoInteressado;
	
	@Column(name="descricao_atividade")
	private String descricaoAtividade;
	
	@Column(name="descricacao_empreendimento")
	private String descricaoEmpreendimento;
	
	@Column(name="quantidade_informada")
	private Double quantidadeInformada;
	
	@Column(name="unidade_medida")
	private String unidadeMedida;
	
	@Column(name="data_cadastro")
	@Temporal(TemporalType.DATE)
	private Date dataCadastro;
	
	@Column(name="hash_validacao_termo")
	private String hashValidacaoTermo;
	
	@Column(name="id_validacao_termo")
	private String idValidacaoTermo;
	
	private Boolean ativo;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Interessado getInteressado() {
		return interessado;
	}
	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}
	public EmpreendimentoAtividade getEmpreendimentoAtividade() {
		return empreendimentoAtividade;
	}
	public void setEmpreendimentoAtividade(
			EmpreendimentoAtividade empreendimentoAtividade) {
		this.empreendimentoAtividade = empreendimentoAtividade;
	}
	public String getNomeInteressado() {
		return nomeInteressado;
	}
	public void setNomeInteressado(String nomeInteressado) {
		this.nomeInteressado = nomeInteressado;
	}
	public String getDescricaoAtividade() {
		return descricaoAtividade;
	}
	public void setDescricaoAtividade(String descricaoAtividade) {
		this.descricaoAtividade = descricaoAtividade;
	}
	public String getDescricaoEmpreendimento() {
		return descricaoEmpreendimento;
	}
	public void setDescricaoEmpreendimento(String descricaoEmpreendimento) {
		this.descricaoEmpreendimento = descricaoEmpreendimento;
	}
	public Double getQuantidadeInformada() {
		return quantidadeInformada;
	}
	public void setQuantidadeInformada(Double quantidadeInformada) {
		this.quantidadeInformada = quantidadeInformada;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public String getHashValidacaoTermo() {
		return hashValidacaoTermo;
	}
	public void setHashValidacaoTermo(String hashValidacaoTermo) {
		this.hashValidacaoTermo = hashValidacaoTermo;
	}
	public String getIdValidacaoTermo() {
		return idValidacaoTermo;
	}
	public void setIdValidacaoTermo(String idValidacaoTermo) {
		this.idValidacaoTermo = idValidacaoTermo;
	}
	public String getDocumentoInteressado() {
		return documentoInteressado;
	}
	public void setDocumentoInteressado(String documentoInteressado) {
		this.documentoInteressado = documentoInteressado;
	}
	public String getUnidadeMedida() {
		return unidadeMedida;
	}
	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
