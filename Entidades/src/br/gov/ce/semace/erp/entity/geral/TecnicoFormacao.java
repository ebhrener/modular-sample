package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;


@Entity
@Audited
@Table(name="tecnico_formacao", schema="geral")
public class TecnicoFormacao implements Serializable{

	private static final long serialVersionUID = 183626563109712468L;
	
	@Id @SequenceGenerator(sequenceName="geral.seq_tecnico_formacao", name="SEQTECNICO_FORMACAO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQTECNICO_FORMACAO")
	private Long id;
	
	@Column(name="numero_registro")
	private String numeroRegistro;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tecnico_id")
	private Tecnico tecnico = new Tecnico();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="formacao_profissional")
	private FormacaoProfissional formacaoProfissional = new FormacaoProfissional();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="orgao_registro")
	private OrgaoRegistro orgaoRegistro = new OrgaoRegistro();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	public Tecnico getTecnico() {
		return tecnico;
	}

	public void setTecnico(Tecnico tecnico) {
		this.tecnico = tecnico;
	}

	public FormacaoProfissional getFormacaoProfissional() {
		return formacaoProfissional;
	}

	public void setFormacaoProfissional(FormacaoProfissional formacaoProfissional) {
		this.formacaoProfissional = formacaoProfissional;
	}

	public OrgaoRegistro getOrgaoRegistro() {
		return orgaoRegistro;
	}

	public void setOrgaoRegistro(OrgaoRegistro orgaoRegistro) {
		this.orgaoRegistro = orgaoRegistro;
	}
	
	

}
