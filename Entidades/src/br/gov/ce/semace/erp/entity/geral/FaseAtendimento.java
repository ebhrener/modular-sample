package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.StatusAtendimento;
import br.gov.ce.semace.erp.enuns.StatusRequerimento;

@Entity
@Table(name="fase_atendimento", schema="geral")
@Audited
public class FaseAtendimento implements Serializable, Comparable<FaseAtendimento> {

	/**
	 *
	 */
	private static final long serialVersionUID = 8829152966501578057L;


	@Id
	@SequenceGenerator(sequenceName="geral.seq_fase_atendimento", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parametro_id")
	private Parametro parametro;

	@Enumerated(EnumType.STRING)
	private StatusAtendimento status;

	private Integer ordem;

	@Column(name="inicio_fluxo")
	private Boolean inicioFluxo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Parametro getParametro() {
		return parametro;
	}

	public void setParametro(Parametro parametro) {
		this.parametro = parametro;
	}

	public StatusAtendimento getStatus() {
		return status;
	}

	public void setStatus(StatusAtendimento status) {
		this.status = status;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	@Override
	public int compareTo(FaseAtendimento o) {
		return this.ordem.compareTo(o.getOrdem());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FaseAtendimento other = (FaseAtendimento) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public StatusRequerimento getStatusRequerimento(StatusAtendimento status){
		StatusRequerimento req = null;
		switch (status) {
		case AGENDADO:
			req = StatusRequerimento.AGENDADO;
			break;
		case AGUARDANDO_VALIDACAO_DOCUMENTO:
		case AGUARDANDO_SHAPE:
			req = StatusRequerimento.VALIDACAO_DOCUMENTO_SHAPE;
			break;
		case ATENDIDO:
			req = StatusRequerimento.ATENDIDO;
			break;
		case ATENDIMENTO_COM_PENDENCIA:
			req = StatusRequerimento.VALIDACAO_CONCLUIDA_PENDENCIA;
			break;
		case ATENDIMENTO_INICIADO:
			req = StatusRequerimento.AGENDADO;
			break;
		case CANCELADO:
			req = StatusRequerimento.CANCELADO;
			break;
		case EM_ATENDIMENTO:
			req = StatusRequerimento.AGENDADO;
			break;
		case EXCLUIDO:
			req = StatusRequerimento.EXCLUIDO;
			break;
		case GERAR_DAE:
			req = StatusRequerimento.GERACAO_BOLETO;
			break;
		case GERAR_PROCESSO_PROVISORIO:
			req = StatusRequerimento.VALIDACAO_PREVIA;
			break;
		case TODOS:
			req = StatusRequerimento.AGENDADO;
			break;
		case VALIDACAO_DOCUMENTO_SHAPE:
			req = StatusRequerimento.VALIDACAO_DOCUMENTO_SHAPE;
			break;
		case TRIAGEM:
			req = StatusRequerimento.ANEXAR_DOCUMENTOS;
			break;
		default:
			break;
		}

		return req;
	}


}
