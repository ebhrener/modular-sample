package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.geral.Endereco;
import br.gov.ce.semace.erp.entity.geral.Pessoa;
import br.gov.ce.semace.erp.entity.geral.Sede;
import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.entity.seguranca.Usuario;
import br.gov.ce.semace.erp.entity.seguranca.UsuarioWeb;
import br.gov.ce.semace.erp.enuns.Letra;
import br.gov.ce.semace.erp.enuns.StatusDae;

@Entity
@Audited
@Table(schema = "financeiro", name = "dae")
public class Dae implements Serializable, Comparable<Dae> {

	private static final long serialVersionUID = 2410291000990742384L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_boleto_erp", name = "SEQ_BOLETO_ERP", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_BOLETO_ERP")
	private Long id;

	private String identificador;

	@Column(name = "codigo_barra")
	private String codigoBarra;

	@Column(name = "periodo")
	private String periodo;

	@Column(name = "numero_parcela")
	private Integer numeroParcela;

	@Column(name = "codigo_produto")
	private Integer codigoProduto;

	@Column(name = "codigo_receita")
	private Integer codigoReceita;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_vencimento")
	private Date dataVencimento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_documento")
	private Date dataDocumento = new Date();

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_pagamento")
	private Date dataPagamento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_processamento")
	private Date dataProcessamento;

	//valor com correção monetária
	@Column(name = "valor_principal")
	private BigDecimal valorPrincipal;

	//soma dos valores: (principal + juros + multa) - desconto
	@Column(name = "valor_documento")
	private BigDecimal valorDocumento;

	@Column(name = "valor_desconto")
	private BigDecimal valorDesconto;

	@Column(name = "valor_multa")
	private BigDecimal valorMulta;

	@Column(name = "valor_juros")
	private BigDecimal valorJuros;

	@Column(name = "valor_pago")
	private BigDecimal valorPago;

	//valor inicial
	@Column(name = "valor_base")
	private BigDecimal valorBase;

	@Column(name = "status_dae")
	private StatusDae statusDae;

	@Column(columnDefinition="text")
	private String observacao;

	private Letra letra;

	@Column(precision=6, scale=2)
	private BigDecimal distancia;

	@Column(name="quantidade_tecnico")
	private Integer qtdTecnico;

	@Column(name="horas_trabalhadas")
	private Integer horasTrabalhadas;

	private String logradouro;

	private String bairro;

	private String cep;

	@Column(name = "numero_endereco")
	private String numeroEndereco;

	private String complemento;

	@Column(name="tipo_processo")
	private String tipoProcesso;

	@Column(name="valor_original_licenca")
	private BigDecimal valorOriginalLicenca;

	@Column(name="spu_anterior")
	private String spuAnterior;

	@Column(name="segunda_via")
	private boolean segundaVia = false;

	@Column(name="quantidade_veiculo")
	private Integer qtdVeiculo;

	@Column(name="valor_ressarcido")
	private BigDecimal valorRessarcido;

	@Column(name="justificativa_ressarcimento", columnDefinition = "text")
	private String justificativaRessarcimento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_ressarcimento")
	private Date dataRessarcimento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcionario_ressarcimento")
	private Funcionario funcionarioRessarcimento;

	/**
	 * Relacionamentos
	 */

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cidade_id")
	private Cidade cidade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="atividade_id")
	private Atividade atividade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="pessoa_id")
	private Pessoa pessoa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="sede_id")
	private Sede sede;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_id")
	private TipoTaxa tipoTaxa;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="solicitacao_dae_id")
	private SolicitacaoDae solicitacaoDae;

	@Column(name="justificativa_exclusao", columnDefinition = "text")
	private String justificativa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_externo_id")
	private UsuarioWeb usuarioExterno;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_interno_id")
	private Usuario usuarioInterno;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dae_sincronia_id")
	private DaeSincronia daeSincronia;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "endereco_interessado_id")
	private Endereco enderecoInteressado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="documento_id")
	private Documento documento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="config_pagamento_paralizacao_id")
	private ConfiguracaoPagamentoParalizacao configuracaoPagamentoParalizacao;

	@Column(name="pago_pos_vencimento")
	private boolean pagoPosVencimento;

	/**
	 * @Transient
	 */
	@Transient
	private boolean paralizouAtividades;

	@Transient
	private String imagemCodigoBarra;

	@Transient
	private Endereco endereco;

	@Transient
	private Date dataInicial;

	@Transient
	private Date dataFinal;

	@Transient
	private boolean hasHonorarios = false;

	@Transient
	private boolean unidadeConservacao = false;

	@Transient
	private Date dataInicialPagamento;

	@Transient
	private Date dataFinalPagamento;

	@Transient
	private List<String> listaTipoTaxa = new ArrayList<>();

	/**
	 *  @author Antonio Menezes de Araújo [antonio.menezes] 04/04/2016
	 *
	 * Campo criado para possibilitar a busca  de daes por
	 * Tipo de Receita. Como o vínculo acontece por meio de um campo
	 * inteiro, necessitou-se criar esse campo para possibilitar a busca.
	 */
	@Transient
	private TipoReceita tipoReceita = new TipoReceita();

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDae} é
	 * 	do tipo {@link StatusDae#EM_ABERTO}.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:45:43
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDae} seja
	 * 	{@link StatusDae#EM_ABERTO}. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDae#isEmAberto()
	 */
	@Transient
    public boolean isDaeEmAberto() {
		return this.statusDae.isEmAberto();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDae} é
	 * 	do tipo {@link StatusDae#PAGO}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 17:51:12
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDae} seja
	 * 	 {@link StatusDae#PAGO}. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDae#isPago()
	 */
	@Transient
    public boolean isDaePago() {
		return this.statusDae.isPago();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDae} é
	 * 	do tipo {@link StatusDae#PAGO_VENCIDO}
	 *
	 * @author Joerlan Oliveira Lima [joerlan.joe@gmail.com]
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDae} seja
	 * 	 {@link StatusDae#PAGO_VENCIDO}. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDae#isDaePagoAposVencimento()
	 */
	@Transient
    public boolean isDaePagoAposVencimento() {
		return this.statusDae.isPagoVencido();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDae} é
	 * 	do tipo {@link StatusDae#ERRO_PROCESSAMENTO }
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 18:05:05
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDae} seja
	 * 	 {@link StatusDae#ERRO_PROCESSAMENTO }. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDae#isErroProcessamento()
	 */
	@Transient
    public boolean isDaeErroProcessamento() {
		return this.statusDae.isErroProcessamento();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDae} é
	 * 	do tipo {@link StatusDae#VENCIDO}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 18:04:44
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDae} seja
	 * 	 {@link StatusDae#VENCIDO }. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDae#isVencido()
	 */
	@Transient
    public boolean isDaeVencido() {
		return this.statusDae.isVencido();
    }

	@Transient
    public boolean isAguardandoConfirmacaoPagamento() {
		return this.statusDae.isAguardandoConfirmacaoPagamento();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDae} é
	 * 	do tipo {@link StatusDae#EXCLUIDO }
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/10/2013 18:04:41
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDae} seja
	 * 	 {@link StatusDae#EXCLUIDO}. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDae#isExcluido()
	 */
	@Transient
    public boolean isDaeExcluido() {
		return this.statusDae.isExcluido();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDae} é
	 * 	do tipo {@link StatusDae#AGUARDANDO_EMISSAO}
	 *
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 07/11/2013 15:05:06
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDae} seja
	 * 	 {@link StatusDae#AGUARDANDO_EMISSAO}. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDae#isAguardandoEmissao()
	 */
	@Transient
    public boolean isDaeAguardandoEmissao() {
		return this.statusDae.isAguardandoEmissao();
    }
	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDae} é
	 * 	do tipo {@link StatusDae#VINCULADO_VIA_DEPOSITO}
	 *
	 * @author Antônio Menezes [antoniomenezes20@gmail.com] - 02/09/2015 15:05:06
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDae} seja
	 * 	 {@link StatusDae#VINCULADO_VIA_BOLETO}. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDae#isDaeVinculadoDeposito()
	 */
	@Transient
    public boolean isDaeVinculadoDeposito() {
		return this.statusDae.isVinculadoViaDeposito();
    }

	@Override
	public int hashCode() {
		if(getId() != null){
			return getId().hashCode();
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Dae)) {
			return false;
		}
		if(((Dae)obj).getId() != null){
			return ((Dae)obj).getId().equals(getId());
		}

		return ((Dae)obj).getNumeroParcela().equals(getNumeroParcela());
	}

	@Override
	public int compareTo(Dae dae) {
		return this.getNumeroParcela().compareTo(dae.getNumeroParcela());
	}

	@Override
	public Dae clone() throws CloneNotSupportedException {
		Dae dae = new Dae();
		dae.setAtividade(this.atividade);
		dae.setBairro(this.bairro);
		dae.setCep(this.cep);
		dae.setCidade(this.cidade);
		dae.setCodigoBarra(this.codigoBarra);
		dae.setCodigoProduto(this.codigoProduto);
		dae.setCodigoReceita(this.codigoReceita);
		dae.setComplemento(this.complemento);
		dae.setDataDocumento(this.dataDocumento);
		dae.setDataPagamento(this.dataPagamento);
		dae.setDataProcessamento(this.dataProcessamento);
		dae.setDataVencimento(this.dataVencimento);
		dae.setDistancia(this.distancia);
		dae.setEndereco(this.endereco);
		dae.setEnderecoInteressado(this.enderecoInteressado);
		dae.setHorasTrabalhadas(this.horasTrabalhadas);
		dae.setIdentificador(this.identificador);
		dae.setImagemCodigoBarra(this.imagemCodigoBarra);
		dae.setJustificativa(this.justificativa);
		dae.setLetra(this.letra);
		dae.setLogradouro(this.logradouro);
		dae.setNumeroEndereco(this.numeroEndereco);
		dae.setNumeroParcela(this.numeroParcela);
		dae.setObservacao(this.observacao);
		dae.setPeriodo(this.periodo);
		dae.setPessoa(this.pessoa);
		dae.setQtdTecnico(this.qtdTecnico);
		dae.setQtdVeiculo(qtdVeiculo);
		dae.setSede(this.sede);
		dae.setSegundaVia(this.segundaVia);
		dae.setSolicitacaoDae(this.solicitacaoDae);
		dae.setSpuAnterior(this.spuAnterior);
		dae.setStatusDae(this.statusDae);
		dae.setTipoProcesso(this.tipoProcesso);
		dae.setTipoTaxa(this.tipoTaxa);
		dae.setUsuarioExterno(this.usuarioExterno);
		dae.setUsuarioInterno(this.usuarioInterno);
		dae.setValorDesconto(this.valorDesconto);
		dae.setValorDocumento(this.valorDocumento);
		dae.setValorJuros(this.valorJuros);
		dae.setValorMulta(this.valorMulta);
		dae.setValorOriginalLicenca(this.valorOriginalLicenca);
		dae.setValorPago(this.valorPago);
		dae.setValorPrincipal(this.valorPrincipal);
		dae.setValorBase(this.valorBase);
		dae.setValorRessarcido(valorRessarcido);
		dae.setJustificativaRessarcimento(justificativaRessarcimento);
		dae.setDataRessarcimento(dataRessarcimento);
		dae.setFuncionarioRessarcimento(funcionarioRessarcimento);


		return dae;
	}

	/* Métodos getters e setters */

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getCodigoBarra() {
		return codigoBarra;
	}

	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataDocumento() {
		return dataDocumento;
	}

	public void setDataDocumento(Date dataDocumento) {
		this.dataDocumento = dataDocumento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public Date getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public BigDecimal getValorDocumento() {
		return valorDocumento;
	}

	public void setValorDocumento(BigDecimal valorDocumento) {
		this.valorDocumento = valorDocumento;
	}

	public BigDecimal getValorPago() {
		return valorPago;
	}

	public void setValorPago(BigDecimal valorPago) {
		this.valorPago = valorPago;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

	public StatusDae getStatusDae() {
		return statusDae;
	}

	public void setStatusDae(StatusDae statusDae) {
		this.statusDae = statusDae;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Letra getLetra() {
		return letra;
	}

	public void setLetra(Letra letra) {
		this.letra = letra;
	}

	public BigDecimal getDistancia() {
		return distancia;
	}

	public void setDistancia(BigDecimal distancia) {
		this.distancia = distancia;
	}

	public Integer getQtdTecnico() {
		return qtdTecnico;
	}

	public void setQtdTecnico(Integer qtdTecnico) {
		this.qtdTecnico = qtdTecnico;
	}

	public Integer getHorasTrabalhadas() {
		return horasTrabalhadas;
	}

	public void setHorasTrabalhadas(Integer horasTrabalhadas) {
		this.horasTrabalhadas = horasTrabalhadas;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getNumeroEndereco() {
		return numeroEndereco;
	}

	public void setNumeroEndereco(String numeroEndereco) {
		this.numeroEndereco = numeroEndereco;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public String getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(String tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public BigDecimal getValorOriginalLicenca() {
		return valorOriginalLicenca;
	}

	public void setValorOriginalLicenca(BigDecimal valorOriginalLicenca) {
		this.valorOriginalLicenca = valorOriginalLicenca;
	}

	public TipoTaxa getTipoTaxa() {
		return tipoTaxa;
	}

	public void setTipoTaxa(TipoTaxa tipoTaxa) {
		this.tipoTaxa = tipoTaxa;
	}

	public SolicitacaoDae getSolicitacaoDae() {
		return solicitacaoDae;
	}

	public void setSolicitacaoDae(SolicitacaoDae solicitacaoDae) {
		this.solicitacaoDae = solicitacaoDae;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public Integer getNumeroParcela() {
		return numeroParcela;
	}

	public void setNumeroParcela(Integer numeroParcela) {
		this.numeroParcela = numeroParcela;
	}

	public Integer getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(Integer codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public Integer getCodigoReceita() {
		return codigoReceita;
	}

	public void setCodigoReceita(Integer codigoReceita) {
		this.codigoReceita = codigoReceita;
	}

	public BigDecimal getValorPrincipal() {
		return valorPrincipal;
	}

	public void setValorPrincipal(BigDecimal valorPrincipal) {
		this.valorPrincipal = valorPrincipal;
	}

	public BigDecimal getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public BigDecimal getValorMulta() {
		return valorMulta;
	}

	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}

	public BigDecimal getValorJuros() {
		return valorJuros;
	}

	public void setValorJuros(BigDecimal valorJuros) {
		this.valorJuros = valorJuros;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getImagemCodigoBarra() {
		return imagemCodigoBarra;
	}

	public void setImagemCodigoBarra(String imagemCodigoBarra) {
		this.imagemCodigoBarra = imagemCodigoBarra;
	}

	public UsuarioWeb getUsuarioExterno() {
		return usuarioExterno;
	}

	public void setUsuarioExterno(UsuarioWeb usuarioExterno) {
		this.usuarioExterno = usuarioExterno;
	}

	public Usuario getUsuarioInterno() {
		return usuarioInterno;
	}

	public void setUsuarioInterno(Usuario usuarioInterno) {
		this.usuarioInterno = usuarioInterno;
	}

	public DaeSincronia getDaeSincronia() {
		return daeSincronia;
	}

	public void setDaeSincronia(DaeSincronia daeSincronia) {
		this.daeSincronia = daeSincronia;
	}

	public Endereco getEnderecoInteressado() {
		return enderecoInteressado;
	}

	public void setEnderecoInteressado(Endereco enderecoInteressado) {
		this.enderecoInteressado = enderecoInteressado;
	}

	public String getSpuAnterior() {
		return spuAnterior;
	}

	public void setSpuAnterior(String spuAnterior) {
		this.spuAnterior = spuAnterior;
	}

	public boolean isSegundaVia() {
		return segundaVia;
	}

	public void setSegundaVia(boolean segundaVia) {
		this.segundaVia = segundaVia;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public BigDecimal getValorBase() {
		return valorBase;
	}

	public void setValorBase(BigDecimal valorBase) {
		this.valorBase = valorBase;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Integer getQtdVeiculo() {
		return qtdVeiculo;
	}

	public void setQtdVeiculo(Integer qtdVeiculo) {
		this.qtdVeiculo = qtdVeiculo;
	}

	public boolean isHasHonorarios() {
		return hasHonorarios;
	}

	public void setHasHonorarios(boolean hasHonorarios) {
		this.hasHonorarios = hasHonorarios;
	}

	public TipoReceita getTipoReceita() {
		return tipoReceita;
	}

	public void setTipoReceita(TipoReceita tipoReceita) {
		this.tipoReceita = tipoReceita;
	}

	public boolean isParcela(){
		return observacao.contains("Parcela") || observacao.contains("TCPCD");
	}

	public boolean isUnidadeConservacao() {
		return unidadeConservacao;
	}

	public void setUnidadeConservacao(boolean unidadeConservacao) {
		this.unidadeConservacao = unidadeConservacao;
	}

	public Date getDataInicialPagamento() {
		return dataInicialPagamento;
	}

	public void setDataInicialPagamento(Date dataInicialPagamento) {
		this.dataInicialPagamento = dataInicialPagamento;
	}

	public Date getDataFinalPagamento() {
		return dataFinalPagamento;
	}

	public void setDataFinalPagamento(Date dataFinalPagamento) {
		this.dataFinalPagamento = dataFinalPagamento;
	}

	public List<String> getListaTipoTaxa() {
		return listaTipoTaxa;
	}

	public void setListaTipoTaxa(List<String> listaTipoTaxa) {
		this.listaTipoTaxa = listaTipoTaxa;
	}

	public boolean getParalizouAtividades() {
		return paralizouAtividades;
	}

	public void setParalizouAtividades(boolean paralizouAtividades) {
		this.paralizouAtividades = paralizouAtividades;
	}

	public ConfiguracaoPagamentoParalizacao getConfiguracaoPagamentoParalizacao() {
		return configuracaoPagamentoParalizacao;
	}

	public void setConfiguracaoPagamentoParalizacao(ConfiguracaoPagamentoParalizacao configuracaoPagamentoParalizacao) {
		this.configuracaoPagamentoParalizacao = configuracaoPagamentoParalizacao;
	}

	public boolean isPagoPosVencimento() {
		return pagoPosVencimento;
	}

	public void setPagoPosVencimento(boolean pagoPosVencimento) {
		this.pagoPosVencimento = pagoPosVencimento;
	}

	public BigDecimal getValorRessarcido() {
		return valorRessarcido;
	}

	public void setValorRessarcido(BigDecimal valorRessarcido) {
		this.valorRessarcido = valorRessarcido;
	}

	public String getJustificativaRessarcimento() {
		return justificativaRessarcimento;
	}

	public void setJustificativaRessarcimento(String justificativaRessarcimento) {
		this.justificativaRessarcimento = justificativaRessarcimento;
	}

	public Date getDataRessarcimento() {
		return dataRessarcimento;
	}

	public void setDataRessarcimento(Date dataRessarcimento) {
		this.dataRessarcimento = dataRessarcimento;
	}

	public Funcionario getFuncionarioRessarcimento() {
		return funcionarioRessarcimento;
	}

	public void setFuncionarioRessarcimento(Funcionario usuario) {
		this.funcionarioRessarcimento = usuario;
	}

}