package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.TipoDocumento;
import br.gov.ce.semace.erp.utils.DateUtils;

@Entity
@Audited
@Table(schema = "geral", name="documento", uniqueConstraints=@UniqueConstraint(columnNames={"numero_documento","tipo_documento"}))
public class Documento implements Serializable{

	private static final long serialVersionUID = -9212883160682960993L;

	@Id @SequenceGenerator(initialValue = 1, allocationSize = 1, name="SEQ", sequenceName="geral.seq_documento")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Temporal(TemporalType.DATE)
	@Column(name="data_criacao")
	private Date dataCriacao;

	@Temporal(TemporalType.TIME)
	@Column(name="hora_criacao")
	private Date horaCriacao;

	@Column(name="numero_documento")
	private String numeroDocumento;

	@Column(name="ip_solicitante")
	private String ipSolicitante;

	private Integer sequencial;

	@Column(name="tipo_documento")
	private TipoDocumento tipoDocumento;

	private Boolean usado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable  = true, name="funcionario_id", referencedColumnName="pessoa_id")
	private Funcionario funcionario = new Funcionario();

	@Column(name = "id_documento_pdf_gerado")
	public Long idDocumentoPdfGerado;

	/**
	 * Objeto para ser utilizado nos campos de consulta.
	 */
	@Transient private List<String> listFuncionariosId;

	/**
	 * Informa se é possível assinar o {@link Documento}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 03/11/2014 09:32:09
	 */
	@Column(name = "permitido_assinar")
	private boolean permitidoAssinar = true;

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoDocumento} é
	 * 	do tipo <b>PROCESSO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 14:16:46
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoDocumento} seja
	 * 	<b>PROCESSO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoDocumento#isTipoProcesso()
	 */
	@Transient
    public boolean isTipoProcesso() {
		return this.tipoDocumento.isTipoProcesso();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoDocumento} é
	 * 	do tipo <b>OFICIO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 14:17:18
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoDocumento} seja
	 * 	<b>OFICIO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoDocumento#isTipoProcesso()
	 */
	@Transient
    public boolean isTipoOficio() {
		return this.tipoDocumento.isTipoOficio();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoDocumento} é
	 * 	do tipo <b>DOC_AVULSO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 14:19:15
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoDocumento} seja
	 * 	<b>DOC_AVULSO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoDocumento#isTipoDocAvulso()
	 */
	@Transient
    public boolean isTipoDocAvulso() {
		return this.tipoDocumento.isTipoDocAvulso();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoDocumento} é
	 * 	do tipo <b>DOC_ANEXO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 15:03:12
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoDocumento} seja
	 * 	<b>DOC_ANEXO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoDocumento#isTipoDocAnexo()
	 */
	@Transient
    public boolean isTipoDocAnexo() {
		return this.tipoDocumento.isTipoDocAnexo();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoDocumento} é
	 * 	do tipo <b>CI</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 15:05:33
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoDocumento} seja
	 * 	<b>CI</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoDocumento#isTipoCI()
	 */
	@Transient
    public boolean isTipoCI() {
		return this.tipoDocumento.isTipoCI();
    }

	/**
	 * Fornece os campos {@link #dataCriacao} e {@link #horaCriacao} formatados.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jul 22, 2013 1:26:01 PM
	 *
	 * @return retorna uma {@link String} com de acordo com o padrão {@link DateUtils#BRAZIL_FORMAT} para a {@link #dataCriacao} e
	 * {@link DateUtils#BRAZIL_SHORT_TIME_FORMAT} para a {@link #horaCriacao}, concatenados e separados por um espaço em branco.
	 */
	@Transient
	public String getDataHoraFormatada(){
		// formata a data
		String dataFormatada = DateUtils.toString(this.dataCriacao);
		// formata o tempo
		String horaFormatada = DateUtils.getShortTime(this.horaCriacao);

		// concatena e separa por espaços a string a ser retornada
		return dataFormatada + " às " + horaFormatada;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getIpSolicitante() {
		return ipSolicitante;
	}
	public void setIpSolicitante(String ipSolicitante) {
		this.ipSolicitante = ipSolicitante;
	}
	public Date getDataCriacao() {
		return dataCriacao;
	}
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	public Integer getSequencial() {
		return sequencial;
	}
	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}
	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public Boolean getUsado() {
		return usado;
	}
	public void setUsado(Boolean usado) {
		this.usado = usado;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public Date getHoraCriacao() {
		return horaCriacao;
	}
	public void setHoraCriacao(Date horaCriacao) {
		this.horaCriacao = horaCriacao;
	}

	public Long getIdDocumentoPdfGerado() {
		return idDocumentoPdfGerado;
	}

	public void setIdDocumentoPdfGerado(Long idDocumentoPdfGerado) {
		this.idDocumentoPdfGerado = idDocumentoPdfGerado;
	}

	public boolean isPermitidoAssinar() {
		return permitidoAssinar;
	}

	public void setPermitidoAssinar(boolean permitidoAssinar) {
		this.permitidoAssinar = permitidoAssinar;
	}

	public List<String> getListFuncionariosId() {
		return listFuncionariosId;
	}

	public void setListFuncionariosId(List<String> listFuncionariosId) {
		this.listFuncionariosId = listFuncionariosId;
	}
}