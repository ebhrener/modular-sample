package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.financeiro.Dae;

@Entity
@Audited
@Table(name="parcela_acordo_divida", schema="juridico")
public class ParcelaAcordoDivida implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_parcela_acordo_divida", name="juridico.seq_parcela_acordo_divida", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_parcela_acordo_divida")
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="acordo_divida_id")
	private AcordoDivida acordoDivida;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="dae_id")
	private Dae dae;

	@Column(name = "processada")
	private boolean processada;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AcordoDivida getAcordoDivida() {
		return acordoDivida;
	}

	public void setAcordoDivida(AcordoDivida acordoDivida) {
		this.acordoDivida = acordoDivida;
	}

	public Dae getDae() {
		return dae;
	}

	public void setDae(Dae dae) {
		this.dae = dae;
	}

	public boolean isProcessada() {
		return processada;
	}

	public void setProcessada(boolean processada) {
		this.processada = processada;
	}
}
