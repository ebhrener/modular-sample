package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.financeiro.ConfiguracaoPagamentoParalizacao;
import br.gov.ce.semace.erp.entity.financeiro.TipoTaxa;
import br.gov.ce.semace.erp.entity.geral.Interessado;
import br.gov.ce.semace.erp.entity.geral.Questionario;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.SituacaoRama;

/**
 * @author antonio.menezes
 *
 */
@Entity
@Audited
@Table(name="configuracao_licenciamento", schema="licenciamento")
public class ConfiguracaoLicenciamento implements Serializable{

	private static final long serialVersionUID = -8615332760932826731L;

	@Id
	private Long id;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "configuracao_licenciamento_tipo_taxa",
	schema = "licenciamento",
	joinColumns = @JoinColumn(name = "configuracao_id"),
	inverseJoinColumns = @JoinColumn(name = "tipo_taxa_id"))
	private List<TipoTaxa> listTipoTaxaGeraRama = new ArrayList<TipoTaxa>();

	@Column(name="data_inicio_rama_online")
	private Date dataInicioRamaOnline;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo_taxa_rama")
	private TipoTaxa tipoTaxaRama;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_funcionario_gera_dae_rama")
	private Funcionario funcionarioGeraDaeRama;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_questionario_rama")
	private Questionario questionarioRama;

	@Column(name = "quantidade_questoes_paginas")
	private int quantidadeQuestoesPaginas;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "configuracao_consultor_rama",
	schema = "licenciamento",
	joinColumns = @JoinColumn(name = "configuracao_id"),
	inverseJoinColumns = @JoinColumn(name = "interessado_id"))
	private List<Interessado> listConsultor = new ArrayList<Interessado>();

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "configuracao_tecnico_rama",
	schema = "licenciamento",
	joinColumns = @JoinColumn(name = "configuracao_id"),
	inverseJoinColumns = @JoinColumn(name = "funcionario_id"))
	private List<Funcionario> listFuncionario = new ArrayList<Funcionario>();

	@ElementCollection(targetClass = SituacaoRama.class)
	@CollectionTable(name = "situacao_rama",schema="licenciamento",
	joinColumns = @JoinColumn(name = "configuracao_id"))
	@Enumerated(EnumType.ORDINAL)
	@Column(name="situacao_rama")
	private List<SituacaoRama> listSituacaoRama = new ArrayList<SituacaoRama>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "configuracaoLicenciamento", cascade = CascadeType.ALL)
	private List<ConfiguracaoAgendamentoRama> listConfiguracao = new ArrayList<ConfiguracaoAgendamentoRama>();

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="gerente_id")
	private Funcionario gerente;

	@Column(name="usar_apenas_coema_atual_calculo")
	private boolean usarApenasCoemaAtualCalculo = false;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "configuracaoLicenciamento", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ConfiguracaoPagamentoParalizacao> listConfiguracaoDescontoParalizacao = new ArrayList<ConfiguracaoPagamentoParalizacao>();

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "config_tipo_processo_siga_cobranca_paralisacao",
	schema = "licenciamento",
	joinColumns = @JoinColumn(name = "configuracao_id"),
	inverseJoinColumns = @JoinColumn(name = "tipo_processo_siga_id"))
	private List<TipoProcessoSiga> listTipoProcessoSigaCobrancaParalizacao = new ArrayList<>();

	@Column(name="isento_pag_apenas_dentro_periodo_abrang")
	private boolean isentoPagamentoApenasDentroPeriodoAbrangencia = false;

	@Column(name="digitar_valor_sem_calcular")
	private boolean digitarValorSemCalcular = false;

	@Column(name="gera_processo")
	private boolean geraProcesso = true;

	@Column(name="gera_relatorio_analise_natuur")
	private boolean geraRelatorioAnaliseNatuur = true;

	@Column(name="prazo_pagamento_primeira_via")
	private Integer prazoPagamentoPrimeiraVia;

	@Column(name="prazo_pagamento_segunda_via")
	private Integer prazoPagamentoSegundaVia;

	@Column(name="impedir_entrega_presencial_rama")
	private boolean impedirEntregaPresencialRama = false;

	@Column(name="permitir_criacao_requerimento_rama")
	private boolean permitirCriacaoRequerimentoRama = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<TipoTaxa> getListTipoTaxaGeraRama() {
		return listTipoTaxaGeraRama;
	}

	public void setListTipoTaxaGeraRama(List<TipoTaxa> listTipoTaxaGeraRama) {
		this.listTipoTaxaGeraRama = listTipoTaxaGeraRama;
	}

	public Date getDataInicioRamaOnline() {
		return dataInicioRamaOnline;
	}

	public void setDataInicioRamaOnline(Date dataInicioRamaOnline) {
		this.dataInicioRamaOnline = dataInicioRamaOnline;
	}

	public TipoTaxa getTipoTaxaRama() {
		return tipoTaxaRama;
	}

	public void setTipoTaxaRama(TipoTaxa tipoTaxaRama) {
		this.tipoTaxaRama = tipoTaxaRama;
	}

	public Funcionario getFuncionarioGeraDaeRama() {
		return funcionarioGeraDaeRama;
	}

	public void setFuncionarioGeraDaeRama(Funcionario funcionarioGeraDaeRama) {
		this.funcionarioGeraDaeRama = funcionarioGeraDaeRama;
	}

	public Questionario getQuestionarioRama() {
		return questionarioRama;
	}

	public void setQuestionarioRama(Questionario questionarioRama) {
		this.questionarioRama = questionarioRama;
	}

	public int getQuantidadeQuestoesPaginas() {
		return quantidadeQuestoesPaginas;
	}

	public void setQuantidadeQuestoesPaginas(int quantidadeQuestoesPaginas) {
		this.quantidadeQuestoesPaginas = quantidadeQuestoesPaginas;
	}

	public List<Interessado> getListConsultor() {
		return listConsultor;
	}

	public void setListConsultor(List<Interessado> listConsultor) {
		this.listConsultor = listConsultor;
	}

	public List<Funcionario> getListFuncionario() {
		return listFuncionario;
	}

	public void setListFuncionario(List<Funcionario> listFuncionario) {
		this.listFuncionario = listFuncionario;
	}

	public List<SituacaoRama> getListSituacaoRama() {
		return listSituacaoRama;
	}

	public void setListSituacaoRama(List<SituacaoRama> listSituacaoRama) {
		this.listSituacaoRama = listSituacaoRama;
	}

	public Funcionario getGerente() {
		return gerente;
	}

	public void setGerente(Funcionario gerente) {
		this.gerente = gerente;
	}

	public List<ConfiguracaoAgendamentoRama> getListConfiguracao() {
		return listConfiguracao;
	}

	public void setListConfiguracao(List<ConfiguracaoAgendamentoRama> listConfiguracao) {
		this.listConfiguracao = listConfiguracao;
	}

	public boolean isUsarApenasCoemaAtualCalculo() {
		return usarApenasCoemaAtualCalculo;
	}

	public void setUsarApenasCoemaAtualCalculo(boolean usarApenasCoemaAtualCalculo) {
		this.usarApenasCoemaAtualCalculo = usarApenasCoemaAtualCalculo;
	}


	public List<ConfiguracaoPagamentoParalizacao> getListConfiguracaoDescontoParalizacao() {
		return listConfiguracaoDescontoParalizacao;
	}

	public void setListConfiguracaoDescontoParalizacao(List<ConfiguracaoPagamentoParalizacao> listConfiguracaoDescontoParalizacao) {
		this.listConfiguracaoDescontoParalizacao = listConfiguracaoDescontoParalizacao;
	}

	public List<TipoProcessoSiga> getListTipoProcessoSigaCobrancaParalizacao() {
		return listTipoProcessoSigaCobrancaParalizacao;
	}

	public void setListTipoProcessoSigaCobrancaParalizacao(List<TipoProcessoSiga> listTipoProcessoSigaCobrancaParalizacao) {
		this.listTipoProcessoSigaCobrancaParalizacao = listTipoProcessoSigaCobrancaParalizacao;
	}

	public boolean isIsentoPagamentoApenasDentroPeriodoAbrangencia() {
		return isentoPagamentoApenasDentroPeriodoAbrangencia;
	}

	public void setIsentoPagamentoApenasDentroPeriodoAbrangencia(boolean isentoPagamentoApenasDentroPeriodoAbrangencia) {
		this.isentoPagamentoApenasDentroPeriodoAbrangencia = isentoPagamentoApenasDentroPeriodoAbrangencia;
	}

	public boolean isDigitarValorSemCalcular() {
		return digitarValorSemCalcular;
	}

	public void setDigitarValorSemCalcular(boolean digitarValorSemCalcular) {
		this.digitarValorSemCalcular = digitarValorSemCalcular;
	}

	public Integer getPrazoPagamentoPrimeiraVia() {
		return prazoPagamentoPrimeiraVia;
	}

	public void setPrazoPagamentoPrimeiraVia(Integer prazoPagamentoPrimeiraVia) {
		this.prazoPagamentoPrimeiraVia = prazoPagamentoPrimeiraVia;
	}

	public Integer getPrazoPagamentoSegundaVia() {
		return prazoPagamentoSegundaVia;
	}

	public void setPrazoPagamentoSegundaVia(Integer prazoPagamentoSegundaVia) {
		this.prazoPagamentoSegundaVia = prazoPagamentoSegundaVia;
	}

	public ConfiguracaoPagamentoParalizacao getConfiguracaoPagamentoParalizacaoAtivo(){
		if(this.getListConfiguracaoDescontoParalizacao() != null){
			for(ConfiguracaoPagamentoParalizacao cpp: this.getListConfiguracaoDescontoParalizacao()){
				if(cpp.isAtivo()){
					return cpp;
				}
			}
		}
		return null;
	}

	public boolean isGeraProcesso() {
		return geraProcesso;
	}

	public void setGeraProcesso(boolean geraProcesso) {
		this.geraProcesso = geraProcesso;
	}

	public boolean isGeraRelatorioAnaliseNatuur() {
		return geraRelatorioAnaliseNatuur;
	}

	public void setGeraRelatorioAnaliseNatuur(boolean geraRelatorioAnaliseNatuur) {
		this.geraRelatorioAnaliseNatuur = geraRelatorioAnaliseNatuur;
	}

	public boolean isImpedirEntregaPresencialRama() {
		return impedirEntregaPresencialRama;
	}

	public void setImpedirEntregaPresencialRama(boolean impedirEntregaPresencialRama) {
		this.impedirEntregaPresencialRama = impedirEntregaPresencialRama;
	}

	public boolean isPermitirCriacaoRequerimentoRama() {
		return permitirCriacaoRequerimentoRama;
	}

	public void setPermitirCriacaoRequerimentoRama(boolean permitirCriacaoRequerimentoRama) {
		this.permitirCriacaoRequerimentoRama = permitirCriacaoRequerimentoRama;
	}
}
