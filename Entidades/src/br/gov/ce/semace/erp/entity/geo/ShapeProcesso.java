package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "shape_processo")
public class ShapeProcesso implements Serializable {

	private static final long serialVersionUID = 5667725574944671187L;

	@Id
	@SequenceGenerator(sequenceName="seq_shape_processo", name="seq_shape_processo", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_shape_processo")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="processo_id")
	private ProcessoGeo processo = new ProcessoGeo();

	@Column(nullable = false, unique = true, length = 100)
	private String tabela;

	@Column(nullable = false)
	private Boolean ativo = Boolean.TRUE;

	@Column(name = "latitude_maxima")
	private Double latitudeMaxima;

	@Column(name = "latitude_minima")
	private Double latitudeMinima;

	@Column(name = "longitude_maxima")
	private Double longitudeMaxima;

	@Column(name = "longitude_minima")
	private Double longitudeMinima;

	@Deprecated
	@Column(name = "atividade_id")
	private Long idAtividade;

	@OneToMany(mappedBy="shapeProcesso", fetch=FetchType.LAZY)
	private List<HistoricoShapeProcesso> historico;

	@Column(name= "raci_id")
	private Long raciID;

	public ShapeProcesso() {
		super();
	}

	public ShapeProcesso(Long idShapeProcesso) {
		super();
		id = idShapeProcesso;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProcessoGeo getProcesso() {
		return processo;
	}

	public void setProcesso(ProcessoGeo processo) {
		this.processo = processo;
	}

	public String getTabela() {
		return tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	public Double getLatitudeMaxima() {
		return latitudeMaxima;
	}

	public void setLatitudeMaxima(Double latitudeMaxima) {
		this.latitudeMaxima = latitudeMaxima;
	}

	public Double getLatitudeMinima() {
		return latitudeMinima;
	}

	public void setLatitudeMinima(Double latitudeMinima) {
		this.latitudeMinima = latitudeMinima;
	}

	public Double getLongitudeMaxima() {
		return longitudeMaxima;
	}

	public void setLongitudeMaxima(Double longitudeMaxima) {
		this.longitudeMaxima = longitudeMaxima;
	}

	public Double getLongitudeMinima() {
		return longitudeMinima;
	}

	public void setLongitudeMinima(Double longitudeMinima) {
		this.longitudeMinima = longitudeMinima;
	}

	@Deprecated
	public Long getIdAtividade() {
		return idAtividade;
	}

	@Deprecated
	public void setIdAtividade(Long idAtividade) {
		this.idAtividade = idAtividade;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ShapeProcesso other = (ShapeProcesso) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public List<HistoricoShapeProcesso> getHistorico() {
		return historico;
	}

	public void setHistorico(List<HistoricoShapeProcesso> historico) {
		this.historico = historico;
	}

	public Long getRaciID() {
		return raciID;
	}

	public void setRaciID(Long raciID) {
		this.raciID = raciID;
	}

}
