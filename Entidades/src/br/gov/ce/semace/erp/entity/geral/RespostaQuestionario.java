package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="resposta_questionario", schema="geral")
public class RespostaQuestionario implements Serializable, Comparable<RespostaQuestionario>{

	private static final long serialVersionUID = -7238305371983515896L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_resposta_questionario", name = "geral.seq_resposta_questionario", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geral.seq_resposta_questionario")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="questao_id")
	private Questao questao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="opcao_id")
	private Opcao opcao = new Opcao();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="documento_questionario_id")
	private DocumentoQuestionario documentoQuestionario;

	@Column(columnDefinition = "text", name="resposta_questao")
	private String respostaQuestao;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "respostaQuestionario",cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ImagemDocumentoQuestionario> imagensDocumentoQuestionario = new ArrayList<ImagemDocumentoQuestionario>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "respostaQuestionario",cascade = CascadeType.ALL, orphanRemoval = true)
	private List<MapaResposta> listMapaResposta = new ArrayList<MapaResposta>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "respostaQuestionario",cascade = CascadeType.ALL)
	private List<RespostaOpcao> listRespostaOpcao = new ArrayList<RespostaOpcao>();

	@Transient
	private MapaResposta mapaRespostaTemp;

	@Transient
	private List<RespostaOpcao> listRespostaOpcaoTemp = new ArrayList<RespostaOpcao>();

	public RespostaQuestionario clone() throws CloneNotSupportedException {
		RespostaQuestionario respostaQuestionario = new RespostaQuestionario();
		respostaQuestionario.setId(this.id);
		respostaQuestionario.setQuestao(this.questao);
		respostaQuestionario.setOpcao(this.opcao);
		respostaQuestionario.setDocumentoQuestionario(this.documentoQuestionario);
		respostaQuestionario.setRespostaQuestao(new String(this.respostaQuestao));
		return respostaQuestionario;
	}

	public RespostaQuestionario() {
		super();
	}

	public RespostaQuestionario(Questao questao, Opcao opcao, DocumentoQuestionario documentoQuestionario) {
		super();
		this.questao = questao;
		this.opcao = opcao;
		this.documentoQuestionario = documentoQuestionario;
	}

	public RespostaQuestionario(Questao questao, Opcao opcao, DocumentoQuestionario documentoQuestionario, String resposta) {
		super();
		this.questao = questao;
		this.opcao = opcao;
		this.documentoQuestionario = documentoQuestionario;
		this.respostaQuestao = resposta;
	}
	
	/** Métodos getters e setters */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Questao getQuestao() {
		return questao;
	}

	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	public Opcao getOpcao() {
		return opcao;
	}

	public void setOpcao(Opcao opcao) {
		this.opcao = opcao;
	}

	public DocumentoQuestionario getRespostaQuestionario() {
		return documentoQuestionario;
	}

	public void setRespostaQuestionario(DocumentoQuestionario documentoQuestionario) {
		this.documentoQuestionario = documentoQuestionario;
	}

	public String getRespostaQuestao() {
		return respostaQuestao;
	}

	public void setRespostaQuestao(String respostaQuestao) {
		this.respostaQuestao = respostaQuestao;
	}

	public DocumentoQuestionario getDocumentoQuestionario() {
		return documentoQuestionario;
	}

	public void setDocumentoQuestionario(DocumentoQuestionario documentoQuestionario) {
		this.documentoQuestionario = documentoQuestionario;
	}

	public List<ImagemDocumentoQuestionario> getImagensDocumentoQuestionario() {
		return imagensDocumentoQuestionario;
	}

	public void setImagensDocumentoQuestionario(
			List<ImagemDocumentoQuestionario> imagensDocumentoQuestionario) {
		this.imagensDocumentoQuestionario = imagensDocumentoQuestionario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RespostaQuestionario other = (RespostaQuestionario) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public List<MapaResposta> getListMapaResposta() {
		return listMapaResposta;
	}

	public void setListMapaResposta(List<MapaResposta> listMapaResposta) {
		this.listMapaResposta = listMapaResposta;
	}

	public MapaResposta getMapaRespostaTemp() {
		return mapaRespostaTemp;
	}

	public void setMapaRespostaTemp(MapaResposta mapaRespostaTemp) {
		this.mapaRespostaTemp = mapaRespostaTemp;
	}

	@Override
	public int compareTo(RespostaQuestionario o) {
		return questao.compareTo(o.getQuestao());
	}

	public List<RespostaOpcao> getListRespostaOpcaoTemp() {
		return listRespostaOpcaoTemp;
	}

	public void setListRespostaOpcaoTemp(List<RespostaOpcao> listRespostaOpcaoTemp) {
		this.listRespostaOpcaoTemp = listRespostaOpcaoTemp;
	}

	public List<RespostaOpcao> getListRespostaOpcao() {
		return listRespostaOpcao;
	}

	public void setListRespostaOpcao(List<RespostaOpcao> listRespostaOpcao) {
		this.listRespostaOpcao = listRespostaOpcao;
	}
}