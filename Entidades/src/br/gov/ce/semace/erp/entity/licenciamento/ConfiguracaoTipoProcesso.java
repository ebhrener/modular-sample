package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.financeiro.TipoTaxa;
import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.rh.Setor;
import br.gov.ce.semace.erp.utils.FormatUtils;

@Entity
@Audited
@Table(name="configuracao_tipo_processo", schema="atendimento")
public class ConfiguracaoTipoProcesso implements Serializable, Comparable<ConfiguracaoTipoProcesso> {

	private static final long serialVersionUID = 890319713302228478L;

	@Id
	@SequenceGenerator(sequenceName = "atendimento.seq_configuracao_tipo_processo", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tipo_processo_id", nullable = false)
	private TipoProcesso tipoProcesso = new TipoProcesso();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="modalidade_processo_id")
	private Modalidade modalidade = new Modalidade();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="subtipo_processo_id")
	private SubtipoProcesso subtipoProcesso = new SubtipoProcesso();

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="configuracao_tipos_taxa", schema="atendimento",
		joinColumns=@JoinColumn(name="configuracao_tipo_processo_id"),
		inverseJoinColumns=@JoinColumn(name="tipo_taxa_id"))
	private List<TipoTaxa> listTipoTaxa = new ArrayList<>();

	@Column(name="ativo")
	private Boolean ativo;

	@Column(name="exige_empreendimento")
	private Boolean exigeEmpreendimento = false;

	@Column(name="exige_spu")
	private Boolean exigeSpu = false;

	@Column(name="valida_titularidade_licenca")
	private Boolean validaTitularidadeLicenca = false;

	@Column(name="exige_atividade")
	private Boolean exigeAtividade = false;

	@Column(name="gera_dae")
	private Boolean geraDae = false;

	@Column(name="is_produto_agrotoxico")
	private Boolean isProdutoAgrotoxico = false;

	@Column(name="is_regularizacao")
	private Boolean isRegularizacao = false;

	@Column(name="validar_processo")
	private Boolean validarProcesso = false;

	@Column(name="exige_car")
	private Boolean exigeCar = false;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tipo_processo_siga")
	private TipoProcessoSiga tipoProcessoSiga = new TipoProcessoSiga();

	/**
	 * Desconsidera a vigência da licença
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 09/01/2014 08:14:40
	 */
	@Column(name="desconsiderar_vigencia_licenca", nullable = false)
	private boolean desconsiderarVigenciaLicenca = false;

	@Column(name = "exige_horario_atendimento")
	private boolean horarioAtendimentoExigido = true;

	@Column(name = "exige_shape")
	private boolean shapeExigido = false;

	@Column(name = "existe_autorizacao_ambiental")
	private boolean autorizacaoAmbientalExigido = false;

	@Column(name = "exibe_spu_combo")
	private boolean exibeSPUCombo = false;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="configuracao_tipo_processo_atividade", schema="atendimento",
	joinColumns=@JoinColumn(name="configuracao_tipo_processo_id"),
	inverseJoinColumns=@JoinColumn(name="atividade_id"))
	private List<Atividade> atividades;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="setor_destino_id")
	private Setor setorDestino;

	@Column(name = "permite_envio_doc_anexo")
	private boolean permiteEnvioDocAnexo;

	@Column(name = "permite_envio_doc_anexo_processo_arquivado")
	private boolean permiteEnvioDocAnexoProcessoArquivado;

	@Transient
	private List<FuncionarioNotificacaoProcesso> listFuncNotificacao;

	public ConfiguracaoTipoProcesso() {
		super();
	}

	public ConfiguracaoTipoProcesso(Long id) {
		super();
		this.id = id;
	}

	/*
	 * Getters and Setters
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getExigeEmpreendimento() {
		return exigeEmpreendimento;
	}

	public void setExigeEmpreendimento(Boolean exigeEmpreendimento) {
		this.exigeEmpreendimento = exigeEmpreendimento;
	}

	public Boolean getExigeSpu() {
		return exigeSpu;
	}

	public void setExigeSpu(Boolean exigeSpu) {
		this.exigeSpu = exigeSpu;
	}

	public Boolean getExigeAtividade() {
		return exigeAtividade;
	}

	public void setExigeAtividade(Boolean exigeAtividade) {
		this.exigeAtividade = exigeAtividade;
	}

	public Boolean getGeraDae() {
		return geraDae;
	}

	public void setGeraDae(Boolean geraDae) {
		this.geraDae = geraDae;
	}

	public Boolean getIsProdutoAgrotoxico() {
		return isProdutoAgrotoxico;
	}

	public void setIsProdutoAgrotoxico(Boolean isProdutoAgrotoxico) {
		this.isProdutoAgrotoxico = isProdutoAgrotoxico;
	}

	public Boolean getIsRegularizacao() {
		return isRegularizacao;
	}

	public void setIsRegularizacao(Boolean isRegularizacao) {
		this.isRegularizacao = isRegularizacao;
	}

	public TipoProcesso getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(TipoProcesso tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public SubtipoProcesso getSubtipoProcesso() {
		return subtipoProcesso;
	}

	public void setSubtipoProcesso(SubtipoProcesso subtipoProcesso) {
		this.subtipoProcesso = subtipoProcesso;
	}

	public Modalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}

	public Boolean getValidarProcesso() {
		return validarProcesso;
	}

	public void setValidarProcesso(Boolean validarProcesso) {
		this.validarProcesso = validarProcesso;
	}

	public List<TipoTaxa> getListTipoTaxa() {
		return listTipoTaxa;
	}

	public void setListTipoTaxa(List<TipoTaxa> listTipoTaxa) {
		this.listTipoTaxa = listTipoTaxa;
	}

	public TipoProcessoSiga getTipoProcessoSiga() {
		return tipoProcessoSiga;
	}

	public void setTipoProcessoSiga(TipoProcessoSiga tipoProcessoSiga) {
		this.tipoProcessoSiga = tipoProcessoSiga;
	}

	/**
	 * @return the desconsiderarVigenciaLicenca
	 */
	public boolean isDesconsiderarVigenciaLicenca() {
		return desconsiderarVigenciaLicenca;
	}

	/**
	 * @param desconsiderarVigenciaLicenca the desconsiderarVigenciaLicenca to set
	 */
	public void setDesconsiderarVigenciaLicenca(boolean desconsiderarVigenciaLicenca) {
		this.desconsiderarVigenciaLicenca = desconsiderarVigenciaLicenca;
	}

	public Boolean getValidaTitularidadeLicenca() {
		return validaTitularidadeLicenca;
	}

	public void setValidaTitularidadeLicenca(Boolean validaTitularidadeLicenca) {
		this.validaTitularidadeLicenca = validaTitularidadeLicenca;
	}

	public boolean isHorarioAtendimentoExigido() {
		return horarioAtendimentoExigido;
	}

	public void setHorarioAtendimentoExigido(boolean horarioAtendimentoExigido) {
		this.horarioAtendimentoExigido = horarioAtendimentoExigido;
	}

	public boolean isShapeExigido() {
		return shapeExigido;
	}

	public void setShapeExigido(boolean shapeExigido) {
		this.shapeExigido = shapeExigido;
	}

	public boolean isAutorizacaoAmbientalExigido() {
		return autorizacaoAmbientalExigido;
	}

	public void setAutorizacaoAmbientalExigido(boolean autorizacaoAmbientalExigido) {
		this.autorizacaoAmbientalExigido = autorizacaoAmbientalExigido;
	}

	public List<Atividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}

	public boolean getExibeSPUCombo() {
		return exibeSPUCombo;
	}

	public void setExibeSPUCombo(boolean exibeSPUCombo) {
		this.exibeSPUCombo = exibeSPUCombo;
	}

	public Boolean getExigeCar() {
		return exigeCar;
	}

	public void setExigeCar(Boolean exigeCar) {
		this.exigeCar = exigeCar;
	}

	public Setor getSetorDestino() {
		return setorDestino;
	}

	public void setSetorDestino(Setor setorDestino) {
		this.setorDestino = setorDestino;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public List<FuncionarioNotificacaoProcesso> getListFuncNotificacao() {
		return listFuncNotificacao;
	}

	public void setListFuncNotificacao(List<FuncionarioNotificacaoProcesso> listFuncNotificacao) {
		this.listFuncNotificacao = listFuncNotificacao;
	}

	public boolean isPermiteEnvioDocAnexo() {
		return permiteEnvioDocAnexo;
	}

	public void setPermiteEnvioDocAnexo(boolean permiteEnvioDocAnexo) {
		this.permiteEnvioDocAnexo = permiteEnvioDocAnexo;
	}

	@Transient
	public String getDescricaoSiglaTipoModalidadeSubtipoProcesso() {
		return FormatUtils.getDescricaoSiglaTipoModalidadeSubtipoProcesso(this.tipoProcesso, this.subtipoProcesso, this.modalidade);
	}

	@Transient
	public String getDescricaoAtividadeGrupoAtividade() {
		return FormatUtils.getDescricaoAtividadeGrupoAtividade(this.getAtividades());
	}

	@Override
	public int compareTo(ConfiguracaoTipoProcesso o) {
		return o.getDescricaoSiglaTipoModalidadeSubtipoProcesso().compareTo(this.getDescricaoSiglaTipoModalidadeSubtipoProcesso());
	}

	public boolean isPermiteEnvioDocAnexoProcessoArquivado() {
		return permiteEnvioDocAnexoProcessoArquivado;
	}

	public void setPermiteEnvioDocAnexoProcessoArquivado(boolean permiteEnvioDocAnexoProcessoArquivado) {
		this.permiteEnvioDocAnexoProcessoArquivado = permiteEnvioDocAnexoProcessoArquivado;
	}

	@Transient
	public String getDescricao(){
		StringBuilder builder = new StringBuilder("");
		if(modalidade != null && modalidade.getId() != null){
			builder.append(modalidade.getDescricao() + " / " + tipoProcesso.getDescricao());
			if(subtipoProcesso != null && subtipoProcesso.getId() != null){
				builder.append(" / " + subtipoProcesso.getDescricao());
			}
		}else{
			builder.append(tipoProcesso.getDescricao());
			if(subtipoProcesso != null && subtipoProcesso.getId() != null){
				builder.append(" / " + subtipoProcesso.getDescricao());
			}
		}

		if(atividades != null && !atividades.isEmpty()){
			StringBuilder builderAtv = new StringBuilder(" Atividades: ");
			for(Atividade atv : atividades){
				builderAtv.append(atv.getCodigo() + " - " + atv.getDescricao() + " / ");
			}
			builder.append(builderAtv.toString());
		}

		return builder.toString();
	}
}