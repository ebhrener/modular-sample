package br.gov.ce.semace.erp.entity.florestal;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * Entidade do Contrato Dados Consumo
 *
 * @author Carlos Berilo [carberilo@gmail.com] - 12/11/2015
 */
@Entity
@Audited
@Table(name = "dados_consumo", schema = "florestal")
public class DadosConsumo implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 3080462473647977633L;

	@Id
	@SequenceGenerator(sequenceName = "florestal.seq_dados_consumo", name = "SEQDADOSCONSUMO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQDADOSCONSUMO")
	private Long id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "descricao")
	private String descricao;

	@Column(name="ativo")
	private Boolean ativo = false;


	public DadosConsumo() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public DadosConsumo clone() throws CloneNotSupportedException {
		DadosConsumo dadosConsumo = new DadosConsumo();
		dadosConsumo.setAtivo(this.ativo);
		dadosConsumo.setDescricao(this.descricao);
		dadosConsumo.setId(this.id);
		dadosConsumo.setNome(this.nome);
		return dadosConsumo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DadosConsumo other = (DadosConsumo) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}