package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "laboratorio", name = "enquadramento")
public class Enquadramento implements Serializable{

	private static final long serialVersionUID = -9167401432573914470L;
	
	@Id
	@SequenceGenerator(sequenceName = "laboratorio.seq_enquadramento", name = "SEQ_ENQUADRAMENTO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ENQUADRAMENTO")
	private Long id;
	
	private String descricao;

	@Column(columnDefinition = "boolean default true")
	private boolean ativo = true;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String isAtivoToDisplay() {
		return this.ativo ? "Sim" : "Não";
	}
	
}
