package br.gov.ce.semace.erp.entity.licenciamento;

import br.gov.ce.semace.erp.entity.geral.Interessado;
import br.gov.ce.semace.erp.entity.seguranca.IUsuario;

public class FiltroBuscaListaPaginadaJDBC {

	private int firstRegister;
	private int pageSize;
	private String numeroSPU;
	private String numeroLicenca;
	private String ascedente;
	private IUsuario usuarioLogado;
	private Interessado interessado;

	public int getFirstRegister() {
		return firstRegister;
	}
	public void setFirstRegister(int firstRegister) {
		this.firstRegister = firstRegister;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getNumeroSPU() {
		return numeroSPU;
	}
	public void setNumeroSPU(String numeroSPU) {
		this.numeroSPU = numeroSPU;
	}
	public String getNumeroLicenca() {
		return numeroLicenca;
	}
	public void setNumeroLicenca(String numeroLicenca) {
		this.numeroLicenca = numeroLicenca;
	}
	public String getAscedente() {
		return ascedente;
	}
	public void setAscedente(String ascedente) {
		this.ascedente = ascedente;
	}
	public IUsuario getUsuarioLogado() {
		return usuarioLogado;
	}
	public void setUsuarioLogado(IUsuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}
	public Interessado getInteressado() {
		return interessado;
	}
	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}

}
