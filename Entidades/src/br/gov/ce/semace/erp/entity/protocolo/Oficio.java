package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.geral.Interessado;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.StatusDocumento;

@Entity
@Audited
@Table(schema = "protocolo", name = "oficio")
public class Oficio implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 5554027058439361886L;

	@Id
	@SequenceGenerator(sequenceName = "protocolo.seq_oficio", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(columnDefinition="text")
	private String descricao;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "documento_id")
	private Documento documento = new Documento();

	@Column(name = "status_oficio")
	private StatusDocumento statusOficio;

	@Column(name = "is_assinado")
	private Boolean isAssinado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "responsavel_assinatura_id")
	private Funcionario responsavelAssinatura;

	@Column(name = "documento_destinatario")
	private String documentoDestinatario;

	@Column(name = "endereco_destinatario")
	private String enderecoDestinatario;

	@Column(name = "nome_destinatario")
	private String nomeDestinatario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "processo_id")
	private Processo processo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "interessado_id")
	private Interessado interessado;

	@Transient
	private boolean processoSelecionado;

	@Transient
	private boolean pessoaFisicaSelecionado;

	/////////////////MÉTODOS TRANSIENTES/////////////////////////////

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusOficio} é
	 * 	do tipo <b>ARQUIVADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 22/06/2013 08:44:59
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusOficio} seja
	 * 	<b>ARQUIVADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDocumento#isArquivado()
	 */
	@Transient
	public boolean isArquivado(){
		return this.statusOficio.isArquivado();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusOficio} é
	 * 	do tipo <b>CANCELADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 22/06/2013 08:44:56
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusOficio} seja
	 * 	<b>CANCELADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDocumento#isCancelado()
	 */
	@Transient
	public boolean isCancelado(){
		return this.statusOficio.isCancelado();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusOficio} é
	 * 	do tipo <b>CRIADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 22/06/2013 08:44:53
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusOficio} seja
	 * 	<b>CRIADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDocumento#isCriado()
	 */
	@Transient
	public boolean isCriado(){
		return this.statusOficio.isCriado();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusOficio} é
	 * 	do tipo <b>ENCERRADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 22/06/2013 08:44:50
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusOficio} seja
	 * 	<b>ENCERRADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDocumento#isEncerrado()
	 */
	@Transient
	public boolean isEncerrado(){
		return this.statusOficio.isEncerrado();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusOficio} é
	 * 	do tipo <b>PROTOCOLADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 22/06/2013 08:44:46
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusOficio} seja
	 * 	<b>PROTOCOLADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDocumento#isProtocolado()
	 */
	@Transient
	public boolean isProtocolado(){
		return this.statusOficio.isProtocolado();
	}

	/**
	 * Fornece os campos {@link #dataCriacao} e {@link #horaCriacao} formatados.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jul 22, 2013 1:50:01 PM
	 *
	 * @return retorna uma {@link String} com de acordo com o padrão {@link DateUtils#BRAZIL_FORMAT} para a {@link #dataCriacao} e
	 * {@link DateUtils#BRAZIL_SHORT_TIME_FORMAT} para a {@link #horaCriacao}, concatenados e separados por um espaço em branco.
	 */
	@Transient
	public String getDataHoraFormatada(){
		// concatena e separa por espaços a string a ser retornada
		return this.documento.getDataHoraFormatada();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public StatusDocumento getStatusOficio() {
		return statusOficio;
	}

	public void setStatusOficio(StatusDocumento statusOficio) {
		this.statusOficio = statusOficio;
	}

	public Boolean getIsAssinado() {
		return isAssinado;
	}

	public void setIsAssinado(Boolean isAssinado) {
		this.isAssinado = isAssinado;
	}

	public Funcionario getResponsavelAssinatura() {
		return responsavelAssinatura;
	}

	public void setResponsavelAssinatura(Funcionario responsavelAssinatura) {
		this.responsavelAssinatura = responsavelAssinatura;
	}

	public String getDocumentoDestinatario() {
		return documentoDestinatario;
	}

	public void setDocumentoDestinatario(String documentoDestinatario) {
		this.documentoDestinatario = documentoDestinatario;
	}

	public String getEnderecoDestinatario() {
		return enderecoDestinatario;
	}

	public void setEnderecoDestinatario(String enderecoDestinatario) {
		this.enderecoDestinatario = enderecoDestinatario;
	}

	public String getNomeDestinatario() {
		return nomeDestinatario;
	}

	public void setNomeDestinatario(String nomeDestinatario) {
		this.nomeDestinatario = nomeDestinatario;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	/**
	 * @return the processoSelecionado
	 */
	public boolean isProcessoSelecionado() {
		return processoSelecionado;
	}

	/**
	 * @param processoSelecionado the processoSelecionado to set
	 */
	public void setProcessoSelecionado(boolean processoSelecionado) {
		this.processoSelecionado = processoSelecionado;
	}

	/**
	 * @return the pessoaFisicaSelecionado
	 */
	public boolean isPessoaFisicaSelecionado() {
		return pessoaFisicaSelecionado;
	}

	/**
	 * @param pessoaFisicaSelecionado the pessoaFisicaSelecionado to set
	 */
	public void setPessoaFisicaSelecionado(boolean pessoaFisicaSelecionado) {
		this.pessoaFisicaSelecionado = pessoaFisicaSelecionado;
	}

	/**
	 * @return the interessado
	 */
	public Interessado getInteressado() {
		return interessado;
	}

	/**
	 * @param interessado the interessado to set
	 */
	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}

}
