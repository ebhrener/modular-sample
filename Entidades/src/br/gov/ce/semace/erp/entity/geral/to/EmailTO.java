package br.gov.ce.semace.erp.entity.geral.to;

public class EmailTO {

	private String email;
	private String destinatario;
	private Long destinatarioID;

	public EmailTO() {
		super();
	}

	public EmailTO(String email, String destinatario, Long destinatarioID) {
		super();
		this.email = email;
		this.destinatario = destinatario;
		this.destinatarioID = destinatarioID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public Long getDestinatarioID() {
		return destinatarioID;
	}

	public void setDestinatarioID(Long destinatarioID) {
		this.destinatarioID = destinatarioID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EmailTO other = (EmailTO) obj;
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		return true;
	}
}
