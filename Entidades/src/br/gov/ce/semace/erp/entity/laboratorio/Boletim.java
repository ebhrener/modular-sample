package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;

@Entity
@Audited
@Table(name="boletim", schema="laboratorio")
public class Boletim implements Serializable {

	private static final long serialVersionUID = -2702027608544147507L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_boletim", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@ManyToOne(fetch= FetchType.LAZY, optional = false)
	@JoinColumn(name="documento_id")
	private Documento documento = new Documento();

	@ManyToOne(fetch= FetchType.LAZY, optional = false)
	@JoinColumn(name="analise_id")
	private Analise analise = new Analise();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "boletim", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ResultadoBoletim> resultadoBoletim = new ArrayList<ResultadoBoletim>();

	@Column(name="descricao_condicoes_climaticas")
	private String descricaoCondicoesClimaticas;

	/**
	 * Substituido pelo campo mares que terá uma lista de {@link Mare}
	 */
	@Deprecated
	@Column(name="nivel_mare")
	private Float nivelMare;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_cadastro", nullable=false)
	private Date dataCadastro;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_publicacao", nullable=false)
	private Date dataPublicacao;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="boletim", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<FuncionarioBoletim> funcionarios;

	@OneToMany(mappedBy="boletim", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<Mare> mares = new ArrayList<Mare>();

	@Transient private Date dataPublicacaoInicial;
	@Transient private Date dataPublicacaoFinal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public List<ResultadoBoletim> getResultadoBoletim() {
		return resultadoBoletim;
	}

	public void setResultadoBoletim(List<ResultadoBoletim> resultadoBoletim) {
		this.resultadoBoletim = resultadoBoletim;
	}

	public Analise getAnalise() {
		return analise;
	}

	public void setAnalise(Analise analise) {
		this.analise = analise;
	}

	public String getDescricaoCondicoesClimaticas() {
		return descricaoCondicoesClimaticas;
	}

	public void setDescricaoCondicoesClimaticas(String descricaoCondicoesClimaticas) {
		this.descricaoCondicoesClimaticas = descricaoCondicoesClimaticas;
	}

	@Deprecated
	public Float getNivelMare() {
		return nivelMare;
	}

	@Deprecated
	public void setNivelMare(Float nivelMare) {
		this.nivelMare = nivelMare;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataPublicacao() {
		return dataPublicacao;
	}

	public void setDataPublicacao(Date dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}

	public Date getDataPublicacaoInicial() {
		return dataPublicacaoInicial;
	}

	public void setDataPublicacaoInicial(Date dataPublicacaoInicial) {
		this.dataPublicacaoInicial = dataPublicacaoInicial;
	}

	public Date getDataPublicacaoFinal() {
		return dataPublicacaoFinal;
	}

	public void setDataPublicacaoFinal(Date dataPublicacaoFinal) {
		this.dataPublicacaoFinal = dataPublicacaoFinal;
	}

	public List<Mare> getMares() {
		return mares;
	}

	public void setMares(List<Mare> mares) {
		this.mares = mares;
	}

	public void addMare(Mare mare) {
		if (mares == null) {
			mares = new ArrayList<>();
		}
		mares.add(mare);
	}

	public List<FuncionarioBoletim> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<FuncionarioBoletim> funcionarios) {
		this.funcionarios = funcionarios;
	}

}
