package br.gov.ce.semace.erp.entity.geral;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.utils.DateUtils;

@Entity
@Audited
@Table(schema="geral", name="pessoa_fisica", uniqueConstraints=@UniqueConstraint(columnNames="cpf"))
public class PessoaFisica extends Pessoa {

	private static final long serialVersionUID = 506380966349968295L;

	@NotNull
	@Column(unique = true, nullable=false)
	private String cpf;

	private String rg;

	private Character sexo;

	@NotNull
	@Column(nullable=false)
	private String nome;

	@Temporal(TemporalType.DATE)
	@Column(name="data_nascimento")
	@Index(name="data_nascimento_i")
	private Date dataNascimento;

	private String email;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="contato_id")
	private Contato contato = new Contato();

	private String filiacao;

	@Transient
	private static final String MSG_CAMPO_NAO_INFORMADO = "Não Informado";

	@Transient
	private String documentoRepresentante;

	/**
	 * Construtor default
	 *
	 * @author joerlan
	 */
	public PessoaFisica() {
		super();
	}

	/**
	 * Construtor passando a pessoa como parametro
	 *
	 * @author joerlan
	 */
	public PessoaFisica(Pessoa pessoa) {
		super(pessoa);
	}

	/**
	 * Método {@link Transient} que informa de forma detalhada o Sexo da Pessoa Física.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 25/09/2013 11:38:00
	 *
	 * @return Retorna uma {@link String} <code>Masculino</code>, caso o {@link #sexo} da {@link PessoaFisica} seja <code>M</code>.
	 * Caso seja <code>F</code>, retorna <code>Feminino</code>. Caso não seja nenhum dos dois caso, retorna <code>Não informado</code>
	 */
	@Transient
	public String getDetalhesSexo() {
		if(this.getSexo() != null){
			if (this.getSexo().equals('M')){
				return "Masculino";
			}
			else if (this.getSexo().equals('F')){
				return "Feminino";
			}
		}

		return MSG_CAMPO_NAO_INFORMADO;
	}
	/**
	 * Método {@link Transient}e que informa o {@link #getRg()} da {@link PessoaFisica}.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 25/09/2013 11:42:23
	 *
	 * @return Retorna uma {@link String} com o {@link #getRg()} ou caso não exista, retorna <code>Não Informado</code>
	 */
	@Transient
	public String getDetalhesRG() {

		if(this.getRg() == null || this.getRg().trim().isEmpty()) {
			return MSG_CAMPO_NAO_INFORMADO;
		}

		return this.getRg();
	}

	/**
	 * Método {@link Transient}e que informa a Data de Nascimento da {@link PessoaFisica}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 20/11/2015 10:26:29
	 *
	 * @see DateUtils#toString()
	 *
	 * @return
	 * 		Retorna a mensagem <i>Não Informado</i>, caso o campo {@link #getDataNascimento()} seja <code>null</code>.
	 * Caso contrário, retorna a {@link String} com a data de nascimento já formatada.
	 */
	@Transient
	public String getDetalhesDataNascimento(){
		if(this.dataNascimento == null){
			return MSG_CAMPO_NAO_INFORMADO;
		}

		return DateUtils.toString(this.dataNascimento);
	}


	public String getCpf() {
		return this.cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return this.rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public Character getSexo() {
		return this.sexo;
	}
	public void setSexo(Character sexo) {
		this.sexo = sexo;
	}
	public Date getDataNascimento() {
		return this.dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getNome() {
		return this.nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return this.email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String getDisplayNome() {
		return this.getNome();
	}

	@Override
	public String getDisplayDocumento() {
		return this.getCpf();
	}

	@Override
	public String getDisplayEmail() {
		return this.getEmail();
	}
	/**
	 * @return the contato
	 */
	public Contato getContato() {
		return this.contato;
	}
	/**
	 * @param contato the contato to set
	 */
	public void setContato(Contato contato) {
		this.contato = contato;
	}

	@Override
	@Transient
	public String getEmailDefault() {
		return getEmail();
	}

	@Override
	@Transient
	public Contato getContatoDefault() {
		return getContato();
	}

	public String getDocumentoRepresentante() {
		return documentoRepresentante;
	}

	public void setDocumentoRepresentante(String documentoRepresentante) {
		this.documentoRepresentante = documentoRepresentante;
	}

	public String getFiliacao() {
		return filiacao;
	}

	public void setFiliacao(String filiacao) {
		this.filiacao = filiacao;
	}

}