package br.gov.ce.semace.erp.entity.planoviagem;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.enuns.TipoRelatorio;

@Entity
@Audited
@Table(schema="plano_viagem", name="relatorio")
public class Relatorio implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1495661351797771752L;

	@Id
	@SequenceGenerator(sequenceName = "plano_viagem.seq_relatorio", name = "SEQ_RELATORIO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_RELATORIO")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "documento_id")
	private Documento documento;

	@Column(name="data_inicial")
	private Date dataInicio;

	@Column(name="data_final")
	private Date dataFim;

	@Column(name="tipo_relatorio")
	private TipoRelatorio tipoRelatorio;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "relatorio")
	private List<RelatorioPessoa> listaRelatorioPessoa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public TipoRelatorio getTipoRelatorio() {
		return tipoRelatorio;
	}

	public void setTipoRelatorio(TipoRelatorio tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}

	public List<RelatorioPessoa> getListaRelatorioPessoa() {
		return listaRelatorioPessoa;
	}

	public void setListaRelatorioPessoa(List<RelatorioPessoa> listaRelatorioPessoa) {
		this.listaRelatorioPessoa = listaRelatorioPessoa;
	}

}
