package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import br.gov.ce.semace.erp.enuns.StatusArquivoTemporario;

@Entity
@Table(schema = "geral", name="arquivo_temporario")
@Audited
public class ArquivoTemporario implements Serializable {

	private static final long serialVersionUID = 4735668441264862606L;

	@Id @SequenceGenerator(initialValue = 1, allocationSize = 1, name="SEQ", sequenceName="geral.seq_arquivo_temporario")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(name = "bytes")
	@NotAudited
	private byte[] bytes;

	@Column(name = "data_criacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCriacao;

	@Column(name = "data_processamento")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataProcessamento;

	@Enumerated(EnumType.ORDINAL)
	@Column(nullable = false, name = "status")
	private StatusArquivoTemporario status = StatusArquivoTemporario.CRIADO;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public StatusArquivoTemporario getStatus() {
		return status;
	}

	public void setStatus(StatusArquivoTemporario status) {
		this.status = status;
	}

}
