package br.gov.ce.semace.erp.entity.geral.to;

import java.util.Date;

public class Chave {

	private Integer id;
	private Integer ChaveID;
	private String descricao;
	private String solicitacao;
	private Date data;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getChaveID() {
		return ChaveID;
	}

	public void setChaveID(Integer chaveID) {
		ChaveID = chaveID;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSolicitacao() {
		return solicitacao;
	}

	public void setSolicitacao(String solicitacao) {
		this.solicitacao = solicitacao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
}
