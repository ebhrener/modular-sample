package br.gov.ce.semace.erp.entity.viproc;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="sincronizacao_processo", schema="viproc")
public class SincronizacaoProcesso implements Serializable {

	private static final long serialVersionUID = -8676500891199627561L;

	@Id
	@SequenceGenerator(sequenceName="viproc.seq_sincronizacao_processo", name="SEQSINPRO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQSINPRO")
	private Long id;

	@Column(name = "spu")
	private String spu;

	@Column(name = "sistema")
	private int sistema;

	@Column(columnDefinition="sincronizado")
	private boolean sincronizado;

	@Column(name = "qtd_erros")
	private int qtdErros;

	@Column(name = "last_remote_error", columnDefinition="TEXT")
	private String lastRemoteErro;

	//---------------------------------------------GETS e SETTERS----------------------------------------------

	public SincronizacaoProcesso() {
		super();
	}

	public SincronizacaoProcesso(Long id, String spu, int sistema,
			boolean sincronizado, int qtdErros) {
		this(id, spu, sistema, sincronizado, qtdErros, null);
	}

	public SincronizacaoProcesso(Long id, String spu, int sistema,
			boolean sincronizado, int qtdErros, String lastRemoteErro) {
		super();
		this.id = id;
		this.spu = spu;
		this.sistema = sistema;
		this.sincronizado = sincronizado;
		this.qtdErros = qtdErros;
		this.lastRemoteErro = lastRemoteErro;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public int getSistema() {
		return sistema;
	}

	public void setSistema(int sistema) {
		this.sistema = sistema;
	}

	public boolean isSincronizado() {
		return sincronizado;
	}

	public void setSincronizado(boolean sincronizado) {
		this.sincronizado = sincronizado;
	}

	public int getQtdErros() {
		return qtdErros;
	}

	public void setQtdErros(int qtdErros) {
		this.qtdErros = qtdErros;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SincronizacaoProcesso other = (SincronizacaoProcesso) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public String getLastRemoteErro() {
		return lastRemoteErro;
	}

	public void setLastRemoteErro(String lastRemoteErro) {
		this.lastRemoteErro = lastRemoteErro;
	}

}