package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.util.Date;

public class UsuarioSiga implements Serializable {

	private static final long serialVersionUID = -7869664368134585029L;

	private String usuarioId;
	private String areaId;
	private String login;
	private String nome;
	private String email;
	private Boolean excluido;
	private String siglaCategoria;
	private String cpf;
	private String especialidade;
	private Boolean substituido = Boolean.FALSE;
	private String userSubs;
	private Date dataTravamento;
	private Boolean travado = Boolean.FALSE;
	private Boolean usComp = Boolean.FALSE;
	private String userCad;
	private Integer nivelId;
	private String quemExcluiu;
	private Integer credor;
	private String ramal;
	private Boolean agendamento = Boolean.FALSE;
	private String ultimoAcesso;
	private String ultimaPagina;
	private String rg;
	private Boolean acessoExterno = Boolean.FALSE;
	private Boolean receberPlano;
	private String quemAtivou;
	private Date acessoExpirado;
	private Integer pessoaIdNatuur;

	public UsuarioSiga() {
		super();
	}

	public UsuarioSiga(String usuarioId, String areaId, String login, String nome, String email, Boolean excluido, String siglaCategoria,
			String cpf, String especialidade, Boolean substituido, String userSubs, Date dataTravamento, Boolean travado, Boolean usComp,
			String userCad, Integer nivelId, String quemExcluiu, Integer credor, String ramal, Boolean agendamento, String ultimoAcesso,
			String ultimaPagina, String rg, Boolean acessoExterno, Boolean receberPlano, String quemAtivou, Date acessoExpirado, Integer pessoaIdNatuur) {
		this.usuarioId = usuarioId;
		this.areaId = areaId;
		this.login = login;
		this.nome = nome;
		this.email = email;
		this.excluido = excluido;
		this.siglaCategoria = siglaCategoria;
		this.cpf = cpf;
		this.especialidade = especialidade;
		this.substituido = substituido;
		this.userSubs = userSubs;
		this.dataTravamento = dataTravamento;
		this.travado = travado;
		this.usComp = usComp;
		this.userCad = userCad;
		this.nivelId = nivelId;
		this.quemExcluiu = quemExcluiu;
		this.credor = credor;
		this.ramal = ramal;
		this.agendamento = agendamento;
		this.ultimoAcesso = ultimoAcesso;
		this.ultimaPagina = ultimaPagina;
		this.rg = rg;
		this.acessoExterno = acessoExterno;
		this.receberPlano = receberPlano;
		this.quemAtivou = quemAtivou;
		this.acessoExpirado = acessoExpirado;
		this.pessoaIdNatuur = pessoaIdNatuur;
	}

	public String getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getExcluido() {
		return excluido;
	}

	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}

	public String getSiglaCategoria() {
		return siglaCategoria;
	}

	public void setSiglaCategoria(String siglaCategoria) {
		this.siglaCategoria = siglaCategoria;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public Boolean getSubstituido() {
		return substituido;
	}

	public void setSubstituido(Boolean substituido) {
		this.substituido = substituido;
	}

	public String getUserSubs() {
		return userSubs;
	}

	public void setUserSubs(String userSubs) {
		this.userSubs = userSubs;
	}

	public Date getDataTravamento() {
		return dataTravamento;
	}

	public void setDataTravamento(Date dataTravamento) {
		this.dataTravamento = dataTravamento;
	}

	public Boolean getTravado() {
		return travado;
	}

	public void setTravado(Boolean travado) {
		this.travado = travado;
	}

	public Boolean getUsComp() {
		return usComp;
	}

	public void setUsComp(Boolean usComp) {
		this.usComp = usComp;
	}

	public String getUserCad() {
		return userCad;
	}

	public void setUserCad(String userCad) {
		this.userCad = userCad;
	}

	public Integer getNivelId() {
		return nivelId;
	}

	public void setNivelId(Integer nivelId) {
		this.nivelId = nivelId;
	}

	public String getQuemExcluiu() {
		return quemExcluiu;
	}

	public void setQuemExcluiu(String quemExcluiu) {
		this.quemExcluiu = quemExcluiu;
	}

	public Integer getCredor() {
		return credor;
	}

	public void setCredor(Integer credor) {
		this.credor = credor;
	}

	public String getRamal() {
		return ramal;
	}

	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

	public Boolean getAgendamento() {
		return agendamento;
	}

	public void setAgendamento(Boolean agendamento) {
		this.agendamento = agendamento;
	}

	public String getUltimoAcesso() {
		return ultimoAcesso;
	}

	public void setUltimoAcesso(String ultimoAcesso) {
		this.ultimoAcesso = ultimoAcesso;
	}

	public String getUltimaPagina() {
		return ultimaPagina;
	}

	public void setUltimaPagina(String ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Boolean getAcessoExterno() {
		return acessoExterno;
	}

	public void setAcessoExterno(Boolean acessoExterno) {
		this.acessoExterno = acessoExterno;
	}

	public Boolean getReceberPlano() {
		return receberPlano;
	}

	public void setReceberPlano(Boolean receberPlano) {
		this.receberPlano = receberPlano;
	}

	public String getQuemAtivou() {
		return quemAtivou;
	}

	public void setQuemAtivou(String quemAtivou) {
		this.quemAtivou = quemAtivou;
	}

	public Date getAcessoExpirado() {
		return acessoExpirado;
	}

	public void setAcessoExpirado(Date acessoExpirado) {
		this.acessoExpirado = acessoExpirado;
	}

	public Integer getPessoaIdNatuur() {
		return pessoaIdNatuur;
	}

	public void setPessoaIdNatuur(Integer pessoaIdNatuur) {
		this.pessoaIdNatuur = pessoaIdNatuur;
	}
}