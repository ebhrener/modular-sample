package br.gov.ce.semace.erp.entity.notificacaoprocesso;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.StatusRecebimentoNotificacao;


@Audited
@Entity
@Table(name="recebimento_notificacoes", schema="notificacao_processo")
public class RecebimentoNotificacoes implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 3592379106779298239L;

	@Id
	@SequenceGenerator(sequenceName="notificacao_processo.seq_recebimento_notificacoes", name="notificacao_processo.seq_recebimento_notificacoes", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="notificacao_processo.seq_recebimento_notificacoes")
	private Long id;

	@OneToMany(mappedBy="recebimentoNotificacoes", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<RecebimentoNotificacoesProcesso> recebimentoNotificacoesProcessos;

	@OneToMany(mappedBy="recebimentoNotificacoes", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<RecebimentoNotificacoesPessoa> recebimentoNotificacoesPessoas;

	private String telefone;

	private String ddd;

	private String ddi;

	@Column(unique=true)
	private String email;

	@Column(nullable=false, name="data_cadastro")
	private Date dataCadastro;

	@Enumerated(EnumType.ORDINAL)
	@Column(name="status")
	private StatusRecebimentoNotificacao statusRecebimentoNotificacao;

	@Transient
	public static final String JSON_EMAIL = "e";
	@Transient
	public static final String JSON_PROCESSO_ID = "pr";
	@Transient
	public static final String JSON_PESSOA_ID = "pe";

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getDdi() {
		return ddi;
	}

	public void setDdi(String ddi) {
		this.ddi = ddi;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public StatusRecebimentoNotificacao getStatusRecebimentoNotificacao() {
		return statusRecebimentoNotificacao;
	}

	public void setStatusRecebimentoNotificacao(
			StatusRecebimentoNotificacao statusRecebimentoNotificacao) {
		this.statusRecebimentoNotificacao = statusRecebimentoNotificacao;
	}

	public List<RecebimentoNotificacoesPessoa> getRecebimentoNotificacoesPessoas() {
		return recebimentoNotificacoesPessoas;
	}

	public void setRecebimentoNotificacoesPessoas(
			List<RecebimentoNotificacoesPessoa> recebimentoNotificacoesPessoas) {
		this.recebimentoNotificacoesPessoas = recebimentoNotificacoesPessoas;
	}

	public List<RecebimentoNotificacoesProcesso> getRecebimentoNotificacoesProcessos() {
		return recebimentoNotificacoesProcessos;
	}

	public void setRecebimentoNotificacoesProcessos(
			List<RecebimentoNotificacoesProcesso> recebimentoNotificacoesProcessos) {
		this.recebimentoNotificacoesProcessos = recebimentoNotificacoesProcessos;
	}

}
