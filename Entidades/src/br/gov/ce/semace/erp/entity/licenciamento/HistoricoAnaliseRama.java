package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.SituacaoRama;

@Entity
@Audited
@Table(name = "historico_analise_rama", schema = "licenciamento")
public class HistoricoAnaliseRama implements Serializable {

	private static final long serialVersionUID = 2473183685472120188L;
	@Id
	@SequenceGenerator(sequenceName = "licenciamento.seq_historico_analise_rama", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(name = "spu_licenca")
	private String spuLicenca;

	@Column(name = "spu_rama")
	private String spuRama;

	@Column(name = "data_inicio_entrega")
	private Date dataInicioEntrega;

	@Column(name = "data_fim_entrega")
	private Date dataFimEntrega;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tecnico_id")
	private Funcionario tecnico;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="rama_id")
	private Rama rama;

	@Column(name = "situacao")
	private SituacaoRama situacao;

	@Column(name = "data_finalizacao")
	private Date dataFinalizacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpuLicenca() {
		return spuLicenca;
	}

	public void setSpuLicenca(String spuLicenca) {
		this.spuLicenca = spuLicenca;
	}

	public String getSpuRama() {
		return spuRama;
	}

	public void setSpuRama(String spuRama) {
		this.spuRama = spuRama;
	}

	public Date getDataInicioEntrega() {
		return dataInicioEntrega;
	}

	public void setDataInicioEntrega(Date dataInicioEntrega) {
		this.dataInicioEntrega = dataInicioEntrega;
	}

	public Date getDataFimEntrega() {
		return dataFimEntrega;
	}

	public void setDataFimEntrega(Date dataFimEntrega) {
		this.dataFimEntrega = dataFimEntrega;
	}

	public Funcionario getTecnico() {
		return tecnico;
	}

	public void setTecnico(Funcionario tecnico) {
		this.tecnico = tecnico;
	}

	public SituacaoRama getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoRama situacao) {
		this.situacao = situacao;
	}

	public Date getDataFinalizacao() {
		return dataFinalizacao;
	}

	public void setDataFinalizacao(Date dataFinalizacao) {
		this.dataFinalizacao = dataFinalizacao;
	}

	public Rama getRama() {
		return rama;
	}

	public void setRama(Rama rama) {
		this.rama = rama;
	}
}
