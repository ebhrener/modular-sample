package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "classificacao_grandeza", schema = "licenciamento")
public class ClassificacaoGrandeza implements Serializable {

	private static final long serialVersionUID = 4079343627862292688L;

	@Id
	@SequenceGenerator(sequenceName = "licenciamento.seq_grandeza", name = "SEQGrandeza", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQGrandeza")
	private Long id;

	@Column(columnDefinition = "text")
	private String descricao;

	@Column(name = "ativo")
	private boolean ativo;

	@Column(name = "titulo")
	private String titulo;

	@Column(name = "pergunta")
	private String pergunta;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getPergunta() {
		return pergunta;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}
}
