package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="dados_procuracao", schema = "geral")
public class DadosProcuracao implements Serializable{

	private static final long serialVersionUID = -7263194996117013826L;
	
	@Id @Column(name="dados_procuracao_id")
	@SequenceGenerator(sequenceName="geral.seq_dados_procuracao", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_validade")
	private Date dataValidade;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="consultoria_interessado_id")
	private ConsultoriaInteressado consultoriaInteressado = new ConsultoriaInteressado();
	
	private Boolean ativo = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}

	public ConsultoriaInteressado getConsultoriaInteressado() {
		return consultoriaInteressado;
	}

	public void setConsultoriaInteressado(
			ConsultoriaInteressado consultoriaInteressado) {
		this.consultoriaInteressado = consultoriaInteressado;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	
	
	@Override
	public int hashCode() {
		if(getId() != null){
			return getId().hashCode();
		}
		return 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof DadosProcuracao)) {
			return false;
		}
		return ((DadosProcuracao)obj).getId().equals(getId());
	}	

}
