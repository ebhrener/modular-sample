package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;

@Entity
@Audited
@Table(name="arquivo_aviso_recebimento", schema="protocolo")
public class ArquivoAvisoRecebimento implements Serializable{

	private static final long serialVersionUID = 4138066516400191388L;

	@Id
	@SequenceGenerator(sequenceName ="protocolo.seq_arquivo_aviso_recebimento", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(name = "nome_arquivo")
	private String nomeArquivo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "aviso_recebimento_id")
	private AvisoRecebimento avisoRecebimento = new AvisoRecebimento();

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "documento_id")
	private Documento documento = new Documento();


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AvisoRecebimento getAvisoRecebimento() {
		return avisoRecebimento;
	}

	public void setAvisoRecebimento(AvisoRecebimento avisoRecebimento) {
		this.avisoRecebimento = avisoRecebimento;
	}

	/**
	 * @return the documento
	 */
	public Documento getDocumento() {
		return documento;
	}

	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	/**
	 * @return the nomeArquivo
	 */
	public String getNomeArquivo() {
		return nomeArquivo;
	}

	/**
	 * @param nomeArquivo the nomeArquivo to set
	 */
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
}