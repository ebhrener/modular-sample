package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "tipo_telefone", schema = "geral")
public class TipoTelefone implements Serializable {

	private static final long serialVersionUID = 6486302574529470591L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_tipo_telefone", name = "SEQTIPO_TELEFONE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQTIPO_TELEFONE")
	private Long id;

	@Column(name = "tipo_telefone")
	private String tipoTelefone;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoTelefone() {
		return tipoTelefone;
	}

	public void setTipoTelefone(String tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}
}
