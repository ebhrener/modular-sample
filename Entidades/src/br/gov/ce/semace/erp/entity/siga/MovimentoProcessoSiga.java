package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "movimento_processo", schema = "adm_siga")
public class MovimentoProcessoSiga implements Serializable {

	private static final long serialVersionUID = 4937012549361171919L;

	@Id
	@SequenceGenerator(sequenceName = "adm_siga.sq_movimento_processo", name = "SEQMOVPRO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMOVPRO")
	@Column(name = "mov_id")
	private Long id;

	@Column(name = "processo_id", length = 10)
	private String processo;

	@Column(name = "data_hora")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHora;

	@Column(name = "status_processo_id")
	private String statusProcessoId;

	@Column(name = "area_usuario_destino_id")
	private String idAreaUsuarioDestino;

	@Column(name = "fim_da_fila")
	private Integer fimFila;

	@Column(name = "usuario_inicio_id")
	private String usuarioInicioId;

	@Column(name = "area_usuario_inicio_id")
	private String areaUsuarioInicioId;

	@Column(name = "usuario_destino_id")
	private String usuarioDestinoId;

	@Column(name = "status_externo_id")
	private Integer statusExternoId;

	@Transient
	private String spu;

	/**
	 * Atributos usados em ProcessoSigaDao.listaHistorico Anotados com @Transient
	 * para nao gerar impacto.
	 */
	@Transient
	private String status;

	@Transient
	private String statusDescricao;

	@Transient
	private String area;

	@Transient
	private String sigla;

	public String getProcesso() {
		return processo;
	}

	public void setProcesso(String processo) {
		this.processo = processo;
	}

	public String getDataHora() {
		return new SimpleDateFormat("dd/MM/yyyy").format(dataHora);
	}

	public Date getDataHoraAsDate() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDescricao() {
		return statusDescricao;
	}

	public void setStatusDescricao(String statusDescricao) {
		this.statusDescricao = statusDescricao;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getIdAreaUsuarioDestino() {
		return idAreaUsuarioDestino;
	}

	public void setIdAreaUsuarioDestino(String idAreaUsuarioDestino) {
		this.idAreaUsuarioDestino = idAreaUsuarioDestino;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MovimentoProcessoSiga other = (MovimentoProcessoSiga) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public String getNumeroSPUProcesso() {
		if (processo != null && processo.length() >= 7
				&& processo.contains("/")) {
			return processo.substring(0, 7);
		}
		return processo.replace("-", "");
	}

	public String getAnoProcesso() {
		if (processo != null && processo.length() >= 12
				&& processo.contains("/")) {
			return processo.substring(8, 12);
		}
		return null;
	}

	public String getStatusProcessoId() {
		return statusProcessoId;
	}

	public void setStatusProcessoId(String statusProcessoId) {
		this.statusProcessoId = statusProcessoId;
	}

	public Integer getFimFila() {
		return fimFila;
	}

	public void setFimFila(Integer fimFila) {
		this.fimFila = fimFila;
	}

	public String getUsuarioInicioId() {
		return usuarioInicioId;
	}

	public void setUsuarioInicioId(String usuarioInicioId) {
		this.usuarioInicioId = usuarioInicioId;
	}

	public String getAreaUsuarioInicioId() {
		return areaUsuarioInicioId;
	}

	public void setAreaUsuarioInicioId(String areaUsuarioInicioId) {
		this.areaUsuarioInicioId = areaUsuarioInicioId;
	}

	public String getUsuarioDestinoId() {
		return usuarioDestinoId;
	}

	public void setUsuarioDestinoId(String usuarioDestinoId) {
		this.usuarioDestinoId = usuarioDestinoId;
	}

	public Integer getStatusExternoId() {
		return statusExternoId;
	}

	public void setStatusExternoId(Integer statusExternoId) {
		this.statusExternoId = statusExternoId;
	}



}
