package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.time.Period;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="feriado", schema="geral")
//@TypeDef(name="period", typeClass= JodaTypePeriodParser.class)
public class Feriado implements Serializable{

	private static final long serialVersionUID = -4685970444640260764L;

	@Id @Column(name="feriado_id")
	@SequenceGenerator(sequenceName ="geral.seq_feriado", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(nullable=false)
	@Temporal(TemporalType.DATE)
	private Date data;

	@Column(nullable=false)
	private String descricao;

	@Column(name="ponto_facultativo_compensado")
	private Boolean pontoFacultativoCompensado;

	@Column(name = "horas_compensar")
//	@Type(type="period")
	private Period horasCompensar;

	@Column(name = "date_inicio_compensacao")
	private Date inicioCompensacao;

	@Column(name = "date_fim_compensacao")
	private Date fimCompensacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Boolean getPontoFacultativoCompensado() {
		return pontoFacultativoCompensado;
	}

	public void setPontoFacultativoCompensado(Boolean pontoFacultativoCompensado) {
		this.pontoFacultativoCompensado = pontoFacultativoCompensado;
	}

	public Period getHorasCompensar() {
		return horasCompensar;
	}

	public void setHorasCompensar(Period horasCompensar) {
		this.horasCompensar = horasCompensar;
	}

	public Date getInicioCompensacao() {
		return inicioCompensacao;
	}

	public void setInicioCompensacao(Date inicioCompensacao) {
		this.inicioCompensacao = inicioCompensacao;
	}

	public Date getFimCompensacao() {
		return fimCompensacao;
	}

	public void setFimCompensacao(Date fimCompensacao) {
		this.fimCompensacao = fimCompensacao;
	}

	/**
	 * Mover esta regra para camada de negócio e não no entidades
	 */
//	public String getPeriod(){
//		PeriodFormatter formater = new PeriodFormatterBuilder().printZeroAlways().minimumPrintedDigits(1).appendHours().appendLiteral(":").minimumPrintedDigits(2).appendMinutes().toFormatter();
//		if ( this.getHorasCompensar() == null) {
//			return new String();
//		}
//		return formater.print(this.getHorasCompensar().normalizedStandard(PeriodType.time()));
//	}
//
//	public void setPeriod(String hora){
//		if (StringUtils.isNotBlank(hora)) {
//			if( this.getHorasCompensar() == null){
//				this.horasCompensar = new Period();
//			}
//			if (StringUtils.isNotBlank(hora.split(":")[0])) {
//				this.horasCompensar = this.getHorasCompensar().withHours(Integer.parseInt(hora.split(":")[0]));
//			}
//			if (hora.length() > 1 && StringUtils.isNotBlank(hora.split(":")[1])) {
//				this.horasCompensar = this.getHorasCompensar().withMinutes(Integer.parseInt(hora.split(":")[1]));
//			}
//		}
//	}
}
