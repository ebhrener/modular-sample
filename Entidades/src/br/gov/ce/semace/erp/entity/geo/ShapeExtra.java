package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "shape_extra")
public class ShapeExtra implements Serializable {
	
	private static final long serialVersionUID = -4079824823255809181L;
	
	@Id
	@SequenceGenerator(sequenceName="seq_shape_extra", name="seq_shape_extra", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_shape_extra")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="processo_id")
	private ProcessoGeo processo = new ProcessoGeo();
	
	@Column(nullable = false, unique = true, length = 100)
	private String tabela;
	
	@Column(nullable = false)
	private Boolean ativo = Boolean.TRUE;
	
	@OneToMany(mappedBy="shapeExtra", fetch=FetchType.LAZY)
	private List<HistoricoShapeExtra> historico;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProcessoGeo getProcesso() {
		return processo;
	}

	public void setProcesso(ProcessoGeo processo) {
		this.processo = processo;
	}

	public String getTabela() {
		return tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public List<HistoricoShapeExtra> getHistorico() {
		return historico;
	}

	public void setHistorico(List<HistoricoShapeExtra> historico) {
		this.historico = historico;
	}

}
