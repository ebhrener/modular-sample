package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * @author stevens
 *
 */

@Entity
@Audited
@Table(name="vinculo_empregaticio", schema="rh")
public class VinculoEmpregaticio implements Serializable{


	private static final long serialVersionUID = 8135969259938251398L;

	@Id
	@SequenceGenerator(sequenceName="rh.seq_vinculo", name="SEQVINCULO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQVINCULO")
	private Long id;

	@Column(nullable= false)
	private String vinculo;

	@Column(nullable = false)
	private boolean ativo;

	//------------------------------------------------------------------------Gets e Setters------------------------------------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVinculo() {
		return vinculo;
	}

	public void setVinculo(String vinculo) {
		this.vinculo = vinculo;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VinculoEmpregaticio other = (VinculoEmpregaticio) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return this.vinculo;
	}

}
