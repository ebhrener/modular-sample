package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.ArquivoDocumento;

@Entity
@Audited
@Table(name="arquivo_requerimento", schema="atendimento")
public class ArquivoRequerimento extends ArquivoDocumento {

	private static final long serialVersionUID = 2820723457900170725L;

	private Boolean validado;

	@JoinColumn(name = "requerimento_atividade_checklist_itemchecklist_id")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private RequerimentoAtividadeCheckListItemCheckList requerimentoAtividadeCheckListItemCheckList;

	public Boolean getValidado() {
		return validado;
	}

	public void setValidado(Boolean validado) {
		this.validado = validado;
	}

	public RequerimentoAtividadeCheckListItemCheckList getRequerimentoAtividadeCheckListItemCheckList() {
		return requerimentoAtividadeCheckListItemCheckList;
	}

	public void setRequerimentoAtividadeCheckListItemCheckList(RequerimentoAtividadeCheckListItemCheckList requerimentoAtividadeCheckListItemCheckList) {
		this.requerimentoAtividadeCheckListItemCheckList = requerimentoAtividadeCheckListItemCheckList;
	}

	@Transient
	public String getDisplayNome() {
		if(this.getNome() != null && !this.getNome().isEmpty()) {
			if(this.getNome().length() > 25) {
				String extension = this.getNome().substring(this.getNome().lastIndexOf('.'));
				return this.getNome().substring(0, 22).concat("...").concat(extension);
			}
			return this.getNome();
		}
		return "-";
	}

	@Transient
	public String getDisplayValidado() {
		if (Boolean.TRUE.equals(this.validado)) {
			return "Validado";
		} else if (Boolean.FALSE.equals(this.validado)) {
			return "Invalidado";
		} else {
			if (requerimentoAtividadeCheckListItemCheckList.getRequerimento().isDocumentacaoPendente()
					|| requerimentoAtividadeCheckListItemCheckList.getRequerimento().isAtendimentoPreAgendado()) {
				return "Aguardando Envio";
			}
			return "Aguardando Validação";
		}
	}

}