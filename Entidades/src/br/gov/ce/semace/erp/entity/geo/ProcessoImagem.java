package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "processo_imagem", schema = "public")
public class ProcessoImagem implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -9157789591555226593L;

	@Id
	@SequenceGenerator(sequenceName="seq_processo_imagem", name="seq_processo_imagem", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_processo_imagem")
	private Long id;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="processo_geo_id")
	private ProcessoGeo processoGeo;
	
	@Column(nullable=false)
	private String nome;
	
	@Column(name = "data_cadastro", nullable=false)
	private Date dataCadastro;
	
	@Column(name="usuario_cadastro_id")
	private Long usuarioCadastro;
	
	@Column(nullable=false)
	private Boolean ativo;
	
	@Column(name="usuario_desativacao_id")
	private Long usuarioDesativacao;
	
	@OneToOne(fetch=FetchType.LAZY, optional=false, cascade=CascadeType.ALL)
	@JoinColumn(name="imagem_id")
	private Imagem imagem;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Long getUsuarioCadastro() {
		return usuarioCadastro;
	}

	public void setUsuarioCadastro(Long usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Long getUsuarioDesativacao() {
		return usuarioDesativacao;
	}

	public void setUsuarioDesativacao(Long usuarioDesativacao) {
		this.usuarioDesativacao = usuarioDesativacao;
	}

	public ProcessoGeo getProcessoGeo() {
		return processoGeo;
	}

	public void setProcessoGeo(ProcessoGeo processoGeo) {
		this.processoGeo = processoGeo;
	}

	public Imagem getImagem() {
		return imagem;
	}

	public void setImagem(Imagem imagem) {
		this.imagem = imagem;
	}
}

