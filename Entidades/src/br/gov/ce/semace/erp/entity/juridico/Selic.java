package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

@Entity
@Audited
@Table(name="selic", schema="juridico")
public class Selic implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -7545006130755455749L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_selic", name="juridico.seq_selic", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_selic")
	private Long id;
	
	@Column(nullable=false, scale=5, precision=12)
	private BigDecimal valor = new BigDecimal(0.00);	
	
	@Column(nullable=false,unique=true)
	private Date data;
	
	@NotAudited
	@OneToMany(mappedBy="selic", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<HistoricoSelic> historico = new ArrayList<HistoricoSelic>();
	

	//------------------------------------------------Gets e Setters---------------------------------------------

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public List<HistoricoSelic> getHistorico() {
		return historico;
	}

	public void setHistorico(List<HistoricoSelic> historico) {
		this.historico = historico;
	}
	
}
