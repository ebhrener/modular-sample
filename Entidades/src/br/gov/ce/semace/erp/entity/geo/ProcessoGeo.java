package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "processo_geo", schema = "public")
public class ProcessoGeo implements Serializable {

	private static final long serialVersionUID = 3619209366312211229L;

	@Id
	@SequenceGenerator(sequenceName="seq_processo_geo", name="seq_processo_geo", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_processo_geo")
	private Long id;

	@Column(unique = true, length = 30)
	private String spu;

	@Column(length = 30, name = "numero_requerimento")
	private String numeroRequerimento;

	@Transient
	private Boolean hasShape;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "processo")
	private List<ShapeProcesso> shapeProcesso = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "processo")
	private List<ShapeExtra> shapeExtra = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "processo")
	private List<ShapeEquipamento> shapeEquipamento = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "processo")
	private List<ShapeOriginal> shapeOriginal = new ArrayList<>();

	@OneToMany(mappedBy="processoGeo")
	private List<ProcessoImagem> imagens = new ArrayList<>();

	public ProcessoGeo() {
		super();
	}

	public ProcessoGeo(String spu) {
		this();
		this.spu = spu;
	}

	public ProcessoGeo(String spu, String numeroRequerimento) {
		this();
		this.spu = spu;
		this.numeroRequerimento = numeroRequerimento;
	}

	public ProcessoGeo(String spu, String requerimento, Boolean hasShape) {
		this();
		this.spu = spu;
		this.numeroRequerimento = requerimento;
		this.hasShape = hasShape;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public List<ShapeProcesso> getShapeProcesso() {
		return shapeProcesso;
	}

	public void setShapeProcesso(List<ShapeProcesso> shapeProcesso) {
		this.shapeProcesso = shapeProcesso;
	}

	public String getNumeroRequerimento() {
		return numeroRequerimento;
	}

	public void setNumeroRequerimento(String numeroRequerimento) {
		this.numeroRequerimento = numeroRequerimento;
	}

	public Boolean getHasShape() {
		return hasShape;
	}

	public void setHasShape(Boolean hasShape) {
		this.hasShape = hasShape;
	}

	public List<ShapeExtra> getShapeExtra() {
		return shapeExtra;
	}

	public void setShapeExtra(List<ShapeExtra> shapeExtra) {
		this.shapeExtra = shapeExtra;
	}

	public List<ShapeEquipamento> getShapeEquipamento() {
		return shapeEquipamento;
	}

	public void setShapeEquipamento(List<ShapeEquipamento> shapeEquipamento) {
		this.shapeEquipamento = shapeEquipamento;
	}

	public List<ShapeOriginal> getShapeOriginal() {
		return shapeOriginal;
	}

	public void setShapeOriginal(List<ShapeOriginal> shapeOriginal) {
		this.shapeOriginal = shapeOriginal;
	}

	public List<ProcessoImagem> getImagens() {
		return imagens;
	}

	public void setImagens(List<ProcessoImagem> imagens) {
		this.imagens = imagens;
	}

}
