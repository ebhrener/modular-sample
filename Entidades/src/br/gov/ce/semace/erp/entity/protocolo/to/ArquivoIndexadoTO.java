package br.gov.ce.semace.erp.entity.protocolo.to;

import java.io.Serializable;
import java.math.BigInteger;

public class ArquivoIndexadoTO implements Serializable {

	private static final long serialVersionUID = -332373626993971021L;

	private Long id;

	private String tipoDocumento;

	private Integer pagina;

	private Integer numeroPaginas;

	private String justificativa;

	public ArquivoIndexadoTO() {
		super();
	}

	public ArquivoIndexadoTO(Long id, String tipoDocumento, Integer pagina, Integer numeroPaginas) {
		super();
		this.id = id;
		this.tipoDocumento = tipoDocumento;
		this.pagina = pagina;
		this.numeroPaginas = numeroPaginas;
	}

	public ArquivoIndexadoTO(BigInteger id, String tipoDocumento, Integer pagina, Integer numeroPaginas) {
		super();
		this.id = id != null ? id.longValue() : null;
		this.tipoDocumento = tipoDocumento;
		this.pagina = pagina;
		this.numeroPaginas = numeroPaginas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Integer getPagina() {
		return pagina;
	}

	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}

	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}

	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public String getDisplayPagina() {
		if (pagina != null && numeroPaginas != null) {
			if (numeroPaginas > 1) {
				return pagina + " - " + ((pagina + numeroPaginas) - 1);
			}
			return Integer.toString(pagina);
		}
		return "-";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ArquivoIndexadoTO other = (ArquivoIndexadoTO) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}