package br.gov.ce.semace.erp.entity.fiscalizacao.vo;

import br.gov.ce.semace.erp.enuns.TipoOcorrencia;

public class TipoPeriodo {
	private TipoOcorrencia tipoOcorrencia;
	private Integer mes;
	private Integer ano;
	private Long quantidade;
	
	public TipoPeriodo() {
		super();
	}

	public TipoPeriodo(TipoOcorrencia tipoOcorrencia, Integer mes,
			Integer ano, Long quantidade) {
		super();
		this.tipoOcorrencia = tipoOcorrencia;
		this.mes = mes;
		this.ano = ano;
		this.quantidade = quantidade;
	}

	public TipoOcorrencia getTipoOcorrencia() {
		return tipoOcorrencia;
	}

	public void setTipoOcorrencia(TipoOcorrencia tipoOcorrencia) {
		this.tipoOcorrencia = tipoOcorrencia;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}
}
