package br.gov.ce.semace.erp.entity.geral.to;

import java.io.Serializable;

public class PaginatorAssinadorDTO implements Serializable {

	private static final long serialVersionUID = 3146347902386160288L;

	private Long assinadorId;

	private String spu;

	private Long tipoDocumentoId;

	private Long start;

	private Integer limit;

	private String orderField;

	private String orderType;

	public PaginatorAssinadorDTO() {
		super();
	}

	public PaginatorAssinadorDTO(Long assinadorId, Long start, Integer limit, String orderField, String orderType) {
		super();
		this.assinadorId = assinadorId;
		this.start = start;
		this.limit = limit;
		this.orderField = orderField;
		this.orderType = orderType;
	}

	public PaginatorAssinadorDTO(Long assinadorId, String spu, Long tipoDocumentoId, Long start, Integer limit, String orderField, String orderType) {
		super();
		this.assinadorId = assinadorId;
		this.spu = spu;
		this.tipoDocumentoId = tipoDocumentoId;
		this.start = start;
		this.limit = limit;
		this.orderField = orderField;
		this.orderType = orderType;
	}

	public Long getAssinadorId() {
		return assinadorId;
	}

	public void setAssinadorId(Long assinadorId) {
		this.assinadorId = assinadorId;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public Long getTipoDocumentoId() {
		return tipoDocumentoId;
	}

	public void setTipoDocumentoId(Long tipoDocumentoId) {
		this.tipoDocumentoId = tipoDocumentoId;
	}

	public Long getStart() {
		return start;
	}

	public void setStart(Long start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

}
