package br.gov.ce.semace.erp.entity.transporte;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.planoviagem.SolicitacaoViagem;
import br.gov.ce.semace.erp.entity.rh.Setor;
import br.gov.ce.semace.erp.enuns.StatusSolicitacaoTransporte;
import br.gov.ce.semace.erp.enuns.TipoSolicitacaoTransporte;
import br.gov.ce.semace.erp.enuns.TransporteUtilizado;

/**
 * Entidade {@link SolicitacaoTransporte} do esquema Viagem.
 *
 * @author Harisson Rafael [harissonrafael@gmail.com] - 30/07/2014 15:55:57
 *
 */
@Entity
@Audited
@Table(schema = "transporte", name = "solicitacao_transporte")
public class SolicitacaoTransporte implements Serializable {

	private static final long serialVersionUID = 6806230960172602339L;

	@Id
	@SequenceGenerator(sequenceName = "transporte.seq_solicitacao_transporte", name = "SEQ_SOLICITACAO_TRANSPORTE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SOLICITACAO_TRANSPORTE")
	private Long id;

	@Column(name = "tipo_solicitacao_transporte")
	private TipoSolicitacaoTransporte tipoSolicitacaoTransporte;

	@Column(name = "status_solicitacao_transporte")
	private StatusSolicitacaoTransporte statusSolicitacaoTransporte;

	@Column(name = "data_inicial_proposta")
	private Date dataInicialProposta;

	@Column(name = "data_final_proposta")
	private Date dataFinalProposta;

	@Column(name = "transporte_utilizado")
	private TransporteUtilizado transporteUtilizado;

	@Column
	private String observacao;

	private boolean padrao = true;

	@Column (nullable = false)
	private String objetivo;

	@Column
	private String justificativa;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "tipo_objetivo_id")
	private TipoObjetivo tipoObjetivo;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "setor_id")
	private Setor setor;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "solicitacaoTransporte", optional = true)
	private OrdemTransporte ordemTransporte;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "solicitacaoTransporte")
	private List<SolicitacaoTransporteDestino> listSolicitacaoTransporteDestino = new ArrayList<>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "solicitacaoTransporte")
	private List<Passageiro> listPassageiro = new ArrayList<>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "solicitacaoTransporte")
	private List<ProcessoSolicitacaoTransporte> listProcessoSolicitacaoTransporte = new ArrayList<>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "solicitacaoTransporte")
	private List<SolicitacaoViagem> listSolicitacaoViagem;

	@Transient
	private List<Setor> setoresFilho;

	@Transient
    public boolean isSolicitacaoNacional() {
		return this.tipoSolicitacaoTransporte.isNacional();
    }

	@Transient
    public boolean isSolicitacaoEstadual() {
		return this.tipoSolicitacaoTransporte.isEstadual();
    }

	@Transient
    public boolean isSolicitacaoInternacional() {
		return this.tipoSolicitacaoTransporte.isInternacional();
    }

	@Transient
    public boolean isSolicitacaoRegional() {
		return this.tipoSolicitacaoTransporte.isRegional();
    }

	@Transient
    public Date inicioPeriodoDataInicialProposta;

	@Transient
    public Date fimPeriodoDataInicialProposta;


	@Transient
    public boolean pagaDiaria() {
		return TipoSolicitacaoTransporte.getTipoSolicitacaoPagaDiaria().contains(this.tipoSolicitacaoTransporte);
    }

	@Transient
	public String getDetalhePadrao(){

		if (this.padrao){
			return "Sim";
		}

		return "Não";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoSolicitacaoTransporte getTipoSolicitacaoTransporte() {
		return tipoSolicitacaoTransporte;
	}

	public void setTipoSolicitacaoTransporte(TipoSolicitacaoTransporte tipoSolicitacaoTransporte) {
		this.tipoSolicitacaoTransporte = tipoSolicitacaoTransporte;
	}

	public StatusSolicitacaoTransporte getStatusSolicitacaoTransporte() {
		return statusSolicitacaoTransporte;
	}

	public void setStatusSolicitacaoTransporte(StatusSolicitacaoTransporte statusSolicitacaoTransporte) {
		this.statusSolicitacaoTransporte = statusSolicitacaoTransporte;
	}

	public Date getDataInicialProposta() {
		return dataInicialProposta;
	}

	public void setDataInicialProposta(Date dataInicialProposta) {
		this.dataInicialProposta = dataInicialProposta;
	}

	public Date getDataFinalProposta() {
		return dataFinalProposta;
	}

	public void setDataFinalProposta(Date dataFinalProposta) {
		this.dataFinalProposta = dataFinalProposta;
	}

	public TransporteUtilizado getTransporteUtilizado() {
		return transporteUtilizado;
	}

	public void setTransporteUtilizado(TransporteUtilizado transporteUtilizado) {
		this.transporteUtilizado = transporteUtilizado;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public boolean isPadrao() {
		return padrao;
	}

	public void setPadrao(boolean padrao) {
		this.padrao = padrao;
	}

	public String getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public TipoObjetivo getTipoObjetivo() {
		return tipoObjetivo;
	}

	public void setTipoObjetivo(TipoObjetivo tipoObjetivo) {
		this.tipoObjetivo = tipoObjetivo;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public OrdemTransporte getOrdemTransporte() {
		return ordemTransporte;
	}

	public void setOrdemTransporte(OrdemTransporte ordemTransporte) {
		this.ordemTransporte = ordemTransporte;
	}

	public List<SolicitacaoTransporteDestino> getListSolicitacaoTransporteDestino() {
		return listSolicitacaoTransporteDestino;
	}

	public void setListSolicitacaoTransporteDestino(List<SolicitacaoTransporteDestino> listSolicitacaoTransporteDestino) {
		this.listSolicitacaoTransporteDestino = listSolicitacaoTransporteDestino;
	}

	public List<Passageiro> getListPassageiro() {
		return listPassageiro;
	}

	public void setListPassageiro(List<Passageiro> listPassageiro) {
		this.listPassageiro = listPassageiro;
	}

	public List<ProcessoSolicitacaoTransporte> getListProcessoSolicitacaoTransporte() {
		return listProcessoSolicitacaoTransporte;
	}

	public void setListProcessoSolicitacaoTransporte(
			List<ProcessoSolicitacaoTransporte> listProcessoSolicitacaoTransporte) {
		this.listProcessoSolicitacaoTransporte = listProcessoSolicitacaoTransporte;
	}

	public List<Setor> getSetoresFilho() {
		return setoresFilho;
	}

	public void setSetoresFilho(List<Setor> setoresFilho) {
		this.setoresFilho = setoresFilho;
	}

	public List<SolicitacaoViagem> getListSolicitacaoViagem() {
		return listSolicitacaoViagem;
	}

	public void setListSolicitacaoViagem(
			List<SolicitacaoViagem> listSolicitacaoViagem) {
		this.listSolicitacaoViagem = listSolicitacaoViagem;
	}

	public Date getInicioPeriodoDataInicialProposta() {
		return inicioPeriodoDataInicialProposta;
	}

	public void setInicioPeriodoDataInicialProposta(Date inicioPeriodoDataInicialProposta) {
		this.inicioPeriodoDataInicialProposta = inicioPeriodoDataInicialProposta;
	}

	public Date getFimPeriodoDataInicialProposta() {
		return fimPeriodoDataInicialProposta;
	}

	public void setFimPeriodoDataInicialProposta(Date fimPeriodoDataInicialProposta) {
		this.fimPeriodoDataInicialProposta = fimPeriodoDataInicialProposta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SolicitacaoTransporte other = (SolicitacaoTransporte) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}