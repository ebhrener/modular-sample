package br.gov.ce.semace.erp.entity.geral.to;

import br.gov.ce.semace.erp.utils.StringUtils;

public class RotaTO {

	private String latitudeOrigem;

	private String longitudeOrigem;

	private String cidadeOrigem;

	private String ufOrigem;

	private String latitudeDestino;

	private String longitudeDestino;

	private String cidadeDestino;

	private String ufDestino;

	public String getLatitudeOrigem() {
		return latitudeOrigem;
	}

	public void setLatitudeOrigem(String latitudeOrigem) {
		this.latitudeOrigem = latitudeOrigem;
	}

	public String getLongitudeOrigem() {
		return longitudeOrigem;
	}

	public void setLongitudeOrigem(String longitudeOrigem) {
		this.longitudeOrigem = longitudeOrigem;
	}

	public String getCidadeOrigem() {
		return cidadeOrigem;
	}

	public void setCidadeOrigem(String cidadeOrigem) {
		this.cidadeOrigem = cidadeOrigem;
	}

	public String getUfOrigem() {
		return ufOrigem;
	}

	public void setUfOrigem(String ufOrigem) {
		this.ufOrigem = ufOrigem;
	}

	public String getLatitudeDestino() {
		return latitudeDestino;
	}

	public void setLatitudeDestino(String latitudeDestino) {
		this.latitudeDestino = latitudeDestino;
	}

	public String getLongitudeDestino() {
		return longitudeDestino;
	}

	public void setLongitudeDestino(String longitudeDestino) {
		this.longitudeDestino = longitudeDestino;
	}

	public String getCidadeDestino() {
		return cidadeDestino;
	}

	public void setCidadeDestino(String cidadeDestino) {
		this.cidadeDestino = cidadeDestino;
	}

	public String getUfDestino() {
		return ufDestino;
	}

	public void setUfDestino(String ufDestino) {
		this.ufDestino = ufDestino;
	}

	public String getFormattedOrigem() {

		if (StringUtils.isNotBlank(latitudeOrigem) && StringUtils.isNotBlank(longitudeOrigem)) {
			return latitudeOrigem + "," + longitudeOrigem;
		} else if (StringUtils.isNotBlank(cidadeOrigem) && StringUtils.isNotBlank(ufOrigem)) {
			return cidadeOrigem + "," + ufOrigem;
		}

		return "";
	}

	public String getFormattedDestino() {

		if (StringUtils.isNotBlank(latitudeDestino) && StringUtils.isNotBlank(longitudeDestino)) {
			return latitudeDestino + "," + longitudeDestino;
		} else if (StringUtils.isNotBlank(cidadeDestino) && StringUtils.isNotBlank(ufDestino)) {
			return cidadeDestino + "," + ufDestino;
		}

		return "";
	}

}
