package br.gov.ce.semace.erp.entity.geral;

import java.util.ArrayList;
import java.util.List;

public enum EspacoGeografico {

	ZONA_URBANA(1L, "Zona Urbana"), ZONA_RURAL(2L, "Zona Rural");

	private Long indice;
	private String descricao;

	private EspacoGeografico(Long indice, String descricao) {
		this.indice = indice;
		this.descricao = descricao;
	}

	public Long getIndice() {
		return indice;
	}

	public String getDescricao() {
		return descricao;
	}

	public static List<EspacoGeografico> listEspacos() {
		List<EspacoGeografico> listEspacos = new ArrayList<EspacoGeografico>();
		listEspacos.add(ZONA_RURAL);
		listEspacos.add(ZONA_URBANA);
		return listEspacos;
	}

}
