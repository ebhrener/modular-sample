package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(schema = "financeiro", name = "log_erro_boleto")
public class LogErroBoleto implements Serializable {
	
	private static final long serialVersionUID = 1469642562733546445L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_log_erro_boleto", name = "SEQ_LOG_ERRO_BOLETO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LOG_ERRO_BOLETO")
	private Long id;

	@Column(name="path_arquivo", nullable = false)
	private String pathArquivo;
	
	@Column(name="nome_arquivo", nullable = false)
	private String nomeArquivo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao", nullable = false)
	private Date dataCriacao;
	
	@Column(name="descricao_erro", columnDefinition="text")
	private String descricaoErro;
	
	@Column(name="numero_boleto")
	private String numeroBoleto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPathArquivo() {
		return pathArquivo;
	}

	public void setPathArquivo(String pathArquivo) {
		this.pathArquivo = pathArquivo;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getDescricaoErro() {
		return descricaoErro;
	}

	public void setDescricaoErro(String descricaoErro) {
		this.descricaoErro = descricaoErro;
	}

	public String getNumeroBoleto() {
		return numeroBoleto;
	}

	public void setNumeroBoleto(String numeroBoleto) {
		this.numeroBoleto = numeroBoleto;
	}

}
