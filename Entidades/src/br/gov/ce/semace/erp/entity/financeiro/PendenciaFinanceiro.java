package br.gov.ce.semace.erp.entity.financeiro;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Pendencia;
import br.gov.ce.semace.erp.entity.licenciamento.ItensCheckList;


@Entity
@Audited
@Table(schema="financeiro", name="pendencia_financeiro")
public class PendenciaFinanceiro extends Pendencia{

	private static final long serialVersionUID = 6394393996647654381L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="dae_id")
	private Dae dae = new Dae();

	public Dae getDae() {
		return dae;
	}

	public void setDae(Dae dae) {
		this.dae = dae;
	}

	/**
	 * Utilizado para visualização do item da pendência, para ficar similar ao {@link ItensCheckList}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 29/10/2013 11:16:19
	 *
	 * @return retorna o titulo do da {@link Pendencia}
	 */
	@Override
	public String getTitulo() {
		return "";
	}

}
