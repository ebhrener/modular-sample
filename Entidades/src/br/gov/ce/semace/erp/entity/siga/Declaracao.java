package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;

public class Declaracao implements Serializable{

	private static final long serialVersionUID = -1936275848899354123L;

	private String processoSigaID;

	public String getProcessoSigaID() {
		return processoSigaID;
	}

	public void setProcessoSigaID(String processoSigaID) {
		this.processoSigaID = processoSigaID;
	}

}
