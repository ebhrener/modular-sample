package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="legenda")
public class Legenda implements Serializable {

	private static final long serialVersionUID = 4430154538515894807L;

	@Id
	@SequenceGenerator(sequenceName="seq_legenda", name="seq_legenda", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "seq_legenda")
	private Long id;
	
	@Column(unique = true, nullable = false)
	private String nome;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "legenda")
	private List<BaseCartografica> baseCartografica = new ArrayList<BaseCartografica>();
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<BaseCartografica> getBaseCartografica() {
		return baseCartografica;
	}

	public void setBaseCartografica(List<BaseCartografica> baseCartografica) {
		this.baseCartografica = baseCartografica;
	}
	
}