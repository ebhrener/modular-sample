	package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="geral", name="empreendimento_atividade")
public class EmpreendimentoAtividade implements Serializable{

	private static final long serialVersionUID = -1897660073476255737L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_empreendimento_atividade", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="empreendimento_id")
	private Empreendimento empreendimento = new Empreendimento();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="atividade_id")
	private Atividade atividade = new Atividade();

	@Column(name="quantidade")
	private Double quantidade;

	@Column(name="ativo")
	private Boolean ativo = true;

	@Column(name="faturamento")
	private BigDecimal faturamento;

	@Column(name="area_construida")
	private BigDecimal areaConstruida;

	@Column(name="quantidade_funcionario")
	private Integer quantidadeFuncionario;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "empreendimentoAtividade", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ValorGrandezaEmpreendimentoAtividade> listValorGrandezaEmpreendimentoAtividade = new ArrayList<ValorGrandezaEmpreendimentoAtividade>();

	@Column(name="selecionado")
	private boolean selecionado = false;

	public Empreendimento getEmpreendimento() {
		return empreendimento;
	}

	public void setEmpreendimento(Empreendimento empreendimento) {
		this.empreendimento = empreendimento;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public BigDecimal getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(BigDecimal faturamento) {
		this.faturamento = faturamento;
	}

	public BigDecimal getAreaConstruida() {
		return areaConstruida;
	}

	public void setAreaConstruida(BigDecimal areaConstruida) {
		this.areaConstruida = areaConstruida;
	}

	public Integer getQuantidadeFuncionario() {
		return quantidadeFuncionario;
	}

	public void setQuantidadeFuncionario(Integer quantidadeFuncionario) {
		this.quantidadeFuncionario = quantidadeFuncionario;
	}

	public List<ValorGrandezaEmpreendimentoAtividade> getListValorGrandezaEmpreendimentoAtividade() {
		return listValorGrandezaEmpreendimentoAtividade;
	}

	public void setListValorGrandezaEmpreendimentoAtividade(List<ValorGrandezaEmpreendimentoAtividade> listValorGrandezaEmpreendimentoAtividade) {
		this.listValorGrandezaEmpreendimentoAtividade = listValorGrandezaEmpreendimentoAtividade;
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		EmpreendimentoAtividade ea = ((EmpreendimentoAtividade)obj);
		Atividade atv = ea.getAtividade();

		if(ea.getId() == null){
			Atividade atvAtual = this.getAtividade();
			if(atvAtual.getId().equals(atv.getId())){
				return true;
			}
		} else {
			if(this.getId() != null && this.getId().equals(ea.getId())){
				return true;
			}
		}
		return false;
	}


}
