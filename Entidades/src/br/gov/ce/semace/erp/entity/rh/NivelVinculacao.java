package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;


/**
 * Entidade {@link NivelVinculacao} do esquema RH.
 *
 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 16/04/2014 16:15:26
 */
@Entity
@Audited
@Table(schema="rh", name="nivel_vinculacao", uniqueConstraints = { @UniqueConstraint(columnNames = {"empresa_id", "sindicato_id", "vinculo_empregaticio_id"}) })
public class NivelVinculacao implements Serializable{

	private static final long serialVersionUID = 6446630413854310415L;

	@Id
	@SequenceGenerator(sequenceName="rh.seq_nivel_vinculacao", name="SEQNIVELVINCULACAO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQNIVELVINCULACAO")
	private Long id;

	private String descricao;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="empresa_id", referencedColumnName="id")
	private Empresa empresa = new Empresa();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="sindicato_id", referencedColumnName="id")
	private Sindicato sindicato = new Sindicato();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="vinculo_empregaticio_id", referencedColumnName="id", nullable=false)
	private VinculoEmpregaticio vinculoEmpregaticio = new VinculoEmpregaticio();

	@Column(name="semace_paga")
	private boolean semacePaga = true;

	@Column(name="ativo")
	private boolean ativo = true;

	//================================== GETERS and SETERS ====================================//

	@Transient
	public String getDetalheAtivo(){

		if (this.ativo){
			return "Sim";
		}

		return "Não";
	}

	@Transient
	public String getDetalheSemacePaga(){

		if (this.semacePaga){
			return "Sim";
		}

		return "Não";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Sindicato getSindicato() {
		return sindicato;
	}

	public void setSindicato(Sindicato sindicato) {
		this.sindicato = sindicato;
	}

	public VinculoEmpregaticio getVinculoEmpregaticio() {
		return vinculoEmpregaticio;
	}

	public void setVinculoEmpregaticio(VinculoEmpregaticio vinculoEmpregaticio) {
		this.vinculoEmpregaticio = vinculoEmpregaticio;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isSemacePaga() {
		return semacePaga;
	}

	public void setSemacePaga(boolean semacePaga) {
		this.semacePaga = semacePaga;
	}

}
