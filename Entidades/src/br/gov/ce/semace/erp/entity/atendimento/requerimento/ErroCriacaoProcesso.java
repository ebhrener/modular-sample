package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="erro_criacao_processo", schema="atendimento")
public class ErroCriacaoProcesso implements Serializable{

	private static final long serialVersionUID = -6555018580986105396L;

	@Id @SequenceGenerator(sequenceName="atendimento.seq_erro_criacao_processo", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="agendamento_requerimento_id")
	private AgendamentoRequerimento agendamentoRequerimento;

	@Column(name="qtd_erro")
	private int qtd;

	@Column(name="pilha",columnDefinition="text")
	private String pilha;

	@Column(name="data_hora")
	private Date data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AgendamentoRequerimento getAgendamentoRequerimento() {
		return agendamentoRequerimento;
	}

	public void setAgendamentoRequerimento(AgendamentoRequerimento agendamentoRequerimento) {
		this.agendamentoRequerimento = agendamentoRequerimento;
	}

	public int getQtd() {
		return qtd;
	}

	public void setQtd(int qtd) {
		this.qtd = qtd;
	}

	public String getPilha() {
		return pilha;
	}

	public void setPilha(String pilha) {
		this.pilha = pilha;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

}
