package br.gov.ce.semace.erp.entity.viproc;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * Entidade para mapeamento das etiquetas de número de processo disponibilizadas pelo ViPROC
 * 
 * @author Victor Hugo R. Rodrigues [victor.hugorr@gmail.com]
 *
 */
@Entity
@Table(name="etiqueta_numero_spu", schema="viproc")
public class EtiquetaNumeroSpu implements Serializable {	
		
	private static final long serialVersionUID = -1742562641151781357L;

	@Id
	@SequenceGenerator(sequenceName="viproc.seq_etiqueta_numero_spu", name="SEQETINUMSPU", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQETINUMSPU")
	private Long id;	
	
	@Column
	private String spu;
	
	private boolean disponivel = true;
	
	//------------------------------------ Get's e Set's ----------------------------------------------

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSpu() {
		return spu;
	}
	
	public void setSpu(String spu) {
		this.spu = spu;
	}
	
	public boolean isDisponivel() {
		return disponivel;
	}
	
	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EtiquetaNumeroSpu other = (EtiquetaNumeroSpu) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}