package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="condicao_climatica", schema="laboratorio")
public class CondicaoClimatica implements Serializable {
	
	private static final long serialVersionUID = 6020141948066584309L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_condicao_climatica", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	private String descricao;
	
	@OneToMany(mappedBy="condicaoClimatica")
	private List<Coleta> coletas = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Coleta> getColetas() {
		return coletas;
	}

	public void setColetas(List<Coleta> coletas) {
		this.coletas = coletas;
	}
	
}
