package br.gov.ce.semace.erp.entity.atendimento.requerimento.to;

import java.io.Serializable;
import java.util.Date;

public class ClienteTO implements Serializable{

	private static final long serialVersionUID = 8444634445159320310L;
	
	private String nome;
	
	private String documento;
	
	private Date dataValidadeSpu;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Date getDataValidadeSpu() {
		return dataValidadeSpu;
	}

	public void setDataValidadeSpu(Date dataValidadeSpu) {
		this.dataValidadeSpu = dataValidadeSpu;
	}

}
