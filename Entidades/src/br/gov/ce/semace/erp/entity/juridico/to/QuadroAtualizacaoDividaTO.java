package br.gov.ce.semace.erp.entity.juridico.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class QuadroAtualizacaoDividaTO implements Serializable {

	private static final long serialVersionUID = 2975196982060270372L;

	private BigDecimal valorInicial = BigDecimal.valueOf(0.00d);
	private BigDecimal valorCorrigido = BigDecimal.valueOf(0.00d);
	private BigDecimal multaMora = BigDecimal.valueOf(0.00d);
	private BigDecimal jurosMora = BigDecimal.valueOf(0.00d);
	private BigDecimal honorarios = BigDecimal.valueOf(0.00d);
	private BigDecimal valorTotal = BigDecimal.valueOf(0.00d);

	private BigDecimal valorCorrigidoPerCent = BigDecimal.valueOf(0.00d);
	private BigDecimal multaMoraPerCent = BigDecimal.valueOf(0.00d);
	private BigDecimal jurosMoraPerCent = BigDecimal.valueOf(0.00d);
	private BigDecimal honorariosPerCent = BigDecimal.valueOf(0.00d);
	private BigDecimal valorTotalPerCent = BigDecimal.valueOf(100.00d);

	private BigDecimal valorTotalPartial = BigDecimal.valueOf(0.00d);

	public QuadroAtualizacaoDividaTO() {
		super();
	}

	public QuadroAtualizacaoDividaTO(BigDecimal valorInicial, BigDecimal valorCorrigido, BigDecimal multaMora, BigDecimal jurosMora, BigDecimal honorarios, BigDecimal valorTotal) {
		super();
		this.valorInicial = valorInicial;
		this.valorCorrigido = valorCorrigido;
		this.multaMora = multaMora;
		this.jurosMora = jurosMora;
		this.honorarios = honorarios;
		this.valorTotal = valorTotal;
		computePerCentFields();
	}

	public QuadroAtualizacaoDividaTO(BigDecimal valorTotal) {
		super();
		this.valorTotal = valorTotal;
	}

	public void computePerCentFields() throws IllegalStateException {

		if(valorTotal.compareTo(BigDecimal.ZERO) == 0) {
			throw new IllegalStateException("Divisão por zero.");
		}

		valorCorrigidoPerCent = valorCorrigido.divide(valorTotal, 5, RoundingMode.DOWN).multiply(BigDecimal.valueOf(100l)).setScale(2, RoundingMode.DOWN);
		multaMoraPerCent = multaMora.divide(valorTotal, 5, RoundingMode.DOWN).multiply(BigDecimal.valueOf(100.00d)).setScale(2, RoundingMode.DOWN);
		jurosMoraPerCent = jurosMora.divide(valorTotal, 5, RoundingMode.DOWN).multiply(BigDecimal.valueOf(100.00d)).setScale(2, RoundingMode.DOWN);
		honorariosPerCent = honorarios.divide(valorTotal, 5, RoundingMode.DOWN).multiply(BigDecimal.valueOf(100.00d)).setScale(2, RoundingMode.DOWN);
	}

	public void computePartialPerCentFields() {

		if(valorTotalPartial.compareTo(BigDecimal.ZERO) == 0) {
			return;
		}

		valorCorrigidoPerCent = valorCorrigido.divide(valorTotalPartial, 5, RoundingMode.DOWN).multiply(BigDecimal.valueOf(100l)).setScale(2, RoundingMode.DOWN);
		multaMoraPerCent = multaMora.divide(valorTotalPartial, 5, RoundingMode.DOWN).multiply(BigDecimal.valueOf(100.00d)).setScale(2, RoundingMode.DOWN);
		jurosMoraPerCent = jurosMora.divide(valorTotalPartial, 5, RoundingMode.DOWN).multiply(BigDecimal.valueOf(100.00d)).setScale(2, RoundingMode.DOWN);
		honorariosPerCent = honorarios.divide(valorTotalPartial, 5, RoundingMode.DOWN).multiply(BigDecimal.valueOf(100.00d)).setScale(2, RoundingMode.DOWN);
	}

	public void computeValorTotalPartial() {
		valorTotalPartial = valorCorrigido.add(multaMora).add(jurosMora).add(honorarios);
		computePartialPerCentFields();
	}

	public BigDecimal getValorInicial() {
		return valorInicial;
	}

	public void setValorInicial(BigDecimal valorInicial) {
		this.valorInicial = valorInicial;
	}

	public BigDecimal getValorCorrigido() {
		return valorCorrigido;
	}

	public void setValorCorrigido(BigDecimal valorCorrigido) {
		this.valorCorrigido = valorCorrigido;
	}

	public BigDecimal getMultaMora() {
		return multaMora;
	}

	public void setMultaMora(BigDecimal multaMora) {
		this.multaMora = multaMora;
	}

	public BigDecimal getJurosMora() {
		return jurosMora;
	}

	public void setJurosMora(BigDecimal jurosMora) {
		this.jurosMora = jurosMora;
	}

	public BigDecimal getHonorarios() {
		return honorarios;
	}

	public void setHonorarios(BigDecimal honorarios) {
		this.honorarios = honorarios;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public BigDecimal getValorCorrigidoPerCent() {
		return valorCorrigidoPerCent;
	}

	public BigDecimal getMultaMoraPerCent() {
		return multaMoraPerCent;
	}

	public BigDecimal getJurosMoraPerCent() {
		return jurosMoraPerCent;
	}

	public BigDecimal getHonorariosPerCent() {
		return honorariosPerCent;
	}

	public BigDecimal getValorTotalPerCent() {
		return valorTotalPerCent;
	}

	public BigDecimal getValorTotalPartial() {
		return valorTotalPartial;
	}

}