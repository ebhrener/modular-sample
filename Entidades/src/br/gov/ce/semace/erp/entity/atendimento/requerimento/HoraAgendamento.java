package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.io.Serializable;
import java.sql.Time;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Sede;

@Entity
@Audited
@Table(name="hora_agendamento", schema="atendimento")
public class HoraAgendamento implements Serializable{


	private static final long serialVersionUID = -6661903421508873006L;

	@Id @SequenceGenerator(sequenceName="atendimento.seq_hora_agendamento", name="atendimento.seq_hora_agendamento", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="atendimento.seq_hora_agendamento")
	private Long id;

	private Time hora;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sede_id")
	private Sede sede = new Sede();

	/**
	 * Campo indicando se a {@link HoraAgendamento} está ativa ou não para ser visualizada.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 28/07/2015 11:11:36
	 */
	private boolean ativo;

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Time getHora() {
		return hora;
	}

	public void setHora(Time hora) {
		this.hora = hora;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

}