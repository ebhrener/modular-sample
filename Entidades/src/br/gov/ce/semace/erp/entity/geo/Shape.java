package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;

/**
 * Classe de tratamento de shape
 * 
 * Status
 * 1 : Não iniciado
 * 2 : Processando
 * 3 : Finalizado
 * 4 : Cancelado
 * 5 : Erro
 * 
 * @author Victor Sobreira [victorcsv@gmail.com] em 29/07/2013 as 15:15:21
 * @version 1.1
 */
public class Shape implements Serializable{

	private static final long serialVersionUID = -2636558581973614445L;

	private String nome;
	private String srid;
	
	private String tipo;
	private String canonicalPath;
	private String tempPath;
	
	private String schema;
	private String tabela;
	
	private String numeroDocumento;
	private String numeroSpu;
	private String numeroProcesso;
	private String numeroRequerimento;
	
	private String interessado;
	private String descricao;
	private String dataCadastro;

	public String getSrid() {
		return srid;
	}

	public void setSrid(String srid) {
		this.srid = srid;
	}

	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public String getNumeroSpu() {
		return numeroSpu;
	}

	public void setNumeroSpu(String numeroSpu) {
		this.numeroSpu = numeroSpu;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getTabela() {
		return tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCanonicalPath() {
		return canonicalPath;
	}

	public void setCanonicalPath(String canonicalPath) {
		this.canonicalPath = canonicalPath;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getInteressado() {
		return interessado;
	}

	public void setInteressado(String interessado) {
		this.interessado = interessado;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	public String getNumeroRequerimento() {
		return numeroRequerimento;
	}

	public void setNumeroRequerimento(String numeroRequerimento) {
		this.numeroRequerimento = numeroRequerimento;
	}

}

