package br.gov.ce.semace.erp.entity.julgamento;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.DocumentoQuestionario;
import br.gov.ce.semace.erp.enuns.TipoParecer;
import br.gov.ce.semace.erp.utils.DateUtils;

@Entity
@Audited
@Table(name="parecer", schema="julgamento")
public class Parecer extends DocumentoQuestionario{

	private static final long serialVersionUID = 2714450196948413462L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "julgamento_id")
	private Julgamento julgamento = new Julgamento();

	private boolean finalizado = false;

	@Column(name="tipo_parecer")
	private TipoParecer tipoParecer;

	private boolean ativo = true;

	@Column(name="prazo_alegacao_encerrado")
	private boolean prazoAlegacaoEncerrado = false;

	@Temporal(TemporalType.DATE)
	@Column(name="data_conclusao")
	private Date dataConclusao;

	@Temporal(TemporalType.TIME)
	@Column(name="hora_conclusao")
	private Date horaConclusao;

	@Column(name = "valor_sugerido", precision=15, scale=2)
	private BigDecimal valorSugerido;

	@OneToMany(mappedBy = "parecer", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ParecerMajoracao> listParecerMajoracao = new ArrayList<>();

	/**
	 * Fornece os campos {@link #dataConclusao} e {@link #horaConclusao} formatados.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 31/07/2013 10:08:17
	 *
	 * @return retorna uma {@link String} com de acordo com o padrão {@link DateUtils#BRAZIL_FORMAT} para a {@link #dataConclusao} e
	 * {@link DateUtils#BRAZIL_SHORT_TIME_FORMAT} para a {@link #horaConclusao}, concatenados e separados por um espaço em branco.
	 */
	@Transient
	public String getDataHoraConclusaoFormatada(){

		if (this.dataConclusao != null && this.horaConclusao != null) {
			// formata a data
			final String dataFormatada = DateUtils.toString(this.dataConclusao);
			// formata o tempo
			final String horaFormatada = DateUtils.getShortTime(this.horaConclusao);
			// concatena e separa por espaços a string a ser retornada
			return dataFormatada + "   às " + horaFormatada;
		}
		return "";
	}

	//------------------------------------------Gets e Setters----------------------------------------------------

	public Julgamento getJulgamento() {
		return julgamento;
	}

	public void setJulgamento(Julgamento julgamento) {
		this.julgamento = julgamento;
	}

	public TipoParecer getTipoParecer() {
		return tipoParecer;
	}

	public void setTipoParecer(TipoParecer tipoParecer) {
		this.tipoParecer = tipoParecer;
	}

	public Date getDataConclusao() {
		return dataConclusao;
	}

	public void setDataConclusao(Date dataConclusao) {
		this.dataConclusao = dataConclusao;
	}

	public Date getHoraConclusao() {
		return horaConclusao;
	}

	public void setHoraConclusao(Date horaConclusao) {
		this.horaConclusao = horaConclusao;
	}

	public boolean isFinalizado() {
		return finalizado;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isPrazoAlegacaoEncerrado() {
		return prazoAlegacaoEncerrado;
	}

	public void setPrazoAlegacaoEncerrado(boolean prazoAlegacaoEncerrado) {
		this.prazoAlegacaoEncerrado = prazoAlegacaoEncerrado;
	}

	public BigDecimal getValorSugerido() {
		return valorSugerido;
	}

	public void setValorSugerido(BigDecimal valorSugerido) {
		this.valorSugerido = valorSugerido;
	}

	public List<ParecerMajoracao> getListParecerMajoracao() {
		return listParecerMajoracao;
	}

	public void setListParecerMajoracao(List<ParecerMajoracao> listParecerMajoracao) {
		this.listParecerMajoracao = listParecerMajoracao;
	}

}
