package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.GrupoAtividade;


@Entity
@Audited
@Table(name = "configuracao_ppd_grupo_atividade", schema = "licenciamento")
public class ConfiguracaoPPDGrupoAtividade implements Serializable{

	private static final long serialVersionUID = 1219622264197309123L;

	@Id
	@SequenceGenerator(sequenceName="licenciamento.seq_config_ppd_grupo_atividade",name="licenciamento.seq_config_ppd_grupo_atividade", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="licenciamento.seq_config_ppd_grupo_atividade")
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="grupo_atividade_id")
	private GrupoAtividade grupoAtividade;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="potencial_poluidor_id")
	private PotencialPoluidor potencialPoluidor;

	@Column(name="qtd_horas_trabalhadas")
	private Long qtdHorasTrabalhadas;

	@Column(name="numero_tecnico")
	private Long numeroTecnico;

	@Column(name="ativo")
	private boolean ativo;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="tipo_estudo_id")
	private TipoEstudo tipoEstudo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GrupoAtividade getGrupoAtividade() {
		return grupoAtividade;
	}

	public void setGrupoAtividade(GrupoAtividade grupoAtividade) {
		this.grupoAtividade = grupoAtividade;
	}

	public PotencialPoluidor getPotencialPoluidor() {
		return potencialPoluidor;
	}

	public void setPotencialPoluidor(PotencialPoluidor potencialPoluidor) {
		this.potencialPoluidor = potencialPoluidor;
	}

	public Long getQtdHorasTrabalhadas() {
		return qtdHorasTrabalhadas;
	}

	public void setQtdHorasTrabalhadas(Long qtdHorasTrabalhadas) {
		this.qtdHorasTrabalhadas = qtdHorasTrabalhadas;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Long getNumeroTecnico() {
		return numeroTecnico;
	}

	public void setNumeroTecnico(Long numeroTecnico) {
		this.numeroTecnico = numeroTecnico;
	}

	public TipoEstudo getTipoEstudo() {
		return tipoEstudo;
	}

	public void setTipoEstudo(TipoEstudo tipoEstudo) {
		this.tipoEstudo = tipoEstudo;
	}

}
