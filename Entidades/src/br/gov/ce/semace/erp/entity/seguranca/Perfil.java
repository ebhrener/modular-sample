package br.gov.ce.semace.erp.entity.seguranca;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="perfil", schema="seguranca")
public class Perfil implements Serializable{

	private static final long serialVersionUID = 6354872118499698278L;
	
	@Id
	@SequenceGenerator(sequenceName = "seguranca.seq_perfil", name = "SEQUSER", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUSER")
	private Long id;
	
	@Column(name="nome_perfil")
	private String nomePerfil;
	
	@Column(columnDefinition = "text")
	private String permissoes;
	
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="perfis")
	private List<Usuario> usuarios = new ArrayList<Usuario>();

	public boolean hasPermissao(String role) {
		String[] p = getPermissoesArray();
		//TODO: Investigar forma mais barata de executar o código abaixo
		return p == null ? false : Arrays.asList(p).contains(role);
	}

	public void setPermissoes(String permissoes) {
		this.permissoes = permissoes.replace("[", "").replace("]", "")
				.replace(" ", "");
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Perfil other = (Perfil) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	/*
	 * Métodos getters e setters 
	 */
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNomePerfil() {
		return nomePerfil;
	}

	public void setNomePerfil(String nomePerfil) {
		this.nomePerfil = nomePerfil;
	}

	public String getPermissoes() {
		return permissoes;
	}

	public String[] getPermissoesArray() {
		return permissoes == null ? null : permissoes.split(",");
	}

}
