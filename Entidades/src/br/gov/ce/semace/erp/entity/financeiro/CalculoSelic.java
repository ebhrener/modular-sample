package br.gov.ce.semace.erp.entity.financeiro;

import java.math.BigDecimal;

/**
 * Classe usada para transportar todos os valores usados no calculo;
 * 
 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 16/07/2014
 *
 */
public class CalculoSelic extends CalculoAtualizacaoMonetaria{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -5915631785464018593L;

	private BigDecimal selicAcumulado;
	
	public BigDecimal getSelicAcumulado() {
		return selicAcumulado;
	}
	public void setSelicAcumulado(BigDecimal selicAcumulado) {
		this.selicAcumulado = selicAcumulado;
	}
		
	
}
