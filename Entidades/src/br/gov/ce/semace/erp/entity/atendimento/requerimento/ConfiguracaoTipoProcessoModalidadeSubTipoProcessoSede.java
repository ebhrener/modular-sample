package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.geral.Sede;
import br.gov.ce.semace.erp.entity.licenciamento.Modalidade;
import br.gov.ce.semace.erp.entity.licenciamento.SubtipoProcesso;
import br.gov.ce.semace.erp.entity.licenciamento.TipoProcesso;

@Entity
@Audited
@Table(name = "configuracao_tipo_processo_modalidade_subtipo_processo_sede", schema = "atendimento")
public class ConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede implements Serializable{

	private static final long serialVersionUID = 2335463677093657422L;

	@Id
	@SequenceGenerator(sequenceName = "atendimento.config_tipo_processo_modalidade_subtipo_processo_sede_seq", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_processo_id")
	private TipoProcesso tipoProcesso;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modalidade_id")
	private Modalidade modalidade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subtipo_processo_id")
	private SubtipoProcesso subtipoProcesso;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_id")
	private Atividade atividade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sede_id")
	private Sede sede;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoProcesso getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(TipoProcesso tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public Modalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}

	public SubtipoProcesso getSubtipoProcesso() {
		return subtipoProcesso;
	}

	public void setSubtipoProcesso(SubtipoProcesso subtipoProcesso) {
		this.subtipoProcesso = subtipoProcesso;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

	public String getDescricao(){
		StringBuilder descricao = new StringBuilder("");

		if (modalidade != null && modalidade.getId() != null && modalidade.getId() != 0) {
			descricao.append(" Modalidade: " + modalidade.getDescricao() + " / ");
		}

		if (tipoProcesso != null && tipoProcesso.getId() != null && tipoProcesso.getId() != 0) {
			descricao.append(" Tipo de Processo: " + tipoProcesso.getDescricao());
		}

		if (subtipoProcesso != null && subtipoProcesso.getId() != null && subtipoProcesso.getId() != 0) {
			descricao.append(" / " + " SubTipo de Processo : " + subtipoProcesso.getDescricao());
		}

		if(atividade != null && atividade.getId() != null && atividade.getId() != 0){
			descricao.append(" / " + " Atividade: " + atividade.getCodigo() + " - " + atividade.getDescricao());
		}

		return descricao.toString();

	}

}
