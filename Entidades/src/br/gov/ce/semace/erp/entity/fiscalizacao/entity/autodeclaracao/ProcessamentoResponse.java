package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="retorno_processamento", schema="servicos")
public class ProcessamentoResponse {
	

	@Id
	@SequenceGenerator(sequenceName = "servicos.seq_retorno_processamento", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRetorno;
	private Integer retorno;
	private String numeroRegistroEmaterce;
	
	@ManyToOne
	private AutoDeclaracao autoDeclaracao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(Date dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public AutoDeclaracao getAutoDeclaracao() {
		return autoDeclaracao;
	}

	public void setAutoDeclaracao(AutoDeclaracao autoDeclaracao) {
		this.autoDeclaracao = autoDeclaracao;
	}

	public Integer getRetorno() {
		return retorno;
	}

	public void setRetorno(Integer retorno) {
		this.retorno = retorno;
	}

	public String getNumeroRegistroEmaterce() {
		return numeroRegistroEmaterce;
	}

	public void setNumeroRegistroEmaterce(String numeroRegistroEmaterce) {
		this.numeroRegistroEmaterce = numeroRegistroEmaterce;
	}

}
