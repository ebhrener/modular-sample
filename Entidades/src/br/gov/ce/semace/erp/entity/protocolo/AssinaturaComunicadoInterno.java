package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;


@Entity
@Audited
@Table(name="assinatura_comunicado_interno",schema="protocolo")
public class AssinaturaComunicadoInterno implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1820222975307917898L;

	@Id
	@SequenceGenerator(sequenceName ="protocolo.seq_assinatura_comunicado_interno", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario = new Funcionario();

	@Temporal(TemporalType.DATE)
	private Date data;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="comunicado_interno_id")
	private ComunicadoInterno comunicadoInterno;
	
	private String assinatura;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	public ComunicadoInterno getComunicadoInterno() {
		return comunicadoInterno;
	}

	public void setComunicadoInterno(ComunicadoInterno comunicadoInterno) {
		this.comunicadoInterno = comunicadoInterno;
	}

	public String getAssinatura() {
		return assinatura;
	}

	public void setAssinatura(String assinatura) {
		this.assinatura = assinatura;
	}

}
