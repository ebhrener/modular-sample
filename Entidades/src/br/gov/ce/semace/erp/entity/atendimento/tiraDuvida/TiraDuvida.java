package br.gov.ce.semace.erp.entity.atendimento.tiraDuvida;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;

@Entity
@Audited
@Table(name="tira_duvida", schema="atendimento")
public class TiraDuvida implements Serializable{

	private static final long serialVersionUID = -3922670382701151215L;
	
	@Id @SequenceGenerator(sequenceName="atendimento.seq_tira_duvida", name="SEQTIRA_DUVIDA", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQTIRA_DUVIDA")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="funcionario_id", nullable=false)
	private Funcionario funcionario;
	
	@Column(name="nome_cliente", nullable=false)
	private String nomeCliente;
	
	@Column(nullable=false)
	private String documento;
	
	@Column(nullable=false)
	private String descricao;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date data = new Date();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

}
