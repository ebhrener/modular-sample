package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio;

public enum AutoDeclaracaoRegimeCasamento {
	

	PARCIAL(2,"Comunhão parcial de bens"),
	FINAL(3,"Participacão final nos aquestos"),
	SEPARACAO(4,"Separacão de bens"),
	OUTRO(5,"Outro"),
	NAO_SE_APLICA(6,"Não se aplica"),
	OUTROS(99,"Outro"),
	TOTAL(1,"Comunhão universal de bens");
	
	private Integer codigo;
	private String descricao;
	
	private AutoDeclaracaoRegimeCasamento(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static String showDescricaoByCodigo(Integer codigo){
		for(AutoDeclaracaoRegimeCasamento auto: AutoDeclaracaoRegimeCasamento.values()){
			if(auto.getCodigo().equals(codigo)){
				return auto.descricao;
			}
		}
		return "Não Informado";
	}
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
}
