package br.gov.ce.semace.erp.entity.geral;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="geral", name="procurador")
@PrimaryKeyJoinColumn(name="pessoa_fisica_id")
public class Procurador extends PessoaFisica{
	
	private static final long serialVersionUID = 8312971529560183499L;
	
}
