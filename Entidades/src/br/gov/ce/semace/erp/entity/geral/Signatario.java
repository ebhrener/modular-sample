package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.StatusAssinatura;

@Entity
@Audited
@Table(name="signatario",schema="geral")
public class Signatario implements Serializable {

	private static final long serialVersionUID = -7094471879513702618L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_signatario", name="geral.seq_signatario",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="geral.seq_signatario")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="arquivo_documento_id", nullable = false)
	private ArquivoDocumento arquivoDocumento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="pessoa_id")
	private Pessoa pessoa;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_assinatura")
	private Date dataAssinatura;

	private StatusAssinatura status;

	private String justificativa;

	public boolean isRejeitada() {
		return status != null && status.isRejeitada();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ArquivoDocumento getArquivoDocumento() {
		return arquivoDocumento;
	}

	public void setArquivoDocumento(ArquivoDocumento arquivoDocumento) {
		this.arquivoDocumento = arquivoDocumento;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Date getDataAssinatura() {
		return dataAssinatura;
	}

	public void setDataAssinatura(Date dataAssinatura) {
		this.dataAssinatura = dataAssinatura;
	}

	public StatusAssinatura getStatus() {
		return status;
	}

	public void setStatus(StatusAssinatura status) {
		this.status = status;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}
}