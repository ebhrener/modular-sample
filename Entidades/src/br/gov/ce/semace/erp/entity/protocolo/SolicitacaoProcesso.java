package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.OrigemSolicitacaoProcesso;

@Entity
@Audited
@Table(name = "solicitacao_processo", schema = "protocolo")
public class SolicitacaoProcesso implements Serializable{

	private static final long serialVersionUID = -2950851522691050442L;
	
	@Id
	@SequenceGenerator(sequenceName ="protocolo.seq_solicitacao_processo", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;
	
	@Column(name = "origem_processo")
	private OrigemSolicitacaoProcesso origem;
	
	@Column(name = "numero_solicitacao")
	private String numeroSolicitacao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario = new Funcionario();
	
	private Boolean cancelado = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrigemSolicitacaoProcesso getOrigem() {
		return origem;
	}

	public void setOrigem(OrigemSolicitacaoProcesso origem) {
		this.origem = origem;
	}

	public String getNumeroSolicitacao() {
		return numeroSolicitacao;
	}

	public void setNumeroSolicitacao(String numeroSolicitacao) {
		this.numeroSolicitacao = numeroSolicitacao;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Boolean getCancelado() {
		return cancelado;
	}

	public void setCancelado(Boolean cancelado) {
		this.cancelado = cancelado;
	}
	
}
