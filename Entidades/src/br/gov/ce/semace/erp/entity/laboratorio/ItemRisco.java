package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import br.gov.ce.semace.erp.enuns.TipoLaboratorioSistema;

@Entity
@Audited
@Table(name="item_risco", schema="laboratorio")
public class ItemRisco implements Serializable {
	
	private static final long serialVersionUID = -3185930215133087154L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_item_risco", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	@ElementCollection(targetClass=TipoLaboratorioSistema.class)
	@CollectionTable(name="item_risco_tipo_laboratorio_sistema", schema="laboratorio",
	joinColumns=@JoinColumn(name="item_risco_id"))
	@Column(name="tipo_laboratorio_sistema")
	private List<TipoLaboratorioSistema> tiposLaboratorioSistema = new ArrayList<>();
	
	private String descricao;
	
	private Boolean ativo = Boolean.TRUE;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy="itensRisco")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private List<Coleta> coletas = new ArrayList<Coleta>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public List<TipoLaboratorioSistema> getTiposLaboratorioSistema() {
		return tiposLaboratorioSistema;
	}

	public void setTiposLaboratorioSistema(List<TipoLaboratorioSistema> tiposLaboratorioSistema) {
		this.tiposLaboratorioSistema = tiposLaboratorioSistema;
	}
}
