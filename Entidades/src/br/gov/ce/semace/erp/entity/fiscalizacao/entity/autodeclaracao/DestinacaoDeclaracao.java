package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio.AutoDeclaracaoDestinacaoDeclaracao;

@Entity
@Table(name="destinacao_declaracao", schema="servicos")
public class DestinacaoDeclaracao {
	
	@Id
	@SequenceGenerator(sequenceName = "servicos.seq_destinacao_declaracao", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;
	private Integer codigoDestinacaoDeclaracao;
	
	@ManyToOne
	private AutoDeclaracao autoDeclaracao;
	
	@Transient
	private String descricao;
	
	@Transient
	private String tempoColheita;
	
	@Transient
	private String unidadeMedida;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AutoDeclaracao getAutoDeclaracao() {
		return autoDeclaracao;
	}

	public void setAutoDeclaracao(AutoDeclaracao autoDeclaracao) {
		this.autoDeclaracao = autoDeclaracao;
	}

	public Integer getCodigoDestinacaoDeclaracao() {
		return codigoDestinacaoDeclaracao;
	}

	public void setCodigoDestinacaoDeclaracao(Integer codigoDestinacaoDeclaracao) {
		this.codigoDestinacaoDeclaracao = codigoDestinacaoDeclaracao;
	}

	public String getDescricao() {
		descricao = AutoDeclaracaoDestinacaoDeclaracao.showDescricaoByCodigo(getCodigoDestinacaoDeclaracao());
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getTempoColheita() {
		tempoColheita = AutoDeclaracaoDestinacaoDeclaracao.showTempoColheitaByCodigo(getCodigoDestinacaoDeclaracao());
		return tempoColheita;
	}

	public void setTempoColheita(String tempoColheita) {
		this.tempoColheita = tempoColheita;
	}

	public String getUnidadeMedida() {
		unidadeMedida = AutoDeclaracaoDestinacaoDeclaracao.showUnidadeMedidaByCodigo(getCodigoDestinacaoDeclaracao());
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
	
	

}
