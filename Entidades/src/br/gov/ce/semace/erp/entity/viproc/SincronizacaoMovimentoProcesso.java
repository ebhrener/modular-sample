package br.gov.ce.semace.erp.entity.viproc;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.semace.erp.enuns.TipoSistema;


@Entity
@Table(name="sincronizacao_movimento_processo", schema="viproc")
public class SincronizacaoMovimentoProcesso implements Serializable {

	private static final long serialVersionUID = -6374474429344197574L;

	@Id
	@SequenceGenerator(sequenceName="viproc.seq_sincronizacao_movimento_processo", name="SEQSINMOVPRO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQSINMOVPRO")
	private Long id;

	@Column(name = "movimento_processo_id")
	private Long movimentoProcessoid;

	@Column(name = "sistema")
	private int sistema;

	@Column(columnDefinition="sincronizado")
	private Boolean sincronizado;

	@Column(name = "qtd_erros")
	private int qtdErros;

	@Column(name = "last_remote_error", columnDefinition="TEXT")
	private String lastRemoteErro;

	//---------------------------------------------GETS e SETTERS----------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMovimentoProcessoid() {
		return movimentoProcessoid;
	}

	public void setMovimentoProcessoid(Long movimentoProcessoid) {
		this.movimentoProcessoid = movimentoProcessoid;
	}

	public int getSistema() {
		return sistema;
	}

	public void setSistema(int sistema) {
		this.sistema = sistema;
	}

	public void setSistema(TipoSistema tipoSistema) {
		this.sistema = tipoSistema.getValue();
	}

	public Boolean isSincronizado() {
		return sincronizado;
	}

	public void setSincronizado(Boolean sincronizado) {
		this.sincronizado = sincronizado;
	}

	public int getQtdErros() {
		return qtdErros;
	}

	public void setQtdErros(int qtdErros) {
		this.qtdErros = qtdErros;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SincronizacaoMovimentoProcesso other = (SincronizacaoMovimentoProcesso) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public String getLastRemoteErro() {
		return lastRemoteErro;
	}

	public void setLastRemoteErro(String lastRemoteErro) {
		this.lastRemoteErro = lastRemoteErro;
	}

}