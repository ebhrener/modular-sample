package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="regiao", schema="laboratorio")
public class Regiao implements Serializable{

	private static final long serialVersionUID = 6093095317573182387L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_regiao", name="laboratorio.seq_regiao", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="laboratorio.seq_regiao")
	private Long id;

	@Column(unique=true, nullable= false)
	private String descricao;

	//------------------------------------------------Gets e Setters---------------------------------------------

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Regiao other = (Regiao) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}