package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoQuestionario;

@Entity
@Audited
@Table(name="questionario", schema = "geral")
public class Questionario implements Serializable {

	private static final long serialVersionUID = 531910285943454975L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_questionario", name = "SEQ_QUESTIONARIO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_QUESTIONARIO")
	private Long id;

	private String titulo;

	private boolean ativo = true;

	@Column(name = "tipo_questionario")
	private TipoQuestionario tipoQuestionario;

	/** Relacionamentos */

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "questionario")
	private List<Questao> questoes;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "questionario")
	private List<DocumentoQuestionario> documentoQuestionarios;

	/**
	 * Informa a resposta da última pergunta, referente a Considerações e Conclusões.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 14/06/2013 16:04:24
	 */
	@Transient
	private String respostaConsideracoesEConclusoes;

	/**
	 * Informa a descrição da propriedade ativo, no caso, Sim ou Não, representando ativo ou inativo, respectivamente.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 24/02/2014 15:12:07
	 */
	@Transient
	private String detalheAtivo;

	/** Métodos getters e setters */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<Questao> getQuestoes() {
		return questoes;
	}

	public void setQuestoes(List<Questao> questoes) {
		this.questoes = questoes;
	}

	public TipoQuestionario getTipoQuestionario() {
		return tipoQuestionario;
	}

	public void setTipoQuestionario(TipoQuestionario tipoQuestionario) {
		this.tipoQuestionario = tipoQuestionario;
	}

	public List<DocumentoQuestionario> getDocumentoQuestionarios() {
		return documentoQuestionarios;
	}

	public void setDocumentoQuestionarios(
			List<DocumentoQuestionario> documentoQuestionarios) {
		this.documentoQuestionarios = documentoQuestionarios;
	}

	public String getRespostaConsideracoesEConclusoes(){

		String resposta = "";

		if (this.questoes != null && !this.questoes.isEmpty()){

			for (int i = this.questoes.size() - 1; i > 0; i--) {
				if (this.questoes.get(i) !=null && this.questoes.get(i).getRespostaQuestionario() != null && this.questoes.get(i).isTextoLongo()){
					resposta = this.questoes.get(i).getRespostaQuestionario().getRespostaQuestao();
					break;
				}

			}
		}

		return resposta;
	}

	public String getDetalheAtivo(){
		String retorno = "Não";

		if (ativo){
			retorno = "Sim";
		}

		return retorno;
	}

	public void setDetalheAtivo(String detalheAtivo) {
		this.detalheAtivo = detalheAtivo;
	}

	public void setRespostaConsideracoesEConclusoes(
			String respostaConsideracoesEConclusoes) {
		this.respostaConsideracoesEConclusoes = respostaConsideracoesEConclusoes;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
}
