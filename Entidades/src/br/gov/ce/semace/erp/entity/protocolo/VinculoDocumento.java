package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.TipoVinculo;

@Entity
@Audited
@Table(name = "vinculo_documento", schema = "protocolo")
public class VinculoDocumento implements Serializable {

	private static final long serialVersionUID = 7070237707161164303L;
	
	@Id
	@SequenceGenerator(sequenceName = "protocolo.seq_vinculo_documento", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documento_pai_id")
	private Documento documentoPai;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documento_filho_id")
	private Documento documentoFilho;
	
	@Temporal(TemporalType.DATE)
	private Date data;
	
	@Temporal(TemporalType.TIME)
	private Date hora;

	@Column(name = "tipo_vinculo")
	private TipoVinculo tipoVinculo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;
	
	private Boolean vinculado;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public Documento getDocumentoPai() {
		return documentoPai;
	}

	public void setDocumentoPai(Documento documentoPai) {
		this.documentoPai = documentoPai;
	}

	public Documento getDocumentoFilho() {
		return documentoFilho;
	}

	public void setDocumentoFilho(Documento documentoFilho) {
		this.documentoFilho = documentoFilho;
	}

	public TipoVinculo getTipoVinculo() {
		return tipoVinculo;
	}

	public void setTipoVinculo(TipoVinculo tipoVinculo) {
		this.tipoVinculo = tipoVinculo;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Boolean getVinculado() {
		return vinculado;
	}

	public void setVinculado(Boolean vinculado) {
		this.vinculado = vinculado;
	}

}
