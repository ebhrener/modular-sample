package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio;

public enum AutoDeclaracaoOrganizacaoSocial {
	
	
	INDEFINIDO (0,"Indefinido"),
	STR_SIND(1,"STR/SIND.AGR.FAM."),
	SIND_RURAL(2,"Sind. Rural"),
	COOPERATIVA(3,"Cooperativa"),
	ASSOCIACAO(4,"Associacao"),
	QUILOMBO(5,"Quilombo"),
	INDIGENA(6,"Indigena"),
	OUTRO(7,"Outra"),
	NAO_PERTENCE(8,"Nao Pertence"),
	ANPA_MPA(9,"Sindicato e/ou Associação vinculado a ANPA/MPA"),
	FETRAF(10,"Sindicato e/ou Associação vinculado a FETRAF"),
	CONTAG(11,"Sindicato vinculado a CONTAG"),
	CNA(12,"Sindicato vinculado a CNA"),
	OUTRAS(13,"Outras");

	private String descricao;
	private Integer codigo;
	
	private AutoDeclaracaoOrganizacaoSocial(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static String showDescricaoByCodigo(Integer codigo){
		for(AutoDeclaracaoOrganizacaoSocial auto: AutoDeclaracaoOrganizacaoSocial.values()){
			if(auto.getCodigo().equals(codigo)){
				return auto.descricao;
			}
		}
		return "Não Informado";
	}
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
}
