package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * Tentar trazer esses valores ou em string ou em integer para depois converter para o tipo period
 *
 * @author joerlan.lima
 *
 */

@Entity
@Audited
@Table(schema = "rh", name = "jornada_trabalho")
//@TypeDef(name="period", typeClass= JodaTypePeriodParser.class)
public class JornadaTrabalho implements Serializable {

	private static final long serialVersionUID = 156978211423191882L;

	@Id
	@SequenceGenerator(sequenceName = "rh.seq_jornada_trabalho", name = "seq_jornada_trabalho", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_jornada_trabalho")
	private Long id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;

//	@Column(name = "horas_trabalhadas")
//	@Type(type="period")
//	private Period horasTrabalhadas;

	@Column(name = "data_dia_cadastro")
	private Date data;

//	@Column(name = "horas_computadas")
//	@Type(type="period")
//	private Period horasComputadas;
//
//	@Transient
//	private Period cargaHorariaValidacao;
	/*
	 * Construtores
	 */
	public JornadaTrabalho() {
		super();
	}

//	public JornadaTrabalho(Funcionario funcionario, Period horasTrabalhadas, Date data) {
//		super();
//		this.funcionario = funcionario;
//		this.horasTrabalhadas = horasTrabalhadas;
//		this.data = data;
//	}

	public JornadaTrabalho(Funcionario funcionario, Date data) {
		super();
		this.funcionario = funcionario;
		this.data = data;
	}

	/*
	 * Getters e Setters
	 */

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
//	public Period getHorasTrabalhadas() {
//		return horasTrabalhadas;
//	}
//	public void setHorasTrabalhadas(Period horasTrabalhadas) {
//		this.horasTrabalhadas = horasTrabalhadas;
//	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
//	public Period getHorasComputadas() {
//		return horasComputadas;
//	}
//	public void setHorasComputadas(Period horasComputadas) {
//		this.horasComputadas = horasComputadas;
//	}

//	public Period getCargaHorariaValidacao() {
//		return cargaHorariaValidacao;
//	}
//
//	public void setCargaHorariaValidacao(Period cargaHorariaValidacao) {
//		this.cargaHorariaValidacao = cargaHorariaValidacao;
//	}

	/**
	 * Método para mostrar horas trabalhadas pelo servidor de um dia em tela
	 * @author Erick Bhrener [erick.bhb@gmail.com] 01/02/2017 08:45:40
	 * @return String
	 */
//	@Transient
//	public String getHorasTrabalhadasAsString(){
//		if(getHorasTrabalhadas() == null){
//			return "Este dia está irregular!";
//		}
//		if(getHorasTrabalhadas().getMinutes() == 0 && getHorasTrabalhadas().getHours()==0){
//			return PeriodUtils.printPeriod(new Period());
//		}
//		return PeriodUtils.printPeriod(getHorasTrabalhadas());
//	}

	/**
	 * Pega as horas computadas e subtrai da carga horária da minuta (8 horas) e formata para impressão em tela
	 * @author Erick Bhrener [erick.bhb@gmail.com] 08/02/2017 10:48:43
	 * @return String
	 */
//	@Transient
//	public String getHorasRestantesAsString(){
//		if(getHorasComputadas()==null){
//			return "Sem informações";
//		}
//		Period horasRestantes = getHorasComputadas().normalizedStandard(PeriodType.time());
//		if (cargaHorariaValidacao == null || cargaHorariaValidacao.equals(new Period())) {
//			cargaHorariaValidacao = new Period(8,0,0,0);
//		}
//
//		if (funcionario != null && funcionario.getListPonto() != null && funcionario.getListPonto().size() > 0 && funcionario.getListPonto().get(0).isPossuiCargaHoraria()) {
//			cargaHorariaValidacao = funcionario.getListPonto().get(0).getCargaHoraria();
//		} else if (funcionario != null && funcionario.getVinculoEmpregaticio() != null && funcionario.getVinculoEmpregaticio().getId() == 3L) {
//			cargaHorariaValidacao = new Period(4,0,0,0);
//		}
//
//		if(horasRestantes.toStandardDuration().isEqual(new Duration(0))){
//			return PeriodUtils.printPeriod(cargaHorariaValidacao);
//		}
//
//		horasRestantes = cargaHorariaValidacao.minus(horasRestantes).normalizedStandard(PeriodType.time());
//		if(horasRestantes.toStandardDuration().compareTo(new Duration(0)) < 0){
//			return PeriodUtils.printPeriod(new Period());
//		}
//		if(horasRestantes.getSeconds() > 0){
//			horasRestantes = horasRestantes.withSeconds(0);
//			horasRestantes = horasRestantes.plusMinutes(1);
//		}
//
//		return PeriodUtils.printPeriod(horasRestantes);
//	}
//
//	/**
//	 * Total de horas computadas (trabalhadas + justificadas) de um funcionário.
//	 * @author Erick Bhrener [erick.bhb@gmail.com] 10/03/2017 09:24:30
//	 * @param
//	 * @return
//	 */
//	@Transient
//	public String getHorasComputadasAsString(){
//		return PeriodUtils.printPeriod(this.horasComputadas.normalizedStandard(PeriodType.time()));
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		JornadaTrabalho other = (JornadaTrabalho) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}
