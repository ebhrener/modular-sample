package br.gov.ce.semace.erp.entity.notificacaoprocesso;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.GrupoEnvioSMS;
import br.gov.ce.semace.erp.enuns.GrupoEnvioURA;

/**
 * Esta tablela armazena os status da tabela adm_siga.movimento_processo que serão considerados
 * na hora de notificar o usuário sobre o andamento do processo, sendo assim, os status que
 * estiverem nesta tabela serão os status que serão notificados aos usuários quando houver uma
 * movimentação correspondente.
 *
 * @author joerlan.lima
 *
 */
@Audited
@Entity
@Table(name="mapeamento_status_notificacoes", schema="notificacao_processo")
public class MapeamentoStatusNotificacoes implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -1959238411482306537L;

	@Id
	@SequenceGenerator(sequenceName="notificacao_processo.seq_mapeamento_status_notificacoes", name="notificacao_processo.seq_mapeamento_status_notificacoes", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="notificacao_processo.seq_mapeamento_status_notificacoes")
	private Long id;

	@Column(nullable= false)
	private String descricao;

	/**
	 * Mensagem modelo para cada status
	 *
	 * OBS: utilizar {{var}} para variáveis dinamicas Ex: "A licença do processo {{spu}} foi emitida."
	 */
	@Column(nullable= false, name="template_email", columnDefinition="text")
	private String templateEmail;

	/**
	 * Mensagem modelo para cada status
	 *
	 * OBS: utilizar {{var}} para variáveis dinamicas Ex: "A licença do processo {{spu}} foi emitida."
	 */
	@Column(nullable= false, name="template_sms")
	private String templateSMS;

	/**
	 * Id do status da movimento_processo que será considerado nas notificações
	 */
	@Column(nullable=false, name="status_processo_id")
	private String statusProcessoID;

	@Column(nullable=false, name="grupo_tipo_processo_id")
	private String grupoTipoProcessoID;

	/**
	 * Este campo é usado para mapear qual o id do grupo que será enviado ao webservice para cada msg
	 *
	 * GRUPO-SEMACE-TESTE ID 215
	 * SEMACE SMS 1 ID 294 (1a notificação de SMS),
	 * SEMACE SMS 2 ID 295 (2a notificação),
	 * SEMACE SMS 3 ID 296 (3a),
	 * SEMACE URA 1 ID 297 (1a ligação de URA) e
	 * SEMACE URA 2 ID 298 (2a ligação)
	 *
	 */
	@Column(nullable=false, name="grupo_envio_sms_enum")
	@Enumerated(EnumType.ORDINAL)
	private GrupoEnvioSMS grupoEnvioSMS;

	/**
	 * Utilize tipoProcessoID nulo para tornar este mapeamento o templete genérico para o grupoTipoProcesso
	 */
	@Column(name="tipo_processo_id")
	private String tipoProcessoID;

	@Column(name="grupo_envio_ura_enum")
	@Enumerated(EnumType.ORDINAL)
	private GrupoEnvioURA grupoEnvioURA;

	@Column(nullable= false)
	private Boolean ativo = Boolean.TRUE;

	//------------------------------------------------Gets e Setters---------------------------------------------

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getStatusProcessoID() {
		return statusProcessoID;
	}

	public void setStatusProcessoID(String statusProcessoID) {
		this.statusProcessoID = statusProcessoID;
	}

	public String getTemplateSMS() {
		return templateSMS;
	}

	public void setTemplateSMS(String templateSMS) {
		this.templateSMS = templateSMS;
	}

	public String getTemplateEmail() {
		return templateEmail;
	}

	public void setTemplateEmail(String templateEmail) {
		this.templateEmail = templateEmail;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GrupoEnvioSMS getGrupoEnvioSMS() {
		return grupoEnvioSMS;
	}

	public void setGrupoEnvioSMS(GrupoEnvioSMS grupoEnvioSMS) {
		this.grupoEnvioSMS = grupoEnvioSMS;
	}

	public GrupoEnvioURA getGrupoEnvioURA() {
		return grupoEnvioURA;
	}

	public void setGrupoEnvioURA(GrupoEnvioURA grupoEnvioURA) {
		this.grupoEnvioURA = grupoEnvioURA;
	}

}