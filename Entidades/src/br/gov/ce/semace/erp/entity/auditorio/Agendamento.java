package br.gov.ce.semace.erp.entity.auditorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.DiaSemana;
import br.gov.ce.semace.erp.enuns.TipoRepeticaoAgendamento;

/**
 * @author antoniomenezes
 *
 */
@Entity
@Audited
@Table(schema="auditorio", name="agendamento_auditorio")
public class Agendamento implements Serializable  {

	private static final long serialVersionUID = 7846196786119139452L;

	@Id
	@SequenceGenerator(sequenceName="auditorio.seq_agendamento_auditorio", name="SEQAGENDAUDIT", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQAGENDAUDIT")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable  = true, name="solicitante_id", referencedColumnName="pessoa_id")
	private Funcionario solicitante = new Funcionario();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable  = true, name="espaco_id")
	private Espaco espaco = new Espaco();

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "agendamento_objeto_reserva",	schema = "auditorio", joinColumns = @JoinColumn(name = "agendamento_id"),
	inverseJoinColumns = @JoinColumn(name = "objeto_reserva_id"))
	private List<ObjetoReserva> objetos = new ArrayList<ObjetoReserva>();

	@Column(name="quantidade_pessoas")
	private Integer quantidadePessoas;

	private String assunto;

	@Column(columnDefinition = "text")
	private String descricao;

	@Temporal(TemporalType.DATE)
	private Date data;

	@Temporal(TemporalType.TIME)
	@Column(name="hora_inicial")
	private Date horaInicial;

	@Temporal(TemporalType.TIME)
	@Column(name="hora_final")
	private Date horaFinal;

	@Temporal(TemporalType.DATE)
	@Column(name="data_cancelamento")
	private Date dataCancelamento;

	private Boolean cancelado = false;

	@Transient
	private Boolean repeteAgendamento = false;

	@Transient
	private TipoRepeticaoAgendamento tipoRepeticaoAgendamento;

	@Transient
	private Integer intervaloRepeticao;

	@Transient
	private Integer opcaoTermino;

	@Transient
	private Integer qtdOcorrencias;

	@Transient
	private Date dataTermino;

	@Transient
	private List<DiaSemana> listDiasDaSemanaSelecionados = new ArrayList<DiaSemana>();

	@Transient
	private Date dataFinalRepeticao;

	@Transient
	private Date dataInicial;

	@Transient
	private Date dataFinal;

	@Transient
	private List<DiaSemana> listDiasSemana = new ArrayList<DiaSemana>();



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(Funcionario solicitante) {
		this.solicitante = solicitante;
	}

	public Integer getQuantidadePessoas() {
		return quantidadePessoas;
	}

	public void setQuantidadePessoas(Integer quantidadePessoas) {
		this.quantidadePessoas = quantidadePessoas;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getHoraInicial() {
		return horaInicial;
	}

	public void setHoraInicial(Date horaInicial) {
		this.horaInicial = horaInicial;
	}

	public Date getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(Date horaFinal) {
		this.horaFinal = horaFinal;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public List<ObjetoReserva> getObjetos() {
		return objetos;
	}

	public void setObjetos(List<ObjetoReserva> objetos) {
		this.objetos = objetos;
	}

	public Boolean getCancelado() {
		return cancelado;
	}

	public void setCancelado(Boolean cancelado) {
		this.cancelado = cancelado;
	}

	public Date getDataCancelamento() {
		return dataCancelamento;
	}

	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public Espaco getEspaco() {
		return espaco;
	}

	public void setEspaco(Espaco espaco) {
		this.espaco = espaco;
	}

	public Boolean getRepeteAgendamento() {
		return repeteAgendamento;
	}

	public void setRepeteAgendamento(Boolean repeteAgendamento) {
		this.repeteAgendamento = repeteAgendamento;
	}

	public TipoRepeticaoAgendamento getTipoRepeticaoAgendamento() {
		return tipoRepeticaoAgendamento;
	}

	public void setTipoRepeticaoAgendamento(TipoRepeticaoAgendamento tipoRepeticaoAgendamento) {
		this.tipoRepeticaoAgendamento = tipoRepeticaoAgendamento;
	}

	public Integer getIntervaloRepeticao() {
		return intervaloRepeticao;
	}

	public void setIntervaloRepeticao(Integer intervaloRepeticao) {
		this.intervaloRepeticao = intervaloRepeticao;
	}

	public Integer getOpcaoTermino() {
		return opcaoTermino;
	}

	public void setOpcaoTermino(Integer opcaoTermino) {
		this.opcaoTermino = opcaoTermino;
	}

	public Integer getQtdOcorrencias() {
		return qtdOcorrencias;
	}

	public void setQtdOcorrencias(Integer qtdOcorrencias) {
		this.qtdOcorrencias = qtdOcorrencias;
	}

	public Date getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}
/*
	public List<DiaSemana> getListDiasDaSemanaSelecionados() {
		return listDiasDaSemanaSelecionados;
	}

	public void setListDiasDaSemanaSelecionados(List<DiaSemana> listDiasDaSemanaSelecionados) {
		this.listDiasDaSemanaSelecionados = listDiasDaSemanaSelecionados;
	}
*/

	public Date getDataFinalRepeticao() {
		return dataFinalRepeticao;
	}

	public List<br.gov.ce.semace.erp.enuns.DiaSemana> getListDiasDaSemanaSelecionados() {
		return listDiasDaSemanaSelecionados;
	}

	public void setListDiasDaSemanaSelecionados(
			List<br.gov.ce.semace.erp.enuns.DiaSemana> listDiasDaSemanaSelecionados) {
		this.listDiasDaSemanaSelecionados = listDiasDaSemanaSelecionados;
	}

	public void setDataFinalRepeticao(Date dataFinalRepeticao) {
		this.dataFinalRepeticao = dataFinalRepeticao;
	}

	public List<br.gov.ce.semace.erp.enuns.DiaSemana> getListDiasSemana() {
		return listDiasSemana;
	}

	public void setListDiasSemana(
			List<br.gov.ce.semace.erp.enuns.DiaSemana> listDiasSemana) {
		this.listDiasSemana = listDiasSemana;
	}

}
