package br.gov.ce.semace.erp.entity.rh;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.fiscalizacao.OrdemFiscalizacao;
import br.gov.ce.semace.erp.entity.geral.FormacaoProfissional;
import br.gov.ce.semace.erp.entity.geral.PessoaFisica;
import br.gov.ce.semace.erp.entity.geral.Sede;
import br.gov.ce.semace.erp.entity.seguranca.Usuario;

@Entity
@Audited
@Table(schema="rh", name="funcionario", uniqueConstraints=@UniqueConstraint(columnNames="matricula"))
public class Funcionario extends PessoaFisica implements Comparable<Funcionario> {

	private static final long serialVersionUID = 4552173438066482043L;

	@NotNull
	@Column(unique = true, nullable=false)
	private String matricula;

	@Temporal(TemporalType.DATE)
	@Column(name="data_admissao")
	private Date dataAdmissao;

	//----------------------------------------------------------------------------Relacionamentos----------------------------------------------------------------------------

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="setor_id")
	private Setor setor = new Setor();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="cargo_id")
	private Cargo cargo = new Cargo();

	@OneToMany(mappedBy = "funcionario", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<Ferias> listFerias = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="funcao_id")
	private Funcao funcao = new Funcao();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="formacao_profissional")
	private FormacaoProfissional formacaoProfissional = new FormacaoProfissional();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="usuario_id")
	private Usuario usuario;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "tecnicos")
	private List<OrdemFiscalizacao> ordensFiscalizacao = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="vinculo_id", referencedColumnName="id")
	private VinculoEmpregaticio vinculoEmpregaticio = new VinculoEmpregaticio();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="lotacao_id", referencedColumnName="id")
	private Sede lotacao = new Sede();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="situacao_funcionario_id")
	private SituacaoFuncionario situacaoFuncionario = new SituacaoFuncionario();

	@Column(name = "cadastro_informado")
	private Boolean cadastroInformado;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="nivel_vinculacao_id")
	private NivelVinculacao nivelVinculacao = new NivelVinculacao();

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="funcionario_habilitacao", schema="rh",
		joinColumns=@JoinColumn(name="funcionario_id"),
		inverseJoinColumns=@JoinColumn(name="habilitacao_id"))
	private List<Habilitacao> listHabilitacao = new ArrayList<>();

	/**
	 * Relação entre {@link Funcionario} e {@link Ponto} para trazer carga horária especial de cada funcionário
	 * @author erick.silva - Erick Bhrener Barroso Silva [erick.bhb@gmail.com] = 09/04/2018
	 */
	@OneToMany(mappedBy = "funcionario", fetch = FetchType.LAZY)
	private List<Ponto> listPonto = new ArrayList<>();

	@Transient
	private Long quantidadeDocumentosPautaAtivos;

	@Transient
	private Double quantidadeDiarias;

	private static final String MSG_GENERICA_NAO_INFORMADO = "Não informado";

	@Transient
	private Boolean selecionado;

	/**
	 * Método {@link Transient} para informar a descrição do {@link Setor}.
	 * Feito um tratamento para que quando não houver sigla no setor, informar a mensagem <b>Não Informado</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 09/12/2014 14:03:53
	 *
	 * @return
	 * 		Retorna a descrição do {@link Setor}. Caso não exista sigla, informa a mensagem <b>Não Informado</b>
	 */
	@Transient
	public String getDetalheSetor(){
		if(this.setor != null && this.setor.getSigla() != null && !this.getSetor().getSigla().isEmpty()){
			return this.getSetor().getSigla();
		}

		return MSG_GENERICA_NAO_INFORMADO;
	}

	/**
	 * Método {@link Transient} para informar a descrição da {@link SituacaoFuncionario}.
	 * Feito um tratamento para que quando não houver descrição, informar a mensagem <b>Não Informado</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 09/12/2014 14:15:24
	 *
	 * @return
	 * 		Retorna a descrição do {@link SituacaoFuncionario}. Caso não exista sigla, informa a mensagem <b>Não Informado</b>
	 */
	@Transient
	public String getDetalheSituacao(){
		if(this.situacaoFuncionario != null && this.situacaoFuncionario.getDescricao() != null && !this.situacaoFuncionario.getDescricao().isEmpty()){
			return this.situacaoFuncionario.getDescricao();
		}

		return MSG_GENERICA_NAO_INFORMADO;
	}

	/**
	 * Método {@link Transient} para informar a descrição do {@link VinculoEmpregaticio}.
	 * Feito um tratamento para que quando não houver descrição, informar a mensagem <b>Não Informado</b>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 09/12/2014 14:16:32
	 *
	 * @return
	 * 		Retorna a descrição do {@link VinculoEmpregaticio}. Caso não exista sigla, informa a mensagem <b>Não Informado</b>
	 */
	@Transient
	public String getDetalheVinculo(){
		if(this.vinculoEmpregaticio != null && this.vinculoEmpregaticio.getVinculo() != null && !this.vinculoEmpregaticio.getVinculo().isEmpty()){
			return this.vinculoEmpregaticio.getVinculo();
		}

		return MSG_GENERICA_NAO_INFORMADO;
	}

	public Funcionario() {
		super();
	}

	public Funcionario(Long id, Long quantidadeDocumentosPautaAtivos){
		setId(id);
		this.quantidadeDocumentosPautaAtivos = quantidadeDocumentosPautaAtivos;
	}

	public Funcionario(Long id, Double qtdDiarias){
		setId(id);
		this.quantidadeDiarias = qtdDiarias;
	}

	//----------------------------------------------------------------------------Gets e Setters----------------------------------------------------------------------------

	/**
	 * Método {@link Transient} para informar a situação do Funcionário.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 09/12/2014 14:17:44
	 *
	 * @return
	 * 		Retorna a situação do Funcionário. Caso esteja inativo, informa <b>Não</b>. Caso contrário, <b>Sim</b>
	 */
	@Transient
	public String getDetalheAtivo(){
		if(getAtivo() !=null && getAtivo()){
			return "Sim";
		}

		return "Não";
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Funcao getFuncao() {
		return funcao;
	}

	public void setFuncao(Funcao funcao) {
		this.funcao = funcao;
	}

	public FormacaoProfissional getFormacaoProfissional() {
		return formacaoProfissional;
	}

	public void setFormacaoProfissional(FormacaoProfissional formacaoProfissional) {
		this.formacaoProfissional = formacaoProfissional;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public List<OrdemFiscalizacao> getOrdensFiscalizacao() {
		return ordensFiscalizacao;
	}

	public void setOrdensFiscalizacao(List<OrdemFiscalizacao> ordensFiscalizacao) {
		this.ordensFiscalizacao = ordensFiscalizacao;
	}

	public VinculoEmpregaticio getVinculoEmpregaticio() {
		return vinculoEmpregaticio;
	}

	public void setVinculoEmpregaticio(VinculoEmpregaticio vinculoEmpregaticio) {
		this.vinculoEmpregaticio = vinculoEmpregaticio;
	}


	public Sede getLotacao() {
		return lotacao;
	}

	public void setLotacao(Sede lotacao) {
		this.lotacao = lotacao;
	}

	@Override
	public int compareTo(Funcionario funcionario) {
		if(getNome() == null) {
			return 1;
		}
		return funcionario.getNome().compareTo(getNome());
	}

	public Date getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(Date dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public List<Ferias> getListFerias() {
		return listFerias;
	}

	public void setListFerias(List<Ferias> listFerias) {
		this.listFerias = listFerias;
	}

	public SituacaoFuncionario getSituacaoFuncionario() {
		return situacaoFuncionario;
	}

	public void setSituacaoFuncionario(SituacaoFuncionario situacaoFuncionario) {
		this.situacaoFuncionario = situacaoFuncionario;
	}

	public Long getQuantidadeDocumentosPautaAtivos() {
		return quantidadeDocumentosPautaAtivos;
	}

	public void setQuantidadeDocumentosPautaAtivos(
			Long quantidadeDocumentosPautaAtivos) {
		this.quantidadeDocumentosPautaAtivos = quantidadeDocumentosPautaAtivos;
	}

	public Boolean getCadastroInformado() {
		return cadastroInformado;
	}

	public void setCadastroInformado(Boolean cadastroInformado) {
		this.cadastroInformado = cadastroInformado;
	}

	public NivelVinculacao getNivelVinculacao() {
		return nivelVinculacao;
	}

	public void setNivelVinculacao(NivelVinculacao nivelVinculacao) {
		this.nivelVinculacao = nivelVinculacao;
	}

	public List<Habilitacao> getListHabilitacao() {
		return listHabilitacao;
	}

	public void setListHabilitacao(List<Habilitacao> listHabilitacao) {
		this.listHabilitacao = listHabilitacao;
	}

	public Double getQuantidadeDiarias() {
		return quantidadeDiarias;
	}

	public void setQuantidadeDiarias(Double quantidadeDiarias) {
		this.quantidadeDiarias = quantidadeDiarias;
	}

	public Boolean getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(Boolean selecionado) {
		this.selecionado = selecionado;
	}

	public List<Ponto> getListPonto() {
		return listPonto;
	}

	public void setListPonto(List<Ponto> listPonto) {
		this.listPonto = listPonto;
	}

	@Override
	public int hashCode() {
		if(getId() != null){
			return getId().hashCode();
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Funcionario)) {
			return false;
		}
		Funcionario other = (Funcionario) obj;
		if (getId() == null) {
			if (other.getId() != null) {
				return false;
			}
		} else if (!getId().equals(other.getId())) {
			return false;
		}
		return true;
	}

	// TODO: Verificar a possibilidade de implementar o método clone.
	public Advogado downcastingToAdvogado() {
		Advogado advogado = new Advogado();
		advogado.setId(this.getId());
		advogado.setAtivo(this.getAtivo());
		advogado.setNome(this.getNome());
		advogado.setCpf(this.getCpf());
		advogado.setRg(this.getRg());
		advogado.setSexo(this.getSexo());
		advogado.setMatricula(this.matricula);
		advogado.setCargo(this.cargo);
		advogado.setSetor(this.setor);
		advogado.setFuncao(this.funcao);
		advogado.setUsuario(this.usuario);

		return advogado;
	}

}
