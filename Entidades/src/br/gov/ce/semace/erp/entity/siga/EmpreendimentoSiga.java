package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the empreendimento database table.
 *
 * @author patrick
 */
@Entity
@Table(name = "empreendimento", schema = "adm_siga")
public class EmpreendimentoSiga implements Serializable {

	private static final long serialVersionUID = 7113415167159735069L;

	@Id
	@SequenceGenerator(name = "EMPREENDIMENTO_EMPREENDIMENTOID_GENERATOR", sequenceName = "SQ_EMPREENDIMENTO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPREENDIMENTO_EMPREENDIMENTOID_GENERATOR")
	@Column(name = "empreendimento_id")
	private String empreendimentoId;

	@Column(name = "area_empreendimento")
	private Float areaEmpreendimento;

	@Column(name = "atividade_id")
	private String atividadeId;

	@Column(name = "cep_id")
	private String cepId;

	@Column(name = "cliente_id")
	private String clienteId;

	private String cnpj;

	private String complemento;

	@Column(name = "coordenada_x")
	private float coordenadaX;

	@Column(name = "coordenada_y")
	private float coordenadaY;

	@Column(name = "divisao_atividade_id")
	private String divisaoAtividadeId;

	private String empreendimento;

	@Column(name = "grupo_atividade_id")
	private String grupoAtividadeId;

	private double investimento;

	private String latitude;

	private String longitude;

	@Column(name = "porte_empreendimento_id")
	private String porteEmpreendimentoId;

	@Column(name = "qtd_funcionarios")
	private String qtdFuncionarios;

	@Column(name = "spu_id")
	private String spuId;

	@Column(name = "status_cep_id")
	private String statusCepId;

	private String telefone;

	@Column(name = "tipo_coordenada")
	private String tipoCoordenada;

	@Column(name = "tipo_vegetacao")
	private String tipoVegetacao;

	@Column(name = "unidade_area")
	private String unidadeArea;

	@Transient
	private String codigoAtividade;

	@Transient
	private String descricaoAtividade;

	@Transient
	private Date dataCadastro;

	public EmpreendimentoSiga() {
	}

	public String getEmpreendimentoId() {
		return this.empreendimentoId;
	}

	public void setEmpreendimentoId(String empreendimentoId) {
		this.empreendimentoId = empreendimentoId;
	}

	public Float getAreaEmpreendimento() {
		return this.areaEmpreendimento;
	}

	public void setAreaEmpreendimento(Float areaEmpreendimento) {
		this.areaEmpreendimento = areaEmpreendimento;
	}

	public String getAtividadeId() {
		return this.atividadeId;
	}

	public void setAtividadeId(String atividadeId) {
		this.atividadeId = atividadeId;
	}

	public String getCepId() {
		return this.cepId;
	}

	public void setCepId(String cepId) {
		this.cepId = cepId;
	}

	public String getClienteId() {
		return this.clienteId;
	}

	public void setClienteId(String clienteId) {
		this.clienteId = clienteId;
	}

	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getComplemento() {
		return this.complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public float getCoordenadaX() {
		return this.coordenadaX;
	}

	public void setCoordenadaX(float coordenadaX) {
		this.coordenadaX = coordenadaX;
	}

	public float getCoordenadaY() {
		return this.coordenadaY;
	}

	public void setCoordenadaY(float coordenadaY) {
		this.coordenadaY = coordenadaY;
	}

	public String getDivisaoAtividadeId() {
		return this.divisaoAtividadeId;
	}

	public void setDivisaoAtividadeId(String divisaoAtividadeId) {
		this.divisaoAtividadeId = divisaoAtividadeId;
	}

	public String getEmpreendimento() {
		return this.empreendimento;
	}

	public void setEmpreendimento(String empreendimento) {
		this.empreendimento = empreendimento;
	}

	public String getGrupoAtividadeId() {
		return this.grupoAtividadeId;
	}

	public void setGrupoAtividadeId(String grupoAtividadeId) {
		this.grupoAtividadeId = grupoAtividadeId;
	}

	public double getInvestimento() {
		return this.investimento;
	}

	public void setInvestimento(double investimento) {
		this.investimento = investimento;
	}

	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPorteEmpreendimentoId() {
		return this.porteEmpreendimentoId;
	}

	public void setPorteEmpreendimentoId(String porteEmpreendimentoId) {
		this.porteEmpreendimentoId = porteEmpreendimentoId;
	}

	public String getQtdFuncionarios() {
		return this.qtdFuncionarios;
	}

	public void setQtdFuncionarios(String qtdFuncionarios) {
		this.qtdFuncionarios = qtdFuncionarios;
	}

	public String getSpuId() {
		return this.spuId;
	}

	public void setSpuId(String spuId) {
		this.spuId = spuId;
	}

	public String getStatusCepId() {
		return this.statusCepId;
	}

	public void setStatusCepId(String statusCepId) {
		this.statusCepId = statusCepId;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getTipoCoordenada() {
		return this.tipoCoordenada;
	}

	public void setTipoCoordenada(String tipoCoordenada) {
		this.tipoCoordenada = tipoCoordenada;
	}

	public String getTipoVegetacao() {
		return this.tipoVegetacao;
	}

	public void setTipoVegetacao(String tipoVegetacao) {
		this.tipoVegetacao = tipoVegetacao;
	}

	public String getUnidadeArea() {
		return this.unidadeArea;
	}

	public void setUnidadeArea(String unidadeArea) {
		this.unidadeArea = unidadeArea;
	}

	public String getCodigoAtividade() {
		return codigoAtividade;
	}

	public void setCodigoAtividade(String codigoAtividade) {
		this.codigoAtividade = codigoAtividade;
	}

	public String getDescricaoAtividade() {
		return descricaoAtividade;
	}

	public void setDescricaoAtividade(String descricaoAtividade) {
		this.descricaoAtividade = descricaoAtividade;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

}