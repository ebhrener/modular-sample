package br.gov.ce.semace.erp.entity.planoviagem;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Cargo;
import br.gov.ce.semace.erp.entity.rh.Funcao;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.entity.rh.Habilitacao;
import br.gov.ce.semace.erp.entity.rh.Sindicato;
import br.gov.ce.semace.erp.entity.rh.VinculoEmpregaticio;

@Entity
@Audited
@Table(schema="plano_viagem", name="configuracao_solicitacao_viagem")
public class ConfiguracaoSolicitacaoViagem implements Serializable{

	private static final long serialVersionUID = -476758239005035883L;

	@Id
	private Long id;

	/**
	 * {@link Cargo} que obriga o cadastro da {@link Habilitacao}
	 * do {@link Funcionario}.
	 *
	 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 06/05/2014 09:07:08
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cargo_habilitacao_id")
	private Cargo cargoHabilitacao = new Cargo();

	/**
	 * {@link Funcao} que obriga o cadastro da {@link Habilitacao}
	 * do {@link Funcionario}.
	 *
	 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 06/05/2014 09:07:18
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="funcao_habilitacao_id")
	private Funcao funcaoHabilitacao = new Funcao();

	/**
	 * {@link VinculoEmpregaticio} Comissionado.
	 *
	 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 06/05/2014 10:11:39
	 */
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="vinculo_comissionado_id", referencedColumnName="id")
	private VinculoEmpregaticio vinculoComissionado = new VinculoEmpregaticio();

	/**
	 * {@link VinculoEmpregaticio} Servidor.
	 *
	 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 06/05/2014 10:12:05
	 */
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="vinculo_servidor_id", referencedColumnName="id")
	private VinculoEmpregaticio vinculoServidor = new VinculoEmpregaticio();

	/**
	 * {@link VinculoEmpregaticio} Terceirizado.
	 *
	 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 06/05/2014 10:12:31
	 */
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="vinculo_terceirizado_id", referencedColumnName="id")
	private VinculoEmpregaticio vinculoTerceirizado = new VinculoEmpregaticio();

	/**
	 * 
	 * @author Tiago Nascimento - vilard@gmail.com 24/12/2014 10:57:28
	 * 
	 */
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="sindicato_motorista_id", referencedColumnName="id")
	private Sindicato sindicatoMotorista = new Sindicato();
	
	/**
	 *
	 * @author Tiago Nascimento - vilard@gmail.com 05/06/2014 09:11:37
	 *
	 *  - Quantidade de diárias que uma pessoa pode acumular em um unico mês
	 *
	 */
	@Column(name="diarias_limite_por_mes")
	private Integer diariasLimitePorMes;

	/**
	 *
	 * @author Tiago Nascimento - vilard@gmail.com 05/06/2014 09:27:36
	 *
	 *  - Quantidade de dias retroativos para contagem de diárias do motoristas para ordena-los no ranking de seleção automática
	 *
	 */
	@Column(name="dias_ranking_motoristas")
	private Integer diasRankingMotoristas;
	
	/**
	 * 
	 * @author Tiago Nascimento - vilard@gmail.com 08/01/2015 08:16:01
	 * 
	 */
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="vinculo_estagiario_id", referencedColumnName="id")
	private VinculoEmpregaticio vinculoEstagiario = new VinculoEmpregaticio();
	
	//================================== GETERS and SETERS ====================================//

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cargo getCargoHabilitacao() {
		return cargoHabilitacao;
	}

	public void setCargoHabilitacao(Cargo cargoHabilitacao) {
		this.cargoHabilitacao = cargoHabilitacao;
	}

	public Funcao getFuncaoHabilitacao() {
		return funcaoHabilitacao;
	}

	public void setFuncaoHabilitacao(Funcao funcaoHabilitacao) {
		this.funcaoHabilitacao = funcaoHabilitacao;
	}

	public VinculoEmpregaticio getVinculoComissionado() {
		return vinculoComissionado;
	}

	public void setVinculoComissionado(VinculoEmpregaticio vinculoComissionado) {
		this.vinculoComissionado = vinculoComissionado;
	}

	public VinculoEmpregaticio getVinculoServidor() {
		return vinculoServidor;
	}

	public void setVinculoServidor(VinculoEmpregaticio vinculoServidor) {
		this.vinculoServidor = vinculoServidor;
	}

	public VinculoEmpregaticio getVinculoTerceirizado() {
		return vinculoTerceirizado;
	}

	public void setVinculoTerceirizado(VinculoEmpregaticio vinculoTerceirizado) {
		this.vinculoTerceirizado = vinculoTerceirizado;
	}

	public Integer getDiariasLimitePorMes() {
		return diariasLimitePorMes;
	}

	public void setDiariasLimitePorMes(Integer diariasLimitePorMes) {
		this.diariasLimitePorMes = diariasLimitePorMes;
	}

	public Integer getDiasRankingMotoristas() {
		return diasRankingMotoristas;
	}

	public void setDiasRankingMotoristas(Integer diasRankingMotoristas) {
		this.diasRankingMotoristas = diasRankingMotoristas;
	}

	public Sindicato getSindicatoMotorista() {
		return sindicatoMotorista;
	}

	public void setSindicatoMotorista(Sindicato sindicatoMotorista) {
		this.sindicatoMotorista = sindicatoMotorista;
	}

	public VinculoEmpregaticio getVinculoEstagiario() {
		return vinculoEstagiario;
	}

	public void setVinculoEstagiario(VinculoEmpregaticio vinculoEstagiario) {
		this.vinculoEstagiario = vinculoEstagiario;
	}
}
