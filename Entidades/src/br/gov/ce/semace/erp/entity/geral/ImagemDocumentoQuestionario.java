package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

/**
 * @author victor
 *
 */
@Entity
@Audited
@Table(name="imagem_documento_questionario", schema="geral")
public class ImagemDocumentoQuestionario implements Serializable {

	private static final long serialVersionUID = 7412941041734045683L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_imagem_documento_questionario", name="geral.seq_imagem_documento_questionario", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="geral.seq_imagem_documento_questionario")
	private Long id;

	@Column(name = "nome_arquivo")
	private String nomeArquivo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="resposta_questionario_id")
	private RespostaQuestionario respostaQuestionario;

	@Column(name = "file_type")
	private String fileType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documento_id")
	private Documento documento = new Documento();

	@Transient
	private byte[] content;

	@Transient
	private String caminhoTempFile;

//	@Transient
//	private StreamedContent streamedContent;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public RespostaQuestionario getRespostaQuestionario() {
		return respostaQuestionario;
	}

	public void setRespostaQuestionario(RespostaQuestionario respostaQuestionario) {
		this.respostaQuestionario = respostaQuestionario;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getCaminhoTempFile() {
		return caminhoTempFile;
	}

	public void setCaminhoTempFile(String caminhoTempFile) {
		this.caminhoTempFile = caminhoTempFile;
	}

//	public StreamedContent getStreamedContent() {
//		return streamedContent;
//	}
//
//	public void setStreamedContent(StreamedContent streamedContent) {
//		this.streamedContent = streamedContent;
//	}

}
