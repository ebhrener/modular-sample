package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="unidade_medida", schema="geral")
public class UnidadeMedida implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -8823725542353361162L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_unidade_medida", name="SEQUNIMEDIDA", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQUNIMEDIDA")
	private Long id;

	@NotEmpty(message="Campo Requerido")
	@Column(nullable=false)
	private String sigla;

	private String descricao;

	//-----------------------------------------------------------------Gets e Setters-----------------------------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public UnidadeMedida clone() throws CloneNotSupportedException {
		UnidadeMedida unidadeMedida = new UnidadeMedida();
		unidadeMedida.setDescricao(this.descricao);
		unidadeMedida.setId(this.id);
		unidadeMedida.setSigla(this.sigla);
		return unidadeMedida;
	}
}