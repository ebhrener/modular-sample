package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.licenciamento.CheckList;

@Entity
@Table(name = "atividade_requerimento_checklist", schema="atendimento")
@Audited
public class AtividadeRequerimentoChecklist implements Serializable {

	private static final long serialVersionUID = 6958342229343600228L;

	@Id
	@SequenceGenerator(sequenceName="atendimento.atividade_requerimento_checklist_id_seq", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agendamento_requerimento_id")
	private AgendamentoRequerimento agendamentoRequerimento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_id")
	private Atividade atividade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "checklist_id")
	private CheckList checklist;

	@Column(name="faturamento")
	private BigDecimal faturamento;

	@Column(name="area_construida")
	private BigDecimal areaConstruida;

	@Column(name="quantidade_funcionario")
	private Integer quantidadeFuncionario;

	@Column(name="selecionado")
	private boolean selecionado = false;

	@Transient
	private List<CheckList> listChecklist;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AgendamentoRequerimento getAgendamentoRequerimento() {
		return agendamentoRequerimento;
	}

	public void setAgendamentoRequerimento(
			AgendamentoRequerimento agendamentoRequerimento) {
		this.agendamentoRequerimento = agendamentoRequerimento;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public CheckList getChecklist() {
		return checklist;
	}

	public void setChecklist(CheckList checklist) {
		this.checklist = checklist;
	}

	public List<CheckList> getListChecklist() {
		return listChecklist;
	}

	public void setListChecklist(List<CheckList> listChecklist) {
		this.listChecklist = listChecklist;
	}

	public BigDecimal getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(BigDecimal faturamento) {
		this.faturamento = faturamento;
	}

	public BigDecimal getAreaConstruida() {
		return areaConstruida;
	}

	public void setAreaConstruida(BigDecimal areaConstruida) {
		this.areaConstruida = areaConstruida;
	}

	public Integer getQuantidadeFuncionario() {
		return quantidadeFuncionario;
	}

	public void setQuantidadeFuncionario(Integer quantidadeFuncionario) {
		this.quantidadeFuncionario = quantidadeFuncionario;
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((agendamentoRequerimento == null) ? 0
						: agendamentoRequerimento.hashCode());
		result = prime * result
				+ ((atividade == null) ? 0 : atividade.hashCode());
		result = prime * result
				+ ((checklist == null) ? 0 : checklist.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		AtividadeRequerimentoChecklist other = (AtividadeRequerimentoChecklist) obj;
		Boolean retorno = false;
		if(agendamentoRequerimento != null && other.getAgendamentoRequerimento() != null){
			if(atividade != null && checklist != null && other.getAtividade() != null && other.getChecklist() != null){
				retorno = atividade.getId().equals(other.getAtividade().getId()) && checklist.getId().equals(other.getChecklist().getId());
			} else if(atividade == null && checklist != null && other.getChecklist() != null && other.getAtividade() == null){
				retorno = checklist.getId().equals(other.getChecklist().getId());
			} else if (atividade != null && checklist == null && other.getAtividade() != null && other.getChecklist()== null) {
				retorno = atividade.getId().equals(other.getAtividade().getId());
			}
			retorno = retorno && agendamentoRequerimento.getId().equals(other.getAgendamentoRequerimento().getId());


		} else if(id != null && other.getId() != null){
			retorno = id.equals(other.getId());
		}

		return retorno;

	}

}
