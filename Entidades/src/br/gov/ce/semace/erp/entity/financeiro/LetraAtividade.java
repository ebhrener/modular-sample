package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.enuns.Letra;


@Entity
@Table(name = "letra_atividade", schema = "financeiro")
public class LetraAtividade implements Serializable {
	
	private static final long serialVersionUID = 6310053385360144144L;

	@Id
	@SequenceGenerator(sequenceName="financeiro.seq_letra_atividade", name="SEQ_LETRA_ATIVIDADE", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ_LETRA_ATIVIDADE")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_id")
	private Atividade atividade;
	
	private Letra letra;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public Letra getLetra() {
		return letra;
	}

	public void setLetra(Letra letra) {
		this.letra = letra;
	}

}
