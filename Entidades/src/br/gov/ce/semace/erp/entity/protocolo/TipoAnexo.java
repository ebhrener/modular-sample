package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="tipo_anexo", schema="geral")
public class TipoAnexo implements Serializable{

	private static final long serialVersionUID = 4138066516400191388L;

	@Id
	@SequenceGenerator(sequenceName ="geral.seq_tipo_anexo", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(nullable = false)
	private String descricao;

	@Column(nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
	private boolean shape = false;

	/**
	 * Campo booleano responsável por informar se o status do Tipo de Anexo é Ativo ou não.
	 * <p> <b>OBS:</b> Valor <code>default</code> é <code>true</code>
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 17/03/2014 13:51:55
	 */
	@Column(nullable = false)
	private boolean ativo = true;

	/**
	 * Campo {@link Transient}e utilizado para verificar o {@link TipoAnexo} foi selecionado ou não
	 * pelo usuário na tabela/lista.
	 *
	 * Por default é selecionado como falso.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 13/02/2014 09:06:08
	 */
	@Transient
	public boolean selecionado = false;

	/**
	 * Informa a descrição da propriedade ativo, no caso, Sim ou Não, representando ativo ou inativo, respectivamente.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - Mar 17, 2014 3:27:48 PM
	 *
	 * @return Retorna uma {@link String} com o valor <code>Sim</code>,
	 * 	caso o valor da propriedade ativo seja diferente de <code>null</code> e <code>true</code>. Caso Contrário, retorna <code>false</code>
	 */
	@Transient
	public String getDetalheAtivo(){

		if (this.ativo){
			return "Sim";
		}

		return "Não";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TipoAnexo other = (TipoAnexo) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the selecionado
	 */
	public boolean isSelecionado() {
		return selecionado;
	}

	/**
	 * @param selecionado the selecionado to set
	 */
	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	/**
	 * @return the ativo
	 */
	public boolean isAtivo() {
		return ativo;
	}

	/**
	 * @param ativo the ativo to set
	 */
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isShape() {
		return shape;
	}

	public void setShape(boolean shape) {
		this.shape = shape;
	}

}