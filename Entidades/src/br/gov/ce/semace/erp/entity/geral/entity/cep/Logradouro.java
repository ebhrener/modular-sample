package br.gov.ce.semace.erp.entity.geral.entity.cep;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;


@Entity
@Audited
@Table(name="logradouros", schema="cep")
public class Logradouro implements Serializable{

	private static final long serialVersionUID = -7646965994755979444L;
	
	@Id 
	@SequenceGenerator(sequenceName="cep.seq_logradouro", name="SEQLOGRA", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQLOGRA")
	@Column(name="cd_logradouro")
	@Index(name="log_i1")
	private Integer id; 
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cd_bairro")
	@Index(name="log_bairro_i1")
	private Bairro bairro = new Bairro();

	@Column(name="cd_tipo_logradouros")
	private String tipoLogradouro;
	
	@Column(name="ds_logradouro_nome")
	private String logradouro;
	
	@Column(name="no_logradouro_cep")
	@Index(name="log_i2")
	private String cep;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Bairro getBairro() {
		return bairro;
	}
	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}
	public String getTipoLogradouro() {
		return tipoLogradouro;
	}
	public void setTipoLogradouro(String tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
}
