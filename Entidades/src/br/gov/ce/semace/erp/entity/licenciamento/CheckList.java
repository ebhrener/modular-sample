package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.geral.GrupoAtividade;
import br.gov.ce.semace.erp.utils.StringUtils;

@Entity
@Audited
@Table(name="checklist", schema="atendimento")
public class CheckList implements Serializable{

	private static final long serialVersionUID = 7430887190092728212L;

	@Id @SequenceGenerator(sequenceName="atendimento.seq_checklist", name="SEQCHECK", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQCHECK")
	private Long id;

	@Column(name="titulo_checklist", length = 500)
	private String tituloCheckList;

	@Column(name="descricao_tipo_processo",columnDefinition="text")
	private String descricaoTipoProcesso;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="checklist_atividades", schema="atendimento", joinColumns=@JoinColumn(name="checklist_id"), inverseJoinColumns=@JoinColumn(name="atividade_id"))
	private List<Atividade> atividades = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tipos_processo_id")
	private TipoProcesso tiposProcesso = new TipoProcesso();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="subtipo_processo_id")
	private SubtipoProcesso subtipoProcesso = new SubtipoProcesso();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="modalidade_processo_id")
	private Modalidade modalidade = new Modalidade();

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="checklist_itens", schema="atendimento", joinColumns=@JoinColumn(name="checklist_id"),
		inverseJoinColumns=@JoinColumn(name="item_checklist_id"))
	private List<ItensCheckList> itensCheckLists = new ArrayList<>();

	@Column(columnDefinition="text")
	private String observacao;

	private Boolean ativo;

	/**
	 * Campo {@link Transient}e utilizado para verificar se esse {@link CheckList} é o que foi selecionado ou não
	 * pelo usuário na tabela/lista.
	 *
	 * Por default é selecionado como falso.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 18/11/2013 14:14:07
	 */
	@Transient
	public boolean selecionado = false;

	@Transient
	public String configuracao;

	//Campos para busca
	@Transient
	private GrupoAtividade grupoAtividade = new GrupoAtividade();

	@Transient
	private Atividade atividade = new Atividade();

	@Transient
	private boolean inicializaAtividade = true;

	//TODO:VERIFICAR SE EXISTE A POSSIBILIDADE DE SER ALTERADO APOS VINCULADO A UM PROCESSO

	@Override
	public CheckList clone() throws CloneNotSupportedException {
		CheckList checkList = new CheckList();
		checkList.setTituloCheckList(this.tituloCheckList);
		checkList.setDescricaoTipoProcesso(this.descricaoTipoProcesso);
		checkList.setAtividades(this.atividades);
		checkList.setTiposProcesso(this.tiposProcesso);
		checkList.setSubtipoProcesso(this.subtipoProcesso);
		checkList.setModalidade(this.modalidade);
		checkList.setItensCheckLists(new ArrayList<ItensCheckList>());
		if(itensCheckLists != null) {
			for (ItensCheckList itensCheckList : itensCheckLists) {
				checkList.getItensCheckLists().add(itensCheckList);
			}
		}
		checkList.setObservacao(this.observacao);
		checkList.setAtivo(this.ativo);

		return checkList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTituloCheckList() {
		return tituloCheckList;
	}

	public void setTituloCheckList(String tituloCheckList) {
		this.tituloCheckList = tituloCheckList;
	}

	public String getDescricaoTipoProcesso() {
		return descricaoTipoProcesso;
	}

	public void setDescricaoTipoProcesso(String descricaoTipoProcesso) {
		this.descricaoTipoProcesso = descricaoTipoProcesso;
	}

	public List<Atividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}

	public TipoProcesso getTiposProcesso() {
		return tiposProcesso;
	}

	public void setTiposProcesso(TipoProcesso tiposProcesso) {
		this.tiposProcesso = tiposProcesso;
	}

	public List<ItensCheckList> getItensCheckLists() {
		return itensCheckLists;
	}

	public void setItensCheckLists(List<ItensCheckList> itensCheckLists) {
		this.itensCheckLists = itensCheckLists;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public SubtipoProcesso getSubtipoProcesso() {
		return subtipoProcesso;
	}

	public void setSubtipoProcesso(SubtipoProcesso subtipoProcesso) {
		this.subtipoProcesso = subtipoProcesso;
	}
	public Modalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}

	public GrupoAtividade getGrupoAtividade() {
		return grupoAtividade;
	}

	public void setGrupoAtividade(GrupoAtividade grupoAtividade) {
		this.grupoAtividade = grupoAtividade;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public boolean isInicializaAtividade() {
		return inicializaAtividade;
	}

	public void setInicializaAtividade(boolean inicializaAtividade) {
		this.inicializaAtividade = inicializaAtividade;
	}

	@Override
	public int hashCode() {
		if(getId() != null){
			return getId().hashCode();
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof CheckList)) {
			return false;
		}
		return ((CheckList)obj).getId().equals(getId());
	}

	/**
	 * @return the selecionado
	 */
	public boolean isSelecionado() {
		return selecionado;
	}

	/**
	 * @param selecionado the selecionado to set
	 */
	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	public String getConfiguracao() {
		String conf = "";
		if (tiposProcesso != null && StringUtils.isNotBlank(tiposProcesso.getDescricao())) {
			conf = tiposProcesso.getDescricao();
		}
		if (modalidade != null && StringUtils.isNotBlank(modalidade.getDescricao())) {
			conf = conf + " - " + modalidade.getDescricao();
		}
		if (subtipoProcesso != null && StringUtils.isNotBlank(subtipoProcesso.getDescricao())) {
			conf = conf + " - " + subtipoProcesso.getDescricao();
		}
		return conf;
	}

}