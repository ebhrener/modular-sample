package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="rh",name="nivel_hierarquico")
public class NivelHierarquico implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8824902869282240960L;
	
	@Id
	@SequenceGenerator(sequenceName="rh.seq_nivel_hierarquico",name="SEQNIVEL",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQNIVEL")
	private Long id;
	
	
	private Integer nivel;
	
	
	private String descricao;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
