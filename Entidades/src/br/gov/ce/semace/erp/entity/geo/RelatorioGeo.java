package br.gov.ce.semace.erp.entity.geo;

import java.sql.Timestamp;

/**
 * Classe usada para mapear os atributos da consulta com o JasperReport
 * 
 * @author Joerlan
 */
public class RelatorioGeo {

	private String spu_id;
	private String requerimento_id;
	private String nome_fantasia;
	private String municipio;
	private String grupo_condicionante;
	private Timestamp data_validade;
	private String atividade;
	private String atividade_id2;
	
	public RelatorioGeo() {
		super();
	}
	
	public RelatorioGeo(String spu, String cliente, String municipio, String tipoLicenca, Timestamp validade, String atividade, String atividade_id2, String requerimento_id) {
		this();
		this.spu_id = spu;
		this.requerimento_id = requerimento_id;
		this.nome_fantasia = cliente;
		this.municipio = municipio;
		this.grupo_condicionante = tipoLicenca;
		this.data_validade = validade;
		this.atividade = atividade;
		this.atividade_id2 = atividade_id2;
	}

	public String getSpu_id() {
		return spu_id;
	}

	public void setSpu_id(String spu_id) {
		this.spu_id = spu_id;
	}

	public String getNome_fantasia() {
		return nome_fantasia;
	}

	public void setNome_fantasia(String nome_fantasia) {
		this.nome_fantasia = nome_fantasia;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	
	public String getGrupo_condicionante() {
		return grupo_condicionante;
	}

	public void setGrupo_condicionante(String grupo_condicionante) {
		this.grupo_condicionante = grupo_condicionante;
	}

	public Timestamp getData_validade() {
		return data_validade;
	}

	public void setData_validade(Timestamp data_validade) {
		this.data_validade = data_validade;
	}

	public String getAtividade() {
		return atividade;
	}

	public void setAtividade(String atividade) {
		this.atividade = atividade;
	}

	public String getAtividade_id2() {
		return atividade_id2;
	}

	public void setAtividade_id2(String atividade_id2) {
		this.atividade_id2 = atividade_id2;
	}

	public String getRequerimento_id() {
		return requerimento_id;
	}

	public void setRequerimento_id(String requerimento_id) {
		this.requerimento_id = requerimento_id;
	}
}
