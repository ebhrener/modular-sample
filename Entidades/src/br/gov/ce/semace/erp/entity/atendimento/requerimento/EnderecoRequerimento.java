package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;
import br.gov.ce.semace.erp.entity.geral.entity.cep.UF;
import br.gov.ce.semace.erp.utils.StringUtils;

@Entity
@Audited
@Table(name="endereco_requerimento", schema="atendimento")
public class EnderecoRequerimento implements Serializable{

	private static final long serialVersionUID = 2942367731629214449L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_endereco_requerimento", name="SEQ_ENDERECO_REQUERIMENTO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ_ENDERECO_REQUERIMENTO")
//	@Index(name="end_pk_i1")
	private Long id;

	private String logradouro;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cidade_id")
	private Cidade cidade = new Cidade();

	private String bairro;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="uf_id")
	private UF uf = new UF();

	private String numero;

	private String complemento;

	@Column(name="caixa_postal")
	private String caixaPostal;

	private String cep;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public String getCaixaPostal() {
		return caixaPostal;
	}

	public void setCaixaPostal(String caixaPostal) {
		this.caixaPostal = caixaPostal;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@Override
	public String toString() {
		/** Rua Jaime Benevolo, 1400 - Fátima, Fortaleza - CE, 60050-081 */

		StringBuilder sb = new StringBuilder();

		if(StringUtils.isNotBlank(logradouro)) {
			sb.append(logradouro.trim());
		}

		if(StringUtils.isNotBlank(numero)) {
			if(numero.equalsIgnoreCase("s/n")) {
				sb.append(", " + getNumero());
			} else {
				sb.append(", nº " + getNumero());
			}
		}
		if (StringUtils.isNotBlank(complemento)) {
			sb.append(", " + getComplemento());
		}

		if(StringUtils.isNotBlank(bairro)) {
			sb.append(" - " + getBairro().trim());
		}

		if(cidade != null && StringUtils.isNotBlank(cidade.getDescricao())) {
			sb.append(", " + cidade.getDescricao().trim());
			if(uf != null && StringUtils.isNotBlank(uf.getSigla())) {
				sb.append(" - " + uf.getSigla().trim());
			}
		}

		if(StringUtils.isNotBlank(cep) && cep.length() >= 8) {
			sb.append(", " + cep.subSequence(0, 5));
			sb.append("-");
			sb.append(cep.subSequence(5, 8));
		}

		return sb.toString();
	}

}