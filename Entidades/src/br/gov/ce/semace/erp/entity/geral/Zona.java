package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;

/**
 * @author marcus
 *
 */
@Entity
@Audited
@Table(schema="geral", name="zona")
public class Zona implements Comparable<Zona>, Serializable{

	private static final long serialVersionUID = -4749650518816940213L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_zona", name="geral.seq_zona", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="geral.seq_zona")
	private Long id;
	private String descricao;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE, mappedBy = "zona")
	@JsonBackReference
	private List<Cidade> cidades = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}

	@Override
	public int hashCode() {
		if(getId() != null) {
			return getId().hashCode();
		}

		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Zona)) {
			return false;
		}
		return ((Zona)obj).getId().equals(getId());
	}

	@Override
	public int compareTo(Zona zona) {
		return getId().compareTo(zona.getId());
	}
}
