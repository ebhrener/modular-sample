package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.gov.ce.semace.erp.entity.geral.PessoaJuridica;

@Entity
@Audited
@Table(name="corresponsavel", schema="juridico")
public class Corresponsavel implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 5390371934560315985L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_corresponsavel", name="juridico.seq_corresponsavel", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_corresponsavel")
	private Long id;

	@Column(nullable=false)
	private String nome;

	@Column(nullable=false)
	private String documento;

	private String endereco;
	private String numero;
	private String bairro;
	private String municipio;
	private String uf;
	private String cep;

	private String telefone;

	private boolean pessoaFisica = true;

	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="pessoa_juridica_id")
	@JsonManagedReference
	private PessoaJuridica empresa;

	@Column(name="fundamento",columnDefinition="text")
	private String fundamentoCorresponsabilidade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public PessoaJuridica getEmpresa() {
		return empresa;
	}

	public void setEmpresa(PessoaJuridica empresa) {
		this.empresa = empresa;
	}

	public boolean isPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(boolean pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}


	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Corresponsavel other = (Corresponsavel) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public String getFundamentoCorresponsabilidade() {
		return fundamentoCorresponsabilidade;
	}

	public void setFundamentoCorresponsabilidade(
			String fundamentoCorresponsabilidade) {
		this.fundamentoCorresponsabilidade = fundamentoCorresponsabilidade;
	}

}
