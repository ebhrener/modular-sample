package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.gov.ce.semace.erp.entity.geral.Zona;
import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;
import br.gov.ce.semace.erp.entity.rh.Setor;

@Entity
@Table(name="configuracao_laboratorio", schema="laboratorio")
public class ConfiguracaoLaboratorio implements Serializable {

	private static final long serialVersionUID = 3174517531631253767L;

	@Id
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="setor_laboratorio_id", nullable=false)
	private Setor setorLaboratorio = new Setor();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="municipio_id", nullable=false)
	private Cidade cidade = new Cidade();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="zona_id", nullable=false)
	private Zona zona = new Zona();

	@Column(name= "data_ci_597_2018")
	private Date dataDeployCI_597_2018;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Setor getSetorLaboratorio() {
		return setorLaboratorio;
	}

	public void setSetorLaboratorio(Setor setorLaboratorio) {
		this.setorLaboratorio = setorLaboratorio;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	public Date getDataDeployCI_597_2018() {
		return dataDeployCI_597_2018;
	}

	public void setDataDeployCI_597_2018(Date dataDeployCI_597_2018) {
		this.dataDeployCI_597_2018 = dataDeployCI_597_2018;
	}

}
