package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.atendimento.pendenciaRequerimento.PendenciaRequerimento;
import br.gov.ce.semace.erp.entity.financeiro.PendenciaFinanceiro;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.StatusPendenciaEnum;

/**
 * <h1>Entidade {@link Pendencia} do schema 'geral'</h1>
 * <br>
 * <b>{@link Pendencia} possui os seguintes atributos:</b>
 * <ul>
 *   <li>{@link Pendencia#dataCriacao}: Data em que a pendência foi criada.</li>
 *   <li>{@link Pendencia#notificado}: Informa se o interessado foi notificado.</li>
 *   <li>{@link Pendencia#statusPendenciaEnum}: Informa o estado atual da pendência.</li>
 *   <li>{@link Pendencia#dataAtualizacao}: Data em que a pendência foi atualizada pela última vez.</li>
 *   <li>{@link Pendencia#comentario}: Comentário adicionado pelo atendente que criou a pendência.</li>
 *   <li>{@link Pendencia#funcionario}: Funcionário que criou a pendência.</li>
 *   <li>{@link Pendencia#tecnicoNotificado}: Informa se o técnico foi notificado caso a pendência não tenha sido resolvida até o prazo.</li>
 * </ul>
 *
 * @author robson.carvalho
 */
@Entity
@Audited
@Table(schema = "geral", name = "pendencia")
@Inheritance(strategy = InheritanceType.JOINED)
public class Pendencia implements Serializable {

	private static final long serialVersionUID = -6049237327302925410L;

	@Id @Column(name="pendencia_id")
	@SequenceGenerator(sequenceName="geral.seq_pendencia", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@Column(name = "data_criacao", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCriacao;

	/**
	 * Flag para ter a informação de quando o interessado teve ciência da existencia da pendência.
	 */
	@Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
	private Boolean notificado = Boolean.FALSE;

	@Column(name = "status_pendencia", nullable = false, columnDefinition = "INTEGER DEFAULT 0")
	@Enumerated(EnumType.ORDINAL)
	private StatusPendenciaEnum statusPendenciaEnum = StatusPendenciaEnum.ABERTO;

	@Column(name = "data_atualizacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAtualizacao;

	@NotEmpty
	@Column(length = 255, nullable = false)
	private String comentario;

	@JoinColumn(name = "funcionario_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Funcionario funcionario;

	@Column(name = "tecnico_notificado", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
	private Boolean tecnicoNotificado = Boolean.FALSE;

	@Temporal(TemporalType.TIMESTAMP)
	private Date prazo;

	@Column(name = "data_notificacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataNotificacao;

	@OneToMany(mappedBy = "pendencia", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<ArquivoPendencia> listArquivoPendencia;

	/**
	 * Flag para ter a informação de quando o interessado visualizou a pendência.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_visualizacao")
	private Date dataVisualizacao;

	@Transient
	private Integer prazoEmDias = 0;

	@Transient
	private String obs;

	@Transient
    public boolean isPendenciaEmAberto() {
		return this.statusPendenciaEnum.isAberto();
    }

	@Transient
    public boolean isPendenciaEmAguardandoAnalise() {
		return this.statusPendenciaEnum.isAguardandoAnalise();
    }

	@Transient
    public boolean isPendenciaResolvida() {
		return this.statusPendenciaEnum.isResolvido();
    }

	@Transient
	public boolean isPendenciaLiberada() {
		return this.statusPendenciaEnum.isLiberado();
	}

	public String getDisplayNotificado() {
		if (this.notificado != null) {
			return this.notificado ? "Sim" : "Não";
		}
		return "-";
	}

	@Transient
	public String getDisplayComentario(int length) {
		if (this.comentario.length() > length) {
			return this.comentario = this.comentario.trim().substring(0, length).concat("...");
		}
		return this.comentario;
	}

	@Transient
	public void setAsResolvido() {
		this.dataAtualizacao = new Date();
		this.statusPendenciaEnum = StatusPendenciaEnum.RESOLVIDO;
	}

	@Transient
	public void setAsEmAberto() {
		this.dataAtualizacao = new Date();
		this.statusPendenciaEnum = StatusPendenciaEnum.ABERTO;
	}

	@Transient
	public void setAsAguardandoAnalise() {
		this.dataAtualizacao = new Date();
		this.statusPendenciaEnum = StatusPendenciaEnum.AGUARDANDO_ANALISE;
	}

	@Transient
	public void setAsLiberado() {
		this.dataAtualizacao = new Date();
		this.statusPendenciaEnum = StatusPendenciaEnum.LIBERADO;
	}

	@Transient
	public String getDisplayTipoPendencia() {
		if (this instanceof PendenciaRequerimento) {
			return "Pendência de documentação";
		} else if (this instanceof PendenciaFinanceiro) {
			return "Pendência financeira";
		}
		return "-";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Boolean getNotificado() {
		return notificado;
	}

	public void setNotificado(Boolean notificado) {
		this.notificado = notificado;
	}

	public StatusPendenciaEnum getStatusPendenciaEnum() {
		return statusPendenciaEnum;
	}

	public void setStatusPendenciaEnum(StatusPendenciaEnum statusPendenciaEnum) {
		this.statusPendenciaEnum = statusPendenciaEnum;
	}

	public Date getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Date getPrazo() {
		return prazo;
	}

	public void setPrazo(Date prazo) {
		this.prazo = prazo;
	}

	public Integer getPrazoEmDias() {
		return prazoEmDias;
	}

	public void setPrazoEmDias(Integer prazoEmDias) {
		this.prazoEmDias = prazoEmDias;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public String getDescricao(){
		return "";
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public Boolean getTecnicoNotificado() {
		return tecnicoNotificado;
	}

	public void setTecnicoNotificado(Boolean tecnicoNotificado) {
		this.tecnicoNotificado = tecnicoNotificado;
	}

	public Date getDataNotificacao() {
		return dataNotificacao;
	}

	public void setDataNotificacao(Date dataNotificacao) {
		this.dataNotificacao = dataNotificacao;
	}

	public List<ArquivoPendencia> getListArquivoPendencia() {
		return listArquivoPendencia;
	}

	public void setListArquivoPendencia(List<ArquivoPendencia> listArquivoPendencia) {
		this.listArquivoPendencia = listArquivoPendencia;
	}

	public Date getDataVisualizacao() {
		return dataVisualizacao;
	}

	public void setDataVisualizacao(Date dataVisualizacao) {
		this.dataVisualizacao = dataVisualizacao;
	}

	/*
	 * Utilizado para visualização do item da pendência, para ficar similar ao {@link ItensCheckList}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 29/10/2013 11:16:19
	 *
	 * @return retorna o titulo do da {@link Pendencia}
	 */
	public String getTitulo() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((comentario == null) ? 0 : comentario.hashCode());
		result = prime * result
				+ ((dataAtualizacao == null) ? 0 : dataAtualizacao.hashCode());
		result = prime * result
				+ ((dataCriacao == null) ? 0 : dataCriacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Pendencia other = (Pendencia) obj;
		if (comentario == null) {
			if (other.comentario != null) {
				return false;
			}
		} else if (!comentario.equals(other.comentario)) {
			return false;
		}
		if (dataAtualizacao == null) {
			if (other.dataAtualizacao != null) {
				return false;
			}
		} else if (!dataAtualizacao.equals(other.dataAtualizacao)) {
			return false;
		}
		if (dataCriacao == null) {
			if (other.dataCriacao != null) {
				return false;
			}
		} else if (!dataCriacao.equals(other.dataCriacao)) {
			return false;
		}
		return true;
	}
}