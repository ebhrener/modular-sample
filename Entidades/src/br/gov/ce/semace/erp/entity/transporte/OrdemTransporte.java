package br.gov.ce.semace.erp.entity.transporte;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.planoviagem.SolicitacaoViagem;
import br.gov.ce.semace.erp.enuns.StatusOrdemTransporte;

/**
 * Entidade {@link OrdemTransporte} do esquema Transporte.
 *
 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 19/05/2014 11:54:27
 *
 */
@Entity
@Audited
@Table(schema="transporte", name="ordem_transporte")
public class OrdemTransporte implements Serializable{

	private static final long serialVersionUID = 496950962687834632L;

	@Id
	@SequenceGenerator(sequenceName="transporte.seq_ordem_transporte", name="SEQ_ORDEM_TRANSPORTE", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ_ORDEM_TRANSPORTE")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="documento_id")
	private Documento documento = new Documento();

	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="solicitacao_transporte_id")
	private SolicitacaoTransporte solicitacaoTransporte;

	@Column(name="status_ordem_transporte")
	private StatusOrdemTransporte statusOrdemTransporte;

	@Column(name="justificativa", columnDefinition="text")
	private String justificativa;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "ordemTransporte")
	private List<OrdemTransporteVeiculo> ordemTransporteVeiculos = new ArrayList<>();

	@Transient
	private SolicitacaoViagem solicitacaoViagem;

	//===================================== TRANSIENTE ========================================//

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusOrdemTransporte} é
	 * 	do tipo <b>AGUARDANDO_VEICULO</b>.
	 *
	 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 26/05/2014 08:10:58
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusOrdemTransporte} seja
	 * 	<b>AGUARDANDO_VEICULO</b>. Caso contrário, retorna <code>false</code>.
	 *
	 * @see StatusOrdemTransporte#isAguardandoVeiculo()
	 */
	@Transient
	public boolean isAguardandoVeiculo(){
		return this.statusOrdemTransporte.isAguardandoVeiculo();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusOrdemTransporte} é
	 * 	do tipo <b>INICIADA</b>.
	 *
	 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 26/05/2014 08:13:23
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusOrdemTransporte} seja
	 * 	<b>INICIADA</b>. Caso contrário, retorna <code>false</code>.
	 *
	 * @see StatusOrdemTransporte#isIniciada()
	 */
	@Transient
	public boolean isIniciada(){
		return this.statusOrdemTransporte.isIniciada();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusOrdemTransporte} é
	 * 	do tipo <b>FINALIZADA</b>.
	 *
	 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 26/05/2014 08:14:32
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusOrdemTransporte} seja
	 * 	<b>FINALIZADA</b>. Caso contrário, retorna <code>false</code>.
	 *
	 * @see StatusOrdemTransporte#isFinalizada()
	 */
	@Transient
	public boolean isFinalizada(){
		return this.statusOrdemTransporte.isFinalizada();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusOrdemTransporte} é
	 * 	do tipo <b>CANCELADA</b>.
	 *
	 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 26/05/2014 08:15:34
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusOrdemTransporte} seja
	 * 	<b>CANCELADA</b>. Caso contrário, retorna <code>false</code>.
	 *
	 * @see StatusOrdemTransporte#isFinalizada()
	 */
	@Transient
	public boolean isCancelada(){
		return this.statusOrdemTransporte.isCancelada();
	}

	//================================== GETERS and SETERS ====================================//

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public SolicitacaoTransporte getSolicitacaoTransporte() {
		return solicitacaoTransporte;
	}

	public void setSolicitacaoTransporte(SolicitacaoTransporte solicitacaoTransporte) {
		this.solicitacaoTransporte = solicitacaoTransporte;
	}

	public StatusOrdemTransporte getStatusOrdemTransporte() {
		return statusOrdemTransporte;
	}

	public void setStatusOrdemTransporte(StatusOrdemTransporte statusOrdemTransporte) {
		this.statusOrdemTransporte = statusOrdemTransporte;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public List<OrdemTransporteVeiculo> getOrdemTransporteVeiculos() {
		return ordemTransporteVeiculos;
	}

	public void setOrdemTransporteVeiculos(
			List<OrdemTransporteVeiculo> ordemTransporteVeiculos) {
		this.ordemTransporteVeiculos = ordemTransporteVeiculos;
	}

	public SolicitacaoViagem getSolicitacaoViagem() {
		return solicitacaoViagem;
	}

	public void setSolicitacaoViagem(SolicitacaoViagem solicitacaoViagem) {
		this.solicitacaoViagem = solicitacaoViagem;
	}

}
