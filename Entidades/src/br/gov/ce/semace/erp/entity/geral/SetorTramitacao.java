package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Setor;

@Entity
@Audited
@Table(name = "setor_tramitacao", schema="geral")
public class SetorTramitacao implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -4124892802050071479L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_setor_tramitacao", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setor_id")
	private Setor setor = new Setor();

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "setor_tramitacao_setor",
			schema = "geral",
			joinColumns = @JoinColumn(name = "setor_pai_id"),
			inverseJoinColumns = @JoinColumn(name = "setor_filho_id"))
	private List<Setor> setoresRelacionados = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parametro_id")
	private Parametro parametro;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public List<Setor> getSetoresRelacionados() {
		return setoresRelacionados;
	}

	public void setSetoresRelacionados(List<Setor> setoresRelacionados) {
		this.setoresRelacionados = setoresRelacionados;
	}

	public Parametro getParametro() {
		return parametro;
	}

	public void setParametro(Parametro parametro) {
		this.parametro = parametro;
	}

	@Override
	public String toString() {
		String retorno = "";
		if(setor != null && setor.getId() != null){
			retorno = "Setor Pai: "+setor.getSigla()+" - Setores Relacionados: ";
			for(Setor setorRelacionado: setoresRelacionados){
				retorno += setorRelacionado.getSigla()+" , ";
			}
			return retorno;
		}
		return super.toString();
	}
}
