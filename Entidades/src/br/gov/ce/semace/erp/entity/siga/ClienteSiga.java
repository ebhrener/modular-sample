package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the cliente database table.
 * @author patrick
 */
@Entity
@Table(name="cliente", schema="adm_siga")
public class ClienteSiga implements Serializable {

	private static final long serialVersionUID = 4978364755012532407L;

	@Id
	@Column(name="cliente_id")
	private String clienteId;

	private String atividade;

	@Column(name="celular_cliente")
	private String celularCliente;

	@Column(name="cep_id")
	private String cepId;

	private String cgf;

	private String classificacao;

	private String cliente;

	private String cnpj;

	private String complemento;

	private String cpf;

	private String crea;

	@Column(name="crea_valid_f")
	private String creaValidF;

	@Column(name="data_cadastro")
	private Timestamp dataCadastro;

	@Column(name="dt_nascimento")
	private Timestamp dtNascimento;

	private String email;

	@Column(name="email_cliente")
	private String emailCliente;

	@Column(name="fax_cliente")
	private String faxCliente;

	private String formacao;

	private Integer interesse;

	private String nivel;

	@Column(name="nome_fantasia")
	private String nomeFantasia;

	@Column(name="orgao_rg")
	private String orgaoRg;

	@Column(name="profissional_id")
	private Integer profissionalId;

	private String rg;

	private float saldo;

	private String senha;

	private String sexo;

	private String status;

	@Column(name="telefone_cliente")
	private String telefoneCliente;

	@Column(name="tipo_cliente_id")
	private Integer tipoClienteId;

	private Boolean travado;

	@Column(name="uf_rg")
	private String ufRg;

	private Boolean urgente;

	@Column(name="vinculo_id")
	private String vinculoId;

    public ClienteSiga() {
    }

	public String getClienteId() {
		return this.clienteId;
	}

	public void setClienteId(String clienteId) {
		this.clienteId = clienteId;
	}

	public String getAtividade() {
		return this.atividade;
	}

	public void setAtividade(String atividade) {
		this.atividade = atividade;
	}

	public String getCelularCliente() {
		return this.celularCliente;
	}

	public void setCelularCliente(String celularCliente) {
		this.celularCliente = celularCliente;
	}

	public String getCepId() {
		return this.cepId;
	}

	public void setCepId(String cepId) {
		this.cepId = cepId;
	}

	public String getCgf() {
		return this.cgf;
	}

	public void setCgf(String cgf) {
		this.cgf = cgf;
	}

	public String getClassificacao() {
		return this.classificacao;
	}

	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}

	public String getCliente() {
		return this.cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getComplemento() {
		return this.complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCpf() {
		return this.cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCrea() {
		return this.crea;
	}

	public void setCrea(String crea) {
		this.crea = crea;
	}

	public String getCreaValidF() {
		return this.creaValidF;
	}

	public void setCreaValidF(String creaValidF) {
		this.creaValidF = creaValidF;
	}

	public Timestamp getDataCadastro() {
		return this.dataCadastro;
	}

	public void setDataCadastro(Timestamp dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Timestamp getDtNascimento() {
		return this.dtNascimento;
	}

	public void setDtNascimento(Timestamp dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailCliente() {
		return this.emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public String getFaxCliente() {
		return this.faxCliente;
	}

	public void setFaxCliente(String faxCliente) {
		this.faxCliente = faxCliente;
	}

	public String getFormacao() {
		return this.formacao;
	}

	public void setFormacao(String formacao) {
		this.formacao = formacao;
	}

	public Integer getInteresse() {
		return this.interesse;
	}

	public void setInteresse(Integer interesse) {
		this.interesse = interesse;
	}

	public String getNivel() {
		return this.nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getNomeFantasia() {
		return this.nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getOrgaoRg() {
		return this.orgaoRg;
	}

	public void setOrgaoRg(String orgaoRg) {
		this.orgaoRg = orgaoRg;
	}

	public Integer getProfissionalId() {
		return this.profissionalId;
	}

	public void setProfissionalId(Integer profissionalId) {
		this.profissionalId = profissionalId;
	}

	public String getRg() {
		return this.rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public float getSaldo() {
		return this.saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTelefoneCliente() {
		return this.telefoneCliente;
	}

	public void setTelefoneCliente(String telefoneCliente) {
		this.telefoneCliente = telefoneCliente;
	}

	public Integer getTipoClienteId() {
		return this.tipoClienteId;
	}

	public void setTipoClienteId(Integer tipoClienteId) {
		this.tipoClienteId = tipoClienteId;
	}

	public Boolean getTravado() {
		return this.travado;
	}

	public void setTravado(Boolean travado) {
		this.travado = travado;
	}

	public String getUfRg() {
		return this.ufRg;
	}

	public void setUfRg(String ufRg) {
		this.ufRg = ufRg;
	}

	public Boolean getUrgente() {
		return this.urgente;
	}

	public void setUrgente(Boolean urgente) {
		this.urgente = urgente;
	}

	public String getVinculoId() {
		return this.vinculoId;
	}

	public void setVinculoId(String vinculoId) {
		this.vinculoId = vinculoId;
	}

}