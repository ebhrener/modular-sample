package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.AgendamentoRequerimento;

@Entity
@Audited
@Table(name = "erro_descarte_requerimento", schema = "geral")
public class ErroDescarteRequerimento implements Serializable {

	private static final long serialVersionUID = 4977966584629605390L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_erro_descarte_requerimento", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "requerimento_id")
	private AgendamentoRequerimento requerimento;

	@Column(name = "qtd_erro")
	private int qtd;

	@Column(name = "pilha", columnDefinition = "text")
	private String pilha;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora")
	private Date data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AgendamentoRequerimento getRequerimento() {
		return requerimento;
	}

	public void setRequerimento(AgendamentoRequerimento requerimento) {
		this.requerimento = requerimento;
	}

	public int getQtd() {
		return qtd;
	}

	public void setQtd(int qtd) {
		this.qtd = qtd;
	}

	public String getPilha() {
		return pilha;
	}

	public void setPilha(String pilha) {
		this.pilha = pilha;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

}
