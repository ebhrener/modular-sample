package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "mapa_resposta", schema = "geral")
public class MapaResposta implements Serializable {

	private static final long serialVersionUID = 3410030539060414699L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_mapa_resposta", name = "geral.seq_mapa_resposta", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geral.seq_mapa_resposta")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "resposta_questionario_id")
	private RespostaQuestionario respostaQuestionario = new RespostaQuestionario();

	@Column(name = "resposta1",columnDefinition="text")
	private String resposta1;

	@Column(name = "resposta2",columnDefinition="text")
	private String resposta2;

	@Column(name = "resposta3",columnDefinition="text")
	private String resposta3;

	@Column(name = "resposta4",columnDefinition="text")
	private String resposta4;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RespostaQuestionario getRespostaQuestionario() {
		return respostaQuestionario;
	}

	public void setRespostaQuestionario(RespostaQuestionario respostaQuestionario) {
		this.respostaQuestionario = respostaQuestionario;
	}

	public String getResposta1() {
		return resposta1;
	}

	public void setResposta1(String resposta1) {
		this.resposta1 = resposta1;
	}

	public String getResposta2() {
		return resposta2;
	}

	public void setResposta2(String resposta2) {
		this.resposta2 = resposta2;
	}

	public String getResposta3() {
		return resposta3;
	}

	public void setResposta3(String resposta3) {
		this.resposta3 = resposta3;
	}

	public String getResposta4() {
		return resposta4;
	}

	public void setResposta4(String resposta4) {
		this.resposta4 = resposta4;
	}


}
