package br.gov.ce.semace.erp.entity.siga;

public class Condicionante {

	private int id;
	private String condicionante;
	private int dias;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCondicionante() {
		return condicionante;
	}

	public void setCondicionante(String condicionante) {
		this.condicionante = condicionante;
	}

	public int getDias() {
		return dias;
	}

	public void setDias(int dias) {
		this.dias = dias;
	}

}
