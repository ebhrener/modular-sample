package br.gov.ce.semace.erp.entity.seguranca;

import java.io.Serializable;

/**
 * Representa entity de usuário de sistema 
 */
public interface IUsuario extends Serializable {

	/**
	 * @return ID do usuário
	 */
	Long getId();
	
	/**
	 * @return login do usuário (deve ser unique)
	 */
	String getLogin();
}
