package br.gov.ce.semace.erp.entity.transporte;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.StatusOrdemTransporte;

/**
 * Entidade {@link HistoricoOrdemTransporte} do esquema Transporte.
 *
 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 27/05/2014 11:14:31
 */
@Entity
@Audited
@Table(schema="transporte", name="historico_ordem_transporte")
public class HistoricoOrdemTransporte implements Serializable{

	private static final long serialVersionUID = -8384079612913531671L;

	@Id
	@SequenceGenerator(sequenceName="transporte.seq_historico_ordem_transporte", name="SEQ_HISTORICO_ORDEM_TRANSPORTE", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ_HISTORICO_ORDEM_TRANSPORTE")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="ordem_transporte_id")
	private OrdemTransporte ordemTransporte = new OrdemTransporte();

	@Column(name="status_ordem_transporte")
	private StatusOrdemTransporte statusOrdemTransporte;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario = new Funcionario();

	@Column(name = "data_acao")
	private Timestamp dataAcao;

	@Column(name="descricao", columnDefinition="text")
	private String descricao;

	//================================== GETERS and SETERS ====================================//

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrdemTransporte getOrdemTransporte() {
		return ordemTransporte;
	}

	public void setOrdemTransporte(OrdemTransporte ordemTransporte) {
		this.ordemTransporte = ordemTransporte;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Timestamp getDataAcao() {
		return dataAcao;
	}

	public void setDataAcao(Timestamp dataAcao) {
		this.dataAcao = dataAcao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public StatusOrdemTransporte getStatusOrdemTransporte() {
		return statusOrdemTransporte;
	}

	public void setStatusOrdemTransporte(StatusOrdemTransporte statusOrdemTransporte) {
		this.statusOrdemTransporte = statusOrdemTransporte;
	}

}
