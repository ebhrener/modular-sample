package br.gov.ce.semace.erp.entity.protocolo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "arquivo_doc_anexo", schema = "protocolo")
public class ArquivoDocAnexo extends ArquivoProcesso {

	private static final long serialVersionUID = -5829377119344083805L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "doc_anexo_id")
	private DocAnexo docAnexo = new DocAnexo();

	private Boolean validado;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "pendencia_processo_arquivo_doc_anexo",
		schema = "protocolo",
		joinColumns = @JoinColumn(name = "arquivo_documento_id"),
		inverseJoinColumns = @JoinColumn(name = "pendencia_id"))
	private List<PendenciaProcesso> listPendenciaProcesso;

	public DocAnexo getDocAnexo() {
		return docAnexo;
	}

	public void setDocAnexo(DocAnexo docAnexo) {
		this.docAnexo = docAnexo;
	}

	public Boolean getValidado() {
		return validado;
	}

	public void setValidado(Boolean validado) {
		this.validado = validado;
	}

	public List<PendenciaProcesso> getListPendenciaProcesso() {
		return listPendenciaProcesso;
	}

	public void setListPendenciaProcesso(List<PendenciaProcesso> listPendenciaProcesso) {
		this.listPendenciaProcesso = listPendenciaProcesso;
	}

}