package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema = "financeiro", name = "ufirce")
public class Ufirce implements Serializable{

	private static final long serialVersionUID = -3086263147173613844L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_ufirce", name = "SEQ_UFIRCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_UFIRCE")
	private Long id;

	@Column(nullable = false)
	private Integer ano;

	@Column(nullable = false, precision = 12, scale = 8)
	private BigDecimal valor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

}
