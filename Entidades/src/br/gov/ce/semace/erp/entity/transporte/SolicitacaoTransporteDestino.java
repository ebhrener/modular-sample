package br.gov.ce.semace.erp.entity.transporte;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;

/**
 *
 * Entidade {@link SolicitacaoTransporteDestino} do esquema Transporte.
 *
 * @author Harisson Rafael [harissonrafael@gmail.com] - 30/07/2014 15:59:22
 *
 */
@Entity
@Audited
@Table(schema = "transporte", name = "solicitacao_transporte_destino")
public class SolicitacaoTransporteDestino implements Serializable {

	private static final long serialVersionUID = -2408743020986748475L;

	@Id
	@SequenceGenerator(sequenceName = "transporte.seq_solicitacao_transporte_destino", name = "SEQ_SOLICITACAO_TRANSPORTE_DESTINO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SOLICITACAO_TRANSPORTE_DESTINO")
	private Long id;

	@Column (name = "data_hora_chegada")
	private Date dataHoraChegada;

	@Column (name = "data_hora_saida")
	private Date dataHoraSaida;

	@Column
	private boolean visitado;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "municipio_id")
	private Cidade cidade;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="solicitacao_transporte_id")
	private SolicitacaoTransporte solicitacaoTransporte;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataHoraChegada() {
		return dataHoraChegada;
	}

	public void setDataHoraChegada(Date dataHoraChegada) {
		this.dataHoraChegada = dataHoraChegada;
	}

	public Date getDataHoraSaida() {
		return dataHoraSaida;
	}

	public void setDataHoraSaida(Date dataHoraSaida) {
		this.dataHoraSaida = dataHoraSaida;
	}

	public boolean isVisitado() {
		return visitado;
	}

	public void setVisitado(boolean visitado) {
		this.visitado = visitado;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public SolicitacaoTransporte getSolicitacaoTransporte() {
		return solicitacaoTransporte;
	}

	public void setSolicitacaoTransporte(SolicitacaoTransporte solicitacaoTransporte) {
		this.solicitacaoTransporte = solicitacaoTransporte;
	}


}
