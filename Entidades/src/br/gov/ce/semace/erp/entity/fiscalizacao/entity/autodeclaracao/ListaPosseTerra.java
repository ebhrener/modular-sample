package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listaPosseTerra", propOrder = {
    "posseTerra"
})
public class ListaPosseTerra {
	
	private List<PosseTerra> listPosseTerra;

	public List<PosseTerra> getListPosseTerra() {
		return listPosseTerra;
	}

	public void setListPosseTerra(List<PosseTerra> listPosseTerra) {
		this.listPosseTerra = listPosseTerra;
	}
	
	

}
