package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.entity.rh.Setor;
import br.gov.ce.semace.erp.enuns.StatusMonitoramento;

@Entity
@Audited
@Table(name = "monitoramento_oficio", schema = "protocolo")
public class MonitoramentoOficio implements Serializable {

	private static final long serialVersionUID = -1316367074538456170L;

	@Id
	@SequenceGenerator(sequenceName = "protocolo.seq_monitoramento_oficio", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rementente_oficio_id")
	private RemetenteOficio remetenteOficio = new RemetenteOficio();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setor_responsavel_id")
	private Setor setorResponsavel = new Setor();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcionario_responsavel_id")
	private Funcionario funcionarioResponsavel;

	@Column(name = "numero_oficio")
	private String numeroOficio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "municipio_id")
	private Cidade cidade = new Cidade();

	@Column(columnDefinition = "text")
	private String descricao;

	private Integer prazo;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_recebimento")
	private Date dataRecebimento;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_maxima")
	private Date dataMaxima;

	@Column(name = "numero_oficio_reiterado")
	private String numeroOficioReiterado;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_cadastro")
	private Date dataCadastro;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_atualizacao")
	private Date dataAtualizacao;

	@Column(name = "status_monitoramento")
	private StatusMonitoramento statusMonitoramento;

	@Column(name = "spu_processo")
	private String spuProcesso;

	@Column(name = "numero_doc")
	private String numeroDoc;

	@Transient
	private Integer prazoRestante;

	@Transient
	private Integer prazoAdicional;

	@Transient
	private List<Setor> setoresFilho;

	@Transient
	private Date inicioDataRecebimento;


	@Transient
	private Date fimDataRecebimento;


	/**
	 * Mover esta regra para camada de negócio e não no entidades
	 */
//	@Transient
//	public Integer getPrazoRestante() {
//		if (!this.statusMonitoramento.equals(StatusMonitoramento.ABERTO) || this.getPrazo() == null) {
//			return -1;
//		}
//
//		Date dataAtual = new Date();
//		prazoRestante = DateUtils.contaSomenteDias(dataAtual, dataMaxima);
//		return prazoRestante;
//	}
//
//	@Transient
//	public String getStatusFormatado() {
//		if (this.getStatusMonitoramento().equals(StatusMonitoramento.ABERTO) && (this.getPrazo() != null && this.getPrazoRestante() < 0)) {
//			return "Atrasado";
//		}
//
//		return this.getStatusMonitoramento().getDescricao();
//	}

	@Transient
	public Funcionario getFuncionarioResponsavelOficio() {
		if (this.getFuncionarioResponsavel() != null && this.getFuncionarioResponsavel().getId() != null) {
			return this.getFuncionarioResponsavel();
		}

		if (this.getSetorResponsavel() != null && this.getSetorResponsavel().getId() != null && this.getSetorResponsavel().getResponsavel() != null
				&& this.getSetorResponsavel().getResponsavel().getId() != null) {
			return this.getSetorResponsavel().getResponsavel();
		}

		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RemetenteOficio getRemetenteOficio() {
		return remetenteOficio;
	}

	public void setRemetenteOficio(RemetenteOficio remetenteOficio) {
		this.remetenteOficio = remetenteOficio;
	}

	public Setor getSetorResponsavel() {
		return setorResponsavel;
	}

	public void setSetorResponsavel(Setor setorResponsavel) {
		this.setorResponsavel = setorResponsavel;
	}

	public Funcionario getFuncionarioResponsavel() {
		return funcionarioResponsavel;
	}

	public void setFuncionarioResponsavel(Funcionario funcionarioResponsavel) {
		this.funcionarioResponsavel = funcionarioResponsavel;
	}

	public String getNumeroOficio() {
		return numeroOficio;
	}

	public void setNumeroOficio(String numeroOficio) {
		this.numeroOficio = numeroOficio;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getPrazo() {
		return prazo;
	}

	public void setPrazo(Integer prazo) {
		this.prazo = prazo;
	}

	public Date getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(Date dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public Date getDataMaxima() {
		return dataMaxima;
	}

	public void setDataMaxima(Date dataMaxima) {
		this.dataMaxima = dataMaxima;
	}

	public String getNumeroOficioReiterado() {
		return numeroOficioReiterado;
	}

	public void setNumeroOficioReiterado(String numeroOficioReiterado) {
		this.numeroOficioReiterado = numeroOficioReiterado;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public StatusMonitoramento getStatusMonitoramento() {
		return statusMonitoramento;
	}

	public void setStatusMonitoramento(StatusMonitoramento statusMonitoramento) {
		this.statusMonitoramento = statusMonitoramento;
	}

	public String getSpuProcesso() {
		return spuProcesso;
	}

	public void setSpuProcesso(String spuProcesso) {
		this.spuProcesso = spuProcesso;
	}

	public String getNumeroDoc() {
		return numeroDoc;
	}

	public void setNumeroDoc(String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}

	public Integer getPrazoAdicional() {
		return prazoAdicional;
	}

	public void setPrazoAdicional(Integer prazoAdicional) {
		this.prazoAdicional = prazoAdicional;
	}

	public void setPrazoRestante(Integer prazoRestante) {
		this.prazoRestante = prazoRestante;
	}

	public List<Setor> getSetoresFilho() {
		return setoresFilho;
	}

	public void setSetoresFilho(List<Setor> setoresFilho) {
		this.setoresFilho = setoresFilho;
	}

	public Date getInicioDataRecebimento() {
		return inicioDataRecebimento;
	}

	public void setInicioDataRecebimento(Date inicioDataRecebimento) {
		this.inicioDataRecebimento = inicioDataRecebimento;
	}

	public Date getFimDataRecebimento() {
		return fimDataRecebimento;
	}

	public void setFimDataRecebimento(Date fimDataRecebimento) {
		this.fimDataRecebimento = fimDataRecebimento;
	}
}