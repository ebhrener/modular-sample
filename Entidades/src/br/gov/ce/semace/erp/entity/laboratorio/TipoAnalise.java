package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="tipo_analise", schema="laboratorio")
public class TipoAnalise implements Serializable{

	private static final long serialVersionUID = -6096035621201353592L;
	
	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_tipo_analise", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	private String descricao;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name= "agrupamentos", schema = "laboratorio",
			joinColumns = @JoinColumn(name="tipo_analise_id"),
			inverseJoinColumns = @JoinColumn(name="ponto_monitoramento_id"))
	private List<PontoMonitoramento> pontos = new ArrayList<PontoMonitoramento>();

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public List<PontoMonitoramento> getPontos() {
		return pontos;
	}
	public void setPontos(List<PontoMonitoramento> pontos) {
		this.pontos = pontos;
	}
	
}
