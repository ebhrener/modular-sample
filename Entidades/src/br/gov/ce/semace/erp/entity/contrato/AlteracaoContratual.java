package br.gov.ce.semace.erp.entity.contrato;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.TipoAditivo;

@Audited
@Entity
@Table(name = "alteracao_contratual", schema="contrato")
public class AlteracaoContratual implements Serializable, Comparable<AlteracaoContratual>{
	
	private static final long serialVersionUID = 5719592159119696074L;
	
	
	@Id
	@SequenceGenerator(sequenceName = "contrato.seq_alteracao_contratual", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "alteracao_contratual_gestor_vigente",
			schema = "contrato", 
			joinColumns = @JoinColumn(name = "contrato_id"), 
			inverseJoinColumns = @JoinColumn(name = "gestor_id"))
	private List<Funcionario> listGestoresVigentes = new ArrayList<Funcionario>();
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "documento_id")
	private Documento documento = new Documento();
	
	private Date vigenciaAtual;
	
	private Date dataAssinatura;
	
	private Date dataPublicacao;
	
	private Date dataInicial;

	private Double valorAtual;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_alteracao_contratual_id")
	private TipoAlteracaoContratual tipoAlteracaoContratual = new TipoAlteracaoContratual();
	
	private TipoAditivo tipoAditivo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contrato_id")
	private Contrato contrato;
	
	@Column(columnDefinition="text")
	private String observacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Funcionario> getListGestoresVigentes() {
		return listGestoresVigentes;
	}

	public void setListGestoresVigentes(List<Funcionario> listGestoresVigentes) {
		this.listGestoresVigentes = listGestoresVigentes;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Date getVigenciaAtual() {
		return vigenciaAtual;
	}

	public void setVigenciaAtual(Date vigenciaAtual) {
		this.vigenciaAtual = vigenciaAtual;
	}

	public Date getDataAssinatura() {
		return dataAssinatura;
	}

	public void setDataAssinatura(Date dataAssinatura) {
		this.dataAssinatura = dataAssinatura;
	}

	public Date getDataPublicacao() {
		return dataPublicacao;
	}

	public void setDataPublicacao(Date dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}
	
	public Double getValorAtual() {
		return valorAtual;
	}

	public void setValorAtual(Double valorAtual) {
		this.valorAtual = valorAtual;
	}

	public TipoAlteracaoContratual getTipoAlteracaoContratual() {
		return tipoAlteracaoContratual;
	}

	public void setTipoAlteracaoContratual(
			TipoAlteracaoContratual tipoAlteracaoContratual) {
		this.tipoAlteracaoContratual = tipoAlteracaoContratual;
	}

	public TipoAditivo getTipoAditivo() {
		return tipoAditivo;
	}

	public void setTipoAditivo(TipoAditivo tipoAditivo) {
		this.tipoAditivo = tipoAditivo;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@Override
	public int compareTo(AlteracaoContratual o) {
		if(this.getTipoAditivo() != null){
			return o.getTipoAditivo().compareTo(this.getTipoAditivo());
		}
		
		return 0;
	}
	
	public String getNumeroDocumento(){
		String[] valores = this.getDocumento().getNumeroDocumento().split("-");
		return valores[0]+"º "+this.getDocumento().getTipoDocumento().getDescricao()+" do Contrato nº "+valores[1];
	}
}
