package br.gov.ce.semace.erp.entity.florestal.sinaflor;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;

/**
 * @author Erick Bhrener Barroso Silva		[erick.bhb@gmail.com]	05/06/2018 11:00
 */
@Entity
@Audited
@Table(schema="florestal", name="vinculo_projeto_sinaflor")
public class VinculoProjetoSINAFLOR implements Serializable{

	private static final long serialVersionUID = -1933309894149690477L;

	@Id
	@SequenceGenerator(sequenceName = "florestal.seq_vinculo_projeto_sinaflor", name = "seq_vinculo_projeto_sinaflor", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_vinculo_projeto_sinaflor")
	private Long id;

	@Column(name="spu")
	private String spu;

	@Column(name="codigo_sinaflor", nullable=false)
	private String codigoSINAFLOR;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="pessoa_id")
	private Funcionario funcionario;

	@Column(name="data")
	private Date data;

	public VinculoProjetoSINAFLOR() {
		super();
	}

	public String getSpu() {
		return spu;
	}
	public void setSpu(String spu) {
		this.spu = spu;
	}
	public String getCodigoSINAFLOR() {
		return codigoSINAFLOR;
	}
	public void setCodigoSINAFLOR(String codigoSINAFLOR) {
		this.codigoSINAFLOR = codigoSINAFLOR;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VinculoProjetoSINAFLOR other = (VinculoProjetoSINAFLOR) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "VinculoProjetoSINAFLOR [spu=" + spu + ", codigoSINAFLOR="
				+ codigoSINAFLOR + ", funcionario=" + funcionario + ", data="
				+ data + "]";
	}

}