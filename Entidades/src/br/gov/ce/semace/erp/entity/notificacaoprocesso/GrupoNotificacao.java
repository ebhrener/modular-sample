package br.gov.ce.semace.erp.entity.notificacaoprocesso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.GrupoNotificacaoEnum;


/**
 * Está entidade representa os grupos que serão enviados as mensagens (SMS)
 *
 * Inicialmente serão 2 grupos - Control e Treatment
 *
 *  Grupo Controle - não serão notificados
 *
 *  Grupo Treatment - serão notificados via SMS
 *
 * @author joerlan.lima
 *
 */
@Audited
@Entity
@Table(name="grupo_notificacao", schema="notificacao_processo")
public class GrupoNotificacao implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 3269555671550986491L;

	@Id
	@SequenceGenerator(sequenceName="notificacao_processo.seq_grupo_notificacao", name="notificacao_processo.seq_grupo_notificacao", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="notificacao_processo.seq_grupo_notificacao")
	private Long id;

	@OneToMany(mappedBy="grupoNotificacao", fetch=FetchType.LAZY)
	private List<GrupoNotificacaoEmail> listGrupoNotificacaoEmail = new ArrayList<>();

	@Column(name="grupo_notificacao")
	@Enumerated(EnumType.STRING)
	private GrupoNotificacaoEnum grupoNotificacaoEnum;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GrupoNotificacaoEnum getGrupoNotificacaoEnum() {
		return grupoNotificacaoEnum;
	}

	public void setGrupoNotificacaoEnum(GrupoNotificacaoEnum grupoNotificacaoEnum) {
		this.grupoNotificacaoEnum = grupoNotificacaoEnum;
	}

}
