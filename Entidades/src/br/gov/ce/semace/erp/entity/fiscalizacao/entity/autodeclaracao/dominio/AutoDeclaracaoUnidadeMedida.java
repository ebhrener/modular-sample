package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio;

public enum AutoDeclaracaoUnidadeMedida {
	
	PERCENTUAL(1,"%"),
	CABECA(2,"cab"),
	QUILO(4,"kg"),
	QUILO_COLMEIA(5,"kg/col"),
	QUILO_HECTARE(6,"kg/ha"),
	QUILO_VACA_ANO(7,"kg/vac/ano"),
	LITRO(8,"l"),
	LITRO_DIA(9,"l/d"),
	METRO_CUBICO(10,"m³"),
	MIL_HECTARE(11,"mil/ha"),
	MUDA(12,"mudas"),
	NUMERO(13,"nº"),
	RAQUETE(14,"raquete"),
	TONELADA(15,"t"),
	UNIDADE(16,"und"),
	FAMILIA(18,"fam"),
	PRODUTOR(19,"prod"),
	COLMEIA(20,"col"),
	MOLHO(21,"mol"),
	HECTARE(22,"ha"),
	MULHER(23,"mul"),
	INDIGENA(24,"ind"),
	GARFO(25,"garfo"),
	PROJETO(26,"proj"),
	REAL(27,"R$"),
	QUILO_CABRA_ANO(28,"kg/cabra/ano"),
	ARROBA(29,"arroba"),
	MIL(30,"mil"),
	MACO(31,"maço"),
	LITRO_CABECA_ANO(32,"l/cab/ano"),
	MACO_HECTARE(33,"maço/ha"),
	UNIDADE_HECTARE(17,"und/ha"),
	CENTO(35,"cento"),
	AGRICULTOR_FAMILIAR(34,"agf"),
	MILHEIRO(36,"Milheiro"),
	QUILO_M3_ANO(43,"kg/m³/ano"),
	VASO(37,"vaso"),
	VASO_HECTARE(38,"vaso/ha"),
	HASTE(39,"haste"),
	FRUTOS(41,"frutos"),
	FRUTOS_HECTARE(42,"frutos/ha"),
	HASTE_HECTARE(40,"haste/ha"),
	METRO(44,"m"),
	PECA(45,"Peças");
	
	private Integer codigo;
	private String descricao;
	
	private AutoDeclaracaoUnidadeMedida(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static String showDescricaoByCodigo(Integer codigo){
		for(AutoDeclaracaoUnidadeMedida auto: AutoDeclaracaoUnidadeMedida.values()){
			if(auto.getCodigo().equals(codigo)){
				return auto.descricao;
			}
		}
		return "Não Informado";	}
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
}
