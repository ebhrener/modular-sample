package br.gov.ce.semace.erp.entity.atendimento.pendenciaRequerimento;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.RequerimentoAtividadeCheckListItemCheckList;
import br.gov.ce.semace.erp.entity.geral.Pendencia;

@Entity
@Audited
@Table(schema="atendimento", name="pendencia_requerimento")
public class PendenciaRequerimento extends Pendencia {

	private static final long serialVersionUID = -7631987786983004916L;

	@JoinColumn(name = "requerimento_atividade_checklist_itemchecklist_id")
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	private RequerimentoAtividadeCheckListItemCheckList requerimentoAtividadeCheckListItemCheckList;

	public PendenciaRequerimento() {
		super();
	}

	public PendenciaRequerimento(RequerimentoAtividadeCheckListItemCheckList requerimentoAtividadeCheckListItemCheckList) {
		super();
		this.requerimentoAtividadeCheckListItemCheckList = requerimentoAtividadeCheckListItemCheckList;
	}

	public RequerimentoAtividadeCheckListItemCheckList getRequerimentoAtividadeCheckListItemCheckList() {
		return requerimentoAtividadeCheckListItemCheckList;
	}

	public void setRequerimentoAtividadeCheckListItemCheckList(RequerimentoAtividadeCheckListItemCheckList requerimentoAtividadeCheckListItemCheckList) {
		this.requerimentoAtividadeCheckListItemCheckList = requerimentoAtividadeCheckListItemCheckList;
	}

}
