package br.gov.ce.semace.erp.entity.notificacaoprocesso;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="recebimento_notificacoes_processo", schema="notificacao_processo")
public class RecebimentoNotificacoesProcesso implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 5274665527194813808L;

	@Id
	@SequenceGenerator(sequenceName="notificacao_processo.seq_recebimento_notificacoes_processo", name="notificacao_processo.seq_recebimento_notificacoes_processo", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="notificacao_processo.seq_recebimento_notificacoes_processo")
	private Long id;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(nullable=false, name="recebimento_notificacao_id")
	private RecebimentoNotificacoes recebimentoNotificacoes;

	@Column(name="processo_id")
	private String processoID;

	@Column(name="spu")
	private String spu;

	public String getProcessoID() {
		return processoID;
	}

	public void setProcessoID(String processoID) {
		this.processoID = processoID;
	}

	public RecebimentoNotificacoes getRecebimentoNotificacoes() {
		return recebimentoNotificacoes;
	}

	public void setRecebimentoNotificacoes(RecebimentoNotificacoes recebimentoNotificacoes) {
		this.recebimentoNotificacoes = recebimentoNotificacoes;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

}
