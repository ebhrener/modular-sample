package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.fiscalizacao.Resposta;
import br.gov.ce.semace.erp.enuns.TipoResposta;

@Entity
@Audited
@Table(name="opcao", schema = "geral")
public class Opcao implements Serializable, Comparable<Opcao> {

	private static final long serialVersionUID = -5056777702021648147L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_opcao", name = "SEQ_OPCAO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_OPCAO")
	private Long id;

	private String descricao;

	private boolean ativo = true;

	@Column(name = "tipo_resposta")
	private TipoResposta tipoResposta;

	private String resposta;

	/** Relacionamentos */

	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="questao_id")
	private Questao questao;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "opcao")
	private List<RespostaQuestionario> respostaQuestionarios = new ArrayList<RespostaQuestionario>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "opcao", cascade = CascadeType.ALL)
	private List<CabecalhoOpcao> listCabecalho = new ArrayList<CabecalhoOpcao>();

	@Column(name = "cabecalho_campo1")
	private String cabecalhoCampo1;

	@Column(name = "cabecalho_campo2")
	private String cabecalhoCampo2;

	@Column(name = "cabecalho_campo3")
	private String cabecalhoCampo3;

	@Column(name = "cabecalho_campo4")
	private String cabecalhoCampo4;

	@Transient
	private MapaResposta mapaRespostaTemp;

	@Transient
	private List<MapaResposta> listMapaRespostaTemp = new ArrayList<MapaResposta>();

	@Transient
	private Boolean selecionado = false;

	@Transient
	private String pathChecked;

	@Transient
	private String pathNonChecked;

	@Transient
	private String pathSelected;

	@Transient
	private String pathNonSelected;

	@Transient
	private List<Questao> questoesDependentes = new ArrayList<Questao>();

	@Transient
	private String descricaoOpcaoCabecalho;

	@Transient
	private List<RespostaQuestionario> respostaQuestionariosDependentes = new ArrayList<RespostaQuestionario>();

	@Override
	public Opcao clone() throws CloneNotSupportedException {
		Opcao opcao = new Opcao();
		opcao.setId(this.id);
		opcao.setAtivo(this.ativo);
		opcao.setDescricao(this.descricao);
		opcao.setQuestao(this.questao);
		opcao.setSelecionado(this.selecionado);
		opcao.setTipoResposta(this.tipoResposta);
		opcao.setRespostaQuestionarios(this.respostaQuestionarios);
		opcao.setResposta(this.resposta);
		opcao.setCabecalhoCampo1(this.cabecalhoCampo1);
		opcao.setCabecalhoCampo2(this.cabecalhoCampo2);
		opcao.setCabecalhoCampo3(this.cabecalhoCampo3);
		opcao.setCabecalhoCampo4(this.cabecalhoCampo4);
		opcao.setListMapaRespostaTemp(this.listMapaRespostaTemp);
		opcao.setMapaRespostaTemp(this.mapaRespostaTemp);
		opcao.setQuestoesDependentes(this.questoesDependentes);
		opcao.setQuestoesDependentes(this.questoesDependentes);
		return opcao;
	}

	public Opcao() {
		super();
	}

	public Opcao(Long id) {
		super();
		this.id = id;
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoResposta} é
	 * 	do tipo <b>TEXTO_CURTO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 10/06/2013 11:28:18
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoResposta} seja
	 * 	<b>TEXTO_CURTO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoResposta#isTextoCurto()
	 */
	@Transient
	public boolean isTextoCurto() {
		return tipoResposta.isTextoCurto();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoResposta} é
	 * 	do tipo <b>TEXTO_LONGO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 10/06/2013 11:30:13
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoResposta} seja
	 * 	<b>TEXTO_LONGO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoResposta#isTextoLongo()
	 */
	@Transient
	public boolean isTextoLongo() {
		return tipoResposta.isTextoLongo();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoResposta} é
	 * 	do tipo <b>MULTIPLA_ESCOLHA</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 10/06/2013 11:31:21
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoResposta} seja
	 * 	<b>MULTIPLA_ESCOLHA</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoResposta#isMultiplaEscolha()
	 */
	@Transient
	public boolean isMultiplaEscolha() {
		return tipoResposta.isMultiplaEscolha();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoResposta} é
	 * 	do tipo <b>CAIXA_SELECAO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 10/06/2013 11:34:17
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoResposta} seja
	 * 	<b>CAIXA_SELECAO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoResposta#isCaixaSelecao()
	 */
	@Transient
	public boolean isCaixaSelecao() {
		return tipoResposta.isCaixaSelecao();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoResposta} é
	 * 	do tipo <b>OUTROS</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 10/06/2013 11:34:17
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoResposta} seja
	 * 	<b>OUTROS</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoResposta#isOutros()
	 */
	@Transient
	public boolean isOutros() {
		return tipoResposta.isOutros();
	}

	@Transient
	public boolean isUploadImagem() {
		return tipoResposta.isUploadImagem();
	}

	@Transient
	public boolean isMapaResposta() {
		return tipoResposta.isMapaResposta();
	}

	@Transient
	public boolean isMapaCabecalhoResposta() {
		return tipoResposta.isMapaCabecalhoResposta();
	}

	/**
	 * Verifica se o tipo de {@link Opcao} necessita descrição ou não
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 04/11/2014 16:42:40
	 *
	 * @see TipoResposta#getListTipoRespostaExigeDescricaoOpcao()
	 *
	 * @return
	 * 		Caso o tipo de {@link Resposta} seja um dos listados no método {@link TipoResposta#getListTipoRespostaExigeDescricaoOpcao()},
	 * então retorna <code>false</code>, indicando que necessita de descrição para a opção. Caso contrário, <code>true</code>
	 */
	@Transient
	public boolean isDescricaoOpcaoVazia() {

		if (TipoResposta.getListTipoRespostaExigeDescricaoOpcao().contains(tipoResposta)) {
			return false;
		}

		return true;
	}


	/** Métodos getters e setters */

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoResposta getTipoResposta() {
		return tipoResposta;
	}

	public void setTipoResposta(TipoResposta tipoResposta) {
		this.tipoResposta = tipoResposta;
	}

	public Questao getQuestao() {
		return questao;
	}

	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	public Boolean getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(Boolean selecionado) {
		this.selecionado = selecionado;
	}

	public List<RespostaQuestionario> getRespostaQuestionarios() {
		return respostaQuestionarios;
	}

	public void setRespostaQuestionarios(
			List<RespostaQuestionario> respostaQuestionarios) {
		this.respostaQuestionarios = respostaQuestionarios;
	}

	@Override
	public int compareTo(Opcao opcao) {
		if (opcao != null) {
			return this.id.compareTo(opcao.getId());
		}

		return 0;
	}

	@Transient
	public boolean estaSelecionado() {
		switch (tipoResposta) {
			case MULTIPLA_ESCOLHA:
				return getId().equals(questao.getRespostaQuestionario().getOpcao().getId());
			case CAIXA_SELECAO:
				return getSelecionado();
			default:
				return false;
		}
	}

	public String getPathChecked() {
		return pathChecked;
	}

	public void setPathChecked(String pathChecked) {
		this.pathChecked = pathChecked;
	}

	public String getPathNonChecked() {
		return pathNonChecked;
	}

	public void setPathNonChecked(String pathNonChecked) {
		this.pathNonChecked = pathNonChecked;
	}

	public String getPathSelected() {
		return pathSelected;
	}

	public void setPathSelected(String pathSelected) {
		this.pathSelected = pathSelected;
	}

	public String getPathNonSelected() {
		return pathNonSelected;
	}

	public void setPathNonSelected(String pathNonSelected) {
		this.pathNonSelected = pathNonSelected;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getResposta() {
		return resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	public List<Questao> getQuestoesDependentes() {
		return questoesDependentes;
	}

	public void setQuestoesDependentes(List<Questao> questoesDependentes) {
		this.questoesDependentes = questoesDependentes;
	}

	public MapaResposta getMapaRespostaTemp() {
		return mapaRespostaTemp;
	}

	public void setMapaRespostaTemp(MapaResposta mapaRespostaTemp) {
		this.mapaRespostaTemp = mapaRespostaTemp;
	}

	public List<MapaResposta> getListMapaRespostaTemp() {
		return listMapaRespostaTemp;
	}

	public void setListMapaRespostaTemp(List<MapaResposta> listMapaRespostaTemp) {
		this.listMapaRespostaTemp = listMapaRespostaTemp;
	}

	public String getCabecalhoCampo1() {
		return cabecalhoCampo1;
	}

	public void setCabecalhoCampo1(String cabecalhoCampo1) {
		this.cabecalhoCampo1 = cabecalhoCampo1;
	}

	public String getCabecalhoCampo2() {
		return cabecalhoCampo2;
	}

	public void setCabecalhoCampo2(String cabecalhoCampo2) {
		this.cabecalhoCampo2 = cabecalhoCampo2;
	}

	public String getCabecalhoCampo3() {
		return cabecalhoCampo3;
	}

	public void setCabecalhoCampo3(String cabecalhoCampo3) {
		this.cabecalhoCampo3 = cabecalhoCampo3;
	}

	public String getCabecalhoCampo4() {
		return cabecalhoCampo4;
	}

	public void setCabecalhoCampo4(String cabecalhoCampo4) {
		this.cabecalhoCampo4 = cabecalhoCampo4;
	}

	public List<RespostaQuestionario> getRespostaQuestionariosDependentes() {
		return respostaQuestionariosDependentes;
	}

	public void setRespostaQuestionariosDependentes(List<RespostaQuestionario> respostaQuestionariosDependentes) {
		this.respostaQuestionariosDependentes = respostaQuestionariosDependentes;
	}

	public List<CabecalhoOpcao> getListCabecalho() {
		return listCabecalho;
	}

	public void setListCabecalho(List<CabecalhoOpcao> listCabecalho) {
		this.listCabecalho = listCabecalho;
	}

	public String getDescricaoOpcaoCabecalho() {
		return descricaoOpcaoCabecalho;
	}

	public void setDescricaoOpcaoCabecalho(String descricaoOpcaoCabecalho) {
		this.descricaoOpcaoCabecalho = descricaoOpcaoCabecalho;
	}

	public String getDescricaoListCabecalhoCompleto() {
		StringBuilder stb = new StringBuilder();

		if (this.listCabecalho != null && !this.listCabecalho.isEmpty()) {
			for (CabecalhoOpcao cabecalhoOpcao : this.listCabecalho) {
				stb.append(cabecalhoOpcao.getCabecalho());
				stb.append(" | ");
			}
		}

		return stb.toString();
	}
}