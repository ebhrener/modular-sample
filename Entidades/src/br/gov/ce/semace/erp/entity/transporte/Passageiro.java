package br.gov.ce.semace.erp.entity.transporte;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.planoviagem.Lancamento;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.StatusPassageiro;


@Entity
@Audited
@Table(schema = "transporte", name = "passageiro")
public class Passageiro implements Serializable {

	private static final long serialVersionUID = 2716295271424968630L;

	@Id
	@SequenceGenerator(sequenceName = "transporte.seq_passageiro", name = "SEQ_PASSAGEIRO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PASSAGEIRO")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario = new Funcionario();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="solicitacao_transporte_id")
	private SolicitacaoTransporte solicitacaoTransporte;

	@Column(name="status_passageiro")
	private StatusPassageiro statusPassageiro;

	@Column(name="justificativa", columnDefinition="text")
	private String justificativa;

	@Column(name="recebe_ajuda_custo")
	private boolean recebeAjudaDeCusto = false;

	@Column(name="nivel_descricao")
	private String nivelDescricao;

	@Column(name="data_inicial_real")
	private Date dataInicialReal;

	@Column(name="data_final_real")
	private Date dataFinalReal;

	@Transient
	private BigDecimal valorDiaria;

	@Transient
	private Float acrescimo;

	@Transient
	private BigDecimal valorTotalDiaria;

	@Transient
	private BigDecimal quantidadeDiaria;

	@Transient
	private BigDecimal valorTotalPlanoPeriodo;

	@Transient
	private BigDecimal qtdTotalDiariaPlano;

	@Transient
	private String destinoVisitado;

	@OneToMany(mappedBy = "passageiro",fetch = FetchType.LAZY)
	private List<Lancamento> lancamentos = new ArrayList<Lancamento>();

	public boolean isAtivo() {
		if (this.getStatusPassageiro() == null || this.getStatusPassageiro().isCompareceu()) {
			return true;
		}

		return false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public StatusPassageiro getStatusPassageiro() {
		return statusPassageiro;
	}

	public void setStatusPassageiro(StatusPassageiro statusPassageiro) {
		this.statusPassageiro = statusPassageiro;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public boolean isRecebeAjudaDeCusto() {
		return recebeAjudaDeCusto;
	}

	public void setRecebeAjudaDeCusto(boolean recebeAjudaDeCusto) {
		this.recebeAjudaDeCusto = recebeAjudaDeCusto;
	}

	public String getNivelDescricao() {
		return nivelDescricao;
	}

	public void setNivelDescricao(String nivelDescricao) {
		this.nivelDescricao = nivelDescricao;
	}

	public Date getDataInicialReal() {
		return dataInicialReal;
	}

	public void setDataInicialReal(Date dataInicialReal) {
		this.dataInicialReal = dataInicialReal;
	}

	public Date getDataFinalReal() {
		return dataFinalReal;
	}

	public void setDataFinalReal(Date dataFinalReal) {
		this.dataFinalReal = dataFinalReal;
	}

	public SolicitacaoTransporte getSolicitacaoTransporte() {
		return solicitacaoTransporte;
	}

	public void setSolicitacaoTransporte(SolicitacaoTransporte solicitacaoTransporte) {
		this.solicitacaoTransporte = solicitacaoTransporte;
	}

	public BigDecimal getValorDiaria() {
		return valorDiaria;
	}

	public void setValorDiaria(BigDecimal valorDiaria) {
		this.valorDiaria = valorDiaria;
	}

	public Float getAcrescimo() {
		return acrescimo;
	}

	public void setAcrescimo(Float acrescimo) {
		this.acrescimo = acrescimo;
	}

	public BigDecimal getValorTotalDiaria() {
		return valorTotalDiaria;
	}

	public void setValorTotalDiaria(BigDecimal valorTotalDiaria) {
		this.valorTotalDiaria = valorTotalDiaria;
	}

	public BigDecimal getQuantidadeDiaria() {
		return quantidadeDiaria;
	}

	public void setQuantidadeDiaria(BigDecimal quantidadeDiaria) {
		this.quantidadeDiaria = quantidadeDiaria;
	}

	public BigDecimal getValorTotalPlanoPeriodo() {
		return valorTotalPlanoPeriodo;
	}

	public void setValorTotalPlanoPeriodo(BigDecimal valorTotalPlanoPeriodo) {
		this.valorTotalPlanoPeriodo = valorTotalPlanoPeriodo;
	}

	public BigDecimal getQtdTotalDiariaPlano() {
		return qtdTotalDiariaPlano;
	}

	public void setQtdTotalDiariaPlano(BigDecimal qtdTotalDiariaPlano) {
		this.qtdTotalDiariaPlano = qtdTotalDiariaPlano;
	}

	public String getDestinoVisitado() {
		return destinoVisitado;
	}

	public void setDestinoVisitado(String destinoVisitado) {
		this.destinoVisitado = destinoVisitado;
	}

	public List<Lancamento> getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(List<Lancamento> lancamentos) {
		this.lancamentos = lancamentos;
	}
}