package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoHistoricoIGPDI;

@Entity
@Audited
@Table(name="historico_igpdi", schema="juridico")
public class HistoricoIGPDI implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7258426931135089836L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_historico_igpdi", name="juridico.seq_historico_igpdi", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_historico_igpdi")
	private Long id;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="igpdi_id")
	private IGPDI igpdi;
	
	@Column(name="data_cadastro", nullable=false)
	private Date dataCadastro;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(nullable=false)
	private TipoHistoricoIGPDI tipo;
	
	@Column(nullable=false, scale=5, precision=12)
	private BigDecimal valor; 

	//------------------------------------------------Gets e Setters---------------------------------------------

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public TipoHistoricoIGPDI getTipo() {
		return tipo;
	}

	public void setTipo(TipoHistoricoIGPDI tipo) {
		this.tipo = tipo;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public IGPDI getIgpdi() {
		return igpdi;
	}

	public void setIgpdi(IGPDI igpdi) {
		this.igpdi = igpdi;
	}
	
}
