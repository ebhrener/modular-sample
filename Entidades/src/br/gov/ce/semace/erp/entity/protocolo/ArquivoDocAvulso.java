package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;

@Entity
@Audited
@Table(name="arquivo_doc_avulso", schema="protocolo")
public class ArquivoDocAvulso implements Serializable{

	private static final long serialVersionUID = 4138066516400191388L;

	@Id
	@SequenceGenerator(sequenceName ="protocolo.seq_arquivo_doc_avulso", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(name = "nome_arquivo")
	private String nomeArquivo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "doc_avulso_id")
	private DocAvulso docAvulso = new DocAvulso();

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "documento_id")
	private Documento documento = new Documento();


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DocAvulso getDocAvulso() {
		return docAvulso;
	}

	public void setDocAvulso(DocAvulso docAvulso) {
		this.docAvulso = docAvulso;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	/**
	 * @return the documento
	 */
	public Documento getDocumento() {
		return documento;
	}

	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

}