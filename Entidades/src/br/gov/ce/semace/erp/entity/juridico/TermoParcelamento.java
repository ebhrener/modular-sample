package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.geral.Endereco;
import br.gov.ce.semace.erp.entity.geral.Pessoa;
import br.gov.ce.semace.erp.enuns.TipoTermoParcelamento;

@Entity
@Audited
@Table(schema="juridico", name="termo_parcelamento")
public class TermoParcelamento implements Serializable{

	private static final long serialVersionUID = -6189311262473874991L;
	
	@Id
	@SequenceGenerator(sequenceName="juridico.seq_termo_parcelamento", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	@ManyToOne(fetch= FetchType.LAZY, optional = false)
	@JoinColumn(name="pessoa_id")
	private Pessoa pessoa;
	
	@ManyToOne(fetch= FetchType.LAZY, optional = false)
	@JoinColumn(name="documento_id")
	private Documento documento;
	
	@Column(name = "tipo_termo_parcelamento")
	private TipoTermoParcelamento tipoTermoParcelamento;
	
	@Column(name = "valor_total")
	private BigDecimal valorTotal;
	
	@ManyToOne(fetch= FetchType.LAZY, optional = false)
	@JoinColumn(name="endereco_id")
	private Endereco endereco;
	
	
	@Column(name = "quantidade_parcelas")
	private Integer quantidadeParcelas;
	
	@Column(columnDefinition = "text", name = "texto_termo")
	private String textoTermo;
	
	private Boolean finalizado;
	
	/**
	 * Campo utilizado para referenciar o documento que deu origem ao termo.
	 * Ex. Número do Auto de Infração, Número do Processo, Número do Requerimento 
	 */
	@Column(name = "documento_referencia")
	private String documentoReferencia;
	
	@Column(name = "spu_processo")
	private String spuProcesso;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public TipoTermoParcelamento getTipoTermoParcelamento() {
		return tipoTermoParcelamento;
	}

	public void setTipoTermoParcelamento(TipoTermoParcelamento tipoTermoParcelamento) {
		this.tipoTermoParcelamento = tipoTermoParcelamento;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Integer getQuantidadeParcelas() {
		return quantidadeParcelas;
	}

	public void setQuantidadeParcelas(Integer quantidadeParcelas) {
		this.quantidadeParcelas = quantidadeParcelas;
	}

	public String getTextoTermo() {
		return textoTermo;
	}

	public void setTextoTermo(String textoTermo) {
		this.textoTermo = textoTermo;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public String getDocumentoReferencia() {
		return documentoReferencia;
	}

	public void setDocumentoReferencia(String documentoReferencia) {
		this.documentoReferencia = documentoReferencia;
	}

	public String getSpuProcesso() {
		return spuProcesso;
	}

	public void setSpuProcesso(String spuProcesso) {
		this.spuProcesso = spuProcesso;
	}

	
	
}
