package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.geral.Pessoa;

@Entity
@Audited
@Table(schema = "financeiro", name = "tcfa")
public class Tcfa implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 9052533868098800678L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_tcfa", name = "seq_tcfa", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tcfa")
	private Long id;

	@ManyToOne(optional=false)
	@JoinColumn(name="pessoa_id")
	private Pessoa pessoa;

	@OneToMany(mappedBy="tcfa", cascade=CascadeType.ALL)
	private List<PagamentoTCFA> pagamentos;

	private Integer ano;

	@OneToOne(fetch= FetchType.LAZY, optional = false)
	@JoinColumn(name="documento_id")
	private Documento documento;

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public List<PagamentoTCFA> getPagamentos() {
		return pagamentos;
	}

	public void setPagamentos(List<PagamentoTCFA> pagamentos) {
		this.pagamentos = pagamentos;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

}
