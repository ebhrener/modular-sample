package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;
import br.gov.ce.semace.erp.entity.geral.entity.cep.UF;
import br.gov.ce.semace.erp.entity.licenciamento.Cnae;
import br.gov.ce.semace.erp.entity.licenciamento.Modalidade;
import br.gov.ce.semace.erp.entity.licenciamento.Rama;
import br.gov.ce.semace.erp.entity.licenciamento.SubtipoProcesso;
import br.gov.ce.semace.erp.entity.licenciamento.TipoProcesso;
import br.gov.ce.semace.erp.enuns.EntregaDocumento;
import br.gov.ce.semace.erp.enuns.TipoPessoa;
import br.gov.ce.semace.erp.utils.FormatUtils;

@Entity
@Audited
@Table(name="requerimento_imutavel", schema="atendimento")
public class Requerimento implements Serializable {


	private static final long serialVersionUID = 5607246441697742318L;

	@Id @SequenceGenerator(sequenceName="atendimento.seq_requerimento_imutavel", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	/* Dados Interessado */

	@Column(name="nome_interessado")
	private String nomeInteressado;

	@Column(name="documento_interessado")
	private String documentoInteressado;

	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="endereco_requerimento_id")
	private EnderecoRequerimento enderecoRequerimento;

	@Column(precision=15, scale=2)
	private BigDecimal faturamento;

	@Column(name="telefone_interessado")
	private String telefoneInteressado;

	@Column(name="email_interessado")
	private String emailInteressado;

	/* Dados do Empreendimento */

	@Column(name="nome_empreendimento")
	private String nomeEmpreendimento;

	@Column(name="localizacao_empreedimento", columnDefinition = "text")
	private String localizacaoEmpreendimento;

	private String area;

	@Column(name="qtd_funcionario")
	private Integer qtdFuncionario;

	@Column(columnDefinition="text", name="referencia_empreendimento")
	private String referenciaEmpreendimento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cidade_empreendimento_id")
	private Cidade cidadeEmpreendimento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="uf_empreendimento_id")
	private UF ufEmpreendimento;

	/* Dados Requerimento */

	@Temporal(TemporalType.DATE)
	private Date data;

	private Time hora;

	// Campo que guarda as descrições das atividades concatenadas e separadas por ";"
	@Column(name="descricao_atividade")
	private String descricaoAtividade;

	// Campo que guarda os código das atividades concatenados e separados por ";"
	@Column(name="codigo_atividade")
	private String codigoAtividade;

	@Column(name="tipo_pessoa")
	private TipoPessoa tipoPessoa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tipo_processo_id")
	private TipoProcesso tipoProcesso;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="modalidade_processo_id")
	private Modalidade modalidade = new Modalidade();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="subtipo_processo_id")
	private SubtipoProcesso subtipoProcesso = new SubtipoProcesso();

	private String justificativa;

	@Column(name="numero_documento")
	private String numeroDocumento;

	@Column(columnDefinition="text")
	private String observacao;

	@Column(precision=15, scale=2)
	private BigDecimal hectare;

	@Column(columnDefinition="text")
	private String objetivo;

	@Column(name="numero_spu")
	private String spuAnterior;

	@Column(name="entrega_documento")
	private EntregaDocumento entregaDocumento;

	@Column(name="destinacao_materia_prima", columnDefinition="text")
	private String destinacaoMateriaPrima;

	@Column(name="spu_processo")
	private String spuProcesso;

	/* Dados Contato */

	@Column(name="nome_contato")
	private String nomeContato;

	@Column(name="endereco_contato")
	private String enderecoContato;

	@Column(name="telefone_contato")
	private String telefoneContato;

	@Column(name="documento_contato")
	private String documentoContato;

	/* Dados Consultoria */

	@Column(name="nome_consultor")
	private String nomeConsultor;

	@Column(name="telefone_consultor")
	private String telefoneConsultor;

	@Column(name="documento_consultor")
	private String documentoConsultor;

	@Column(name="email_consultor")
	private String emailConsultor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="agendamento_requerimento_id")
	private AgendamentoRequerimento agendamentoRequerimento;

	@Column(name="nome_sede")
	private String nomeSede;

	@Column(name="endereco_sede")
	private String enderecoSede;

	@Column(name = "nao_gera_processo")
	private Boolean naoGeraProcesso;

	@Column(name = "is_processo")
	private Boolean isProcesso;

	@Column(name="nome_produto_selo_verde")
	private String nomeProdutoSeloVerde;

	@Column(name="modelo_selo_verde")
	private String modeloSeloVerde;

	@Column(name="descricao_selo_verde")
	private String descricaoSeloVerde;

	@Column(name="materia_prima_selo_verde")
	private String materiaPrimaUtilizadaSeloVerde;

	@Column(name = "percentual_materia_prima_selo_verde")
	private Double percentualMateriaPrimaSeloVerde;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cnae_id")
	private Cnae cnae;

	@Column(name="quantidade_produtos_alteracao_lote")
	private Integer quantidadeProdutosAlteracaoLote;

	@Column(name="numero_car")
	private String numeroCar;

	@Column(name="preencheu_numero_car")
	private Boolean preencheuNumeroCar;

	@Column(name="numero_licenca")
	private String numeroLicenca;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="rama_id")
	private Rama rama;

	@Transient
	private String atividades;

	@Transient
	private String periodicidade;
	/** Métodos getters e setters */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeInteressado() {
		return nomeInteressado;
	}

	public void setNomeInteressado(String nomeInteressado) {
		this.nomeInteressado = nomeInteressado;
	}

	public String getDocumentoInteressado() {
		return documentoInteressado;
	}

	public void setDocumentoInteressado(String documentoInteressado) {
		this.documentoInteressado = documentoInteressado;
	}

	public EnderecoRequerimento getEnderecoRequerimento() {
		return enderecoRequerimento;
	}

	public void setEnderecoRequerimento(EnderecoRequerimento enderecoRequerimento) {
		this.enderecoRequerimento = enderecoRequerimento;
	}

	public BigDecimal getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(BigDecimal faturamento) {
		this.faturamento = faturamento;
	}

	public String getNomeEmpreendimento() {
		return nomeEmpreendimento;
	}

	public void setNomeEmpreendimento(String nomeEmpreendimento) {
		this.nomeEmpreendimento = nomeEmpreendimento;
	}

	public String getLocalizacaoEmpreendimento() {
		return localizacaoEmpreendimento;
	}

	public void setLocalizacaoEmpreendimento(String localizacaoEmpreendimento) {
		this.localizacaoEmpreendimento = localizacaoEmpreendimento;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Integer getQtdFuncionario() {
		return qtdFuncionario;
	}

	public void setQtdFuncionario(Integer qtdFuncionario) {
		this.qtdFuncionario = qtdFuncionario;
	}

	public String getReferenciaEmpreendimento() {
		return referenciaEmpreendimento;
	}

	public void setReferenciaEmpreendimento(String referenciaEmpreendimento) {
		this.referenciaEmpreendimento = referenciaEmpreendimento;
	}

	public Cidade getCidadeEmpreendimento() {
		return cidadeEmpreendimento;
	}

	public void setCidadeEmpreendimento(Cidade cidadeEmpreendimento) {
		this.cidadeEmpreendimento = cidadeEmpreendimento;
	}

	public UF getUfEmpreendimento() {
		return ufEmpreendimento;
	}

	public void setUfEmpreendimento(UF ufEmpreendimento) {
		this.ufEmpreendimento = ufEmpreendimento;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Time getHora() {
		return hora;
	}

	public void setHora(Time hora) {
		this.hora = hora;
	}

	public String getDescricaoAtividade() {
		return descricaoAtividade;
	}

	public void setDescricaoAtividade(String descricaoAtividade) {
		this.descricaoAtividade = descricaoAtividade;
	}

	public String getCodigoAtividade() {
		return codigoAtividade;
	}

	public void setCodigoAtividade(String codigoAtividade) {
		this.codigoAtividade = codigoAtividade;
	}

	public TipoPessoa getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(TipoPessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public TipoProcesso getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(TipoProcesso tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public BigDecimal getHectare() {
		return hectare;
	}

	public void setHectare(BigDecimal hectare) {
		this.hectare = hectare;
	}

	public String getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public String getSpuAnterior() {
		return spuAnterior;
	}

	public void setSpuAnterior(String spuAnterior) {
		this.spuAnterior = spuAnterior;
	}

	public EntregaDocumento getEntregaDocumento() {
		return entregaDocumento;
	}

	public void setEntregaDocumento(EntregaDocumento entregaDocumento) {
		this.entregaDocumento = entregaDocumento;
	}

	public String getDestinacaoMateriaPrima() {
		return destinacaoMateriaPrima;
	}

	public void setDestinacaoMateriaPrima(String destinacaoMateriaPrima) {
		this.destinacaoMateriaPrima = destinacaoMateriaPrima;
	}

	public String getSpuProcesso() {
		return spuProcesso;
	}

	public void setSpuProcesso(String spuProcesso) {
		this.spuProcesso = spuProcesso;
	}

	public String getNomeConsultor() {
		return nomeConsultor;
	}

	public void setNomeConsultor(String nomeConsultor) {
		this.nomeConsultor = nomeConsultor;
	}

	public String getTelefoneConsultor() {
		return telefoneConsultor;
	}

	public void setTelefoneConsultor(String telefoneConsultor) {
		this.telefoneConsultor = telefoneConsultor;
	}

	public String getDocumentoConsultor() {
		return documentoConsultor;
	}

	public void setDocumentoConsultor(String documentoConsultor) {
		this.documentoConsultor = documentoConsultor;
	}

	public String getEmailConsultor() {
		return emailConsultor;
	}

	public void setEmailConsultor(String emailConsultor) {
		this.emailConsultor = emailConsultor;
	}

	public AgendamentoRequerimento getAgendamentoRequerimento() {
		return agendamentoRequerimento;
	}

	public void setAgendamentoRequerimento(
			AgendamentoRequerimento agendamentoRequerimento) {
		this.agendamentoRequerimento = agendamentoRequerimento;
	}

	public String getNomeContato() {
		return nomeContato;
	}

	public void setNomeContato(String nomeContato) {
		this.nomeContato = nomeContato;
	}

	public String getEnderecoContato() {
		return enderecoContato;
	}

	public void setEnderecoContato(String enderecoContato) {
		this.enderecoContato = enderecoContato;
	}

	public String getTelefoneContato() {
		return telefoneContato;
	}

	public void setTelefoneContato(String telefoneContato) {
		this.telefoneContato = telefoneContato;
	}

	public String getDocumentoContato() {
		return documentoContato;
	}

	public void setDocumentoContato(String documentoContato) {
		this.documentoContato = documentoContato;
	}

	public String getTelefoneInteressado() {
		return telefoneInteressado;
	}

	public void setTelefoneInteressado(String telefoneInteressado) {
		this.telefoneInteressado = telefoneInteressado;
	}

	public String getNomeSede() {
		return nomeSede;
	}

	public void setNomeSede(String nomeSede) {
		this.nomeSede = nomeSede;
	}

	public String getEnderecoSede() {
		return enderecoSede;
	}

	public void setEnderecoSede(String enderecoSede) {
		this.enderecoSede = enderecoSede;
	}

	public Boolean getNaoGeraProcesso() {
		return naoGeraProcesso;
	}

	public void setNaoGeraProcesso(Boolean naoGeraProcesso) {
		this.naoGeraProcesso = naoGeraProcesso;
	}

	public Boolean getIsProcesso() {
		return isProcesso;
	}

	public void setIsProcesso(Boolean isProcesso) {
		this.isProcesso = isProcesso;
	}

	public SubtipoProcesso getSubtipoProcesso() {
		return subtipoProcesso;
	}

	public void setSubtipoProcesso(SubtipoProcesso subtipoProcesso) {
		this.subtipoProcesso = subtipoProcesso;
	}

	public String getDescricaoTipoModalidadeSubtipoProcesso() {
		return FormatUtils.getDescricaoTipoModalidadeSubtipoProcesso(this.tipoProcesso, this.subtipoProcesso, this.modalidade);
	}

	@Transient
	public String getDescricaoSiglaTipoModalidadeSubtipoProcesso() {
		return FormatUtils.getDescricaoSiglaTipoModalidadeSubtipoProcesso(this.tipoProcesso, this.subtipoProcesso, this.modalidade);
	}

	public Modalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}

	public String getEmailInteressado() {
		return emailInteressado;
	}

	public void setEmailInteressado(String emailInteressado) {
		this.emailInteressado = emailInteressado;
	}

	public String getNomeProdutoSeloVerde() {
		return nomeProdutoSeloVerde;
	}

	public void setNomeProdutoSeloVerde(String nomeProdutoSeloVerde) {
		this.nomeProdutoSeloVerde = nomeProdutoSeloVerde;
	}

	public String getModeloSeloVerde() {
		return modeloSeloVerde;
	}

	public void setModeloSeloVerde(String modeloSeloVerde) {
		this.modeloSeloVerde = modeloSeloVerde;
	}

	public String getDescricaoSeloVerde() {
		return descricaoSeloVerde;
	}

	public void setDescricaoSeloVerde(String descricaoSeloVerde) {
		this.descricaoSeloVerde = descricaoSeloVerde;
	}

	public String getMateriaPrimaUtilizadaSeloVerde() {
		return materiaPrimaUtilizadaSeloVerde;
	}

	public void setMateriaPrimaUtilizadaSeloVerde(
			String materiaPrimaUtilizadaSeloVerde) {
		this.materiaPrimaUtilizadaSeloVerde = materiaPrimaUtilizadaSeloVerde;
	}

	public Double getPercentualMateriaPrimaSeloVerde() {
		return percentualMateriaPrimaSeloVerde;
	}

	public void setPercentualMateriaPrimaSeloVerde(
			Double percentualMateriaPrimaSeloVerde) {
		this.percentualMateriaPrimaSeloVerde = percentualMateriaPrimaSeloVerde;
	}

	public Cnae getCnae() {
		return cnae;
	}

	public void setCnae(Cnae cnae) {
		this.cnae = cnae;
	}

	public Integer getQuantidadeProdutosAlteracaoLote() {
		return quantidadeProdutosAlteracaoLote;
	}

	public void setQuantidadeProdutosAlteracaoLote(Integer quantidadeProdutosAlteracaoLote) {
		this.quantidadeProdutosAlteracaoLote = quantidadeProdutosAlteracaoLote;
	}

	public String getAtividades() {
		return atividades;
	}

	public void setAtividades(String atividades) {
		this.atividades = atividades;
	}

	public String getNumeroCar() {
		return numeroCar;
	}

	public void setNumeroCar(String numeroCar) {
		this.numeroCar = numeroCar;
	}

	public Boolean getPreencheuNumeroCar() {
		return preencheuNumeroCar;
	}

	public void setPreencheuNumeroCar(Boolean preencheuNumeroCar) {
		this.preencheuNumeroCar = preencheuNumeroCar;
	}

	public String getNumeroLicenca() {
		return numeroLicenca;
	}

	public void setNumeroLicenca(String numeroLicenca) {
		this.numeroLicenca = numeroLicenca;
	}

	public Rama getRama() {
		return rama;
	}

	public void setRama(Rama rama) {
		this.rama = rama;
	}

	public String getPeriodicidade() {
		return periodicidade;
	}

	public void setPeriodicidade(String periodicidade) {
		this.periodicidade = periodicidade;
	}

}