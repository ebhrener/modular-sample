package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.ConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede;
import br.gov.ce.semace.erp.entity.atendimento.requerimento.HoraAgendamento;
import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;

@Entity
@Audited
@Table(name="sede",schema="geral")
public class Sede implements Serializable {

	private static final long serialVersionUID = -7208554136778962647L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_sede",name="geral.seq_sede",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="geral.seq_sede")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="endereco_id")
	private Endereco endereco = new Endereco();

	private String nome;

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_inicial_manha")
	private Date horaInicialManha;

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_final_manha")
	private Date horaFinalManha;

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_inicial_tarde")
	private Date horaInicialTarde;

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_final_tarde")
	private Date horaFinalTarde;

	@Column(name="qntd_atendimento_hora")
	private Long qntdAtendimentosHora;

	@Column(name = "realiza_atendimento")
	private Boolean realizaAtendimento = false;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sede")
	private List<HoraAgendamento> listHoraAgendamento = new ArrayList<HoraAgendamento>();

	@Column(name = "quantidade_atendimento_dia", columnDefinition = "INTEGER DEFAULT 0", nullable = false)
	private int quantidadeAtendimentoDia;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "sede_cidade",
	schema = "atendimento",
	joinColumns = @JoinColumn(name = "sede_id"),
	inverseJoinColumns = @JoinColumn(name = "cidade_id"))
	@NotAudited
	private List<Cidade> listCidade = new ArrayList<Cidade>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sede")
	private List<ConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede> listConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Date getHoraInicialManha() {
		return horaInicialManha;
	}

	public void setHoraInicialManha(Date horaInicialManha) {
		this.horaInicialManha = horaInicialManha;
	}

	public Date getHoraFinalManha() {
		return horaFinalManha;
	}

	public void setHoraFinalManha(Date horaFinalManha) {
		this.horaFinalManha = horaFinalManha;
	}

	public Date getHoraInicialTarde() {
		return horaInicialTarde;
	}

	public void setHoraInicialTarde(Date horaInicialTarde) {
		this.horaInicialTarde = horaInicialTarde;
	}

	public Date getHoraFinalTarde() {
		return horaFinalTarde;
	}

	public void setHoraFinalTarde(Date horaFinalTarde) {
		this.horaFinalTarde = horaFinalTarde;
	}

	public Long getQntdAtendimentosHora() {
		return qntdAtendimentosHora;
	}

	public void setQntdAtendimentosHora(Long qntdAtendimentosHora) {
		this.qntdAtendimentosHora = qntdAtendimentosHora;
	}

	public Boolean isRealizaAtendimento() {
		return realizaAtendimento;
	}

	public void setRealizaAtendimento(Boolean realizaAtendimento) {
		this.realizaAtendimento = realizaAtendimento;
	}

	public List<HoraAgendamento> getListHoraAgendamento() {
		return listHoraAgendamento;
	}

	public void setListHoraAgendamento(List<HoraAgendamento> listHoraAgendamento) {
		this.listHoraAgendamento = listHoraAgendamento;
	}

	public int getQuantidadeAtendimentoDia() {
		return quantidadeAtendimentoDia;
	}

	public void setQuantidadeAtendimentoDia(int quantidadeAtendimentoDia) {
		this.quantidadeAtendimentoDia = quantidadeAtendimentoDia;
	}

	public List<Cidade> getListCidade() {
		return listCidade;
	}

	public void setListCidade(List<Cidade> listCidade) {
		this.listCidade = listCidade;
	}

	public List<ConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede> getListConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede() {
		return listConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede;
	}

	public void setListConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede(List<ConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede> listConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede) {
		this.listConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede = listConfiguracaoTipoProcessoModalidadeSubTipoProcessoSede;
	}
}
