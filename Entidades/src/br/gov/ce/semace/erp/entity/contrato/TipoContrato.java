package br.gov.ce.semace.erp.entity.contrato;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="tipo_contrato", schema="contrato")
public class TipoContrato implements Serializable{

	private static final long serialVersionUID = -6081855625395202108L;

	@Id
	@SequenceGenerator(sequenceName="contrato.seq_tipo_contrato", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	private String descricao;
	
	@Column(name = "prazo_maximo")
	private Integer prazoMaximo;
	
	@Column(name="percentual_maximo_permitido")
	private Double percentualMaximoPermitido;
	

	/**
	 * Getters and Setters
	 */
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getPrazoMaximo() {
		return prazoMaximo;
	}

	public void setPrazoMaximo(Integer prazoMaximo) {
		this.prazoMaximo = prazoMaximo;
	}

	public Double getPercentualMaximoPermitido() {
		return percentualMaximoPermitido;
	}

	public void setPercentualMaximoPermitido(Double percentualMaximoPermitido) {
		this.percentualMaximoPermitido = percentualMaximoPermitido;
	}

}
