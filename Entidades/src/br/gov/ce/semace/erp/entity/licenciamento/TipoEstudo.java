package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "tipo_estudo", schema = "licenciamento")
public class TipoEstudo implements Serializable{

	private static final long serialVersionUID = -6018657667578119472L;

	@Id
	@SequenceGenerator(sequenceName = "licenciamento.seq_tipo_estudo", name = "SEQTIPOESTUDO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQTIPOESTUDO")
	private Long id;

	@Column(columnDefinition = "text")
	private String descricao;

	@Column(name = "quantidade_tecnico")
	private Integer quantidadeTecnico;

	@Column(name = "quantidade_horas_trabalhadas")
	private Integer quantidadesHorasTrabalhadas;

	@Column(name = "ativo")
	private boolean ativo;

	@Column(name = "depende_ppd_atividade")
	private boolean dependePPDAtividade;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tipoEstudo", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ConfiguracaoPPDGrupoAtividade> listConfiguracaoPPDGrupoAtividade = new ArrayList<ConfiguracaoPPDGrupoAtividade>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getQuantidadeTecnico() {
		return quantidadeTecnico;
	}

	public void setQuantidadeTecnico(Integer quantidadeTecnico) {
		this.quantidadeTecnico = quantidadeTecnico;
	}

	public Integer getQuantidadesHorasTrabalhadas() {
		return quantidadesHorasTrabalhadas;
	}

	public void setQuantidadesHorasTrabalhadas(Integer quantidadesHorasTrabalhadas) {
		this.quantidadesHorasTrabalhadas = quantidadesHorasTrabalhadas;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isDependePPDAtividade() {
		return dependePPDAtividade;
	}

	public void setDependePPDAtividade(boolean dependePPDAtividade) {
		this.dependePPDAtividade = dependePPDAtividade;
	}

	public List<ConfiguracaoPPDGrupoAtividade> getListConfiguracaoPPDGrupoAtividade() {
		return listConfiguracaoPPDGrupoAtividade;
	}

	public void setListConfiguracaoPPDGrupoAtividade(List<ConfiguracaoPPDGrupoAtividade> listConfiguracaoPPDGrupoAtividade) {
		this.listConfiguracaoPPDGrupoAtividade = listConfiguracaoPPDGrupoAtividade;
	}


}
