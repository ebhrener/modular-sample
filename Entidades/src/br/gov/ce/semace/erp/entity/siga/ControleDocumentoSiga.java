package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Transient;

import br.gov.ce.semace.erp.utils.StringUtils;

public class ControleDocumentoSiga implements Serializable {

	private static final long serialVersionUID = 1860318024855067651L;

	private String documentoId;

	private Integer ano;

	private Integer tipoDocumentoId;

	private String assunto;

	private Date dataDocumento;

	private String spuId;

	private Integer usuarioId;

	private Integer areaId;

	private Integer municipioId;

	private Integer numMudas;

	private Double area;

	private String origemPeca;

	private Integer tipoPecaId;

	private Date dataValidade;

	private Boolean embargo = Boolean.FALSE;

	private Long id;

	private Boolean excluido = Boolean.FALSE;

	private Integer tipoParecerInstrutorio;

	private BigInteger retificadoId;

	@Transient
	private Integer pessoaIdNatuur;

	@Transient
	private UsuarioSiga usuarioSiga;

	public ControleDocumentoSiga() {
		super();
	}

	public ControleDocumentoSiga(String documentoId, Integer ano, Integer tipoDocumentoId, String assunto, Date dataDocumento, String spuId, Integer usuarioId,
			Integer areaId, Integer municipioId, Integer numMudas, Double area, String origemPeca, Integer tipoPecaId, Date dataValidade, Boolean embargo,
			Long id, Boolean excluido, Integer tipoParecerInstrutorio, BigInteger retificadoId) {
		this.documentoId = documentoId;
		this.ano = ano;
		this.tipoDocumentoId = tipoDocumentoId;
		this.assunto = assunto;
		this.dataDocumento = dataDocumento;
		this.spuId = spuId;
		this.usuarioId = usuarioId;
		this.areaId = areaId;
		this.municipioId = municipioId;
		this.numMudas = numMudas;
		this.area = area;
		this.origemPeca = origemPeca;
		this.tipoPecaId = tipoPecaId;
		this.dataValidade = dataValidade;
		this.embargo = embargo;
		this.id = id;
		this.excluido = excluido;
		this.tipoParecerInstrutorio = tipoParecerInstrutorio;
		this.retificadoId = retificadoId;
	}

	public ControleDocumentoSiga(String documentoId, Integer ano, Integer tipoDocumentoId, String assunto, Date dataDocumento, String spuId, Integer usuarioId,
			Integer areaId, Integer municipioId, Integer numMudas, Double area, String origemPeca, Integer tipoPecaId, Date dataValidade, Boolean embargo,
			Long id, Boolean excluido, Integer tipoParecerInstrutorio, BigInteger retificadoId, Integer pessoaIdNatuur) {
		this.documentoId = documentoId;
		this.ano = ano;
		this.tipoDocumentoId = tipoDocumentoId;
		this.assunto = assunto;
		this.dataDocumento = dataDocumento;
		this.spuId = spuId;
		this.usuarioId = usuarioId;
		this.areaId = areaId;
		this.municipioId = municipioId;
		this.numMudas = numMudas;
		this.area = area;
		this.origemPeca = origemPeca;
		this.tipoPecaId = tipoPecaId;
		this.dataValidade = dataValidade;
		this.embargo = embargo;
		this.id = id;
		this.excluido = excluido;
		this.tipoParecerInstrutorio = tipoParecerInstrutorio;
		this.retificadoId = retificadoId;
		this.usuarioSiga = new UsuarioSiga(usuarioId.toString(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null, pessoaIdNatuur);
	}

	public String getNumeroDocumentoFormatado() {
		if (StringUtils.isNotBlank(this.getDocumentoId()) && this.getAno() != null) {
			return this.getDocumentoId().trim() + "/" + this.getAno();
		}

		return null;
	}

	public String getDocumentoId() {
		return documentoId;
	}

	public void setDocumentoId(String documentoId) {
		this.documentoId = documentoId;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getTipoDocumentoId() {
		return tipoDocumentoId;
	}

	public void setTipoDocumentoId(Integer tipoDocumentoId) {
		this.tipoDocumentoId = tipoDocumentoId;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public Date getDataDocumento() {
		return dataDocumento;
	}

	public void setDataDocumento(Date dataDocumento) {
		this.dataDocumento = dataDocumento;
	}

	public String getSpuId() {
		return spuId;
	}

	public void setSpuId(String spuId) {
		this.spuId = spuId;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public Integer getMunicipioId() {
		return municipioId;
	}

	public void setMunicipioId(Integer municipioId) {
		this.municipioId = municipioId;
	}

	public Integer getNumMudas() {
		return numMudas;
	}

	public void setNumMudas(Integer numMudas) {
		this.numMudas = numMudas;
	}

	public Double getArea() {
		return area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	public String getOrigemPeca() {
		return origemPeca;
	}

	public void setOrigemPeca(String origemPeca) {
		this.origemPeca = origemPeca;
	}

	public Integer getTipoPecaId() {
		return tipoPecaId;
	}

	public void setTipoPecaId(Integer tipoPecaId) {
		this.tipoPecaId = tipoPecaId;
	}

	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}

	public Boolean getEmbargo() {
		return embargo;
	}

	public void setEmbargo(Boolean embargo) {
		this.embargo = embargo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getExcluido() {
		return excluido;
	}

	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}

	public Integer getTipoParecerInstrutorio() {
		return tipoParecerInstrutorio;
	}

	public void setTipoParecerInstrutorio(Integer tipoParecerInstrutorio) {
		this.tipoParecerInstrutorio = tipoParecerInstrutorio;
	}

	public BigInteger getRetificadoId() {
		return retificadoId;
	}

	public void setRetificadoId(BigInteger retificadoId) {
		this.retificadoId = retificadoId;
	}

	public Integer getPessoaIdNatuur() {
		return pessoaIdNatuur;
	}

	public void setPessoaIdNatuur(Integer pessoaIdNatuur) {
		this.pessoaIdNatuur = pessoaIdNatuur;
	}

	public UsuarioSiga getUsuarioSiga() {
		return usuarioSiga;
	}

	public void setUsuarioSiga(UsuarioSiga usuarioSiga) {
		this.usuarioSiga = usuarioSiga;
	}
}