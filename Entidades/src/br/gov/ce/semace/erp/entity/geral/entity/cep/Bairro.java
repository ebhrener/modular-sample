package br.gov.ce.semace.erp.entity.geral.entity.cep;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Audited
@Table(name="bairros", schema="cep")
public class Bairro implements Serializable{

	private static final long serialVersionUID = 5072122737126238447L;

	@Id
	@SequenceGenerator(sequenceName="cep.seq_bairros", name="SEQBAIRROS", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQBAIRROS")
	@Column(name="cd_bairro")
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cd_cidade")
	@Index(name="bairro_cidade_i1")
	@JsonManagedReference
	private Cidade cidade = new Cidade();

	@Column(name="ds_bairro_nome")
	private String descricao;


	@OneToMany(mappedBy="bairro", fetch=FetchType.LAZY)
	private List<Logradouro> logradouros = new ArrayList<>();


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public List<Logradouro> getLogradouros() {
		return logradouros;
	}
	public void setLogradouros(List<Logradouro> logradouros) {
		this.logradouros = logradouros;
	}
}
