package br.gov.ce.semace.erp.entity.seguranca;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Pessoa;
import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;

@Entity
@Audited
@Table(name = "usuario_car", schema = "seguranca")
public class UsuarioCAR implements Serializable {

	private static final long serialVersionUID = -5073418706072305284L;

	@Id
	@SequenceGenerator(sequenceName = "seguranca.seq_usuario_car", name = "seq_usuario_car", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_usuario_car")
	private Long id;

	@Column(unique = true, nullable = false)
	private String login;

	@Column(nullable = false)
	private String senha;

	@OneToOne(cascade = { javax.persistence.CascadeType.DETACH })
	@JoinColumn(name = "pessoa_id", unique = true)
	private Pessoa pessoa;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "usuario_car_municipio", schema = "car", joinColumns = { @JoinColumn(name = "usuario_car_id") }, inverseJoinColumns = { @JoinColumn(name = "municipio_id") })
	private List<Cidade> listMunicipio;

	public String getListMunicipioToString() {
		if (this.listMunicipio != null && !this.listMunicipio.isEmpty()) {
			List<String> listNomeCidade = new ArrayList<>();

			for (Cidade cidade : listMunicipio) {
				listNomeCidade.add(cidade.getDescricao());
			}

			return String.join(" | ", listNomeCidade);
		}

		return "Não informado";
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public List<Cidade> getListMunicipio() {
		return this.listMunicipio;
	}

	public void setListMunicipio(List<Cidade> listMunicipio) {
		this.listMunicipio = listMunicipio;
	}
}