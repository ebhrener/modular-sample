package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.financeiro.Dae;
import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.enuns.StatusAcordoDivida;
import br.gov.ce.semace.erp.enuns.StatusDae;

@Entity
@Audited
@Table(name = "acordo_divida", schema = "juridico")
public class AcordoDivida implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -658977001145238912L;

	@Id
	@SequenceGenerator(sequenceName = "juridico.seq_acordo_divida", name = "juridico.seq_acordo_divida", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "juridico.seq_acordo_divida")
	private Long id;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "divida_ativa_id")
	private DividaAtiva dividaAtiva;

	@ManyToOne(optional = false)
	private Documento documento;

	// data em que o acordo foi firmado
	@Column(name = "data_acordo")
	private Date dataAcordo;

	@Column(name = "data_cadastro", nullable = false)
	private Date dataCadastro;

	@Column(name = "data_cancelamento")
	private Date dataCancelamento;

	// observação
	private String text;

	@Enumerated(EnumType.ORDINAL)
	@Column(nullable = false)
	private StatusAcordoDivida status;

	@Column(scale = 2, precision = 12)
	private BigDecimal entrada;

	@Column(name = "quantidade_parcelas")
	private int qtdParcelas;

	@Column(name = "data_vencimento")
	private Date dataVencimento;

	@Column(name = "motivo_cancelamento")
	private String motivoCancelamento;

	@Column(name = "valor_parcela", scale = 2, precision = 12)
	private BigDecimal valorParcela;

	@Column(name = "is_migrado", nullable = false)
	private Boolean isMigrado = Boolean.FALSE;

	@Transient
	private Dae daeEntrada;

	@OneToMany
	@JoinTable(schema = "juridico", name = "acordo_dae",
		joinColumns = @JoinColumn(name = "acordo_divida_id"),
		inverseJoinColumns = @JoinColumn(name = "dae_id"))
	private List<Dae> daesParcelas = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "acordoDivida")
	private List<ParcelaAcordoDivida> listParcelaAcordoDivida;

	// ------------------------------------------------Gets e
	// Setters---------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DividaAtiva getDividaAtiva() {
		return dividaAtiva;
	}

	public void setDividaAtiva(DividaAtiva dividaAtiva) {
		this.dividaAtiva = dividaAtiva;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Date getDataAcordo() {
		return dataAcordo;
	}

	public void setDataAcordo(Date dataAcordo) {
		this.dataAcordo = dataAcordo;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataCancelamento() {
		return dataCancelamento;
	}

	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public StatusAcordoDivida getStatus() {
		return status;
	}

	public void setStatus(StatusAcordoDivida status) {
		this.status = status;
	}

	public BigDecimal getEntrada() {
		return entrada;
	}

	public void setEntrada(BigDecimal entrada) {
		this.entrada = entrada;
	}

	public int getQtdParcelas() {
		return qtdParcelas;
	}

	public void setQtdParcelas(int qtdParcelas) {
		this.qtdParcelas = qtdParcelas;
	}

	public BigDecimal getValorParcela() {
		return valorParcela;
	}

	public void setValorParcela(BigDecimal valorParcela) {
		this.valorParcela = valorParcela;
	}

	public Dae getDaeEntrada() {
		return daeEntrada;
	}

	public void setDaeEntrada(Dae daeEntrada) {
		this.daeEntrada = daeEntrada;
	}

	public List<Dae> getDaesParcelas() {
		return daesParcelas;
	}

	public void setDaesParcelas(List<Dae> daesParcelas) {
		this.daesParcelas = daesParcelas;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getMotivoCancelamento() {
		return motivoCancelamento;
	}

	public void setMotivoCancelamento(String motivoCancelamento) {
		this.motivoCancelamento = motivoCancelamento;
	}

	public static String getStatusDaeColor(StatusDae statusDae) {
		switch (statusDae) {
		case EM_ABERTO:
			return "blue";
		case PAGO:
		case PAGO_VENCIDO:
			return "green";
		case VENCIDO:
			return "red";
		case ERRO_PROCESSAMENTO:
			return "red";
		case EXCLUIDO:
			return "red";
		case AGUARDANDO_EMISSAO:
			return "orange";
		default:
			return "black";
		}
	}

	public static String getStatusAcordoColor(
			StatusAcordoDivida statusAcordoDivida) {
		switch (statusAcordoDivida) {
		case ANDAMENTO:
			return "blue";
		case PRE_ACORDO:
			return "blue";
		case CONCLUIDO:
			return "green";
		case CANCELADO:
			return "red";
		case CANCELADO_PRE_ACORDO:
			return "red";
		case ACORDO_EM_ATRASO:
			return "orange";
		default:
			return "black";
		}
	}

	public Boolean getIsMigrado() {
		return isMigrado;
	}

	public void setIsMigrado(Boolean isMigrado) {
		this.isMigrado = isMigrado;
	}

	public List<ParcelaAcordoDivida> getListParcelaAcordoDivida() {
		return listParcelaAcordoDivida;
	}

	public void setListParcelaAcordoDivida(
			List<ParcelaAcordoDivida> listParcelaAcordoDivida) {
		this.listParcelaAcordoDivida = listParcelaAcordoDivida;
	}

	//Propriedade criada para controlar exibição ou não-exibição da parcela na tela de listagem dos acordos de parcelamento (listPagamentosDivida.xhtml)
	//Se a data do acordo for de 04/01/2018 em diante, o valor da parcela não deve ser exibido, pois o valor da parcela depende da selic do mês, segundo IN 03/2017.
	public boolean isAcordoFromIN_03_2017() {
		//Data de deploy das novas regras de parcelamento descritas na IN 03/2017.
		Calendar data_deploy_prod = Calendar.getInstance();
		data_deploy_prod.set(Calendar.YEAR, 2018);
		data_deploy_prod.set(Calendar.MONTH, 3);
		data_deploy_prod.set(Calendar.DAY_OF_MONTH, 17);
		data_deploy_prod.set(Calendar.HOUR_OF_DAY, 0);
		data_deploy_prod.set(Calendar.MINUTE, 0);
		data_deploy_prod.set(Calendar.SECOND, 0);
		data_deploy_prod.set(Calendar.MILLISECOND, 0);

		if (this.dataCadastro != null
				&& this.dataCadastro.compareTo(data_deploy_prod.getTime()) >= 0) {
			return true;
		}
		return false;
	}

}
