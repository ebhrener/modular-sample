package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "laboratorio", name = "servico_laboratorial")
public class ServicoLaboratorial implements Serializable {

	private static final long serialVersionUID = 7453499528405683514L;
	
	@Id
	@SequenceGenerator(sequenceName = "laboratorio.seq_servico_laboratorial", name = "SEQ_SERVICO_LABORATORIAL", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SERVICO_LABORATORIAL")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "analise_laboratorial_id")
	private AnaliseLaboratorial analiseLaboratorial;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "servicoLaboratorial")
	private List<ParametroLaboratorialServicoLaboratorial> parametrosServicos = new ArrayList<ParametroLaboratorialServicoLaboratorial>();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "solicitacao_servico_laboratorial_id")
	private SolicitacaoServicoLaboratorial solicitacaoServicoLaboratorial;
	
	private BigDecimal valor;
	
	@Override
	public ServicoLaboratorial clone() throws CloneNotSupportedException {
		ServicoLaboratorial servico = new ServicoLaboratorial();
		servico.setId(this.getId());
		servico.setAnaliseLaboratorial(this.getAnaliseLaboratorial());
		servico.setParametrosServicos(this.getParametrosServicos());
		servico.setSolicitacaoServicoLaboratorial(this.getSolicitacaoServicoLaboratorial());
		servico.setValor(this.getValor());
		return servico;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AnaliseLaboratorial getAnaliseLaboratorial() {
		return analiseLaboratorial;
	}

	public void setAnaliseLaboratorial(AnaliseLaboratorial analiseLaboratorial) {
		this.analiseLaboratorial = analiseLaboratorial;
	}

	public List<ParametroLaboratorialServicoLaboratorial> getParametrosServicos() {
		return parametrosServicos;
	}

	public void setParametrosServicos(
			List<ParametroLaboratorialServicoLaboratorial> parametrosServicos) {
		this.parametrosServicos = parametrosServicos;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public SolicitacaoServicoLaboratorial getSolicitacaoServicoLaboratorial() {
		return solicitacaoServicoLaboratorial;
	}

	public void setSolicitacaoServicoLaboratorial(
			SolicitacaoServicoLaboratorial solicitacaoServicoLaboratorial) {
		this.solicitacaoServicoLaboratorial = solicitacaoServicoLaboratorial;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		ServicoLaboratorial other = (ServicoLaboratorial) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
