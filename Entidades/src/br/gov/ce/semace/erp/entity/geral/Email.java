package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "email", schema="geral")
public class Email implements Serializable{


	private static final long serialVersionUID = -3683914160276890684L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_email", name = "SEQCONTRATO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQCONTRATO")
	private Long id;

	private String email;

	private String titulo;

	@Column(columnDefinition="text")
	private String conteudo;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "email")
	private List<EmailAttach> files;

	@Transient
	private boolean selecionadoEnvio = false;

	public Email() {
		super();
	}

	public Email(String email, String titulo, String conteudo) {
		super();
		this.email = email;
		this.titulo = titulo;
		this.conteudo = conteudo;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getConteudo() {
		return conteudo;
	}
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<EmailAttach> getFiles() {
		return files;
	}
	public void setFiles(List<EmailAttach> files) {
		this.files = files;
	}
	public boolean isSelecionadoEnvio() {
		return selecionadoEnvio;
	}
	public void setSelecionadoEnvio(boolean selecionadoEnvio) {
		this.selecionadoEnvio = selecionadoEnvio;
	}


}