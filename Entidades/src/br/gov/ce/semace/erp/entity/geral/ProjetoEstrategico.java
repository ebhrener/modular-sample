package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.AgendamentoRequerimento;

@Entity
@Audited
@Table(schema="geral", name="projeto_estrategico")
public class ProjetoEstrategico implements Serializable{

	private static final long serialVersionUID = 3727046018525206358L;
	
	@Id
	@SequenceGenerator(sequenceName = "geral.seq_processo_estrategico", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;
	
	private String descricao;
	
	private Boolean ativo = true;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "projetosEstrategicos")
	private List<AgendamentoRequerimento> agendamentoRequerimentos;
	
	@Override
	public int hashCode() {
		if(getId() != null){
			return getId().hashCode();
		}
		return 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof ProjetoEstrategico)) {
			return false;
		}
		if(((ProjetoEstrategico)obj).getId() == null){
			return ((ProjetoEstrategico)obj).getDescricao().equals(getDescricao());
		}
		return ((ProjetoEstrategico)obj).getId().equals(getId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public List<AgendamentoRequerimento> getAgendamentoRequerimentos() {
		return agendamentoRequerimentos;
	}

	public void setAgendamentoRequerimentos(
			List<AgendamentoRequerimento> agendamentoRequerimentos) {
		this.agendamentoRequerimentos = agendamentoRequerimentos;
	}
	
}
