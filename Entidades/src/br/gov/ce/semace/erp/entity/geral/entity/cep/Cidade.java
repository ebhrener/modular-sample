package br.gov.ce.semace.erp.entity.geral.entity.cep;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.gov.ce.semace.erp.entity.geral.RegiaoMetropolitana;
import br.gov.ce.semace.erp.entity.geral.Zona;


@Entity
@Audited
@Table(name="municipios", schema="cep")
public class Cidade implements Serializable{

	private static final long serialVersionUID = -7659026822803139844L;

	@Id
	@Column(name="cd_cidade")
	@SequenceGenerator(sequenceName="cep.seq_cidades", name="cep.seq_cidades", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="cep.seq_cidades")
	private Integer id;

	@Column(name="ds_cidade_nome")
	private String descricao;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="cd_uf")
	@Index(name="cidade_uf_i1")
	@JsonManagedReference
	private UF uf = new UF();

	@OneToMany(mappedBy="cidade", fetch=FetchType.LAZY)
	@JsonBackReference
	private List<Bairro> bairros = new ArrayList<>();

	@Column(name="codigo_ibge")
	private Long codigoIbge;

	@Column(name="loc_in_tipo_localidade")
	private String tipoLocalidade;

	/**
	 * para saber o id do pai no caso dos distritos
	 */
	@Column(name="loc_nu_sequencial_sub")
	private Integer idPai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "zona_id")
	@JsonManagedReference
	private Zona zona = new Zona();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "regiao_metropolitana_id")
	@JsonManagedReference
	private RegiaoMetropolitana regiaoMetropolitana = new RegiaoMetropolitana();

	private String color;

	/**
	 * TRANSIENT PARA SER USADO EM CONSULTA
	 */
	@Transient private List<String> listZona;
	@Transient private List<String> listUf;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public UF getUf() {
		return uf;
	}
	public void setUf(UF uf) {
		this.uf = uf;
	}
	public List<Bairro> getBairros() {
		return bairros;
	}
	public void setBairros(List<Bairro> bairros) {
		this.bairros = bairros;
	}
	public Long getCodigoIbge() {
		return codigoIbge;
	}

	public void setCodigoIbge(Long codigoIbge) {
		this.codigoIbge = codigoIbge;
	}

	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	public String getTipoLocalidade() {
		return tipoLocalidade;
	}
	public void setTipoLocalidade(String tipoLocalidade) {
		this.tipoLocalidade = tipoLocalidade;
	}

	@Override
	public int hashCode() {
		if(getId() != null){
			return getId().hashCode();
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Cidade)) {
			return false;
		}
		Cidade other = (Cidade) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getIdPai() {
		return idPai;
	}

	public void setIdPai(Integer idPai) {
		this.idPai = idPai;
	}

	public RegiaoMetropolitana getRegiaoMetropolitana() {
		return regiaoMetropolitana;
	}
	public void setRegiaoMetropolitana(RegiaoMetropolitana regiaoMetropolitana) {
		this.regiaoMetropolitana = regiaoMetropolitana;
	}
	@Override
	public String toString() {
		return "id: "+getId()+" descricao: "+getDescricao();
	}
	public List<String> getListZona() {
		return listZona;
	}
	public void setListZona(List<String> listZona) {
		this.listZona = listZona;
	}
	public List<String> getListUf() {
		return listUf;
	}
	public void setListUf(List<String> listUf) {
		this.listUf = listUf;
	}
}