package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Questao;
import br.gov.ce.semace.erp.enuns.TipoArquivo;

@Entity
@Audited
@Table(name = "item_pendencia", schema = "licenciamento")
public class ItemPendencia implements Serializable {

	private static final long serialVersionUID = 4830372736219519085L;

	@Id
	@SequenceGenerator(sequenceName = "licenciamento.seq_item_pendencia", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(name = "observacao",columnDefinition="text")
	private String observacao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "questao_id")
	private Questao questao;

	@Column(name = "tipo_arquivo")
	private TipoArquivo tipoArquivo;

	@Column(name = "excluido")
	private boolean excluido = false;

	@Column(name = "data_remocao")
	private Date dataRemocao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "analise_id")
	private AnaliseTecnicoRama analise;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Questao getQuestao() {
		return questao;
	}

	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	public TipoArquivo getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(TipoArquivo tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}

	public boolean isExcluido() {
		return excluido;
	}

	public void setExcluido(boolean excluido) {
		this.excluido = excluido;
	}

	public Date getDataRemocao() {
		return dataRemocao;
	}

	public void setDataRemocao(Date dataRemocao) {
		this.dataRemocao = dataRemocao;
	}

	public AnaliseTecnicoRama getAnalise() {
		return analise;
	}

	public void setAnalise(AnaliseTecnicoRama analise) {
		this.analise = analise;
	}

	public ItemPendencia clonar(){
		ItemPendencia item = new ItemPendencia();
		item.setId(this.id);
		item.setAnalise(this.analise);
		item.setDataRemocao(this.dataRemocao);
		item.setExcluido(this.excluido);
		item.setObservacao(this.observacao);
		item.setQuestao(this.questao);
		item.setTipoArquivo(this.tipoArquivo);
		return item;
	}

}
