package br.gov.ce.semace.erp.entity.geral.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.gov.ce.semace.erp.entity.geral.AtividadeClassificacaoGrandeza;
import br.gov.ce.semace.erp.entity.geral.IntervaloAtividadeGrandeza;

public class GrandezaClassificacaoTO implements Serializable{

	private static final long serialVersionUID = 8390002379421831554L;

	private List<AtividadeClassificacaoGrandeza> listaComboClassificacao = new ArrayList<AtividadeClassificacaoGrandeza>();

	private AtividadeClassificacaoGrandeza atividadeClassificacaoGrandezaSelecionada = new AtividadeClassificacaoGrandeza();

	private BigDecimal valorDigitado;

	private String intervaloSecionado;

	private String valorAlfaNumericoSelecionado;

	private boolean usaTabelaPadrao = false;

	private List<IntervaloAtividadeGrandeza> listIntervaloAtividadeGrandezas = new ArrayList<IntervaloAtividadeGrandeza>();

	public String getDescricaoGrandeza(){
		if(!listaComboClassificacao.isEmpty()){
			return listaComboClassificacao.get(0).getGrandeza().getDescricao();
		}

		return atividadeClassificacaoGrandezaSelecionada.getGrandeza() != null ? atividadeClassificacaoGrandezaSelecionada.getGrandeza().getDescricao() : "";
	}

	public String getPergunta(){
		if(!listaComboClassificacao.isEmpty()){
			return listaComboClassificacao.get(0).getGrandeza().getPergunta();
		}

		return atividadeClassificacaoGrandezaSelecionada.getGrandeza() != null ? atividadeClassificacaoGrandezaSelecionada.getGrandeza().getPergunta() : "";
	}


	public List<AtividadeClassificacaoGrandeza> getListaComboClassificacao() {
		return listaComboClassificacao;
	}

	public void setListaComboClassificacao(List<AtividadeClassificacaoGrandeza> listaComboClassificacao) {
		this.listaComboClassificacao = listaComboClassificacao;
	}

	public AtividadeClassificacaoGrandeza getAtividadeClassificacaoGrandezaSelecionada() {
		return atividadeClassificacaoGrandezaSelecionada;
	}

	public void setAtividadeClassificacaoGrandezaSelecionada(AtividadeClassificacaoGrandeza atividadeClassificacaoGrandezaSelecionada) {
		this.atividadeClassificacaoGrandezaSelecionada = atividadeClassificacaoGrandezaSelecionada;
	}

	public BigDecimal getValorDigitado() {
		return valorDigitado;
	}

	public void setValorDigitado(BigDecimal valorDigitado) {
		this.valorDigitado = valorDigitado;
	}

	public String getValorAlfaNumericoSelecionado() {
		return valorAlfaNumericoSelecionado;
	}

	public void setValorAlfaNumericoSelecionado(String valorAlfaNumericoSelecionado) {
		this.valorAlfaNumericoSelecionado = valorAlfaNumericoSelecionado;
	}

	public boolean isUsaTabelaPadrao() {
		return usaTabelaPadrao;
	}

	public void setUsaTabelaPadrao(boolean usaTabelaPadrao) {
		this.usaTabelaPadrao = usaTabelaPadrao;
	}

	public String getIntervaloSecionado() {
		return intervaloSecionado;
	}

	public void setIntervaloSecionado(String intervaloSecionado) {
		this.intervaloSecionado = intervaloSecionado;
	}

	public List<IntervaloAtividadeGrandeza> getListIntervaloAtividadeGrandezas() {
		return listIntervaloAtividadeGrandezas;
	}

	public void setListIntervaloAtividadeGrandezas(List<IntervaloAtividadeGrandeza> listIntervaloAtividadeGrandezas) {
		this.listIntervaloAtividadeGrandezas = listIntervaloAtividadeGrandezas;
	}

}
