package br.gov.ce.semace.erp.entity.atendimento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.gov.ce.semace.erp.entity.financeiro.TipoTaxa;
import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.geral.Sede;
import br.gov.ce.semace.erp.entity.licenciamento.Grandeza;
import br.gov.ce.semace.erp.entity.licenciamento.Modalidade;
import br.gov.ce.semace.erp.entity.licenciamento.TipoProcesso;
import br.gov.ce.semace.erp.entity.licenciamento.TipoRequerimento;

@Entity
@Table(name = "configuracao_atendimento_presencial", schema = "atendimento")
public class ConfiguracaoAtendimentoPresencial implements Serializable {

	private static final long serialVersionUID = -6312224950139296959L;

	@Id
	@SequenceGenerator(sequenceName = "atendimento.seq_configuracao_atendimento", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_processo_analise_id")
	private TipoProcesso tipoProcessoAnalise;

	@Column(name = "quantidade_produto_alteracao_lote")
	private Long quantidadeProdutoAlteracaoLote;

	@Column(name = "exige_car")
	private Boolean exigeCar;

	@OneToOne
	@JoinColumn(name = "modalidade_alteracao_lote")
	private Modalidade modalidadeAlteracaoLote;

	@OneToOne
	@JoinColumn(name = "tipo_processo_selo_verde_id")
	private TipoProcesso tipoProcessoSeloVerde;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "configuracao_atendimento_tipo_requerimento", schema = "atendimento", joinColumns = @JoinColumn(name = "configuracao_id"), inverseJoinColumns = @JoinColumn(name = "requerimento_id"))
	private List<TipoRequerimento> listTipoRequerimentosExigemCar = new ArrayList<TipoRequerimento>();

	@ManyToOne
	@JoinColumn(name = "tipo_processo_reanalise_compensacao_ambiental_id")
	private TipoProcesso tipoProcessoReanaliseCompensacaoAmbiental;

	@ManyToOne
	@JoinColumn(name = "sede_atendimento_analise_laboratorial_id")
	private Sede sedeAtendimentoAnaliseLaboratorial;

	@Column(name = "prazo_pagamento_dae")
	private Integer prazoPagamentoDae;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "configuracaoAtendimentoPresencial", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ConfigReqGeracaoTaxaFixaAutomatico> listConfigReqGeracaoTaxaFixaAutomatico = new ArrayList<ConfigReqGeracaoTaxaFixaAutomatico>();

	@OneToOne
	@JoinColumn(name = "grandeza_padrao_id")
	private Grandeza grandezaPadrao;

	@Column(name = "id_tipo_anexo_planta")
	private Long idTipoAnexoPlanta;

	@Column(name = "id_tipo_anexo_art")
	private Long idTipoAnexoART;

	@Column(name = "id_tipo_anexo_shape")
	private Long idTipoAnexoShape;

	@ManyToMany
	@JoinTable(name = "configuracao_atividade_taxa_fixa", schema = "atendimento", joinColumns = @JoinColumn(name = "configuracao_id"), inverseJoinColumns = @JoinColumn(name = "atividade_id"))
	private List<Atividade> listAtividadeTaxaFixa = new ArrayList<Atividade>();

	@OneToOne
	@JoinColumn(name = "modalidade_cancelamento_lote_id")
	private Modalidade modalidadeCancelamentoLote;

	@Column(name = "atender_requerimento_renovacao_licenca_vencida")
	private boolean isAtenderRequerimentoRenovacaoLicencaVencida = false;

	@Column(name = "escolha_sede_automatica")
	private boolean escolhaSedeAutomatica = false;

	@Transient
	private List<TipoTaxa> listTipoTaxa = new ArrayList<TipoTaxa>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoProcesso getTipoProcessoAnalise() {
		return tipoProcessoAnalise;
	}

	public void setTipoProcessoAnalise(TipoProcesso tipoProcessoAnalise) {
		this.tipoProcessoAnalise = tipoProcessoAnalise;
	}

	public Long getQuantidadeProdutoAlteracaoLote() {
		return quantidadeProdutoAlteracaoLote;
	}

	public void setQuantidadeProdutoAlteracaoLote(
			Long quantidadeProdutoAlteracaoLote) {
		this.quantidadeProdutoAlteracaoLote = quantidadeProdutoAlteracaoLote;
	}

	public Modalidade getModalidadeAlteracaoLote() {
		return modalidadeAlteracaoLote;
	}

	public void setModalidadeAlteracaoLote(Modalidade modalidadeAlteracaoLote) {
		this.modalidadeAlteracaoLote = modalidadeAlteracaoLote;
	}

	public TipoProcesso getTipoProcessoSeloVerde() {
		return tipoProcessoSeloVerde;
	}

	public void setTipoProcessoSeloVerde(TipoProcesso tipoProcessoSeloVerde) {
		this.tipoProcessoSeloVerde = tipoProcessoSeloVerde;
	}

	public List<TipoRequerimento> getListTipoRequerimentosExigemCar() {
		return listTipoRequerimentosExigemCar;
	}

	public void setListTipoRequerimentosExigemCar(List<TipoRequerimento> listTipoRequerimentosExigemCar) {
		this.listTipoRequerimentosExigemCar = listTipoRequerimentosExigemCar;
	}

	public List<TipoTaxa> getListTipoTaxa() {
		return listTipoTaxa;
	}

	public void setListTipoTaxa(List<TipoTaxa> listTipoTaxa) {
		this.listTipoTaxa = listTipoTaxa;
	}

	public Boolean getExigeCar() {
		return exigeCar;
	}

	public void setExigeCar(Boolean exigeCar) {
		this.exigeCar = exigeCar;
	}

	public TipoProcesso getTipoProcessoReanaliseCompensacaoAmbiental() {
		return tipoProcessoReanaliseCompensacaoAmbiental;
	}

	public void setTipoProcessoReanaliseCompensacaoAmbiental(
			TipoProcesso tipoProcessoReanaliseCompensacaoAmbiental) {
		this.tipoProcessoReanaliseCompensacaoAmbiental = tipoProcessoReanaliseCompensacaoAmbiental;
	}

	public Sede getSedeAtendimentoAnaliseLaboratorial() {
		return sedeAtendimentoAnaliseLaboratorial;
	}

	public void setSedeAtendimentoAnaliseLaboratorial(
			Sede sedeAtendimentoAnaliseLaboratorial) {
		this.sedeAtendimentoAnaliseLaboratorial = sedeAtendimentoAnaliseLaboratorial;
	}

	public Integer getPrazoPagamentoDae() {
		return prazoPagamentoDae;
	}

	public void setPrazoPagamentoDae(Integer prazoPagamentoDae) {
		this.prazoPagamentoDae = prazoPagamentoDae;
	}

	public List<ConfigReqGeracaoTaxaFixaAutomatico> getListConfigReqGeracaoTaxaFixaAutomatico() {
		return listConfigReqGeracaoTaxaFixaAutomatico;
	}

	public void setListConfigReqGeracaoTaxaFixaAutomatico(
			List<ConfigReqGeracaoTaxaFixaAutomatico> listConfigReqGeracaoTaxaFixaAutomatico) {
		this.listConfigReqGeracaoTaxaFixaAutomatico = listConfigReqGeracaoTaxaFixaAutomatico;
	}

	public Grandeza getGrandezaPadrao() {
		return grandezaPadrao;
	}

	public void setGrandezaPadrao(Grandeza grandezaPadrao) {
		this.grandezaPadrao = grandezaPadrao;
	}

	public Long getIdTipoAnexoPlanta() {
		return idTipoAnexoPlanta;
	}

	public void setIdTipoAnexoPlanta(Long idTipoAnexoPlanta) {
		this.idTipoAnexoPlanta = idTipoAnexoPlanta;
	}

	public Long getIdTipoAnexoART() {
		return idTipoAnexoART;
	}

	public void setIdTipoAnexoART(Long idTipoAnexoART) {
		this.idTipoAnexoART = idTipoAnexoART;
	}

	public Long getIdTipoAnexoShape() {
		return idTipoAnexoShape;
	}

	public void setIdTipoAnexoShape(Long idTipoAnexoShape) {
		this.idTipoAnexoShape = idTipoAnexoShape;
	}

	public List<Atividade> getListAtividadeTaxaFixa() {
		return listAtividadeTaxaFixa;
	}

	public void setListAtividadeTaxaFixa(List<Atividade> listAtividadeTaxaFixa) {
		this.listAtividadeTaxaFixa = listAtividadeTaxaFixa;
	}

	public Modalidade getModalidadeCancelamentoLote() {
		return modalidadeCancelamentoLote;
	}

	public void setModalidadeCancelamentoLote(Modalidade modalidadeCancelamentoLote) {
		this.modalidadeCancelamentoLote = modalidadeCancelamentoLote;
	}

	public boolean isAtenderRequerimentoRenovacaoLicencaVencida() {
		return isAtenderRequerimentoRenovacaoLicencaVencida;
	}

	public void setAtenderRequerimentoRenovacaoLicencaVencida(boolean isAtenderRequerimentoRenovacaoLicencaVencida) {
		this.isAtenderRequerimentoRenovacaoLicencaVencida = isAtenderRequerimentoRenovacaoLicencaVencida;
	}

	public boolean isEscolhaSedeAutomatica() {
		return escolhaSedeAutomatica;
	}

	public void setEscolhaSedeAutomatica(boolean escolhaSedeAutomatica) {
		this.escolhaSedeAutomatica = escolhaSedeAutomatica;
	}

}
