package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.seguranca.Usuario;
import br.gov.ce.semace.erp.enuns.StatusDivida;

@Entity
@Audited
@Table(name="historico_status_divida", schema="juridico")
public class HistoricoStatusDivida implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -1033905292540779116L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_historico_status_divida", name="juridico.seq_historico_status_divida", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_historico_status_divida")
	private Long id;

	@Column(name="data_cadastro", nullable=false)
	private Date dataCadastro;
	
	@ManyToOne(optional=false,fetch=FetchType.EAGER)
	@JoinColumn(name="divida_ativa_id")
	private DividaAtiva dividaAtiva;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(nullable=false)
	private StatusDivida status;
	
	@Column(nullable=false)
	private String justificativa;
	
	@ManyToOne(fetch=FetchType.LAZY,optional=true)
	@JoinColumn(name="usuario_id")
	private Usuario usuario;
	
	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public StatusDivida getStatus() {
		return status;
	}

	public void setStatus(StatusDivida status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DividaAtiva getDividaAtiva() {
		return dividaAtiva;
	}

	public void setDividaAtiva(DividaAtiva dividaAtiva) {
		this.dividaAtiva = dividaAtiva;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	

}
