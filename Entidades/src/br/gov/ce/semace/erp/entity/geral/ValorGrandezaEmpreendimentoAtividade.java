package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.utils.StringUtils;

@Audited
@Entity
@Table(name = "valor_grandeza_empreendimento_atividade", schema = "geral")
public class ValorGrandezaEmpreendimentoAtividade implements Serializable {

	private static final long serialVersionUID = 7326758470778239099L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_valor_grandeza_Emp_Atv", name = "geral.seq_valor_grandeza_Emp_Atv", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geral.seq_valor_grandeza_Emp_Atv")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empreendimento_atividade_id")
	private EmpreendimentoAtividade empreendimentoAtividade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_classificacao_grandeza_id")
	private AtividadeClassificacaoGrandeza atividadeClassificacaoGrandeza;

	@Column(name = "valor_numerico")
	private BigDecimal valorNumerico;

	@Column(name = "valor_alfa_numerico")
	private String valorAlfaNumerico;

	@Transient
	private boolean usado = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EmpreendimentoAtividade getEmpreendimentoAtividade() {
		return empreendimentoAtividade;
	}

	public void setEmpreendimentoAtividade(EmpreendimentoAtividade empreendimentoAtividade) {
		this.empreendimentoAtividade = empreendimentoAtividade;
	}

	public AtividadeClassificacaoGrandeza getAtividadeClassificacaoGrandeza() {
		return atividadeClassificacaoGrandeza;
	}

	public void setAtividadeClassificacaoGrandeza(AtividadeClassificacaoGrandeza atividadeClassificacaoGrandeza) {
		this.atividadeClassificacaoGrandeza = atividadeClassificacaoGrandeza;
	}

	public BigDecimal getValorNumerico() {
		return valorNumerico;
	}

	public void setValorNumerico(BigDecimal valorNumerico) {
		this.valorNumerico = valorNumerico;
	}

	public String getValorAlfaNumerico() {
		return valorAlfaNumerico;
	}

	public void setValorAlfaNumerico(String valorAlfaNumerico) {
		this.valorAlfaNumerico = valorAlfaNumerico;
	}

	public boolean isUsado() {
		return usado;
	}

	public void setUsado(boolean usado) {
		this.usado = usado;
	}

	public boolean isFieldNumeric() {
		return valorNumerico != null;
	}

	public boolean isFieldAlphaNumeric() {
		return StringUtils.isNotBlank(valorAlfaNumerico);
	}

}
