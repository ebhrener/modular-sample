package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="rh", name="funcao")
public class Funcao implements Serializable{

	private static final long serialVersionUID = 5041378121327091332L;

	@Id
	@SequenceGenerator(sequenceName="rh.seq_funcao", name="SEQFUNCAO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQFUNCAO")
	private Long id;

	@NotNull(message="Campo Descrição é Obrigatorio")
	@Column(nullable=false)
	private String descricao;

	private Boolean comissionado = false;

	//------------------------------------------------Relacionamentos---------------------------------------------

	@OneToMany(mappedBy = "funcao", fetch = FetchType.LAZY)
	private List<Funcionario> funcionarios = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tipo_comissao_id")
	private TipoComissao tipoComissao = new TipoComissao();

	//------------------------------------------------Gets e Setters---------------------------------------------


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Informa a descrição da propriedade comissionado, no caso, Sim ou Não, representando comissionado ou não, respectivamente.
	 *
	 * @author Elizandra Amarante [elizandra.ferreira@gmail.com] - 11/07/2014 10:38:02
	 *
	 * @return Retorna uma {@link String} com o valor <code>Sim</code>,
	 * 	caso o valor da propriedade ativo seja diferente de <code>null</code> e <code>true</code>. Caso Contrário, retorna <code>false</code>
	 */
	@Transient
	public String getDetalheComissionado(){

		if (this.comissionado != null && this.comissionado){
			return "Sim";
		}

		return "Não";
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}


	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}


	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}


	public Boolean getComissionado() {
		return comissionado;
	}


	public void setComissionado(Boolean comissionado) {
		this.comissionado = comissionado;
	}


	public TipoComissao getTipoComissao() {
		return tipoComissao;
	}


	public void setTipoComissao(TipoComissao tipoComissao) {
		this.tipoComissao = tipoComissao;
	}

	/**
	 * Método transiente ({@link Transient}) que verifica se a propriedade que
	 * representa o objeto {@link TipoComissao} está <code>null</code> ou
	 * <i>vazia</i>
	 *
	 * @return Retorna <code>true</code> se a propriedade estiver vazia ou
	 *         <code>null</code>. Caso contrário, retorna <code>false</code>.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Feb 20, 2013
	 *         3:07:52 PM
	 */
	@Transient
	public boolean isTipoComissaoNullOrEmpty() {
		if (this.getTipoComissao() == null	|| this.getTipoComissao().getId() == null) {
			return true;
		}

		return false;
	}
}
