package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.AgendamentoRequerimento;
import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.laboratorio.SolicitacaoServicoLaboratorial;
import br.gov.ce.semace.erp.enuns.Letra;

@Entity
@Audited
@Table(schema = "financeiro", name = "parametro_dae")
public class ParametroDae implements Serializable {

	private static final long serialVersionUID = -6949001836025837415L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_parametro_dae", name = "SEQ_PARAMETRO_DAE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PARAMETRO_DAE")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_taxa_id")
	private TipoTaxa tipoTaxa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_id")
	private Atividade atividade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agendamento_requerimento_id")
	private AgendamentoRequerimento agendamentoRequerimento;

	@Column(name = "distancia")
	private BigDecimal distancia;

	@Column(name = "horas_trabalhadas")
	private Integer horasTrabalhadas;

	@Column(name = "quantidade_tecnicos")
	private Integer quantidadeTecnicos;

	@Column(name = "quantidade_veiculos")
	private Integer quantidadeVeiculos;

	@Column(name = "excluido")
	private boolean excluido;

	private Letra letra;

	@Column(name = "observacao")
	private String observacao;

	@Column(name = "data_cadastro")
	private Date dataCadastro;

	@Column(name = "valor_dae")
	private BigDecimal valorDae;

	@OneToOne
	@JoinColumn(name="solicitacao_laboratorial_id")
	private SolicitacaoServicoLaboratorial solicitacaoLaboratorial;

	@OneToOne
	@JoinColumn(name="dae_gerado_id")
	private Dae daeGerado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoTaxa getTipoTaxa() {
		return tipoTaxa;
	}

	public void setTipoTaxa(TipoTaxa tipoTaxa) {
		this.tipoTaxa = tipoTaxa;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public BigDecimal getDistancia() {
		return distancia;
	}

	public void setDistancia(BigDecimal distancia) {
		this.distancia = distancia;
	}

	public Integer getHorasTrabalhadas() {
		return horasTrabalhadas;
	}

	public void setHorasTrabalhadas(Integer horasTrabalhadas) {
		this.horasTrabalhadas = horasTrabalhadas;
	}

	public Integer getQuantidadeTecnicos() {
		return quantidadeTecnicos;
	}

	public void setQuantidadeTecnicos(Integer quantidadeTecnicos) {
		this.quantidadeTecnicos = quantidadeTecnicos;
	}

	public Integer getQuantidadeVeiculos() {
		return quantidadeVeiculos;
	}

	public void setQuantidadeVeiculos(Integer quantidadeVeiculos) {
		this.quantidadeVeiculos = quantidadeVeiculos;
	}

	public Letra getLetra() {
		return letra;
	}

	public void setLetra(Letra letra) {
		this.letra = letra;
	}

	public AgendamentoRequerimento getAgendamentoRequerimento() {
		return agendamentoRequerimento;
	}

	public void setAgendamentoRequerimento(AgendamentoRequerimento agendamentoRequerimento) {
		this.agendamentoRequerimento = agendamentoRequerimento;
	}

	public boolean isExcluido() {
		return excluido;
	}

	public void setExcluido(boolean excluido) {
		this.excluido = excluido;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public BigDecimal getValorDae() {
		return valorDae;
	}

	public void setValorDae(BigDecimal valorDae) {
		this.valorDae = valorDae;
	}

	public SolicitacaoServicoLaboratorial getSolicitacaoLaboratorial() {
		return solicitacaoLaboratorial;
	}

	public void setSolicitacaoLaboratorial(SolicitacaoServicoLaboratorial solicitacaoLaboratorial) {
		this.solicitacaoLaboratorial = solicitacaoLaboratorial;
	}

	public Dae getDaeGerado() {
		return daeGerado;
	}

	public void setDaeGerado(Dae daeGerado) {
		this.daeGerado = daeGerado;
	}

}
