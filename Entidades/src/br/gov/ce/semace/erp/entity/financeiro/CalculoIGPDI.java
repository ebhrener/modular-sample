package br.gov.ce.semace.erp.entity.financeiro;


/**
 * Classe usada para transportar todos os valores usados no calculo;
 * 
 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 16/07/2014
 *
 */
public class CalculoIGPDI extends CalculoAtualizacaoMonetaria {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -3303970472898048761L;

}
