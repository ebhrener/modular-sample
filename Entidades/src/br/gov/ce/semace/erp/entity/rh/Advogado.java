package br.gov.ce.semace.erp.entity.rh;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="rh", name="advogado", uniqueConstraints=@UniqueConstraint(columnNames="numero_inscricao_oab"))
public class Advogado extends Funcionario {

	private static final long serialVersionUID = 4760585349663971120L;

	@NotNull
	@Column(unique = true, name="numero_inscricao_oab", nullable=false)
	private String numeroInscricaoOAB;

	public Advogado() {
		super();
	}

	public Advogado(String numeroInscricaoOAB) {
		super();
		this.numeroInscricaoOAB = numeroInscricaoOAB;
	}

	//----------------------------------------------------------------------------Gets e Setters----------------------------------------------------------------------------

	public String getNumeroInscricaoOAB() {
		return numeroInscricaoOAB;
	}

	public void setNumeroInscricaoOAB(String numeroInscricaoOAB) {
		this.numeroInscricaoOAB = numeroInscricaoOAB;
	}

}
