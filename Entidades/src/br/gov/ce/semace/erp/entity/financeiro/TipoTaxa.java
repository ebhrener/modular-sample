package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.licenciamento.ConfiguracaoTipoProcesso;
import br.gov.ce.semace.erp.entity.licenciamento.TipoEstudo;

@Entity
@Audited
@Table(schema = "financeiro", name = "tipo_taxa")
public class TipoTaxa implements Serializable{

	private static final long serialVersionUID = -38361722618086017L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_tipo_taxa", name = "SEQ_TIPO_TAXA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TIPO_TAXA")
	private Long id;

	@Column(unique = true)
	private String sigla;

	private String descricao;

	private Boolean ativo;

	@Column(name = "gerar_varios_daes")
	private Boolean gerarVariosDaes;

	@Column(name = "visivel_combo")
	private Boolean visivelCombo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_servico_id")
	private TipoServico tipoServico;

	private Boolean avulso = false;

	@Column(name = "usa_distancia")
	private Boolean usaDistancia = false;

	@Column(name = "usa_letra")
	private Boolean usaLetra = false;

	@Column(name = "usa_atividade")
	private Boolean usaAtividade = false;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy="listTipoTaxa")
	private List<ConfiguracaoTipoProcesso> listConfiguracaoTipoProcesso = new ArrayList<ConfiguracaoTipoProcesso>();

	@Column(name = "usa_spu_anterior")
	private Boolean usaSpuAnterior = false;

	@Column(name = "estudo")
	private boolean estudo = false;

	@Column(name = "analise")
	private boolean analise = false;

	@Column(name = "cobrar_apos_atendimento")
	private Boolean cobrarAposAtendimento;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_estudo_id")
	private TipoEstudo tipoEstudo;

	@Transient
	private boolean selecionado = false;

	@Column(name = "publico")
	private boolean publico;

	/***********************
	 * Getters and Setters *
	 ***********************/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDescricao() {
		return descricao;
	}

	@Transient
	public String getDescricao(int quantidadeCaracteres) {
		if (this.descricao != null) {
			if (this.descricao.length() < quantidadeCaracteres) {
				return this.descricao;
			}

			return descricao.substring(0, quantidadeCaracteres) + "...";
		}

		return "";
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getAvulso() {
		return avulso;
	}

	public void setAvulso(Boolean avulso) {
		this.avulso = avulso;
	}

	public Boolean getUsaDistancia() {
		return usaDistancia;
	}

	public void setUsaDistancia(Boolean usaDistancia) {
		this.usaDistancia = usaDistancia;
	}

	public Boolean getUsaLetra() {
		return usaLetra;
	}

	public void setUsaLetra(Boolean usaLetra) {
		this.usaLetra = usaLetra;
	}

	public Boolean getUsaAtividade() {
		return usaAtividade;
	}

	public void setUsaAtividade(Boolean usaAtividade) {
		this.usaAtividade = usaAtividade;
	}

	public Boolean getGerarVariosDaes() {
		return gerarVariosDaes;
	}

	public void setGerarVariosDaes(Boolean gerarVariosDaes) {
		this.gerarVariosDaes = gerarVariosDaes;
	}

	public TipoServico getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(TipoServico tipoServico) {
		this.tipoServico = tipoServico;
	}

	public Boolean getVisivelCombo() {
		return visivelCombo;
	}

	public void setVisivelCombo(Boolean visivelCombo) {
		this.visivelCombo = visivelCombo;
	}

	public boolean isEstudo() {
		return estudo;
	}

	public void setEstudo(boolean estudo) {
		this.estudo = estudo;
	}

	public boolean isAnalise() {
		return analise;
	}

	public void setAnalise(boolean analise) {
		this.analise = analise;
	}

	public Boolean getCobrarAposAtendimento() {
		return cobrarAposAtendimento;
	}

	public void setCobrarAposAtendimento(Boolean cobrarAposAtendimento) {
		this.cobrarAposAtendimento = cobrarAposAtendimento;
	}

	public List<ConfiguracaoTipoProcesso> getListConfiguracaoTipoProcesso() {
		return listConfiguracaoTipoProcesso;
	}

	public void setListConfiguracaoTipoProcesso(
			List<ConfiguracaoTipoProcesso> listConfiguracaoTipoProcesso) {
		this.listConfiguracaoTipoProcesso = listConfiguracaoTipoProcesso;
	}

	public Boolean getUsaSpuAnterior() {
		return usaSpuAnterior;
	}

	public void setUsaSpuAnterior(Boolean usaSpuAnterior) {
		this.usaSpuAnterior = usaSpuAnterior;
	}

	public TipoEstudo getTipoEstudo() {
		return tipoEstudo;
	}

	public void setTipoEstudo(TipoEstudo tipoEstudo) {
		this.tipoEstudo = tipoEstudo;
	}

	public boolean isPublico() {
		return publico;
	}

	public void setPublico(boolean publico) {
		this.publico = publico;
	}

	@Transient
	public String getAtivoFormatado(){
		if (ativo != null) {
			if (ativo == true) {
				return "Sim";
			}
			return "Não";
		}

		return "";
	}

	@Transient
	public String getLetraFormatado(){
		if (usaLetra != null) {
			if (usaLetra == true) {
				return "Sim";
			}
			return "Não";
		}

		return "";
	}

	@Transient
	public String getAtividadeFormatado(){
		if (usaAtividade != null) {
			if (usaAtividade == true) {
				return "Sim";
			}
			return "Não";
		}

		return "";
	}

	@Transient
	public String getDistanciaFormatado(){
		if (usaDistancia != null) {
			if (usaDistancia == true) {
				return "Sim";
			}
			return "Não";
		}

		return "";
	}

	@Transient
	public String getAvulsoFormatado(){
		if (avulso != null) {
			if (avulso == true) {
				return "Sim";
			}
			return "Não";
		}

		return "";
	}

	public boolean getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		TipoTaxa other = (TipoTaxa) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}
