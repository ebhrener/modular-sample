	package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.AgendamentoRequerimento;
import br.gov.ce.semace.erp.entity.fiscalizacao.AutoInfracao;
import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.geral.Interessado;
import br.gov.ce.semace.erp.entity.licenciamento.Modalidade;
import br.gov.ce.semace.erp.entity.licenciamento.SubtipoProcesso;
import br.gov.ce.semace.erp.entity.licenciamento.TipoProcesso;
import br.gov.ce.semace.erp.enuns.StatusProcesso;
import br.gov.ce.semace.erp.enuns.TipoDocumento;
import br.gov.ce.semace.erp.utils.FormatUtils;

@Entity
@Audited
@Table(schema = "protocolo", name = "processo")
public class Processo implements Serializable{

	private static final long serialVersionUID = 414299163083649159L;

	@Id
	@SequenceGenerator(sequenceName = "protocolo.seq_processo", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	private String spu;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_criacao")
	private Date dataCriacao;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "solicitacao_processo_id")
	private SolicitacaoProcesso solicitacaoProcesso = new SolicitacaoProcesso();

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "documento_id")
	private Documento documento = new Documento();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_processo_id")
	private TipoProcesso tipoProcesso = new TipoProcesso();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="modalidade_processo_id")
	private Modalidade modalidade = new Modalidade();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="subtipo_processo_id")
	private SubtipoProcesso subtipoProcesso = new SubtipoProcesso();


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "interessado_id")
	private Interessado interessado= new Interessado();

	@Column(columnDefinition ="text")
	private String observacao;

	@Column(name = "status_processo")
	private StatusProcesso statusProcesso;

	@Column(name = "permite_digitalizacao")
	private boolean permiteDigitalizacao;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "processo")
	private List<ArquivoProcesso> arquivos = new ArrayList<>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "processo")
	private List<DocumentoProcesso> documentosProcesso = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "processo")
	private List<DocAnexo> anexos = new ArrayList<>();

	@Column(name = "anterior_ged")
	private Boolean isAnteriorGED;

	@Transient
	private String numeroSolicitacao;

	@Transient
	private String assunto;

	@Transient
	private AutoInfracao autoInfracao;

	@Transient
	private TipoDocumento tipoDocumento;

	@Transient
	private AgendamentoRequerimento agendamentoRequerimento;

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusProcesso} é
	 * 	do tipo <b>CANCELADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 2:27:27 PM
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusProcesso} seja
	 * 	<b>CANCELADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusProcesso#isCancelado()
	 */
	@Transient
	public boolean isCancelado(){
		return this.statusProcesso.isCancelado();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusProcesso} é
	 * 	do tipo <b>ATIVO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 2:26:29 PM
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusProcesso} seja
	 * 	<b>ATIVO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusProcesso#isAtivo()
	 */
	@Transient
	public boolean isAtivo(){
		return this.statusProcesso.isAtivo();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public SolicitacaoProcesso getSolicitacaoProcesso() {
		return solicitacaoProcesso;
	}

	public void setSolicitacaoProcesso(SolicitacaoProcesso solicitacaoProcesso) {
		this.solicitacaoProcesso = solicitacaoProcesso;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public TipoProcesso getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(TipoProcesso tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Interessado getInteressado() {
		return interessado;
	}

	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}

	public String getNumeroSolicitacao() {
		return numeroSolicitacao;
	}

	public void setNumeroSolicitacao(String numeroSolicitacao) {
		this.numeroSolicitacao = numeroSolicitacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public StatusProcesso getStatusProcesso() {
		return statusProcesso;
	}

	public void setStatusProcesso(StatusProcesso statusProcesso) {
		this.statusProcesso = statusProcesso;
	}

	public boolean isPermiteDigitalizacao() {
		return permiteDigitalizacao;
	}

	public void setPermiteDigitalizacao(boolean permiteDigitalizacao) {
		this.permiteDigitalizacao = permiteDigitalizacao;
	}

	public List<ArquivoProcesso> getArquivos() {
		return arquivos;
	}

	public void setArquivos(List<ArquivoProcesso> arquivos) {
		this.arquivos = arquivos;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public List<DocAnexo> getAnexos() {
		return anexos;
	}

	public void setAnexos(List<DocAnexo> anexos) {
		this.anexos = anexos;
	}

	public List<DocumentoProcesso> getDocumentosProcesso() {
		return documentosProcesso;
	}

	public void setDocumentosProcesso(List<DocumentoProcesso> documentosProcesso) {
		this.documentosProcesso = documentosProcesso;
	}

	public AutoInfracao getAutoInfracao() {
		return autoInfracao;
	}

	public void setAutoInfracao(AutoInfracao autoInfracao) {
		this.autoInfracao = autoInfracao;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public AgendamentoRequerimento getAgendamentoRequerimento() {
		return agendamentoRequerimento;
	}

	public void setAgendamentoRequerimento(
			AgendamentoRequerimento agendamentoRequerimento) {
		this.agendamentoRequerimento = agendamentoRequerimento;
	}

	public Modalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}

	public SubtipoProcesso getSubtipoProcesso() {
		return subtipoProcesso;
	}

	public void setSubtipoProcesso(SubtipoProcesso subtipoProcesso) {
		this.subtipoProcesso = subtipoProcesso;
	}

	public Boolean getIsAnteriorGED() {
		return isAnteriorGED;
	}

	public void setIsAnteriorGED(Boolean isAnteriorGED) {
		this.isAnteriorGED = isAnteriorGED;
	}

	public String getDescricaoTipoModalidadeSubtipoProcesso() {
		return FormatUtils.getDescricaoTipoModalidadeSubtipoProcesso(this.tipoProcesso, this.subtipoProcesso, this.modalidade);
	}

	public String getDescricaoSiglaTipoModalidadeSubtipoProcesso() {
		return FormatUtils.getDescricaoSiglaTipoModalidadeSubtipoProcesso(this.tipoProcesso, this.subtipoProcesso, this.modalidade);
	}
}
