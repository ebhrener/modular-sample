package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "historico_shape_extra")
public class HistoricoShapeExtra implements Serializable{

	private static final long serialVersionUID = 595546045443852731L;

	@Id
	@SequenceGenerator(sequenceName="seq_historio_shape_extra", name="seq_historio_shape_extra", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_historio_shape_extra")
	private Long id;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="shape_extra_id")
	private ShapeExtra shapeExtra;
	
	@Column(name="dt_alteracao")
	private Date data;
	
	@Column(nullable= false)
	private Boolean ativo = Boolean.TRUE;

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public ShapeExtra getShapeExtra() {
		return shapeExtra;
	}

	public void setShapeExtra(ShapeExtra shapeExtra) {
		this.shapeExtra = shapeExtra;
	}
	
}
