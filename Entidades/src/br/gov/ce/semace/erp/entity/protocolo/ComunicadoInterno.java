package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.rh.Setor;
import br.gov.ce.semace.erp.enuns.StatusDocumento;

@Entity
@Audited
@Table(name="comunicado_interno",schema="protocolo")
public class ComunicadoInterno implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -5310586190129101035L;

	@Id
	@SequenceGenerator(sequenceName ="protocolo.seq_comunicado_interno", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	private String titulo;

	@Column(columnDefinition="text")
	private String descricao;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="documento_id")
	private Documento documento = new Documento() ;

	private Boolean ativo;

	@Column(name = "status_ci")
	private StatusDocumento statusCI;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "processo_id")
	private Processo processo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setor_destino_id")
	private Setor setorDestino = new Setor();

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusOficio} é
	 * 	do tipo <b>CANCELADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 22/06/2013 08:44:56
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusOficio} seja
	 * 	<b>CANCELADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDocumento#isCancelado()
	 */
	@Transient
	public boolean isCancelado(){
		return this.statusCI.isCancelado();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public StatusDocumento getStatusCI() {
		return statusCI;
	}

	public void setStatusCI(StatusDocumento statusCI) {
		this.statusCI = statusCI;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public Setor getSetorDestino() {
		return setorDestino;
	}

	public void setSetorDestino(Setor setorDestino) {
		this.setorDestino = setorDestino;
	}
}
