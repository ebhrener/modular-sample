package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Audited
@Table(name="telefone", schema="geral")
public class Telefone implements Serializable{

	private static final long serialVersionUID = -2686407452080740643L;

	@Id @SequenceGenerator(sequenceName="geral.seq_telefone", name="SEQTELEFONE", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQTELEFONE")
	private Long id;

	@ManyToOne
	@JoinColumn(name="tipo_telefone_id")
	private TipoTelefone tipoTelefone = new TipoTelefone();

	@Column
	private String ddd;

	@Column
	private String numero;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="pessoa_id")
	@JsonBackReference
	private Pessoa pessoa = new Pessoa();

	/**
	 * Método {@link Transient} que informa o {@link Telefone} formatado.
	 * <p> Exemplo: ( 85 ) 3226-7777
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 25/09/2013 11:16:57
	 *
	 * @return
	 * 	Retorna uma {@link String} com o padrão ( DDD ) NUMERO.
	 */
	@Transient
	public String getNumeroFormatado(){
		return "( " + getDdd() + " ) " + getNumero();
	}

	@Transient
	private boolean ativo = Boolean.TRUE;

	/**
	 * @return the id
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the tipoTelefone
	 */
	public TipoTelefone getTipoTelefone() {
		return this.tipoTelefone;
	}

	/**
	 * @param tipoTelefone the tipoTelefone to set
	 */
	public void setTipoTelefone(TipoTelefone tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}

	/**
	 * @return the ddd
	 */
	public String getDdd() {
		return this.ddd;
	}

	/**
	 * @param ddd the ddd to set
	 */
	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return this.numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the pessoa
	 */
	public Pessoa getPessoa() {
		return this.pessoa;
	}

	/**
	 * @param pessoa the pessoa to set
	 */
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ddd == null) ? 0 : ddd.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Telefone other = (Telefone) obj;
		if (ddd == null) {
			if (other.ddd != null) {
				return false;
			}
		} else if (!ddd.equals(other.ddd)) {
			return false;
		}
		if (numero == null) {
			if (other.numero != null) {
				return false;
			}
		} else if (!numero.equals(other.numero)) {
			return false;
		}
		return true;
	}
}