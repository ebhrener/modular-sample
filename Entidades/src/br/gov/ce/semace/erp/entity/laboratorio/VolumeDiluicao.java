package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="volume_diluicao", schema="laboratorio")
public class VolumeDiluicao implements Serializable {

	private static final long serialVersionUID = -3369255350260059253L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_volume_diluicao", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	private String descricao;
	
	private Float multiplicador;
	
	private Boolean ativo = Boolean.TRUE;
	
	@OneToMany(mappedBy="volumeDiluicao")
	private List<ResultadoAnalise> resultadosAnalise = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Float getMultiplicador() {
		return multiplicador;
	}

	public void setMultiplicador(Float multiplicador) {
		this.multiplicador = multiplicador;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public List<ResultadoAnalise> getResultadosAnalise() {
		return resultadosAnalise;
	}

	public void setResultadosAnalise(List<ResultadoAnalise> resultadosAnalise) {
		this.resultadosAnalise = resultadosAnalise;
	}
	
}
