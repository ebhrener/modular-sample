package br.gov.ce.semace.erp.entity.seguranca;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "acesso_usuario_web", schema = "seguranca")
public class AcessoUsuarioWeb implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -7947216286983575570L;

	@Id
	@SequenceGenerator(sequenceName = "seguranca.seq_acesso_usuario_web", name = "seguranca.seq_acesso_usuario_web", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seguranca.seq_acesso_usuario_web")
	private Long id;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(name="usuario_web_id")
	private UsuarioWeb usuarioWeb;

	@Column(name="data_acesso", nullable=false)
	private Date dataAcesso;

	public UsuarioWeb getUsuarioWeb() {
		return usuarioWeb;
	}

	public void setUsuarioWeb(UsuarioWeb usuarioWeb) {
		this.usuarioWeb = usuarioWeb;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataAcesso() {
		return dataAcesso;
	}

	public void setDataAcesso(Date dataAcesso) {
		this.dataAcesso = dataAcesso;
	}

}
