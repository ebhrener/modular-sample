package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.TipoFuncionarioBoletim;

@Entity
@Audited
@Table(name="funcionario_boletim", schema="laboratorio")
public class FuncionarioBoletim implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 4605530796431506297L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_funcionario_boletim", name="laboratorio.seq_funcionario_boletim", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="laboratorio.seq_funcionario_boletim")
	private Long id;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(name="funcionario_id", nullable=false)
	private Funcionario funcionario;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(name="boletim_id", nullable=false)
	private Boletim boletim;

	@Enumerated(EnumType.ORDINAL)
	@Column(name="tipo_func_bol")
	private TipoFuncionarioBoletim tipoFuncionarioBoletim;

	@Transient
	private List<TipoFuncionarioBoletim> listTipoFuncionarioBoletim = new ArrayList<TipoFuncionarioBoletim>();

	public FuncionarioBoletim() {
		super();
	}

	public FuncionarioBoletim(Funcionario funcionario, Boletim boletim,
			TipoFuncionarioBoletim tipoFuncionarioBoletim) {
		super();
		this.funcionario = funcionario;
		this.boletim = boletim;
		this.tipoFuncionarioBoletim = tipoFuncionarioBoletim;
	}

	public Boletim getBoletim() {
		return boletim;
	}

	public void setBoletim(Boletim boletim) {
		this.boletim = boletim;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoFuncionarioBoletim getTipoFuncionarioBoletim() {
		return tipoFuncionarioBoletim;
	}

	public void setTipoFuncionarioBoletim(TipoFuncionarioBoletim tipoFuncionarioBoletim) {
		this.tipoFuncionarioBoletim = tipoFuncionarioBoletim;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((boletim == null) ? 0 : boletim.hashCode());
		result = prime * result
				+ ((funcionario == null) ? 0 : funcionario.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((tipoFuncionarioBoletim == null) ? 0
						: tipoFuncionarioBoletim.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FuncionarioBoletim other = (FuncionarioBoletim) obj;
		if (boletim == null) {
			if (other.boletim != null) {
				return false;
			}
		} else if (!boletim.equals(other.boletim)) {
			return false;
		}
		if (funcionario == null) {
			if (other.funcionario != null) {
				return false;
			}
		} else if (!funcionario.equals(other.funcionario)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (tipoFuncionarioBoletim != other.tipoFuncionarioBoletim) {
			return false;
		}
		return true;
	}

	public List<TipoFuncionarioBoletim> getListTipoFuncionarioBoletim() {
		return listTipoFuncionarioBoletim;
	}

	public void setListTipoFuncionarioBoletim(
			List<TipoFuncionarioBoletim> listTipoFuncionarioBoletim) {
		this.listTipoFuncionarioBoletim = listTipoFuncionarioBoletim;
	}

	public String getAllResponsibilities() {
		String s = "";

		for (TipoFuncionarioBoletim tipoFuncionarioBoletim : listTipoFuncionarioBoletim) {
			s += tipoFuncionarioBoletim.getDescricao()+"/";
		}

		if (s.length() > 0) {
			s = s.substring(0, s.length() - 1);
		}
		return s;
	}

}
