package br.gov.ce.semace.erp.entity.atendimento.agrotoxico;

import java.io.Serializable;
import java.util.Date;

public class ProdutoAgrotoxico implements Serializable {

	private static final long serialVersionUID = 1288534609808135866L;

	private int codProduto;
	private String nomeProduto;
	private Date dataRegistroInicial;
	private String situacao;
	private Date dataRegistroNova;
	private String processoId;
	private Date dataValidade;
	private Boolean selecionado;
	private String spu;

	public int getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(int codProduto) {
		this.codProduto = codProduto;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public Date getDataRegistroInicial() {
		return dataRegistroInicial;
	}

	public void setDataRegistroInicial(Date dataRegistroInicial) {
		this.dataRegistroInicial = dataRegistroInicial;
	}

	public Date getDataRegistroNova() {
		return dataRegistroNova;
	}

	public void setDataRegistroNova(Date dataRegistroNova) {
		this.dataRegistroNova = dataRegistroNova;
	}

	public String getProcessoId() {
		return processoId;
	}

	public void setProcessoId(String processoId) {
		this.processoId = processoId;
	}

	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public Boolean getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(Boolean selecionado) {
		this.selecionado = selecionado;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

}
