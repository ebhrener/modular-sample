package br.gov.ce.semace.erp.entity.julgamento;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Comunicacao;
import br.gov.ce.semace.erp.enuns.FaseJulgamento;

@Entity
@Audited
@Table(name = "comunicacao_julgamento", schema="julgamento")
public class ComunicacaoJulgamento extends Comunicacao {

	private static final long serialVersionUID = 2377233019302363116L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "julgamento_id")
	private Julgamento julgamento = new Julgamento();
	
	@Column(name = "fase_julgamento")
	private FaseJulgamento faseJulgamento;
	
	public Julgamento getJulgamento() {
		return julgamento;
	}

	public void setJulgamento(Julgamento julgamento) {
		this.julgamento = julgamento;
	}

	public FaseJulgamento getFaseJulgamento() {
		return faseJulgamento;
	}

	public void setFaseJulgamento(FaseJulgamento faseJulgamento) {
		this.faseJulgamento = faseJulgamento;
	}
	
	/**
	 * Método {@link Transient}e para verificar se o {@link #faseJulgamento} é 
	 * 	do tipo <b>DEFESA</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:23:40 PM
	 *
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #faseJulgamento} seja 
	 * 	<b>DEFESA</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see FaseJulgamento#isFaseDefesa()
	 */
	@Transient
    public boolean isFaseDefesa() {
    	return this.faseJulgamento.isFaseDefesa();
    }
	
	/**
	 * Método {@link Transient}e para verificar se o {@link #faseJulgamento} é 
	 * 	do tipo <b>ALEGACAO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:16:15 PM
	 *
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #faseJulgamento} seja 
	 * 	<b>ALEGACAO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see FaseJulgamento#isFaseAlegacao()
	 */
	@Transient
    public boolean isFaseAlegacao() {
    	return this.faseJulgamento.isFaseAlegacao();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #faseJulgamento} é 
	 * 	do tipo <b>DECISAO_1_INSTANCIA</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:16:15 PM
	 *
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #faseJulgamento} seja 
	 * 	<b>DECISAO_1_INSTANCIA</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see FaseJulgamento#isFaseDecisao1Instancia()
	 */
	@Transient
    public boolean isFaseDecisao1Instancia() {
    	return this.faseJulgamento.isFaseDecisao1Instancia();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #faseJulgamento} é 
	 * 	do tipo <b>RETRATACAO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:15:13 PM
	 *
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #faseJulgamento} seja 
	 * 	<b>RETRATACAO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see FaseJulgamento#isFaseRetratacao()
	 */
	@Transient
    public boolean isFaseRetratacao() {
    	return this.faseJulgamento.isFaseRetratacao();
    }
    
	/**
	 * Método {@link Transient}e para verificar se o {@link #faseJulgamento} é 
	 * 	do tipo <b>TRANSITADO_EM_JULGADO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:14:40 PM
	 *
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #faseJulgamento} seja 
	 * 	<b>TRANSITADO_EM_JULGADO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see FaseJulgamento#isFaseTransitadoEmJulgado()
	 */
	@Transient
    public boolean isFaseTransitadoEmJulgado() {
    	return this.faseJulgamento.isFaseTransitadoEmJulgado();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #faseJulgamento} é 
	 * 	do tipo <b>INSTRUCAO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:14:10 PM
	 *
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #faseJulgamento} seja 
	 * 	<b>INSTRUCAO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see FaseJulgamento#isFaseInstrucao()
	 */
	@Transient
    public boolean isFaseInstrucao() {
    	return this.faseJulgamento.isFaseInstrucao();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #faseJulgamento} é 
	 * 	do tipo <b>DECISAO_2_INSTANCIA</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:13:12 PM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #faseJulgamento} seja 
	 * 	<b>DECISAO_2_INSTANCIA</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see FaseJulgamento#isFaseDecisao2Instancia()
	 */
	@Transient
    public boolean isFaseDecisao2Instancia() {
    	return this.faseJulgamento.isFaseDecisao2Instancia();
    }
    
	/**
	 * Método {@link Transient}e para verificar se o {@link #faseJulgamento} é 
	 * 	do tipo <b>SUSPENSO</b>
	 * 
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - Jun 3, 2013 3:12:41 PM
	 * 
	 * @return 
	 * 		Retorna <code>true</code> caso o {@link #faseJulgamento} seja 
	 * 	<b>SUSPENSO</b>. Caso contrário, retorna <code>false</code>
	 * 
	 * @see FaseJulgamento#isFaseSuspenso()
	 */
	@Transient
    public boolean isFaseSuspenso() {
    	return this.faseJulgamento.isFaseSuspenso();
    }
}