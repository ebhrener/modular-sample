package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="movimento_agendamento", schema="adm_siga")
public class MovimentoAgendamentoSiga implements Serializable{


	private static final long serialVersionUID = -2128746573014371954L;

	@Id @SequenceGenerator(sequenceName="adm_siga.movimento_agendamento_id_seq", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Integer id;
	
	@Column(name="agendamento_id",nullable=false)
	private Integer agendamentoId;

	@Column(name="usuario_inicio_id",nullable=false,length=4)
	private String usuarioInicioId;
	
	@Column(name="usuario_destino_id",nullable=false,length=4)
	private String usuarioDestinoId;
	
	@Column(name="fim_da_fila")
	private Boolean fimDaFila=true;
	
	@Column(name="status_id",nullable=false)
	private Integer statusId;
	
	@Column(name="data_hora",nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHora = new Date();
	
	@Column(columnDefinition="text")
	private String comentario;
	
	@Column(name="sistema_natuur")
	private Boolean sistemaNatuur;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAgendamentoId() {
		return agendamentoId;
	}

	public void setAgendamentoId(Integer agendamentoId) {
		this.agendamentoId = agendamentoId;
	}

	public String getUsuarioInicioId() {
		return usuarioInicioId;
	}

	public void setUsuarioInicioId(String usuarioInicioId) {
		this.usuarioInicioId = usuarioInicioId;
	}

	public String getUsuarioDestinoId() {
		return usuarioDestinoId;
	}

	public void setUsuarioDestinoId(String usuarioDestinoId) {
		this.usuarioDestinoId = usuarioDestinoId;
	}

	public Boolean getFimDaFila() {
		return fimDaFila;
	}

	public void setFimDaFila(Boolean fimDaFila) {
		this.fimDaFila = fimDaFila;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Boolean getSistemaNatuur() {
		return sistemaNatuur;
	}

	public void setSistemaNatuur(Boolean sistemaNatuur) {
		this.sistemaNatuur = sistemaNatuur;
	}

}