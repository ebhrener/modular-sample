package br.gov.ce.semace.erp.entity.notificacaoprocesso;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * Esta entidade mapeia qual processo está em qual grupo de notificação
 *
 * @author joerlan.lima
 *
 */
@Audited
@Entity
@Table(name="grupo_notificacao_processo", schema="notificacao_processo")
public class GrupoNotificacaoProcesso implements Serializable{


	/**
	 * Serial
	 */
	private static final long serialVersionUID = -5937764084096030899L;

	@Id
	@SequenceGenerator(sequenceName="notificacao_processo.seq_grupo_notificacao_processo", name="notificacao_processo.seq_grupo_notificacao_processo", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="notificacao_processo.seq_grupo_notificacao_processo")
	private Long id;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(nullable=false, name="grupo_notificacao_id")
	private GrupoNotificacao grupoNotificacao;

	@Column(nullable= false, unique=true, name="processo_id")
	private String processoID;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GrupoNotificacao getGrupoNotificacao() {
		return grupoNotificacao;
	}

	public void setGrupoNotificacao(GrupoNotificacao grupoNotificacao) {
		this.grupoNotificacao = grupoNotificacao;
	}

	public String getProcessoID() {
		return processoID;
	}

	public void setProcessoID(String processoID) {
		this.processoID = processoID;
	}


}
