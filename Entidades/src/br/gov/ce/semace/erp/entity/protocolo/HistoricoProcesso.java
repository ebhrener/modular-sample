package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.entity.rh.Setor;
import br.gov.ce.semace.erp.enuns.TipoHistoricoProcesso;

@Entity
@Audited
@Table(schema = "protocolo", name = "historico_processo")
public class HistoricoProcesso implements Serializable{

	private static final long serialVersionUID = 6372404011772781072L;

	@Id
	@SequenceGenerator(sequenceName = "protocolo.seq_historico_processo", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_criacao")
	private Date dataCriacao;

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_criacao")
	private Date horaCriacao;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "documento_gerador_id")
	private Documento documentoGerador = new Documento();

	@Column(columnDefinition="text")
	private String observacao;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "funcionario_criador_id")
	private Funcionario funcionarioCriador;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "funcionario_destino_id")
	private Funcionario funcionarioDestino;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "processo_id")
	private Processo processo;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "setor_origem_id")
	private Setor setorOrigem;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "setor_destino_id")
	private Setor setorDestino;

	/**
	 * Indica o Tipo de histórico do processo (ver {@link TipoHistoricoProcesso})
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 04/07/2013 09:46:27
	 */
	@Column(name = "tipo_historico_processo")
	private TipoHistoricoProcesso tipoHistoricoProcesso;

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoHistoricoProcesso} é
	 * 	do tipo <b>JULGAMENTO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 04/07/2013 09:50:15
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoHistoricoProcesso} seja
	 * 	<b>JULGAMENTO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoHistoricoProcesso#isTipoJulgamento()
	 */
	@Transient
    public boolean isTipoJulgamento() {
		return this.tipoHistoricoProcesso.isTipoJulgamento();
    }

	/**
	 * Método {@link Transient}e para verificar se o {@link #tipoHistoricoProcesso} é
	 * 	do tipo <b>PROCESSO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 05/07/2013 09:43:49
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #tipoHistoricoProcesso} seja
	 * 	<b>PROCESSO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see TipoHistoricoProcesso#isTipoProcesso()
	 */
	@Transient
    public boolean isTipoProcesso() {
		return this.tipoHistoricoProcesso.isTipoProcesso();
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Documento getDocumentoGerador() {
		return documentoGerador;
	}

	public void setDocumentoGerador(Documento documentoGerador) {
		this.documentoGerador = documentoGerador;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Funcionario getFuncionarioCriador() {
		return funcionarioCriador;
	}

	public void setFuncionarioCriador(Funcionario funcionarioCriador) {
		this.funcionarioCriador = funcionarioCriador;
	}

	public Funcionario getFuncionarioDestino() {
		return funcionarioDestino;
	}

	public void setFuncionarioDestino(Funcionario funcionarioDestino) {
		this.funcionarioDestino = funcionarioDestino;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public Setor getSetorOrigem() {
		return setorOrigem;
	}

	public void setSetorOrigem(Setor setorOrigem) {
		this.setorOrigem = setorOrigem;
	}

	public Setor getSetorDestino() {
		return setorDestino;
	}

	public void setSetorDestino(Setor setorDestino) {
		this.setorDestino = setorDestino;
	}

	public Date getHoraCriacao() {
		return horaCriacao;
	}

	public void setHoraCriacao(Date horaCriacao) {
		this.horaCriacao = horaCriacao;
	}

	/**
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 04/07/2013 09:47:53
	 *
	 * @return the tipoHistoricoProcesso
	 */
	public TipoHistoricoProcesso getTipoHistoricoProcesso() {
		return tipoHistoricoProcesso;
	}

	/**
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 04/07/2013 09:47:49
	 *
	 * @param tipoHistoricoProcesso the tipoHistoricoProcesso to set
	 */
	public void setTipoHistoricoProcesso(TipoHistoricoProcesso tipoHistoricoProcesso) {
		this.tipoHistoricoProcesso = tipoHistoricoProcesso;
	}

}