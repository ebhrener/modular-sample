package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.utils.StringUtils;

/**
 *
 * @author Harisson Rafael [harissonrafael@gmail.com] - 08/07/2016 09:16:48
 *
 */
@Entity
@Audited
@Table(name = "funcionario_registro_ponto", schema = "rh")
public class RegistroPonto implements Serializable {

	private static final long serialVersionUID = -8032528225713739604L;

	@Id
	@SequenceGenerator(sequenceName = "rh.seq_funcionario_registro_ponto", name = "seq_funcionario_registro_ponto", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_funcionario_registro_ponto")
	private Long id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;

	@Column(name = "data_hora_cadastro")
	private Date dataHoraCadastro;

	@Column(name = "ativo")
	private boolean ativo;

	@OneToMany(mappedBy="registroPonto",fetch=FetchType.LAZY)
	private List<JustificativaRegistroPonto> justificativas = new ArrayList<>();

	public RegistroPonto(Funcionario funcionario, Date dataHoraCadastro,
			boolean ativo) {
		this.funcionario = funcionario;
		this.dataHoraCadastro = dataHoraCadastro;
		this.ativo = ativo;
	}
	public RegistroPonto(Date dataHoraCadastro,	boolean ativo) {
		this.dataHoraCadastro = dataHoraCadastro;
		this.ativo = ativo;
	}
	public RegistroPonto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Date getDataHoraCadastro() {
		return dataHoraCadastro;
	}

	public void setDataHoraCadastro(Date dataHoraCadastro) {
		this.dataHoraCadastro = dataHoraCadastro;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public List<JustificativaRegistroPonto> getJustificativas() {
		return justificativas;
	}

	public void setJustificativas(List<JustificativaRegistroPonto> justificativas) {
		this.justificativas = justificativas;
	}

	public void getHoraFromString(String hora, Date data){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		if(StringUtils.isNotBlank(hora.split(":")[0])){
			calendar.set(Calendar.HOUR_OF_DAY,Integer.parseInt(hora.split(":")[0]));
		}
		if(StringUtils.isNotBlank(hora.split(":")[1])){
			calendar.set(Calendar.MINUTE,Integer.parseInt(hora.split(":")[1]));
		}
		setDataHoraCadastro(calendar.getTime());
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RegistroPonto other = (RegistroPonto) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}