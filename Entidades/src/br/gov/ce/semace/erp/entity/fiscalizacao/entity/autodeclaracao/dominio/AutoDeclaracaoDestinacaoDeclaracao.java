package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio;

public enum AutoDeclaracaoDestinacaoDeclaracao {
	
	AMENDOIN(2,"Amendoim",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	GERGILIM(3,"Gergilim",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	ARROZ(4,"Arroz",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	FEIJAO(5,"Feijão",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	MILHO(6,"Milho",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	SORGO(7,"Sorgo",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	GIRASSOL(8,"Girassol",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	MAMONA(9,"Mamona",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(4)),
	ALGODAO(10,"Algodão",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	ABACAXI(15,"Abacaxi",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(4)),
	ACEROLA(16,"Acerola",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(5)),
	BANANA(17,"Banana",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	CAJU(18,"Caju",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(5)),
	COCO(19,"Côco",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(5)),
	GOIABA(20,"Goiaba",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(5)),
	LIMAO(21,"Limão",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(5)),
	LARANJA(22,"Laranja",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(5)),
	MAMAO(23,"Mamão",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(4)),
	MANGA(24,"Manga",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(5)),
	MARACUJA(25,"Maracujá",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(4)),
	UVA(26,"Uva",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(5)),
	PESCA_CONTINENTAL(27,"Pescado Continental",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(4),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	PESCA_MARITIMO(28,"Pescado Maritimo",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(4),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	BOVINO(29,"Bovino",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(2),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	APICULTURA(30,"Apicultura",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(20),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	CAPRINO(31,"Caprino",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(2),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	OVINO(32,"Ovino",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(2),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	GALINHA(33,"Galinha Caipira",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(2),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	PALMA(34,"Palma Forrageira",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	PASTAGEM(35,"Pastagem Cultivada",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	MANEJO_AGROPASTORIL(36,"Manejo Agropastoril",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	MILHO_FEIJAO(37,"Milho e Feijão Consorciado",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	MANDIOCA_FEIJAO(38,"Mandioca e Feijão Consorciado",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	MAMONA_FEIJAO(39,"Mamona e Feijão Consorciado",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	MAMONA_MILHO_FEIJAO(40,"Mamona, Milho e Feijão Consorciado",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	MANDIOCA_MILHO_FEIJAO(41,"Mandioca, Milho e Feijão Consorciado",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	MANDIOCA(42,"Mandioca",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(4)),
	SISAL(43,"Sisal",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	PIQUI(44,"Piqui",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	CARNAUBA(45,"Carnaúba",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	COCO_BABACU(46,"Coco Babaçu",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	CANA_ACUCAR(47,"Cana de Açúcar",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(4)),
	OLERICOLAS(48,"Olerícolas",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(22),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	SUINO(49,"Suinocultura",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(2),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(6)),
	CISTERNA(50,"Cisterna",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(16),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(5)),
	ROCADEIRA(51,"Roçadeira",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(16),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(5)),
	MAMONA_MILHO(52,"Mamona + Milho",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(40),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	FRANGO(53,"Frango",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(2),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(2)),
	CERCA_ARAME(54,"Cerca de Arame",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(44),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(5)),
	ARTESANATO(55,"Artesanato",AutoDeclaracaoUnidadeMedida.showDescricaoByCodigo(45),AutoDeclaracaoTempoColheita.showDescricaoByCodigo(4));
	
	
	private Integer codigo;
	private String descricao;
	private String unidadeMedida;
	private String tempoColheita;
	
	private AutoDeclaracaoDestinacaoDeclaracao(Integer codigo, String descricao, String unidadeMedida, String tempoColheita) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.unidadeMedida = unidadeMedida;
		this.tempoColheita = tempoColheita;
	}

	public static String showDescricaoByCodigo(Integer codigo){
		for(AutoDeclaracaoDestinacaoDeclaracao auto: AutoDeclaracaoDestinacaoDeclaracao.values()){
			if(auto.getCodigo().equals(codigo)){
				return auto.descricao;
			}
		}
		return "Não Informado";
	}
	
	public static String showUnidadeMedidaByCodigo(Integer codigo){
		for(AutoDeclaracaoDestinacaoDeclaracao auto: AutoDeclaracaoDestinacaoDeclaracao.values()){
			if(auto.getCodigo().equals(codigo)){
				return auto.unidadeMedida;
			}
		}
		return "Não Informado";
	}
	
	public static String showTempoColheitaByCodigo(Integer codigo){
		for(AutoDeclaracaoDestinacaoDeclaracao auto: AutoDeclaracaoDestinacaoDeclaracao.values()){
			if(auto.getCodigo().equals(codigo)){
				return auto.tempoColheita;
			}
		}
		return "Não Informado";
	}
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public String getTempoColheita() {
		return tempoColheita;
	}

	public void setTempoColheita(String tempoColheita) {
		this.tempoColheita = tempoColheita;
	}
	
}
