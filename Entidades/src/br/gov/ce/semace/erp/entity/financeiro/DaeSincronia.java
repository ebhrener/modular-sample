package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "financeiro", name = "dae_sincronia")
public class DaeSincronia implements Serializable{

	private static final long serialVersionUID = -7116834763584818280L;
	
	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_dae_sincronia", name = "SEQ_DAE_SINCRONIA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DAE_SINCRONIA")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Date dataCadastro;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_sincronia")
	private Date dataSincronia;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataSincronia() {
		return dataSincronia;
	}

	public void setDataSincronia(Date dataSincronia) {
		this.dataSincronia = dataSincronia;
	}

}
