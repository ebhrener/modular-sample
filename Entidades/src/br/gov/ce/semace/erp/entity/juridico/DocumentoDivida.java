package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.enuns.ModeloDocumentoDivida;


/**
 *
 * Documento Dívida
 *
 * @author joerlan.lima
 *
 */
@Entity
@Audited
@Table(name="documento_divida", schema="juridico")
public class DocumentoDivida implements Serializable{


	/**
	 * Serial
	 */
	private static final long serialVersionUID = -1820925574161123857L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_documento_divida", name="juridico.seq_documento_divida", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_documento_divida")
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="divida_ativa_id", nullable= false)
	private DividaAtiva dividaAtiva;

	/**
	 * TODO: tornar not null quando o ged for implementado
	 */
	@Column(name="ged_id")
	private String identificadorGED;

	@OneToOne(fetch= FetchType.LAZY, optional = false)
	@JoinColumn(name="documento_id")
	private Documento documento;

	/**
	 * Remover coluna após a implementação do GED
	 */
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "org.hibernate.type.BinaryType")
	private byte[] arquivo;

	@Column(name="file_name")
	private String fileName;

	private Boolean excluido;

	private Boolean assinado;

	@Column(name="nr_documento")
	private String nrDocumento;

	private ModeloDocumentoDivida modelo;

	@Column(name="has_acordo")
	private Boolean hasAcordo;

	@Column(name="numero_aif")
	private String numeroAIF;

	@Column(name="numero_decisao")
	private String numeroDecisao;

	@Column(name="qtd_dias")
	private Integer qtdDias;

	@Column(name="has_julgamento")
	private Boolean hasJulgamento;

	@Column(name="data_notificacao_autuado")
	private Date dataNotificacaoAutuado;

	@Column(name="data_notificacao_apos_julgamento")
	private Date dataNotificacaoAposJulgamento;

	@Column(name="data_fim_prazo_notificacao")
	private Date dataFimPrazoNotificacao;

	@Column(name="valor_decisao")
	private BigDecimal valorDecisao;

	@Column(name="numero_acordo")
	private String numeroAcordo;

	@Column(name="termo_inscricao")
	private String termoInscricao;

	/**
	 * Ao gerar {@link DocumentoDivida}s dos {@link ModeloDocumentoDivida} TIDA e TRIDA é possível informar outros SPU's além do original oriundo da {@link DividaAtiva}.
	 */
	@Column(name="list_spu")
	private String listSpu;

	//TODO: implementar na Petição Inicial
//	@Column(name="nr_cda")
//	private String nrCDA;

	/**
	 * Campos da tabela de valores
	 */
	private BigDecimal valor1;
	private BigDecimal valor2;
	private BigDecimal valor3;
	private BigDecimal valor4;

	private Date data1;
	private Date data2;
	private Date data3;

	@Column(name="data_cadastro")
	private Date dataCadastro;

	@OneToMany(mappedBy="documentoDivida")
	private List<AssinanteDocumentoDivida> assinantes;

	@Transient
	private List<Corresponsavel> corresponsaveis;

	public String getNumeroAIF() {
		return numeroAIF;
	}

	public void setNumeroAIF(String numeroAIF) {
		this.numeroAIF = numeroAIF;
	}

	public Date getDataNotificacaoAutuado() {
		return dataNotificacaoAutuado;
	}

	public void setDataNotificacaoAutuado(Date dataNotificacaoAutuado) {
		this.dataNotificacaoAutuado = dataNotificacaoAutuado;
	}

	public ModeloDocumentoDivida getModelo() {
		return modelo;
	}

	public void setModelo(ModeloDocumentoDivida modelo) {
		this.modelo = modelo;
	}

	public Boolean getHasAcordo() {
		return hasAcordo;
	}

	public void setHasAcordo(Boolean hasAcordo) {
		this.hasAcordo = hasAcordo;
	}

	public String getNumeroDecisao() {
		return numeroDecisao;
	}

	public void setNumeroDecisao(String numeroDecisao) {
		this.numeroDecisao = numeroDecisao;
	}

	public Date getDataFimPrazoNotificacao() {
		return dataFimPrazoNotificacao;
	}

	public void setDataFimPrazoNotificacao(Date dataFimPrazoNotificacao) {
		this.dataFimPrazoNotificacao = dataFimPrazoNotificacao;
	}

	public BigDecimal getValorDecisao() {
		return valorDecisao;
	}

	public void setValorDecisao(BigDecimal valorDecisao) {
		this.valorDecisao = valorDecisao;
	}

	public Boolean getHasJulgamento() {
		return hasJulgamento;
	}

	public void setHasJulgamento(Boolean hasJulgamento) {
		this.hasJulgamento = hasJulgamento;
	}

	public Date getDataNotificacaoAposJulgamento() {
		return dataNotificacaoAposJulgamento;
	}

	public void setDataNotificacaoAposJulgamento(
			Date dataNotificacaoAposJulgamento) {
		this.dataNotificacaoAposJulgamento = dataNotificacaoAposJulgamento;
	}

	public Integer getQtdDias() {
		return qtdDias;
	}

	public void setQtdDias(Integer qtdDias) {
		this.qtdDias = qtdDias;
	}

	public BigDecimal getValor1() {
		return valor1;
	}

	public void setValor1(BigDecimal valor1) {
		this.valor1 = valor1;
	}

	public BigDecimal getValor2() {
		return valor2;
	}

	public void setValor2(BigDecimal valor2) {
		this.valor2 = valor2;
	}

	public BigDecimal getValor3() {
		return valor3;
	}

	public void setValor3(BigDecimal valor3) {
		this.valor3 = valor3;
	}

	public BigDecimal getValor4() {
		return valor4;
	}

	public void setValor4(BigDecimal valor4) {
		this.valor4 = valor4;
	}

	public Date getData1() {
		return data1;
	}

	public void setData1(Date data1) {
		this.data1 = data1;
	}

	public Date getData2() {
		return data2;
	}

	public void setData2(Date data2) {
		this.data2 = data2;
	}

	public Date getData3() {
		return data3;
	}

	public void setData3(Date data3) {
		this.data3 = data3;
	}

	public String getNumeroAcordo() {
		return numeroAcordo;
	}

	public void setNumeroAcordo(String numeroAcordo) {
		this.numeroAcordo = numeroAcordo;
	}

	public String getNrDocumento() {
		return nrDocumento;
	}

	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	public DividaAtiva getDividaAtiva() {
		return dividaAtiva;
	}

	public void setDividaAtiva(DividaAtiva dividaAtiva) {
		this.dividaAtiva = dividaAtiva;
	}

	public String getIdentificadorGED() {
		return identificadorGED;
	}

	public void setIdentificadorGED(String identificadorGED) {
		this.identificadorGED = identificadorGED;
	}

	public Boolean getExcluido() {
		return excluido;
	}

	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}

	public Boolean getAssinado() {
		return assinado;
	}

	public void setAssinado(Boolean assinado) {
		this.assinado = assinado;
	}

	public byte[] getArquivo() {
		return arquivo;
	}

	public void setArquivo(byte[] arquivo) {
		this.arquivo = arquivo;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public List<Corresponsavel> getCorresponsaveis() {
		return corresponsaveis;
	}

	public void setCorresponsaveis(List<Corresponsavel> corresponsaveis) {
		this.corresponsaveis = corresponsaveis;
	}

	public List<AssinanteDocumentoDivida> getAssinantes() {
		return assinantes;
	}

	public void setAssinantes(List<AssinanteDocumentoDivida> assinantes) {
		this.assinantes = assinantes;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getTermoInscricao() {
		return termoInscricao;
	}

	public void setTermoInscricao(String termoInscricao) {
		this.termoInscricao = termoInscricao;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getListSpu() {
		return listSpu;
	}

	public void setListSpu(String listSpu) {
		this.listSpu = listSpu;
	}

	// TODO: Verificar a possibilidade de implementar o método clone.
	public void copy(DocumentoDivida documentoDivida) {
		this.setDataNotificacaoAutuado(documentoDivida.getDataNotificacaoAutuado());
		this.setQtdDias(documentoDivida.getQtdDias());
		this.setDataNotificacaoAposJulgamento(documentoDivida.getDataNotificacaoAposJulgamento());
		this.setNumeroAcordo(documentoDivida.getNumeroAcordo());
		this.setValor1(documentoDivida.getValor1());
		this.setData1(documentoDivida.getData1());
		this.setValor2(documentoDivida.getValor2());
		this.setData2(documentoDivida.getData2());
		this.setValor3(documentoDivida.getValor3());
		this.setValor4(documentoDivida.getValor4());
		this.setData3(documentoDivida.getData3());
		this.setHasJulgamento(documentoDivida.getHasJulgamento());
		this.setNumeroDecisao(documentoDivida.getNumeroDecisao());
		this.setDataFimPrazoNotificacao(documentoDivida.getDataFimPrazoNotificacao());
		this.setValorDecisao(documentoDivida.getValorDecisao());
		this.setHasAcordo(documentoDivida.getHasAcordo());
		this.setListSpu(documentoDivida.getListSpu());
	}

}
