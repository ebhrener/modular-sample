package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="base_cartografica")
public class BaseCartografica implements Serializable {

	private static final long serialVersionUID = -2300398497068651653L;

	@Id 
	@SequenceGenerator(sequenceName="seq_base_cartografica", name="seq_base_cartografica", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_base_cartografica")
	private Long id;
	
	@Column(nullable = false)
	private String nome;

	@Column(length=6, unique = true, nullable = false)
	private String cor;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="legenda_id")
	private Legenda legenda = new Legenda();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "baseCartografica", targetEntity = ShapeBaseCartografica.class)
	private List<ShapeBaseCartografica> shapeBaseCartografica = new ArrayList<ShapeBaseCartografica>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "baseCartografica", targetEntity = CamadaMapaTematico.class, cascade = CascadeType.ALL)
	private List<CamadaMapaTematico> camadaMapaTematico = new ArrayList<CamadaMapaTematico>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Legenda getLegenda() {
		return legenda;
	}

	public void setLegenda(Legenda legenda) {
		this.legenda = legenda;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseCartografica other = (BaseCartografica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public List<ShapeBaseCartografica> getShapeBaseCartografica() {
		return shapeBaseCartografica;
	}

	public void setShapeBaseCartografica(
			List<ShapeBaseCartografica> shapeBaseCartografica) {
		this.shapeBaseCartografica = shapeBaseCartografica;
	}

	public List<CamadaMapaTematico> getCamadaMapaTematico() {
		return camadaMapaTematico;
	}

	public void setCamadaMapaTematico(List<CamadaMapaTematico> camadaMapaTematico) {
		this.camadaMapaTematico = camadaMapaTematico;
	}
	
}