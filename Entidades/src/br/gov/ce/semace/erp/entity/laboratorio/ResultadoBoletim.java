package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import br.gov.ce.semace.erp.enuns.StatusResultadoBoletim;

@Entity
@Audited
@Table(name="resultado_boletim", schema="laboratorio")
public class ResultadoBoletim implements Serializable {
	
	private static final long serialVersionUID = -3089519098680170759L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_resultado_boletim", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "resul_analise_resul_boletim",
			schema = "laboratorio", 
			joinColumns = @JoinColumn(name = "resultado_boletim_id", nullable=false), 
			inverseJoinColumns = @JoinColumn(name = "resultado_analise_id", nullable=false))
	@NotAudited
	private List<ResultadoAnalise> resultadosAnalise = new ArrayList<ResultadoAnalise>();
	
	@Transient
	private ResultadoAnalise resultadoAnalise;
	
	@ManyToOne(fetch= FetchType.LAZY, optional = false)
	@JoinColumn(name="boletim_id")
	private Boletim boletim = new Boletim();
	
	@Column(name="justificativa_impropria")
	private String justificativaImpropria;
	
	@Column(name="status")
	private StatusResultadoBoletim status = StatusResultadoBoletim.PROPRIA;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boletim getBoletim() {
		return boletim;
	}

	public void setBoletim(Boletim boletim) {
		this.boletim = boletim;
	}

	public String getJustificativaImpropria() {
		return justificativaImpropria;
	}

	public void setJustificativaImpropria(String justificativaImpropria) {
		this.justificativaImpropria = justificativaImpropria;
	}

	public List<ResultadoAnalise> getResultadosAnalise() {
		return resultadosAnalise;
	}

	public void setResultadosAnalise(List<ResultadoAnalise> resultadoAnalise) {
		this.resultadosAnalise = resultadoAnalise;
	}

	public StatusResultadoBoletim getStatus() {
		return status;
	}

	public void setStatus(StatusResultadoBoletim status) {
		this.status = status;
	}

	public ResultadoAnalise getResultadoAnalise() {
		return resultadoAnalise;
	}

	public void setResultadoAnalise(ResultadoAnalise resultadoAnalise) {
		this.resultadoAnalise = resultadoAnalise;
	}

}
