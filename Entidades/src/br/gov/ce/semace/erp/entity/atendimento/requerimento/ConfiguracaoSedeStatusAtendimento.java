package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Sede;
import br.gov.ce.semace.erp.enuns.StatusAtendimento;

@Entity
@Audited
@Table(name="configuracao_sede_status_atendimento", schema="atendimento")
public class ConfiguracaoSedeStatusAtendimento implements Serializable {

	private static final long serialVersionUID = 7918036665959705063L;

	@Id
	@SequenceGenerator(sequenceName = "atendimento.configuracao_sede_status_atendimento_seq", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sede_base_id")
	private Sede sedeBase;

	@ElementCollection(targetClass = StatusAtendimento.class, fetch = FetchType.LAZY)
	@CollectionTable(name = "configuracao_atendimento_status_atendimento", schema = "atendimento",
	joinColumns = @JoinColumn(name = "configuracao_sede_status_atendimento_id"))
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "status_atendimento")
	private List<StatusAtendimento> listStatus = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sede_visualizada_id")
	private Sede sedeVisualizada;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Sede getSedeBase() {
		return sedeBase;
	}

	public void setSedeBase(Sede sedeBase) {
		this.sedeBase = sedeBase;
	}

	public List<StatusAtendimento> getListStatus() {
		return listStatus;
	}

	public void setListStatus(List<StatusAtendimento> listStatus) {
		this.listStatus = listStatus;
	}

	public Sede getSedeVisualizada() {
		return sedeVisualizada;
	}

	public void setSedeVisualizada(Sede sedeVisualizada) {
		this.sedeVisualizada = sedeVisualizada;
	}

}
