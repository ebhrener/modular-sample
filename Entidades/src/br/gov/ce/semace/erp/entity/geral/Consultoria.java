package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;


@Entity
@Audited
@Table(name="consultoria", schema = "geral")
public class Consultoria implements Serializable{
	
	private static final long serialVersionUID = 6155654272243693917L;


	@Id @Column(name="consultoria_id")
	@SequenceGenerator(sequenceName="geral.seq_consultoria", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	@Index(name="consultoria_pk_i1")
	private Long id;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="interessado_id")
	private Interessado interessado = new Interessado(); 
		
	@OneToMany(mappedBy = "consultoria", fetch = FetchType.LAZY)
	private List<ConsultoriaInteressado> interessados = new ArrayList<ConsultoriaInteressado>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Interessado getInteressado() {
		return interessado;
	}

	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}

	public List<ConsultoriaInteressado> getInteressados() {
		return interessados;
	}

	public void setInteressados(List<ConsultoriaInteressado> interessados) {
		this.interessados = interessados;
	}
}
