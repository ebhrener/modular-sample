package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoJustificativaRegistroPonto;

@Entity
@Audited
@Table(schema="rh",name="justificativa_registro_ponto")
public class JustificativaRegistroPonto implements Serializable{

	private static final long serialVersionUID = 4537611001002298871L;

	@Id
	@SequenceGenerator(sequenceName = "rh.seq_justificativa_registro_ponto", name = "seq_justificativa_registro_ponto", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_justificativa_registro_ponto")
	private Long id;

	@Column(name = "tipo_justificativa")
	private TipoJustificativaRegistroPonto tipoJustificativaRegistroPonto;

	@Column(name = "justificativa")
	private String justificativa;

	@ManyToOne(optional = false)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;

	@Column(name="data_hora_justificativa")
	private Date dataHoraJustificativa;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="registro_ponto_id")
	private RegistroPonto registroPonto;

	public JustificativaRegistroPonto() {
		super();
	}

	public JustificativaRegistroPonto(
			TipoJustificativaRegistroPonto tipoJustificativaRegistroPonto,
			String justificativa) {
		super();
		this.tipoJustificativaRegistroPonto = tipoJustificativaRegistroPonto;
		this.justificativa = justificativa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoJustificativaRegistroPonto getTipoJustificativaRegistroPonto() {
		return tipoJustificativaRegistroPonto;
	}

	public void setTipoJustificativaRegistroPonto(
			TipoJustificativaRegistroPonto tipoJustificativaRegistroPonto) {
		this.tipoJustificativaRegistroPonto = tipoJustificativaRegistroPonto;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Date getDataHoraJustificativa() {
		return dataHoraJustificativa;
	}

	public void setDataHoraJustificativa(Date dataHoraJustificativa) {
		this.dataHoraJustificativa = dataHoraJustificativa;
	}

	public RegistroPonto getRegistroPonto() {
		return registroPonto;
	}

	public void setRegistroPonto(RegistroPonto registroPonto) {
		this.registroPonto = registroPonto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		JustificativaRegistroPonto other = (JustificativaRegistroPonto) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "JustificativaRegistroPonto [id=" + id
				+ ", tipoJustificativaRegistroPonto="
				+ tipoJustificativaRegistroPonto + ", justificativa="
				+ justificativa + ", funcionario=" + funcionario
				+ ", dataHoraJustificativa=" + dataHoraJustificativa
				+ ", registroPonto=" + registroPonto.getId() + "]";
	}


}
