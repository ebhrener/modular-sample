package br.gov.ce.semace.erp.entity.produtividade;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.enuns.StatusOcorrencia;

@Entity
@Audited
@Table(name = "resposta_atividade", schema = "produtividade")
public class RespostaAtividade implements Serializable{

	private static final long serialVersionUID = 5506332961148810568L;
	
	@Id @SequenceGenerator(sequenceName="produtividade.seq_resposta_atividade", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_produtividade_id")
	private AtividadeProdutividade atividade = new AtividadeProdutividade();
	
	@Column(name = "nome_atividade")
	private String nomeAtividade;
	
	@Column(name = "numero_documento_resposta")
	private String numeroDocumentoResposta;
	
	@Column(name = "descricao_resposta", columnDefinition="text")
	private String descricaoResposta;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_resposta")
	private Date dataResposta;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "produtividade_id")
	private Produtividade produtividade;
	
	@Column(name = "status_ocorrencia")
	private StatusOcorrencia statusOcorrencia;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_id")
	private Atividade atividadeCoema = new Atividade();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "peso_setor_id")
	private PesoSetor pesoSetor = new PesoSetor();
	
	private String spu;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AtividadeProdutividade getAtividade() {
		return atividade;
	}

	public void setAtividade(AtividadeProdutividade atividade) {
		this.atividade = atividade;
	}

	public String getDescricaoResposta() {
		return descricaoResposta;
	}

	public void setDescricaoResposta(String descricaoResposta) {
		this.descricaoResposta = descricaoResposta;
	}

	public Date getDataResposta() {
		return dataResposta;
	}

	public void setDataResposta(Date dataResposta) {
		this.dataResposta = dataResposta;
	}

	public Produtividade getProdutividade() {
		return produtividade;
	}

	public void setProdutividade(Produtividade produtividade) {
		this.produtividade = produtividade;
	}

	public String getNumeroDocumentoResposta() {
		return numeroDocumentoResposta;
	}

	public void setNumeroDocumentoResposta(String numeroDocumentoResposta) {
		this.numeroDocumentoResposta = numeroDocumentoResposta;
	}
	
	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public String getNomeAtividade() {
		return nomeAtividade;
	}

	public void setNomeAtividade(String nomeAtividade) {
		this.nomeAtividade = nomeAtividade;
	}

	public StatusOcorrencia getStatusOcorrencia() {
		return statusOcorrencia;
	}

	public void setStatusOcorrencia(StatusOcorrencia statusOcorrencia) {
		this.statusOcorrencia = statusOcorrencia;
	}

	public Atividade getAtividadeCoema() {
		return atividadeCoema;
	}

	public void setAtividadeCoema(Atividade atividadeCoema) {
		this.atividadeCoema = atividadeCoema;
	}

	public PesoSetor getPesoSetor() {
		return pesoSetor;
	}

	public void setPesoSetor(PesoSetor pesoSetor) {
		this.pesoSetor = pesoSetor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((atividade == null) ? 0 : atividade.hashCode());
		result = prime * result
				+ ((dataResposta == null) ? 0 : dataResposta.hashCode());
		result = prime
				* result
				+ ((descricaoResposta == null) ? 0 : descricaoResposta
						.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((numeroDocumentoResposta == null) ? 0
						: numeroDocumentoResposta.hashCode());
		result = prime * result
				+ ((produtividade == null) ? 0 : produtividade.hashCode());
		result = prime
				* result
				+ ((statusOcorrencia == null) ? 0
						: statusOcorrencia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		RespostaAtividade other = (RespostaAtividade) obj;
		if(other.getId() != null && this.getId() != null){
			return getId().equals(other.getId());
		} 
			
		return other.getDescricaoResposta().equals(getDescricaoResposta());
	}
}
