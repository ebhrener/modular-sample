package br.gov.ce.semace.erp.entity.atendimento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "cartao_visita", schema = "atendimento")
@Audited
public class CartaoVisita implements Serializable {

	private static final long serialVersionUID = 4585591401193910732L;

	@Id
	@SequenceGenerator(sequenceName = "atendimento.seq_cartao_visita", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(nullable=false)
	private String numero;

	@Column(columnDefinition = "boolean default true", nullable = false)
	private Boolean ativo = Boolean.TRUE;

	public String getDisplayAtivo() {
		return ativo == null ? "" : ativo ? "Sim" : "Não";
	}

	/***************************************** GET's And SET's ***************************************************/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CartaoVisita other = (CartaoVisita) obj;
		if (numero == null) {
			if (other.numero != null) {
				return false;
			}
		} else if (!numero.equals(other.numero)) {
			return false;
		}
		return true;
	}

}
