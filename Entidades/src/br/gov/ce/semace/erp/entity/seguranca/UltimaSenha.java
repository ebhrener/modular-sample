package br.gov.ce.semace.erp.entity.seguranca;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="ultima_senha", schema="seguranca")
public class UltimaSenha implements Serializable{

	private static final long serialVersionUID = -8349910473812964260L;
	
	@Id
	@SequenceGenerator(sequenceName = "seguranca.seq_ultima_senha", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;
	
	private String senha;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_inclusao_senha")
	private Date dataInclusaoSenha;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="usuario_id")
	private Usuario usuario = new Usuario();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Date getDataInclusaoSenha() {
		return dataInclusaoSenha;
	}

	public void setDataInclusaoSenha(Date dataInclusaoSenha) {
		this.dataInclusaoSenha = dataInclusaoSenha;
	}

}
