package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import br.gov.ce.semace.erp.entity.rh.Funcionario;

/**
 * Entidade de Mapeamento da tabela 'resultado_analise'.
 * 
 * @author Joerlan Oliveira Lima - joerlan.lima@semace.ce.gov.br - 17/10/2013
 *
 */
@Entity
@Audited
@Table(name="resultado_analise", schema="laboratorio")
public class ResultadoAnalise implements Serializable{
	
	private static final long serialVersionUID = -6222173282638958552L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_resultado_analise", name="seq_resultado_analise", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_resultado_analise")
	private Long id;
	
	@ManyToOne(optional=false,fetch=FetchType.LAZY)
	@JoinColumn(name="analise_id")
	private Analise analise;

	@OneToOne(optional=false,fetch=FetchType.LAZY)
	@JoinColumn(name="coleta_id")
	private Coleta coleta;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="volume_diluicao_id")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private VolumeDiluicao volumeDiluicao;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="tabela_nmp_id")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private TabelaNMP tabelaNMP;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fator_impedimento_id")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private FatorImpedimento fatorImpedimento;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_realizada")
	private Date dataRealizada;
	
	@Column(nullable=false)
	private Boolean media = Boolean.FALSE;
	
	private Float valor;
	
	private String justificativa;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy="resultadosAnalise")
	private List<ResultadoBoletim> resultadoBoletim = new ArrayList<>();

	public ResultadoAnalise() {
		super();
	}
	
	public ResultadoAnalise(Long id) {
		this.id = id;
	}
	
	public Analise getAnalise() {
		return analise;
	}

	public void setAnalise(Analise analise) {
		this.analise = analise;
	}

	public Coleta getColeta() {
		return coleta;
	}

	public void setColeta(Coleta coleta) {
		this.coleta = coleta;
	}

	public VolumeDiluicao getVolumeDiluicao() {
		return volumeDiluicao;
	}

	public void setVolumeDiluicao(VolumeDiluicao volumeDiluicao) {
		this.volumeDiluicao = volumeDiluicao;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TabelaNMP getTabelaNMP() {
		return tabelaNMP;
	}

	public void setTabelaNMP(TabelaNMP tabelaNMP) {
		this.tabelaNMP = tabelaNMP;
	}

	public FatorImpedimento getFatorImpedimento() {
		return fatorImpedimento;
	}

	public void setFatorImpedimento(FatorImpedimento fatorImpedimento) {
		this.fatorImpedimento = fatorImpedimento;
	}

	public Date getDataRealizada() {
		return dataRealizada;
	}

	public void setDataRealizada(Date dataRealizada) {
		this.dataRealizada = dataRealizada;
	}

	public Boolean getMedia() {
		return media;
	}

	public void setMedia(Boolean media) {
		this.media = media;
	}
	
	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	public List<ResultadoBoletim> getResultadoBoletim() {
		return resultadoBoletim;
	}

	public void setResultadoBoletim(List<ResultadoBoletim> resultadoBoletim) {
		this.resultadoBoletim = resultadoBoletim;
	}

}
