package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe usada para transportar todos os valores usados no calculo;
 * 
 * @author Joerlan Lima - joerlan.lima@semace.ce.gov.br - 16/07/2014
 *
 */
public class CalculoAtualizacaoMonetaria implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 6644475720287970319L;
	
	private Date dataInicialCorrecao;
	private Date dataFinalCorrecao;
	private Date dataInicialJuros;
	private Date dataFinalJuros;
	private BigDecimal valorPrincipal = new BigDecimal("0.00");
	private BigDecimal valorAtualizado = new BigDecimal("0.00");
	private BigDecimal valorAtualizacao = new BigDecimal("0.00");
	private BigDecimal valorJurosEmPercentagem = new BigDecimal("0.00");
	private BigDecimal valorJurosMensalEmPercentagem = new BigDecimal("0.00");
	private BigDecimal valorJuros = new BigDecimal("0.00");
	private BigDecimal valorMultaEmPercentagem = new BigDecimal("0.00");
	private BigDecimal valorMulta = new BigDecimal("0.00");
	private BigDecimal valorTotal = new BigDecimal("0.00");
	private List<?> indices = new ArrayList<>();
	
	public BigDecimal getValorPrincipal() {
		return valorPrincipal;
	}
	public void setValorPrincipal(BigDecimal valorPrincipal) {
		this.valorPrincipal = valorPrincipal;
	}
	public BigDecimal getValorAtualizado() {
		return valorAtualizado;
	}
	public void setValorAtualizado(BigDecimal valorAtualizado) {
		this.valorAtualizado = valorAtualizado;
	}
	public BigDecimal getValorAtualizacao() {
		return valorAtualizacao;
	}
	public void setValorAtualizacao(BigDecimal valorAtualizacao) {
		this.valorAtualizacao = valorAtualizacao;
	}
	public BigDecimal getValorJurosEmPercentagem() {
		return valorJurosEmPercentagem;
	}
	public void setValorJurosEmPercentagem(BigDecimal valorJurosEmPercentagem) {
		this.valorJurosEmPercentagem = valorJurosEmPercentagem;
	}
	public BigDecimal getValorJuros() {
		return valorJuros;
	}
	public void setValorJuros(BigDecimal valorJuros) {
		this.valorJuros = valorJuros;
	}
	public BigDecimal getValorMultaEmPercentagem() {
		return valorMultaEmPercentagem;
	}
	public void setValorMultaEmPercentagem(BigDecimal valorMultaEmPercentagem) {
		this.valorMultaEmPercentagem = valorMultaEmPercentagem;
	}
	public BigDecimal getValorMulta() {
		return valorMulta;
	}
	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}
	public BigDecimal getValorJurosMensalEmPercentagem() {
		return valorJurosMensalEmPercentagem;
	}
	public void setValorJurosMensalEmPercentagem(
			BigDecimal valorJurosMensalEmPercentagem) {
		this.valorJurosMensalEmPercentagem = valorJurosMensalEmPercentagem;
	}
	public BigDecimal getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}
	public Date getDataInicialCorrecao() {
		return dataInicialCorrecao;
	}
	public void setDataInicialCorrecao(Date dataInicialCorrecao) {
		this.dataInicialCorrecao = dataInicialCorrecao;
	}
	public Date getDataFinalCorrecao() {
		return dataFinalCorrecao;
	}
	public void setDataFinalCorrecao(Date dataFinalCorrecao) {
		this.dataFinalCorrecao = dataFinalCorrecao;
	}
	public Date getDataInicialJuros() {
		return dataInicialJuros;
	}
	public void setDataInicialJuros(Date dataInicialJuros) {
		this.dataInicialJuros = dataInicialJuros;
	}
	public Date getDataFinalJuros() {
		return dataFinalJuros;
	}
	public void setDataFinalJuros(Date dataFinalJuros) {
		this.dataFinalJuros = dataFinalJuros;
	}
	public List<?> getIndices() {
		return indices;
	}
	public void setIndices(List<?> indices) {
		this.indices = indices;
	}
	
}