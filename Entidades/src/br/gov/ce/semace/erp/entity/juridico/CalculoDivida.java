package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.IndiceAtualizacaoMonetaria;

@Entity
@Audited
@Table(name="calculo_divida", schema="juridico")
public class CalculoDivida implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 4934625429854638276L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_calculo_divida", name="juridico.seq_calculo_divida", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_calculo_divida")
	private Long id;

	@OneToOne(optional=false, cascade=CascadeType.ALL)
	@JoinColumn(name="historico_divida_id")
	private HistoricoDivida historicoDivida;

	@Column(name="data_inicio_correcao", nullable=false)
	private Date dataInicioCorrecao;

	@Column(name="data_inicio_juros", nullable=false)
	private Date dataInicioJuros;

	@Column(name="data_fim", nullable=false)
	private Date dataFim;

	@Column(scale=5, precision=12)
	private BigDecimal juros;

	@Column(scale=5, precision=12)
	private BigDecimal multa;

	@Column(scale=2, precision=12, name="juros_real")
	private BigDecimal jurosReal;

	@Column(scale=2, precision=12, name="multa_real")
	private BigDecimal multaReal;

	@Column(name="valor_base", nullable=false, scale=2, precision=12)
	private BigDecimal valorBase;

	@Column(name="valor_atualizado", nullable=false, scale=2, precision=12)
	private BigDecimal valorAtualizado;

	@Column(name="valor_resultado", nullable=false, scale=2, precision=12)
	private BigDecimal valorResultado;

	@Column(scale=5, precision=12)
	private BigDecimal honorarios;

	@Column(name="abatimento_parcelas", scale=2, precision=12)
	private BigDecimal abatimentoParcelas;

	@Enumerated(EnumType.ORDINAL)
	@Column(nullable=false, name= "indice_atualizacao_monetaria")
	private IndiceAtualizacaoMonetaria indiceAtualizacaoMonetaria;

	@Transient
	public BigDecimal computeHonorariosReal() {
		if(honorarios == null || honorarios.compareTo(BigDecimal.ZERO) == 0) {
			return BigDecimal.ZERO;
		}
		return valorAtualizado.add(multaReal).add(jurosReal).multiply(honorarios.divide(new BigDecimal("100.00"))).setScale(2, RoundingMode.UP);
	}

	//------------------------------------------------Gets e Setters---------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public HistoricoDivida getHistoricoDivida() {
		return historicoDivida;
	}

	public void setHistoricoDivida(HistoricoDivida historicoDivida) {
		this.historicoDivida = historicoDivida;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public BigDecimal getJuros() {
		return juros;
	}

	public void setJuros(BigDecimal juros) {
		this.juros = juros;
	}

	public BigDecimal getMulta() {
		return multa;
	}

	public void setMulta(BigDecimal multa) {
		this.multa = multa;
	}

	public BigDecimal getValorBase() {
		return valorBase;
	}

	public void setValorBase(BigDecimal valorBase) {
		this.valorBase = valorBase;
	}

	public BigDecimal getValorAtualizado() {
		return valorAtualizado;
	}

	public void setValorAtualizado(BigDecimal valorAtualizado) {
		this.valorAtualizado = valorAtualizado;
	}

	public BigDecimal getValorResultado() {
		return valorResultado;
	}

	public void setValorResultado(BigDecimal valorResultado) {
		this.valorResultado = valorResultado;
	}

	public BigDecimal getHonorarios() {
		return honorarios;
	}

	public void setHonorarios(BigDecimal honorarios) {
		this.honorarios = honorarios;
	}

	public BigDecimal getAbatimentoParcelas() {
		return abatimentoParcelas;
	}

	public void setAbatimentoParcelas(BigDecimal abatimentoParcelas) {
		this.abatimentoParcelas = abatimentoParcelas;
	}

	public BigDecimal getJurosReal() {
		return jurosReal;
	}

	public void setJurosReal(BigDecimal jurosReal) {
		this.jurosReal = jurosReal;
	}

	public BigDecimal getMultaReal() {
		return multaReal;
	}

	public void setMultaReal(BigDecimal multaReal) {
		this.multaReal = multaReal;
	}

	public Date getDataInicioCorrecao() {
		return dataInicioCorrecao;
	}

	public void setDataInicioCorrecao(Date dataInicioCorrecao) {
		this.dataInicioCorrecao = dataInicioCorrecao;
	}

	public Date getDataInicioJuros() {
		return dataInicioJuros;
	}

	public void setDataInicioJuros(Date dataInicioJuros) {
		this.dataInicioJuros = dataInicioJuros;
	}

	public BigDecimal getSaldoAbatimentoParcelas() {
		return valorBase.subtract(abatimentoParcelas);
	}

	public IndiceAtualizacaoMonetaria getIndiceAtualizacaoMonetaria() {
		return indiceAtualizacaoMonetaria;
	}

	public void setIndiceAtualizacaoMonetaria(IndiceAtualizacaoMonetaria indiceAtualizacaoMonetaria) {
		this.indiceAtualizacaoMonetaria = indiceAtualizacaoMonetaria;
	}

}
