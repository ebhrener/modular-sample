package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;

@Entity
@Audited
@Table(schema = "licenciamento", name = "funcionario_notificacao_processo")
public class FuncionarioNotificacaoProcesso implements Serializable {

	private static final long serialVersionUID = 2621526758344839758L;

	@Id
	@SequenceGenerator(sequenceName = "licenciamento.seq_funcionario_notificacao_processo", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcionario_id", referencedColumnName = "pessoa_id", nullable = false)
	private Funcionario funcionario = new Funcionario();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "configuracao_tipo_processo_id", referencedColumnName = "id", nullable = false)
	private ConfiguracaoTipoProcesso configuracaoTipoProcesso;

	public FuncionarioNotificacaoProcesso() {
		super();
	}

	public FuncionarioNotificacaoProcesso(Long id) {
		super();
		this.id = id;
	}

	public FuncionarioNotificacaoProcesso(Funcionario funcionario, ConfiguracaoTipoProcesso configuracaoTipoProcesso) {
		super();
		this.funcionario = funcionario;
		this.configuracaoTipoProcesso = configuracaoTipoProcesso;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public ConfiguracaoTipoProcesso getConfiguracaoTipoProcesso() {
		return configuracaoTipoProcesso;
	}

	public void setConfiguracaoTipoProcesso(ConfiguracaoTipoProcesso configuracaoTipoProcesso) {
		this.configuracaoTipoProcesso = configuracaoTipoProcesso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((configuracaoTipoProcesso == null) ? 0 : configuracaoTipoProcesso.hashCode());
		result = prime * result + ((funcionario == null) ? 0 : funcionario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FuncionarioNotificacaoProcesso other = (FuncionarioNotificacaoProcesso) obj;
		if (configuracaoTipoProcesso == null) {
			if (other.configuracaoTipoProcesso != null) {
				return false;
			}
		} else if (!configuracaoTipoProcesso.equals(other.configuracaoTipoProcesso)) {
			return false;
		}
		if (funcionario == null) {
			if (other.funcionario != null) {
				return false;
			}
		} else if (!funcionario.equals(other.funcionario)) {
			return false;
		}
		return true;
	}

}
