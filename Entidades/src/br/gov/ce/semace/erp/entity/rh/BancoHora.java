package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;


@Entity
@Table(schema="rh", name="banco_hora")
@Audited
//@TypeDef(name="period", typeClass= JodaTypePeriodParser.class)
public class BancoHora implements Serializable{

	private static final long serialVersionUID = 1179653022271746263L;

	@Id
	@SequenceGenerator(sequenceName = "rh.seq_banco_hora", name = "seq_banco_hora", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_banco_hora")
	private long id;

	@ManyToOne(optional=false)
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario;

//	@Column(name="horas_disponiveis")
//	@Type(type="period")
//	private Period horas;

	@Column(name="mes_referente")
	private int mesReferente;

	@Column(name="ano_referente")
	private int anoReferente;

	@Column(name="data_contabilizacao")
	private Date dataContabilizacao;

	public BancoHora(Funcionario funcionario,
//			Period horas,
			int mesReferente, int anoReferente, Date dataContabilizacao) {
		super();
		this.funcionario = funcionario;
//		this.horas = horas;
		this.mesReferente = mesReferente;
		this.anoReferente = anoReferente;
		this.dataContabilizacao = dataContabilizacao;
	}

	public BancoHora() {
		super();
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
//	public Period getHoras() {
//		return horas;
//	}
//	public void setHoras(Period horas) {
//		this.horas = horas;
//	}
	public int getMesReferente() {
		return mesReferente;
	}
	public void setMesReferente(int mesReferente) {
		this.mesReferente = mesReferente;
	}
	public int getAnoReferente() {
		return anoReferente;
	}
	public void setAnoReferente(int anoReferente) {
		this.anoReferente = anoReferente;
	}
	public Date getDataContabilizacao() {
		return dataContabilizacao;
	}
	public void setDataContabilizacao(Date dataContabilizacao) {
		this.dataContabilizacao = dataContabilizacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BancoHora other = (BancoHora) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

}
