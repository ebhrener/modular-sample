package br.gov.ce.semace.erp.entity.atendimento.acompanhamento;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

public class Oficio {

	private static final long serialVersionUID = -7032097009509637727L;

	@Id
	@Column(name = "oficio_id")
	private Integer oficioId;

	@Column(name = "ano")
	private Integer ano;

	@Column(name = "oficio_coema_id")
	private Integer oficioCoemaId;

	@Column(name = "processo_vinc_id")
	private String processoVincId;

	@Column(name = "numero")
	private String numero;

	@Column(name = "area_id")
	private Integer areaId;

	@Column(name = "data_oficio")
	private String dataOficio;

	@Transient
	private String dataOficioExtenso;

	@Column(name = "tratamento_oficio_id")
	private Integer tratamentoOficioId;

	@Column(name = "texto")
	private String texto;

	@Column(name = "cargo_usuario")
	private String cargoUsuario;

	@Column(name = "responsavel_id")
	private Integer responsavelId;

	@Column(name = "usuario_assinou_id")
	private Integer usuarioAssinouId;

	@Column(name = "usuario_acordo_id")
	private Integer usuarioAcordoId;

	@Column(name = "cargo_assina")
	private String cargoAssina;

	@Column(name = "coema")
	private Boolean coema;

	@Column(name = "coema_cont")
	private Integer coemaCont;

	@Column(name = "pendente")
	private Boolean pendente;

	@Column(name = "atendido")
	@Temporal(TemporalType.DATE)
	private Boolean atendido;

	@Transient
	private String tratamentoOficio;

	@Transient
	private String nomeDestinatario;

	@Transient
	private String nomeUsuarioAcordo;

	@Transient
	private String nomeUsuarioAssinou;

	@Transient
	private String tratamentoDestinatario;

	@Transient
	private String cargoDestinatario;

	@Transient
	private String orgaoDestinatario;

	@Transient
	private String enderecoDestinatario;

	@Transient
	private String numeroProcesso;

	@Transient
	private String nomeArea;

	@Transient
	private String area;

	public Integer getOficioId() {
		return oficioId;
	}

	public void setOficioId(Integer oficioId) {
		this.oficioId = oficioId;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getOficioCoemaId() {
		return oficioCoemaId;
	}

	public void setOficioCoemaId(Integer oficioCoemaId) {
		this.oficioCoemaId = oficioCoemaId;
	}

	public String getProcessoVincId() {
		return processoVincId;
	}

	public void setProcessoVincId(String processoVincId) {
		this.processoVincId = processoVincId;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public String getDataOficio() {
		return dataOficio;
	}

	public void setDataOficio(String dataOficio) {
		this.dataOficio = dataOficio;
	}

	public String getDataOficioExtenso() {
		return dataOficioExtenso;
	}

	public void setDataOficioExtenso(String dataOficioExtenso) {
		this.dataOficioExtenso = dataOficioExtenso;
	}

	public Integer getTratamentoOficioId() {
		return tratamentoOficioId;
	}

	public void setTratamentoOficioId(Integer tratamentoOficioId) {
		this.tratamentoOficioId = tratamentoOficioId;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getCargoUsuario() {
		return cargoUsuario;
	}

	public void setCargoUsuario(String cargoUsuario) {
		this.cargoUsuario = cargoUsuario;
	}

	public Integer getResponsavelId() {
		return responsavelId;
	}

	public void setResponsavelId(Integer responsavelId) {
		this.responsavelId = responsavelId;
	}

	public Integer getUsuarioAssinouId() {
		return usuarioAssinouId;
	}

	public void setUsuarioAssinouId(Integer usuarioAssinouId) {
		this.usuarioAssinouId = usuarioAssinouId;
	}

	public Integer getUsuarioAcordoId() {
		return usuarioAcordoId;
	}

	public void setUsuarioAcordoId(Integer usuarioAcordoId) {
		this.usuarioAcordoId = usuarioAcordoId;
	}

	public String getCargoAssina() {
		return cargoAssina;
	}

	public void setCargoAssina(String cargoAssina) {
		this.cargoAssina = cargoAssina;
	}

	public Boolean getCoema() {
		return coema;
	}

	public void setCoema(Boolean coema) {
		this.coema = coema;
	}

	public Integer getCoemaCont() {
		return coemaCont;
	}

	public void setCoemaCont(Integer coemaCont) {
		this.coemaCont = coemaCont;
	}

	public Boolean getPendente() {
		return pendente;
	}

	public void setPendente(Boolean pendente) {
		this.pendente = pendente;
	}

	public Boolean getAtendido() {
		return atendido;
	}

	public void setAtendido(Boolean atendido) {
		this.atendido = atendido;
	}

	public String getTratamentoOficio() {
		return tratamentoOficio;
	}

	public void setTratamentoOficio(String tratamentoOficio) {
		this.tratamentoOficio = tratamentoOficio;
	}

	public String getNomeUsuarioAssinou() {
		return nomeUsuarioAssinou;
	}

	public void setNomeUsuarioAssinou(String nomeUsuarioAssinou) {
		this.nomeUsuarioAssinou = nomeUsuarioAssinou;
	}

	public String getNomeDestinatario() {
		return nomeDestinatario;
	}

	public void setNomeDestinatario(String nomeDestinatario) {
		this.nomeDestinatario = nomeDestinatario;
	}

	public String getNomeUsuarioAcordo() {
		return nomeUsuarioAcordo;
	}

	public void setNomeUsuarioAcordo(String nomeUsuarioAcordo) {
		this.nomeUsuarioAcordo = nomeUsuarioAcordo;
	}

	public String getTratamentoDestinatario() {
		return tratamentoDestinatario;
	}

	public void setTratamentoDestinatario(String tratamentoDestinatario) {
		this.tratamentoDestinatario = tratamentoDestinatario;
	}

	public String getCargoDestinatario() {
		return cargoDestinatario;
	}

	public void setCargoDestinatario(String cargoDestinatario) {
		this.cargoDestinatario = cargoDestinatario;
	}

	public String getOrgaoDestinatario() {
		return orgaoDestinatario;
	}

	public void setOrgaoDestinatario(String orgaoDestinatario) {
		this.orgaoDestinatario = orgaoDestinatario;
	}

	public String getEnderecoDestinatario() {
		return enderecoDestinatario;
	}

	public void setEnderecoDestinatario(String enderecoDestinatario) {
		this.enderecoDestinatario = enderecoDestinatario;
	}

	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getNomeArea() {
		return nomeArea;
	}

	public void setNomeArea(String nomeArea) {
		this.nomeArea = nomeArea;
	}

}
