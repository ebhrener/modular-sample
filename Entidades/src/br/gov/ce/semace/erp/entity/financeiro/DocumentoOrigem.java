package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "financeiro", name = "documento_origem", uniqueConstraints=@UniqueConstraint(columnNames={"numero_documento_pai"}))
public class DocumentoOrigem implements Serializable {
	
	private static final long serialVersionUID = 1994150541887643963L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_documento_origem", name = "SEQ_DOCUMENTO_ORIGEM", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DOCUMENTO_ORIGEM")
	private Long id;
	
	
	@Column(name = "numero_documento_pai")
	private String numeroDocumentoPai;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroDocumentoPai() {
		return numeroDocumentoPai;
	}

	public void setNumeroDocumentoPai(String numeroDocumentoPai) {
		this.numeroDocumentoPai = numeroDocumentoPai;
	}

}
