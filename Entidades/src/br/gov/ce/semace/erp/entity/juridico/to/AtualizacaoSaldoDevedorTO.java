package br.gov.ce.semace.erp.entity.juridico.to;

import java.util.Calendar;
import java.util.Date;

import br.gov.ce.semace.erp.entity.financeiro.Dae;
import br.gov.ce.semace.erp.entity.juridico.Selic;

public class AtualizacaoSaldoDevedorTO {

	private Dae dae;
	private Selic selic;

	public Dae getDae() {
		return dae;
	}
	public void setDae(Dae dae) {
		this.dae = dae;
	}
	public Selic getSelic() {
		return selic;
	}
	public void setSelic(Selic selic) {
		this.selic = selic;
	}

	public Date getTime() {
		if (selic != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(selic.getData());
			cal.add(Calendar.MONTH, 1);
			return cal.getTime();
		}
		return dae.getDataPagamento();
	}

}
