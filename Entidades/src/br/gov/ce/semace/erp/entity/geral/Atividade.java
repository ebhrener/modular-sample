package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.licenciamento.PotencialPoluidor;
import br.gov.ce.semace.erp.enuns.TipoPotencialPoluidor;


@Entity
@Audited
@Table(name="atividade", schema="geral")
public class Atividade implements Comparable<Atividade>, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7227566877966653216L;

	@Id
	@SequenceGenerator(sequenceName="geral.seq_atividade", name="SEQATV", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQATV")
	private Long id;

	@NotNull
	@OrderBy
	@Column(nullable=false)
	private String codigo;

	@NotNull
	@Column(nullable=false)
	private String descricao;

	@Column(columnDefinition="text")
	private String observacao;

	@Column(name = "autorizacao_aa")
	private Boolean autorizacaoAA;

	@Column(name="potencial_poluidor", length=6)
	private String potencialPoluidor;

	@Column(name="use_par_coordenadas")
	private Boolean useParCoordenadas;


	@Column(name = "exige_empreendimento")
	private Boolean exigeEmpreendimento;

	@Column(name = "possui_ponto_corte")
	private Boolean possuiPontoCorte;

	private Boolean ativo = true;

	@Column(name="exige_apenas_lo")
	private Boolean exigeApenasLO = false;

	@Column(name="exige_numero_car_zona_rural")
	private Boolean exigeNumeroCarZonaRural = false;

	@Column(name="exige_numero_car_zona_urbana")
	private Boolean exigeNumeroCarZonaUrbana = false;

	@Column(name="exige_car")
	private Boolean exigeCar = false;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ppd_id")
	private PotencialPoluidor ppd;

	@Transient
	private boolean podeSelecionar = true;

	/**
	 * Campo somente para filtros
	 * @author erick.silva		ERICK BHRENER BARROSO SILVA		[erick.bhb@gmail.com]		21/06/2018
	 */
	@Transient
	private Coema coemaSearch;
	/**
	 * Retorna informação a ser exibida em uma tabela ou detalhes.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/12/2013 18:47:09
	 *
	 * @return
	 * 		Retorna <code>Não informado</code> caso o campo for nulo ou vazio. Caso contrário, retorna o valor do campo
	 * {@link #potencialPoluidor}.
	 */
	@Transient
	public String getDetalhePotencialPoluidor(){
		if(potencialPoluidor == null) {
			return "Não informado";
		}

		String descricaoPotencialPoluidor = TipoPotencialPoluidor.getDescricaoBySigla(this.potencialPoluidor.charAt(0));

		if (descricaoPotencialPoluidor == null || descricaoPotencialPoluidor.trim().isEmpty()){
			return "Não informado";
		}

		return descricaoPotencialPoluidor;
	}

	//---------------------------------------------Relacionamentos---------------------------------------------

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="grupo_atividade_id", referencedColumnName="id")
	private GrupoAtividade grupoAtividade = new GrupoAtividade();

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ponto_corte_id")
	private PontoCorte pontoCorte;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "atividade")
	private List<EmpreendimentoAtividade> empreendimentos = new ArrayList<>();


	//---------------------------------------------GETS e SETTERS----------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getPotencialPoluidor() {
		return potencialPoluidor;
	}

	public void setPotencialPoluidor(String potencialPoluidor) {
		this.potencialPoluidor = potencialPoluidor;
	}

	public GrupoAtividade getGrupoAtividade() {
		return grupoAtividade;
	}

	public void setGrupoAtividade(GrupoAtividade grupoAtividade) {
		this.grupoAtividade = grupoAtividade;
	}

	public Boolean getAutorizacaoAA() {
		return autorizacaoAA;
	}

	public void setAutorizacaoAA(Boolean autorizacaoAA) {
		this.autorizacaoAA = autorizacaoAA;
	}

	public Boolean getUseParCoordenadas() {
		return useParCoordenadas;
	}

	public void setUseParCoordenadas(Boolean useParCoordenadas) {
		this.useParCoordenadas = useParCoordenadas;
	}

	public Boolean getExigeEmpreendimento() {
		return exigeEmpreendimento;
	}

	public void setExigeEmpreendimento(Boolean exigeEmpreendimento) {
		this.exigeEmpreendimento = exigeEmpreendimento;
	}

	public Boolean getPossuiPontoCorte() {
		return possuiPontoCorte;
	}

	public void setPossuiPontoCorte(Boolean possuiPontoCorte) {
		this.possuiPontoCorte = possuiPontoCorte;
	}

	public PontoCorte getPontoCorte() {
		return pontoCorte;
	}

	public void setPontoCorte(PontoCorte pontoCorte) {
		this.pontoCorte = pontoCorte;
	}

	public List<EmpreendimentoAtividade> getEmpreendimentos() {
		return empreendimentos;
	}

	public void setEmpreendimentos(List<EmpreendimentoAtividade> empreendimentos) {
		this.empreendimentos = empreendimentos;
	}

	public Boolean getExigeNumeroCarZonaRural() {
		return exigeNumeroCarZonaRural;
	}

	public void setExigeNumeroCarZonaRural(Boolean exigeNumeroCarZonaRural) {
		this.exigeNumeroCarZonaRural = exigeNumeroCarZonaRural;
	}

	public Boolean getExigeNumeroCarZonaUrbana() {
		return exigeNumeroCarZonaUrbana;
	}

	public void setExigeNumeroCarZonaUrbana(Boolean exigeNumeroCarZonaUrbana) {
		this.exigeNumeroCarZonaUrbana = exigeNumeroCarZonaUrbana;
	}

	@Transient
	public Coema getCoemaSearch() {
		return coemaSearch;
	}

	@Transient
	public void setCoemaSearch(Coema coemaSearch) {
		this.coemaSearch = coemaSearch;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Atividade other = (Atividade) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getExigeApenasLO() {
		return exigeApenasLO;
	}

	public void setExigeApenasLO(Boolean exigeApenasLO) {
		this.exigeApenasLO = exigeApenasLO;
	}

	public Boolean getExigeCar() {
		return exigeCar;
	}

	public void setExigeCar(Boolean exigeCar) {
		this.exigeCar = exigeCar;
	}

	@Override
	public int compareTo(Atividade atividade) {
		if(atividade != null){
			return getCodigo().compareTo(atividade.getCodigo());
		}

		return 0;

	}



	public boolean isPodeSelecionar() {
		return podeSelecionar;
	}

	public void setPodeSelecionar(boolean podeSelecionar) {
		this.podeSelecionar = podeSelecionar;
	}

	public PotencialPoluidor getPpd() {
		return ppd;
	}

	public void setPpd(PotencialPoluidor ppd) {
		this.ppd = ppd;
	}

	@Override
	@Transient
	public Atividade clone() throws CloneNotSupportedException {
		Atividade atividade = new Atividade();
		atividade.setAtivo(this.ativo);
		atividade.setAutorizacaoAA(this.autorizacaoAA);
		atividade.setCodigo(this.codigo);
		atividade.setDescricao(this.descricao);
		atividade.setEmpreendimentos(this.empreendimentos);
		atividade.setExigeApenasLO(this.exigeApenasLO);
		atividade.setExigeEmpreendimento(this.exigeEmpreendimento);
		atividade.setExigeNumeroCarZonaRural(this.exigeNumeroCarZonaRural);
		atividade.setExigeNumeroCarZonaUrbana(this.exigeNumeroCarZonaUrbana);
		atividade.setGrupoAtividade(this.grupoAtividade);
		atividade.setId(this.id);
		atividade.setObservacao(this.observacao);
		atividade.setPontoCorte(this.pontoCorte);
		atividade.setPossuiPontoCorte(this.possuiPontoCorte);
		atividade.setPotencialPoluidor(this.potencialPoluidor);
		atividade.setUseParCoordenadas(this.useParCoordenadas);
		return atividade;
	}
}