package br.gov.ce.semace.erp.entity.planoviagem;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.entity.rh.NivelVinculacao;

/**
 * 
 *  Entidade responsável pelos dados do
 *  relatório de pagamento de diárias de
 *  terceirizados
 *  
 * @author fredericomota
 *
 */
@Entity
@Audited
@Table(schema="plano_viagem", name="relatorio_pessoa")
public class RelatorioPessoa implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1017189887819667972L;

	@Id
	@SequenceGenerator(sequenceName = "plano_viagem.seq_relatorio_pessoa", name = "SEQ_RELATORIO_PESSOA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_RELATORIO_PESSOA")
	private Integer             id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="relatorio_id")
	private Relatorio           relatorio;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="funcionario_id")
	private Funcionario         funcionario;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="nivel_vinculacao_id")
	private NivelVinculacao     nivelVinculacao;
	
	@Column(name="qtd_diarias")
	private BigDecimal             qtdDiaria;

	@Column(name="valor_diaria")
	private BigDecimal          valorDiaria;
	
	@Column(name="acrescimo")
	private BigDecimal          municipioAcrescimo;
	
	@Column(name="planos")
	private String              planos;
	
	@Transient
	private BigDecimal          valorTotalGeral;
	
	public BigDecimal getValorTotalGeral() {
		return valorTotalGeral;
	}

	public void setValorTotalGeral(BigDecimal valorTotalGeral) {
		this.valorTotalGeral = valorTotalGeral;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public NivelVinculacao getNivelVinculacao() {
		return nivelVinculacao;
	}

	public void setNivelVinculacao(NivelVinculacao nivelVinculacao) {
		this.nivelVinculacao = nivelVinculacao;
	}

	public BigDecimal getQtdDiaria() {
		return qtdDiaria;
	}

	public void setQtdDiaria(BigDecimal qtdDiaria) {
		this.qtdDiaria = qtdDiaria;
	}

	public BigDecimal getValorDiaria() {
		return valorDiaria;
	}

	public void setValorDiaria(BigDecimal valorDiaria) {
		this.valorDiaria = valorDiaria;
	}

	public Relatorio getRelatorio() {
		return relatorio;
	}

	public void setRelatorio(Relatorio relatorio) {
		this.relatorio = relatorio;
	}

	public BigDecimal getMunicipioAcrescimo() {
		return municipioAcrescimo;
	}

	public void setMunicipioAcrescimo(BigDecimal municipioAcrescimo) {
		this.municipioAcrescimo = municipioAcrescimo;
	}

	public String getPlanos() {
		return planos;
	}

	public void setPlanos(String planos) {
		this.planos = planos;
	}

}
