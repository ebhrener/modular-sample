package br.gov.ce.semace.erp.entity.produtividade;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.rh.Setor;
import br.gov.ce.semace.erp.enuns.StatusOcorrencia;

@Entity
@Table(name = "peso_setor", schema = "produtividade")
@Audited
public class PesoSetor implements Serializable, Comparable<PesoSetor>{
	
	private static final long serialVersionUID = 3202063234519346396L;

	@Id @SequenceGenerator(sequenceName="produtividade.seq_peso_setor_atividade", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_produtividade_id")
	private AtividadeProdutividade atividadeProdutividade = new AtividadeProdutividade();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setor_id")
	private Setor setor = new Setor();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "peso_id")
	private PesoAtividade peso = new PesoAtividade();
	
	private StatusOcorrencia status;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_id")
	private Atividade atividadeCoema = new Atividade();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public PesoAtividade getPeso() {
		return peso;
	}

	public void setPeso(PesoAtividade peso) {
		this.peso = peso;
	}
	
	public AtividadeProdutividade getAtividadeProdutividade() {
		return atividadeProdutividade;
	}

	public void setAtividadeProdutividade(
			AtividadeProdutividade atividadeProdutividade) {
		this.atividadeProdutividade = atividadeProdutividade;
	}
	
	public StatusOcorrencia getStatus() {
		return status;
	}

	public void setStatus(StatusOcorrencia status) {
		this.status = status;
	}

	public Atividade getAtividadeCoema() {
		return atividadeCoema;
	}

	public void setAtividadeCoema(Atividade atividadeCoema) {
		this.atividadeCoema = atividadeCoema;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((atividadeProdutividade == null) ? 0
						: atividadeProdutividade.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((peso == null) ? 0 : peso.hashCode());
		result = prime * result + ((setor == null) ? 0 : setor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PesoSetor other = (PesoSetor) obj;
		if(id != null && id.equals(other.getId())){
			return true;
		}
		
		if(peso != null && setor != null && other.getPeso() != null && other.getSetor() != null && atividadeProdutividade != null && other.getAtividadeProdutividade() != null){
			if(peso.getId().equals(other.getPeso().getId()) && other.getSetor().equals(setor.getId()) && atividadeProdutividade.getTitulo() != null && !atividadeProdutividade.getTitulo().isEmpty() && !other.getAtividadeProdutividade().getTitulo().isEmpty() && atividadeProdutividade.getTitulo().equals(other.getAtividadeProdutividade().getTitulo())){
				return true;
			} 
				
			return false;
		}
		return super.equals(obj);
	}

	@Override
	public int compareTo(PesoSetor pesoSetor) {
		if (pesoSetor != null && pesoSetor.getId() != null){
			if(this.getId() != null)
				return this.getId().compareTo(pesoSetor.getId());
		} 
		return 0;
	}
	
	
	
	
}
