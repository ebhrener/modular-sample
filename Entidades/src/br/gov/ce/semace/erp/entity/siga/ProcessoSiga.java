package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.gov.ce.semace.erp.entity.atendimento.acompanhamento.Oficio;
import br.gov.ce.semace.erp.entity.atendimento.acompanhamento.Upload;
import br.gov.ce.semace.erp.entity.geral.Empreendimento;
import br.gov.ce.semace.erp.entity.geral.Interessado;
import br.gov.ce.semace.erp.entity.licenciamento.TipoProcessoSiga;
import br.gov.ce.semace.erp.entity.rh.Setor;
import br.gov.ce.semace.erp.vo.DocumentoProcesso;
import br.gov.ce.semace.erp.vo.EspelhoLicenca;

public class ProcessoSiga implements Serializable {

	private static final long serialVersionUID = -9023478415757686302L;

	private String processo;
	private String spu;
	private String tipoProcesso;
	private String descricaoTipoProcesso;
	private Date dataCadastro;
	private String cliente;
	private String descricao;
	private String statusAtual;
	private List<Oficio> oficios;
	private List<MovimentoProcessoSiga> historico;
	private List<Upload> listUpload;
	private String controle;
	private String documentoCliente;
	private String numeroDocumento;
	private List<DocumentoProcesso> listDocumentoProcesso = new ArrayList<DocumentoProcesso>();
	private EspelhoLicenca espelhoLicenca;
	private TipoProcessoSiga tipoProcessoSiga;
	private Empreendimento empreendimento;
	private Interessado interessado;
	private String responsavelId;
	private String usuarioId;
	private Setor setor = new Setor();
	private boolean recebido;
	private String areaID;
	private Integer requerimentoID;
	private Boolean pendente;
	private Boolean processoDigital;

	public EspelhoLicenca getEspelhoLicenca() {
		return espelhoLicenca;
	}

	public void setEspelhoLicenca(EspelhoLicenca espelhoLicenca) {
		this.espelhoLicenca = espelhoLicenca;
	}

	public ProcessoSiga() {
		super();
	}

	public ProcessoSiga(String cliente, Date dataCadastro, String descricao,
			String processo, String spu, String tipoProcesso, String statusAtual) {
		super();

		this.processo = processo;
		this.cliente = cliente;
		this.dataCadastro = dataCadastro;
		this.descricao = descricao;
		this.spu = spu;
		this.tipoProcesso = tipoProcesso;
		this.statusAtual = statusAtual;

	}

	public List<MovimentoProcessoSiga> getHistorico() {
		return historico;
	}

	public void setHistorico(List<MovimentoProcessoSiga> historico) {
		this.historico = historico;
	}

	public List<Upload> getListUpload() {
		return listUpload;
	}

	public void setListUpload(List<Upload> listUpload) {
		this.listUpload = listUpload;
	}

	public String getProcesso() {
		return processo;
	}

	public void setProcesso(String processo) {
		this.processo = processo;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public String getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(String tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getStatusAtual() {
		return statusAtual;
	}

	public void setStatusAtual(String statusAtual) {
		this.statusAtual = statusAtual;
	}

	public List<Oficio> getOficios() {
		return oficios;
	}

	public void setOficios(List<Oficio> oficios) {
		this.oficios = oficios;
	}

	public String getControle() {
		return controle;
	}

	public void setControle(String controle) {
		this.controle = controle;
	}

	public String getDocumentoCliente() {
		return documentoCliente;
	}

	public void setDocumentoCliente(String documentoCliente) {
		this.documentoCliente = documentoCliente;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public List<DocumentoProcesso> getListDocumentoProcesso() {
		return listDocumentoProcesso;
	}

	public void setListDocumentoProcesso(
			List<DocumentoProcesso> listDocumentoProcesso) {
		this.listDocumentoProcesso = listDocumentoProcesso;
	}

	public TipoProcessoSiga getTipoProcessoSiga() {
		return tipoProcessoSiga;
	}

	public void setTipoProcessoSiga(TipoProcessoSiga tipoProcessoSiga) {
		this.tipoProcessoSiga = tipoProcessoSiga;
	}

	public Empreendimento getEmpreendimento() {
		return empreendimento;
	}

	public void setEmpreendimento(Empreendimento empreendimento) {
		this.empreendimento = empreendimento;
	}

	public Interessado getInteressado() {
		return interessado;
	}

	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}

	public String getResponsavelId() {
		return responsavelId;
	}

	public void setResponsavelId(String responsavelId) {
		this.responsavelId = responsavelId;
	}

	public String getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public boolean isRecebido() {
		return recebido;
	}

	public void setRecebido(boolean recebido) {
		this.recebido = recebido;
	}

	public String getAreaID() {
		return areaID;
	}

	public void setAreaID(String areaID) {
		this.areaID = areaID;
	}

	public String getDescricaoTipoProcesso() {
		return descricaoTipoProcesso;
	}

	public void setDescricaoTipoProcesso(String descricaoTipoProcesso) {
		this.descricaoTipoProcesso = descricaoTipoProcesso;
	}

	public Integer getRequerimentoID() {
		return requerimentoID;
	}

	public void setRequerimentoID(Integer requerimentoID) {
		this.requerimentoID = requerimentoID;
	}

	public Boolean getPendente() {
		return pendente;
	}

	public void setPendente(Boolean pendente) {
		this.pendente = pendente;
	}

	public Boolean getProcessoDigital() {
		return processoDigital;
	}

	public void setProcessoDigital(Boolean processoDigital) {
		this.processoDigital = processoDigital;
	}
}
