package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "intervalo_atividade_grandeza", schema = "geral")
public class IntervaloAtividadeGrandeza implements Serializable {

	private static final long serialVersionUID = -7190559222538277023L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_intervalo_atividade_grandeza", name = "geral.seq_intervalo_atividade_grandeza", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geral.seq_intervalo_atividade_grandeza")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "atividade_classificacao_grandeza_id")
	private AtividadeClassificacaoGrandeza atividadeClassificacaoGrandeza;

	@Column(name = "minimo")
	private BigDecimal minimo;

	@Column(name = "maximo")
	private BigDecimal maximo;

	@Column(name = "descricao", columnDefinition = "text")
	private String descricao;

	@Column(name = "ativo")
	private boolean ativo;

	@Column(name = "ordem")
	private int ordem;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AtividadeClassificacaoGrandeza getAtividadeClassificacaoGrandeza() {
		return atividadeClassificacaoGrandeza;
	}

	public void setAtividadeClassificacaoGrandeza(AtividadeClassificacaoGrandeza atividadeClassificacaoGrandeza) {
		this.atividadeClassificacaoGrandeza = atividadeClassificacaoGrandeza;
	}

	@Transient
	public String getDescricaoIntervalo(){
		if(this.minimo == null && this.maximo != null){
			return " ≤ " + this.maximo;
		}else if(this.maximo == null && this.minimo != null){
			return " > " + this.minimo;
		}else if(this.minimo != null && this.maximo != null){
			return " > " + this.minimo + " ≤ " + this.maximo;
		}
		return this.descricao;
	}

	@Transient
	public String getDescricaoIntervaloParaTabela(){
		if(this.minimo == null && this.maximo != null){
			return " ≤ " + this.maximo;
		}else if(this.maximo == null && this.minimo != null){
			return " > " + this.minimo;
		}else if(this.minimo != null && this.maximo != null){
			return new StringBuilder().append(" > ").append(this.minimo).append(System.getProperty("line.separator")).append(" ≤ ").append(this.maximo).toString();
		}
		return this.descricao;
	}

	public BigDecimal getMinimo() {
		return minimo;
	}

	public void setMinimo(BigDecimal minimo) {
		this.minimo = minimo;
	}

	public BigDecimal getMaximo() {
		return maximo;
	}

	public void setMaximo(BigDecimal maximo) {
		this.maximo = maximo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		IntervaloAtividadeGrandeza other = (IntervaloAtividadeGrandeza) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public int compareTo(IntervaloAtividadeGrandeza other) {
        if (other == null || other.getId() == null) {
        	return 1;
        }
        if (this.id > other.getId()) {
			return 1;
		} else if (this.id < other.getId()) {
			return -1;
		}
		else {
			return 0;
		}
	}
}
