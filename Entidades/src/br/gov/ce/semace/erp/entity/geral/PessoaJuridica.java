package br.gov.ce.semace.erp.entity.geral;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.gov.ce.semace.erp.entity.juridico.Corresponsavel;
import br.gov.ce.semace.erp.utils.DateUtils;

@Entity
@Audited
@Table(schema="geral", name="pessoa_juridica", uniqueConstraints=@UniqueConstraint(columnNames="cnpj"))
public class PessoaJuridica extends Pessoa{

	private static final long serialVersionUID = -1514937360622772274L;

	@Column(name="razao_social")
	private String razaoSocial;

	@Column(name="nome_fantasia")
	private String nomeFantasia;

	@Column(unique = true)
	private String cnpj;

	@Column(name="inscricao_municipal")
	private String inscricaoMunicipal;

	@Column(name="representante_legal")
	private String representanteLegal;

	@Temporal(TemporalType.DATE)
	@Column(name="data_abertura")
	private Date dataAbertura;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="contato_id")
	private Contato contato = new Contato();

	private String email;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="porte_id")
	private Porte porteEmpresa = new Porte();

	@Column(precision=15, scale=2)
	private BigDecimal faturamento;

	@Column(name="cgf")
	private String cgf;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="empresa", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonBackReference
	private List<Corresponsavel> corresponsaveis = new ArrayList<>();

	/**
	 * Mensagem genérica para campos que devem ser apresentados em tela de detalhes/tabela de resultados
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 18/11/2015 10:40:31
	 */
	@Transient
	private final String MSG_NAO_INFORMADO = "Não Informado";

	/**
	 * Informa a Inscrição Estadual para as telas de Detalhes e Tabela de resultados.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 18/11/2015 10:35:38
	 *
	 * @return
	 * 		Retorna o valor do campo {@link #getCgf()}, caso não seja <code>null</code> ou vazio ( {@link String#isEmpty()} ) . Caso contrário, retorna a mensagem <b>Não Informado</b>.
	 */
	@Transient
	public String getDetalheInscricaoEstadual(){

		if (this.cgf != null && !this.cgf.trim().isEmpty()){
			return this.cgf;
		}

		return MSG_NAO_INFORMADO;
	}

	/**
	 * Informa a Inscrição Municipal para as telas de Detalhes e Tabela de resultados.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 18/11/2015 10:45:11
	 *
	 * @return
	 * 		Retorna o valor do campo {@link #getInscricaoMunicipal()}, caso não seja <code>null</code> ou vazio ( {@link String#isEmpty()} ) . Caso contrário, retorna a mensagem <b>Não Informado</b>.
	 */
	@Transient
	public String getDetalheInscricaoMunicipal(){

		if (this.inscricaoMunicipal != null && !this.inscricaoMunicipal.isEmpty()){
			return this.inscricaoMunicipal;
		}

		return MSG_NAO_INFORMADO;
	}

	/**
	 * Informa o Porte da Empresa para as telas de Detalhes e Tabela de resultados.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 18/11/2015 10:49:56
	 *
	 * @return
	 * 		Retorna o valor do campo {@link #getPorteEmpresa()}, caso não seja <code>null</code> ou vazio ( {@link String#isEmpty()} ) . Caso contrário, retorna a mensagem <b>Não Informado</b>.
	 */
	@Transient
	public String getDetalhePorteEmpresa(){

		if (this.porteEmpresa != null && !this.porteEmpresa.getDescricao().trim().isEmpty()){
			return this.porteEmpresa.getDescricao();
		}

		return MSG_NAO_INFORMADO;
	}

	/**
	 * Informa o Nome da Fantasia para as telas de Detalhes e Tabela de resultados.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 18/11/2015 11:25:37
	 *
	 * @return
	 * 		Retorna o valor do campo {@link #getNomeFantasia()}, caso não seja <code>null</code> ou vazio ( {@link String#isEmpty()} ) . Caso contrário, retorna a mensagem <b>Não Informado</b>.
	 */
	@Transient
	public String getDetalheNomeFantasia(){

		if (this.nomeFantasia != null && !this.nomeFantasia.trim().isEmpty()){
			return this.nomeFantasia;
		}

		return MSG_NAO_INFORMADO;
	}

	/**
	 * Informa a data de abertura para as telas de Detalhes e Tabela de resultados.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 18/11/2015 11:52:26
	 *
	 * @return
	 * 		Retorna o valor do campo {@link #getDataAbertura()}, caso não seja <code>null</code>. Caso contrário, retorna a mensagem <b>Não Informado</b>.
	 */
	@Transient
	public String getDetalheDataAbertura(){

		if(this.dataAbertura == null){
				return MSG_NAO_INFORMADO;
			}

		return DateUtils.toString(this.dataAbertura);
	}

	public PessoaJuridica() {
		super();
	}

	public PessoaJuridica(Pessoa pessoa) {
		super(pessoa);
	}

	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public Porte getPorteEmpresa() {
		return porteEmpresa;
	}
	public void setPorteEmpresa(Porte porteEmpresa) {
		this.porteEmpresa = porteEmpresa;
	}
	public Date getDataAbertura() {
		return dataAbertura;
	}
	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}
	public Contato getContato() {
		return contato;
	}
	public void setContato(Contato contato) {
		this.contato = contato;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	@Override
	public String getDisplayNome() {
		return getRazaoSocial();
	}

	@Override
	public String getDisplayDocumento() {
		return getCnpj();
	}

	@Override
	public String getDisplayEmail() {
		return getEmail();
	}

	@Override
	public String getDisplayCgf(){
		return getCgf();
	}

	public String getRepresentanteLegal() {
		return representanteLegal;
	}
	public void setRepresentanteLegal(String representanteLegal) {
		this.representanteLegal = representanteLegal;
	}
	public BigDecimal getFaturamento() {
		return faturamento;
	}
	public void setFaturamento(BigDecimal faturamento) {
		this.faturamento = faturamento;
	}
	public String getCgf() {
		return cgf;
	}
	public void setCgf(String cgf) {
		this.cgf = cgf;
	}
	public List<Corresponsavel> getCorresponsaveis() {
		return corresponsaveis;
	}
	public void setCorresponsaveis(List<Corresponsavel> corresponsaveis) {
		this.corresponsaveis = corresponsaveis;
	}

	@Override
	@Transient
	public String getEmailDefault() {
		return getEmail();
	}

	@Override
	@Transient
	public Contato getContatoDefault() {
		return getContato();
	}
}
