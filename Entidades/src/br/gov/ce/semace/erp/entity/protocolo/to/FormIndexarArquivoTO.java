package br.gov.ce.semace.erp.entity.protocolo.to;

import java.io.Serializable;

public class FormIndexarArquivoTO implements Serializable {

	private static final long serialVersionUID = -8907884806633650080L;

	private String nome;

	private Long tipoDocumentoID;

	private Long tipoAnexoID;

	private String numeroDocumento;

	public FormIndexarArquivoTO() {
		super();
	}

	public FormIndexarArquivoTO(String nome, Long tipoDocumentoID, String numeroDocumento) {
		super();
		this.nome = nome;
		this.tipoDocumentoID = tipoDocumentoID;
		this.numeroDocumento = numeroDocumento;
	}

	public FormIndexarArquivoTO(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getTipoDocumentoID() {
		return tipoDocumentoID;
	}

	public void setTipoDocumentoID(Long tipoDocumentoID) {
		this.tipoDocumentoID = tipoDocumentoID;
	}

	public Long getTipoAnexoID() {
		return tipoAnexoID;
	}

	public void setTipoAnexoID(Long tipoAnexoID) {
		this.tipoAnexoID = tipoAnexoID;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
}