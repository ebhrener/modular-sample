package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;


@Entity
@Audited
@Table(name = "formacao_profissional", schema="geral")
public class FormacaoProfissional implements Serializable{

	private static final long serialVersionUID = -7144199807283144061L;


	//=========================================Atributos=========================================

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_formacao_profissional", name = "SEQFORMPROF", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQFORMPROF")
	private Long id;


	@NotNull
	@Column(nullable=false)
	private String descricao;

	//=========================================Relacionametos=========================================

	@ManyToOne(fetch=FetchType.LAZY )
	@JoinColumn(name="escolaridade_id")
	private Escolaridade escolaridade = new Escolaridade();


	//=========================================Getters and setters=========================================


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the escolaridade
	 */
	public Escolaridade getEscolaridade() {
		return escolaridade;
	}

	/**
	 * @param escolaridade the escolaridade to set
	 */
	public void setEscolaridade(Escolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}




}
