package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.GrupoEnvioSMS;

/**
 * Entidade responsável por controlar os envios dos SMS
 *
 * @author joerlan.lima
 *
 */

@Audited
@Entity
@Table(name = "sms", schema="geral")
public class SMS implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 8080320755282018902L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_sms", name = "geral.seq_sms", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geral.seq_sms")
	private Long id;

	/**
	 * Ex: "+55"
	 */
	@Column(nullable=false, name="cod_pais")
	private String codigoPais;

	/**
	 * Ex: "85"
	 */
	@Column(nullable=false)
	private String ddd;

	/**
	 * Ex: "99996666"
	 */
	@Column(nullable=false)
	private String numero;

	@Column(columnDefinition="text")
	private String conteudo;

	@Column(nullable=false, name="data_cadastro")
	private Date dataCadastro;

	@Column(nullable=false)
	private Boolean enviado;

	@Column(name="data_envio")
	private Date dataEnvio;

	@Column(nullable=false, name="qtd_erros")
	private Integer qtdErros;

	@Column(columnDefinition="text", name="last_error")
	private String lastError;

	@Column(nullable=false)
	private String nome;

	/**
	 * Este campo é usado para mapear qual o id do grupo que será enviado ao webservice para cada msg
	 *
	 */
	@Column(nullable=false, name="grupo_envio_sms_enum")
	@Enumerated(EnumType.ORDINAL)
	private GrupoEnvioSMS grupoEnvioSMS;

	public String getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getLastError() {
		return lastError;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public Integer getQtdErros() {
		return qtdErros;
	}

	public void setQtdErros(Integer qtdErros) {
		this.qtdErros = qtdErros;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataEnvio() {
		return dataEnvio;
	}

	public void setDataEnvio(Date dataEnvio) {
		this.dataEnvio = dataEnvio;
	}

	public Boolean getEnviado() {
		return enviado;
	}

	public void setEnviado(Boolean enviado) {
		this.enviado = enviado;
	}

	public String getNumeroCompleto() {
		String ddd = this.ddd.replaceAll("[^0-9]","");

		if (ddd.charAt(0) == '0') {
			ddd = ddd.substring(1);
		}

		String numero = this.numero.replaceAll("[^0-9]","");
		return codigoPais+ddd+numero;
	}

	public GrupoEnvioSMS getGrupoEnvioSMS() {
		return grupoEnvioSMS;
	}

	public void setGrupoEnvioSMS(GrupoEnvioSMS grupoEnvioSMS) {
		this.grupoEnvioSMS = grupoEnvioSMS;
	}

}