package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="rh", name="usufruir_ferias")
public class UsufruirFerias implements Serializable{

	private static final long serialVersionUID = -346357459267778316L;

	@Id
	@SequenceGenerator(sequenceName="rh.seq_usufruir_ferias", name="SEQUSUFRUIR", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQUSUFRUIR")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_inicial")
	private Date dataInicial;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_final")
	private Date dataFinal;

	@NotNull(message="Campo Ano é obrigatório.")
	@Column(nullable=false)
	private Integer ano;

	@NotNull(message="Campo Quantidade de dias é obrigatório.")
	@Column(nullable=false)
	private Integer dias = 0;

	private Boolean interrompido = false;

	@Column(name="ci_id")
	private String ci;

	@Column(columnDefinition="text")
	private String justificativa;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_retorno")
	private Date dataRetorno;

	//------------------------------------------------Relacionamentos---------------------------------------------

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="ferias_id")
	private Ferias ferias ;

	//------------------------------------------------Gets e Setters---------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Ferias getFerias() {
		return ferias;
	}

	public void setFerias(Ferias ferias) {
		this.ferias = ferias;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getDias() {
		return dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	public String getCi() {
		return ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public Date getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(Date dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public Boolean getInterrompido() {
		return interrompido;
	}

	public void setInterrompido(Boolean interrompido) {
		this.interrompido = interrompido;
	}

}