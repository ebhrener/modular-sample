package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import br.gov.ce.semace.erp.entity.financeiro.Dae;
import br.gov.ce.semace.erp.entity.fiscalizacao.AutoInfracao;
import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.geral.Pessoa;
import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;
import br.gov.ce.semace.erp.entity.julgamento.Decisao;
import br.gov.ce.semace.erp.entity.protocolo.Processo;
import br.gov.ce.semace.erp.entity.siga.AutoInfracaoSiga;
import br.gov.ce.semace.erp.entity.siga.ProcessoSiga;
import br.gov.ce.semace.erp.enuns.StatusDivida;

@Entity
@Audited
@Table(name="divida_ativa", schema="juridico")
public class DividaAtiva implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -5573660966757688128L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_divida_ativa", name="juridico.seq_divida_ativa", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_divida_ativa")
	private Long id;

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(name="origem_divida_id")
	private OrigemDivida origemDivida = new OrigemDivida();

	/**
	 * Auto de Infração do Natuur
	 */
	@OneToOne(optional=true)
	@JoinColumn(name="auto_infracao_id")
	private AutoInfracao autoInfracao;

	/**
	 * Auto de Infração do Rails (Antigo Natuur)
	 */
	@Column(name="documento_auto_infracao_rails")
	private String autoInfracaoRails;

	@OneToOne(optional=true)
	@JoinColumn(name="processo_id")
	private Processo processo;

	@OneToOne(fetch= FetchType.LAZY, optional = false)
	@JoinColumn(name="documento_id")
	private Documento documento;

	@ManyToOne(fetch=FetchType.LAZY,optional=false)
	@JoinColumn(name="pessoa_id")
	private Pessoa pessoa;

	@OneToMany(mappedBy="dividaAtiva", fetch=FetchType.LAZY)
	private List<AcordoDivida> acordos = new ArrayList<>();

	@Column(nullable=false, unique=true)
	private String spu;

	@Column(name="dispositivo_infringido")
	private String dispositivoInfringido;

	@Column(name="data_quitacao")
	private Date dataQuitacao;

	@Column(name="data_lavratura")
	private Date dataLavratura;

	@Column(name="data_vencimento_notificacao")
	private Date dataVencimentoNotificacao;

	@Column(name="data_vencimento_debito")
	private Date dataVencimentoDebito;

	@Column(name="data_cadastro", nullable=false)
	private Date dataCadastro = new Date();

	@Column(name="data_migracao")
	private Date dataMigracao;

	@Enumerated(EnumType.ORDINAL)
	@Column(nullable=false)
	private StatusDivida status;

	@Column(name="numero_folha")
	private Integer numeroFolha;

	@Column(name="valor_divida", nullable=false, scale=2, precision=12)
	private BigDecimal valorDivida;

	@Column(name="valor_atual", nullable=false, scale=2, precision=12)
	private BigDecimal valorAtual;

	private String motivo;

	@Column(name="numero_execucao_fiscal")
	private String numeroExecucaoFiscal;

	@Column(scale=5, precision=12)
	private BigDecimal honorarios;

	@NotAudited
	@OneToMany(mappedBy="dividaAtiva", fetch=FetchType.LAZY)
	private List<HistoricoDivida> historico = new ArrayList<>();

	@NotAudited
	@OneToMany(mappedBy="dividaAtiva", fetch=FetchType.LAZY)
	private List<HistoricoStatusDivida> historicoStatus = new ArrayList<>();

	@Column(name = "processo_siga", length=10)
	private String processoSiga;

	/**
	 * Auto de Infração do SIGA
	 */
	@Column(name = "auto_infracao_siga", length=10)
	private String autoInfracaoSiga;

	/**
	 * Auto de Infração que não foi encontrado em nem uma das 3 bases, Natuur, SIGA e Rails
	 */
	@Column(name = "auto_infracao_origem_desconhecida")
	private String autoInfracaoOrigemDesconhecida;

	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "dae_vigente_id")
	private Dae daeVigente;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="municipio_execucao_id")
	private Cidade municipioExecucao = new Cidade();

	@Column(name = "valor_multa", scale=5, precision=12)
    private BigDecimal valorMulta;

	@Column(name = "data_primeira_notificacao")
	private Date dataPrimeiraNotificacao;

	@Column(name="data_notificacao_apos_julgamento")
	private Date dataNotificacaoAposJulgamento;

	@Column(name="list_spu")
	private String listSpu;

	@Column(name="dias_vencimento")
	private Integer diasVencimento;

	@Transient
	private ProcessoSiga processoSigaObjeto;

	@Transient
	private AutoInfracaoSiga autoInfracaoSigaObjeto;

	@Transient
	private Decisao decisao;

	@Transient
	private String numAIF;

	@Transient
	private Date dataVencimentoPrimeiraNotificacao;

	@Transient
	private AcordoDivida acordoVigente;

	//------------------------------------------------Gets e Setters---------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrigemDivida getOrigemDivida() {
		return origemDivida;
	}

	public void setOrigemDivida(OrigemDivida origemDivida) {
		this.origemDivida = origemDivida;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public AutoInfracao getAutoInfracao() {
		return autoInfracao;
	}

	public void setAutoInfracao(AutoInfracao autoInfracao) {
		this.autoInfracao = autoInfracao;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public String getDispositivoInfringido() {
		return dispositivoInfringido;
	}

	public void setDispositivoInfringido(String dispositivoInfringido) {
		this.dispositivoInfringido = dispositivoInfringido;
	}

	public Date getDataQuitacao() {
		return dataQuitacao;
	}

	public void setDataQuitacao(Date dataQuitacao) {
		this.dataQuitacao = dataQuitacao;
	}

	public Date getDataLavratura() {
		return dataLavratura;
	}

	public void setDataLavratura(Date dataLavratura) {
		this.dataLavratura = dataLavratura;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getNumeroFolha() {
		return numeroFolha;
	}

	public void setNumeroFolha(Integer numeroFolha) {
		this.numeroFolha = numeroFolha;
	}

	public BigDecimal getValorDivida() {
		return valorDivida;
	}

	public void setValorDivida(BigDecimal valorDivida) {
		this.valorDivida = valorDivida;
	}

	public BigDecimal getValorAtual() {
		return valorAtual;
	}

	public void setValorAtual(BigDecimal valorAtual) {
		this.valorAtual = valorAtual;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getNumeroExecucaoFiscal() {
		return numeroExecucaoFiscal;
	}

	public void setNumeroExecucaoFiscal(String numeroExecucaoFiscal) {
		this.numeroExecucaoFiscal = numeroExecucaoFiscal;
	}

	public BigDecimal getHonorarios() {
		return honorarios;
	}

	public void setHonorarios(BigDecimal honorarios) {
		this.honorarios = honorarios;
	}

	public List<HistoricoDivida> getHistorico() {
		return historico;
	}

	public void setHistorico(List<HistoricoDivida> historico) {
		this.historico = historico;
	}

	public List<AcordoDivida> getAcordos() {
		return acordos;
	}

	public void setAcordos(List<AcordoDivida> acordos) {
		this.acordos = acordos;
	}

	public StatusDivida getStatus() {
		return status;
	}

	public void setStatus(StatusDivida status) {
		this.status = status;
	}

	public String getProcessoSiga() {
		return processoSiga;
	}

	public void setProcessoSiga(String processoSiga) {
		this.processoSiga = processoSiga;
	}

	public String getAutoInfracaoSiga() {
		return autoInfracaoSiga;
	}

	public void setAutoInfracaoSiga(String autoInfracaoSiga) {
		this.autoInfracaoSiga = autoInfracaoSiga;
	}

	public String getDisplayNumeroAIF() {
		if (autoInfracao != null) {
			return autoInfracao.getNumeroAutoInfracao().getNumeroDocumento();
		}
		if (getAutoInfracaoRails() != null && !autoInfracaoRails.trim().isEmpty()) {
			return autoInfracaoRails;
		}
		if (autoInfracaoSiga != null && !autoInfracaoSiga.trim().isEmpty()) {
			return autoInfracaoSiga;
		}
		return autoInfracaoOrigemDesconhecida;
	}

	public Dae getDaeVigente() {
		return daeVigente;
	}

	public void setDaeVigente(Dae daeVigente) {
		this.daeVigente = daeVigente;
	}

	public ProcessoSiga getProcessoSigaObjeto() {
		return processoSigaObjeto;
	}

	public void setProcessoSigaObjeto(ProcessoSiga processoSigaObjeto) {
		this.processoSigaObjeto = processoSigaObjeto;
	}

	public AutoInfracaoSiga getAutoInfracaoSigaObjeto() {
		return autoInfracaoSigaObjeto;
	}

	public void setAutoInfracaoSigaObjeto(AutoInfracaoSiga autoInfracaoSigaObjeto) {
		this.autoInfracaoSigaObjeto = autoInfracaoSigaObjeto;
	}

	public Decisao getDecisao() {
		return decisao;
	}

	public void setDecisao(Decisao decisao) {
		this.decisao = decisao;
	}

	public Cidade getMunicipioExecucao() {
		return municipioExecucao;
	}

	public void setMunicipioExecucao(Cidade municipioExecucao) {
		this.municipioExecucao = municipioExecucao;
	}

	public List<HistoricoStatusDivida> getHistoricoStatus() {
		return historicoStatus;
	}

	public void setHistoricoStatus(List<HistoricoStatusDivida> historicoStatus) {
		this.historicoStatus = historicoStatus;
	}

	public Date getDataVencimentoNotificacao() {
		return dataVencimentoNotificacao;
	}

	public void setDataVencimentoNotificacao(Date dataVencimentoNotificacao) {
		this.dataVencimentoNotificacao = dataVencimentoNotificacao;
	}

	public Date getDataVencimentoDebito() {
		return dataVencimentoDebito;
	}

	public void setDataVencimentoDebito(Date dataVencimentoDebito) {
		this.dataVencimentoDebito = dataVencimentoDebito;
	}

	public BigDecimal getValorMulta() {
		return valorMulta;
	}

	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}

	public Date getDataMigracao() {
		return dataMigracao;
	}

	public void setDataMigracao(Date dataMigracao) {
		this.dataMigracao = dataMigracao;
	}

	public String getNumAIF() {
		return numAIF;
	}

	public void setNumAIF(String numAIF) {
		this.numAIF = numAIF;
	}

	public String getAutoInfracaoRails() {
		return autoInfracaoRails;
	}

	public void setAutoInfracaoRails(String autoInfracaoRails) {
		this.autoInfracaoRails = autoInfracaoRails;
	}

	public String getAutoInfracaoOrigemDesconhecida() {
		return autoInfracaoOrigemDesconhecida;
	}

	public void setAutoInfracaoOrigemDesconhecida(
			String autoInfracaoOrigemDesconhecida) {
		this.autoInfracaoOrigemDesconhecida = autoInfracaoOrigemDesconhecida;
	}

	public Date getDataPrimeiraNotificacao() {
		return dataPrimeiraNotificacao;
	}

	public void setDataPrimeiraNotificacao(Date dataPrimeiraNotificacao) {
		this.dataPrimeiraNotificacao = dataPrimeiraNotificacao;
	}

	public Date getDataNotificacaoAposJulgamento() {
		return dataNotificacaoAposJulgamento;
	}

	public void setDataNotificacaoAposJulgamento(Date dataNotificacaoAposJulgamento) {
		this.dataNotificacaoAposJulgamento = dataNotificacaoAposJulgamento;
	}

	public String getListSpu() {
		return listSpu;
	}

	public void setListSpu(String listSpu) {
		this.listSpu = listSpu;
	}

	public Integer getDiasVencimento() {
		return diasVencimento;
	}

	public void setDiasVencimento(Integer diasVencimento) {
		this.diasVencimento = diasVencimento;
	}

	public Date getDataVencimentoPrimeiraNotificacao() {
		return dataVencimentoPrimeiraNotificacao;
	}

	public void setDataVencimentoPrimeiraNotificacao(
			Date dataVencimentoPrimeiraNotificacao) {
		this.dataVencimentoPrimeiraNotificacao = dataVencimentoPrimeiraNotificacao;
	}

	public AcordoDivida getAcordoVigente() {
		return acordoVigente;
	}

	public void setAcordoVigente(AcordoDivida acordoVigente) {
		this.acordoVigente = acordoVigente;
	}

}
