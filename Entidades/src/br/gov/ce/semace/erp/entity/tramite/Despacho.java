package br.gov.ce.semace.erp.entity.tramite;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;


@Entity
@Table(schema="protocolo")
@Audited
public class Despacho implements Serializable{

	private static final long serialVersionUID = -6937476977914542397L;
	
	@Id @SequenceGenerator(name="SEQ", sequenceName="protocolo.seq_despacho", initialValue = 1, allocationSize = 1)
	@GeneratedValue(generator = "SEQ", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	private String assunto;

	@Column(columnDefinition = "text")
	private String descricao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tramite_id")
	private Tramite tramite; 
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="documento_id")
	private Documento documento = new Documento();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Tramite getTramite() {
		return tramite;
	}

	public void setTramite(Tramite tramite) {
		this.tramite = tramite;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
}
