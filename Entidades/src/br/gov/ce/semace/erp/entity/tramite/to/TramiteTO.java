package br.gov.ce.semace.erp.entity.tramite.to;

import java.io.Serializable;

public class TramiteTO implements Serializable {

	private static final long serialVersionUID = 2916328332456342202L;

	private String areaOrigem;

	private String areaDestino;

	private String data;

	private String responsavel;

	public String getAreaOrigem() {
		return areaOrigem;
	}

	public void setAreaOrigem(String areaOrigem) {
		this.areaOrigem = areaOrigem;
	}

	public String getAreaDestino() {
		return areaDestino;
	}

	public void setAreaDestino(String areaDestino) {
		this.areaDestino = areaDestino;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

}
