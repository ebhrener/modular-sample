package br.gov.ce.semace.erp.entity.atendimento.to;

import java.io.Serializable;
import java.util.Date;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.RequerimentoAtividadeCheckListItemCheckList;

public class EnvioArquivoOriginalShapeTO implements Serializable, Comparable<EnvioArquivoOriginalShapeTO> {

	private static final long serialVersionUID = -1775765265856424594L;

	private String nomeArquivo;

	private Date dataEnvio;

	private Boolean validado;

	private boolean excluido;

	private RequerimentoAtividadeCheckListItemCheckList item;

	public EnvioArquivoOriginalShapeTO() {
		super();
	}

	public EnvioArquivoOriginalShapeTO(String nomeArquivo, Date dataEnvio, Boolean validado, boolean excluido, RequerimentoAtividadeCheckListItemCheckList item) {
		super();
		this.nomeArquivo = nomeArquivo;
		this.dataEnvio = dataEnvio;
		this.validado = validado;
		this.excluido = excluido;
		this.item = item;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public Date getDataEnvio() {
		return dataEnvio;
	}

	public void setDataEnvio(Date dataEnvio) {
		this.dataEnvio = dataEnvio;
	}

	public Boolean getValidado() {
		return validado;
	}

	public void setValidado(Boolean validado) {
		this.validado = validado;
	}

	public boolean isExcluido() {
		return excluido;
	}

	public void setExcluido(boolean excluido) {
		this.excluido = excluido;
	}

	public RequerimentoAtividadeCheckListItemCheckList getItem() {
		return item;
	}

	public void setItem(RequerimentoAtividadeCheckListItemCheckList item) {
		this.item = item;
	}

	public String getDisplayValidado() {
		if (Boolean.TRUE.equals(this.validado)) {
			return "Validado";
		} else if (Boolean.FALSE.equals(this.validado)) {
			return "Invalidado";
		} else {
			if (item.getRequerimento().isDocumentacaoPendente() || item.getRequerimento().isAtendimentoPreAgendado()) {
				return "Aguardando Envio";
			}
			return "Aguardando Validação";
		}
	}

	public String getDisplayNomeArquivo() {
		if(this.getNomeArquivo() != null && !this.getNomeArquivo().isEmpty()) {
			if(this.getNomeArquivo().length() > 25) {
				return this.getNomeArquivo().substring(0, 22).concat("...");
			}
			return this.getNomeArquivo();
		}
		return "-";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataEnvio == null) ? 0 : dataEnvio.hashCode());
		result = prime * result + (excluido ? 1231 : 1237);
		result = prime * result + ((nomeArquivo == null) ? 0 : nomeArquivo.hashCode());
		result = prime * result + ((validado == null) ? 0 : validado.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EnvioArquivoOriginalShapeTO other = (EnvioArquivoOriginalShapeTO) obj;
		if (dataEnvio == null) {
			if (other.dataEnvio != null) {
				return false;
			}
		} else if (!dataEnvio.equals(other.dataEnvio)) {
			return false;
		}
		if (excluido != other.excluido) {
			return false;
		}
		if (nomeArquivo == null) {
			if (other.nomeArquivo != null) {
				return false;
			}
		} else if (!nomeArquivo.equals(other.nomeArquivo)) {
			return false;
		}
		if (validado == null) {
			if (other.validado != null) {
				return false;
			}
		} else if (!validado.equals(other.validado)) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(EnvioArquivoOriginalShapeTO outroEnvio) {
		if(this.dataEnvio.after(outroEnvio.getDataEnvio())) {
			return -1;
		}
		if(this.dataEnvio.before(outroEnvio.getDataEnvio())) {
			return 1;
		}
		return this.nomeArquivo.compareToIgnoreCase(outroEnvio.getNomeArquivo());
	}

}
