package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "email_attach", schema = "geral")
public class EmailAttach  implements Serializable{
	
	private static final long serialVersionUID = -3572511773110600873L;

	@Id 
	@SequenceGenerator(sequenceName = "geral.seq_email_attach", name = "SEQCONTRATO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQCONTRATO")
	private Long id;
	
	private String name;
	
	private byte[] bytes;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "email_id")
	private Email email;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public byte[] getBytes() {
		return bytes;
	}
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
	public Email getEmail() {
		return email;
	}
	public void setEmail(Email email) {
		this.email = email;
	}
}
