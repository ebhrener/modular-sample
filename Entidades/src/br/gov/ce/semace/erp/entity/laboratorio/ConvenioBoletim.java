package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="convenio_boletim", schema="laboratorio")
public class ConvenioBoletim implements Serializable {
	
	private static final long serialVersionUID = 1360083123927132207L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_convenio_boletim", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	@Column(name= "nome", nullable= false)
	private String nome;
	
	@Column(name= "email", unique=true, nullable= false, length= 120)
	private String email;
	
	@Column(name= "contato")
	private String contato;
	
	@Column(name= "telefone", length= 30)
	private String telefone;
	
	@Temporal(TemporalType.DATE)
	@Column(name= "data_inicial")
	private Date dataInicial;
	
	@Temporal(TemporalType.DATE)
	@Column(name= "data_final")
	private Date dataFinal;
	
	private Boolean ativo = Boolean.TRUE;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

}
