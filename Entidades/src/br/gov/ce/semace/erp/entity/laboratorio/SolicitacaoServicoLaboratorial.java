package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.AgendamentoAnaliseLaboratorial;
import br.gov.ce.semace.erp.entity.financeiro.SolicitacaoDae;
import br.gov.ce.semace.erp.enuns.StatusSolicitacaoServicoLaboratorial;

@Entity
@Audited
@Table(schema = "laboratorio", name = "solicitacao_servico_laboratorial")
public class SolicitacaoServicoLaboratorial implements Serializable {

	private static final long serialVersionUID = -1221075827457027275L;

	@Id
	@SequenceGenerator(sequenceName = "laboratorio.seq_solicitacao_servico_laboratorial", name = "SEQ_SOLICITACAO_SERVICO_LABORATORIAL", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SOLICITACAO_SERVICO_LABORATORIAL")
	private Long id;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "solicitacaoServicoLaboratorial", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ServicoLaboratorial> servicosLaboratoriais = new ArrayList<ServicoLaboratorial>();
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "solicitacao_dae_id")
	private SolicitacaoDae solicitacaoDae;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agendamento_analise_id")
	private AgendamentoAnaliseLaboratorial agendamentoAnalise;
	
	@Column(name= "status_solicitacao_laboratorial")
	private StatusSolicitacaoServicoLaboratorial  statusSolicitacaoServicoLaboratorial;
	
	@Override
	public SolicitacaoServicoLaboratorial clone() throws CloneNotSupportedException {
		SolicitacaoServicoLaboratorial sol = new SolicitacaoServicoLaboratorial();
		sol.setId(this.id);
		sol.setSolicitacaoDae(this.solicitacaoDae);
		sol.setAgendamentoAnalise(this.agendamentoAnalise);
		sol.setServicosLaboratoriais(this.servicosLaboratoriais);
		sol.setStatusSolicitacaoServicoLaboratorial(this.statusSolicitacaoServicoLaboratorial);
		return sol;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ServicoLaboratorial> getServicosLaboratoriais() {
		return servicosLaboratoriais;
	}

	public void setServicosLaboratoriais(
			List<ServicoLaboratorial> servicosLaboratoriais) {
		this.servicosLaboratoriais = servicosLaboratoriais;
	}

	public SolicitacaoDae getSolicitacaoDae() {
		return solicitacaoDae;
	}

	public void setSolicitacaoDae(SolicitacaoDae solicitacaoDae) {
		this.solicitacaoDae = solicitacaoDae;
	}

	public AgendamentoAnaliseLaboratorial getAgendamentoAnalise() {
		return agendamentoAnalise;
	}

	public void setAgendamentoAnalise(
			AgendamentoAnaliseLaboratorial agendamentoAnalise) {
		this.agendamentoAnalise = agendamentoAnalise;
	}

	public StatusSolicitacaoServicoLaboratorial getStatusSolicitacaoServicoLaboratorial() {
		return statusSolicitacaoServicoLaboratorial;
	}

	public void setStatusSolicitacaoServicoLaboratorial(
			StatusSolicitacaoServicoLaboratorial statusSolicitacaoServicoLaboratorial) {
		this.statusSolicitacaoServicoLaboratorial = statusSolicitacaoServicoLaboratorial;
	}

}
