package br.gov.ce.semace.erp.entity.fiscalizacao.vo;

import br.gov.ce.semace.erp.enuns.StatusOcorrencia;

public class StatusPeriodo {
	
	private StatusOcorrencia status;

	private Integer mes;
	
	private Integer ano;
	
	private Long quantidade;

	public StatusPeriodo() {
		super();
	}
	
	public StatusPeriodo(StatusOcorrencia status, Integer mes,
			Integer ano, Long quantidade) {
		super();
		this.setStatus(status);
		this.mes = mes;
		this.ano = ano;
		this.quantidade = quantidade;
	}

	public StatusOcorrencia getStatus() {
		return status;
	}

	public void setStatus(StatusOcorrencia status) {
		this.status = status;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

}
