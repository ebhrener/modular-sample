package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.OrigemSolicitacaoDae;

@Entity
@Audited
@Table(schema = "financeiro", name = "solicitacao_dae")
public class SolicitacaoDae implements Serializable{

	private static final long serialVersionUID = 2757741777547858154L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_solicitacao_dae", name = "SEQ_SOLICITACAO_DAE", allocationSize = 1 )
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SOLICITACAO_DAE")
	private Long id;

	@NotNull
	@Column(nullable=false)
	private String documento;

	@Column(name="origem_solicitacao_dae")
	private OrigemSolicitacaoDae origemSolicitacaoDae;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "documento_origem_id")
	private DocumentoOrigem documentoOrigem;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "solicitacaoDae")
	private Dae dae;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "segunda_via_id")
	private SolicitacaoDae segundaVia;

	private Boolean ativo = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public OrigemSolicitacaoDae getOrigemSolicitacaoDae() {
		return origemSolicitacaoDae;
	}

	public void setOrigemSolicitacaoDae(OrigemSolicitacaoDae origemSolicitacaoDae) {
		this.origemSolicitacaoDae = origemSolicitacaoDae;
	}

	public Dae getDae() {
		return dae;
	}

	public void setDae(Dae dae) {
		this.dae = dae;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public DocumentoOrigem getDocumentoOrigem() {
		return documentoOrigem;
	}

	public void setDocumentoOrigem(DocumentoOrigem documentoOrigem) {
		this.documentoOrigem = documentoOrigem;
	}

	public SolicitacaoDae getSegundaVia() {
		return segundaVia;
	}

	public void setSegundaVia(SolicitacaoDae segundaVia) {
		this.segundaVia = segundaVia;
	}

}
