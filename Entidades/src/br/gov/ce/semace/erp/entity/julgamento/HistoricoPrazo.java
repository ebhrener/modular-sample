package br.gov.ce.semace.erp.entity.julgamento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.FaseJulgamento;


@Entity
@Table(schema = "julgamento", name = "historico_prazo")
@Audited
public class HistoricoPrazo implements Serializable {

	private static final long serialVersionUID = 7024077788592772736L;

	@Id
	@SequenceGenerator(sequenceName ="julgamento.seq_historico_prazo", name = "SEQHISTORICOPRAZO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQHISTORICOPRAZO")
	private Long id;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio")
	private Date dataInicio;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_fim")
	private Date dataFim;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_modificacao")
	private Date dataModificacao;

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_modificacao")
	private Date horaModificacao;
	
	private Boolean ativo;
	
	@Column(columnDefinition="text")
	private String justificativa;
	
	@Column(name = "fase_julgamento")
	private FaseJulgamento faseJulgamento;

	@ManyToOne(fetch = FetchType.LAZY)
	private Funcionario funcionario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Julgamento julgamento;

	
	/**
	 *Construtor default
	 * 
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 10/07/2013 11:26:59 
	 */
	public HistoricoPrazo() {
		super();
	}

	/**
	 * Construtor
	 * 
	 * @author Rômulo Augusto [romuloaugusto.silva@gmail.com] - 10/07/2013 11:26:59
	 * 
	 * @param dataInicio
	 * @param dataFim
	 * @param dataModificacao
	 * @param horaModificacao
	 * @param ativo
	 * @param justificativa
	 * @param faseJulgamento
	 * @param funcionario
	 * @param julgamento
	 */
	public HistoricoPrazo(Date dataInicio, Date dataFim, Date dataModificacao,
			Date horaModificacao, Boolean ativo, String justificativa,
			FaseJulgamento faseJulgamento, Funcionario funcionario,
			Julgamento julgamento) {
		super();
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.dataModificacao = dataModificacao;
		this.horaModificacao = horaModificacao;
		this.ativo = ativo;
		this.justificativa = justificativa;
		this.faseJulgamento = faseJulgamento;
		this.funcionario = funcionario;
		this.julgamento = julgamento;
	}

	@Override
	protected HistoricoPrazo clone() throws CloneNotSupportedException {
		HistoricoPrazo historicoPrazo = new HistoricoPrazo();
		historicoPrazo.setAtivo(this.ativo);
		historicoPrazo.setDataFim(this.dataFim);
		historicoPrazo.setDataInicio(this.dataInicio);
		historicoPrazo.setDataModificacao(this.dataModificacao);
		historicoPrazo.setFaseJulgamento(this.faseJulgamento);
		historicoPrazo.setFuncionario(this.funcionario);
		historicoPrazo.setHoraModificacao(this.horaModificacao);
		historicoPrazo.setJulgamento(this.julgamento);
		historicoPrazo.setJustificativa(this.justificativa);
		return historicoPrazo;
	}
	
	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Julgamento getJulgamento() {
		return julgamento;
	}

	public void setJulgamento(Julgamento julgamento) {
		this.julgamento = julgamento;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Date getDataModificacao() {
		return dataModificacao;
	}

	public void setDataModificacao(Date dataModificacao) {
		this.dataModificacao = dataModificacao;
	}

	public Date getHoraModificacao() {
		return horaModificacao;
	}

	public void setHoraModificacao(Date horaModificacao) {
		this.horaModificacao = horaModificacao;
	}

	public FaseJulgamento getFaseJulgamento() {
		return faseJulgamento;
	}

	public void setFaseJulgamento(FaseJulgamento faseJulgamento) {
		this.faseJulgamento = faseJulgamento;
	}
	
}
