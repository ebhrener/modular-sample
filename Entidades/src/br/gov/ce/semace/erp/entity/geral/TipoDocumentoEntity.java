package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "tipo_documento", schema = "geral")
public class TipoDocumentoEntity implements Serializable {

	private static final long serialVersionUID = -2809473116176107625L;

	@Id @Column(name = "id")
	@SequenceGenerator(sequenceName="geral.seq_tipo_documento", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@Column(name = "tipo_documento_id_natuur")
	private Long tipoDocumentoIdNatuur;

	@Column(name = "tipo_documento_id_siga")
	private Long tipoDocumentoIdSiga;

	@Column(name = "sigla")
	private String sigla;

	@Column(name = "descricao")
	private String descricao;

	@Column(name = "ativo", nullable = false)
	private Boolean ativo;

	@Column(name = "publico", nullable = true)
	private Boolean publico;

	@Column(name = "permite_qrcode", nullable = false)
	private boolean permiteQRCode = true;

	public TipoDocumentoEntity() {
		super();
	}

	public TipoDocumentoEntity(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTipoDocumentoIdNatuur() {
		return tipoDocumentoIdNatuur;
	}

	public void setTipoDocumentoIdNatuur(Long tipoDocumentoIdNatuur) {
		this.tipoDocumentoIdNatuur = tipoDocumentoIdNatuur;
	}

	public Long getTipoDocumentoIdSiga() {
		return tipoDocumentoIdSiga;
	}

	public void setTipoDocumentoIdSiga(Long tipoDocumentoIdSiga) {
		this.tipoDocumentoIdSiga = tipoDocumentoIdSiga;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getPublico() {
		return publico;
	}

	public void setPublico(Boolean publico) {
		this.publico = publico;
	}

	public boolean isPermiteQRCode() {
		return permiteQRCode;
	}

	public void setPermiteQRCode(boolean permiteQRCode) {
		this.permiteQRCode = permiteQRCode;
	}
}
