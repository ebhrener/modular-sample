package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.Letra;

@Entity
@Audited
@Table(name="matriz_grandeza", schema = "geral")
public class MatrizGrandeza implements Serializable {

	private static final long serialVersionUID = -4358846689095811029L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_matriz_grandeza", name = "geral.seq_matriz_grandeza", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geral.seq_matriz_grandeza")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "intervalo_atividade_grandeza_id_1")
	private IntervaloAtividadeGrandeza intervalo1;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "intervalo_atividade_grandeza_id_2")
	private IntervaloAtividadeGrandeza intervalo2;

	@Column(name = "letra")
	private Letra letra;

	@Column(name = "ativo")
	private boolean ativo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public IntervaloAtividadeGrandeza getIntervalo1() {
		return intervalo1;
	}

	public void setIntervalo1(IntervaloAtividadeGrandeza intervalo1) {
		this.intervalo1 = intervalo1;
	}

	public IntervaloAtividadeGrandeza getIntervalo2() {
		return intervalo2;
	}

	public void setIntervalo2(IntervaloAtividadeGrandeza intervalo2) {
		this.intervalo2 = intervalo2;
	}

	public Letra getLetra() {
		return letra;
	}

	public void setLetra(Letra letra) {
		this.letra = letra;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
