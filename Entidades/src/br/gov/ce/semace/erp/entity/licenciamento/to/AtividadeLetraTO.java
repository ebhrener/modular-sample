package br.gov.ce.semace.erp.entity.licenciamento.to;

import java.util.ArrayList;
import java.util.List;

import br.gov.ce.semace.erp.entity.financeiro.LetraAtividade;
import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.enuns.Letra;

public class AtividadeLetraTO {

	private Atividade atividade;

	private Letra letra;

	private List<LetraAtividade> listLetraAtividade = new ArrayList<LetraAtividade>();


	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public Letra getLetra() {
		return letra;
	}

	public void setLetra(Letra letra) {
		this.letra = letra;
	}

	public List<LetraAtividade> getListLetraAtividade() {
		return listLetraAtividade;
	}

	public void setListLetraAtividade(List<LetraAtividade> listLetraAtividade) {
		this.listLetraAtividade = listLetraAtividade;
	}



}
