package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;

import br.gov.ce.semace.erp.entity.geral.Interessado;
public class ConfiguracaoConsultorRama implements Serializable{

	private static final long serialVersionUID = -8703422904137956803L;
	private Long id;

	private Interessado interessado;

	private ConfiguracaoLicenciamento configuracao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Interessado getInteressado() {
		return interessado;
	}

	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}

	public ConfiguracaoLicenciamento getConfiguracao() {
		return configuracao;
	}

	public void setConfiguracao(ConfiguracaoLicenciamento configuracao) {
		this.configuracao = configuracao;
	}

}
