package br.gov.ce.semace.erp.entity.integracao.geoprocessamento;

import java.io.Serializable;


public class Estatistica implements Serializable{

	private static final long serialVersionUID = 1839046995587816257L;
	
	private String mes;
	private Integer totalEntrada;
	private Integer totalSaida;
	
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public Integer getTotalEntrada() {
		return totalEntrada;
	}
	public void setTotalEntrada(Integer totalEntrada) {
		this.totalEntrada = totalEntrada;
	}
	public Integer getTotalSaida() {
		return totalSaida;
	}
	public void setTotalSaida(Integer totalSaida) {
		this.totalSaida = totalSaida;
	}
	
	
}
