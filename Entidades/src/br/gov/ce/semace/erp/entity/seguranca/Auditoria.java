package br.gov.ce.semace.erp.entity.seguranca;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import br.gov.ce.semace.erp.auditoria.AuditoriaListener;

@Entity
@Table(name="auditoria", schema="auditoria")
@RevisionEntity(AuditoriaListener.class)
public class Auditoria implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1196322026672159248L;

	@Id @SequenceGenerator(sequenceName = "auditoria.seq_auditoria", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	@RevisionNumber
	private Long id;

	private Date data;
 
	// TODO: modificar para usar instância de usuário (@manytoone), porém a
	// referência deve ser de um tipo genérico para usuario.class, usuarioWeb.class, ...
	@Column(name="usuario_id")
	private Long usuarioId;

	@Column(name="usuario_login")
	private String usuarioLogin;

	@Column(name="usuario_tipo")
	private String usuarioTipo;
	
	private String ipSolicitacao;
	
	@Column(name = "stack_trace", columnDefinition = "text")
	private String stackTrace;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	@RevisionTimestamp
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getUsuarioLogin() {
		return usuarioLogin;
	}

	public void setUsuarioLogin(String usuarioLogin) {
		this.usuarioLogin = usuarioLogin;
	}

	public String getUsuarioTipo() {
		return usuarioTipo;
	}

	public void setUsuarioTipo(String usuarioTipo) {
		this.usuarioTipo = usuarioTipo;
	}

	public String getIpSolicitacao() {
		return ipSolicitacao;
	}

	public void setIpSolicitacao(String ipSolicitacao) {
		this.ipSolicitacao = ipSolicitacao;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}

}
