package br.gov.ce.semace.erp.entity.planoviagem;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.transporte.Passageiro;
import br.gov.ce.semace.erp.enuns.TipoLancamento;

/**
 * Entidade {@link Lancamento} do esquema Plano de Viagem.
 *
 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 09/06/2014 09:07:03
 */
@Entity
@Audited
@Table(schema = "plano_viagem", name = "lancamento")
public class Lancamento implements Serializable {

	private static final long serialVersionUID = 4799514767874125076L;

	@Id
	@SequenceGenerator(sequenceName = "plano_viagem.seq_lancamento", name = "SEQ_LANCAMENTO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LANCAMENTO")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional=false)
	@JoinColumn(name = "passageiro_id")
	private Passageiro passageiro = new Passageiro();

	@Column(name = "data_lancamento")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataLancamento;

	@Column(name = "tipo_lancamento")
	private TipoLancamento tipoLancamento;

	@Column(name = "valor_diaria")
	private BigDecimal valorDiaria;

	@Column(name = "acrescimo")
	private Float acrescimo;

	@Column(name = "valor")
	private BigDecimal valor;

	@Column(name = "data_pagamento")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataPagamento;

	@Column(nullable = false)
	private boolean ativo = true;

	@Column(name="justificativa", columnDefinition="text")
	private String justificativa;

	@Transient
	private SolicitacaoViagem solicitacaoViagemPesquisa;

	@Transient
	private boolean statusPagamento = false;

	// ================================== GETERS and SETERS ====================================//

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Passageiro getPassageiro() {
		return passageiro;
	}

	public void setPassageiro(Passageiro passageiro) {
		this.passageiro = passageiro;
	}

	public Date getDataLancamento() {
		return dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	public TipoLancamento getTipoLancamento() {
		return tipoLancamento;
	}

	public void setTipoLancamento(TipoLancamento tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}

	public BigDecimal getValorDiaria() {
		return valorDiaria;
	}

	public void setValorDiaria(BigDecimal valorDiaria) {
		this.valorDiaria = valorDiaria;
	}

	public Float getAcrescimo() {
		return acrescimo;
	}

	public void setAcrescimo(Float acrescimo) {
		this.acrescimo = acrescimo;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public SolicitacaoViagem getSolicitacaoViagem() {
		if (passageiro != null && passageiro.getSolicitacaoTransporte() != null
				&& passageiro.getSolicitacaoTransporte().getListSolicitacaoViagem() != null
				&& !passageiro.getSolicitacaoTransporte().getListSolicitacaoViagem().isEmpty()) {
			return passageiro.getSolicitacaoTransporte().getListSolicitacaoViagem().get(0);
		}
		return null;
	}

	public SolicitacaoViagem getSolicitacaoViagemPesquisa() {
		return this.solicitacaoViagemPesquisa;
	}

	public void setSolicitacaoViagemPesquisa(SolicitacaoViagem solicitacaoViagemPesquisa) {
		this.solicitacaoViagemPesquisa = solicitacaoViagemPesquisa;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public boolean getStatusPagamento() {
		return statusPagamento;
	}

	public void setStatusPagamento(boolean statusPagamento) {
		this.statusPagamento = statusPagamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Lancamento other = (Lancamento) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}
