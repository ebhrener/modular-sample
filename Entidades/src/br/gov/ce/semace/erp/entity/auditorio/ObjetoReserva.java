package br.gov.ce.semace.erp.entity.auditorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="auditorio", name="objeto_reserva")
public class ObjetoReserva implements Serializable {

	private static final long serialVersionUID = -687156554738812018L;

	@Id
	@SequenceGenerator(sequenceName="auditorio.seq_objeto_reserva", name="SEQOBJETORESERVA", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQOBJETORESERVA")
	private Long id;

	private String nome;

	@Column(columnDefinition = "text")
	private String descricao;

	@ManyToMany(mappedBy="objetos", fetch = FetchType.LAZY)
	private List<Agendamento> agendamento = new ArrayList<Agendamento>();

	private Boolean ativo;

	//---------------------------------------------------------------------------------------------------Gets e Setters---------------------------------------------------------------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Agendamento> getAgendamento() {
		return agendamento;
	}

	public void setAgendamento(List<Agendamento> agendamento) {
		this.agendamento = agendamento;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}



}
