package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "geral", name = "cabecalho_opcao")
public class CabecalhoOpcao implements Serializable {

	private static final long serialVersionUID = 3671199971549224033L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_cabecalho_questao", name = "geral.seq_cabecalho_questao", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geral.seq_cabecalho_questao")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "opcao_id")
	private Opcao opcao;

	@Column(name = "cabecalho")
	private String cabecalho;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Opcao getOpcao() {
		return opcao;
	}

	public void setOpcao(Opcao opcao) {
		this.opcao = opcao;
	}

	public String getCabecalho() {
		return cabecalho;
	}

	public void setCabecalho(String cabecalho) {
		this.cabecalho = cabecalho;
	}
}