package br.gov.ce.semace.erp.entity.fiscalizacao.vo;

public class ZonaPeriodo {
	
	private String nomeZona;
	
	private Integer mes;
	
	private Integer ano;
	
	private Long quantidade;

	public ZonaPeriodo() {
		super();
	}
	
	public ZonaPeriodo(String nomeZona, Integer mes, Integer ano, Long quantidade) {
		super();
		this.nomeZona = nomeZona;
		this.mes = mes;
		this.ano = ano;
		this.quantidade = quantidade;
	}

	public String getNomeZona() {
		return nomeZona;
	}

	public void setNomeZona(String nomeZona) {
		this.nomeZona = nomeZona;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}
	
	

}