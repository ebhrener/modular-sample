package br.gov.ce.semace.erp.entity.fiscalizacao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "fiscalizacao", name = "dosimetria")
@SequenceGenerator(sequenceName = Dosimetria.SEQUENCE_NAME, name = Dosimetria.SEQUENCE_NAME, allocationSize = 1)
public class Dosimetria implements Serializable {

	/**
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 29/07/2016 13:55:14
	 */
	private static final long serialVersionUID = 4786134557567198169L;

	/**
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 29/07/2016 13:54:34
	 */
	public static final String SEQUENCE_NAME = "fiscalizacao.seq_dosimetria";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
	private Long id;

	/**
	 * Artigo no qual a autuação se enquadra
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 29/07/2016 14:31:03
	 */
	@Column(name = "artigo_dosimetria_id")
	public Long artigo;

	/**
	 * Capacidade econômica do autuado
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 29/07/2016 14:28:17
	 */
	@Column(name = "capacidade_economica_id")
	public Long capacidadeEconomica;

	/**
	 * Sigla para Potencial Poluidor Degradador
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 29/07/2016 14:17:09
	 */
	@Column(name = "ppd_id")
	public Long ppd;

	/**
	 * Nível de intenção/motivação que levou o autuado a cometar a infração
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 29/07/2016 14:29:33
	 */
	@Column(name = "motivacao_infracao_id")
	public Long motivacaoInfracao;

	/**
	 * Nível de conseguência ao meio ambiente causada pela infração
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 29/07/2016 14:29:26
	 */
	@Column(name = "consequencia_meio_ambiente_id")
	public Long consequenciaMeioAmbiente;

	/**
	 * Nível de conseguência à saúde pública causada pela infração
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 29/07/2016 14:29:00
	 */
	@Column(name = "consequencia_saude_publica_id")
	public Long consequenciaSaudePublica;

	/**
	 * Valor fornecedido pelo fiscal que será utilizado como referência no cálculo do valor da multa
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 29/07/2016 14:18:54
	 */
	@Column(name = "valor_multa_referencia")
	public Double valorMultaReferencia;

	/**
	 * Quantidade de unidades (hectare ou fração) que será utilizada para o cálculo em multa que precisam ser um adicionado valor extra por unidade
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 29/07/2016 14:27:09
	 */
	@Column(name = "quantidade_unidade")
	public Double qtdUnidade;

	/**
	 * Valor da unidade utilizado para o cálculo em multa que precisam ser um adicionado valor extra por unidade
	 *
	 * @author Harisson Rafael [harissonrafael@gmail.com] - 29/07/2016 14:27:52
	 */
	@Column(name = "valor_unidade")
	public Double valorUnidade;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "auto_infracao_id")
	private AutoInfracao autoInfracao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getArtigo() {
		return artigo;
	}

	public void setArtigo(Long artigo) {
		this.artigo = artigo;
	}

	public Long getCapacidadeEconomica() {
		return capacidadeEconomica;
	}

	public void setCapacidadeEconomica(Long capacidadeEconomica) {
		this.capacidadeEconomica = capacidadeEconomica;
	}

	public Long getPpd() {
		return ppd;
	}

	public void setPpd(Long ppd) {
		this.ppd = ppd;
	}

	public Long getMotivacaoInfracao() {
		return motivacaoInfracao;
	}

	public void setMotivacaoInfracao(Long motivacaoInfracao) {
		this.motivacaoInfracao = motivacaoInfracao;
	}

	public Long getConsequenciaMeioAmbiente() {
		return consequenciaMeioAmbiente;
	}

	public void setConsequenciaMeioAmbiente(Long consequenciaMeioAmbiente) {
		this.consequenciaMeioAmbiente = consequenciaMeioAmbiente;
	}

	public Long getConsequenciaSaudePublica() {
		return consequenciaSaudePublica;
	}

	public void setConsequenciaSaudePublica(Long consequenciaSaudePublica) {
		this.consequenciaSaudePublica = consequenciaSaudePublica;
	}

	public Double getValorMultaReferencia() {
		return valorMultaReferencia;
	}

	public void setValorMultaReferencia(Double valorMultaReferencia) {
		this.valorMultaReferencia = valorMultaReferencia;
	}

	public Double getQtdUnidade() {
		return qtdUnidade;
	}

	public void setQtdUnidade(Double qtdUnidade) {
		this.qtdUnidade = qtdUnidade;
	}

	public Double getValorUnidade() {
		return valorUnidade;
	}

	public void setValorUnidade(Double valorUnidade) {
		this.valorUnidade = valorUnidade;
	}

	public AutoInfracao getAutoInfracao() {
		return autoInfracao;
	}

	public void setAutoInfracao(AutoInfracao autoInfracao) {
		this.autoInfracao = autoInfracao;
	}
}