package br.gov.ce.semace.erp.entity.ged;

import java.io.File;
import java.io.Serializable;

public class ArquivoGED implements Serializable {

	private static final long serialVersionUID = -3439046585562691041L;

	private Long id;
	private String numeroSPU;
	private String tipoAnexo;
	private String nome;
	private File file;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNumeroSPU() {
		return numeroSPU;
	}
	public void setNumeroSPU(String numeroSPU) {
		this.numeroSPU = numeroSPU;
	}
	public String getTipoAnexo() {
		return tipoAnexo;
	}
	public void setTipoAnexo(String tipoAnexo) {
		this.tipoAnexo = tipoAnexo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ArquivoGED other = (ArquivoGED) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (nome == null) {
			if (other.nome != null) {
				return false;
			}
		} else if (!nome.equals(other.nome)) {
			return false;
		}
		return true;
	}

}