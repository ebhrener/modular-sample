package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="potencial_poluidor",schema="licenciamento")
public class PotencialPoluidor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2929084158903794545L;

	@Id
	@SequenceGenerator(sequenceName="licenciamento.seq_potencial_poluidor",name="SEQLICEN", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQLICEN")
	private Long id ;
	
	
	private String descricao;
	
	
	private String sigla;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
