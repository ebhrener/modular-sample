package br.gov.ce.semace.erp.entity.florestal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "agrotoxico_fabricante", schema ="adm_siga")
public class FabricanteProdutoAgrotoxicoSiga implements Serializable{

	private static final long serialVersionUID = 8607181963295836338L;
	
	@Id
	@Column(name = "codigoempresa")
	private Integer codigoEmpresa;
	
	@Column(name="razaosocial")
	private String razaoSocial;

	@Column(name ="cgc_cnpj")
	private String cnpj;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="datacadastro")
	private Date dataCadastro;
	
	
	private Boolean excluido;


	public Integer getCodigoEmpresa() {
		return codigoEmpresa;
	}


	public void setCodigoEmpresa(Integer codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}


	public String getRazaoSocial() {
		return razaoSocial;
	}


	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}


	public String getCnpj() {
		return cnpj;
	}


	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}


	public Date getDataCadastro() {
		return dataCadastro;
	}


	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}


	public Boolean getExcluido() {
		return excluido;
	}


	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}

}
