package br.gov.ce.semace.erp.entity.transporte;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "transporte", name = "passageiro_externo")
public class PassageiroExterno implements Serializable{

	private static final long serialVersionUID = -665626809212489447L;

	@Id
	@SequenceGenerator(sequenceName = "transporte.seq_passageiro_externo", name = "SEQ_PASSAGEIROEXTERNO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PASSAGEIROEXTERNO")
	private Long id;

	@Column(name="nome", columnDefinition="text")
	private String nome = new String();

	@Column(name="origem", columnDefinition="text")
	private String origem = new String();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="solicitacao_transporte_id")
	private SolicitacaoTransporte solicitacaoTransporte;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		PassageiroExterno other = (PassageiroExterno) obj;
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}

	//================================== GETERS and SETERS ====================================//

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public SolicitacaoTransporte getSolicitacaoTransporte() {
		return solicitacaoTransporte;
	}

	public void setSolicitacaoTransporte(SolicitacaoTransporte solicitacaoTransporte) {
		this.solicitacaoTransporte = solicitacaoTransporte;
	}

}
