package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.licenciamento.ConfiguracaoLicenciamento;

@Audited
@Entity
@Table(schema = "financeiro", name = "configuracao_pagamento_paralizacao_atividade")
public class ConfiguracaoPagamentoParalizacao implements Serializable{

	private static final long serialVersionUID = 3615253509136740980L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_config_pag_paralizacao", name = "SEQ_DESCONTO_PARALIZACAO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DESCONTO_PARALIZACAO")
	private Long id;

	@Column(name="ano")
	private Integer ano;

	@Column(name="porcentagem")
	private Double porcentagem;

	@Column(name="ativo")
	private boolean ativo;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="configuracao_licenciamento_id")
	private ConfiguracaoLicenciamento configuracaoLicenciamento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Double getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(Double porcentagem) {
		this.porcentagem = porcentagem;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public ConfiguracaoLicenciamento getConfiguracaoLicenciamento() {
		return configuracaoLicenciamento;
	}

	public void setConfiguracaoLicenciamento(ConfiguracaoLicenciamento configuracaoLicenciamento) {
		this.configuracaoLicenciamento = configuracaoLicenciamento;
	}

}
