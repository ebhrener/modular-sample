package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "porte", schema = "geral")
public class Porte implements Serializable {

	private static final long serialVersionUID = -7354708486244336486L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_porte", name = "SEQPORTE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQPORTE")
	private Long id;

	@Column(name = "descricao")
	private String descricao;

	@Column(name = "faturamento_minimo")
	private BigDecimal faturamentoMinimo;

	@Column(name = "faturamento_maximo")
	private BigDecimal faturamentoMaximo;

	@Column(name = "area_construida_minima")
	private BigDecimal areaConstruidoMinima;

	@Column(name = "area_construida_maxima")
	private BigDecimal areaConstruidoMaxima;

	@Column(name = "num_funcionario_minimo")
	private Integer numFuncionarioMinimo;

	@Column(name = "num_funcionario_maximo")
	private Integer numFuncionarioMaximo;

	@Column(name = "grau_prioridade")
	private Integer grauPrioridade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getFaturamentoMinimo() {
		return faturamentoMinimo;
	}

	public void setFaturamentoMinimo(BigDecimal faturamentoMinimo) {
		this.faturamentoMinimo = faturamentoMinimo;
	}

	public BigDecimal getFaturamentoMaximo() {
		return faturamentoMaximo;
	}

	public void setFaturamentoMaximo(BigDecimal faturamentoMaximo) {
		this.faturamentoMaximo = faturamentoMaximo;
	}

	public BigDecimal getAreaConstruidoMinima() {
		return areaConstruidoMinima;
	}

	public void setAreaConstruidoMinima(BigDecimal areaConstruidoMinima) {
		this.areaConstruidoMinima = areaConstruidoMinima;
	}

	public BigDecimal getAreaConstruidoMaxima() {
		return areaConstruidoMaxima;
	}

	public void setAreaConstruidoMaxima(BigDecimal areaConstruidoMaxima) {
		this.areaConstruidoMaxima = areaConstruidoMaxima;
	}

	public Integer getNumFuncionarioMinimo() {
		return numFuncionarioMinimo;
	}

	public void setNumFuncionarioMinimo(Integer numFuncionarioMinimo) {
		this.numFuncionarioMinimo = numFuncionarioMinimo;
	}

	public Integer getNumFuncionarioMaximo() {
		return numFuncionarioMaximo;
	}

	public void setNumFuncionarioMaximo(Integer numFuncionarioMaximo) {
		this.numFuncionarioMaximo = numFuncionarioMaximo;
	}

	public Integer getGrauPrioridade() {
		return grauPrioridade;
	}

	public void setGrauPrioridade(Integer grauPrioridade) {
		this.grauPrioridade = grauPrioridade;
	}

}
