package br.gov.ce.semace.erp.entity.transporte;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

/**
 * Entidade de relacionamento {@link ManyToMany} entre {@link OrdemTransporte} 
 * e {@link Veiculo} do esquema Transporte.
 * 
 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 19/05/2014 16:06:48
 *
 */
@Entity
@Audited
@Table(schema="transporte", name="ordem_transporte_veiculo")
public class OrdemTransporteVeiculo implements Serializable{

	private static final long serialVersionUID = -1738626665862468016L;
	
	@Id
	@SequenceGenerator(sequenceName="transporte.seq_ordem_transporte_veiculo", name="SEQ_ORDEM_TRANSPORTE_VEICULO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ_ORDEM_TRANSPORTE_VEICULO")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="ordem_transporte_id")
	private OrdemTransporte ordemTransporte = new OrdemTransporte();

	@ManyToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.MERGE)
	@JoinColumn(name="passageiro_id")
	private Passageiro passageiro;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="veiculo_id")
	private Veiculo veiculo = new Veiculo();
	
	@Column(name="justificativa", columnDefinition="text")
	private String justificativa;

	@Column(name="ativo")
	private boolean ativo = true;

	@Transient
	public String getDetalheAtivo(){
		
		if (this.ativo){
			return "Sim";
		}
		
		return "Não";
	}
	
	@Override
	public OrdemTransporteVeiculo clone() throws CloneNotSupportedException {
		OrdemTransporteVeiculo ordem = new OrdemTransporteVeiculo();
		
		ordem.setAtivo(this.isAtivo());
		ordem.setOrdemTransporte(this.getOrdemTransporte());
		ordem.setVeiculo(this.getVeiculo());
		
		return ordem;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		OrdemTransporteVeiculo other = (OrdemTransporteVeiculo) obj;
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	//================================== GETERS and SETERS ====================================//
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrdemTransporte getOrdemTransporte() {
		return ordemTransporte;
	}

	public void setOrdemTransporte(OrdemTransporte ordemTransporte) {
		this.ordemTransporte = ordemTransporte;
	}

	public Passageiro getPassageiro() {
		return passageiro;
	}

	public void setPassageiro(Passageiro passageiro) {
		this.passageiro = passageiro;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
