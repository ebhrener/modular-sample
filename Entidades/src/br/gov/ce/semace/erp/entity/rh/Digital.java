package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 *
 * @author Harisson Rafael [harissonrafael@gmail.com] - 08/07/2016 10:18:43
 *
 */
@Entity
@Audited
@Table(name = "funcionario_digital", schema = "rh")
public class Digital implements Serializable {

	private static final long serialVersionUID = -6682866876633406636L;

	@Id
	@SequenceGenerator(sequenceName = "rh.seq_funcionario_digital", name = "seq_funcionario_digital", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_funcionario_digital")
	private Long id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;

	private byte[] digital;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public byte[] getDigital() {
		return digital;
	}

	public void setDigital(byte[] digital) {
		this.digital = digital;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Digital other = (Digital) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}