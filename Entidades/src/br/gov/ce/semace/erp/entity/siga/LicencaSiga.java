package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.util.Date;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.geral.Empreendimento;


public class LicencaSiga implements Serializable {

	private static final long serialVersionUID = 6566958743052774128L;

	private String processoId;

	private String empreendimentoId;

	private Boolean apa;

	private Double taxaLicenciamento;

	private String textoLicenca;

	private Boolean renovacao;

	private Boolean regularizacao;

	private Boolean alteracao;

	private Boolean via2;

	private String licencaId;

	private Date dataEmissao;

	private Date dataValidade;

	private String coordAssinouId;

	private String usuarioAssinouId;

	private String usuarioCoordId;

	private String areaNucleoId;

	private String condicionantesEspecificos;

	private Date entregue;

	private Boolean cancelado;

	private Date dataAssinatura;

	private String spuId;

	private String numeroProcesso;

	private String tipoProcesso;

	private String nomeEmpreendimento;

	private int empreendimentoNatuur;

	private String descricaoAtividade;

	private Empreendimento empreendimento;

	private int espaco;

	private boolean natuur = false;

	private boolean existeRama = false;

	private String atividadeId;

	private Atividade atividade;

	private String idTipoProcesso;

	private AtividadeSiga atividadeSiga = new AtividadeSiga();

	private EmpreendimentoSiga empreendimentoSiga;

	private String documentoCliente;



	public int getEspaco() {
		return espaco;
	}

	public void setEspaco(int espaco) {
		this.espaco = espaco;
	}

	public void setEmpreendimentoNatuur(int empreendimentoNatuur) {
		this.empreendimentoNatuur = empreendimentoNatuur;
	}

	public String getProcessoId() {
		return processoId;
	}

	public void setProcessoId(String processoId) {
		this.processoId = processoId;
	}

	public String getEmpreendimentoId() {
		return empreendimentoId;
	}

	public void setEmpreendimentoId(String empreendimentoId) {
		this.empreendimentoId = empreendimentoId;
	}

	public Boolean getApa() {
		return apa;
	}

	public void setApa(Boolean apa) {
		this.apa = apa;
	}

	public Double getTaxaLicenciamento() {
		return taxaLicenciamento;
	}

	public void setTaxaLicenciamento(Double taxaLicenciamento) {
		this.taxaLicenciamento = taxaLicenciamento;
	}

	public String getTextoLicenca() {
		return textoLicenca;
	}

	public void setTextoLicenca(String textoLicenca) {
		this.textoLicenca = textoLicenca;
	}

	public Boolean getRenovacao() {
		return renovacao;
	}

	public void setRenovacao(Boolean renovacao) {
		this.renovacao = renovacao;
	}

	public Boolean getRegularizacao() {
		return regularizacao;
	}

	public void setRegularizacao(Boolean regularizacao) {
		this.regularizacao = regularizacao;
	}

	public Boolean getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(Boolean alteracao) {
		this.alteracao = alteracao;
	}

	public Boolean getVia2() {
		return via2;
	}

	public void setVia2(Boolean via2) {
		this.via2 = via2;
	}

	public String getLicencaId() {
		return licencaId;
	}

	public void setLicencaId(String licencaId) {
		this.licencaId = licencaId;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}

	public String getCoordAssinouId() {
		return coordAssinouId;
	}

	public void setCoordAssinouId(String coordAssinouId) {
		this.coordAssinouId = coordAssinouId;
	}

	public String getUsuarioAssinouId() {
		return usuarioAssinouId;
	}

	public void setUsuarioAssinouId(String usuarioAssinouId) {
		this.usuarioAssinouId = usuarioAssinouId;
	}

	public String getUsuarioCoordId() {
		return usuarioCoordId;
	}

	public void setUsuarioCoordId(String usuarioCoordId) {
		this.usuarioCoordId = usuarioCoordId;
	}

	public String getAreaNucleoId() {
		return areaNucleoId;
	}

	public void setAreaNucleoId(String areaNucleoId) {
		this.areaNucleoId = areaNucleoId;
	}

	public String getCondicionantesEspecificos() {
		return condicionantesEspecificos;
	}

	public void setCondicionantesEspecificos(String condicionantesEspecificos) {
		this.condicionantesEspecificos = condicionantesEspecificos;
	}

	public Date getEntregue() {
		return entregue;
	}

	public void setEntregue(Date entregue) {
		this.entregue = entregue;
	}

	public Boolean getCancelado() {
		return cancelado;
	}

	public void setCancelado(Boolean cancelado) {
		this.cancelado = cancelado;
	}

	public Date getDataAssinatura() {
		return dataAssinatura;
	}

	public void setDataAssinatura(Date dataAssinatura) {
		this.dataAssinatura = dataAssinatura;
	}

	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public String getSpuId() {
		return spuId;
	}

	public void setSpuId(String spuId) {
		this.spuId = spuId;
	}

	public String getNomeEmpreendimento() {
		return nomeEmpreendimento;
	}

	public void setNomeEmpreendimento(String nomeEmpreendimento) {
		this.nomeEmpreendimento = nomeEmpreendimento;
	}

	public EmpreendimentoSiga getEmpreendimentoSiga() {
		return empreendimentoSiga;
	}

	public void setEmpreendimentoSiga(EmpreendimentoSiga empreendimentoSiga) {
		this.empreendimentoSiga = empreendimentoSiga;
	}

	public Empreendimento getEmpreendimento() {
		return empreendimento;
	}

	public void setEmpreendimento(Empreendimento empreendimento) {
		this.empreendimento = empreendimento;
	}

	public int getEmpreendimentoNatuur() {
		return empreendimentoNatuur;
	}

	public boolean isNatuur() {
		return natuur;
	}

	public void setNatuur(boolean natuur) {
		this.natuur = natuur;
	}

	public boolean isExisteRama() {
		return existeRama;
	}

	public void setExisteRama(boolean existeRama) {
		this.existeRama = existeRama;
	}

	public String getAtividadeId() {
		return atividadeId;
	}

	public void setAtividadeId(String atividadeId) {
		this.atividadeId = atividadeId;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public String getDescricaoAtividade() {
		return descricaoAtividade;
	}

	public void setDescricaoAtividade(String descricaoAtividade) {
		this.descricaoAtividade = descricaoAtividade;
	}

	public String getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(String tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public AtividadeSiga getAtividadeSiga() {
		return atividadeSiga;
	}

	public void setAtividadeSiga(AtividadeSiga atividadeSiga) {
		this.atividadeSiga = atividadeSiga;
	}

	public String getIdTipoProcesso() {
		return idTipoProcesso;
	}

	public void setIdTipoProcesso(String idTipoProcesso) {
		this.idTipoProcesso = idTipoProcesso;
	}

	public String getDocumentoCliente() {
		return documentoCliente;
	}

	public void setDocumentoCliente(String documentoCliente) {
		this.documentoCliente = documentoCliente;
	}

}