package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="apoio_tecnico", schema="rh")
public class ApoioTecnico implements Serializable{
	
	private static final long serialVersionUID = -4411729545477180084L;


	@Id
	@SequenceGenerator(sequenceName="rh.seq_setor", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Date dataCadastro;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_fim")
	private Date dataFim;
	
	@Column(name = "setor_antigo")
	private Long setorAntigo;

	//------------------------------------------------Relacionamentos---------------------------------------------
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario = new  Funcionario();
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "listApoioTecnico")
	private List<Setor> listSetor = new ArrayList<Setor>();

	//------------------------------------------------Gets e Setters---------------------------------------------
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public List<Setor> getListSetor() {
		return listSetor;
	}

	public void setListSetor(List<Setor> listSetor) {
		this.listSetor = listSetor;
	}

	/**
	 * Setor que o apoio pertenceu antes de ser excluído.s
	 * @return
	 */
	public Long getSetorAntigo() {
		return setorAntigo;
	}

	public void setSetorAntigo(Long setorAntigo) {
		this.setorAntigo = setorAntigo;
	} 
	
	
}