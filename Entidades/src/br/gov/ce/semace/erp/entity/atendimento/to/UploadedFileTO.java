package br.gov.ce.semace.erp.entity.atendimento.to;

import java.io.Serializable;

import br.gov.ce.semace.erp.entity.geral.Atividade;

public class UploadedFileTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4574967963693892085L;

	private Atividade atividade;

//	private UploadedFile file;

	public UploadedFileTO() {}

//	public UploadedFileTO(Atividade atividade, UploadedFile shape) {
//		super();
//		this.atividade = atividade;
//		this.file = shape;
//	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

//	public UploadedFile getFile() {
//		return file;
//	}
//
//	public void setFile(UploadedFile file) {
//		this.file = file;
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((atividade == null) ? 0 : atividade.hashCode());
		result = prime * result
//				+ ((file == null) ? 0 : file.hashCode())
				;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof UploadedFileTO)) {
			return false;
		}
		UploadedFileTO other = (UploadedFileTO) obj;
		if (atividade == null) {
			if (other.atividade != null) {
				return false;
			}
		} else if (!atividade.getId().equals(other.atividade.getId())) {
			return false;
		}
//		if (file == null) {
//			if (other.file != null) {
//				return false;
//			}
//		} else if (!file.getFileName().equals(other.file.getFileName())) {
//			return false;
//		}
		return true;
	}

}
