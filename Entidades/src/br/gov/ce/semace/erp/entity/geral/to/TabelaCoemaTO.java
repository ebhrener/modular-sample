package br.gov.ce.semace.erp.entity.geral.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.gov.ce.semace.erp.entity.geral.AtividadeClassificacaoGrandeza;

public class TabelaCoemaTO implements Serializable{

	private static final long serialVersionUID = 7911188519627926401L;

	private GrandezaClassificacaoTO grandezaClassificacaoTOX = new GrandezaClassificacaoTO();

	private GrandezaClassificacaoTO grandezaClassificacaoTOY = new GrandezaClassificacaoTO();

	private List<AtividadeClassificacaoGrandeza> listClassificacaoGrandezaGrupo = new ArrayList<AtividadeClassificacaoGrandeza>();

	private Integer identificadorTabela;

	private boolean selecionado = false;

//	private boolean exibirTabela = false;


	public String getDescricaoTabelaCoema(){
		String retornoX = retornaNomeGrupo(grandezaClassificacaoTOX);
		String retornoY = retornaNomeGrupo(grandezaClassificacaoTOY);
		if(retornoX != null && !retornoX.isEmpty() && retornoY != null && !retornoY.isEmpty()){
			return retornoX + " / " + retornoY;
		}else if(retornoX != null && !retornoX.isEmpty()){
			return retornoX;
		}else{
			return retornoY;
		}
	}

	private String retornaNomeGrupo(GrandezaClassificacaoTO grandezaClassificacaoTO){
		if(grandezaClassificacaoTO.getAtividadeClassificacaoGrandezaSelecionada() != null && grandezaClassificacaoTO.getAtividadeClassificacaoGrandezaSelecionada().getId() != null){
			if(grandezaClassificacaoTO.getListaComboClassificacao() != null && !grandezaClassificacaoTO.getListaComboClassificacao().isEmpty()){
				return grandezaClassificacaoTO.getListaComboClassificacao().get(0).getGrandeza().getDescricao();
			}
			return grandezaClassificacaoTO.getAtividadeClassificacaoGrandezaSelecionada().getGrandeza() != null ? grandezaClassificacaoTO.getAtividadeClassificacaoGrandezaSelecionada().getGrandeza().getDescricao() : "";
		}else if(grandezaClassificacaoTO.getListaComboClassificacao() != null && !grandezaClassificacaoTO.getListaComboClassificacao().isEmpty()){
			return grandezaClassificacaoTO.getListaComboClassificacao().get(0).getGrandeza().getDescricao();
		}
		return "";
	}

	public GrandezaClassificacaoTO getGrandezaClassificacaoTOX() {
		return grandezaClassificacaoTOX;
	}

	public void setGrandezaClassificacaoTOX(GrandezaClassificacaoTO grandezaClassificacaoTOX) {
		this.grandezaClassificacaoTOX = grandezaClassificacaoTOX;
	}

	public GrandezaClassificacaoTO getGrandezaClassificacaoTOY() {
		return grandezaClassificacaoTOY;
	}

	public void setGrandezaClassificacaoTOY(GrandezaClassificacaoTO grandezaClassificacaoTOY) {
		this.grandezaClassificacaoTOY = grandezaClassificacaoTOY;
	}

	public List<AtividadeClassificacaoGrandeza> getListClassificacaoGrandezaGrupo() {
		return listClassificacaoGrandezaGrupo;
	}

	public void setListClassificacaoGrandezaGrupo(List<AtividadeClassificacaoGrandeza> listClassificacaoGrandezaGrupo) {
		this.listClassificacaoGrandezaGrupo = listClassificacaoGrandezaGrupo;
	}

	public Integer getIdentificadorTabela() {
		return identificadorTabela;
	}

	public void setIdentificadorTabela(Integer identificadorTabela) {
		this.identificadorTabela = identificadorTabela;
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

//	public boolean isExibirTabela() {
//		return exibirTabela;
//	}
//
//	public void setExibirTabela(boolean exibirTabela) {
//		this.exibirTabela = exibirTabela;
//	}

}
