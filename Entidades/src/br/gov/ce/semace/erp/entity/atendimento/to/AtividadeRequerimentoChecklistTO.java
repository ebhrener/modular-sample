package br.gov.ce.semace.erp.entity.atendimento.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.AgendamentoRequerimento;
import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.licenciamento.CheckList;

public class AtividadeRequerimentoChecklistTO implements Serializable {

	private static final long serialVersionUID = 6958342229343600228L;

	private Long id;

	private AgendamentoRequerimento agendamentoRequerimento;

	private Atividade atividade;

	private CheckList checklist;

	private Boolean isento;

	private List<CheckList> listChecklist = new ArrayList<CheckList>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AgendamentoRequerimento getAgendamentoRequerimento() {
		return agendamentoRequerimento;
	}

	public void setAgendamentoRequerimento(
			AgendamentoRequerimento agendamentoRequerimento) {
		this.agendamentoRequerimento = agendamentoRequerimento;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public CheckList getChecklist() {
		return checklist;
	}

	public void setChecklist(CheckList checklist) {
		this.checklist = checklist;
	}

	public Boolean getIsento() {
		return isento;
	}

	public void setIsento(Boolean isento) {
		this.isento = isento;
	}

	public List<CheckList> getListChecklist() {
		return listChecklist;
	}

	public void setListChecklist(List<CheckList> listChecklist) {
		this.listChecklist = listChecklist;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((agendamentoRequerimento == null) ? 0
						: agendamentoRequerimento.hashCode());
		result = prime * result
				+ ((atividade == null) ? 0 : atividade.hashCode());
		result = prime * result
				+ ((checklist == null) ? 0 : checklist.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isento == null) ? 0 : isento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		AtividadeRequerimentoChecklistTO other = (AtividadeRequerimentoChecklistTO) obj;
		Boolean retorno = false;
		if(agendamentoRequerimento != null && other.getAgendamentoRequerimento() != null){
			if(atividade != null && checklist != null && other.getAtividade() != null && other.getChecklist() != null){
				retorno = atividade.getId().equals(other.getAtividade().getId()) && checklist.getId().equals(other.getChecklist().getId());
			} else if(atividade == null && checklist != null && other.getChecklist() != null && other.getAtividade() == null){
				retorno = checklist.getId().equals(other.getChecklist().getId());
			} else if (atividade != null && checklist == null && other.getAtividade() != null && other.getChecklist()== null) {
				retorno = atividade.getId().equals(other.getAtividade().getId());
			}
			retorno = retorno && agendamentoRequerimento.getId().equals(other.getAgendamentoRequerimento().getId());


		} else if(id != null && other.getId() != null){
			retorno = id.equals(other.getId());
		}

		return retorno;

	}

}
