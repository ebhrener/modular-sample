package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "financeiro", name = "tipo_servico")
public class TipoServico implements Serializable{

	private static final long serialVersionUID = -38361722618086017L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_tipo_servico", name = "SEQ_TIPO_SERVICO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TIPO_SERVICO")
	private Long id;

	private String descricao;

	@Column(name="codigo_produto")
	private Integer codigoProduto;

	private Boolean ativo;

	// Relacionamentos

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_receita_id")
	private TipoReceita tipoReceita;

	@Transient
	public String getAtivoFormatado(){
		if (ativo != null) {
			if (ativo == true) {
				return "Sim";
			}
			return "Não";
		}

		return "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Integer getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(Integer codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public TipoReceita getTipoReceita() {
		return tipoReceita;
	}

	public void setTipoReceita(TipoReceita tipoReceita) {
		this.tipoReceita = tipoReceita;
	}

}
