package br.gov.ce.semace.erp.entity.atendimento.to;

import java.io.Serializable;

import br.gov.ce.semace.erp.entity.protocolo.TipoAnexo;

public class DocumentoUploadedTO implements Serializable {

	private static final long serialVersionUID = -8813172602790719225L;

//	private UploadedFile uploadedFile;

	private TipoAnexo tipoAnexo;

	public DocumentoUploadedTO() {}

//	public DocumentoUploadedTO(UploadedFile uploadedFile, TipoAnexo tipoAnexo) {
//		super();
//		this.uploadedFile = uploadedFile;
//		this.tipoAnexo = tipoAnexo;
//	}
//
//	public UploadedFile getUploadedFile() {
//		return uploadedFile;
//	}
//
//	public void setUploadedFile(UploadedFile uploadedFile) {
//		this.uploadedFile = uploadedFile;
//	}

	public TipoAnexo getTipoAnexo() {
		return tipoAnexo;
	}

	public void setTipoAnexo(TipoAnexo tipoAnexo) {
		this.tipoAnexo = tipoAnexo;
	}

}
