package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "laboratorio", name = "parametro_laboratorial_servico_laboratorial")
public class ParametroLaboratorialServicoLaboratorial implements Serializable {

	private static final long serialVersionUID = -1221075827457027275L;

	@Id
	@SequenceGenerator(sequenceName = "laboratorio.seq_parametro_laboratorial_servico_laboratorial", name = "SEQ_PARAMETRO_LABORATORIAL_SERVICO_LABORATORIAL", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PARAMETRO_LABORATORIAL_SERVICO_LABORATORIAL")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servico_laboratorial_id")
	private ServicoLaboratorial servicoLaboratorial;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parametro_laboratorial_id")
	private ParametroLaboratorial parametroLaboratorial;
	
	private String descricao;
	
	private BigDecimal valor;
	
	private boolean metal = false;

	@Override
	public ParametroLaboratorialServicoLaboratorial clone() throws CloneNotSupportedException {
		ParametroLaboratorialServicoLaboratorial object = new ParametroLaboratorialServicoLaboratorial();
		object.setDescricao(this.descricao);
		object.setId(this.id);
		object.setMetal(this.metal);
		object.setParametroLaboratorial(this.parametroLaboratorial);
		object.setServicoLaboratorial(this.servicoLaboratorial);
		object.setValor(this.valor);
		return object;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ServicoLaboratorial getServicoLaboratorial() {
		return servicoLaboratorial;
	}

	public void setServicoLaboratorial(ServicoLaboratorial servicoLaboratorial) {
		this.servicoLaboratorial = servicoLaboratorial;
	}

	public ParametroLaboratorial getParametroLaboratorial() {
		return parametroLaboratorial;
	}

	public void setParametroLaboratorial(ParametroLaboratorial parametroLaboratorial) {
		this.parametroLaboratorial = parametroLaboratorial;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public boolean getMetal() {
		return metal;
	}

	public void setMetal(boolean metal) {
		this.metal = metal;
	}

}
