package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Classe que representa um boleto oriundo do banco SIGA.
 * Atributos criados conforme modelagem já existente no banco.
 * @author leandros
 *
 */
@Entity
@Table(name="boleto_cef",schema="adm_siga")
public class BoletoCef implements Serializable {


	//srializacao
	private static final long serialVersionUID = 146090523908873098L;

	/**
	 * Atributos do objeto boleto na base siga
	 *
	 */

	@Column(name="boleto_id",unique=true)
	@SequenceGenerator(name="sq_boleto_cef",sequenceName="adm_siga.sq_boleto_cef")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="sq_boleto_cef")
	@Id
	private Integer id;

	@Column(name="spu_id")
	private String spu;

	@Column(name="nomsac")
	private String nomeSacado;

	@Column(name="cepsac")
	private String cepSacado;

	@Column(name="cidsac")
	private String municipioSacado;

	@Column(name="endsac")
	private String enderecoSacado;

	@Column(name="vcto")
	private String dataVencimento;

	@Column(name="dtproc")
	private String dataProcessamento;

	@Column(name="dtdoc")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataDocumento;

	@Column(name="dt_pagam")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataPagamento;

	@Column(name="nossonr")
	private String nossoNumero;

	@Column(name="nrdoc")
	private String numeroDocumento;

	@Column(name="valordoc")
	private String valorDocumento;

	@Column(name="pago")
	private Boolean pago;

	@Column(name="docsac")
	private String documentoSacado;

	@Column(name="referente")
	private String observacao;

	@Column(name="distancia")
	private Double distancia;

	@Column(name="pendente")
	private Boolean pendente;

	@Column(name="excluido")
	private Boolean excluido;

	@Column(name="tipo")
	private Integer tipo;

	@Column(name="vl_pago")
	private String valorPago;

	@Column(name="acordo")
	private Integer acordo;

	@Column(name="devolvido")
	private Boolean devolvido;

	@Column(name="agendamento_id")
	private Integer agendamentoId;

	@Column(name="spu_devolucao")
	private String spuDevolucao;

	@Column(name="usuario_devolucao")
	private String usuarioDevolucao;

	@Column(name="data_devolucao")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataDevolucao;

	@Column(name="tipo_boleto_id")
	private Integer tipoBoletoId;

	@Column(name="identificador")
	private String identificador;

	@Column(name="nossonr_digito")
	private String nossoNumeroDigito;

	@Column(name="pago_parcialmente")
	private Boolean pagoParcialmente;

	@Column(name="processo_id")
	private String processoId;

	@Column(name="deposito")
	private boolean deposito;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public String getNomeSacado() {
		return nomeSacado;
	}

	public void setNomeSacado(String nomeSacado) {
		this.nomeSacado = nomeSacado;
	}

	public String getCepSacado() {
		return cepSacado;
	}

	public void setCepSacado(String cepSacado) {
		this.cepSacado = cepSacado;
	}

	public String getMunicipioSacado() {
		return municipioSacado;
	}

	public void setMunicipioSacado(String municipioSacado) {
		this.municipioSacado = municipioSacado;
	}

	public String getEnderecoSacado() {
		return enderecoSacado;
	}

	public void setEnderecoSacado(String enderecoSacado) {
		this.enderecoSacado = enderecoSacado;
	}

	public String getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataDocumento() {
		return dataDocumento;
	}

	public void setDataDocumento(Date dataDocumento) {
		this.dataDocumento = dataDocumento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public String getNossoNumero() {
		return nossoNumero;
	}

	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getValorDocumento() {
		return valorDocumento;
	}

	public void setValorDocumento(String valorDocumento) {
		this.valorDocumento = valorDocumento;
	}

	public Boolean getPago() {
		return pago;
	}

	public void setPago(Boolean pago) {
		this.pago = pago;
	}

	public String getDocumentoSacado() {
		return documentoSacado;
	}

	public void setDocumentoSacado(String documentoSacado) {
		this.documentoSacado = documentoSacado;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Double getDistancia() {
		return distancia;
	}

	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}

	public Boolean getPendente() {
		return pendente;
	}

	public void setPendente(Boolean pendente) {
		this.pendente = pendente;
	}

	public Boolean getExcluido() {
		return excluido;
	}

	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public String getValorPago() {
		return valorPago;
	}

	public void setValorPago(String valorPago) {
		this.valorPago = valorPago;
	}

	public Integer getAcordo() {
		return acordo;
	}

	public void setAcordo(Integer acordo) {
		this.acordo = acordo;
	}

	public Boolean getDevolvido() {
		return devolvido;
	}

	public void setDevolvido(Boolean devolvido) {
		this.devolvido = devolvido;
	}

	public Integer getAgendamentoId() {
		return agendamentoId;
	}

	public void setAgendamentoId(Integer agendamentoId) {
		this.agendamentoId = agendamentoId;
	}

	public String getSpuDevolucao() {
		return spuDevolucao;
	}

	public void setSpuDevolucao(String spuDevolucao) {
		this.spuDevolucao = spuDevolucao;
	}

	public String getUsuarioDevolucao() {
		return usuarioDevolucao;
	}

	public void setUsuarioDevolucao(String usuarioDevolucao) {
		this.usuarioDevolucao = usuarioDevolucao;
	}

	public Date getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(Date dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public Integer getTipoBoletoId() {
		return tipoBoletoId;
	}

	public void setTipoBoletoId(Integer tipoBoletoId) {
		this.tipoBoletoId = tipoBoletoId;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(String dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public String getNossoNumeroDigito() {
		return nossoNumeroDigito;
	}

	public void setNossoNumeroDigito(String nossoNumeroDigito) {
		this.nossoNumeroDigito = nossoNumeroDigito;
	}

	public Boolean getPagoParcialmente() {
		return pagoParcialmente;
	}

	public void setPagoParcialmente(Boolean pagoParcialmente) {
		this.pagoParcialmente = pagoParcialmente;
	}

	public String getProcessoId() {
		return processoId;
	}

	public void setProcessoId(String processoId) {
		this.processoId = processoId;
	}

	public boolean isDeposito() {
		return deposito;
	}

	public void setDeposito(boolean deposito) {
		this.deposito = deposito;
	}


}
