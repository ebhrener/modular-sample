package br.gov.ce.semace.erp.entity.licenciamento.to;

import java.io.Serializable;


public class ItemCheckListCheckListAtividadeTO implements Serializable {

	private static final long serialVersionUID = 2837759938305312384L;

	private Long itemCheckListID;
	private Long atividadeID;
	private Long checkListID;

	public Long getItemCheckListID() {
		return itemCheckListID;
	}
	public void setItemCheckListID(Long itemCheckListID) {
		this.itemCheckListID = itemCheckListID;
	}
	public Long getAtividadeID() {
		return atividadeID;
	}
	public void setAtividadeID(Long atividadeID) {
		this.atividadeID = atividadeID;
	}
	public Long getCheckListID() {
		return checkListID;
	}
	public void setCheckListID(Long checkListID) {
		this.checkListID = checkListID;
	}
}
