package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.semace.erp.entity.geral.Coema;
import br.gov.ce.semace.erp.enuns.Letra;


@Entity
@Table(schema = "financeiro", name = "configuracao_valor_ufirce")
public class ConfiguracaoValorUfirce implements Serializable{

	private static final long serialVersionUID = -8311637514742013049L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_configuracao_valor_ufir", name = "SEQ_CONFIGURACAO_VALOR_UFIR", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CONFIGURACAO_VALOR_UFIR")
	private Long id;

	private Letra letra;

	@Column(name = "quantidade_ufir", precision=10, scale=2)
	private BigDecimal quantidadeUfir;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tipo_taxa_id")
	private TipoTaxa tipoTaxa = new TipoTaxa();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="coema_id")
	private Coema coema = new Coema();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getQuantidadeUfir() {
		return quantidadeUfir;
	}

	public void setQuantidadeUfir(BigDecimal quantidadeUfir) {
		this.quantidadeUfir = quantidadeUfir;
	}

	public Letra getLetra() {
		return letra;
	}

	public void setLetra(Letra letra) {
		this.letra = letra;
	}

	public TipoTaxa getTipoTaxa() {
		return tipoTaxa;
	}

	public void setTipoTaxa(TipoTaxa tipoTaxa) {
		this.tipoTaxa = tipoTaxa;
	}

	public Coema getCoema() {
		return coema;
	}

	public void setCoema(Coema coema) {
		this.coema = coema;
	}

}
