package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.gov.ce.semace.erp.entity.seguranca.Usuario;
import br.gov.ce.semace.erp.entity.seguranca.UsuarioWeb;
import br.gov.ce.semace.erp.enuns.OperacaoDocumento;

@Entity
@Table(name = "historico_arquivo_documento", schema = "geral")
public class HistoricoArquivoDocumento implements Serializable {

	private static final long serialVersionUID = 1353584385640232279L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_historico_arquivo_documento", name = "SEQ_HISTORICO_ARQUIVO_DOCUMENTO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_HISTORICO_ARQUIVO_DOCUMENTO")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="documento_id")
	private Documento documento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="usuario_id")
	private Usuario usuario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="usuario_web_id")
	private UsuarioWeb usuarioWeb;

	@Column(name = "operacao_documento")
	private OperacaoDocumento operacaoDocumento;

	@Column(name = "data_cadastro", updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	@Column(name = "id_documento_pdf")
	private Long idDocumentoPdf;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public OperacaoDocumento getOperacaoDocumento() {
		return operacaoDocumento;
	}

	public void setOperacaoDocumento(OperacaoDocumento operacaoDocumento) {
		this.operacaoDocumento = operacaoDocumento;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Long getIdDocumentoPdf() {
		return idDocumentoPdf;
	}

	public void setIdDocumentoPdf(Long idDocumentoPdf) {
		this.idDocumentoPdf = idDocumentoPdf;
	}

	public UsuarioWeb getUsuarioWeb() {
		return usuarioWeb;
	}

	public void setUsuarioWeb(UsuarioWeb usuarioWeb) {
		this.usuarioWeb = usuarioWeb;
	}

}
