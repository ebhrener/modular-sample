package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="criacao_capa_processo", schema="protocolo")
public class CriacaoCapaProcesso implements Serializable {

	private static final long serialVersionUID = 6952627334956828613L;

	@Id
	@SequenceGenerator(sequenceName="protocolo.seq_criacao_capa_processo", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@Column(name = "spu")
	private String spu;

	@Column(nullable=false, name="data_cadastro")
	private Date dataCadastro;

	@Column(nullable=false)
	private Boolean processado;

	@Column(name="data_processado")
	private Date dataProcessado;

	@Column(name="last_error", columnDefinition="text")
	private String lastError;

	@Column(name="data_last_error")
	private Date dataLastError;

	@Column(name = "qtd_erros")
	private Integer qtdErros;

	public CriacaoCapaProcesso() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public Boolean getProcessado() {
		return processado;
	}

	public void setProcessado(Boolean processado) {
		this.processado = processado;
	}

	public String getLastError() {
		return lastError;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataLastError() {
		return dataLastError;
	}

	public void setDataLastError(Date dataLastError) {
		this.dataLastError = dataLastError;
	}

	public Date getDataProcessado() {
		return dataProcessado;
	}

	public void setDataProcessado(Date dataProcessado) {
		this.dataProcessado = dataProcessado;
	}

	public Integer getQtdErros() {
		return qtdErros;
	}

	public void setQtdErros(Integer qtdErros) {
		this.qtdErros = qtdErros;
	}

}
