package br.gov.ce.semace.erp.entity.contrato;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

/**
 *
 * Entidade que forma a lista de objetos do contrato
 *
 * @author Tiago Freire (tiago.freire@semace.ce.gov.br) 22/10/2013 15:44:31
 *
 */

@Entity
@Audited
@Table(name="objeto_contrato", schema="contrato")
public class ObjetoContrato implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -628217378610481118L;

	@Id
	@SequenceGenerator(sequenceName = "contrato.seq_objeto", name = "SEQOBJETOCONTRATO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQOBJETOCONTRATO")
	private Long id;

	@NotNull
	@Column(name = "descricao", nullable=false)
	private String descricao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="contrato_id", nullable = false )
	private Contrato contrato = new Contrato();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}


}
