package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "geral")
public class Coema implements Serializable {

	private static final long serialVersionUID = 5957225311133268032L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_coema", name = "geral.seq_coema", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geral.seq_coema")
	private Long id;

	private String descricao;

	@Column(name="data_inicio_coema")
	private Date dataInicioCoema;

	@Column(name="data_fim_coema")
	private Date dataFimCoema;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataInicioCoema() {
		return dataInicioCoema;
	}

	public void setDataInicioCoema(Date dataInicioCoema) {
		this.dataInicioCoema = dataInicioCoema;
	}

	public Date getDataFimCoema() {
		return dataFimCoema;
	}

	public void setDataFimCoema(Date dataFimCoema) {
		this.dataFimCoema = dataFimCoema;
	}

}
