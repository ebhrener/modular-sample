package br.gov.ce.semace.erp.entity.florestal.to;

import java.util.Date;

import br.gov.ce.semace.erp.enuns.StatusCadastroConsumoFlorestal;

/**
 * TO para receber os dados da consulta do relatório de CCF's vencidos por mais de 30 dias e que não possuem renovação.
 *
 * @author rodrigo.silva - Rodrigo Silva Oliveira - 27/04/2017 16:01:28
 */
public class RelatorioCadastroFlorestalVencidosTO {

	private String numeroCadastro;
	private String cpf_cnpj;
	private String nome_razaoSocial;
	private String endereco;
	private String municipio;
	private Integer qtdDiasVencidos;
	private Date dataValidade;
	private StatusCadastroConsumoFlorestal statusCadastro;

	public String getNumeroCadastro() {
		return numeroCadastro;
	}

	public void setNumeroCadastro(String numeroCadastro) {
		this.numeroCadastro = numeroCadastro;
	}

	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}

	public String getCpf_cnpj() {
		return cpf_cnpj;
	}

	public void setCpf_cnpj(String cpf_cnpj) {
		this.cpf_cnpj = cpf_cnpj;
	}

	public String getNome_razaoSocial() {
		return nome_razaoSocial;
	}

	public void setNome_razaoSocial(String nome_razaoSocial) {
		this.nome_razaoSocial = nome_razaoSocial;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public Integer getQtdDiasVencidos() {
		return qtdDiasVencidos;
	}

	public void setQtdDiasVencidos(Integer qtdDiasVencidos) {
		this.qtdDiasVencidos = qtdDiasVencidos;
	}

	public StatusCadastroConsumoFlorestal getStatusCadastro() {
		return statusCadastro;
	}

	public void setStatusCadastro(StatusCadastroConsumoFlorestal statusCadastro) {
		this.statusCadastro = statusCadastro;
	}
}