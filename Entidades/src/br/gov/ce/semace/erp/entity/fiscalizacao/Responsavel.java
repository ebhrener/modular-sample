package br.gov.ce.semace.erp.entity.fiscalizacao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema="fiscalizacao")
@SequenceGenerator(sequenceName = Responsavel.SEQUENCE_NAME, name = Responsavel.SEQUENCE_NAME, allocationSize=1)
public class Responsavel implements Serializable{

	/**
	 * SerialVersionUID generated by Eclipse
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 31/03/2016 13:42:53
	 */
	private static final long serialVersionUID = -4438970863746488016L;

	/**
	 * Nome da Sequence que existirá no banco, também que será  <i>conhecida</i> dentro da aplicação
	 * e nome do gerador para onde deve apontar o tipo de geração {@link GeneratedType#}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 30/03/2016 15:06:57
	 */
	public static final String SEQUENCE_NAME = "fiscalizacao.seq_responsavel";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = SEQUENCE_NAME)
	private Long id;

	private String nome;

	@Column(name="documento_identificacao")
	private String documentoIdentificacao;

	private String endereco;

	private String bairro;

	private String cep;

	private String municipio;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDocumentoIdentificacao() {
		return documentoIdentificacao;
	}
	public void setDocumentoIdentificacao(String documentoIdentificacao) {
		this.documentoIdentificacao = documentoIdentificacao;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

}
