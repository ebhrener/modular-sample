package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 *
 * @author Harisson Rafael [harissonrafael@gmail.com] - 08/07/2016 10:17:33
 *
 */
@Entity
@Audited
@Table(name = "funcionario_ponto", schema = "rh")
//@TypeDef(name="period", typeClass= JodaTypePeriodParser.class)
public class Ponto implements Serializable {

	private static final long serialVersionUID = -8838343063343656197L;

	@Id
	@SequenceGenerator(sequenceName = "rh.seq_funcionario_ponto", name = "seq_funcionario_ponto", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_funcionario_ponto")
	private Long id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;

	private String senha;

	private byte[] foto;

//	@Column(name = "carga_horaria")
//	@Type(type="period")
//	private Period cargaHoraria;

	@Column(name = "inicio_carga_horaria")
	private Date dataInicioCargaHoraria;

	@Column(name = "possui_carga_horaria")
	private Boolean possuiCargaHoraria;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

//	public Period getCargaHoraria() {
//		return cargaHoraria;
//	}
//
//	public void setCargaHoraria(Period cargaHoraria) {
//		this.cargaHoraria = cargaHoraria;
//	}

	public Date getDataInicioCargaHoraria() {
		return dataInicioCargaHoraria;
	}

	public void setDataInicioCargaHoraria(Date dataInicioCargaHoraria) {
		this.dataInicioCargaHoraria = dataInicioCargaHoraria;
	}

	public Boolean isPossuiCargaHoraria() {
		return possuiCargaHoraria;
	}

	public void setPossuiCargaHoraria(Boolean possuiCargaHoraria) {
		this.possuiCargaHoraria = possuiCargaHoraria;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Ponto other = (Ponto) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}