package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "arquivo_extra", schema = "public")
public class ArquivoExtra implements Serializable {

	private static final long serialVersionUID = -1988666685245326119L;

	@Id
	@SequenceGenerator(sequenceName = "seq_arquivo_extra", name = "seq_arquivo_extra", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_arquivo_extra")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "processo_id")
	private ProcessoGeo processo = new ProcessoGeo();

	@Column(nullable = false)
	private String nome;

	@Column(nullable = false)
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "org.hibernate.type.BinaryType")
	private byte[] dados;

//	private FieldHandler fieldHandler;

	@Column(nullable = false, name = "data_cadastro")
	private Date dataCadastro;

	public ArquivoExtra() {
		super();
	}

	public ArquivoExtra(String nome, byte[] dados, Date dataCadastro) {
		super();
		this.nome = nome;
		this.dados = dados;
		this.dataCadastro = dataCadastro;
	}

	public ArquivoExtra(ProcessoGeo processo, String nome, byte[] dados) {
		super();
		this.processo = processo;
		this.nome = nome;
		this.dados = dados;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProcessoGeo getProcesso() {
		return processo;
	}

	public void setProcesso(ProcessoGeo processo) {
		this.processo = processo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

//	public byte[] getDados() {
//		if (fieldHandler != null) {
//		   return (byte[]) fieldHandler.readObject(this, "dados", dados);
//		}
//
//		return dados;
//	}
//
//	public void setDados(byte[] dados) {
//		if (fieldHandler != null) {
//			this.dados = (byte[]) fieldHandler.writeObject(this, "dados", this.dados, dados);
//			return;
//		}
//
//		this.dados = dados;
//	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

//	public FieldHandler getFieldHandler() {
//		return fieldHandler;
//	}
//
//	public void setFieldHandler(FieldHandler fieldHandler) {
//		this.fieldHandler = fieldHandler;
//	}

}
