package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.TipoLaboratorioSistema;
import br.gov.ce.semace.erp.enuns.TipoOrdemColeta;

/**
 * @author victorsobreira
 *
 */
@Entity
@Audited
@Table(name="ordem_coleta", schema="laboratorio")
public class OrdemColeta implements Serializable {
	
	private static final long serialVersionUID = 1956123509054810609L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_ordem_coleta", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@Column(name = "tipo_laboratorio_sistema")
	private TipoLaboratorioSistema tipoLaboratorioSistema;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ordem_coleta_funcionario",
			schema = "laboratorio", 
			joinColumns = @JoinColumn(name = "ordem_coleta_id", nullable=false), 
			inverseJoinColumns = @JoinColumn(name = "funcionario_id", nullable=false))
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private List<Funcionario> funcionarios = new ArrayList<Funcionario>();
	
	@ManyToOne(fetch= FetchType.LAZY, optional = false)
	@JoinColumn(name="documento_id")
	private Documento numeroOrdemColeta = new Documento();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ordemColeta", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Coleta> coletas = new ArrayList<Coleta>();
	
	@Temporal(TemporalType.DATE)
	@Column(name= "data_inicial")
	private Date dataInicial;
	
	@Temporal(TemporalType.DATE)
	@Column(name= "data_final")
	private Date dataFinal;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_cadastro", nullable=false)
	private Date dataCadastro;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_finalizacao")
	private Date dataFinalizacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_cancelamento")
	private Date dataCancelamento;
	
	@Column(name="motivo_cancelamento", columnDefinition="Text")
	private String motivoCancelamento;
	
	@Column(name="tipo_ordem_coleta")
	private TipoOrdemColeta tipoOrdemColeta;
	
	@OneToOne(mappedBy="ordemColeta")
	private Analise analise;
	
	@Transient
	private Funcionario funcionario = new Funcionario();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public Documento getNumeroOrdemColeta() {
		return numeroOrdemColeta;
	}

	public void setNumeroOrdemColeta(Documento numeroOrdemColeta) {
		this.numeroOrdemColeta = numeroOrdemColeta;
	}

	public TipoOrdemColeta getTipoOrdemColeta() {
		return tipoOrdemColeta;
	}

	public void setTipoOrdemColeta(TipoOrdemColeta tipoOrdemColeta) {
		this.tipoOrdemColeta = tipoOrdemColeta;
	}

	public List<Coleta> getColetas() {
		return coletas;
	}

	public void setColetas(List<Coleta> coletas) {
		this.coletas = coletas;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataFinalizacao() {
		return dataFinalizacao;
	}

	public void setDataFinalizacao(Date dataFinalizacao) {
		this.dataFinalizacao = dataFinalizacao;
	}

	public Date getDataCancelamento() {
		return dataCancelamento;
	}

	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public String getMotivoCancelamento() {
		return motivoCancelamento;
	}

	public void setMotivoCancelamento(String motivoCancelamento) {
		this.motivoCancelamento = motivoCancelamento;
	}

	public TipoLaboratorioSistema getTipoLaboratorioSistema() {
		return tipoLaboratorioSistema;
	}

	public void setTipoLaboratorioSistema(
			TipoLaboratorioSistema tipoLaboratorioSistema) {
		this.tipoLaboratorioSistema = tipoLaboratorioSistema;
	}

	public Analise getAnalise() {
		return analise;
	}

	public void setAnalise(Analise analise) {
		this.analise = analise;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}
