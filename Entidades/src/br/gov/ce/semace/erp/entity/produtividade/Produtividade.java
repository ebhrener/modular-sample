package br.gov.ce.semace.erp.entity.produtividade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;

@Entity
@Audited
@Table(name = "produtividade", schema = "produtividade", uniqueConstraints=@UniqueConstraint(columnNames={"funcionario_id","mes","ano"}))
public class Produtividade implements Serializable{

	private static final long serialVersionUID = -2299107723491077623L;
	
	@Id @SequenceGenerator(sequenceName="produtividade.seq_produtividade", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	private Integer mes;
	
	private Integer ano;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario = new Funcionario();
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "produtividade", orphanRemoval = true)
	private List<RespostaAtividade> respostasProdutividade = new ArrayList<RespostaAtividade>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public List<RespostaAtividade> getRespostasProdutividade() {
		return respostasProdutividade;
	}

	public void setRespostasProdutividade(
			List<RespostaAtividade> respostasProdutividade) {
		this.respostasProdutividade = respostasProdutividade;
	}
}
