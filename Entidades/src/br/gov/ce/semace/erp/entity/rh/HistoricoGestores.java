package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;


@Entity
@Audited
@Table(name="historico_gestores", schema="rh")
public class HistoricoGestores implements Serializable{
	
	private static final long serialVersionUID = 27434321003187421L;

	@Id
	@SequenceGenerator(sequenceName="rh.seq_historico_gestores", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;	
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="responsavel_id")
	private Funcionario responsavel;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio_gestao")
	private Date dataInicioGestao;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_fim_gestao")
	private Date dataFimGestao;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="setor_id")
	private Setor setor = new Setor();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Funcionario responsavel) {
		this.responsavel = responsavel;
	}

	public Date getDataInicioGestao() {
		return dataInicioGestao;
	}

	public void setDataInicioGestao(Date dataInicioGestao) {
		this.dataInicioGestao = dataInicioGestao;
	}

	public Date getDataFimGestao() {
		return dataFimGestao;
	}

	public void setDataFimGestao(Date dataFimGestao) {
		this.dataFimGestao = dataFimGestao;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}
}
