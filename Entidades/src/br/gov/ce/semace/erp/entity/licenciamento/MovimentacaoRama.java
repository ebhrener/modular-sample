package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.SituacaoRama;

@Entity
@Audited
@Table(name = "movimentacao_rama", schema = "licenciamento")
public class MovimentacaoRama implements Serializable {

	private static final long serialVersionUID = 5924095845259618758L;

	@Id
	@SequenceGenerator(sequenceName = "licenciamento.seq_movimentacao_rama", name = "SEQ_MOVIMENTACAO_RAMA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MOVIMENTACAO_RAMA")
	private Long id;

	@Column(name = "usuario",columnDefinition="text")
	private String usuario;

	@Column(name = "acao_feita",columnDefinition="text")
	private String acao;

	@Column(name = "status")
	private SituacaoRama statusRama;

	@Column(name = "data")
	private Date data;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rama_id")
	private Rama rama;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public SituacaoRama getStatusRama() {
		return statusRama;
	}

	public void setStatusRama(SituacaoRama statusRama) {
		this.statusRama = statusRama;
	}

	public Rama getRama() {
		return rama;
	}

	public void setRama(Rama rama) {
		this.rama = rama;
	}
}
