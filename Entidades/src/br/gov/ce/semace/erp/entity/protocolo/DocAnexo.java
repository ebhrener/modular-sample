package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.enuns.StatusDocumento;

@Entity
@Audited
@Table(name="doc_anexo", schema="protocolo")
public class DocAnexo implements Serializable{

	private static final long serialVersionUID = 4759522870346759989L;

	public static final String DEFAULT_OBS = "Doc Anexo criado automaticamente devido anexo de arquivo de processo";
	public static final String OBS_DOC_ANEXO_PENDENCIA = "Doc Anexo criado automaticamente devido atendimento de pendência de processo";

	@Id
	@SequenceGenerator(sequenceName ="protocolo.seq_doc_anexo", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(nullable=false, columnDefinition = "text")
	private String observacao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="processo_id")
	private Processo processo = new Processo();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="documento_id")
	private Documento documento = new Documento();

	@Column(name="status_doc_anexo")
	private StatusDocumento statusDocAnexo;

	/**
	 * {@link List}a de {@link ArquivoDocAnexo} referente aos arquivos anexados.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 8, 2014 3:44:18 PM
	 */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "docAnexo")
	private List<ArquivoDocAnexo> listArquivoDocAnexo = new ArrayList<ArquivoDocAnexo>();

	@Column(name="is_from_pendencia")
	private boolean isFromPendencia;

	/////////////////TRANSIENTE/////////////////////////////

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDocAnexo} é
	 * 	do tipo <b>ARQUIVADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 20/06/2013 14:38:54
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDocAnexo} seja
	 * 	<b>ARQUIVADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDocumento#isArquivado()
	 */
	@Transient
	public boolean isArquivado(){
		return this.statusDocAnexo.isArquivado();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDocAnexo} é
	 * 	do tipo <b>CANCELADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 20/06/2013 14:39:20
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDocAnexo} seja
	 * 	<b>CANCELADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDocumento#isCancelado()
	 */
	@Transient
	public boolean isCancelado(){
		return this.statusDocAnexo.isCancelado();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDocAnexo} é
	 * 	do tipo <b>CRIADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 20/06/2013 14:40:00
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDocAnexo} seja
	 * 	<b>CRIADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDocumento#isCriado()
	 */
	@Transient
	public boolean isCriado(){
		return this.statusDocAnexo.isCriado();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDocAnexo} é
	 * 	do tipo <b>ENCERRADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 20/06/2013 14:41:29
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDocAnexo} seja
	 * 	<b>ENCERRADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDocumento#isEncerrado()
	 */
	@Transient
	public boolean isEncerrado(){
		return this.statusDocAnexo.isEncerrado();
	}

	/**
	 * Método {@link Transient}e para verificar se o {@link #statusDocAnexo} é
	 * 	do tipo <b>PROTOCOLADO</b>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 20/06/2013 14:42:28
	 *
	 * @return
	 * 		Retorna <code>true</code> caso o {@link #statusDocAnexo} seja
	 * 	<b>PROTOCOLADO</b>. Caso contrário, retorna <code>false</code>
	 *
	 * @see StatusDocumento#isProtocolado()
	 */
	@Transient
	public boolean isProtocolado(){
		return this.statusDocAnexo.isProtocolado();
	}

	@Transient
	private Date dataInicial;

	@Transient
	private Date dataFinal;

	@Transient
	private List<ArquivoDocAnexo> listAddedArquivos = new ArrayList<ArquivoDocAnexo>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public StatusDocumento getStatusDocAnexo() {
		return statusDocAnexo;
	}

	public void setStatusDocAnexo(StatusDocumento statusDocAnexo) {
		this.statusDocAnexo = statusDocAnexo;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	/**
	 * @return the arquivos
	 */
	public List<ArquivoDocAnexo> getListArquivoDocAnexo() {
		return listArquivoDocAnexo;
	}

	/**
	 * @param arquivos the arquivos to set
	 */
	public void setListArquivoDocAnexo(List<ArquivoDocAnexo> listArquivoDocAnexo) {
		this.listArquivoDocAnexo = listArquivoDocAnexo;
	}

	public boolean isFromPendencia() {
		return isFromPendencia;
	}

	public void setFromPendencia(boolean isFromPendencia) {
		this.isFromPendencia = isFromPendencia;
	}

	public List<ArquivoDocAnexo> getListAddedArquivos() {
		return listAddedArquivos;
	}

	public void setListAddedArquivos(List<ArquivoDocAnexo> listAddedArquivos) {
		this.listAddedArquivos = listAddedArquivos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DocAnexo other = (DocAnexo) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Transient
	public ArquivoDocAnexo addArquivoDocAnexo(ArquivoDocAnexo arquivoDocAnexo) {
		listArquivoDocAnexo.add(arquivoDocAnexo);
		return arquivoDocAnexo;
	}
}