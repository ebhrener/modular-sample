package br.gov.ce.semace.erp.entity.atendimento.requerimento.to;

import java.io.Serializable;

public class ProcessoTO implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = -8346972688139779669L;

	private Long id;
	private String spu;
	private Integer sistema;
	private boolean sincronizado;
	private Integer qtdErro;
	private String nomeAutor;
	private String documento;
	private String setorViproc;
	private String tipoProcessoViproc;
	private String descricao;
	private String lastRemoteError;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public Integer getSistema() {
		return sistema;
	}

	public void setSistema(Integer sistema) {
		this.sistema = sistema;
	}

	public boolean isSincronizado() {
		return sincronizado;
	}

	public void setSincronizado(boolean sincronizado) {
		this.sincronizado = sincronizado;
	}

	public Integer getQtdErro() {
		return qtdErro;
	}

	public void setQtdErro(Integer qtdErro) {
		this.qtdErro = qtdErro;
	}

	public String getNomeAutor() {
		return nomeAutor;
	}

	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getSetorViproc() {
		return setorViproc;
	}

	public void setSetorViproc(String setorViproc) {
		this.setorViproc = setorViproc;
	}

	public String getTipoProcessoViproc() {
		return tipoProcessoViproc;
	}

	public void setTipoProcessoViproc(String tipoProcessoViproc) {
		this.tipoProcessoViproc = tipoProcessoViproc;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getLastRemoteError() {
		return lastRemoteError;
	}

	public void setLastRemoteError(String lastRemoteError) {
		this.lastRemoteError = lastRemoteError;
	}

}
