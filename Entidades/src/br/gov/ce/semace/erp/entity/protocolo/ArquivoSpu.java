package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "arquivo_spu", schema = "protocolo")
public class ArquivoSpu implements Serializable, Comparable<ArquivoSpu> {

	/**
	 *
	 */
	private static final long serialVersionUID = 5086060719830054775L;

	@Id
	@SequenceGenerator(sequenceName = "protocolo.seq_arquivo_spu", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	private Integer ordem;

	private String spu;

	private Long indiceGED;

	private String nome;

	private Integer tamanho;

	private Integer paginas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public Long getIndiceGED() {
		return indiceGED;
	}

	public void setIndiceGED(Long indiceGED) {
		this.indiceGED = indiceGED;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getTamanho() {
		return tamanho;
	}

	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}

	public Integer getPaginas() {
		return paginas;
	}

	public void setPaginas(Integer paginas) {
		this.paginas = paginas;
	}

	@Override
	public int compareTo(ArquivoSpu o) {
		// TODO Auto-generated method stub
		return this.getOrdem().compareTo(o.getOrdem());
	}

}