package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;
import br.gov.ce.semace.erp.enuns.Setor;

@Entity
@Audited
@Table(name="praia", schema="laboratorio")
public class Praia implements Serializable{

	private static final long serialVersionUID = -2696069948697508731L;	
	
	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_praia", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	private String nome;
	private Setor setor;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cidade_id")
	private Cidade cidade = new Cidade();

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	public Setor getSetor() {
		return setor;
	}
	public void setSetor(Setor setor) {
		this.setor = setor;
	}
	

}
