package br.gov.ce.semace.erp.entity.transporte;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
/**
 *
 * @author Tiago Nascimento - vilard@gmail.com - 14/05/2014 - 13:28:10
 *
 */
@Entity
@Audited
@Table(schema = "transporte", name = "tipo_objetivo")
public class TipoObjetivo implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7766885411285559826L;

	@Id
	@SequenceGenerator(sequenceName = "transporte.seq_tipo_objetivo", name = "SEQ_TIPO_OBJETIVO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TIPO_OBJETIVO")
	private Long id;

	private String descricao;

	@Column(nullable = false)
	private boolean ativo = true;

	@Transient
	public String getDetalheAtivo(){

		if (this.ativo){
			return "Sim";
		}

		return "Não";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
