package br.gov.ce.semace.erp.entity.planoviagem;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.transporte.SolicitacaoTransporte;

/**
 * Entidade {@link SolicitacaoViagem} do esquema Viagem.
 *
 * @author Harisson Rafael [harissonrafael@gmail.com] - 30/07/2014 15:59:01
 *
 */
@Entity
@Audited
@Table(schema = "plano_viagem", name = "solicitacao_viagem")
public class SolicitacaoViagem implements Serializable {

	private static final long serialVersionUID = 2356371348872665773L;

	@Id
	@SequenceGenerator(sequenceName = "plano_viagem.seq_solicitacao_viagem", name = "SEQ_SOLICITACAO_VIAGEM", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SOLICITACAO_VIAGEM")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "documento_id")
	private Documento documento = new Documento();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "solicitacao_transporte_id")
	private SolicitacaoTransporte solicitacaoTransporte;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public SolicitacaoTransporte getSolicitacaoTransporte() {
		return solicitacaoTransporte;
	}

	public void setSolicitacaoTransporte(SolicitacaoTransporte solicitacaoTransporte) {
		this.solicitacaoTransporte = solicitacaoTransporte;
	}
}