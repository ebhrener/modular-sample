package br.gov.ce.semace.erp.entity.atendimento.requerimento.to;

import java.util.ArrayList;
import java.util.List;

import br.gov.ce.semace.erp.entity.laboratorio.AnaliseLaboratorial;
import br.gov.ce.semace.erp.entity.laboratorio.ParametroLaboratorial;

public class ParametroLaboratorialTO {

	private AnaliseLaboratorial analiseLaboratorial = new AnaliseLaboratorial();

	private ParametroLaboratorial distancia = new ParametroLaboratorial();

	private ParametroLaboratorial parametroLaboratorial = new ParametroLaboratorial();

	private boolean desejaAdicionarParametroLaboratorial = false;

	private List<ParametroLaboratorial> listParametroLaboratorial = new ArrayList<ParametroLaboratorial>();

	public AnaliseLaboratorial getAnaliseLaboratorial() {
		return analiseLaboratorial;
	}

	public void setAnaliseLaboratorial(AnaliseLaboratorial analiseLaboratorial) {
		this.analiseLaboratorial = analiseLaboratorial;
	}

	public ParametroLaboratorial getDistancia() {
		return distancia;
	}

	public void setDistancia(ParametroLaboratorial distancia) {
		this.distancia = distancia;
	}

	public ParametroLaboratorial getParametroLaboratorial() {
		return parametroLaboratorial;
	}

	public void setParametroLaboratorial(ParametroLaboratorial parametroLaboratorial) {
		this.parametroLaboratorial = parametroLaboratorial;
	}

	public boolean isDesejaAdicionarParametroLaboratorial() {
		return desejaAdicionarParametroLaboratorial;
	}

	public void setDesejaAdicionarParametroLaboratorial(boolean desejaAdicionarParametroLaboratorial) {
		this.desejaAdicionarParametroLaboratorial = desejaAdicionarParametroLaboratorial;
	}

	public List<ParametroLaboratorial> getListParametroLaboratorial() {
		return listParametroLaboratorial;
	}

	public void setListParametroLaboratorial(List<ParametroLaboratorial> listParametroLaboratorial) {
		this.listParametroLaboratorial = listParametroLaboratorial;
	}

}
