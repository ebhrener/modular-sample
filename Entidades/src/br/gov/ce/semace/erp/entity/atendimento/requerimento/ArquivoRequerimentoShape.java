package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.ClassificacaoShape;

@Entity
@Audited
@Table(name="arquivo_requerimento_shape", schema="atendimento")
public class ArquivoRequerimentoShape extends ArquivoRequerimento {

	private static final long serialVersionUID = -5537184314595281041L;

	@Column(nullable = false)
	private boolean original;

	@Enumerated(EnumType.ORDINAL)
	@Column(name="classificacao_shape")
	private ClassificacaoShape classificacaoShape;

	public boolean isOriginal() {
		return original;
	}

	public void setOriginal(boolean original) {
		this.original = original;
	}

	public ClassificacaoShape getClassificacaoShape() {
		return classificacaoShape;
	}

	public void setClassificacaoShape(ClassificacaoShape classificacaoShape) {
		this.classificacaoShape = classificacaoShape;
	}

	public boolean isArea() {
		return this.classificacaoShape.isArea();
	}

	public boolean isEquipamento() {
		return this.classificacaoShape.isEquipamento();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((classificacaoShape == null) ? 0 : classificacaoShape.hashCode());
		result = prime * result + (original ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ArquivoRequerimentoShape other = (ArquivoRequerimentoShape) obj;
		if (classificacaoShape != other.classificacaoShape) {
			return false;
		}
		if (original != other.original) {
			return false;
		}
		return true;
	}

}