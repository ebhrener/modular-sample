package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Audited
@Table(schema = "geral", name = "pessoa")
@Inheritance(strategy = InheritanceType.JOINED)
public class Pessoa implements Serializable{

	private static final long serialVersionUID = 506380966349968295L;

	@Id @Column(name="pessoa_id")
	@SequenceGenerator(sequenceName="geral.seq_pessoa", name="geral.seq_pessoa", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="geral.seq_pessoa")
	private Long id;

	private Boolean ativo = true;

	@OneToMany(mappedBy="pessoa",cascade=CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Endereco.class)
	@JsonManagedReference
	private List<Endereco> enderecos = new ArrayList<>();

	@OneToMany(mappedBy="pessoa",cascade=CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Telefone.class)
	@JsonManagedReference
	private List<Telefone> telefones = new ArrayList<>();

	@Transient
	private String displayDocumento;

	@Transient
	private String displayNome;

	@Transient
	private String displayEmail;

	@Transient
	private String displayCgf;

	/**
	 * Método {@link Transient}e que informa se o campo {@link #pessoa} é do tipo {@link PessoaFisica}.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 18/05/2015 15:41:06
	 *
	 * @return Retorna <code>true</code>, se a propriedade {@link Pessoa#getDisplayDocumento()} do
	 * campo {@link #pessoa} contém menos que 11 caracteres. Caso contrário, retorna <code>false</code>.
	 */
	@Transient
	public boolean isPessoaFisica(){
		// verifica se a propriedade displayDocumento tem mais que 11 caracteres
		if(this.getDisplayDocumento() != null && this.getDisplayDocumento().replaceAll("\\D", "").length() > 11){
			// caso tenha mais que 11 caracteres, é pessoa jurídica, e não pessoa física
			return false;
		}
		// caso seja menor que 11 caracteres, informa que é pessoa física
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Pessoa)) {
			return false;
		}
		return ((Pessoa)obj).getId().equals(getId());
	}

	public Pessoa() {
		super();
	}

	public Pessoa(Pessoa pessoa) {
		super();
		this.id = pessoa.id;
		this.ativo = pessoa.ativo;
		this.enderecos = pessoa.enderecos;
		this.telefones = pessoa.telefones;
		this.displayDocumento = pessoa.displayDocumento;
		this.displayNome = pessoa.displayNome;
		this.displayEmail = pessoa.displayEmail;
		this.displayCgf = pessoa.displayCgf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}

	// ============ METODOS DE SOBRECARGA ================ //

	public String getDisplayDocumento() {
		return displayDocumento;
	}

	public void setDisplayDocumento(String displayDocumento) {
		this.displayDocumento = displayDocumento;
	}

	public String getDisplayNome() {
		return displayNome;
	}

	public String getDisplayCutNome(int length) {
		String nome = getDisplayNome();
		if (nome != null && nome.length() > length) {
			return nome.substring(0, length-3)+"...";
		}
		return nome;
	}

	@Transient
	public String getPrimeiroNome() {
		if (getDisplayNome() != null) {
			return getDisplayNome().trim().split(" ")[0];
		}
		return null;
	}

	public void setDisplayNome(String displayNome) {
		this.displayNome = displayNome;
	}

	public String getDisplayEmail() {
		return displayEmail;
	}

	public void setDisplayEmail(String displayEmail) {
		this.displayEmail = displayEmail;
	}

	@Transient
	public Endereco getEnderecoPadrao(){

		if (enderecos == null || enderecos.isEmpty()){
			return null;
		}

		for(Endereco end: enderecos){
			if(end.getIsEnderecoPadrao() != null && end.getIsEnderecoPadrao()){
				return end;
			}
		}
		return !enderecos.isEmpty()?enderecos.get(0):null;
	}

	@Transient
	public String getEnderecoPadraoCompleto(){

		for(Endereco end: enderecos){
			if(end.getIsEnderecoPadrao()){
				return end.toString();
			}
		}
		return !enderecos.isEmpty()?enderecos.get(0).toString():"-";
	}

	@Transient
	public String getPrimeiroTelefone(){

		for(Telefone end: telefones){
			return end.getNumeroFormatado();
		}
		return "-";
	}

	@Transient
	public Telefone getTelefoneMovel(){

		for(Telefone tel: telefones){
			if (tel.getNumero() != null) {
				String numero = tel.getNumero().replaceAll("[^0-9]","");
				if (numero.length() == 8 || numero.length() == 9) {
					int first = Character.getNumericValue(numero.charAt(numero.length()-8));
					if (first >= 6 && first <= 9) {
						return tel;
					}
				}
			}
		}

		Contato contato = getContatoDefault();
		if (contato != null && contato.getTelefone() != null) {
			String numero = contato.getTelefone().replaceAll("[^0-9]","");
			if (numero.startsWith("0")) {
				numero = numero.substring(1);
			}

			if (numero.length() == 10 || numero.length() == 11) {
				int first = Character.getNumericValue(numero.charAt(numero.length()-8));
				if (first >= 6 && first <= 9) {

					Telefone t = new Telefone();
					t.setDdd(numero.substring(0, 2));
					t.setNumero(numero.substring(2));
					return t;
				}

			}
		}
		return null;
	}

	@Transient
	public String getEmailDefault() {
		return null;
	}

	@Transient
	public Contato getContatoDefault() {
		return null;
	}

	public String getDisplayCgf() {
		return displayCgf;
	}

	public void setDisplayCgf(String displayCgf) {
		this.displayCgf = displayCgf;
	}

}