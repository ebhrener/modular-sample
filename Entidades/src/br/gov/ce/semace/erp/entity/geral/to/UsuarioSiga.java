package br.gov.ce.semace.erp.entity.geral.to;

import java.io.Serializable;

public class UsuarioSiga implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -1557785777587595629L;

	private String usuarioID;
	private String areaID;
	private String login;
	private String nome;
	private String email;

	public String getUsuarioID() {
		return usuarioID;
	}

	public void setUsuarioID(String usuarioID) {
		this.usuarioID = usuarioID;
	}

	public String getAreaID() {
		return areaID;
	}

	public void setAreaID(String areaID) {
		this.areaID = areaID;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
