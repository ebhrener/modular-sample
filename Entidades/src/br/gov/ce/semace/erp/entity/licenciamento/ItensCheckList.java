package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.protocolo.TipoAnexo;
import br.gov.ce.semace.erp.utils.StringUtils;

@Entity
@Audited
@Table(name="itens_checklist", schema="atendimento")
public class ItensCheckList implements Serializable{

	private static final long serialVersionUID = -6353276257165306259L;

	@Id @SequenceGenerator(sequenceName="atendimento.seq_itens_checklist", name="SEQITENSCHECK", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQITENSCHECK")
	private Long id;

	@Column(length = 500)
	private String titulo;

	@Column(columnDefinition="text")
	private String descricao;

	private Boolean ativo = true;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy="itensCheckLists",cascade = CascadeType.ALL)
	private List<CheckList> checklist = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_anexo_id")
	private TipoAnexo tipoAnexo;

	@Column(columnDefinition = "BOOLEAN DEFAULT TRUE")
	private boolean obrigatorio = true;

	@Transient
	private String descricaoReduzida;

	@Transient
	private Atividade atividade;

//	@Transient
//	private List<ArquivoRequerimento> listDocumento = new ArrayList<ArquivoRequerimento>();

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public List<CheckList> getChecklist() {
		return checklist;
	}
	public void setChecklist(List<CheckList> checklist) {
		this.checklist = checklist;
	}
	public TipoAnexo getTipoAnexo() {
		return tipoAnexo;
	}
	public void setTipoAnexo(TipoAnexo tipoAnexo) {
		this.tipoAnexo = tipoAnexo;
	}
	public Atividade getAtividade() {
		return atividade;
	}
	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
	public boolean isShape() {
		return (this.tipoAnexo != null && this.tipoAnexo.getId() != null) ? this.tipoAnexo.isShape() : false;
	}
	public boolean isObrigatorio() {
		return obrigatorio;
	}
	public void setObrigatorio(boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}
	@Override
	public int hashCode() {
		if(getId() != null){
			return getId().hashCode();
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof ItensCheckList)) {
			return false;
		}
		if(((ItensCheckList)obj).getId() != null){
			return ((ItensCheckList)obj).getId().equals(getId());
		}

		if(((ItensCheckList)obj).getTitulo() != null && ((ItensCheckList)obj).getDescricao() != null ){
			return ((ItensCheckList)obj).getTitulo().equals(getTitulo()) && ((ItensCheckList)obj).getDescricao().equals(getDescricao());
		}

		return false;
	}


	public String getDescricaoReduzida() {
		return descricaoReduzida;
	}

	public void setDescricaoReduzida(String descricaoReduzida) {
		this.descricaoReduzida = descricaoReduzida;
	}

	public String getDisplayDescricao() {
		return getDisplayDescricao(140);
	}

	public String getDisplayDescricao(int length) {

		String title = StringUtils.isNotBlank(this.titulo) ? this.titulo.trim() : "";
		String desc = StringUtils.isNotBlank(this.descricao) ? this.descricao.trim() : "";

		String displayText = "";

		if (StringUtils.isNotBlank(title) && StringUtils.isNotBlank(desc)) {
			displayText = title + " - " + desc;
		} else if (StringUtils.isNotBlank(title)) {
			displayText = title;
		} else if (StringUtils.isNotBlank(desc)) {
			displayText = desc;
		}

		return displayText.length() > length ? displayText.substring(0, length).concat("...") : displayText;
	}

//	public List<ArquivoRequerimento> getListDoc() {
//		return listDoc;
//	}
//
//	public void setListDoc(List<ArquivoRequerimento> listDoc) {
//		this.listDoc = listDoc;
//	}

}
