package br.gov.ce.semace.erp.entity.atendimento.requerimento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.atendimento.to.EnvioArquivoOriginalShapeTO;
import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.licenciamento.CheckList;
import br.gov.ce.semace.erp.entity.licenciamento.ItensCheckList;
import br.gov.ce.semace.erp.utils.FileUtil;

/**
 * <h1>Entidade {@link RequerimentoAtividadeCheckListItemCheckList} do schema 'atendimento'</h1>
 * <br>
 * <b>{@link RequerimentoAtividadeCheckListItemCheckList} possui os seguintes atributos:</b>
 * <ul>
 *   <li>{@link RequerimentoAtividadeCheckListItemCheckList#listArquivoRequerimento}: Lista de {@link ArquivoRequerimento}s enviados pelo interessado.</li>
 *   <li>{@link RequerimentoAtividadeCheckListItemCheckList#validado}: Informa se o mesmo já foi validado pelo atendente.</li>
 *   <li>{@link RequerimentoAtividadeCheckListItemCheckList#requerimento}: Informa qual o {@link AgendamentoRequerimento} que o RACI está vinculado.</li>
 *   <li>{@link RequerimentoAtividadeCheckListItemCheckList#atividade}: Informa qual a {@link Atividade} que o RACI está vinculado.</li>
 *   <li>{@link RequerimentoAtividadeCheckListItemCheckList#checkList}: Informa qual o {@link CheckList} que o RACI está vinculado.</li>
 *   <li>{@link RequerimentoAtividadeCheckListItemCheckList#itemCheckList}: Informa qual o {@link ItensCheckList} que o RACI está vinculado.</li>
 * </ul>
 *
 * @author robson.carvalho [robson01jc@gmail.com]
 */
@Entity
@Audited
@Table(schema = "atendimento", name = "requerimento_atividade_checklist_itemchecklist")
public class RequerimentoAtividadeCheckListItemCheckList implements Serializable {

	private static final long serialVersionUID = -7006774000239437603L;

	@Transient
	private static final String EXTENSAO_FILE_SHAPE = ".shp";

	@Id
	@SequenceGenerator(sequenceName = "atendimento.seq_raci", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@OneToMany(mappedBy = "requerimentoAtividadeCheckListItemCheckList", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	private List<ArquivoRequerimento> listArquivoRequerimento = new ArrayList<>();

	private Boolean validado;

	@JoinColumn(name = "requerimento_id")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private AgendamentoRequerimento requerimento;

	@JoinColumn(name = "atividade_id")
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private Atividade atividade;

	@JoinColumn(name = "checklist_id")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private CheckList checkList;

	@JoinColumn(name = "itemchecklist_id")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private ItensCheckList itemCheckList;

	@Column(columnDefinition = "BOOLEAN DEFAULT FALSE", nullable = false)
	private boolean excluido = false;

	@Transient
	private Boolean validadoTransient;

	@Transient
	private Map<EnvioArquivoOriginalShapeTO, List<ArquivoRequerimentoShape>> mapEnvioShapesOriginais = new TreeMap<>();

	@Transient
	private Map<String, List<ArquivoRequerimentoShape>> mapShapesArea = new TreeMap<>();

	@Transient
	private Map<String, List<ArquivoRequerimentoShape>> mapShapesEquipamento = new TreeMap<>();

	public RequerimentoAtividadeCheckListItemCheckList() {
		super();
	}

	public RequerimentoAtividadeCheckListItemCheckList(AgendamentoRequerimento requerimento, Atividade atividade, CheckList checkList, ItensCheckList itemCheckList) {
		super();
		this.requerimento = requerimento;
		this.atividade = atividade;
		this.checkList = checkList;
		this.itemCheckList = itemCheckList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ArquivoRequerimento> getListArquivoRequerimento() {
		return listArquivoRequerimento;
	}

	public void setListArquivoRequerimento(List<ArquivoRequerimento> listArquivoRequerimento) {
		this.listArquivoRequerimento = listArquivoRequerimento;
	}

	public Boolean getValidado() {
		return validado;
	}

	public void setValidado(Boolean validado) {
		this.validado = validado;
	}

	public AgendamentoRequerimento getRequerimento() {
		return requerimento;
	}

	public void setRequerimento(AgendamentoRequerimento requerimento) {
		this.requerimento = requerimento;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public CheckList getCheckList() {
		return checkList;
	}

	public void setCheckList(CheckList checkList) {
		this.checkList = checkList;
	}

	public ItensCheckList getItemCheckList() {
		return itemCheckList;
	}

	public void setItemCheckList(ItensCheckList itemCheckList) {
		this.itemCheckList = itemCheckList;
	}

	public boolean isExcluido() {
		return excluido;
	}

	public void setExcluido(boolean excluido) {
		this.excluido = excluido;
	}

	public Boolean getValidadoTransient() {
		return validadoTransient;
	}

	public void setValidadoTransient(Boolean validadoTransient) {
		this.validadoTransient = validadoTransient;
	}

	public Map<EnvioArquivoOriginalShapeTO, List<ArquivoRequerimentoShape>> getMapEnvioShapesOriginais() {
		return mapEnvioShapesOriginais;
	}

	public void setMapEnvioShapesOriginais(Map<EnvioArquivoOriginalShapeTO, List<ArquivoRequerimentoShape>> mapEnvioShapesOriginais) {
		this.mapEnvioShapesOriginais = mapEnvioShapesOriginais;
	}

	public Map<String, List<ArquivoRequerimentoShape>> getMapShapesArea() {
		return mapShapesArea;
	}

	public void setMapShapesArea(Map<String, List<ArquivoRequerimentoShape>> mapShapesArea) {
		this.mapShapesArea = mapShapesArea;
	}

	public Map<String, List<ArquivoRequerimentoShape>> getMapShapesEquipamento() {
		return mapShapesEquipamento;
	}

	public void setMapShapesEquipamento(Map<String, List<ArquivoRequerimentoShape>> mapShapesEquipamento) {
		this.mapShapesEquipamento = mapShapesEquipamento;
	}

	public boolean isPossuiShapeArea() {
		return !mapShapesArea.isEmpty();
	}

	public void mountScreenShapeInteressado() {

		mapEnvioShapesOriginais = new TreeMap<>();

		for (ArquivoRequerimento arquivo : listArquivoRequerimento) {

			if(!(arquivo instanceof ArquivoRequerimentoShape)) {
				continue;
			}

			ArquivoRequerimentoShape arquivoShape = (ArquivoRequerimentoShape) arquivo;
			if(arquivoShape.isOriginal()) {
				prepareDisplayArquivosOriginais(arquivoShape);
			}
		}
	}

	public void mountScreenShape() {

		mapShapesArea = new TreeMap<>();
		mapShapesEquipamento = new TreeMap<>();
		mapEnvioShapesOriginais = new TreeMap<>();

		for (ArquivoRequerimento arquivo : listArquivoRequerimento) {

			if(!(arquivo instanceof ArquivoRequerimentoShape)) {
				continue;
			}

			ArquivoRequerimentoShape arquivoShape = (ArquivoRequerimentoShape) arquivo;
			if(arquivoShape.isExcluido()) {
				continue;
			}

			if(arquivoShape.isOriginal()) {
				prepareDisplayArquivosOriginais(arquivoShape);
			} else {
				prepareDisplayArquivosShape(arquivoShape);
			}
		}
	}

	private void prepareDisplayArquivosShape(ArquivoRequerimentoShape arquivoShape) {
		String key = FileUtil.getNameWithoutExtension(arquivoShape.getNome()) + EXTENSAO_FILE_SHAPE;
		if (arquivoShape.isArea()) {
			if (mapShapesArea.containsKey(key)) {
				mapShapesArea.get(key).add(arquivoShape);
			} else {
				mapShapesArea.put(key, new ArrayList<>(Arrays.asList(arquivoShape)));
			}
		} else if (arquivoShape.isEquipamento()) {
			if (mapShapesEquipamento.containsKey(key)) {
				mapShapesEquipamento.get(key).add(arquivoShape);
			} else {
				mapShapesEquipamento.put(key, new ArrayList<>(Arrays.asList(arquivoShape)));
			}
		}
	}

	private void prepareDisplayArquivosOriginais(ArquivoRequerimentoShape arquivoShape) {
		String name = FileUtil.getNameWithoutExtension(arquivoShape.getNome());

		EnvioArquivoOriginalShapeTO envio = new EnvioArquivoOriginalShapeTO(name, arquivoShape.getDataCadastro(), arquivoShape.getValidado(), arquivoShape.isExcluido(), this);

		if (mapEnvioShapesOriginais.containsKey(envio)) {
			mapEnvioShapesOriginais.get(envio).add(arquivoShape);
		} else {
			mapEnvioShapesOriginais.put(new EnvioArquivoOriginalShapeTO(name, arquivoShape.getDataCadastro(), arquivoShape.getValidado(), arquivoShape.isExcluido(), this), new ArrayList<>(Arrays.asList(arquivoShape)));
		}
	}

	public String getDisplayValidado() {
		if(validado == null) {
			return "Aguardando validação";
		}
		return validado ? "Validado" : "Inválido";
	}

	public boolean isPossuiArquivosAtivos() {
		for (ArquivoRequerimento arquivo : listArquivoRequerimento) {
			if (!arquivo.isExcluido()) {
				return true;
			}
		}
		return false;
	}

	public int getCountArquivosAtivos() {
		int count = 0;
		for (ArquivoRequerimento arquivo : listArquivoRequerimento) {
			if (!arquivo.isExcluido()) {
				count++;
			}
		}
		return count;
	}

	public boolean isPossuiArquivosShapeAtivos() {
		for (Map.Entry<EnvioArquivoOriginalShapeTO, List<ArquivoRequerimentoShape>> entry : mapEnvioShapesOriginais.entrySet()) {
			if (!entry.getKey().isExcluido()) {
				return true;
			}
		}
		return false;
	}

	public int getCountArquivosShapeAtivos() {
		int count = 0;
		for (Map.Entry<EnvioArquivoOriginalShapeTO, List<ArquivoRequerimentoShape>> entry : mapEnvioShapesOriginais.entrySet()) {
			if (!entry.getKey().isExcluido()) {
				count++;
			}
		}
		return count;
	}

	public boolean isPossuiArquivosAtivosOrMigrados() {
		for (ArquivoRequerimento arquivo : listArquivoRequerimento) {
			if (!arquivo.isExcluido() || arquivo.getGedId() != null || arquivo.getArquivoTemporario().getStatus().isSincronizado()) {
				return true;
			}
		}
		return false;
	}

	public boolean isPossuiNovosArquivos() {
		for (ArquivoRequerimento arquivo : listArquivoRequerimento) {
			if (!arquivo.isExcluido() && arquivo.getValidado() == null) {
				return true;
			}
		}
		return false;
	}
}
