package br.gov.ce.semace.erp.entity.laboratorio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="app_alerts", schema="laboratorio")
public class AppAlerts {

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_app_alerts", name="laboratorio.seq_app_alerts", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="laboratorio.seq_app_alerts")
	private Long id;

	private Boolean ativo;

	private String titulo;

	private String texto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

}
