package br.gov.ce.semace.erp.entity.geral.entity.cep;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Audited
@Table(name = "estados", schema="cep")
public class UF implements Serializable{

	private static final long serialVersionUID = -2552219816888136458L;

	@Id @Column(name="cd_uf")
	@SequenceGenerator(sequenceName = "cep.seq_uf", name = "cep.seq_uf", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cep.seq_uf")
	private Integer id;

	@Column(name ="ds_uf_sigla")
	private String sigla;

	@Column(name ="ds_uf_nome")
	private String descricao;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UF other = (UF) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}


	@OneToMany(mappedBy="uf", fetch=FetchType.LAZY)
	@JsonBackReference
	private Set<Cidade> cidades = new HashSet<>();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="pais_id")
	private Pais pais = new Pais();

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Set<Cidade> getCidades() {
		return cidades;
	}
	public void setCidades(Set<Cidade> cidades) {
		this.cidades = cidades;
	}
	public Pais getPais() {
		return pais;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
}
