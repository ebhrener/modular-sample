package br.gov.ce.semace.erp.entity.licenciamento;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.DocumentoQuestionario;
import br.gov.ce.semace.erp.entity.geral.Endereco;
import br.gov.ce.semace.erp.entity.geral.Telefone;
import br.gov.ce.semace.erp.enuns.SituacaoRama;
import br.gov.ce.semace.erp.utils.StringUtils;

/**
 * @author antoniomenezes
 */
@Entity
@Audited
@Table(name = "rama", schema = "licenciamento")
public class Rama extends DocumentoQuestionario {

	private static final long serialVersionUID = 2096736776139623873L;

	@Column(name = "responsavel",columnDefinition="text")
	private String responsavel;

	@Column(name="email")
	private String email;

	@Column(name="cargo_responsavel")
	private String cargoResponsavel;

	@Column(name="situacao_rama")
	private SituacaoRama situacao;

	@Column(name = "data_emissao_licenca")
	private Date dataEmissao = new Date();

	@Column(name = "data_validade_licenca")
	private Date dataValidadeLicenca = new Date();

	@Column(name = "data_inicio_abrangencia")
	private Date dataInicioAbrangencia = new Date();

	@Column(name = "data_fim_abrangencia")
	private Date dataFimAbrangencia = new Date();

	@Column(name = "data_inicio_preenchimento")
	private Date dataInicioPreenchimento = new Date();

	@Column(name = "data_fim_preenchimento")
	private Date dataFimPreenchimento = new Date();

	@Column(name = "data_protocolado_presencial")
	private Date dataProtocoladoPresencial = new Date();

	@Column(name = "data_entrega_rama_manual")
	private Date dataEntregaRamaManual;

	@Column(name = "data_protocolo_online")
	private Date dataProtocoloOnline;

	@Column(name = "data_dispensa")
	private Date dataDispensa;

	@Column(name = "spu_licenca")
	private String spuLicenca;

	@Column(name = "spu_rama")
	private String spuRama;

	@Column(name="tipo_processo")
	private String tipoProcesso;

	@Column(name="houve_paralizacao")
	private Boolean houveParalizacao;

	@Column(name="cpf_responsavel")
	private String cpfResponsavel;

	//Irá existir relacionamento até o momento em que o usuario
	// finalizar.Depois os valores serão salvos em string valorEndereco e valorTelefone
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "telefone_id")
	private Telefone telefone;

	@Column(name="despachado_retificacao")
	private Boolean despachadoRetificacao;

	//atributos adicionados para quando o usuario finalizar o cadastro
	//os valores serem salvos na tabela, e assim os dados do rama sejam
	// imutáveis.
	@Column(name = "valor_endereco_interessado",columnDefinition="text")
	private String valorEnderecoInteressado;

	@Column(name = "valor_cep_interessado")
	private String valorCepInteressado;

	@Column(name = "valor_telefone_interessado")
	private String valorTelefoneInteressado;

	@Column(name = "valor_ddd_interessado")
	private String valorDDDInteressado;

	@Column(name = "valor_documento_interessado")
	private String valorDocumentoInteressado;

	@Column(name = "valor_nome_interessado",columnDefinition="text")
	private String valorNomeInteressado;

	@Column(name = "valor_descricao_empreendimento",columnDefinition="text")
	private String valorDescricaoEmpreendimento;

	@Column(name = "valor_endereco_empreendimento",columnDefinition="text")
	private String valorEnderecoEmpreendimento;

	@Column(name = "valor_atividade_empreendimento",columnDefinition="text")
	private String valorAtividadeEmpreendimento;

	@Column(name = "houve_atividade_periodo")
	private Boolean houveAtividadeNessePeriodo = true;

	@Column(name = "justificativa_nao_operacao",columnDefinition="text")
	private String justificativaNaoOperacao;

	//-------------------------------------------------------------------

	@Column(name="justificativa",columnDefinition="text")
	private String justificativaDispensa;

	@Column(name = "quantidade_tempo_paralisado")
	private Integer quantidadeTempoParalisado;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "rama")
	private List<ArquivoRama> listArquivos;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "rama")
	private List<AnaliseTecnicoRama> listAnalise;

	@Column(name="rama_virtual")
	private Boolean ramaVirtual;

	@Column(name="isento_pagamento")
	private boolean isentoPagamento;

	@Transient
	private List<ArquivoRama> listDeclaracoes = new ArrayList<>();

	@Transient
	private boolean periodoPreenchimento = true;

	@Transient
	private String corStatus;

	@Transient
	private String documentoCliente;

	@Transient
	private String nomeCliente;

	@Transient
	private String periodicidade;

	@Transient
	private boolean selecionado;

	@Transient
	private Long idTecnico;

	@Transient
	private Date dataVencimentoDaeTemporaria;

	@Transient
	private Date dataMinima;

	@Transient
	private Date dataMaxima;

	@Transient
	private String numeroDocumento;

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Date getDataInicioAbrangencia() {
		return dataInicioAbrangencia;
	}

	public void setDataInicioAbrangencia(Date dataInicioAbrangencia) {
		this.dataInicioAbrangencia = dataInicioAbrangencia;
	}

	public Date getDataFimAbrangencia() {
		return dataFimAbrangencia;
	}

	public void setDataFimAbrangencia(Date dataFimAbrangencia) {
		this.dataFimAbrangencia = dataFimAbrangencia;
	}

	public Date getDataInicioPreenchimento() {
		return dataInicioPreenchimento;
	}

	public void setDataInicioPreenchimento(Date dataInicioPreenchimento) {
		this.dataInicioPreenchimento = dataInicioPreenchimento;
	}

	public Date getDataFimPreenchimento() {
		return dataFimPreenchimento;
	}

	public void setDataFimPreenchimento(Date dataFimPreenchimento) {
		this.dataFimPreenchimento = dataFimPreenchimento;
	}

	public String getSpuRama() {
		return spuRama;
	}

	public void setSpuRama(String spuRama) {
		this.spuRama = spuRama;
	}

	public String getSpuLicenca() {
		return spuLicenca;
	}

	public void setSpuLicenca(String spuLicenca) {
		this.spuLicenca = spuLicenca;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public SituacaoRama getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoRama situacao) {
		this.situacao = situacao;
	}

	public boolean isPeriodoPreenchimento() {
		return periodoPreenchimento;
	}

	public void setPeriodoPreenchimento(boolean periodoPreenchimento) {
		this.periodoPreenchimento = periodoPreenchimento;
	}

	public String getJustificativaDispensa() {
		return justificativaDispensa;
	}

	public void setJustificativaDispensa(String justificativaDispensa) {
		this.justificativaDispensa = justificativaDispensa;
	}

	public List<ArquivoRama> getListArquivos() {
		return listArquivos;
	}

	public void setListArquivos(List<ArquivoRama> listArquivos) {
		this.listArquivos = listArquivos;
	}

	public String getCorStatus() {
		return corStatus;
	}

	public void setCorStatus(String corStatus) {
		this.corStatus = corStatus;
	}

	public String getValorDocumentoInteressado() {
		return valorDocumentoInteressado;
	}

	public void setValorDocumentoInteressado(String valorDocumentoInteressado) {
		this.valorDocumentoInteressado = valorDocumentoInteressado;
	}

	public String getValorDescricaoEmpreendimento() {
		return valorDescricaoEmpreendimento;
	}

	public void setValorDescricaoEmpreendimento(String valorDescricaoEmpreendimento) {
		this.valorDescricaoEmpreendimento = valorDescricaoEmpreendimento;
	}

	public String getValorEnderecoEmpreendimento() {
		return valorEnderecoEmpreendimento;
	}

	public void setValorEnderecoEmpreendimento(String valorEnderecoEmpreendimento) {
		this.valorEnderecoEmpreendimento = valorEnderecoEmpreendimento;
	}

	public String getValorAtividadeEmpreendimento() {
		return valorAtividadeEmpreendimento;
	}

	public void setValorAtividadeEmpreendimento(String valorAtividadeEmpreendimento) {
		this.valorAtividadeEmpreendimento = valorAtividadeEmpreendimento;
	}

	public String getValorEnderecoInteressado() {
		return valorEnderecoInteressado;
	}

	public void setValorEnderecoInteressado(String valorEnderecoInteressado) {
		this.valorEnderecoInteressado = valorEnderecoInteressado;
	}

	public String getValorCepInteressado() {
		return valorCepInteressado;
	}

	public void setValorCepInteressado(String valorCepInteressado) {
		this.valorCepInteressado = valorCepInteressado;
	}

	public String getValorTelefoneInteressado() {
		return valorTelefoneInteressado;
	}

	public void setValorTelefoneInteressado(String valorTelefoneInteressado) {
		this.valorTelefoneInteressado = valorTelefoneInteressado;
	}

	public String getValorDDDInteressado() {
		return valorDDDInteressado;
	}

	public void setValorDDDInteressado(String valorDDDInteressado) {
		this.valorDDDInteressado = valorDDDInteressado;
	}

	public Boolean getHouveAtividadeNessePeriodo() {
		return houveAtividadeNessePeriodo;
	}

	public void setHouveAtividadeNessePeriodo(Boolean houveAtividadeNessePeriodo) {
		this.houveAtividadeNessePeriodo = houveAtividadeNessePeriodo;
	}

	public String getJustificativaNaoOperacao() {
		return justificativaNaoOperacao;
	}

	public void setJustificativaNaoOperacao(String justificativaNaoOperacao) {
		this.justificativaNaoOperacao = justificativaNaoOperacao;
	}

	public String getValorNomeInteressado() {
		return valorNomeInteressado;
	}

	public void setValorNomeInteressado(String valorNomeInteressado) {
		this.valorNomeInteressado = valorNomeInteressado;
	}

	public String getDocumentoCliente() {
		return documentoCliente;
	}

	public void setDocumentoCliente(String documentoCliente) {
		this.documentoCliente = documentoCliente;
	}

	public Date getDataEntregaRamaManual() {
		return dataEntregaRamaManual;
	}

	public void setDataEntregaRamaManual(Date dataEntregaRamaManual) {
		this.dataEntregaRamaManual = dataEntregaRamaManual;
	}

	public Date getDataProtocoloOnline() {
		return dataProtocoloOnline;
	}

	public void setDataProtocoloOnline(Date dataProtocoloOnline) {
		this.dataProtocoloOnline = dataProtocoloOnline;
	}

	@Transient
	public String getDocumentoClienteFormatado(){
		if(documentoCliente != null && !documentoCliente.trim().equals("")){
			if(documentoCliente.length() == 11){
				return StringUtils.getCPFFormatado(documentoCliente);
			}
			return StringUtils.getCNPJFormatado(documentoCliente);
		}
		return null;
	}

	public String getPeriodicidade() {
		return periodicidade;
	}

	public void setPeriodicidade(String periodicidade) {
		this.periodicidade = periodicidade;
	}

	public Integer getQuantidadeTempoParalisado() {
		return quantidadeTempoParalisado;
	}

	public void setQuantidadeTempoParalisado(Integer quantidadeTempoParalisado) {
		this.quantidadeTempoParalisado = quantidadeTempoParalisado;
	}

	public String getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(String tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public Boolean getHouveParalizacao() {
		return houveParalizacao;
	}

	public void setHouveParalizacao(Boolean houveParalizacao) {
		this.houveParalizacao = houveParalizacao;
	}

	public String getCpfResponsavel() {
		return cpfResponsavel;
	}

	public void setCpfResponsavel(String cpfResponsavel) {
		this.cpfResponsavel = cpfResponsavel;
	}

	public String getCargoResponsavel() {
		return cargoResponsavel;
	}

	public void setCargoResponsavel(String cargoResponsavel) {
		this.cargoResponsavel = cargoResponsavel;
	}

	public List<ArquivoRama> getListDeclaracoes() {
		return listDeclaracoes;
	}

	public void setListDeclaracoes(List<ArquivoRama> listDeclaracoes) {
		this.listDeclaracoes = listDeclaracoes;
	}

	public Date getDataDispensa() {
		return dataDispensa;
	}

	public void setDataDispensa(Date dataDispensa) {
		this.dataDispensa = dataDispensa;
	}

	public Date getDataProtocoladoPresencial() {
		return dataProtocoladoPresencial;
	}

	public void setDataProtocoladoPresencial(Date dataProtocoladoPresencial) {
		this.dataProtocoladoPresencial = dataProtocoladoPresencial;
	}

	public Date getDataValidadeLicenca() {
		return dataValidadeLicenca;
	}

	public void setDataValidadeLicenca(Date dataValidadeLicenca) {
		this.dataValidadeLicenca = dataValidadeLicenca;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	public List<AnaliseTecnicoRama> getListAnalise() {
		return listAnalise;
	}

	public void setListAnalise(List<AnaliseTecnicoRama> listAnalise) {
		this.listAnalise = listAnalise;
	}

	public Long getIdTecnico() {
		return idTecnico;
	}

	public void setIdTecnico(Long idTecnico) {
		this.idTecnico = idTecnico;
	}

	public Boolean getRamaVirtual() {
		return(this.ramaVirtual != null && this.ramaVirtual);
	}

	public void setRamaVirtual(Boolean ramaVirtual) {
		this.ramaVirtual = ramaVirtual;
	}

	public Boolean getDespachadoRetificacao() {
		return despachadoRetificacao;
	}

	public void setDespachadoRetificacao(Boolean despachadoRetificacao) {
		this.despachadoRetificacao = despachadoRetificacao;
	}

	public Date getDataVencimentoDaeTemporaria() {
		return dataVencimentoDaeTemporaria;
	}

	public void setDataVencimentoDaeTemporaria(Date dataVencimentoDaeTemporaria) {
		this.dataVencimentoDaeTemporaria = dataVencimentoDaeTemporaria;
	}

	public boolean isIsentoPagamento() {
		return isentoPagamento;
	}

	public void setIsentoPagamento(boolean isentoPagamento) {
		this.isentoPagamento = isentoPagamento;
	}

	public Date getDataMinima() {
		return dataMinima;
	}

	public void setDataMinima(Date dataMinima) {
		this.dataMinima = dataMinima;
	}

	public Date getDataMaxima() {
		return dataMaxima;
	}

	public void setDataMaxima(Date dataMaxima) {
		this.dataMaxima = dataMaxima;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	@Transient
	public boolean isDesabilitaOpcao(){
		return SituacaoRama.listSituacaoDesabilitaOpcao().contains(this.situacao);
	}

	@Transient
	public boolean isPodeResponder(){
		return SituacaoRama.listSituacaoHabilitaResponder().contains(this.situacao);
	}

	@Transient
	public boolean isPodeResponderQuestionario(){
		return SituacaoRama.listSituacaoHabilitaResponder().contains(this.situacao);
	}

	@Transient
	public boolean isPodeFazerAgendamento(){
		return SituacaoRama.listSituacaoHabilitaCriacaoAgendamento().contains(this.situacao);
	}


	@Transient
	public boolean isTipoProcessoLicencaLI(){
		return (this.tipoProcesso != null && (this.tipoProcesso.equals("0002") || this.tipoProcesso.equals("0111") || this.tipoProcesso.equals("0149")));
	}

	@Transient
	public boolean isTipoProcessoLicencaLO(){
		return (this.tipoProcesso != null && (this.tipoProcesso.equals("0003") || this.tipoProcesso.equals("0150") || this.tipoProcesso.equals("0112")));
	}

	@Transient
	public boolean isTipoProcessoLicencaLIO(){
		return (this.tipoProcesso != null && (this.tipoProcesso.equals("0172") || this.tipoProcesso.equals("0089") || this.tipoProcesso.equals("0148")));
	}

	@Transient
	public boolean isTipoProcessoLicencaLIAM(){
		return (this.tipoProcesso != null && (this.tipoProcesso.equals("0182")));
	}
}
