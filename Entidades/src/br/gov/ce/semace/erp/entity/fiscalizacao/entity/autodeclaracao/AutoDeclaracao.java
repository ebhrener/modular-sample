package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio.AutoDeclaracaoEstadoCivil;
import br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio.AutoDeclaracaoRegimeCasamento;

/**
 * @author marcus
 * 
 */
@Entity
@Table(schema = "servicos", name = "auto_declaracao")
public class AutoDeclaracao {

	@Id
	@SequenceGenerator(sequenceName = "servicos.seq_auto_declaracao", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;
	private String nome;
	private String sexo;
	private String cpf;
	private String nomeMae;
	private String apelido;
	
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;
	private String rg;
	private String ufOrgaoEmissor;
	private String municipioNascimento;
	
	private String nomeConjuge;
	private String sexoConjuge;
	private String cpfConjuge;
	private String nomeMaeConjuge;
	private String apelidoConjuge;
	
	@Temporal(TemporalType.DATE)
	private Date dataNascimentoConjuge;
	private String rgConjuge;
	private String ufOrgaoEmissorConjuge;
	private String municipioNascimentoConjuge;
	private String estadoCivil;
	private String regimeCasamento;
	private String endereco;
	private String numero;
	private String bairro;
	private String cep;
	private String uf;
	private String municipio;
	private String distrito;
	private String localidade;
	private String denominacaoImovel;
	private String enderecoImovel;
	private String numeroImovel;
	private String bairroImovel;
	private String cepImovel;
	private String ufImovel;
	private String municipioImovel;
	private String distritoImovel;
	private String localidadeImovel;
	private Double areaEstabelecimento;
	private Boolean isProprietario;
	private String documentoIdentificacaoProprietario;
	private String nomeProprietario;
	
	private Boolean inAreaIndigena;
	private Boolean inAreaQuilombola;
	private Boolean inAreaHistorica;
	private Boolean inAreaConservacaoIntegral;
	private Boolean inAreaConservacaoSustentavel;
	private Boolean inAreaProtecaoMananciais;
	private Boolean inAreaManguezais;
	private Boolean inAreaBanhados;
	private Boolean inAreaGrutas;
	private Boolean inAreaRural;
	private Boolean inAreaUrbana;
	private Boolean inAreaIndustrial;
	private Boolean inAreaReservaLegalAverbada;
	private Boolean autoInfracaoUltimosCincoAnos;
	private Boolean usaResiduos;
	private Boolean usaMadeira;
	private Boolean usaLenha;
	private Boolean fazReusoAgua;
	private Boolean reusoMaterialDescartado;
	private Boolean trocouInsumo;
	private Boolean energiaRenovavel;
	private Boolean terraDesmatamento;
	private Integer status;

	
	@Column(columnDefinition = "text")
	private String xmlEnviado;
	private String numeroMatriculaUsuario;
	private String codigoConvenio;
	@Temporal(TemporalType.DATE)
	private Date dataCadastro;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "autoDeclaracao")
	private List<AtividadePrincipal> listaAtividadePrincipal;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "autoDeclaracao")
	private List<OrganizacaoSocial> listaOganizacaoSocial;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "autoDeclaracao")
	private List<PosseTerra> listaPosseTerra;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "autoDeclaracao")
	private List<DestinacaoDeclaracao> listaDestinacaoDeclaracao;
	
	private String categoria;
	private String produto;
	private Double areaExplorada;
	private String unidadeMedida;
	private String numeroAutoDeclaracao;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNomeMae() {
		return nomeMae;
	}
	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}
	public String getApelido() {
		return apelido;
	}
	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getUfOrgaoEmissor() {
		return ufOrgaoEmissor;
	}
	public void setUfOrgaoEmissor(String ufOrgaoEmissor) {
		this.ufOrgaoEmissor = ufOrgaoEmissor;
	}
	public String getMunicipioNascimento() {
		return municipioNascimento;
	}
	public void setMunicipioNascimento(String municipioNascimento) {
		this.municipioNascimento = municipioNascimento;
	}
	public String getNomeConjuge() {
		return nomeConjuge;
	}
	public void setNomeConjuge(String nomeConjuge) {
		this.nomeConjuge = nomeConjuge;
	}
	public String getSexoConjuge() {
		return sexoConjuge;
	}
	public void setSexoConjuge(String sexoConjuge) {
		this.sexoConjuge = sexoConjuge;
	}
	public String getCpfConjuge() {
		return cpfConjuge;
	}
	public void setCpfConjuge(String cpfConjuge) {
		this.cpfConjuge = cpfConjuge;
	}
	public String getNomeMaeConjuge() {
		return nomeMaeConjuge;
	}
	public void setNomeMaeConjuge(String nomeMaeConjuge) {
		this.nomeMaeConjuge = nomeMaeConjuge;
	}
	public String getApelidoConjuge() {
		return apelidoConjuge;
	}
	public void setApelidoConjuge(String apelidoConjuge) {
		this.apelidoConjuge = apelidoConjuge;
	}
	public Date getDataNascimentoConjuge() {
		return dataNascimentoConjuge;
	}
	public void setDataNascimentoConjuge(Date dataNascimentoConjuge) {
		this.dataNascimentoConjuge = dataNascimentoConjuge;
	}
	public String getRgConjuge() {
		return rgConjuge;
	}
	public void setRgConjuge(String rgConjuge) {
		this.rgConjuge = rgConjuge;
	}
	public String getUfOrgaoEmissorConjuge() {
		return ufOrgaoEmissorConjuge;
	}
	public void setUfOrgaoEmissorConjuge(String ufOrgaoEmissorConjuge) {
		this.ufOrgaoEmissorConjuge = ufOrgaoEmissorConjuge;
	}
	public String getMunicipioNascimentoConjuge() {
		return municipioNascimentoConjuge;
	}
	public void setMunicipioNascimentoConjuge(String municipioNascimentoConjuge) {
		this.municipioNascimentoConjuge = municipioNascimentoConjuge;
	}
	public String getEstadoCivil() {
		if(estadoCivil != null)
			estadoCivil = AutoDeclaracaoEstadoCivil.showDescricaoByCodigo(new Integer(estadoCivil));
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public String getRegimeCasamento() {
		if(regimeCasamento != null)
			regimeCasamento = AutoDeclaracaoRegimeCasamento.showDescricaoByCodigo(new Integer(regimeCasamento));
		return regimeCasamento;
	}
	public void setRegimeCasamento(String regimeCasamento) {
		this.regimeCasamento = regimeCasamento;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getLocalidade() {
		return localidade;
	}
	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}
	public String getDenominacaoImovel() {
		return denominacaoImovel;
	}
	public void setDenominacaoImovel(String denominacaoImovel) {
		this.denominacaoImovel = denominacaoImovel;
	}
	public String getEnderecoImovel() {
		return enderecoImovel;
	}
	public void setEnderecoImovel(String enderecoImovel) {
		this.enderecoImovel = enderecoImovel;
	}
	public String getBairroImovel() {
		return bairroImovel;
	}
	public void setBairroImovel(String bairroImovel) {
		this.bairroImovel = bairroImovel;
	}
	public String getCepImovel() {
		return cepImovel;
	}
	public void setCepImovel(String cepImovel) {
		this.cepImovel = cepImovel;
	}
	public String getUfImovel() {
		return ufImovel;
	}
	public void setUfImovel(String ufImovel) {
		this.ufImovel = ufImovel;
	}
	public String getMunicipioImovel() {
		return municipioImovel;
	}
	public void setMunicipioImovel(String municipioImovel) {
		this.municipioImovel = municipioImovel;
	}
	public String getDistritoImovel() {
		return distritoImovel;
	}
	public void setDistritoImovel(String distritoImovel) {
		this.distritoImovel = distritoImovel;
	}
	public String getLocalidadeImovel() {
		return localidadeImovel;
	}
	public void setLocalidadeImovel(String localidadeImovel) {
		this.localidadeImovel = localidadeImovel;
	}
	public Double getAreaEstabelecimento() {
		return areaEstabelecimento;
	}
	public void setAreaEstabelecimento(Double areaEstabelecimento) {
		this.areaEstabelecimento = areaEstabelecimento;
	}
	public Boolean getIsProprietario() {
		return isProprietario;
	}
	public void setIsProprietario(Boolean isProprietario) {
		this.isProprietario = isProprietario;
	}
	public String getDocumentoIdentificacaoProprietario() {
		return documentoIdentificacaoProprietario;
	}
	public void setDocumentoIdentificacaoProprietario(
			String documentoIdentificacaoProprietario) {
		this.documentoIdentificacaoProprietario = documentoIdentificacaoProprietario;
	}
	public String getNomeProprietario() {
		return nomeProprietario;
	}
	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}
	public Boolean getInAreaIndigena() {
		return inAreaIndigena;
	}
	public void setInAreaIndigena(Boolean inAreaIndigena) {
		this.inAreaIndigena = inAreaIndigena;
	}
	public Boolean getInAreaQuilombola() {
		return inAreaQuilombola;
	}
	public void setInAreaQuilombola(Boolean inAreaQuilombola) {
		this.inAreaQuilombola = inAreaQuilombola;
	}
	public Boolean getInAreaHistorica() {
		return inAreaHistorica;
	}
	public void setInAreaHistorica(Boolean inAreaHistorica) {
		this.inAreaHistorica = inAreaHistorica;
	}
	public Boolean getInAreaConservacaoIntegral() {
		return inAreaConservacaoIntegral;
	}
	public void setInAreaConservacaoIntegral(Boolean inAreaConservacaoIntegral) {
		this.inAreaConservacaoIntegral = inAreaConservacaoIntegral;
	}
	public Boolean getInAreaConservacaoSustentavel() {
		return inAreaConservacaoSustentavel;
	}
	public void setInAreaConservacaoSustentavel(Boolean inAreaConservacaoSustentavel) {
		this.inAreaConservacaoSustentavel = inAreaConservacaoSustentavel;
	}
	public Boolean getInAreaProtecaoMananciais() {
		return inAreaProtecaoMananciais;
	}
	public void setInAreaProtecaoMananciais(Boolean inAreaProtecaoMananciais) {
		this.inAreaProtecaoMananciais = inAreaProtecaoMananciais;
	}
	public Boolean getInAreaManguezais() {
		return inAreaManguezais;
	}
	public void setInAreaManguezais(Boolean inAreaManguezais) {
		this.inAreaManguezais = inAreaManguezais;
	}
	public Boolean getInAreaBanhados() {
		return inAreaBanhados;
	}
	public void setInAreaBanhados(Boolean inAreaBanhados) {
		this.inAreaBanhados = inAreaBanhados;
	}
	public Boolean getInAreaGrutas() {
		return inAreaGrutas;
	}
	public void setInAreaGrutas(Boolean inAreaGrutas) {
		this.inAreaGrutas = inAreaGrutas;
	}
	public Boolean getInAreaRural() {
		return inAreaRural;
	}
	public void setInAreaRural(Boolean inAreaRural) {
		this.inAreaRural = inAreaRural;
	}
	public Boolean getInAreaUrbana() {
		return inAreaUrbana;
	}
	public void setInAreaUrbana(Boolean inAreaUrbana) {
		this.inAreaUrbana = inAreaUrbana;
	}
	public Boolean getInAreaIndustrial() {
		return inAreaIndustrial;
	}
	public void setInAreaIndustrial(Boolean inAreaIndustrial) {
		this.inAreaIndustrial = inAreaIndustrial;
	}
	public Boolean getInAreaReservaLegalAverbada() {
		return inAreaReservaLegalAverbada;
	}
	public void setInAreaReservaLegalAverbada(Boolean inAreaReservaLegalAverbada) {
		this.inAreaReservaLegalAverbada = inAreaReservaLegalAverbada;
	}
	public Boolean getAutoInfracaoUltimosCincoAnos() {
		return autoInfracaoUltimosCincoAnos;
	}
	public void setAutoInfracaoUltimosCincoAnos(Boolean autoInfracaoUltimosCincoAnos) {
		this.autoInfracaoUltimosCincoAnos = autoInfracaoUltimosCincoAnos;
	}
	public Boolean getUsaResiduos() {
		return usaResiduos;
	}
	public void setUsaResiduos(Boolean usaResiduos) {
		this.usaResiduos = usaResiduos;
	}
	public Boolean getUsaMadeira() {
		return usaMadeira;
	}
	public void setUsaMadeira(Boolean usaMadeira) {
		this.usaMadeira = usaMadeira;
	}
	public Boolean getUsaLenha() {
		return usaLenha;
	}
	public void setUsaLenha(Boolean usaLenha) {
		this.usaLenha = usaLenha;
	}
	public Boolean getFazReusoAgua() {
		return fazReusoAgua;
	}
	public void setFazReusoAgua(Boolean fazReusoAgua) {
		this.fazReusoAgua = fazReusoAgua;
	}
	public Boolean getReusoMaterialDescartado() {
		return reusoMaterialDescartado;
	}
	public void setReusoMaterialDescartado(Boolean reusoMaterialDescartado) {
		this.reusoMaterialDescartado = reusoMaterialDescartado;
	}
	public Boolean getTrocouInsumo() {
		return trocouInsumo;
	}
	public void setTrocouInsumo(Boolean trocouInsumo) {
		this.trocouInsumo = trocouInsumo;
	}
	public Boolean getEnergiaRenovavel() {
		return energiaRenovavel;
	}
	public void setEnergiaRenovavel(Boolean energiaRenovavel) {
		this.energiaRenovavel = energiaRenovavel;
	}
	public Boolean getTerraDesmatamento() {
		return terraDesmatamento;
	}
	public void setTerraDesmatamento(Boolean terraDesmatamento) {
		this.terraDesmatamento = terraDesmatamento;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getXmlEnviado() {
		return xmlEnviado;
	}
	public void setXmlEnviado(String xmlEnviado) {
		this.xmlEnviado = xmlEnviado;
	}
	public String getNumeroMatriculaUsuario() {
		return numeroMatriculaUsuario;
	}
	public void setNumeroMatriculaUsuario(String numeroMatriculaUsuario) {
		this.numeroMatriculaUsuario = numeroMatriculaUsuario;
	}
	public String getCodigoConvenio() {
		return codigoConvenio;
	}
	public void setCodigoConvenio(String codigoConvenio) {
		this.codigoConvenio = codigoConvenio;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public List<AtividadePrincipal> getListaAtividadePrincipal() {
		return listaAtividadePrincipal;
	}
	public void setListaAtividadePrincipal(
			List<AtividadePrincipal> listaAtividadePrincipal) {
		this.listaAtividadePrincipal = listaAtividadePrincipal;
	}
	public List<OrganizacaoSocial> getListaOganizacaoSocial() {
		return listaOganizacaoSocial;
	}
	public void setListaOganizacaoSocial(
			List<OrganizacaoSocial> listaOganizacaoSocial) {
		this.listaOganizacaoSocial = listaOganizacaoSocial;
	}
	public List<PosseTerra> getListaPosseTerra() {
		return listaPosseTerra;
	}
	public void setListaPosseTerra(List<PosseTerra> listaPosseTerra) {
		this.listaPosseTerra = listaPosseTerra;
	}
	public List<DestinacaoDeclaracao> getListaDestinacaoDeclaracao() {
		return listaDestinacaoDeclaracao;
	}
	public void setListaDestinacaoDeclaracao(
			List<DestinacaoDeclaracao> listaDestinacaoDeclaracao) {
		this.listaDestinacaoDeclaracao = listaDestinacaoDeclaracao;
	}
	
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	public Double getAreaExplorada() {
		return areaExplorada;
	}
	public void setAreaExplorada(Double areaExplorada) {
		this.areaExplorada = areaExplorada;
	}
	public String getUnidadeMedida() {
		return unidadeMedida;
	}
	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public String getNumeroAutoDeclaracao() {
		return numeroAutoDeclaracao;
	}
	public void setNumeroAutoDeclaracao(String numeroAutoDeclaracao) {
		this.numeroAutoDeclaracao = numeroAutoDeclaracao;
	}
	public String getNumeroImovel() {
		return numeroImovel;
	}
	
}
