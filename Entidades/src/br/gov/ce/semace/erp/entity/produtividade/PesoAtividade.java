package br.gov.ce.semace.erp.entity.produtividade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "peso_atividade", schema = "produtividade")
@Audited
public class PesoAtividade implements Serializable{

	private static final long serialVersionUID = 8692045713530157882L;
	
	@Id @SequenceGenerator(sequenceName="produtividade.seq_peso_atividade", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	@Column(name = "valor_peso")
	private Double valorPeso;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getValorPeso() {
		return valorPeso;
	}

	public void setValorPeso(Double valorPeso) {
		this.valorPeso = valorPeso;
	}

}
