package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;

public class ProcessoAdministrativo implements Serializable{

	private static final long serialVersionUID = -7309796344898840213L;

	private String processoSigaID;

	public String getProcessoSigaID() {
		return processoSigaID;
	}

	public void setProcessoSigaID(String processoSigaID) {
		this.processoSigaID = processoSigaID;
	}

}
