package br.gov.ce.semace.erp.entity.licenciamento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.geral.DocumentoQuestionario;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.SituacaoRama;

@Entity
@Audited
@Table(name = "relatorio_tecnico", schema = "licenciamento")
public class RelatorioTecnico implements Serializable {

	private static final long serialVersionUID = 4410622824839310573L;

	@Id
	@SequenceGenerator(sequenceName = "licenciamento.seq_relatorio_tecnico", name = "SEQ_RELATORIO_TECNICO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_RELATORIO_TECNICO")
	private Long id;

	@Column(name = "data_analise")
	private Date dataAnalise;

	@Column(name = "numero_relatorio",columnDefinition="text")
	private String numeroRelatorio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documento_questionario_id")
	private DocumentoQuestionario documentoQuestionario;

	@Column(name = "consideracao_final",columnDefinition="text")
	private String consideracaoFinal;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documento_id", nullable = true)
	private Documento documento;

	@Column(name = "aprovado_tecnico")
	private Boolean aprovadoTecnico;

	@Column(name = "aprovado_gerente")
	private Boolean aprovadoGerente;

	@Column(name = "data_aprovacao_gerente")
	private Date dataAprovacaoGerente;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "analise_tecnica_id")
	private AnaliseTecnicoRama analiseTecnica;

	@Column(name = "resultado_analise")
	private SituacaoRama resultadoAnalise;

	@Column(name = "finalizado")
	private boolean finalizado;

	@Column(name = "tipo_relatorio")
	private TipoRelatorioRama tipoRelatorio;

	@Transient
	private Funcionario funcionario = new Funcionario();

	@Transient
	private Date dataInicio;

	@Transient
	private Date dataFinal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataAnalise() {
		return dataAnalise;
	}

	public void setDataAnalise(Date dataAnalise) {
		this.dataAnalise = dataAnalise;
	}

	public String getNumeroRelatorio() {
		return numeroRelatorio;
	}

	public void setNumeroRelatorio(String numeroRelatorio) {
		this.numeroRelatorio = numeroRelatorio;
	}

	public DocumentoQuestionario getDocumentoQuestionario() {
		return documentoQuestionario;
	}

	public void setDocumentoQuestionario(
			DocumentoQuestionario documentoQuestionario) {
		this.documentoQuestionario = documentoQuestionario;
	}

	public String getConsideracaoFinal() {
		return consideracaoFinal;
	}

	public void setConsideracaoFinal(String consideracaoFinal) {
		this.consideracaoFinal = consideracaoFinal;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Boolean getAprovadoTecnico() {
		return aprovadoTecnico;
	}

	public void setAprovadoTecnico(Boolean aprovadoTecnico) {
		this.aprovadoTecnico = aprovadoTecnico;
	}

	public Boolean getAprovadoGerente() {
		return aprovadoGerente;
	}

	public void setAprovadoGerente(Boolean aprovadoGerente) {
		this.aprovadoGerente = aprovadoGerente;
	}

	public Date getDataAprovacaoGerente() {
		return dataAprovacaoGerente;
	}

	public void setDataAprovacaoGerente(Date dataAprovacaoGerente) {
		this.dataAprovacaoGerente = dataAprovacaoGerente;
	}

	public AnaliseTecnicoRama getAnaliseTecnica() {
		return analiseTecnica;
	}

	public void setAnaliseTecnica(AnaliseTecnicoRama analiseTecnica) {
		this.analiseTecnica = analiseTecnica;
	}

	public SituacaoRama getResultadoAnalise() {
		return resultadoAnalise;
	}

	public void setResultadoAnalise(SituacaoRama resultadoAnalise) {
		this.resultadoAnalise = resultadoAnalise;
	}

	public boolean isFinalizado() {
		return finalizado;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}

	public TipoRelatorioRama getTipoRelatorio() {
		return tipoRelatorio;
	}

	public void setTipoRelatorio(TipoRelatorioRama tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
}
