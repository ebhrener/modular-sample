package br.gov.ce.semace.erp.entity.julgamento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.geral.TemplateDocumento;
import br.gov.ce.semace.erp.enuns.StatusDecisao;
import br.gov.ce.semace.erp.enuns.TipoComunicacao;
import br.gov.ce.semace.erp.enuns.TipoDecisao;
import br.gov.ce.semace.erp.utils.DateUtils;

@Entity
@Audited
@Table(name="decisao", schema = "julgamento")
public class Decisao implements Serializable {

	private static final long serialVersionUID = 7517776508738013977L;

	@Id
	@SequenceGenerator(sequenceName = "julgamento.seq_decisao", name = "SEQ_DECISAO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DECISAO")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "julgamento_id")
	private Julgamento julgamento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documento_id")
	private Documento documento;

	@Column(columnDefinition = "text")
	private String texto;

	@Column(name="status_decisao")
	private StatusDecisao statusDecisao;

	@Column(name="finalizada")
	private boolean finalizada = false;

	@Column(name="tipo_comunicacao")
	private TipoComunicacao tipoComunicacao;

	@Column(name="ativa")
	private boolean ativa = true;

	@Column(name="tipo_decisao")
	private TipoDecisao tipoDecisao;

	@Temporal(TemporalType.DATE)
	@Column(name="data_conclusao")
	private Date dataConclusao;

	@Temporal(TemporalType.TIME)
	@Column(name="hora_conclusao")
	private Date horaConclusao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "template_documento_id")
	private TemplateDocumento templateDocumento;

	@OneToMany(mappedBy = "decisao", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<DecisaoMajoracao> listDecisaoMajoracao = new ArrayList<>();

	@Column(name = "valor_consolidado", precision=15, scale=2)
	private BigDecimal valorConsolidado;


	/**
	 * Fornece os campos {@link #dataConclusao} e {@link #horaConclusao} formatados.
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 31/07/2013 09:58:06
	 *
	 * @return retorna uma {@link String} com de acordo com o padrão {@link DateUtils#BRAZIL_FORMAT} para a {@link #dataConclusao} e
	 * {@link DateUtils#BRAZIL_SHORT_TIME_FORMAT} para a {@link #horaConclusao}, concatenados e separados por um espaço em branco.
	 */
	@Transient
	public String getDataHoraConclusaoFormatada(){

		if (this.dataConclusao != null && this.horaConclusao != null) {
			// formata a data
			final String dataFormatada = DateUtils.toString(this.dataConclusao);
			// formata o tempo
			final String horaFormatada = DateUtils.getShortTime(this.horaConclusao);
			// concatena e separa por espaços a string a ser retornada
			return dataFormatada + "   às " + horaFormatada;
		}

		return "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Julgamento getJulgamento() {
		return julgamento;
	}

	public void setJulgamento(Julgamento julgamento) {
		this.julgamento = julgamento;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public StatusDecisao getStatusDecisao() {
		return statusDecisao;
	}

	public void setStatusDecisao(StatusDecisao statusDecisao) {
		this.statusDecisao = statusDecisao;
	}

	public TipoComunicacao getTipoComunicacao() {
		return tipoComunicacao;
	}

	public void setTipoComunicacao(TipoComunicacao tipoComunicacao) {
		this.tipoComunicacao = tipoComunicacao;
	}

	public TipoDecisao getTipoDecisao() {
		return tipoDecisao;
	}

	public void setTipoDecisao(TipoDecisao tipoDecisao) {
		this.tipoDecisao = tipoDecisao;
	}

	public TemplateDocumento getTemplateDocumento() {
		return templateDocumento;
	}

	public void setTemplateDocumento(TemplateDocumento templateDocumento) {
		this.templateDocumento = templateDocumento;
	}

	public List<DecisaoMajoracao> getListDecisaoMajoracao() {
		return listDecisaoMajoracao;
	}

	public void setListDecisaoMajoracao(List<DecisaoMajoracao> listDecisaoMajoracao) {
		this.listDecisaoMajoracao = listDecisaoMajoracao;
	}

	public Date getDataConclusao() {
		return dataConclusao;
	}

	public void setDataConclusao(Date dataConclusao) {
		this.dataConclusao = dataConclusao;
	}

	public Date getHoraConclusao() {
		return horaConclusao;
	}

	public void setHoraConclusao(Date horaConclusao) {
		this.horaConclusao = horaConclusao;
	}

	/**
	 * @return the valorConsolidado
	 */
	public BigDecimal getValorConsolidado() {
		return valorConsolidado;
	}

	/**
	 * @param valorConsolidado the valorConsolidado to set
	 */
	public void setValorConsolidado(BigDecimal valorConsolidado) {
		this.valorConsolidado = valorConsolidado;
	}

	public boolean isFinalizada() {
		return finalizada;
	}

	public void setFinalizada(boolean finalizada) {
		this.finalizada = finalizada;
	}

	public boolean isAtiva() {
		return ativa;
	}

	public void setAtiva(boolean ativa) {
		this.ativa = ativa;
	}


}
