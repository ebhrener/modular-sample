package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="mapa_tematico")
public class MapaTematico implements Serializable {

	private static final long serialVersionUID = -995172300986185173L;

	@Id 
	@SequenceGenerator(sequenceName="seq_mapa_tematico", name="seq_mapa_tematico", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_mapa_tematico")
	private Long id;
	
	@Column
	private String nome;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="tipo_mapa_tematico_id")
	private TipoMapaTematico tipoMapaTematico = new TipoMapaTematico();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "mapaTematico", cascade = CascadeType.ALL)
	private List<CamadaMapaTematico> listaCamadaMapaTematico;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoMapaTematico getTipoMapaTematico() {
		return tipoMapaTematico;
	}

	public void setTipoMapaTematico(TipoMapaTematico tipoMapaTematico) {
		this.tipoMapaTematico = tipoMapaTematico;
	}

	public List<CamadaMapaTematico> getListaCamadaMapaTematico() {
		return listaCamadaMapaTematico;
	}

	public void setListaCamadaMapaTematico(
			List<CamadaMapaTematico> listaCamadaMapaTematico) {
		this.listaCamadaMapaTematico = listaCamadaMapaTematico;
	}
	
	
}