package br.gov.ce.semace.erp.entity.julgamento;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="decisao_majoracao", schema="julgamento")
public class DecisaoMajoracao implements Serializable{

	private static final long serialVersionUID = 7738874823606765578L;
	
	@Id    
	@SequenceGenerator(sequenceName ="julgamento.seq_decisao_majoracao", name = "SEQDECISAOMAJORACAO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQDECISAOMAJORACAO")
	private Long id;
	
	private BigDecimal valor;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="decisao_id")
	private Decisao decisao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="majoracao_id")
	private Majoracao majoracao;
	
	public DecisaoMajoracao clone() throws CloneNotSupportedException {
		DecisaoMajoracao decisaoMajoracao = new DecisaoMajoracao();
		decisaoMajoracao.setMajoracao(this.majoracao);
		decisaoMajoracao.setValor(this.valor);
		return decisaoMajoracao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Decisao getDecisao() {
		return decisao;
	}

	public void setDecisao(Decisao decisao) {
		this.decisao = decisao;
	}

	public Majoracao getMajoracao() {
		return majoracao;
	}

	public void setMajoracao(Majoracao majoracao) {
		this.majoracao = majoracao;
	}
	
}
