package br.gov.ce.semace.erp.entity.contrato;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "forma_contratual", schema="contrato")
public class FormaContratual implements Serializable {

	private static final long serialVersionUID = 6782792467308005363L;
	
	@Id
	@SequenceGenerator(sequenceName = "contrato.seq_forma_contratual", name = "SEQCONTRATO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQCONTRATO")
	private Long id;
	
	private String descricao;
	
	@Column(name = "prazo_forma_contratual")
	private Integer prazoFormaContratual; 
	
	@Column(name = "permitir_alteracao_contratual")
	private Boolean permitirAlteracaoContratual;
	
	@Column(name = "permitir_alteracao_periodo")
	private Boolean permitirAlteracaoPeriodo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getPrazoFormaContratual() {
		return prazoFormaContratual;
	}

	public void setPrazoFormaContratual(Integer prazoFormaContratual) {
		this.prazoFormaContratual = prazoFormaContratual;
	}

	public Boolean getPermitirAlteracaoContratual() {
		return permitirAlteracaoContratual;
	}

	public void setPermitirAlteracaoContratual(Boolean permitirAlteracaoContratual) {
		this.permitirAlteracaoContratual = permitirAlteracaoContratual;
	}

	public Boolean getPermitirAlteracaoPeriodo() {
		return permitirAlteracaoPeriodo;
	}

	public void setPermitirAlteracaoPeriodo(Boolean permitirAlteracaoPeriodo) {
		this.permitirAlteracaoPeriodo = permitirAlteracaoPeriodo;
	}
	
}
