package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;

@Entity
@Table(name = "status_externo_processo", schema = "adm_siga")
public class StatusExternoProcesso implements Serializable {

	private static final long serialVersionUID = -3588847036312393529L;

	@Id
	@SequenceGenerator(name = "SEQ_STATUS_EXTERNO_PROCESSO", sequenceName = "adm_siga.status_externo_processo_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STATUS_EXTERNO_PROCESSO")
	private Long id;

	@Max(10)
	@Column(name = "processo_id", nullable = false)
	private String processoId;

	@Column(name = "status_externo_id", nullable = false)
	private Integer statusExternoId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data")
	private Date dataCriacao = new Date();

	@Max(4)
	@Column(name = "area_id", nullable = false)
	private String areaId;

	public StatusExternoProcesso() {
		super();
	}

	public StatusExternoProcesso(String processoId, Integer statusExternoId, String areaId) {
		super();
		this.processoId = processoId;
		this.statusExternoId = statusExternoId;
		this.areaId = areaId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProcessoId() {
		return processoId;
	}

	public void setProcessoId(String processoId) {
		this.processoId = processoId;
	}

	public Integer getStatusExternoId() {
		return statusExternoId;
	}

	public void setStatusExternoId(Integer statusExternoId) {
		this.statusExternoId = statusExternoId;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

}