package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.atendimento.requerimento.ArquivoRequerimento;
import br.gov.ce.semace.erp.entity.licenciamento.ItensCheckList;

@Entity
@Audited
@Table(name = "arquivo_requerimento_item_checklist", schema = "atendimento")
public class ArquivoRequerimentoItemChecklist implements Serializable {

	private static final long serialVersionUID = 6073526150703228317L;

	@Id @Column(name = "id")
	@SequenceGenerator(sequenceName="atendimento.seq_arquivo_requerimento_item_checklist", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "arquivo_requerimento_id")
	private ArquivoRequerimento arquivoRequerimento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "item_checklist_id")
	private ItensCheckList itemChecklist;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ArquivoRequerimento getArquivoRequerimento() {
		return arquivoRequerimento;
	}

	public void setArquivoRequerimento(ArquivoRequerimento arquivoRequerimento) {
		this.arquivoRequerimento = arquivoRequerimento;
	}

	public ItensCheckList getItemChecklist() {
		return itemChecklist;
	}

	public void setItemChecklist(ItensCheckList itemChecklist) {
		this.itemChecklist = itemChecklist;
	}

}
