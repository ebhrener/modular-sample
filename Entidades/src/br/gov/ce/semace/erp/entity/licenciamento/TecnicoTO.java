package br.gov.ce.semace.erp.entity.licenciamento;

import br.gov.ce.semace.erp.entity.rh.Funcionario;

public class TecnicoTO {

	private Funcionario tecnico = new Funcionario();

	private Long quantidadeRamas;

	private Long quantidadeRamasAnalisado30dias;

	private Long quantidadeRamasRecebidos30dias;

	private boolean selecionadoReceber = false;

	private boolean selecionadoVisualizar = false;

	public Long getQuantidadeRamas() {
		return quantidadeRamas;
	}

	public void setQuantidadeRamas(Long quantidadeRamas) {
		this.quantidadeRamas = quantidadeRamas;
	}

	public Funcionario getTecnico() {
		return tecnico;
	}

	public void setTecnico(Funcionario tecnico) {
		this.tecnico = tecnico;
	}

	public Long getQuantidadeRamasAnalisado30dias() {
		return quantidadeRamasAnalisado30dias;
	}

	public void setQuantidadeRamasAnalisado30dias(Long quantidadeRamasAnalisado30dias) {
		this.quantidadeRamasAnalisado30dias = quantidadeRamasAnalisado30dias;
	}

	public Long getQuantidadeRamasRecebidos30dias() {
		return quantidadeRamasRecebidos30dias;
	}

	public void setQuantidadeRamasRecebidos30dias(Long quantidadeRamasRecebidos30dias) {
		this.quantidadeRamasRecebidos30dias = quantidadeRamasRecebidos30dias;
	}

	public boolean isSelecionadoReceber() {
		return selecionadoReceber;
	}

	public void setSelecionadoReceber(boolean selecionadoReceber) {
		this.selecionadoReceber = selecionadoReceber;
	}

	public boolean isSelecionadoVisualizar() {
		return selecionadoVisualizar;
	}

	public void setSelecionadoVisualizar(boolean selecionadoVisualizar) {
		this.selecionadoVisualizar = selecionadoVisualizar;
	}

}
