package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "rh", name = "habilitacao")
public class Habilitacao implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3061202081763630775L;

	@Id
	@SequenceGenerator(sequenceName = "rh.seq_habilitacao", name = "SEQ_HABILITACAO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_HABILITACAO")
	private Long id;
	
	private String descricao;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy="listHabilitacao")
	private List<Funcionario> listFuncionario = new ArrayList<>();
	
	@Column(name="ativo")
	private boolean ativo = true;

	//================================== GETERS and SETERS ====================================//

	@Transient
	public String getDetalheAtivo(){

		if (this.ativo){
			return "Sim";
		}

		return "Não";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Funcionario> getListFuncionario() {
		return listFuncionario;
	}

	public void setListFuncionario(List<Funcionario> listFuncionario) {
		this.listFuncionario = listFuncionario;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Habilitacao other = (Habilitacao) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}

