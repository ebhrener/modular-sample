package br.gov.ce.semace.erp.entity.tramite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.julgamento.Julgamento;
import br.gov.ce.semace.erp.entity.protocolo.Processo;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.entity.rh.Setor;
import br.gov.ce.semace.erp.enuns.StatusTramitacao;

@Entity
@Audited
@Table(schema="protocolo")
public class Tramite implements Serializable{

	private static final long serialVersionUID = 1647323576620969962L;
	
	@Id 
	@SequenceGenerator(sequenceName = "protocolo.seq_tramite", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "responsavel_acao_id")
	private Funcionario responsavelAcao = new Funcionario();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rementente_id")
	private Funcionario remetente = new Funcionario();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "destinatario_id")
	private Funcionario destinatario = new Funcionario();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setor_rementente_id")
	private Setor setorRemetente = new Setor();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setor_destinatario_id")
	private Setor setorDestinatario = new Setor();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documento_id")
	private Documento documento;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "tramite", targetEntity = Despacho.class)
	private List<Despacho> despachos;
	
	@Column(name = "id_documento_despacho")
	private Long idDocumentoDespacho;

	@Column(name = "status_tramitacao")
	private StatusTramitacao statusTramitacao;
	
	@Column(name = "ultimo_fila")
	private Boolean isUltimoFila;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_tramitacao")
	private Date dataTramitacao;
	
	@Temporal(TemporalType.TIME)
	private Date hora;

	@Column(columnDefinition = "text")
	private String observacao;
	
	private String assunto;
	
	private Boolean assinado;
	
	@Transient
	private Long quantidadeDiasPauta;
	
	@Transient
	private List<Setor> setoresFilhos = new ArrayList<Setor>();
	
	@Transient
	private Processo processo;
	
	@Transient
	private byte[] bytesDocumentoDespacho;
	
	@Transient
	private Boolean selecionado = false;
	
	@Transient
	private Julgamento julgamento;
	
	@Transient
	private List<StatusTramitacao> listaStatusTramitacao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getRemetente() {
		return remetente;
	}

	public void setRemetente(Funcionario remetente) {
		this.remetente = remetente;
	}

	public Funcionario getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(Funcionario destinatario) {
		this.destinatario = destinatario;
	}

	public Setor getSetorRemetente() {
		return setorRemetente;
	}

	public void setSetorRemetente(Setor setorRemetente) {
		this.setorRemetente = setorRemetente;
	}

	public Setor getSetorDestinatario() {
		return setorDestinatario;
	}

	public void setSetorDestinatario(Setor setorDestinatario) {
		this.setorDestinatario = setorDestinatario;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public StatusTramitacao getStatusTramitacao() {
		return statusTramitacao;
	}

	public void setStatusTramitacao(StatusTramitacao statusTramitacao) {
		this.statusTramitacao = statusTramitacao;
	}

	public Boolean getIsUltimoFila() {
		return isUltimoFila;
	}

	public void setIsUltimoFila(Boolean isUltimoFila) {
		this.isUltimoFila = isUltimoFila;
	}

	public Date getDataTramitacao() {
		return dataTramitacao;
	}

	public void setDataTramitacao(Date dataTramitacao) {
		this.dataTramitacao = dataTramitacao;
	}
	
	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public List<Setor> getSetoresFilhos() {
		return setoresFilhos;
	}

	public void setSetoresFilhos(List<Setor> setoresFilhos) {
		this.setoresFilhos = setoresFilhos;
	}

	public Boolean getAssinado() {
		return assinado;
	}

	public void setAssinado(Boolean assinado) {
		this.assinado = assinado;
	}

	public Long getQuantidadeDiasPauta() {
		return quantidadeDiasPauta;
	}

	public void setQuantidadeDiasPauta(Long quantidadeDiasPauta) {
		this.quantidadeDiasPauta = quantidadeDiasPauta;
	}

	public Funcionario getResponsavelAcao() {
		return responsavelAcao;
	}

	public void setResponsavelAcao(Funcionario responsavelAcao) {
		this.responsavelAcao = responsavelAcao;
	}
	
	public List<Despacho> getDespachos() {
		return despachos;
	}

	public void setDespachos(List<Despacho> despachos) {
		this.despachos = despachos;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public Long getIdDocumentoDespacho() {
		return idDocumentoDespacho;
	}

	public void setIdDocumentoDespacho(Long idDocumentoDespacho) {
		this.idDocumentoDespacho = idDocumentoDespacho;
	}

	public byte[] getBytesDocumentoDespacho() {
		return bytesDocumentoDespacho;
	}

	public void setBytesDocumentoDespacho(byte[] bytesDocumentoDespacho) {
		this.bytesDocumentoDespacho = bytesDocumentoDespacho;
	}

	public Boolean getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(Boolean selecionado) {
		this.selecionado = selecionado;
	}

	public Julgamento getJulgamento() {
		return julgamento;
	}

	public void setJulgamento(Julgamento julgamento) {
		this.julgamento = julgamento;
	}
	
	public List<StatusTramitacao> getListaStatusTramitacao() {
		return listaStatusTramitacao;
	}

	public void setListaStatusTramitacao(
			List<StatusTramitacao> listaStatusTramitacao) {
		this.listaStatusTramitacao = listaStatusTramitacao;
	}

}
