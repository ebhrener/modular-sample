package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "resposta_opcao", schema = "geral")
public class RespostaOpcao implements Serializable{

	private static final long serialVersionUID = 1911891745256162164L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_resposta_opcao", name = "seq_resposta_opcao", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_resposta_opcao")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "cabecalho_opcao_id")
	private CabecalhoOpcao cabecalhoOpcao;

	@Column(name = "resposta", columnDefinition = "text")
	private String resposta;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "resposta_questionario_id")
	private RespostaQuestionario respostaQuestionario = new RespostaQuestionario();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getResposta() {
		return resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	public RespostaQuestionario getRespostaQuestionario() {
		return respostaQuestionario;
	}

	public void setRespostaQuestionario(RespostaQuestionario respostaQuestionario) {
		this.respostaQuestionario = respostaQuestionario;
	}

	public CabecalhoOpcao getCabecalhoOpcao() {
		return cabecalhoOpcao;
	}

	public void setCabecalhoOpcao(CabecalhoOpcao cabecalhoOpcao) {
		this.cabecalhoOpcao = cabecalhoOpcao;
	}
}