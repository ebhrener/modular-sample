package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "shape_equipamento")
public class ShapeEquipamento implements Serializable {

	private static final long serialVersionUID = 6691831280938927691L;

	@Id
	@SequenceGenerator(sequenceName="seq_shape_equipamento", name="seq_shape_equipamento", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_shape_equipamento")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="processo_id")
	private ProcessoGeo processo = new ProcessoGeo();

	@Column(nullable = false, unique = true, length = 100)
	private String tabela;

	@Column(nullable = false)
	private Boolean ativo = Boolean.TRUE;

	@Deprecated
	@Column(name = "atividade_id")
	private Long idAtividade;

	@OneToMany(mappedBy="shapeEquipamento", fetch=FetchType.LAZY)
	private List<HistoricoShapeEquipamento> historico;

	@Column(name= "raci_id")
	private Long raciID;

	public ShapeEquipamento() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProcessoGeo getProcesso() {
		return processo;
	}

	public void setProcesso(ProcessoGeo processo) {
		this.processo = processo;
	}

	public String getTabela() {
		return tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Deprecated
	public Long getIdAtividade() {
		return idAtividade;
	}

	@Deprecated
	public void setIdAtividade(Long idAtividade) {
		this.idAtividade = idAtividade;
	}

	public List<HistoricoShapeEquipamento> getHistorico() {
		return historico;
	}

	public void setHistorico(List<HistoricoShapeEquipamento> historico) {
		this.historico = historico;
	}

	public Long getRaciID() {
		return raciID;
	}

	public void setRaciID(Long raciID) {
		this.raciID = raciID;
	}

}