package br.gov.ce.semace.erp.entity.financeiro.to;

/**
 * TO para receber os dados do relatório de arrecadação anual por produto vindo da base de dados.
 *
 * @author rodrigo.silva - Rodrigo Silva Oliveira - 21/03/2017 13:04:37
 */
public class RelatorioArrecadacaoAnualProdutoTO {

	private Integer ano;
	private Integer mes;
	private Integer codigoProduto;
	private String descricao;
	private Double valor;

	public String getDescricaoMes() {
		String descricaoMes = "";
		switch (this.mes) {
			case 1:
				descricaoMes = "Janeiro";
				break;
			case 2:
				descricaoMes = "Fevereiro";
				break;
			case 3:
				descricaoMes = "Março";
				break;
			case 4:
				descricaoMes = "Abril";
				break;
			case 5:
				descricaoMes = "Maio";
				break;
			case 6:
				descricaoMes = "Junho";
				break;
			case 7:
				descricaoMes = "Julho";
				break;
			case 8:
				descricaoMes = "Agosto";
				break;
			case 9:
				descricaoMes = "Setembro";
				break;
			case 10:
				descricaoMes = "Outubro";
				break;
			case 11:
				descricaoMes = "Novembro";
				break;
			case 12:
				descricaoMes = "Dezembro";
				break;
			default:
				break;
		}
		return descricaoMes;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(Integer codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
}