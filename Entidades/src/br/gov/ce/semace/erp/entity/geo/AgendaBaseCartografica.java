package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.semace.erp.enuns.StatusAgenda;

@Entity
@Table(name="agenda_base_cartografica")
public class AgendaBaseCartografica implements Serializable{
	
	private static final long serialVersionUID = -8814444919055256589L;

	@Id 
	@SequenceGenerator(sequenceName="seq_agenda_base_cartografica", name="seq_agenda_base_cartografica", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_agenda_base_cartografica")
	private Long id;
	
	@Column(name="status_agenda")
	@Enumerated(EnumType.STRING)
	private StatusAgenda statusAgenda;
	
	@OneToOne
	@JoinColumn(name="shape_base_cartografica_id")
	private ShapeBaseCartografica shapeBaseCartografica;	

	public ShapeBaseCartografica getShapeBaseCartografica() {
		return shapeBaseCartografica;
	}

	public void setShapeBaseCartografica(ShapeBaseCartografica shapeBaseCartografica) {
		this.shapeBaseCartografica = shapeBaseCartografica;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public StatusAgenda getStatusAgenda() {
		return statusAgenda;
	}

	public void setStatusAgenda(StatusAgenda statusAgenda) {
		this.statusAgenda = statusAgenda;
	}

}
