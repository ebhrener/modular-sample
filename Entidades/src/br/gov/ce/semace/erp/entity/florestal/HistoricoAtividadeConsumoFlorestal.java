package br.gov.ce.semace.erp.entity.florestal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Atividade;
import br.gov.ce.semace.erp.entity.geral.Interessado;
import br.gov.ce.semace.erp.entity.geral.UnidadeMedida;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.TipoAcao;
import br.gov.ce.semace.erp.enuns.TipoSistema;

@Entity
@Audited
@Table(name = "historico_atividade_consumo_florestal", schema = "florestal")
public class HistoricoAtividadeConsumoFlorestal implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -8758533128450998365L;

	@Id
	@SequenceGenerator(sequenceName = "florestal.seq_historico_atividade_consumo_florestal", name = "florestal.seq_historico_atividade_consumo_florestal", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "florestal.seq_historico_atividade_consumo_florestal")
	private Long id;

	@Column(name="is_principal")
	private Boolean principal;

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="cadastro_consumo_florestal_id")
	private CadastroConsumoFlorestal cadastroConsumoFlorestal = new CadastroConsumoFlorestal();

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="atividade_id")
	private Atividade atividade = new Atividade();

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="atividade_florestal_id")
	private AtividadeFlorestal atividadeFlorestal = new AtividadeFlorestal();

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="classificacao_id")
	private Classificacao classificacao = new Classificacao();

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="producao_id")
	private Producao producao = new Producao();

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="dado_consumo_id")
	private DadosConsumo dadosConsumo;

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="produto_id")
	private Produto produto = new Produto();

	@Column(name="producao_media")
	private BigDecimal producaoMedia;

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="unidade_medida_producao_id")
	private UnidadeMedida unidadeMedidaProducao;

	@Column(name="comsumo_medio")
	private BigDecimal consumoMedio;

	@ManyToOne(optional=true,fetch=FetchType.LAZY)
	@JoinColumn(name="unidade_medida_consumo_id")
	private UnidadeMedida unidadeMedidaConsumo;

	@Column(name="quantidade_meses_consumo")
	private Integer qtdMesesConsumo;

	@ManyToOne(optional=true, fetch=FetchType.LAZY)
	@JoinColumn(name="tipologia_id")
	private Tipologia tipologia = new Tipologia();

	@Column(name = "descricao_outra_tipologia")
	private String descricao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data")
    private Date data;

	@Column(name="tipo_acao")
    private TipoAcao tipoAcao;

	@Column(name="sistema")
    private TipoSistema sistema;

	@Column(name="justificativa")
    private String justificativa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario;

    @Column(name="atividade_consumo_florestal_id")
	private Long atividadeConsumoFlorestalId;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "interessado_id")
	private Interessado interessado;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public CadastroConsumoFlorestal getCadastroConsumoFlorestal() {
		return cadastroConsumoFlorestal;
	}


	public void setCadastroConsumoFlorestal(
			CadastroConsumoFlorestal cadastroConsumoFlorestal) {
		this.cadastroConsumoFlorestal = cadastroConsumoFlorestal;
	}


	public Atividade getAtividade() {
		return atividade;
	}


	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}


	public Classificacao getClassificacao() {
		return classificacao;
	}


	public void setClassificacao(Classificacao classificacao) {
		this.classificacao = classificacao;
	}


	public Producao getProducao() {
		return producao;
	}


	public void setProducao(Producao producao) {
		this.producao = producao;
	}


	public Produto getProduto() {
		return produto;
	}


	public void setProduto(Produto produto) {
		this.produto = produto;
	}


	public BigDecimal getProducaoMedia() {
		return producaoMedia;
	}


	public void setProducaoMedia(BigDecimal producaoMedia) {
		this.producaoMedia = producaoMedia;
	}


	public UnidadeMedida getUnidadeMedidaProducao() {
		return unidadeMedidaProducao;
	}


	public void setUnidadeMedidaProducao(UnidadeMedida unidadeMedidaProducao) {
		this.unidadeMedidaProducao = unidadeMedidaProducao;
	}


	public BigDecimal getConsumoMedio() {
		return consumoMedio;
	}


	public void setConsumoMedio(BigDecimal consumoMedio) {
		this.consumoMedio = consumoMedio;
	}


	public UnidadeMedida getUnidadeMedidaConsumo() {
		return unidadeMedidaConsumo;
	}


	public void setUnidadeMedidaConsumo(UnidadeMedida unidadeMedidaConsumo) {
		this.unidadeMedidaConsumo = unidadeMedidaConsumo;
	}


	public Integer getQtdMesesConsumo() {
		return qtdMesesConsumo;
	}


	public void setQtdMesesConsumo(Integer qtdMesesConsumo) {
		this.qtdMesesConsumo = qtdMesesConsumo;
	}


	public Tipologia getTipologia() {
		return tipologia;
	}


	public void setTipologia(Tipologia tipologia) {
		this.tipologia = tipologia;
	}


	public AtividadeFlorestal getAtividadeFlorestal() {
		return atividadeFlorestal;
	}


	public void setAtividadeFlorestal(AtividadeFlorestal atividadeFlorestal) {
		this.atividadeFlorestal = atividadeFlorestal;
	}

	public String getDisplayAtividade(){
		if (atividade != null && atividade.getId() != null) {
			return atividade.getCodigo()+" - "+atividade.getDescricao();
		}else if (atividadeFlorestal != null && atividadeFlorestal.getId() != null){
			if (atividadeFlorestal.getCodigo() != null && !atividadeFlorestal.getCodigo().trim().isEmpty()) {
				return atividadeFlorestal.getCodigo()+" - "+atividadeFlorestal.getDescricao();
			}
			return atividadeFlorestal.getDescricao();
		}
		return null;
	}


	public Boolean getPrincipal() {
		return principal;
	}


	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}


	public DadosConsumo getDadosConsumo() {
		return dadosConsumo;
	}


	public void setDadosConsumo(DadosConsumo dadosConsumo) {
		this.dadosConsumo = dadosConsumo;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public Date getData() {
		return data;
	}


	public void setData(Date data) {
		this.data = data;
	}

	public TipoAcao getTipoAcao() {
		return tipoAcao;
	}


	public void setTipoAcao(TipoAcao tipoAcao) {
		this.tipoAcao = tipoAcao;
	}


	public TipoSistema getSistema() {
		return sistema;
	}


	public void setSistema(TipoSistema sistema) {
		this.sistema = sistema;
	}


	public String getJustificativa() {
		return justificativa;
	}

	/**
	 * Informa o valor do campo {@link #justificativa} até o limite que for passado por parâmetro.
	 *
	 * @author rodrigo.silva - Rodrigo Silva Oliveira - 13/03/2017 11:57:58
	 *
	 * @param quantidadeCaracteres - Valor que representa a quantidade de caracteres a ser repassado
	 *
	 * @return - Retorna o valor do campo justificativa, caso o tamanho da {@link String} da justificativa for
	 * menor que a quantidade de caracteres passada por parametro. Caso contrário, retorna uma {@link String}
	 * com o valor da justificativa até o limite de quantidade de caracteres requisitado, acrescido de <i>...</i>
	 */
	@Transient
	public String getJustificativa(int quantidadeCaracteres) {
		if (this.justificativa.length() < quantidadeCaracteres) {
			return this.justificativa;
		}
		return justificativa.substring(0, quantidadeCaracteres) + " <bold>...</bold>";
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}


	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}


	public Long getAtividadeConsumoFlorestalId() {
		return atividadeConsumoFlorestalId;
	}

	public void setAtividadeConsumoFlorestalId(Long atividadeConsumoFlorestalId) {
		this.atividadeConsumoFlorestalId = atividadeConsumoFlorestalId;
	}


	public Interessado getInteressado() {
		return interessado;
	}


	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}
}