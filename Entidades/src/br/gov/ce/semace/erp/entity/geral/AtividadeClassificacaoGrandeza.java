package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.licenciamento.ClassificacaoGrandeza;
import br.gov.ce.semace.erp.entity.licenciamento.Grandeza;

@Entity
@Audited
@Table(name="atividade_classificacao_grandeza",schema = "geral")
public class AtividadeClassificacaoGrandeza implements Serializable {

	private static final long serialVersionUID = 8113189330132823924L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_atv_classificacao_grandeza", name = "geral.seq_atv_classificacao_grandeza", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geral.seq_atv_classificacao_grandeza")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="grandeza_id")
	private Grandeza grandeza;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="classificacao_grandeza_id")
	private ClassificacaoGrandeza classificacaoGrandeza;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="atividade_id")
	private Atividade atividade;

	@Column(name = "ativo")
	private boolean ativo;

	@Column(name = "indice_agrupador")
	private Integer indiceAgrupador;

//	@Column(name = "exibe_tabela")
//	private boolean exibeTabela = false;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "atividadeClassificacaoGrandeza", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<IntervaloAtividadeGrandeza> listIntervalos = new ArrayList<IntervaloAtividadeGrandeza>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Transient
	public String getDescricaoAtividadeClassificacaoGrandeza(){
		if(classificacaoGrandeza != null && grandeza != null){
			return classificacaoGrandeza.getDescricao() + " - " + grandeza.getDescricao();
		} else if(grandeza != null){
			return grandeza.getDescricao();
		}
		return "";
	}

	public Grandeza getGrandeza() {
		return grandeza;
	}

	public void setGrandeza(Grandeza grandeza) {
		this.grandeza = grandeza;
	}

	public ClassificacaoGrandeza getClassificacaoGrandeza() {
		return classificacaoGrandeza;
	}

	public void setClassificacaoGrandeza(ClassificacaoGrandeza classificacaoGrandeza) {
		this.classificacaoGrandeza = classificacaoGrandeza;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Integer getIndiceAgrupador() {
		return indiceAgrupador;
	}

	public void setIndiceAgrupador(Integer indiceAgrupador) {
		this.indiceAgrupador = indiceAgrupador;
	}

//	public boolean isExibeTabela() {
//		return exibeTabela;
//	}
//
//	public void setExibeTabela(boolean exibeTabela) {
//		this.exibeTabela = exibeTabela;
//	}

	public List<IntervaloAtividadeGrandeza> getListIntervalos() {
		return listIntervalos;
	}

	public void setListIntervalos(List<IntervaloAtividadeGrandeza> listIntervalos) {
		this.listIntervalos = listIntervalos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AtividadeClassificacaoGrandeza other = (AtividadeClassificacaoGrandeza) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public int compareTo(AtividadeClassificacaoGrandeza other) {
        if (other == null || other.getId() == null) {
        	return 1;
        }
        if (this.id > other.getId()) {
			return 1;
		} else if (this.id < other.getId()) {
			return -1;
		}
		else {
			return 0;
		}
	}
}