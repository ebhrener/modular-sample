package br.gov.ce.semace.erp.entity.protocolo.to;

import java.io.Serializable;

public class ConsultaProcessoTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private Integer numeroPaginas;
	private String tamanho;
	private Long indiceGed;
	private String spu;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}
	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}
	public String getTamanho() {
		return tamanho;
	}
	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}
	public Long getIndiceGed() {
		return indiceGed;
	}
	public void setIndiceGed(Long indiceGed) {
		this.indiceGed = indiceGed;
	}
	public String getSpu() {
		return spu;
	}
	public void setSpu(String spu) {
		this.spu = spu;
	}

}
