/**
 *
 */
package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.semace.erp.entity.seguranca.Usuario;

/**
 * {@link JustificativaAlteracaoBoleto}.
 *
 * @author antoniomenezes - Antonio Menezes de Araújo[antoniomenezes20@gmail.com] - 10/09/2015 15:10:49
 *
 */
@Entity
@Table(schema = "adm_siga", name = "justificativa_alteracao_boleto")
public class JustificativaAlteracaoBoleto implements Serializable {

	private static final long serialVersionUID = -8779464631454096988L;

	@Id
	@SequenceGenerator(sequenceName = "adm_siga.seq_justificativa_alteracao_boleto", name = "SEQ_JUSTIFICATIVA_ALTERACAO_BOLETO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_JUSTIFICATIVA_ALTERACAO_BOLETO")
	private Long id;

	@Column(name = "justificativa", columnDefinition = "text")
	private String justificativa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "boleto_id")
	private BoletoCef boletoCef = new BoletoCef();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_logado")
	private Usuario usuarioLogado;

	@Column(name="ativo", nullable = false)
	private boolean ativo = true;

	@Column(name = "data_deposito")
	private Date dataDeposito;

	@Column(name="valor_deposito")
	private Double valorDeposito;

	@Column(name="data_vinculo")
	private Date dataVinculo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public BoletoCef getBoletoCef() {
		return boletoCef;
	}

	public void setBoletoCef(BoletoCef boletoCef) {
		this.boletoCef = boletoCef;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Date getDataDeposito() {
		return dataDeposito;
	}

	public void setDataDeposito(Date dataDeposito) {
		this.dataDeposito = dataDeposito;
	}

	public Double getValorDeposito() {
		return valorDeposito;
	}

	public void setValorDeposito(Double valorDeposito) {
		this.valorDeposito = valorDeposito;
	}

	public Date getDataVinculo() {
		return dataVinculo;
	}

	public void setDataVinculo(Date dataVinculo) {
		this.dataVinculo = dataVinculo;
	}

}
