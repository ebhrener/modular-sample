package br.gov.ce.semace.erp.entity.financeiro.to;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

/**
 * Classe que representa um boleto oriundo da remessa emitida pelo banco
 * atributos criados conforme leiaute padrao CNAB 240 
 * @author leandros
 *
 */
public class BoletoArquivoTO implements Serializable {

	
	//srializacao
	private static final long serialVersionUID = 146090523908873098L;
	
	/**
	 * Atributos do objeto boleto onde tVARIAVEL para segmento T, uVARIAVEL para segmento U
	 * 
	 */
	
	private String numeroBoleto;
	
	private String tCodigoBancoCompensacao;
	private String tLoteServico;
	private String tRegistroDetalhe;
	private String tNumeroSequencialRegistroNoLote;
	private String tCodigoSegmentoDoRegistroDetalhe;
	private String tUsoExclusivoFebrabanCenab063;
	private String tCodigoDeMovimento;
	private String tAgenciaMantenedoraDaConta;
	private String tDigitoVerificadorDaAgencia;
	private String tNumeroDaContaCorrente;
	private String tDigitoVerificadorDaConta;
	private String tDigitoVerificadorDaAgenciaConta;
	private String tUsoExclusivoCaixa;
	private String tIdentificacaoDoTituloNoBanco;
	private String tIdentificacaoDotitiloNoBancoDigito;
	private String tCodigoDaCarteira;
	private String tNumeroDoDocumentoDeCobranca;
	private Date tDataDoVencimentoDoTitulo;
	private String tValorNominalDoTitulo;
	private String tUfEntidadeCedente;
	private String tBranco;
	private String tCodigoDaEntidadeCedente;
	private String tDigitoDaentidadeCedente;
	private String tNomeDaEntidadeCedente;
	private String tCodigoDaMoeda;
	private String tTipoDeInscricao;
	private String tNumeroDeInscricao;
	private String tNome;
	private String tUsoExclusivoFebrabanCenab283T;
	private Double tValorDaTarifaCustas;
	private String tIdentificacaoParaRejeicoes;
	private String tUsoExclusivoFebrabanCenab313T;
	private String uCodigoBancoCompensacao;
	private String uLoteServico;
	private String uRegistroDetalhe;
	private String uNumeroSequencialRegistroNoLote;
	private String uCodigoSegmentoDoRegistroDetalhe;
	private String uUsoExclusivoFebrabanCenab063U;
	private String ucodigoDeMovimento;
	private Double uJurosMultasEncargos;
	private Double uValorDoDescontoConcedido;
	private Double uValorDoAbatimentoConcedidoOuCancelado;
	private Double uValorDoIOFRecolhido;
	private Double uValorPagoPeloSacado;
	private Double uValorDeCreditoBruto;
	private Double uValorDeOutrasDespesas;
	private String uPercentualCreditoEntFinanceira;
	private String uBrancos;
	private Date uDataDaOcorrencia;
	private Date uDataDaEfetivacaoDoCredito;
	private String uUsoExclusivoFenabranCNAB193U;
	
	
	// ****************gets e sets*************************
	
	public String getNumeroBoleto() {
		return numeroBoleto;
	}
	public void setNumeroBoleto(String numeroBoleto) {
		this.numeroBoleto = numeroBoleto;
	}
	public String gettCodigoBancoCompensacao() {
		return tCodigoBancoCompensacao;
	}
	public void settCodigoBancoCompensacao(String tCodigoBancoCompensacao) {
		this.tCodigoBancoCompensacao = tCodigoBancoCompensacao;
	}
	public String gettLoteServico() {
		return tLoteServico;
	}
	public void settLoteServico(String tLoteServico) {
		this.tLoteServico = tLoteServico;
	}
	public String gettRegistroDetalhe() {
		return tRegistroDetalhe;
	}
	public void settRegistroDetalhe(String tRegistroDetalhe) {
		this.tRegistroDetalhe = tRegistroDetalhe;
	}
	public String gettNumeroSequencialRegistroNoLote() {
		return tNumeroSequencialRegistroNoLote;
	}
	public void settNumeroSequencialRegistroNoLote(
			String tNumeroSequencialRegistroNoLote) {
		this.tNumeroSequencialRegistroNoLote = tNumeroSequencialRegistroNoLote;
	}
	public String gettCodigoSegmentoDoRegistroDetalhe() {
		return tCodigoSegmentoDoRegistroDetalhe;
	}
	public void settCodigoSegmentoDoRegistroDetalhe(
			String tCodigoSegmentoDoRegistroDetalhe) {
		this.tCodigoSegmentoDoRegistroDetalhe = tCodigoSegmentoDoRegistroDetalhe;
	}
	public String gettUsoExclusivoFebrabanCenab063() {
		return tUsoExclusivoFebrabanCenab063;
	}
	public void settUsoExclusivoFebrabanCenab063(
			String tUsoExclusivoFebrabanCenab063) {
		this.tUsoExclusivoFebrabanCenab063 = tUsoExclusivoFebrabanCenab063;
	}
	public String gettCodigoDeMovimento() {
		return tCodigoDeMovimento;
	}
	public void settCodigoDeMovimento(String tCodigoDeMovimento) {
		this.tCodigoDeMovimento = tCodigoDeMovimento;
	}
	public String gettAgenciaMantenedoraDaConta() {
		return tAgenciaMantenedoraDaConta;
	}
	public void settAgenciaMantenedoraDaConta(String tAgenciaMantenedoraDaConta) {
		this.tAgenciaMantenedoraDaConta = tAgenciaMantenedoraDaConta;
	}
	public String gettDigitoVerificadorDaAgencia() {
		return tDigitoVerificadorDaAgencia;
	}
	public void settDigitoVerificadorDaAgencia(String tDigitoVerificadorDaAgencia) {
		this.tDigitoVerificadorDaAgencia = tDigitoVerificadorDaAgencia;
	}
	public String gettNumeroDaContaCorrente() {
		return tNumeroDaContaCorrente;
	}
	public void settNumeroDaContaCorrente(String tNumeroDaContaCorrente) {
		this.tNumeroDaContaCorrente = tNumeroDaContaCorrente;
	}
	public String gettDigitoVerificadorDaConta() {
		return tDigitoVerificadorDaConta;
	}
	public void settDigitoVerificadorDaConta(String tDigitoVerificadorDaConta) {
		this.tDigitoVerificadorDaConta = tDigitoVerificadorDaConta;
	}
	public String gettDigitoVerificadorDaAgenciaConta() {
		return tDigitoVerificadorDaAgenciaConta;
	}
	public void settDigitoVerificadorDaAgenciaConta(
			String tDigitoVerificadorDaAgenciaConta) {
		this.tDigitoVerificadorDaAgenciaConta = tDigitoVerificadorDaAgenciaConta;
	}
	public String gettUsoExclusivoCaixa() {
		return tUsoExclusivoCaixa;
	}
	public void settUsoExclusivoCaixa(String tUsoExclusivoCaixa) {
		this.tUsoExclusivoCaixa = tUsoExclusivoCaixa;
	}
	public String gettIdentificacaoDoTituloNoBanco() {
		return tIdentificacaoDoTituloNoBanco;
	}
	public void settIdentificacaoDoTituloNoBanco(
			String tIdentificacaoDoTituloNoBanco) {
		this.tIdentificacaoDoTituloNoBanco = tIdentificacaoDoTituloNoBanco;
	}
	public String gettCodigoDaCarteira() {
		return tCodigoDaCarteira;
	}
	public void settCodigoDaCarteira(String tCodigoDaCarteira) {
		this.tCodigoDaCarteira = tCodigoDaCarteira;
	}
	public String gettNumeroDoDocumentoDeCobranca() {
		return tNumeroDoDocumentoDeCobranca;
	}
	public void settNumeroDoDocumentoDeCobranca(String tNumeroDoDocumentoDeCobranca) {
		this.tNumeroDoDocumentoDeCobranca = tNumeroDoDocumentoDeCobranca;
	}
	public Date gettDataDoVencimentoDoTitulo() {
		return tDataDoVencimentoDoTitulo;
	}
	public void settDataDoVencimentoDoTitulo(Date tDataDoVencimentoDoTitulo) {
		this.tDataDoVencimentoDoTitulo = tDataDoVencimentoDoTitulo;
	}
	public String gettValorNominalDoTitulo() {
		return tValorNominalDoTitulo;
	}
	public void settValorNominalDoTitulo(String tValorNominalDoTitulo) {
		this.tValorNominalDoTitulo = tValorNominalDoTitulo;
	}
	public String gettUfEntidadeCedente() {
		return tUfEntidadeCedente;
	}
	public void settUfEntidadeCedente(String tUfEntidadeCedente) {
		this.tUfEntidadeCedente = tUfEntidadeCedente;
	}
	public String gettBranco() {
		return tBranco;
	}
	public void settBranco(String tBranco) {
		this.tBranco = tBranco;
	}
	public String gettCodigoDaEntidadeCedente() {
		return tCodigoDaEntidadeCedente;
	}
	public void settCodigoDaEntidadeCedente(String tCodigoDaEntidadeCedente) {
		this.tCodigoDaEntidadeCedente = tCodigoDaEntidadeCedente;
	}
	public String gettDigitoDaentidadeCedente() {
		return tDigitoDaentidadeCedente;
	}
	public void settDigitoDaentidadeCedente(String tDigitoDaentidadeCedente) {
		this.tDigitoDaentidadeCedente = tDigitoDaentidadeCedente;
	}
	public String gettNomeDaEntidadeCedente() {
		return tNomeDaEntidadeCedente;
	}
	public void settNomeDaEntidadeCedente(String tNomeDaEntidadeCedente) {
		this.tNomeDaEntidadeCedente = tNomeDaEntidadeCedente;
	}
	public String gettCodigoDaMoeda() {
		return tCodigoDaMoeda;
	}
	public void settCodigoDaMoeda(String tCodigoDaMoeda) {
		this.tCodigoDaMoeda = tCodigoDaMoeda;
	}
	public String gettTipoDeInscricao() {
		return tTipoDeInscricao;
	}
	public void settTipoDeInscricao(String tTipoDeInscricao) {
		this.tTipoDeInscricao = tTipoDeInscricao;
	}
	public String gettNumeroDeInscricao() {
		return tNumeroDeInscricao;
	}
	public void settNumeroDeInscricao(String tNumeroDeInscricao) {
		this.tNumeroDeInscricao = tNumeroDeInscricao;
	}
	public String gettNome() {
		return tNome;
	}
	public void settNome(String tNome) {
		this.tNome = tNome;
	}
	public String gettUsoExclusivoFebrabanCenab283T() {
		return tUsoExclusivoFebrabanCenab283T;
	}
	public void settUsoExclusivoFebrabanCenab283T(
			String tUsoExclusivoFebrabanCenab283T) {
		this.tUsoExclusivoFebrabanCenab283T = tUsoExclusivoFebrabanCenab283T;
	}
	public Double gettValorDaTarifaCustas() {
		return tValorDaTarifaCustas;
	}
	public void settValorDaTarifaCustas(Double tValorDaTarifaCustas) {
		this.tValorDaTarifaCustas = tValorDaTarifaCustas;
	}
	public String gettIdentificacaoParaRejeicoes() {
		return tIdentificacaoParaRejeicoes;
	}
	public void settIdentificacaoParaRejeicoes(String tIdentificacaoParaRejeicoes) {
		this.tIdentificacaoParaRejeicoes = tIdentificacaoParaRejeicoes;
	}
	public String gettUsoExclusivoFebrabanCenab313T() {
		return tUsoExclusivoFebrabanCenab313T;
	}
	public void settUsoExclusivoFebrabanCenab313T(
			String tUsoExclusivoFebrabanCenab313T) {
		this.tUsoExclusivoFebrabanCenab313T = tUsoExclusivoFebrabanCenab313T;
	}
	public String getuCodigoBancoCompensacao() {
		return uCodigoBancoCompensacao;
	}
	public void setuCodigoBancoCompensacao(String uCodigoBancoCompensacao) {
		this.uCodigoBancoCompensacao = uCodigoBancoCompensacao;
	}
	public String getuLoteServico() {
		return uLoteServico;
	}
	public void setuLoteServico(String uLoteServico) {
		this.uLoteServico = uLoteServico;
	}
	public String getuRegistroDetalhe() {
		return uRegistroDetalhe;
	}
	public void setuRegistroDetalhe(String uRegistroDetalhe) {
		this.uRegistroDetalhe = uRegistroDetalhe;
	}
	public String getuNumeroSequencialRegistroNoLote() {
		return uNumeroSequencialRegistroNoLote;
	}
	public void setuNumeroSequencialRegistroNoLote(
			String uNumeroSequencialRegistroNoLote) {
		this.uNumeroSequencialRegistroNoLote = uNumeroSequencialRegistroNoLote;
	}
	public String getuCodigoSegmentoDoRegistroDetalhe() {
		return uCodigoSegmentoDoRegistroDetalhe;
	}
	public void setuCodigoSegmentoDoRegistroDetalhe(
			String uCodigoSegmentoDoRegistroDetalhe) {
		this.uCodigoSegmentoDoRegistroDetalhe = uCodigoSegmentoDoRegistroDetalhe;
	}
	public String getuUsoExclusivoFebrabanCenab063U() {
		return uUsoExclusivoFebrabanCenab063U;
	}
	public void setuUsoExclusivoFebrabanCenab063U(
			String uUsoExclusivoFebrabanCenab063U) {
		this.uUsoExclusivoFebrabanCenab063U = uUsoExclusivoFebrabanCenab063U;
	}
	public String getUcodigoDeMovimento() {
		return ucodigoDeMovimento;
	}
	public void setUcodigoDeMovimento(String ucodigoDeMovimento) {
		this.ucodigoDeMovimento = ucodigoDeMovimento;
	}
	public Double getuJurosMultasEncargos() {
		return uJurosMultasEncargos;
	}
	public void setuJurosMultasEncargos(Double uJurosMultasEncargos) {
		this.uJurosMultasEncargos = uJurosMultasEncargos;
	}
	public Double getuValorDoDescontoConcedido() {
		return uValorDoDescontoConcedido;
	}
	public void setuValorDoDescontoConcedido(Double uValorDoDescontoConcedido) {
		this.uValorDoDescontoConcedido = uValorDoDescontoConcedido;
	}
	public Double getuValorDoAbatimentoConcedidoOuCancelado() {
		return uValorDoAbatimentoConcedidoOuCancelado;
	}
	public void setuValorDoAbatimentoConcedidoOuCancelado(
			Double uValorDoAbatimentoConcedidoOuCancelado) {
		this.uValorDoAbatimentoConcedidoOuCancelado = uValorDoAbatimentoConcedidoOuCancelado;
	}
	public Double getuValorDoIOFRecolhido() {
		return uValorDoIOFRecolhido;
	}
	public void setuValorDoIOFRecolhido(Double uValorDoIOFRecolhido) {
		this.uValorDoIOFRecolhido = uValorDoIOFRecolhido;
	}
	public Double getuValorPagoPeloSacado() {
		return uValorPagoPeloSacado;
	}
	public void setuValorPagoPeloSacado(Double uValorPagoPeloSacado) {
		this.uValorPagoPeloSacado = uValorPagoPeloSacado;
	}
	public Double getuValorDeCreditoBruto() {
		return uValorDeCreditoBruto;
	}
	public void setuValorDeCreditoBruto(Double uValorDeCreditoBruto) {
		this.uValorDeCreditoBruto = uValorDeCreditoBruto;
	}
	public Double getuValorDeOutrasDespesas() {
		return uValorDeOutrasDespesas;
	}
	public void setuValorDeOutrasDespesas(Double uValorDeOutrasDespesas) {
		this.uValorDeOutrasDespesas = uValorDeOutrasDespesas;
	}
	public String getuPercentualCreditoEntFinanceira() {
		return uPercentualCreditoEntFinanceira;
	}
	public void setuPercentualCreditoEntFinanceira(
			String uPercentualCreditoEntFinanceira) {
		this.uPercentualCreditoEntFinanceira = uPercentualCreditoEntFinanceira;
	}
	public String getuBrancos() {
		return uBrancos;
	}
	public void setuBrancos(String uBrancos) {
		this.uBrancos = uBrancos;
	}
	public Date getuDataDaOcorrencia() {
		return uDataDaOcorrencia;
	}
	public void setuDataDaOcorrencia(Date uDataDaOcorrencia) {
		this.uDataDaOcorrencia = uDataDaOcorrencia;
	}
	public Date getuDataDaEfetivacaoDoCredito() {
		return uDataDaEfetivacaoDoCredito;
	}
	public void setuDataDaEfetivacaoDoCredito(Date uDataDaEfetivacaoDoCredito) {
		this.uDataDaEfetivacaoDoCredito = uDataDaEfetivacaoDoCredito;
	}
	public String getuUsoExclusivoFenabranCNAB193U() {
		return uUsoExclusivoFenabranCNAB193U;
	}
	public void setuUsoExclusivoFenabranCNAB193U(
			String uUsoExclusivoFenabranCNAB193U) {
		this.uUsoExclusivoFenabranCNAB193U = uUsoExclusivoFenabranCNAB193U;
	}
	public String gettIdentificacaoDotitiloNoBancoDigito() {
		return tIdentificacaoDotitiloNoBancoDigito;
	}
	public void settIdentificacaoDotitiloNoBancoDigito(
			String tIdentificacaoDotitiloNoBancoDigito) {
		this.tIdentificacaoDotitiloNoBancoDigito = tIdentificacaoDotitiloNoBancoDigito;
	}
	
	
	private static final double LATITUDE_SEMACE = -3.74213;

	private static final double LONGITUDE_SEMACE = -38.5297;
	public static void main(String[] args) {
		Double distanceKm = null;
		try {
			String endereco = "http://maps.google.com/maps/api/directions/xml?origin=" + LATITUDE_SEMACE + "," + LONGITUDE_SEMACE + "&destination=" + -3.779719 + "," + -38.499227 + "&sensor=false";
			URL url = new URL(endereco);
			
			DocumentBuilderFactory xmlFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = xmlFactory.newDocumentBuilder();
			
			XPathFactory xpathFact = XPathFactory.newInstance();
			XPath xpath = xpathFact.newXPath();

			Document xmlDoc = docBuilder.parse(url.openStream());

			String distanceMetros = (String) xpath.evaluate("/DirectionsResponse/route/leg/distance/value", xmlDoc, XPathConstants.STRING);
			
			Double metros = new Double(distanceMetros);
			
			distanceKm = metros/1000;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	System.out.println(distanceKm);
	}
	
	
	
	
	
	
	

}
