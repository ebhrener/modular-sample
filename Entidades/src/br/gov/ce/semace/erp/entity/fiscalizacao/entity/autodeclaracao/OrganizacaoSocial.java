package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio.AutoDeclaracaoOrganizacaoSocial;

@Entity
@Table(schema = "servicos", name="organizacao_social")
public class OrganizacaoSocial {
	
	@Id
	@SequenceGenerator(sequenceName = "servicos.seq_organizacao_social", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;
	private Integer codigoOrganizacaoSocial;
	
	@ManyToOne
	private AutoDeclaracao autoDeclaracao;
	
	@Transient
	private String descricao;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCodigoOrganizacaoSocial() {
		return codigoOrganizacaoSocial;
	}

	public void setCodigoOrganizacaoSocial(Integer codigoOrganizacaoSocial) {
		this.codigoOrganizacaoSocial = codigoOrganizacaoSocial;
	}

	public AutoDeclaracao getAutoDeclaracao() {
		return autoDeclaracao;
	}

	public void setAutoDeclaracao(AutoDeclaracao autoDeclaracao) {
		this.autoDeclaracao = autoDeclaracao;
	}

	public String getDescricao() {
		descricao = AutoDeclaracaoOrganizacaoSocial.showDescricaoByCodigo(getCodigoOrganizacaoSocial());
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
