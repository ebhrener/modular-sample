package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "view_documento_nao_assinado", schema = "geral")
public class DocumentoNaoAssinado implements Serializable {

	private static final long serialVersionUID = 6313013323462463791L;

	public static final String COLUMN_ARQUIVO_DOCUMENTO_ID = "arquivo_documento_id";

	@Id
	@Column(name = "arquivo_documento_id")
	private Long id;

	@Column(name = "documento_id_natuur")
	private Long documentoIdNatuur;

	@Column(name = "documento_id_siga")
	private Long documentoIdSiga;

	@Column(name = "ged_id")
	private Long gedId;

	@Column(name = "arquivo_temporario_id")
	private Long arquivoTemporarioId;

	@Column(name = "numero_spu")
	private String spu;

	@Column(name = "nome_arquivo")
	private String nomeArquivo;

	@Column(name = "tipo_documento_id")
	private Long tipoDocumentoId;

	@Column(name = "sigla")
	private String siglaTipoDocumento;

	@Column(name = "descricao")
	private String descricaoTipoDocumento;

	@Column(name = "pessoa_id")
	private Long pessoaId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDocumentoIdNatuur() {
		return documentoIdNatuur;
	}

	public void setDocumentoIdNatuur(Long documentoIdNatuur) {
		this.documentoIdNatuur = documentoIdNatuur;
	}

	public Long getDocumentoIdSiga() {
		return documentoIdSiga;
	}

	public void setDocumentoIdSiga(Long documentoIdSiga) {
		this.documentoIdSiga = documentoIdSiga;
	}

	public Long getGedId() {
		return gedId;
	}

	public void setGedId(Long gedId) {
		this.gedId = gedId;
	}

	public Long getArquivoTemporarioId() {
		return arquivoTemporarioId;
	}

	public void setArquivoTemporarioId(Long arquivoTemporarioId) {
		this.arquivoTemporarioId = arquivoTemporarioId;
	}

	public String getSpu() {
		return spu;
	}

	public void setSpu(String spu) {
		this.spu = spu;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public Long getTipoDocumentoId() {
		return tipoDocumentoId;
	}

	public void setTipoDocumentoId(Long tipoDocumentoId) {
		this.tipoDocumentoId = tipoDocumentoId;
	}

	public String getSiglaTipoDocumento() {
		return siglaTipoDocumento;
	}

	public void setSiglaTipoDocumento(String siglaTipoDocumento) {
		this.siglaTipoDocumento = siglaTipoDocumento;
	}

	public String getDescricaoTipoDocumento() {
		return descricaoTipoDocumento;
	}

	public void setDescricaoTipoDocumento(String descricaoTipoDocumento) {
		this.descricaoTipoDocumento = descricaoTipoDocumento;
	}

	public Long getPessoaId() {
		return pessoaId;
	}

	public void setPessoaId(Long pessoaId) {
		this.pessoaId = pessoaId;
	}

}
