package br.gov.ce.semace.erp.entity.seguranca;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import br.gov.ce.semace.erp.enuns.TipoUsuarioWeb;

@Entity
@Audited
@Table(name = "usuario_web", schema = "seguranca", uniqueConstraints = {@UniqueConstraint(columnNames="login"), @UniqueConstraint(columnNames="email")})
public class UsuarioWeb implements IUsuario {

	private static final long serialVersionUID = -1183834844037635593L;

	@Id
	@SequenceGenerator(sequenceName = "seguranca.seq_usuario_web", name = "SEQUSER", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUSER")
	private Long id;

	@NotNull(message="Campo Nome não pode ser nulo")
	@Column(name="nome_usuario", nullable=false)
	private String nomeUsuario;

	@Column(nullable = false)
	@NotNull(message="Campo Login não pode ser nulo")
	private String login;


	@Column(nullable = false)
	@NotNull(message="Campo Senha não pode ser nulo")
	@NotEmpty(message="Campo Senha não pode ser vazio")
	private String senha;

	@NotNull(message="Campo Email não pode ser nulo")
	@Column(nullable=false)
	private String email;

	@Column(name = "tipo_usuario_web")
	private TipoUsuarioWeb tipoUsuarioWeb;

	private Boolean liberado = false;

	@Column(name = "liberar_regra")
	private Boolean liberarRegra = false;

	@Transient
	private Boolean emailUnico = true;

	@NotAudited
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(schema="seguranca", name = "usuario_web_solicitacao_nova_senha", joinColumns = @JoinColumn(name = "usuario_web_id"))
	public Set<SolicitacaoNovaSenha> listSolicitacaoNovaSenha = new HashSet<>();

	@Transient
	private String tipo = "EXTERNO";

	public Boolean getLiberado() {
		return liberado;
	}

	public void setLiberado(Boolean liberado) {
		this.liberado = liberado;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	@Override
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public TipoUsuarioWeb getTipoUsuarioWeb() {
		return tipoUsuarioWeb;
	}

	public void setTipoUsuarioWeb(TipoUsuarioWeb tipoUsuarioWeb) {
		this.tipoUsuarioWeb = tipoUsuarioWeb;
	}

	public Boolean getEmailUnico() {
		return emailUnico;
	}

	public void setEmailUnico(Boolean emailUnico) {
		this.emailUnico = emailUnico;
	}

	public Boolean getLiberarRegra() {
		return liberarRegra;
	}

	public void setLiberarRegra(Boolean liberarRegra) {
		this.liberarRegra = liberarRegra;
	}

	@Transient
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * O retorno desse método é utilizado para identificar o tipo de usuário na engine de auditoria
	 * @return
	 */
	@Override
	public String toString() {
		return tipo;
	}

	public Set<SolicitacaoNovaSenha> getListSolicitacaoNovaSenha() {
		return listSolicitacaoNovaSenha;
	}

	public void setListSolicitacaoNovaSenha(Set<SolicitacaoNovaSenha> listSolicitacaoNovaSenha) {
		this.listSolicitacaoNovaSenha = listSolicitacaoNovaSenha;
	}
}