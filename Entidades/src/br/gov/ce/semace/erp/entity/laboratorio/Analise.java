package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Documento;
import br.gov.ce.semace.erp.entity.rh.Funcionario;


/** 
 * Entidade de Mapeamento da tabela 'analise'.
 * 
 * @author Joerlan Oliveira Lima - joerlan.lima@semace.ce.gov.br - 17/10/2013
 *
 */
@Entity
@Audited
@Table(name="analise", schema="laboratorio")
public class Analise implements Serializable {

	private static final long serialVersionUID = 4284099140840624886L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_analise", name="seq_analise", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_analise")
	private Long id;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Date dataCadastro;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_finalizacao")
	private Date dataFinalizacao;
	
	@OneToOne(fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="documento_id")
	private Documento documento = new Documento();
	
	@OneToOne(optional=false)
	@JoinColumn(name="ordem_coleta_id")
	private OrdemColeta ordemColeta = new OrdemColeta();
	
	@OneToMany(mappedBy="analise",fetch=FetchType.LAZY, cascade=CascadeType.PERSIST)
	private List<ResultadoAnalise> resultados = new ArrayList<>();
	
	@Transient private Funcionario funcionario = new Funcionario();
	@Transient private Date dataFinalizacaoInicial;
	@Transient private Date dataFinalizacaoFinal;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public OrdemColeta getOrdemColeta() {
		return ordemColeta;
	}

	public void setOrdemColeta(OrdemColeta ordemColeta) {
		this.ordemColeta = ordemColeta;
	}

	public List<ResultadoAnalise> getResultados() {
		return resultados;
	}

	public void setResultados(List<ResultadoAnalise> resultados) {
		this.resultados = resultados;
	}
	
	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataFinalizacao() {
		return dataFinalizacao;
	}

	public void setDataFinalizacao(Date dataFinalizacao) {
		this.dataFinalizacao = dataFinalizacao;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Date getDataFinalizacaoInicial() {
		return dataFinalizacaoInicial;
	}

	public void setDataFinalizacaoInicial(Date dataFinalizacaoInicial) {
		this.dataFinalizacaoInicial = dataFinalizacaoInicial;
	}

	public Date getDataFinalizacaoFinal() {
		return dataFinalizacaoFinal;
	}

	public void setDataFinalizacaoFinal(Date dataFinalizacaoFinal) {
		this.dataFinalizacaoFinal = dataFinalizacaoFinal;
	}
	
}
