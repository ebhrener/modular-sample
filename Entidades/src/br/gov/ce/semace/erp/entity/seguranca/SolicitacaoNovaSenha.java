package br.gov.ce.semace.erp.entity.seguranca;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class SolicitacaoNovaSenha implements Serializable {

	private static final long serialVersionUID = 8259760611349706515L;

	@NotNull
	@Column(nullable=false)
	private Boolean confirmado = Boolean.FALSE;

	@NotNull
	@Column(nullable=false)
	private Boolean rejeitado = Boolean.FALSE;

	@Column(nullable=false)
	private String hash;

	@NotNull
	@Column(name="data_solicitacao", nullable=false)
	private Date dataSolicitacao;

	@Column(name="data_confirmacao")
	private Date dataConfirmacao;

	@Column(name="data_rejeicao")
	private Date dataRejeicao;

	public SolicitacaoNovaSenha() {
		super();
	}

	public SolicitacaoNovaSenha(String hash, Date dataSolicitacao) {
		super();
		this.hash = hash;
		this.dataSolicitacao = dataSolicitacao;
	}

	public void rejeitarSolicitacao() {
		if(!this.rejeitado && !this.confirmado) {
			this.rejeitado = Boolean.TRUE;
			this.dataRejeicao = new Date();
		}
	}

	public void confirmarSolicitacao() {
		if(!this.rejeitado && !this.confirmado) {
			this.confirmado = Boolean.TRUE;
			this.dataConfirmacao = new Date();
		}
	}

	public Boolean isConfirmado() {
		return confirmado;
	}

	public void setConfirmado(Boolean confirmado) {
		this.confirmado = confirmado;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Date getDataSolicitacao() {
		return dataSolicitacao;
	}

	public void setDataSolicitacao(Date dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	public Date getDataConfirmacao() {
		return dataConfirmacao;
	}

	public void setDataConfirmacao(Date dataConfirmacao) {
		this.dataConfirmacao = dataConfirmacao;
	}

	public Boolean getRejeitado() {
		return rejeitado;
	}

	public void setRejeitado(Boolean rejeitado) {
		this.rejeitado = rejeitado;
	}

	public Date getDataRejeicao() {
		return dataRejeicao;
	}

	public void setDataRejeicao(Date dataRejeicao) {
		this.dataRejeicao = dataRejeicao;
	}

	public Boolean getConfirmado() {
		return confirmado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (confirmado ? 1231 : 1237);
		result = prime * result
				+ ((dataSolicitacao == null) ? 0 : dataSolicitacao.hashCode());
		result = prime * result + ((hash == null) ? 0 : hash.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SolicitacaoNovaSenha other = (SolicitacaoNovaSenha) obj;
		if (confirmado != other.confirmado) {
			return false;
		}
		if (dataSolicitacao == null) {
			if (other.dataSolicitacao != null) {
				return false;
			}
		} else if (!dataSolicitacao.equals(other.dataSolicitacao)) {
			return false;
		}
		if (hash == null) {
			if (other.hash != null) {
				return false;
			}
		} else if (!hash.equals(other.hash)) {
			return false;
		}
		return true;
	}

}