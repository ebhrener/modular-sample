package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(schema = "financeiro", name = "tipo_receita")
public class TipoReceita implements Serializable {

	private static final long serialVersionUID = -7451253144549510193L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_tipo_receita", name = "SEQ_TIPO_RECEITA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TIPO_RECEITA")
	private Long id;

	private Integer codigo;

	private String descricao;

	private Boolean ativo;

	@Transient
	public String getAtivoFormatado(){
		if (ativo != null) {
			if (ativo == true) {
				return "Sim";
			}
			return "Não";
		}

		return "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Transient
	public String getDescricao(int quantidadeCaracteres) {
		if (this.descricao.length() < quantidadeCaracteres) {
			return this.descricao;
		}
		return descricao.substring(0, quantidadeCaracteres) + " ...";
	}
}