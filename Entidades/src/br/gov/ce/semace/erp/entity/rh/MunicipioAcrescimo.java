package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Sede;
import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;

@Entity
@Audited
@Table(schema = "rh", name = "municipio_acrescimo", uniqueConstraints=@UniqueConstraint(columnNames="municipio_id"))
public class MunicipioAcrescimo implements Serializable {

	private static final long serialVersionUID = -1883829109342615706L;

	@Id
	@SequenceGenerator(sequenceName = "rh.seq_municipio_acrescimo", name = "SEQ_MUNICIPIO_ACRESCIMO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MUNICIPIO_ACRESCIMO")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="municipio_id", unique = true)
	private Cidade municipio = new Cidade();

	private BigDecimal acrescimo;

	@Column(name="ativo")
	private boolean ativo = true;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "sede_id", unique = true)
	private Sede sede = new Sede();
	//================================== GETERS and SETERS ====================================//

	@Transient
	public String getDetalheAtivo(){

		if (this.ativo){
			return "Sim";
		}

		return "Não";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cidade getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Cidade municipio) {
		this.municipio = municipio;
	}

	public BigDecimal getAcrescimo() {
		return acrescimo;
	}

	public void setAcrescimo(BigDecimal acrescimo) {
		this.acrescimo = acrescimo;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

}
