package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;
import br.gov.ce.semace.erp.enuns.TipoCoordenada;


@Entity
@Audited
@Table(name="empreendimento", schema="geral")
public class Empreendimento implements Serializable {

	private static final long serialVersionUID = -111440950848219937L;

	/**
	 * Mensagem genérica informando que onde o campo for nulo ou vazio, esta mensagem será
	 * exibida nos detalhes ou em uma tabela.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/12/2013 18:27:33
	 */
	private static final String MSG_NAO_INFORMADO = "Não Informado";

	@Id
	@SequenceGenerator(sequenceName="geral.seq_empreendimento", name="SEQEMPR", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQEMPR")
	private Long id;

	@Column(columnDefinition="text")
	private String descricao;

	@Column(name="tipo_coordenada")
	private TipoCoordenada tipoCoordenada = TipoCoordenada.GEOGRAFICA;

	@Deprecated
	private String coordenada1;

	@Deprecated
	private String coordenada2;

	@Column(precision=15, scale=2)
	private BigDecimal area;

	@Column(name="qtd_funcionario")
	private Integer qtdFuncionario;

	@NotNull
	@Column(columnDefinition="text", nullable=false)
	private String localizacao;

	@NotNull
	@Column(columnDefinition="text", nullable=false)
	private String referencia;

	private String bairro;

	private String cep;

	private String matricula;

	@Column(name="nome_empreendimento", nullable=false)
	@NotNull
	private String nomeEmpreendimento;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="unidade_medida_id" , referencedColumnName="id")
	private UnidadeMedida unidadeMedida = new UnidadeMedida();

	@OneToMany(fetch=FetchType.LAZY, mappedBy = "empreendimento", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<EmpreendimentoAtividade> atividades = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="interessado_id")
	private Interessado interessado = new Interessado();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cidade_id")
	private Cidade cidade = new Cidade();

	private Boolean excluido = false;

	@Column(name="espaco_geografico")
	private EspacoGeografico espacoGeografico;

	private String latitude;

	private String longitude;

	@Transient
	private String coordUTMmN;

	@Transient
	private String coordUTMmE;

	/**
	 * Retorna informação a ser exibida em uma tabela ou detalhes.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/12/2013 18:28:13
	 *
	 * @see Empreendimento#MSG_NAO_INFORMADO
	 *
	 * @return
	 * 		Retorna {@value #MSG_NAO_INFORMADO} caso o campo for nulo ou vazio. Caso contrário, retorna o valor do campo
	 * {@link #coordenada1}.
	 */
	@Transient
	public String getDetalheCoordenada1(){

		if(this.coordenada1 == null || this.coordenada1.trim().isEmpty()) {

			return MSG_NAO_INFORMADO;
		}

		return this.coordenada1;
	}

	/**
	 * Retorna informação a ser exibida em uma tabela ou detalhes.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/12/2013 18:30:46
	 *
	 * @see Empreendimento#MSG_NAO_INFORMADO
	 *
	 * @return
	 * 		Retorna {@value #MSG_NAO_INFORMADO} caso o campo for nulo ou vazio. Caso contrário, retorna o valor do campo
	 * {@link #coordenada2}.
	 */
	@Transient
	public String getDetalheCoordenada2(){

		if(this.coordenada2 == null || this.coordenada2.trim().isEmpty()) {

			return MSG_NAO_INFORMADO;
		}

		return this.coordenada2;
	}

	/**
	 * Retorna informação a ser exibida em uma tabela ou detalhes.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/12/2013 18:30:56
	 *
	 * @see Empreendimento#MSG_NAO_INFORMADO
	 *
	 * @return
	 * 		Retorna {@value #MSG_NAO_INFORMADO} caso o campo for nulo ou vazio. Caso contrário, retorna o valor do campo
	 * {@link #qtdFuncionario}.
	 */
	@Transient
	public String getDetalheQtdFuncionario(){

		if(this.qtdFuncionario == null) {

			return MSG_NAO_INFORMADO;
		}

		return this.qtdFuncionario.toString();
	}

	/**
	 * Retorna informação a ser exibida em uma tabela ou detalhes.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/12/2013 18:31:05
	 *
	 * @see Empreendimento#MSG_NAO_INFORMADO
	 *
	 * @return
	 * 		Retorna {@value #MSG_NAO_INFORMADO} caso o campo for nulo ou vazio. Caso contrário, retorna o valor do campo
	 * {@link #matricula}.
	 */
	@Transient
	public String getDetalheMatricula(){

		if(this.matricula == null || this.matricula.trim().isEmpty()) {

			return MSG_NAO_INFORMADO;
		}

		return this.matricula;
	}

	/**
	 * Retorna informação a ser exibida em uma tabela ou detalhes.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/12/2013 18:32:26
	 *
	 * @see Empreendimento#MSG_NAO_INFORMADO
	 *
	 * @return
	 * 		Retorna {@value #MSG_NAO_INFORMADO} caso o campo for nulo ou vazio. Caso contrário, retorna o valor do campo
	 * {@link #descricao}.
	 */
	@Transient
	public String getDetalheDescricao(){

		if(this.descricao == null || this.descricao.trim().isEmpty()) {

			return MSG_NAO_INFORMADO;
		}

		return this.descricao;
	}

	/**
	 * Retorna informação a ser exibida em uma tabela ou detalhes.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/12/2013 18:32:34
	 *
	 * @see Empreendimento#MSG_NAO_INFORMADO
	 *
	 * @return
	 * 		Retorna {@value #MSG_NAO_INFORMADO} caso o campo for nulo ou vazio. Caso contrário, retorna o valor do campo
	 * {@link #area}.
	 */
	@Transient
	public String getDetalheArea(){

		if(this.area == null) {

			return MSG_NAO_INFORMADO;
		}

		return this.area.toString();
	}

	/**
	 * Retorna informação a ser exibida em uma tabela ou detalhes.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/12/2013 18:32:51
	 *
	 * @see Empreendimento#MSG_NAO_INFORMADO
	 *
	 * @return
	 * 		Retorna {@value #MSG_NAO_INFORMADO} caso o campo for nulo ou vazio. Caso contrário, retorna o valor
	 * {@link UnidadeMedida#setSigla(String)} - {@link UnidadeMedida#getDescricao()}
	 */
	@Transient
	public String getDetalheUnidadeMedida(){

		if( this.unidadeMedida == null ||
			(this.unidadeMedida.getSigla() == null || this.unidadeMedida.getSigla().trim().isEmpty() || this.unidadeMedida.getDescricao() == null || this.unidadeMedida.getDescricao().trim().isEmpty())){
			return MSG_NAO_INFORMADO;
		}

		return this.unidadeMedida.getSigla() + " - " + this.unidadeMedida.getDescricao();
	}

	public EspacoGeografico getEspacoGeografico() {
		return espacoGeografico;
	}

	public void setEspacoGeografico(EspacoGeografico espacoGeografico) {
		this.espacoGeografico = espacoGeografico;
	}

	public BigDecimal getArea() {
		return area;
	}

	public void setArea(BigDecimal area) {
		this.area = area;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoCoordenada getTipoCoordenada() {
		return tipoCoordenada;
	}

	public void setTipoCoordenada(TipoCoordenada tipoCoordenada) {
		this.tipoCoordenada = tipoCoordenada;
	}

	@Deprecated
	public String getCoordenada1() {
		return coordenada1;
	}

	@Deprecated
	public void setCoordenada1(String coordenada1) {
		this.coordenada1 = coordenada1;
	}

	@Deprecated
	public String getCoordenada2() {
		return coordenada2;
	}

	@Deprecated
	public void setCoordenada2(String coordenada2) {
		this.coordenada2 = coordenada2;
	}

	public Integer getQtdFuncionario() {
		return qtdFuncionario;
	}

	public void setQtdFuncionario(Integer qtdFuncionario) {
		this.qtdFuncionario = qtdFuncionario;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNomeEmpreendimento() {
		return nomeEmpreendimento;
	}

	public void setNomeEmpreendimento(String nomeEmpreendimento) {
		this.nomeEmpreendimento = nomeEmpreendimento;
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public Interessado getInteressado() {
		return interessado;
	}

	public void setInteressado(Interessado interessado) {
		this.interessado = interessado;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public List<EmpreendimentoAtividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<EmpreendimentoAtividade> atividades) {
		this.atividades = atividades;
	}

	public Boolean getExcluido() {
		return excluido;
	}

	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getCoordUTMmN() {
		return coordUTMmN;
	}

	public void setCoordUTMmN(String coordUTMmN) {
		this.coordUTMmN = coordUTMmN;
	}

	public String getCoordUTMmE() {
		return coordUTMmE;
	}

	public void setCoordUTMmE(String coordUTMmE) {
		this.coordUTMmE = coordUTMmE;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Empreendimento other = (Empreendimento) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/**
	 * Mover esta regra para camada de negócio e não no entidades
	 */
//	@Transient
//	public String getDetalheLatitude() {
//		if(this.latitude != null && !this.latitude.isEmpty()) {
//			if (this.tipoCoordenada.isUTM()) {
//				this.parseLatLngToUTM();
//				return this.coordUTMmN + " mN";
//			}
//			return this.latitude;
//		}
//
//		return "Não Informado";
//	}
//
//	@Transient
//	public String getDetalheLongitude() {
//		if(this.longitude != null && !this.longitude.isEmpty()) {
//			if (this.tipoCoordenada.isUTM()) {
//				this.parseLatLngToUTM();
//				return this.coordUTMmE + " mE";
//			}
//			return this.longitude;
//		}
//
//		return "Não Informado";
//	}
//
//	public void parseLatLngToUTM() {
//		if (isNotBlank(latitude) && isNotBlank(longitude)) {
//			double lat = parseDouble(latitude);
//			double lng = parseDouble(longitude);
//			UTMPoint utmPoint = MapUtils.convertToUTMFromLatLong(lat, lng);
//			coordUTMmN = Double.toString(utmPoint.northing);
//			coordUTMmE = Double.toString(utmPoint.easting);
//		}
//	}
//
//	public void parseUTMToLatLng() {
//		if (isNotBlank(coordUTMmN) && isNotBlank(coordUTMmE)) {
//			double northing = parseDouble(coordUTMmN);
//			double easting = parseDouble(coordUTMmE);
//			LatLng latLng = MapUtils.getLatLngFromUTM(northing, easting);
//			latitude = Double.toString(latLng.getLat());
//			longitude = Double.toString(latLng.getLng());
//		}
//	}
}