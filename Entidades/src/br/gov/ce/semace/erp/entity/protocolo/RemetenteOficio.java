package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

/**
 * Entidade referente aos remetentes de ofício para monitoramento
 *
 * @author Victor Hugo R. Rodrigues - 19/12/2013 08:28:51
 * @version 1.0
 */
@Entity
@Audited
@Table(name="remetente_oficio", schema="protocolo")
public class RemetenteOficio implements Serializable{

	private static final long serialVersionUID = 4325916490656605658L;

	@Id
	@SequenceGenerator(sequenceName = "protocolo.seq_remetente_oficio", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@Column(name="descricao",nullable=false)
	private String descricao;

	@Column(name="ativo",nullable=false)
	private Boolean ativo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Informa a descrição da propriedade ativo, no caso, Sim ou Não, representando ativo ou inativo, respectivamente.
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - Apr 10, 2014 4:49:41 PM
	 *
	 * @return Retorna uma {@link String} com o valor <code>Sim</code>,
	 * 	caso o valor da propriedade ativo seja diferente de <code>null</code> e <code>true</code>. Caso Contrário, retorna <code>false</code>
	 */
	@Transient
	public String getDetalheAtivo(){

		if (this.ativo){
			return "Sim";
		}

		return "Não";
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RemetenteOficio other = (RemetenteOficio) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}