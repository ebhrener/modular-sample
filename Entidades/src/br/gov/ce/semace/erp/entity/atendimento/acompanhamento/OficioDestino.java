package br.gov.ce.semace.erp.entity.atendimento.acompanhamento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

public class OficioDestino implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "oficio_id")
	private Integer oficioId;

	@Column(name = "ano")
	private Integer ano;

	@Column(name = "destino_id")
	private Integer destinoId;

	@Column(name = "tratamento_oficio_id")
	private Integer tratamentoOficioId;

	@Column(name = "nome_destino")
	private String nomeDestino;

	@Column(name = "cargo_destino")
	private String cargoDestino;

	@Column(name = "orgao_destino")
	private String orgaoDestino;

	@Column(name = "endereco")
	private String endereco;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOficioId() {
		return oficioId;
	}

	public void setOficioId(Integer oficioId) {
		this.oficioId = oficioId;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getDestinoId() {
		return destinoId;
	}

	public void setDestinoId(Integer destinoId) {
		this.destinoId = destinoId;
	}

	public Integer getTratamentoOficioId() {
		return tratamentoOficioId;
	}

	public void setTratamentoOficioId(Integer tratamentoOficioId) {
		this.tratamentoOficioId = tratamentoOficioId;
	}

	public String getNomeDestino() {
		return nomeDestino;
	}

	public void setNomeDestino(String nomeDestino) {
		this.nomeDestino = nomeDestino;
	}

	public String getCargoDestino() {
		return cargoDestino;
	}

	public void setCargoDestino(String cargoDestino) {
		this.cargoDestino = cargoDestino;
	}

	public String getOrgaoDestino() {
		return orgaoDestino;
	}

	public void setOrgaoDestino(String orgaoDestino) {
		this.orgaoDestino = orgaoDestino;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

}
