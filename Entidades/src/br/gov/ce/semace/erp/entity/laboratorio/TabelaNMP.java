package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="tabela_nmp", schema="laboratorio")
public class TabelaNMP implements Serializable {

	private static final long serialVersionUID = 6605862022342781357L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_tabela_nmp", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;
	
	@Column(name = "positivo_1")
	private Integer positivo1;
	
	@Column(name = "positivo_2")
	private Integer positivo2;
	
	@Column(name = "positivo_3")
	private Integer positivo3;
	
	@Column(name = "indice_nmp")
	private Integer indiceNmp;
	
	@Column(name = "limite_inferior")
	private Float limiteInferior;
	
	@Column(name = "limite_superior")
	private Float limiteSuperior;
	
	private String modificador;
	
	private Boolean ativo = Boolean.TRUE;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPositivo1() {
		return positivo1;
	}

	public void setPositivo1(Integer positivo1) {
		this.positivo1 = positivo1;
	}

	public Integer getPositivo2() {
		return positivo2;
	}

	public void setPositivo2(Integer positivo2) {
		this.positivo2 = positivo2;
	}

	public Integer getPositivo3() {
		return positivo3;
	}

	public void setPositivo3(Integer positivo3) {
		this.positivo3 = positivo3;
	}

	public Integer getIndiceNmp() {
		return indiceNmp;
	}

	public void setIndiceNmp(Integer indiceNmp) {
		this.indiceNmp = indiceNmp;
	}

	public String getModificador() {
		return modificador;
	}

	public void setModificador(String modificador) {
		this.modificador = modificador;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Float getLimiteInferior() {
		return limiteInferior;
	}

	public void setLimiteInferior(Float limiteInferior) {
		this.limiteInferior = limiteInferior;
	}

	public Float getLimiteSuperior() {
		return limiteSuperior;
	}

	public void setLimiteSuperior(Float limiteSuperior) {
		this.limiteSuperior = limiteSuperior;
	}
	
}
