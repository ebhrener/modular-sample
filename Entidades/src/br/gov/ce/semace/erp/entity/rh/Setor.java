package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.geral.Sede;

/**
 * @author Marcus Mazzo Laprano [marcus.mazzo@gmail.com]
 *
 */
@Entity
@Audited
@Table(name="setor", schema="rh")
public class Setor implements Serializable, Comparable<Setor>{

	private static final long serialVersionUID = -7467337115428061638L;

	@Id
	@SequenceGenerator(sequenceName="rh.seq_setor", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	private String sigla;

	private String descricao;

	@Transient
	private String areaSigaID;

	//------------------------------------------------Relacionamentos---------------------------------------------

	public Setor(Long id) {
		super();
		this.id = id;
	}

	public Setor(Long id, String sigla, String descricao) {
		super();
		this.id = id;
		this.sigla = sigla;
		this.descricao = descricao;
	}

	public Setor() {
		super();
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setor_pai_id", insertable = true, updatable = true, nullable = true)
	private Setor setorPai;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="responsavel_id")
	private Funcionario responsavel;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="nivel_id")
	private NivelHierarquico nivelHierarquico = new NivelHierarquico();

	@ManyToMany(fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinTable(name = "setor_apoio_tecnico",	schema = "rh",
			joinColumns = @JoinColumn(name = "setor_id"),
			inverseJoinColumns = @JoinColumn(name = "apoio_tecnico_id"))
	private List<ApoioTecnico> listApoioTecnico = new ArrayList<ApoioTecnico>();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "sede_id")
	private Sede sede;

	@Transient
	public String getSiglaAndDescricao() {
		if (this.sigla != null) {
			return this.sigla + " - " + descricao;
		}

		return descricao;
	}

	/**
	 * Filtro utilizado para trazer na listagem apenas os registros que possuem responsável no SIGA.
	 */
	@Transient
	private Boolean onlyWithResponsavel;

	//------------------------------------------------Gets e Setters---------------------------------------------


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}


	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Setor getSetorPai() {
		return setorPai;
	}


	public void setSetorPai(Setor setorPai) {
		this.setorPai = setorPai;
	}

	public Funcionario getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Funcionario responsavel) {
		this.responsavel = responsavel;
	}


	public List<ApoioTecnico> getListApoioTecnico() {
		return listApoioTecnico;
	}


	public void setListApoioTecnico(List<ApoioTecnico> listApoioTecnico) {
		this.listApoioTecnico = listApoioTecnico;
	}


	public NivelHierarquico getNivelHierarquico() {
		return nivelHierarquico;
	}


	public void setNivelHierarquico(NivelHierarquico nivelHierarquico) {
		this.nivelHierarquico = nivelHierarquico;
	}


	public String getSigla() {
		return sigla;
	}


	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

	public String getAreaSigaID() {
		return areaSigaID;
	}

	public void setAreaSigaID(String areaSigaID) {
		this.areaSigaID = areaSigaID;
	}

	@Override
	public int compareTo(Setor setor) {
		if (setor != null && setor.getSigla() != null && !setor.getSigla().isEmpty()){
			if(this.getSigla() != null) {
				return this.getSigla().compareTo(setor.getSigla());
			}
		} else if(setor != null && setor.getDescricao() != null && !setor.getDescricao().isEmpty()) {
			if(this.getDescricao() != null) {
				return this.getDescricao().compareTo(setor.getDescricao());
			}
		}
		return 0;

	}

		@Override
		public boolean equals(Object obj) {
			if(obj != null && obj instanceof Setor){
				Setor other = (Setor) obj;
				if(id.equals(other.getId())){
					return true;
				}
				if(!sigla.isEmpty() && other.getSigla() != null && sigla.equals(other.getSigla())){
					return true;
				}
				if(!descricao.isEmpty() && other.getDescricao() != null && descricao.equals(other.getDescricao())){
					return true;
				}
			}
			return super.equals(obj);
		}

	public Boolean getOnlyWithResponsavel() {
		return onlyWithResponsavel;
	}

	public void setOnlyWithResponsavel(Boolean onlyWithResponsavel) {
		this.onlyWithResponsavel = onlyWithResponsavel;
	}


}
