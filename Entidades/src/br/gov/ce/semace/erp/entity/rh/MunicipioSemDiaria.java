package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import br.gov.ce.semace.erp.entity.geral.Sede;
import br.gov.ce.semace.erp.entity.geral.entity.cep.Cidade;

/**
 * Entidade que representa os municipios que não recebem diária
 * 
 * @author fredericomota
 *
 */
@Entity
@Table(schema = "rh", name = "municipio_sem_diaria", uniqueConstraints=@UniqueConstraint(columnNames="municipio_id"))
public class MunicipioSemDiaria implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4904724377107878960L;
	
	@Id
	@SequenceGenerator(sequenceName = "rh.seq_municipio_sem_diaria", name = "SEQ_MUNICIPIO_SEM_DIARIA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MUNICIPIO_SEM_DIARIA")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="municipio_id", unique = true)
	private Cidade municipio = new Cidade();
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="sede_id", unique = true)
	private Sede sede;
	
	@Column(name="ativo")
	private boolean ativo = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cidade getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Cidade municipio) {
		this.municipio = municipio;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}
	
	@Transient
	public String getDetalheAtivo(){

		if (this.ativo){
			return "Sim";
		}

		return "Não";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (ativo ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((municipio == null) ? 0 : municipio.hashCode());
		result = prime * result + ((sede == null) ? 0 : sede.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MunicipioSemDiaria other = (MunicipioSemDiaria) obj;
		if (ativo != other.ativo)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (municipio == null) {
			if (other.municipio != null)
				return false;
		} else if (!municipio.equals(other.municipio))
			return false;
		if (sede == null) {
			if (other.sede != null)
				return false;
		} else if (!sede.equals(other.sede))
			return false;
		return true;
	}	

}
