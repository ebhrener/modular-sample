package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;

public class AutoInfracaoSiga implements Serializable{

	private static final long serialVersionUID = 2134343847751445612L;
	
	private String autoID;
	private String clienteID;
	private String processo;
	private String motivo;
	private String enquadrando;
	private String ufirExtenso;
	private String comentario;
	private String dataEmissao;
	private Integer status;
	
	public AutoInfracaoSiga(String autoID, String clienteID, String processo,
			String motivo, String enquadrando, String ufirExtenso,
			String comentario, String dataEmissao, Integer status) {
		super();
		this.autoID = autoID;
		this.clienteID = clienteID;
		this.processo = processo;
		this.motivo = motivo;
		this.enquadrando = enquadrando;
		this.ufirExtenso = ufirExtenso;
		this.comentario = comentario;
		this.dataEmissao = dataEmissao;
		this.status = status;
	}
	
	public String getAutoID() {
		return autoID;
	}
	public void setAutoID(String autoID) {
		this.autoID = autoID;
	}
	public String getClienteID() {
		return clienteID;
	}
	public void setClienteID(String clienteID) {
		this.clienteID = clienteID;
	}
	public String getProcesso() {
		return processo;
	}
	public void setProcesso(String processo) {
		this.processo = processo;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getEnquadrando() {
		return enquadrando;
	}
	public void setEnquadrando(String enquadrando) {
		this.enquadrando = enquadrando;
	}
	public String getUfirExtenso() {
		return ufirExtenso;
	}
	public void setUfirExtenso(String ufirExtenso) {
		this.ufirExtenso = ufirExtenso;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
