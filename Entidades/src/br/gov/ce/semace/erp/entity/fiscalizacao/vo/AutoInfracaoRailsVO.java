/**
 *
 */
package br.gov.ce.semace.erp.entity.fiscalizacao.vo;

import java.util.Date;

import javax.persistence.Transient;

public class AutoInfracaoRailsVO {

	private final String MSG_NAO_INFORMADO = "Não informado";

	/**
	 * Coluna fiscalizacao.auto_infracoes.num_aui representando o Número do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:00:47
	 */
	private String numeroDocumento;

	/**
	 * Coluna fiscalizacao.auto_infracoes.tipo_auto_id representando o tipo de Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:01:03
	 */
	private int tipo;

	/**
	 * Coluna fiscalizacao.auto_infracoes.valor representando o Valor da Multa do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:01:31
	 */
	private Double valorMulta;

	/**
	 * Coluna fiscalizacao.auto_infracoes.data_hora representando a Data da Autuação do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:02:40
	 */
	private Date dataAutuacao;

	/**
	 * Coluna fiscalizacao.auto_infracoes.data_vencimento representando a Data de Vencimento do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:02:47
	 */
	private Date dataVencimento;

	/**
	 * Coluna fiscalizacao.auto_infracoes.local representando o Local do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:03:31
	 */
	private String local;

	/**
	 * Coluna fiscalizacao.auto_infracoes.descricao representando a Descrição do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:03:52
	 */
	private String descricao;

	/**
	 * Coluna fiscalizacao.auto_infracoes.observacao representando a Observação do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:04:08
	 */
	private String observacao;

	/**
	 * Coluna fiscalizacao.interessados.cep representando o CEP do Endereço do Autuado do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:04:08
	 */
	private String cep;

	/**
	 * Coluna fiscalizacao.interessados.endereco representando o Endereço do Autuado do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:04:08
	 */
	private String endereco;

	/**
	 * Coluna fiscalizacao.interessados.bairro representando o Bairro do Endereço do Autuado do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:04:08
	 */
	private String bairro;

	/**
	 * Coluna public.municipios.descricao representando o Município do Endereço do Autuado do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:04:08
	 */
	private String municipio;

	/**
	 * Coluna public.estados.descricao representando o Estado do Endereço do Autuado do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:04:08
	 */
	private String estado;

	/**
	 * Coluna usuario.pessoas.nome representando o nome do Fiscal que fez o Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:07:29
	 */
	private String fiscalNome;

	/**
	 * Coluna fiscalizacao.interessados.nome representando o nome do Autuado do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:07:18
	 */
	private String autuadoNome;

	/**
	 * Coluna fiscalizacao.interessados.documento representando o Documento do Autuado do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:04:08
	 */
	private String autuadoDocumento;

	/**
	 * Coluna fiscalizacao.interessados.tipo_documento representando o Tipo de Documento (CPF ou CNPJ) do Autuado do Auto de Infração
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 01/08/2016 14:06:23
	 */
	private String autuadoTipoDocumento;

	/**
	 *
	 */
	public AutoInfracaoRailsVO() {
		super();
	}

	/**
	 * @param numeroDocumento
	 * @param tipo
	 * @param valorMulta
	 * @param dataAutuacao
	 * @param dataVencimento
	 * @param local
	 * @param descricao
	 * @param observacao
	 * @param cep
	 * @param endereco
	 * @param bairro
	 * @param municipio
	 * @param estado
	 * @param fiscalNome
	 * @param autuadoNome
	 * @param autuadoDocumento
	 * @param autuadoTipoDocumento
	 */
	public AutoInfracaoRailsVO(String numeroDocumento, int tipo, Double valorMulta, Date dataAutuacao, Date dataVencimento, String local,
			String descricao, String observacao, String cep, String endereco, String bairro, String municipio, String estado, String fiscalNome,
			String autuadoNome, String autuadoDocumento, String autuadoTipoDocumento) {

		super();

		this.numeroDocumento = numeroDocumento;
		this.tipo = tipo;
		this.valorMulta = valorMulta;
		this.dataAutuacao = dataAutuacao;
		this.dataVencimento = dataVencimento;
		this.local = local;
		this.descricao = descricao;
		this.observacao = observacao;
		this.cep = cep;
		this.endereco = endereco;
		this.bairro = bairro;
		this.municipio = municipio;
		this.estado = estado;
		this.fiscalNome = fiscalNome;
		this.autuadoNome = autuadoNome;
		this.autuadoDocumento = autuadoDocumento;
		this.autuadoTipoDocumento = autuadoTipoDocumento;
	}

	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * @return the dataAutuacao
	 */
	public Date getDataAutuacao() {
		return dataAutuacao;
	}

	/**
	 * @param dataAutuacao the dataAutuacao to set
	 */
	public void setDataAutuacao(Date dataAutuacao) {
		this.dataAutuacao = dataAutuacao;
	}

	/**
	 * @return the dataVencimento
	 */
	public Date getDataVencimento() {
		return dataVencimento;
	}

	/**
	 * @param dataVencimento the dataVencimento to set
	 */
	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	/**
	 * @return the local
	 */
	public String getLocal() {
		return local;
	}

	/**
	 * @param local the local to set
	 */
	public void setLocal(String local) {
		this.local = local;
	}

	@Transient
	public String getDescricao(int quantidadeCaracteres) {
		if (this.descricao.length() < quantidadeCaracteres) {
			return this.descricao;
		}

		return descricao.substring(0, quantidadeCaracteres) + " <bold>...</bold>";
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the observacao
	 */
	public String getObservacao() {

		if (this.observacao == null || this.observacao.trim().isEmpty()) {
			return MSG_NAO_INFORMADO;
		}

		return observacao;
	}

	/**
	 * @param observacao the observacao to set
	 */
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	/**
	 * @return the cep
	 */
	public String getCep() {
		return cep;
	}

	/**
	 * @param cep the cep to set
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}

	/**
	 * @return the endereco
	 */
	public String getEndereco() {
		return endereco;
	}

	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	/**
	 * @return the bairro
	 */
	public String getBairro() {
		return bairro;
	}

	/**
	 * @param bairro the bairro to set
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	/**
	 * @return the municipio
	 */
	public String getMunicipio() {
		return municipio;
	}

	/**
	 * @param municipio the municipio to set
	 */
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fiscalNome
	 */
	public String getFiscalNome() {
		return fiscalNome;
	}

	/**
	 * @param fiscalNome the fiscalNome to set
	 */
	public void setFiscalNome(String fiscalNome) {
		this.fiscalNome = fiscalNome;
	}

	/**
	 * @return the autuadoNome
	 */
	public String getAutuadoNome() {

		if (this.autuadoNome == null || this.autuadoNome.trim().isEmpty()) {
			return "Não Informado";
		}

		return autuadoNome;
	}

	/**
	 * @param autuadoNome the autuadoNome to set
	 */
	public void setAutuadoNome(String autuadoNome) {
		this.autuadoNome = autuadoNome;
	}

	/**
	 * @return the autuadoDocumento
	 */
	public String getAutuadoDocumento() {
		return autuadoDocumento;
	}

	/**
	 * @param autuadoDocumento the autuadoDocumento to set
	 */
	public void setAutuadoDocumento(String autuadoDocumento) {
		this.autuadoDocumento = autuadoDocumento;
	}

	/**
	 * @return the autuadoTipoDocumento
	 */
	public String getAutuadoTipoDocumento() {
		return autuadoTipoDocumento;
	}

	/**
	 * @param autuadoTipoDocumento the autuadoTipoDocumento to set
	 */
	public void setAutuadoTipoDocumento(String autuadoTipoDocumento) {
		this.autuadoTipoDocumento = autuadoTipoDocumento;
	}

	/**
	 * @return the tipo
	 */
	public int getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the valorMulta
	 */
	public Double getValorMulta() {
		return valorMulta;
	}

	/**
	 * @param valorMulta the valorMulta to set
	 */
	public void setValorMulta(Double valorMulta) {
		this.valorMulta = valorMulta;
	}

}