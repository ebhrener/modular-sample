package br.gov.ce.semace.erp.entity.juridico;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="configuracao_juridico", schema="juridico")
public class ConfiguracaoJuridico implements Serializable {

	private static final long serialVersionUID = -7796045409713776336L;

	@Id
	@SequenceGenerator(sequenceName="juridico.seq_configuracao_juridico", name="juridico.seq_configuracao_juridico", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="juridico.seq_configuracao_juridico")
	private Long id;

	@Column(name = "max_hipotese_ajuizamento")
	private BigDecimal maxHipoteseAjuizamento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getMaxHipoteseAjuizamento() {
		return maxHipoteseAjuizamento;
	}

	public void setMaxHipoteseAjuizamento(BigDecimal maxHipoteseAjuizamento) {
		this.maxHipoteseAjuizamento = maxHipoteseAjuizamento;
	}

}
