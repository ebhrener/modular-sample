package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio;

public enum AutoDeclaracaoCondicaoUsoPosseTerra {
	
	INDEFINIDO(0,"Indefinido"),
	PROPRIETARIO(1,"Proprietario/a"),
	ARRENDATARIO(2,"Arrendatario/a"),
	POSSEIRO(3,"Posseiro/a"),
	PARCEIO(4,"Parceiro/a"),
	MEEIRO(5,"Meeiro/a"),
	ASSENTADO(6,"Assentado/a pelo PNRA"),
	BENEFICIARIO(7,"Beneficiario/a do B. da Terra"),
	COMODATARIO(8,"Comodatario"),
	USO_COLETIVO(9,"Uso Coletivo"),
	NAO_SE_APLICA(10,"Nao se aplica"),
	CFCPR(11,"Beneficiario/a do CFCPR"),
	PN1A(12,"Benefiicario/a do PN1a. Terra"),
	OUTRA(99,"Outra"),
	PNRA(15,"PNRA s/ Dema. Qualific."),
	CEDULA_TERRA(16,"Ben. do Cédula da Terra"),
	ACAMPAMENTO(13,"Acampamento"),
	PNCF(14,"Beneficiario/a do PNCF");
	
	private Integer codigo;
	private String descricao;
	
	private  AutoDeclaracaoCondicaoUsoPosseTerra(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static String showDescricaoByCodigo(Integer codigo){
		for(AutoDeclaracaoCondicaoUsoPosseTerra auto: AutoDeclaracaoCondicaoUsoPosseTerra.values()){
			if(auto.getCodigo().equals(codigo)){
				return auto.descricao;
			}
		}
		return "Não Informado";
	}
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
}
