package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="tipo_mapa_tematico")
public class TipoMapaTematico implements Serializable {

	private static final long serialVersionUID = -995172300986185173L;

	@Id 
	@SequenceGenerator(sequenceName="seq_tipo_mapa_tematico", name="seq_tipo_mapa_tematico", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_tipo_mapa_tematico")
	private Long id;
	
	@Column(nullable = false)
	private String nome;
	
	@OneToMany(mappedBy="tipoMapaTematico", fetch=FetchType.LAZY)
	private List<MapaTematico> mapaTematicos = new ArrayList<MapaTematico>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<MapaTematico> getMapaTematicos() {
		return mapaTematicos;
	}

	public void setMapaTematicos(List<MapaTematico> mapaTematicos) {
		this.mapaTematicos = mapaTematicos;
	}
	
}