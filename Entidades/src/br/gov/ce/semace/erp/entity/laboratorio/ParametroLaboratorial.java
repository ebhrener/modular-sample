package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.TipoParametroLaboratorial;

@Entity
@Audited
@Table(schema = "laboratorio", name = "parametro_laboratorial")
public class ParametroLaboratorial implements Serializable {

	private static final long serialVersionUID = -1221075827457027275L;

	@Id
	@SequenceGenerator(sequenceName = "laboratorio.seq_parametro_laboratorial", name = "SEQ_PARAMETRO_LABORATORIAL", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PARAMETRO_LABORATORIAL")
	private Long id;
	
	private String descricao;
	
	private BigDecimal valor;
	
	private boolean metal = false;
	
	@Column(name = "tipo_parametro_laboratorial")
	private TipoParametroLaboratorial tipoParametroLaboratorial;
	
	@Column(columnDefinition = "boolean default true")
	private boolean ativo = true;
	
	@Override
	public int hashCode() {
		if(getId() != null){
			return getId().hashCode();
		}
		return 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof ParametroLaboratorial)) {
			return false;
		}
		return ((ParametroLaboratorial)obj).getId().equals(getId());
	}
	
	@Override
	public ParametroLaboratorial clone() throws CloneNotSupportedException {
		ParametroLaboratorial parametroClone = new ParametroLaboratorial();
		parametroClone.setId(this.getId());
		parametroClone.setDescricao(this.getDescricao());
		parametroClone.setMetal(this.getMetal());
		parametroClone.setTipoParametroLaboratorial(this.getTipoParametroLaboratorial());
		parametroClone.setValor(this.getValor());
		parametroClone.setMetal(this.metal);
		parametroClone.setAtivo(this.ativo);
		return parametroClone;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public boolean getMetal() {
		return metal;
	}

	public void setMetal(boolean metal) {
		this.metal = metal;
	}

	public TipoParametroLaboratorial getTipoParametroLaboratorial() {
		return tipoParametroLaboratorial;
	}

	public void setTipoParametroLaboratorial(
			TipoParametroLaboratorial tipoParametroLaboratorial) {
		this.tipoParametroLaboratorial = tipoParametroLaboratorial;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String isAtivoToDisplay() {
		return this.ativo ? "Sim" : "Não";
	}

}
