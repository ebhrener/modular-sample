package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio;

public enum AutoDeclaracaoEstadoCivil {
	
	Indefinido(0, "Indefinido"),
	Solteiro(1, "Solteiro"),
	Casado(2, "Casado"),
	Viuvo(3, "Viúvo"),
	Divorciado(4, "Divorciado"),
	Amasiado(5, "Amasiado"),
	Outros(6, "Outros");

	private Integer codigo;
	private String descricao;
	
	private AutoDeclaracaoEstadoCivil(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static String showDescricaoByCodigo(Integer codigo){
		for(AutoDeclaracaoEstadoCivil auto: AutoDeclaracaoEstadoCivil.values()){
			if(auto.getCodigo().equals(codigo)){
				return auto.descricao;
			}
		}
		return "Não Informado";
	}
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
}
