/**
 *
 */
package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

/**
 * @author stevens
 *
 */

@Entity
@Audited
@Table(schema="rh", name="situacao_funcionario")
public class SituacaoFuncionario implements Serializable {

	private static final long serialVersionUID = 7078438124230179561L;

	public SituacaoFuncionario() {
	}

	public SituacaoFuncionario(String descricao) {
		this.descricao = descricao;
	}

	@Id
	@SequenceGenerator(sequenceName="rh.seq_sit_funcionario", name="SEQSITFUNCIONARIO", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQSITFUNCIONARIO")
	private Long id;

	@NotNull(message="Campo Descição é obrigatório.")
	@Column(nullable=false)
	private String descricao;

	private Boolean ativo = false;

	//--------------------------------------------------------------------------------------------Gets e Setters--------------------------------------------------------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ativo == null) ? 0 : ativo.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SituacaoFuncionario other = (SituacaoFuncionario) obj;
		if (ativo == null) {
			if (other.ativo != null) {
				return false;
			}
		} else if (!ativo.equals(other.ativo)) {
			return false;
		}
		if (descricao == null) {
			if (other.descricao != null) {
				return false;
			}
		} else if (!descricao.equals(other.descricao)) {
			return false;
		}
		return true;
	}

}
