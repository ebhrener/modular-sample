package br.gov.ce.semace.erp.entity.protocolo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.gov.ce.semace.erp.entity.geral.Sede;
import br.gov.ce.semace.erp.entity.rh.Funcionario;

/**
 * Entidade referente às configurações do sistema Protocolo
 *
 * @author Victor Hugo R. Rodrigues - 18/12/2013 13:00:45
 * @version 1.0
 */
@Entity
@Table(name="configuracao_protocolo", schema="protocolo")
public class ConfiguracaoProtocolo implements Serializable{

	private static final long serialVersionUID = 5334110117483233971L;

	@Id
	private Long id;

	@Column(name="qtd_dias_alerta_monit_oficio",nullable=false)
	private Integer qtdDiasAlertaMonitoramentoOficio;

	@Column(name="qtd_dias_alerta_critico_monit_oficio",nullable=false)
	private Integer qtdDiasAlertaCriticoMonitoramentoOficio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionarioOficioMonitoramento = new Funcionario();

	@Column(name = "chave_geracao_processo_siga_id")
	private Integer chaveGeracaoProcessoSigaID;

	@Column(name = "usuario_sistema_siga_default")
	private String usuarioSistemaSigaDefault;

	@Column(name = "data_implantacao_ged")
	private Date dataImplantacaoGED;

	@Column(name = "prazo_seguranca_resolucao_pendencia")
	private Integer prazoSegurancaResolucaoPendencia;

	@Column(name = "prazo_resolucao_pendecia")
	private Long prazoResolucaoPendecia;

	@Column(name = "notificar_sede")
	private boolean notificarPorSede;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sede_padrao_id")
	private Sede sedePadrao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQtdDiasAlertaMonitoramentoOficio() {
		return qtdDiasAlertaMonitoramentoOficio;
	}

	public void setQtdDiasAlertaMonitoramentoOficio(Integer qtdDiasAlertaMonitoramentoOficio) {
		this.qtdDiasAlertaMonitoramentoOficio = qtdDiasAlertaMonitoramentoOficio;
	}

	public Integer getQtdDiasAlertaCriticoMonitoramentoOficio() {
		return qtdDiasAlertaCriticoMonitoramentoOficio;
	}

	public void setQtdDiasAlertaCriticoMonitoramentoOficio(Integer qtdDiasAlertaCriticoMonitoramentoOficio) {
		this.qtdDiasAlertaCriticoMonitoramentoOficio = qtdDiasAlertaCriticoMonitoramentoOficio;
	}

	public Funcionario getFuncionarioOficioMonitoramento() {
		return funcionarioOficioMonitoramento;
	}

	public void setFuncionarioOficioMonitoramento(Funcionario funcionarioOficioMonitoramento) {
		this.funcionarioOficioMonitoramento = funcionarioOficioMonitoramento;
	}

	public Integer getChaveGeracaoProcessoSigaID() {
		return chaveGeracaoProcessoSigaID;
	}

	public void setChaveGeracaoProcessoSigaID(Integer chaveGeracaoProcessoSigaID) {
		this.chaveGeracaoProcessoSigaID = chaveGeracaoProcessoSigaID;
	}

	public String getUsuarioSistemaSigaDefault() {
		return usuarioSistemaSigaDefault;
	}

	public void setUsuarioSistemaSigaDefault(String usuarioSistemaSigaDefault) {
		this.usuarioSistemaSigaDefault = usuarioSistemaSigaDefault;
	}

	public Date getDataImplantacaoGED() {
		return dataImplantacaoGED;
	}

	public void setDataImplantacaoGED(Date dataImplantacaoGED) {
		this.dataImplantacaoGED = dataImplantacaoGED;
	}

	public Long getPrazoResolucaoPendecia() {
		return prazoResolucaoPendecia;
	}

	public void setPrazoResolucaoPendecia(Long prazoResolucaoPendecia) {
		this.prazoResolucaoPendecia = prazoResolucaoPendecia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ConfiguracaoProtocolo other = (ConfiguracaoProtocolo) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public Integer getPrazoSegurancaResolucaoPendencia() {
		return prazoSegurancaResolucaoPendencia;
	}

	public void setPrazoSegurancaResolucaoPendencia(Integer prazoSegurancaResolucaoPendencia) {
		this.prazoSegurancaResolucaoPendencia = prazoSegurancaResolucaoPendencia;
	}

	public boolean isNotificarPorSede() {
		return notificarPorSede;
	}

	public void setNotificarPorSede(boolean notificarPorSede) {
		this.notificarPorSede = notificarPorSede;
	}

	public Sede getSedePadrao() {
		return sedePadrao;
	}

	public void setSedePadrao(Sede sedePadrao) {
		this.sedePadrao = sedePadrao;
	}

}