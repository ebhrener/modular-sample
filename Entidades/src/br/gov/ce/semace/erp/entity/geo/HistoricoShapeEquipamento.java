package br.gov.ce.semace.erp.entity.geo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "historico_shape_equipamento")
public class HistoricoShapeEquipamento implements Serializable{

	private static final long serialVersionUID = 595546045443852731L;

	@Id
	@SequenceGenerator(sequenceName="seq_historio_shape_equipamento", name="seq_historio_shape_equipamento", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq_historio_shape_equipamento")
	private Long id;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="shape_equipamento_id")
	private ShapeEquipamento shapeEquipamento;
	
	@Column(name="dt_alteracao")
	private Date data;
	
	private Boolean ativo;

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public ShapeEquipamento getShapeEquipamento() {
		return shapeEquipamento;
	}

	public void setShapeEquipamento(ShapeEquipamento shapeEquipamento) {
		this.shapeEquipamento = shapeEquipamento;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
