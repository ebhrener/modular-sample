package br.gov.ce.semace.erp.entity.fiscalizacao.entity.autodeclaracao.dominio;

public enum AutoDeclaracaoAtividadePrincipal {
	
	QUILOMBOLA(5,"Quilombola"),
	OUTRA(8,"Outra"),
	AQUICULTOR(3,"Aquicultor/a"),
	INDIO(7,"Indio/a"),
	INDEFINIDO(0,"Indefinido"),
	AGRICULTOR(1,"Agricultor/a"),
	PESCADOR(2,"Pescador/a"),
	EXTRATIVISTA(4,"Extrativista"),
	SILVICULTOR(6,"Silvicultor/a"),
	ARTESAO(9,"Artesao(a)"),
	TURISMO_RURAL(10,"Turismo Rural"),
	PECUARISTA(11,"Pecuarista"),
	OUTRAS(99,"Outra");
	
	private Integer codigo;
	private String descricao;
	
	private AutoDeclaracaoAtividadePrincipal(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static String showDescricaoByCodigo(Integer codigo){
		for(AutoDeclaracaoAtividadePrincipal auto: AutoDeclaracaoAtividadePrincipal.values()){
			if(auto.getCodigo().equals(codigo)){
				return auto.descricao;
			}
		}
		return "Não Informado";
	}
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
}
