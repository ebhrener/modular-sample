package br.gov.ce.semace.erp.entity.julgamento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.enuns.StatusJulgamento;

@Entity
@Audited
@Table(name = "suspensao", schema="julgamento")
public class Suspensao implements Serializable{

	private static final long serialVersionUID = -2241414448835221580L;
	
	@Id
	@SequenceGenerator(sequenceName ="julgamento.seq_suspensao", name = "SEQSUSPENSAO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQSUSPENSAO")
	private Long id;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "julgamento_id")
	private Julgamento julgamento = new Julgamento();
	
	@Column(name="justificativa_ativa", columnDefinition="text")
	private String justificativaAtiva;
	
	@Column(name="justificativa_suspenso", columnDefinition="text")
	private String justificativaSuspenso;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcionario_ativador_id")
	private Funcionario funcionarioAtivador = new Funcionario();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcionario_suspendeu_id")
	private Funcionario funcionarioSuspendeu = new Funcionario();
	
	@Column(name = "data_ativacao")
	private Date dataAtivacao;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_suspensao")
	private Date dataSuspensao = new Date();
	
	@Column(name = "ultimo_status_id")
	private StatusJulgamento ultimoStatus;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Julgamento getJulgamento() {
		return julgamento;
	}

	public void setJulgamento(Julgamento julgamento) {
		this.julgamento = julgamento;
	}

	public String getJustificativaAtiva() {
		return justificativaAtiva;
	}

	public void setJustificativaAtiva(String justificativaAtiva) {
		this.justificativaAtiva = justificativaAtiva;
	}

	public Funcionario getFuncionarioAtivador() {
		return funcionarioAtivador;
	}

	public void setFuncionarioAtivador(Funcionario funcionarioAtivador) {
		this.funcionarioAtivador = funcionarioAtivador;
	}

	public Date getDataAtivacao() {
		return dataAtivacao;
	}

	public void setDataAtivacao(Date dataAtivacao) {
		this.dataAtivacao = dataAtivacao;
	}

	public String getJustificativaSuspenso() {
		return justificativaSuspenso;
	}

	public void setJustificativaSuspenso(String justificativaSuspenso) {
		this.justificativaSuspenso = justificativaSuspenso;
	}

	public Funcionario getFuncionarioSuspendeu() {
		return funcionarioSuspendeu;
	}

	public void setFuncionarioSuspendeu(Funcionario funcionarioSuspendeu) {
		this.funcionarioSuspendeu = funcionarioSuspendeu;
	}

	public Date getDataSuspensao() {
		return dataSuspensao;
	}

	public void setDataSuspensao(Date dataSuspensao) {
		this.dataSuspensao = dataSuspensao;
	}
	
	public StatusJulgamento getUltimoStatus() {
		return ultimoStatus;
	}

	public void setUltimoStatus(StatusJulgamento ultimoStatus) {
		this.ultimoStatus = ultimoStatus;
	}

}
