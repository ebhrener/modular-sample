package br.gov.ce.semace.erp.entity.laboratorio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.semace.erp.enuns.TipoOrdemColeta;

@Entity
@Table(name="regiao_zona_ponto", schema="laboratorio")
public class RegiaoZonaPontoAmostragem implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -1179912842994838920L;

	@Id
	@SequenceGenerator(sequenceName="laboratorio.seq_regiao_zona_ponto", name="laboratorio.seq_regiao_zona_ponto", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="laboratorio.seq_regiao_zona_ponto")
	private Long id;

	@Enumerated(EnumType.ORDINAL)
	@Column(name="tipo_ordem_coleta", nullable=false)
	private TipoOrdemColeta tipoOrdemColeta;

	@ManyToOne
	@JoinColumn(name="regiao_id")
	private Regiao regiao;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="regiaoZonaPontoAmostragem")
	private List<PontoAmostragem> listPontoAmostragem = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Regiao getRegiao() {
		return regiao;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}


	public List<PontoAmostragem> getListPontoAmostragem() {
		return listPontoAmostragem;
	}

	public void setListPontoAmostragem(List<PontoAmostragem> listPontoAmostragem) {
		this.listPontoAmostragem = listPontoAmostragem;
	}

	public TipoOrdemColeta getTipoOrdemColeta() {
		return tipoOrdemColeta;
	}

	public void setTipoOrdemColeta(TipoOrdemColeta tipoOrdemColeta) {
		this.tipoOrdemColeta = tipoOrdemColeta;
	}


}
