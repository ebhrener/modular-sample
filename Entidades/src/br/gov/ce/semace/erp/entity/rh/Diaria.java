package br.gov.ce.semace.erp.entity.rh;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.enuns.Moeda;
import br.gov.ce.semace.erp.enuns.TipoSolicitacaoTransporte;

/**
 * Entidade {@link Diaria} do esquema RH.
 * 
 * @author Saulo Fernandes - saulofernandes.ti@gmail.com - 23/04/2014 11:35:12
 */
@Entity
@Audited
@Table(schema="rh", name="diaria")
public class Diaria implements Serializable{

	private static final long serialVersionUID = 7436453136800538778L;
	
	@Id
	@SequenceGenerator(sequenceName="rh.seq_diaria", name="SEQDIARIA", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQDIARIA")
	private Long id;
	
	@Column(name="tipo_solicitacao_transporte")
	private TipoSolicitacaoTransporte tipoSolicitacaoTransporte;
	
	private Moeda moeda;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="nivel_vinculacao_id", referencedColumnName="id")
	private NivelVinculacao nivelVinculacao = new NivelVinculacao();
	
	private BigDecimal valor;
	
	@Column(name="ativo")
	private boolean ativo = true;

	//================================== GETERS and SETERS ====================================//

	@Transient
	public String getDetalheAtivo(){

		if (this.ativo){
			return "Sim";
		}

		return "Não";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoSolicitacaoTransporte getTipoSolicitacaoTransporte() {
		return tipoSolicitacaoTransporte;
	}

	public void setTipoSolicitacaoTransporte(TipoSolicitacaoTransporte tipoSolicitacaoTransporte) {
		this.tipoSolicitacaoTransporte = tipoSolicitacaoTransporte;
	}

	public Moeda getMoeda() {
		return moeda;
	}

	public void setMoeda(Moeda moeda) {
		this.moeda = moeda;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public NivelVinculacao getNivelVinculacao() {
		return nivelVinculacao;
	}

	public void setNivelVinculacao(NivelVinculacao nivelVinculacao) {
		this.nivelVinculacao = nivelVinculacao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
}
