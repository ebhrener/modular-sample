package br.gov.ce.semace.erp.entity.protocolo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.ce.semace.erp.entity.geral.Sede;
import br.gov.ce.semace.erp.entity.rh.Funcionario;
import br.gov.ce.semace.erp.entity.rh.Setor;

@Entity
@Table(name = "config_notificacao_pendencia_liberada", schema = "protocolo")
public class ConfiguracaoNotificacaoPendenciaLiberada {

	@Id
	@SequenceGenerator(sequenceName = "protocolo.seq_config_notificacao_pendencia_liberada", name = "SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sede_id")
	private Sede sede;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setor_parada_processo_id")
	private Setor setorParadaProcesso;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "config_notif_pendencia_liberada_func",
		schema = "protocolo",
		joinColumns = @JoinColumn(name = "config_notificacao_pendencia_liberada_id"),
		inverseJoinColumns = @JoinColumn(name = "funcionario_id"))
	private List<Funcionario> listFuncionariosNotificado = new ArrayList<Funcionario>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

	public Setor getSetorParadaProcesso() {
		return setorParadaProcesso;
	}

	public void setSetorParadaProcesso(Setor setorParadaProcesso) {
		this.setorParadaProcesso = setorParadaProcesso;
	}

	public List<Funcionario> getListFuncionariosNotificado() {
		return listFuncionariosNotificado;
	}

	public void setListFuncionariosNotificado(List<Funcionario> listFuncionariosNotificado) {
		this.listFuncionariosNotificado = listFuncionariosNotificado;
	}

}
