package br.gov.ce.semace.erp.entity.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.utils.StringUtils;

@Entity
@Audited
@Table(name="contato", schema = "geral")
public class Contato implements Serializable{

	private static final long serialVersionUID = -3754275922634449770L;

	@Id @Column(name="contato_id")
	@SequenceGenerator(sequenceName="geral.seq_contato", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	private String nome;

	private String endereco;

	private String numero;

	private String bairro;

	private String cep;

	private String municipio;

	private String uf;

	private String telefone;

	private String documento;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getEnderecoCompleto(){
		String endereco = null;
		if (StringUtils.isNotBlank(getEndereco())) {
			endereco = getEndereco();
			if (StringUtils.isNotBlank(getNumero())) {
				endereco.concat(", Nº " + getNumero());
			}
			if (StringUtils.isNotBlank(getCep())) {
				endereco.concat(", CEP: " + getCep());
			}
			if (StringUtils.isNotBlank(getBairro())) {
				endereco.concat(", " + getBairro());
			}
			if (StringUtils.isNotBlank(getMunicipio())) {
				endereco.concat(", " + getMunicipio());
			}
			if (StringUtils.isNotBlank(getUf())) {
				endereco.concat(" - " + getUf());
			}
		}
		return endereco;
	}
}
