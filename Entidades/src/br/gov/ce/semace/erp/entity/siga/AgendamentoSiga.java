package br.gov.ce.semace.erp.entity.siga;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name="agendamento", schema="adm_siga")
public class AgendamentoSiga implements Serializable{


	private static final long serialVersionUID = 4516568776579025660L;

	@Id
	@SequenceGenerator(sequenceName="adm_siga.agendamdento_id_seq", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Integer id;

	@Column(nullable=false)
	private String nome;

	@Column(columnDefinition="text",nullable=false)
	private String endereco;

	@Column(nullable=false,length=8)
	private String cep;

	@Column(name="municipio_id",nullable=false,length=4)
	private String municipioId;

	private Double area;

	private Integer funcionarios;

	@Column(nullable=false)
	private String requerimento;

	@Column(nullable=false)
	private String contato;

	@Column(name="endereco_contato",columnDefinition="text",nullable=false)
	private String enderecoContato;

	@Column(name="cep_contato",nullable=false,length=8)
	private String cepContato;

	@Column(nullable=false,length=13)
	private String telefone;

	@Column(length=13)
	private String celular;

	@Column(length=13)
	private String fax;

	@Column(nullable=false,length=170)
	private String email;

	@Column(nullable=false,length=12)
	private String entrega;

	@Column(name="codigo_validacao",nullable=false,length=6)
	private String codigoValidacao;

	@Column(nullable=false)
	private Boolean validado = false;

	@Column(nullable=false)
	private Boolean confirmado = true;

	private Double hectares;

	@Column(columnDefinition="text")
	private String objetivo;

	private String atividade;

	@Column(name="endereco_empreendimento",columnDefinition="text")
	private String enderecoEmpreendimento;

	@Column(name="municipio_empreendimento",length=4)
	private String municipioEmpreendimento;

	@Column(name="cep_empreendimento",length=8)
	private String cepEmpreendimento;

	@Column(name="ponto_empreendimento")
	private String pontoEmpreendimento;

	@Column(length=7)
	private String latitude;

	@Column(length=6)
	private String longitude;

	@Column(columnDefinition="text")
	private String info;

	@Column(length=2)
	private String uf;

	@Column(length=2, name = "uf_empreendimento")
	private String ufEmpreendimento;

	@Column(name="valor_fatura",length=16)
	private String valorFatura;

	@Column(nullable=false,length=14)
	private String cnpj;

	@Column(name="data_agendamento")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAgendamento;

	@Column(name="data_cadastro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro = new Date();

	@Column(name="spu_licenca_id")
	private String spuLicencaId;

	@Column(name="usuario_id",length=4)
	private String usuarioId;

	@Column(name="spu_id",columnDefinition="character(15) DEFAULT NULL::bpchar")
	private String spuId;

	@Column(name="spu_anterior",length=10)
	private String spuAnterior;

	@Column(nullable=false)
	private Boolean isento = false;

	@Column(columnDefinition="text")
	private String destinacao;

	@Column(name="requerimento_id")
	private Integer requerimentoId;

	@Column(name="sede_id")
	private Integer sede;

	@Column(name = "nao_gera_spu")
	private Boolean naoGeraSpu;

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getEndereco() {
		return endereco;
	}


	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}


	public String getCep() {
		return cep;
	}


	public void setCep(String cep) {
		this.cep = cep;
	}


	public String getMunicipioId() {
		return municipioId;
	}


	public void setMunicipioId(String municipioId) {
		this.municipioId = municipioId;
	}

	public Double getArea() {
		return area;
	}


	public void setArea(Double area) {
		this.area = area;
	}


	public Integer getFuncionarios() {
		return funcionarios;
	}


	public void setFuncionarios(Integer funcionarios) {
		this.funcionarios = funcionarios;
	}


	public String getRequerimento() {
		return requerimento;
	}


	public void setRequerimento(String requerimento) {
		this.requerimento = requerimento;
	}


	public String getContato() {
		return contato;
	}


	public void setContato(String contato) {
		this.contato = contato;
	}


	public String getEnderecoContato() {
		return enderecoContato;
	}


	public void setEnderecoContato(String enderecoContato) {
		this.enderecoContato = enderecoContato;
	}


	public String getCepContato() {
		return cepContato;
	}


	public void setCepContato(String cepContato) {
		this.cepContato = cepContato;
	}


	public String getTelefone() {
		return telefone;
	}


	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getEntrega() {
		return entrega;
	}


	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}


	public String getCodigoValidacao() {
		return codigoValidacao;
	}


	public void setCodigoValidacao(String codigoValidacao) {
		this.codigoValidacao = codigoValidacao;
	}


	public Boolean getValidado() {
		return validado;
	}


	public void setValidado(Boolean validado) {
		this.validado = validado;
	}


	public Boolean getConfirmado() {
		return confirmado;
	}


	public void setConfirmado(Boolean confirmado) {
		this.confirmado = confirmado;
	}

	public Double getHectares() {
		return hectares;
	}


	public void setHectares(Double hectares) {
		this.hectares = hectares;
	}


	public String getObjetivo() {
		return objetivo;
	}


	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}


	public String getAtividade() {
		return atividade;
	}


	public void setAtividade(String atividade) {
		this.atividade = atividade;
	}


	public String getEnderecoEmpreendimento() {
		return enderecoEmpreendimento;
	}


	public void setEnderecoEmpreendimento(String enderecoEmpreendimento) {
		this.enderecoEmpreendimento = enderecoEmpreendimento;
	}


	public String getMunicipioEmpreendimento() {
		return municipioEmpreendimento;
	}


	public void setMunicipioEmpreendimento(String municipioEmpreendimento) {
		this.municipioEmpreendimento = municipioEmpreendimento;
	}


	public String getCepEmpreendimento() {
		return cepEmpreendimento;
	}


	public void setCepEmpreendimento(String cepEmpreendimento) {
		this.cepEmpreendimento = cepEmpreendimento;
	}


	public String getPontoEmpreendimento() {
		return pontoEmpreendimento;
	}


	public void setPontoEmpreendimento(String pontoEmpreendimento) {
		this.pontoEmpreendimento = pontoEmpreendimento;
	}


	public String getLatitude() {
		return latitude;
	}


	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}


	public String getLongitude() {
		return longitude;
	}


	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}


	public String getInfo() {
		return info;
	}


	public void setInfo(String info) {
		this.info = info;
	}


	public String getUf() {
		return uf;
	}


	public void setUf(String uf) {
		this.uf = uf;
	}


	public String getValorFatura() {
		return valorFatura;
	}


	public void setValorFatura(String valorFatura) {
		this.valorFatura = valorFatura;
	}


	public String getCnpj() {
		return cnpj;
	}


	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}


	public Date getDataAgendamento() {
		return dataAgendamento;
	}


	public void setDataAgendamento(Date dataAgendamento) {
		this.dataAgendamento = dataAgendamento;
	}


	public Date getDataCadastro() {
		return dataCadastro;
	}


	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}


	public String getUsuarioId() {
		return usuarioId;
	}


	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}


	public String getSpuId() {
		return spuId;
	}


	public void setSpuId(String spuId) {
		this.spuId = spuId;
	}


	public String getSpuAnterior() {
		return spuAnterior;
	}


	public void setSpuAnterior(String spuAnterior) {
		this.spuAnterior = spuAnterior;
	}


	public Boolean getIsento() {
		return isento;
	}


	public void setIsento(Boolean isento) {
		this.isento = isento;
	}


	public String getDestinacao() {
		return destinacao;
	}


	public void setDestinacao(String destinacao) {
		this.destinacao = destinacao;
	}


	public Integer getRequerimentoId() {
		return requerimentoId;
	}


	public void setRequerimentoId(Integer requerimentoId) {
		this.requerimentoId = requerimentoId;
	}


	public String getUfEmpreendimento() {
		return ufEmpreendimento;
	}


	public void setUfEmpreendimento(String ufEmpreendimento) {
		this.ufEmpreendimento = ufEmpreendimento;
	}


	public Integer getSede() {
		return sede;
	}


	public void setSede(Integer sede) {
		this.sede = sede;
	}


	public Boolean getNaoGeraSpu() {
		return naoGeraSpu;
	}


	public void setNaoGeraSpu(Boolean naoGeraSpu) {
		this.naoGeraSpu = naoGeraSpu;
	}


	public String getSpuLicencaId() {
		return spuLicencaId;
	}


	public void setSpuLicencaId(String spuLicencaId) {
		this.spuLicencaId = spuLicencaId;
	}



}