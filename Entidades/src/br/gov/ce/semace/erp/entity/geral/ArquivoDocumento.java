package br.gov.ce.semace.erp.entity.geral;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.ce.semace.erp.entity.protocolo.TipoAnexo;
import br.gov.ce.semace.erp.enuns.TipoCriacaoArquivo;

@Entity
@Audited
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "arquivo_documento", schema = "geral")
public class ArquivoDocumento implements Serializable, Cloneable {

	private static final long serialVersionUID = 5740312897270759127L;

	@Id
	@Column(name = "arquivo_documento_id")
	@SequenceGenerator(sequenceName = "geral.seq_arquivo_documento", name = "SEQ", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documento_id_natuur")
	private Documento documentoNatuur;

	@Column(name = "documento_id_siga")
	private Long documentoIdSiga;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_documento_id")
	private TipoDocumentoEntity tipoDocumentoEntity;

	@Column(name = "ged_id")
	private Long gedId;

	@Column(name = "numero_spu")
	private String numeroSpu;

	@Column(name = "nome")
	private String nome;

	@Column(name = "tipo_conteudo")
	private String tipoConteudo;

	@Column(name = "tamanho")
	private Integer tamanho;

	@Column(name = "pagina")
	private Integer pagina;

	@Column(name = "numero_paginas")
	private Integer numeroPaginas;

	@Column(name = "excluido")
	private boolean excluido;

	@Column(name = "justificativa")
	private String justificativa;

	@Column(name = "hash")
	private String hash;

	@Column(name="publico")
	private Boolean publico;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro", insertable = false, updatable = false)
	private Date dataCadastro;

	@Transient
	private String numeroDocumento;

	@Transient
	private TipoAnexo tipoAnexo;

	@Transient
	private byte[] bytes;

	@Transient
	private File file;

	@Transient
	private boolean isAssinado = false;

	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "arquivo_temporario_id")
	private ArquivoTemporario arquivoTemporario;

	@OneToMany(mappedBy = "arquivoDocumento", fetch = FetchType.LAZY)
	private List<Signatario> signatarioList;

	@Column(name="tipo_criacao_documento")
	private TipoCriacaoArquivo tipoCriacao;

	@Column(name="sem_efeito")
	private boolean semEfeito;

	public boolean isDigital() {
		return tipoCriacao.isDigital();
	}

	public boolean isDigitalizado() {
		return tipoCriacao.isDigitalizado();
	}

	public ArquivoDocumento() {
		super();
	}

	public ArquivoDocumento(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Documento getDocumentoNatuur() {
		return documentoNatuur;
	}

	public void setDocumentoNatuur(Documento documentoNatuur) {
		this.documentoNatuur = documentoNatuur;
	}

	public Long getDocumentoIdSiga() {
		return documentoIdSiga;
	}

	public void setDocumentoIdSiga(Long documentoIdSiga) {
		this.documentoIdSiga = documentoIdSiga;
	}

	public TipoDocumentoEntity getTipoDocumentoEntity() {
		return tipoDocumentoEntity;
	}

	public void setTipoDocumentoEntity(TipoDocumentoEntity tipoDocumentoEntity) {
		this.tipoDocumentoEntity = tipoDocumentoEntity;
	}

	public Long getGedId() {
		return gedId;
	}

	public void setGedId(Long gedId) {
		this.gedId = gedId;
	}

	public String getNumeroSpu() {
		return numeroSpu;
	}

	public void setNumeroSpu(String numeroSpu) {
		this.numeroSpu = numeroSpu;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipoConteudo() {
		return tipoConteudo;
	}

	public void setTipoConteudo(String tipoConteudo) {
		this.tipoConteudo = tipoConteudo;
	}

	public Integer getTamanho() {
		return tamanho;
	}

	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}

	public Integer getPagina() {
		return pagina;
	}

	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}

	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}

	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	public boolean isExcluido() {
		return excluido;
	}

	public void setExcluido(boolean excluido) {
		this.excluido = excluido;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Boolean getPublico() {
		return publico;
	}

	public void setPublico(Boolean publico) {
		this.publico = publico;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public TipoAnexo getTipoAnexo() {
		return tipoAnexo;
	}

	public void setTipoAnexo(TipoAnexo tipoAnexo) {
		this.tipoAnexo = tipoAnexo;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public boolean isAssinado() {
		return isAssinado;
	}

	public void setIsAssinado(boolean isAssinado) {
		this.isAssinado = isAssinado;
	}

	public ArquivoTemporario getArquivoTemporario() {
		return arquivoTemporario;
	}

	public void setArquivoTemporario(ArquivoTemporario arquivoTemporario) {
		this.arquivoTemporario = arquivoTemporario;
	}

	public List<Signatario> getSignatarioList() {
		return signatarioList;
	}

	public void setSignatarioList(List<Signatario> signatarioList) {
		this.signatarioList = signatarioList;
	}

	public TipoCriacaoArquivo getTipoCriacao() {
		return tipoCriacao;
	}

	public void setTipoCriacao(TipoCriacaoArquivo tipoCriacao) {
		this.tipoCriacao = tipoCriacao;
	}

	public boolean isSemEfeito() {
		return semEfeito;
	}

	public void setSemEfeito(boolean semEfeito) {
		this.semEfeito = semEfeito;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ArquivoDocumento other = (ArquivoDocumento) obj;
		if (nome == null) {
			if (other.nome != null) {
				return false;
			}
		} else if (!nome.equals(other.nome)) {
			return false;
		}
		return true;
	}

	public String getDisplayPagina() {
		if (pagina != null && numeroPaginas != null) {
			if (numeroPaginas > 1) {
				return pagina + " - " + ((pagina + numeroPaginas) - 1);
			}
			return Integer.toString(pagina);
		}
		return "-";
	}

	@Override
	@Transient
	public ArquivoDocumento clone() throws CloneNotSupportedException {

		ArquivoDocumento clone = new ArquivoDocumento();
		clone.setId(this.id);
		clone.setDocumentoNatuur(this.documentoNatuur != null && this.documentoNatuur.getId() != null ? this.documentoNatuur : null);
		clone.setDocumentoIdSiga(this.documentoIdSiga);
		clone.setTipoDocumentoEntity(this.tipoDocumentoEntity);
		clone.setGedId(this.gedId);
		clone.setNumeroSpu(this.numeroSpu);
		clone.setNome(this.nome);
		clone.setTipoConteudo(this.tipoConteudo);
		clone.setTamanho(this.tamanho);
		clone.setPagina(this.pagina);
		clone.setNumeroPaginas(this.numeroPaginas);
		clone.setExcluido(this.excluido);
		clone.setJustificativa(this.justificativa);
		clone.setHash(this.hash);
		clone.setPublico(this.publico);
		clone.setDataCadastro(this.dataCadastro);
		clone.setNumeroDocumento(this.numeroDocumento);
		clone.setTipoAnexo(this.tipoAnexo);
		clone.setBytes(this.bytes);
		clone.setFile(this.file);
		clone.setIsAssinado(this.isAssinado);
		clone.setArquivoTemporario(this.arquivoTemporario != null && this.arquivoTemporario.getId() != null ? this.arquivoTemporario : null);
		clone.setSignatarioList(this.signatarioList != null && !this.signatarioList.isEmpty() ? this.signatarioList : null);
		clone.setTipoCriacao(this.tipoCriacao);

		return clone;
	}



}
