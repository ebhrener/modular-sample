package br.gov.ce.semace.erp.entity.financeiro;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;


@Entity
@Audited
@Table(schema = "financeiro", name = "pagamento_tcfa")
public class PagamentoTCFA implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -3153101608220679867L;

	@Id
	@SequenceGenerator(sequenceName = "financeiro.seq_pagamento_tcfa", name = "seq_pagamento_tcfa", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_pagamento_tcfa")
	private Long id;

	@OneToOne(optional=false)
	@JoinColumn(name="dae_id")
	private Dae dae;

	@ManyToOne(optional=false)
	@JoinColumn(name="tcfa_id")
	private Tcfa tcfa;

	private Boolean trimestre1 = false;

	private Boolean trimestre2 = false;

	private Boolean trimestre3 = false;

	private Boolean trimestre4 = false;

	public Dae getDae() {
		return dae;
	}

	public void setDae(Dae dae) {
		this.dae = dae;
	}

	public Tcfa getTcfa() {
		return tcfa;
	}

	public void setTcfa(Tcfa tcfa) {
		this.tcfa = tcfa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getTrimestre1() {
		return trimestre1;
	}

	public void setTrimestre1(Boolean trimestre1) {
		this.trimestre1 = trimestre1;
	}

	public Boolean getTrimestre2() {
		return trimestre2;
	}

	public void setTrimestre2(Boolean trimestre2) {
		this.trimestre2 = trimestre2;
	}

	public Boolean getTrimestre3() {
		return trimestre3;
	}

	public void setTrimestre3(Boolean trimestre3) {
		this.trimestre3 = trimestre3;
	}

	public Boolean getTrimestre4() {
		return trimestre4;
	}

	public void setTrimestre4(Boolean trimestre4) {
		this.trimestre4 = trimestre4;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PagamentoTCFA other = (PagamentoTCFA) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public PagamentoTCFA clone() throws CloneNotSupportedException {
		PagamentoTCFA pagamentoTCFA = new PagamentoTCFA();
		pagamentoTCFA.setDae(this.dae);
		pagamentoTCFA.setTcfa(this.tcfa);
		pagamentoTCFA.setTrimestre1(this.trimestre1);
		pagamentoTCFA.setTrimestre2(this.trimestre2);
		pagamentoTCFA.setTrimestre3(this.trimestre3);
		pagamentoTCFA.setTrimestre4(this.trimestre4);

		return pagamentoTCFA;
	}

}