package br.gov.ce.semace.erp.vo;

import br.gov.ce.semace.erp.enuns.StatusOcorrencia;

public class RelatorioOcorrenciaVO {

	private StatusOcorrencia statusOcorrencia;

	private int valor;

	public StatusOcorrencia getStatusOcorrencia() {
		return statusOcorrencia;
	}

	public void setStatusOcorrencia(StatusOcorrencia statusOcorrencia) {
		this.statusOcorrencia = statusOcorrencia;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

}
