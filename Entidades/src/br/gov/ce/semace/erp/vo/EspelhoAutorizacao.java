package br.gov.ce.semace.erp.vo;

import java.util.Date;
import java.util.List;

public class EspelhoAutorizacao extends DocumentoProcesso {

	private static final long serialVersionUID = 1L;

	private Boolean vegetacao;
	private String parecer;
	private String relatorio;
	private String localidade;
	private String municipio2;
	private String coordenadas;
	private String rendimentoTotal;
	private String areaReserva;
	private String areaTotal;
	private String lenha;
	private String estaca;
	private String vara;
	private String areaDesmatamento;
	private String autorizacaoId;
	private String unidadeMedidaArea;
	private Boolean renovacao;
	private Boolean regularizacao;
	private Date dataEmissao;
	private String dataEmissaoExtenso;
	private String diaSemanaDataEmissao;
	private Date dataValidade;
	private String dataValidadeFormatada;
	private Integer talhao;
	private String spuOriginalId;
	private String objetivo;
	private String descricaoEmpreendimento;
	private String observacao;
	private String cliente;
	private String cnpj;
	private String cpf;
	private String numero;
	private String spuId;
	private String tipoProcessoId;
	private String areaNucleoId;
	private String coordAssinouId;
	private String complemento;
	private String nome;
	private String usuarioId;
	private String endereco;
	private String cepId;
	private String municipio;
	private String ufId;
	private String textoAutorizacao;
	private String nomeSetor;
	private List<String> condicionantes;
	private List<String> condicionantesFixas;
	private List<String> listEquipeTecnica;
	private String nomeCoord;
	private String siglaCategoriaCoord;
	private String especialidadeCoord;
	private String areaCoord;
	private String siglaCoord;
	private String assinatura1;
	private String assinatura2;

	public Boolean getVegetacao() {
		return vegetacao;
	}
	public void setVegetacao(Boolean vegetacao) {
		this.vegetacao = vegetacao;
	}
	public String getParecer() {
		return parecer;
	}
	public void setParecer(String parecer) {
		this.parecer = parecer;
	}
	public String getRelatorio() {
		return relatorio;
	}
	public void setRelatorio(String relatorio) {
		this.relatorio = relatorio;
	}
	public String getLocalidade() {
		return localidade;
	}
	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}
	public String getMunicipio2() {
		return municipio2;
	}
	public void setMunicipio2(String municipio2) {
		this.municipio2 = municipio2;
	}
	public String getCoordenadas() {
		return coordenadas;
	}
	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}
	public String getRendimentoTotal() {
		return rendimentoTotal;
	}
	public void setRendimentoTotal(String rendimentoTotal) {
		this.rendimentoTotal = rendimentoTotal;
	}
	public String getAreaReserva() {
		return areaReserva;
	}
	public void setAreaReserva(String areaReserva) {
		this.areaReserva = areaReserva;
	}
	public String getAreaTotal() {
		return areaTotal;
	}
	public void setAreaTotal(String areaTotal) {
		this.areaTotal = areaTotal;
	}
	public String getLenha() {
		return lenha;
	}
	public void setLenha(String lenha) {
		this.lenha = lenha;
	}
	public String getEstaca() {
		return estaca;
	}
	public void setEstaca(String estaca) {
		this.estaca = estaca;
	}
	public String getVara() {
		return vara;
	}
	public void setVara(String vara) {
		this.vara = vara;
	}
	public String getAreaDesmatamento() {
		return areaDesmatamento;
	}
	public void setAreaDesmatamento(String areaDesmatamento) {
		this.areaDesmatamento = areaDesmatamento;
	}
	public String getAutorizacaoId() {
		return autorizacaoId;
	}
	public void setAutorizacaoId(String autorizacaoId) {
		this.autorizacaoId = autorizacaoId;
	}
	public String getUnidadeMedidaArea() {
		return unidadeMedidaArea;
	}
	public void setUnidadeMedidaArea(String unidadeMedidaArea) {
		this.unidadeMedidaArea = unidadeMedidaArea;
	}
	public Boolean getRenovacao() {
		return renovacao;
	}
	public void setRenovacao(Boolean renovacao) {
		this.renovacao = renovacao;
	}
	public Boolean getRegularizacao() {
		return regularizacao;
	}
	public void setRegularizacao(Boolean regularizacao) {
		this.regularizacao = regularizacao;
	}
	public Date getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public String getDataEmissaoExtenso() {
		return dataEmissaoExtenso;
	}
	public void setDataEmissaoExtenso(String dataEmissaoExtenso) {
		this.dataEmissaoExtenso = dataEmissaoExtenso;
	}
	public String getDiaSemanaDataEmissao() {
		return diaSemanaDataEmissao;
	}
	public void setDiaSemanaDataEmissao(String diaSemanaDataEmissao) {
		this.diaSemanaDataEmissao = diaSemanaDataEmissao;
	}
	public Date getDataValidade() {
		return dataValidade;
	}
	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}
	public String getDataValidadeFormatada() {
		return dataValidadeFormatada;
	}
	public void setDataValidadeFormatada(String dataValidadeFormatada) {
		this.dataValidadeFormatada = dataValidadeFormatada;
	}
	public Integer getTalhao() {
		return talhao;
	}
	public void setTalhao(Integer talhao) {
		this.talhao = talhao;
	}
	public String getSpuOriginalId() {
		return spuOriginalId;
	}
	public void setSpuOriginalId(String spuOriginalId) {
		this.spuOriginalId = spuOriginalId;
	}
	public String getObjetivo() {
		return objetivo;
	}
	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}
	public String getDescricaoEmpreendimento() {
		return descricaoEmpreendimento;
	}
	public void setDescricaoEmpreendimento(String descricaoEmpreendimento) {
		this.descricaoEmpreendimento = descricaoEmpreendimento;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getSpuId() {
		return spuId;
	}
	public void setSpuId(String spuId) {
		this.spuId = spuId;
	}
	public String getTipoProcessoId() {
		return tipoProcessoId;
	}
	public void setTipoProcessoId(String tipoProcessoId) {
		this.tipoProcessoId = tipoProcessoId;
	}
	public String getAreaNucleoId() {
		return areaNucleoId;
	}
	public void setAreaNucleoId(String areaNucleoId) {
		this.areaNucleoId = areaNucleoId;
	}
	public String getCoordAssinouId() {
		return coordAssinouId;
	}
	public void setCoordAssinouId(String coordAssinouId) {
		this.coordAssinouId = coordAssinouId;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getCepId() {
		return cepId;
	}
	public void setCepId(String cepId) {
		this.cepId = cepId;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getUfId() {
		return ufId;
	}
	public void setUfId(String ufId) {
		this.ufId = ufId;
	}
	public String getTextoAutorizacao() {
		return textoAutorizacao;
	}
	public void setTextoAutorizacao(String textoAutorizacao) {
		this.textoAutorizacao = textoAutorizacao;
	}
	public String getNomeSetor() {
		return nomeSetor;
	}
	public void setNomeSetor(String nomeSetor) {
		this.nomeSetor = nomeSetor;
	}
	public List<String> getCondicionantes() {
		return condicionantes;
	}
	public void setCondicionantes(List<String> condicionantes) {
		this.condicionantes = condicionantes;
	}
	public List<String> getCondicionantesFixas() {
		return condicionantesFixas;
	}
	public void setCondicionantesFixas(List<String> condicionantesFixas) {
		this.condicionantesFixas = condicionantesFixas;
	}
	public List<String> getListEquipeTecnica() {
		return listEquipeTecnica;
	}
	public void setListEquipeTecnica(List<String> listEquipeTecnica) {
		this.listEquipeTecnica = listEquipeTecnica;
	}
	public String getNomeCoord() {
		return nomeCoord;
	}
	public void setNomeCoord(String nomeCoord) {
		this.nomeCoord = nomeCoord;
	}
	public String getSiglaCategoriaCoord() {
		return siglaCategoriaCoord;
	}
	public void setSiglaCategoriaCoord(String siglaCategoriaCoord) {
		this.siglaCategoriaCoord = siglaCategoriaCoord;
	}
	public String getEspecialidadeCoord() {
		return especialidadeCoord;
	}
	public void setEspecialidadeCoord(String especialidadeCoord) {
		this.especialidadeCoord = especialidadeCoord;
	}
	public String getAreaCoord() {
		return areaCoord;
	}
	public void setAreaCoord(String areaCoord) {
		this.areaCoord = areaCoord;
	}
	public String getSiglaCoord() {
		return siglaCoord;
	}
	public void setSiglaCoord(String siglaCoord) {
		this.siglaCoord = siglaCoord;
	}
	public String getAssinatura1() {
		return assinatura1;
	}
	public void setAssinatura1(String assinatura1) {
		this.assinatura1 = assinatura1;
	}
	public String getAssinatura2() {
		return assinatura2;
	}
	public void setAssinatura2(String assinatura2) {
		this.assinatura2 = assinatura2;
	}

}
