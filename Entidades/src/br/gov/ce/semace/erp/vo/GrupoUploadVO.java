package br.gov.ce.semace.erp.vo;

import br.gov.ce.semace.erp.enuns.PadraoUploadShape;


/**
 * !!!!!!!   Atenção   !!!!!!!!!!!
 *
 * Não utilizar a classe UploadedFile no projeto Entidades pois é uma dependencia do primefaces
 *
 * @author joerlan.lima
 *
 */
public class GrupoUploadVO {

	private String name;

	private String suffix;

	private PadraoUploadShape padrao;

//	private UploadedFile file;

	public GrupoUploadVO(String name, PadraoUploadShape padrao) {
		super();
		this.name = name;
		this.padrao = padrao;
		this.suffix = generateSuffix();
	}

	public GrupoUploadVO(String name, String suffix, PadraoUploadShape padrao) {
		super();
		this.name = name;
		this.suffix = suffix;
		this.padrao = padrao;
	}

//	public GrupoUploadVO(String name, String suffix, PadraoUploadShape padrao, UploadedFile file) {
//		super();
//		this.name = name;
//		this.suffix = suffix;
//		this.padrao = padrao;
//		this.file = file;
//	}

//	public GrupoUploadVO(String name, PadraoUploadShape padrao, UploadedFile file) {
//		super();
//		this.name = name;
//		this.suffix = generateSuffix();
//		this.padrao = padrao;
//		this.file = file;
//	}

	public boolean isPadraoKMZ() {
		return this.padrao.isKMZ();
	}

	public boolean isPadraoKML() {
		return this.padrao.isKML();
	}

	public boolean isPadraoShape() {
		return this.padrao.isShape();
	}

	private static String generateSuffix() {
		return "_" + System.nanoTime();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public PadraoUploadShape getPadrao() {
		return padrao;
	}

	public void setPadrao(PadraoUploadShape padrao) {
		this.padrao = padrao;
	}

//	public UploadedFile getFile() {
//		return file;
//	}
//
//	public void setFile(UploadedFile file) {
//		this.file = file;
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((padrao == null) ? 0 : padrao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GrupoUploadVO other = (GrupoUploadVO) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (padrao != other.padrao) {
			return false;
		}
		return true;
	}

//	public static List<FileUploadVO> parse(List<GrupoUploadVO> listGrupoFiles) {
//
//		List<FileUploadVO> listFileUploadVO = new ArrayList<>();
//
//		for (GrupoUploadVO grupoFiles : listGrupoFiles) {
//
//			if (grupoFiles.isPadraoKML()) {
//				UploadedFile fileKML = grupoFiles.getFile();
//				String name = retirarAcentos(grupoFiles.getName()) + grupoFiles.getSuffix() + KML.getExtensao();
//				listFileUploadVO.add(new FileUploadVO(name, null, 0, fileKML.getContents(), fileKML.getContentType()));
//			} else if (grupoFiles.isPadraoKMZ()) {
//				UploadedFile fileKMZ = grupoFiles.getFile();
//				String name = retirarAcentos(grupoFiles.getName()) + grupoFiles.getSuffix() + KMZ.getExtensao();
//				listFileUploadVO.add(new FileUploadVO(name, null, 0, fileKMZ.getContents(), fileKMZ.getContentType()));
//			} else if (grupoFiles.isPadraoShape()) {
//
//				GrupoUploadShapeVO grupoFilesShape = (GrupoUploadShapeVO) grupoFiles;
//
//				String name = retirarAcentos(grupoFilesShape.getName()) + grupoFilesShape.getSuffix();
//
//				UploadedFile fileSHP = grupoFilesShape.getFileSHP();
//				listFileUploadVO.add(new FileUploadVO(name + SHP.getExtensao(), null, 0, fileSHP.getContents(), fileSHP.getContentType()));
//
//				UploadedFile fileSHX = grupoFilesShape.getFileSHX();
//				listFileUploadVO.add(new FileUploadVO(name + SHX.getExtensao(), null, 0, fileSHX.getContents(), fileSHX.getContentType()));
//
//				UploadedFile fileDBF = grupoFilesShape.getFileDBF();
//				listFileUploadVO.add(new FileUploadVO(name + DBF.getExtensao(), null, 0, fileDBF.getContents(), fileDBF.getContentType()));
//
//				UploadedFile filePRJ = grupoFilesShape.getFilePRJ();
//				if (filePRJ != null) {
//					listFileUploadVO.add(new FileUploadVO(name + PRJ.getExtensao(), null, 0, filePRJ.getContents(), filePRJ.getContentType()));
//				}
//			}
//		}
//
//		return listFileUploadVO;
//	}
}
