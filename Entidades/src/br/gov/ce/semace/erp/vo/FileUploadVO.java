package br.gov.ce.semace.erp.vo;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;

/**
 * Value Object com os mesmos campos do UploadedFile do Primefaces, assim sendo, evitando todos os projetos terem dependência com o Primefaces
 *
 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 20 de set de 2016 11:09:40
 */
public class FileUploadVO implements Serializable {

	private static final long serialVersionUID = -8651098517006008608L;

	// public abstract java.lang.String getFileName();
	private String fileName;

	// Method descriptor #6 ()Ljava/io/InputStream;
	//public abstract java.io.InputStream getInputstream() throws java.io.IOException;
	private InputStream inputstream;

	// Method descriptor #10 ()J
	//public abstract long getSize();
	private long size;

	// Method descriptor #12 ()[B
	//public abstract byte[] getContents();
	private byte[] contents;

	// Method descriptor #4 ()Ljava/lang/String;
	//public abstract java.lang.String getContentType();
	private String contentType;

	private File tempFile = null;

	/**
	 * @param fileName
	 * @param inputstream
	 * @param size
	 * @param contents
	 * @param contentType
	 */
	public FileUploadVO(String fileName, InputStream inputstream, long size, byte[] contents, String contentType) {
		super();
		this.fileName = fileName;
		this.inputstream = inputstream;
		this.size = size;
		this.contents = contents;
		this.contentType = contentType;
	}

	public FileUploadVO() {
		super();
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public InputStream getInputstream() {
		return inputstream;
	}

	public void setInputstream(InputStream inputstream) {
		this.inputstream = inputstream;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public byte[] getContents() {
		return contents;
	}

	public void setContents(byte[] contents) {
		this.contents = contents;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public File getTempFile() {
		return tempFile;
	}

	public void setTempFile(File tempFile) {
		this.tempFile = tempFile;
	}
}
