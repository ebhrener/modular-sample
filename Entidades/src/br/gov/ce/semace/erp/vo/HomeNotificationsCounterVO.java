package br.gov.ce.semace.erp.vo;

import java.io.Serializable;

public class HomeNotificationsCounterVO implements Serializable {

	private static final long serialVersionUID = 7633897417118072216L;

	private int quantidadePendenciasProcesso;

	private int quantidadePendenciasAgendamento;

	public int getQuantidadePendenciasProcesso() {
		return quantidadePendenciasProcesso;
	}

	public void setQuantidadePendenciasProcesso(int quantidadePendenciasProcesso) {
		this.quantidadePendenciasProcesso = quantidadePendenciasProcesso;
	}

	public int getQuantidadePendenciasAgendamento() {
		return quantidadePendenciasAgendamento;
	}

	public void setQuantidadePendenciasAgendamento(int quantidadePendenciasAgendamento) {
		this.quantidadePendenciasAgendamento = quantidadePendenciasAgendamento;
	}

//	public String getDisplayQuantidadePendenciasProcesso() {
//		if (this.quantidadePendenciasProcesso > 0) {
//			return this.quantidadePendenciasProcesso + " (" + ExtensoUtil.toExtenso(this.quantidadePendenciasProcesso, Boolean.FALSE) + ")";
//		}
//		return "";
//	}
//
//	public String getDisplayQuantidadePendenciasAgendamento() {
//		if (this.quantidadePendenciasAgendamento > 0) {
//			return this.quantidadePendenciasAgendamento + " (" + ExtensoUtil.toExtenso(this.quantidadePendenciasAgendamento, Boolean.FALSE) + ")";
//		}
//		return "";
//	}

}
