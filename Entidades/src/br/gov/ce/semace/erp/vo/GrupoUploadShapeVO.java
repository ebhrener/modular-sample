package br.gov.ce.semace.erp.vo;

import br.gov.ce.semace.erp.enuns.PadraoUploadShape;

public class GrupoUploadShapeVO extends GrupoUploadVO {

	public GrupoUploadShapeVO(String name, PadraoUploadShape padrao) {
		super(name, padrao);
		// TODO Auto-generated constructor stub
	}
/**
	private UploadedFile filePRJ;

	private UploadedFile fileSHP;

	private UploadedFile fileSHX;

	private UploadedFile fileDBF;

	public GrupoUploadShapeVO(String name, PadraoUploadShape padrao, UploadedFile file) {
		super(name, padrao);
		addFile(file);
	}

	public UploadedFile getFilePRJ() {
		return filePRJ;
	}

	public void setFilePRJ(UploadedFile filePRJ) {
		this.filePRJ = filePRJ;
	}

	public UploadedFile getFileSHP() {
		return fileSHP;
	}

	public void setFileSHP(UploadedFile fileSHP) {
		this.fileSHP = fileSHP;
	}

	public UploadedFile getFileSHX() {
		return fileSHX;
	}

	public void setFileSHX(UploadedFile fileSHX) {
		this.fileSHX = fileSHX;
	}

	public UploadedFile getFileDBF() {
		return fileDBF;
	}

	public void setFileDBF(UploadedFile fileDBF) {
		this.fileDBF = fileDBF;
	}

	public boolean hasFileWithExtension(String extension) {
		if (isSHP(extension)) {
			return fileSHP != null;
		} else if (isDBF(extension)) {
			return fileDBF != null;
		} else if (isSHX(extension)) {
			return fileSHX != null;
		} else if (isPRJ(extension)) {
			return filePRJ != null;
		}
		return false;
	}

	public void addFile(UploadedFile file) {

		String extension = getExtension(file.getFileName());

		if (isSHP(extension)) {
			fileSHP = file;
		} else if (isDBF(extension)) {
			fileDBF = file;
		} else if (isSHX(extension)) {
			fileSHX = file;
		} else if (isPRJ(extension)) {
			filePRJ = file;
		}
	}
**/
}
