package br.gov.ce.semace.erp.vo;


public class EspelhoLicenca extends DocumentoProcesso {

	private static final long serialVersionUID = 1L;

	private Boolean renovacao;

	private Boolean regularizacao;

	private String licencaId;

	private Boolean alteracao;

	private String dataEmissao;

	private String dataValidade;

	private String areaNucleoId;

	private String areaNucleoSigla;

	private String textoLicenca;

	private String cliente;

	private String cpfCnpj;

	private String numero;

	private String tipoProcessoId;

	private String complemento;

	private String nome;

	private String cepId;

	private String endereco;

	private String municipio;

	private String ufId;

	private String condicionantes;

	private String condicionantesEspecificos;

	public Boolean getRenovacao() {
		return renovacao;
	}

	public void setRenovacao(Boolean renovacao) {
		this.renovacao = renovacao;
	}

	public Boolean getRegularizacao() {
		return regularizacao;
	}

	public void setRegularizacao(Boolean regularizacao) {
		this.regularizacao = regularizacao;
	}

	public String getLicencaId() {
		return licencaId;
	}

	public void setLicencaId(String licencaId) {
		this.licencaId = licencaId;
	}

	public Boolean getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(Boolean alteracao) {
		this.alteracao = alteracao;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(String dataValidade) {
		this.dataValidade = dataValidade;
	}

	public String getAreaNucleoId() {
		return areaNucleoId;
	}

	public void setAreaNucleoId(String areaNucleoId) {
		this.areaNucleoId = areaNucleoId;
	}

	public String getAreaNucleoSigla() {
		return areaNucleoSigla;
	}

	public void setAreaNucleoSigla(String areaNucleoSigla) {
		this.areaNucleoSigla = areaNucleoSigla;
	}

	public String getTextoLicenca() {
		return textoLicenca;
	}

	public void setTextoLicenca(String textoLicenca) {
		this.textoLicenca = textoLicenca;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getTipoProcessoId() {
		return tipoProcessoId;
	}

	public void setTipoProcessoId(String tipoProcessoId) {
		this.tipoProcessoId = tipoProcessoId;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCepId() {
		return cepId;
	}

	public void setCepId(String cepId) {
		this.cepId = cepId;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getUfId() {
		return ufId;
	}

	public void setUfId(String ufId) {
		this.ufId = ufId;
	}

	public String getCondicionantes() {
		return condicionantes;
	}

	public void setCondicionantes(String condicionantes) {
		this.condicionantes = condicionantes;
	}

	public String getCondicionantesEspecificos() {
		return condicionantesEspecificos;
	}

	public void setCondicionantesEspecificos(String condicionantesEspecificos) {
		this.condicionantesEspecificos = condicionantesEspecificos;
	}

}
