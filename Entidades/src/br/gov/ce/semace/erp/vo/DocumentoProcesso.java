package br.gov.ce.semace.erp.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Classe criada para listar, no Natuur Online (Acompanhamento de Processos), os documentos de cada processo do interessado.
 *
 * Caminho do caso de uso: Natuur-Web > Interessado > Consulta de Processos.
 *
 * @author jonathan
 *
 */
public class DocumentoProcesso implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nomeDocumento;

	private Date dataMovimento;

	private Date dataCadastro;

	private Long gedID;

	private String descricao;

	private String numeroSpu;

	private String nomeArquivo;

	private Integer numeroPaginas;

	private Integer tamanho;

	private Integer pagina;

	private boolean publico = true;

	private boolean possuiDocAnexo = false;

	private Long arquivoDocumentoId;

	public String getNomeDocumento() {
		return nomeDocumento;
	}

	public void setNomeDocumento(String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}

	public Date getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(Date dataMovimento) {
		this.dataMovimento = dataMovimento;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Long getGedID() {
		return gedID;
	}

	public void setGedID(Long gedID) {
		this.gedID = gedID;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNumeroSpu() {
		return numeroSpu;
	}

	public void setNumeroSpu(String numeroSpu) {
		this.numeroSpu = numeroSpu;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}

	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	public Integer getTamanho() {
		return tamanho;
	}

	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}

	public Integer getPagina() {
		return pagina;
	}

	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}

	public boolean isPublico() {
		return publico;
	}

	public void setPublico(boolean publico) {
		this.publico = publico;
	}

	public String getNomeDocumento(int quantidadeCaracteres) {
		if (this.nomeDocumento != null) {
			if (this.nomeDocumento.length() < quantidadeCaracteres) {
				return this.nomeDocumento;
			}

			return nomeDocumento.substring(0, quantidadeCaracteres) + "...";
		}

		return "";
	}

	public String getNomeArquivo(int quantidadeCaracteres) {
		if (this.nomeArquivo != null) {
			if (this.nomeArquivo.length() < quantidadeCaracteres) {
				return this.nomeArquivo;
			}

			return nomeArquivo.substring(0, quantidadeCaracteres) + "...";
		}

		return "";
	}

	public String getDescricao(int quantidadeCaracteres) {
		if (this.descricao != null) {
			if (this.descricao.length() < quantidadeCaracteres) {
				return this.descricao;
			}

			return descricao.substring(0, quantidadeCaracteres) + "...";
		}

		return "";
	}

	public String getDisplayPagina() {
		if (pagina != null) {
			if (numeroPaginas == null || numeroPaginas <= 1) {
				return String.valueOf(pagina);
			}
			return String.valueOf(pagina + " - " + ((pagina + numeroPaginas) - 1));
		}
		return "--";
	}

	public boolean isPossuiDocAnexo() {
		return possuiDocAnexo;
	}

	public void setPossuiDocAnexo(boolean possuiDocAnexo) {
		this.possuiDocAnexo = possuiDocAnexo;
	}

	public Long getArquivoDocumentoId() {
		return arquivoDocumentoId;
	}

	public void setArquivoDocumentoId(Long arquivoDocumentoId) {
		this.arquivoDocumentoId = arquivoDocumentoId;
	}

}