package br.gov.ce.semace.erp.permissao;

import java.util.Comparator;

public class RoleComparator implements Comparator<Role> {

	@Override
	public int compare(Role role1, Role role2) {
		return role1.toString().compareTo(role2.toString());
	}
}
