package br.gov.ce.semace.erp.permissao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public enum Role {

	/************************************************************************ MODULO SEGURANCA ***********************************************/

	/********* SEGURANCA ***********/
	Sistema_Usuarios_Seguranca_Salvar("Segurança", "Usuários","Salvar"),
	Sistema_Usuarios_Seguranca_Excluir("Segurança","Usuários", "Excluir"),
	Sistema_Usuarios_Seguranca_Atualizar("Segurança","Usuários", "Atualizar"),
	Sistema_Usuarios_Seguranca_Listar("Segurança","Usuários", "Listar"),

	Sistema_Usuarios_UsuarioWeb_Salvar("Segurança", "Usuários Web","Salvar"),
	Sistema_Usuarios_UsuarioWeb_Excluir("Segurança","Usuários Web", "Excluir"),
	Sistema_Usuarios_UsuarioWeb_Atualizar("Segurança","Usuários Web", "Atualizar"),
	Sistema_Usuarios_UsuarioWeb_Listar("Segurança","Usuários Web", "Listar"),

	/********* PERFIL ***********/
	Sistema_Usuarios_Perfil_Salvar("Segurança", "Perfil","Salvar"),
	Sistema_Usuarios_Perfil_Excluir("Segurança", "Perfil","Excluir"),
	Sistema_Usuarios_Perfil_Atualizar("Segurança", "Perfil","Atualizar"),
	Sistema_Usuarios_Perfil_Listar("Segurança", "Perfil","Listar"),
	Sistema_Usuarios_Perfil_Remover_Todos("Segurança", "Perfil","Remover Todos Usuários do Perfil"),
	Sistema_Usuarios_Perfil_Adicionar_Todos("Segurança", "Perfil","Adicionar Todos Usuários ao Perfil"),

	/********* AUDITORIA ***********/
	Sistema_Usuarios_Auditoria_Listar("Segurança", "Auditoria","Salvar"),

	/********************************************************************** MODULO DE PATRIMONIO **********************************************/
	Sistema_Patrimonio_Marca_Salvar("Patrimônio", "Marca","Salvar"),
	Sistema_Patrimonio_Marca_Excluir("Patrimônio", "Marca","Excluir"),
	Sistema_Patrimonio_Marca_Atualizar("Patrimônio", "Marca","Atualizar"),
	Sistema_Patrimonio_Marca_Listar("Patrimônio", "Marca","Listar"),


	Sistema_Patrimonio_Tipo_Produto_Salvar("Patrimônio", "Tipo de Produto","Salvar"),
	Sistema_Patrimonio_Tipo_Produto_Excluir("Patrimônio", "Tipo de Produto","Excluir"),
	Sistema_Patrimonio_Tipo_Produto_Atualizar("Patrimônio", "Tipo de Produto","Atualizar"),
	Sistema_Patrimonio_Tipo_Produto_Listar("Patrimônio", "Tipo de Produto","Listar"),

	/********************************************************************** MODULO DE ATENDIMENTO **********************************************/

	/********* TIRA DUVIDA ***********/
	Sistema_Atendimento_Tira_Duvida_Salvar("Atendimento", "Tira Dúvidas","Salvar"),
	Sistema_Atendimento_Tira_Duvida_Excluir("Atendimento", "Tira Dúvidas","Excluir"),
	Sistema_Atendimento_Tira_Duvida_Atualizar("Atendimento", "Tira Dúvidas","Atualizar"),
	Sistema_Atendimento_Tira_Duvida_Listar("Atendimento", "Tira Dúvidas","Listar"),

	/********* TIPO ATENDIMENTO ***********/
	Sistema_Atendimento_Tipo_Atendimento_Salvar("Atendimento", "Tipo de Atendimento","Salvar"),
	Sistema_Atendimento_Tipo_Atendimento_Excluir("Atendimento", "Tipo de Atendimento","Excluir"),
	Sistema_Atendimento_Tipo_Atendimento_Atualizar("Atendimento", "Tipo de Atendimento","Atualizar"),
	Sistema_Atendimento_Tipo_Atendimento_Listar("Atendimento", "Tipo de Atendimento","Listar"),

	/********* PRESENCIAL ***********/
	Sistema_Atendimento_Presencial_Listar("Atendimento", "Presencial","Listar Atendimentos"),
	Sistema_Atendimento_Presencial_Ver("Atendimento", "Presencial","Visualizar Detalhes do Atendimento"),
	Sistema_Atendimento_Presencial_inicio_atendimento("Atendimento", "Presencial","Inicializar Atendimento"),
	Sistema_Atendimento_Presencial_Continuar_Atendimento("Atendimento", "Presencial","Continuar Atendimento"),
	Sistema_Atendimento_Presencial_Cancelar("Atendimento", "Presencial","Cancelar Atendimento"),
	Sistema_Atendimento_Presencial_Validar_Documentacao("Atendimento","Presencial","Validar Documentação"),
	Sistema_Atendimento_Presencial_Devolver_Agendamento("Atendimento", "Presencial","Devolver Agendamento para a Atendente"),
	Sistema_Atendimento_Presencial_Validar_Shape("Atendimento","Presencial","Validar Arquivos Shape"),
	Sistema_Atendimento_Presencial_Gerar_Pre_Processo("Atendimento","Presencial","Geração de Processo Provisório"),
	Sistema_Atendimento_Presencial_Triagem("Atendimento","Presencial","Triagem"),
	Sistema_Atendimento_Relatorios_Visualizar("Atendimento","Relatórios","Visualizar Relatórios Atendimento"),
	Sistema_Atendimento_Presencial_Priorizar_Agendamento("Atendimento", "Presencial", "Priorizar Agendamento"),
	Sistema_Atendimento_Presencial_Ver_Todos_Requerimentos_Das_Sedes("Atendimento", "Presencial", "Ver Todos Requerimentos das Sedes"),
	Sistema_Atendimento_Presencial_Voltar_Requerimento_Triagem("Atendimento", "Presencial", "Voltar Requerimento Para a Triagem"),

	Sistema_Atendimento_Geracao_dae_Editar_Letra("Atendimento","Geração de Dae","Alterar Parâmetros da Letra"),

	/********* ARQUIVO ***********/
	Sistema_Arquivo_UploadShapes_Listar("Arquivo", "Shapes","Listar"),
	Sistema_Arquivo_UploadShapes_Salvar("Arquivo", "Shapes","Salvar"),
	Sistema_Arquivo_UploadShapes_Original_Salvar("Arquivo", "Shapes","Salvar Shapes Originais"),

	/********* AGENDAMENTO ***********/
	Sistema_Atendimento_Agendamento_Requerimento_Salvar("Atendimento", "Agendamento","Salvar"),
	Sistema_Atendimento_Agendamento_Requerimento_Atualizar("Atendimento", "Agendamento","Atualizar"),
	Sistema_Atendimento_Agendamento_Requerimento_Excluir("Atendimento", "Agendamento","Excluir"),
	Sistema_Atendimento_Agendamento_Requerimento_Listar("Atendimento", "Agendamento","Listar"),
	Sistema_Atendimento_Agendamento_Requerimento_Imprimir("Atendimento","Agendamento","Imprimir Requerimento"),
	Sistema_Atendimento_Agendamento_Requerimento_Imprimir_Checklist("Atendimento","Agendamento","Imprimir Checklist"),
	Sistema_Atendimento_Agendamento_Requerimento_Confirmar_Localizacao("Atendimento","Agendamento","Confirmar Localizacao"),
	Sistema_Atendimento_Agendamento_Requerimento_Atualizar_Agendamento_Em_Qualquer_Fase("Atendimento", "Agendamento","Atualizar Agendamento em Qualquer Fase"),
	Sistema_Atendimento_Agendamento_Cancelar_Requerimento("Atendimento", "Agendamento","Cancelar Requerimento"),
	Sistema_Atendimento_Agendamento_Requerimento_Triagem_cancelar_dae("Atendimento", "Agendamento","Cancelar Dae na triagem"),
	/********* UPLOAD ***********/
	Sistema_Atendimento_Upload_Listar("Atendimento", "Arquivos","Listar Arquivos Enviados"),
	Sistema_Atendimento_Upload_Salvar("Atendimento", "Arquivos","Salvar Arquivos"),

	/********* DAE ***********/
	Sistema_Atendimento_Presencial_Dae_Gerar("Atendimento", "DAE","Gerar DAE"),
	Sistema_Atendimento_Presencial_Dae_Gerar_Segunda_Via("Atendimento", "DAE","Gerar Segunda Via de DAE"),
	Sistema_Atendimento_Presencial_Dae_Calcular_Valor("Atendimento", "DAE", "Calcular Valor do DAE"),
	Sistema_Atendimento_Presencial_Dae_Imprimir("Atendimento", "DAE", "Imprimir DAE"),
	Sistema_Atendimento_Presencial_Dae_Excluir("Atendimento", "DAE","Excluir DAE"),
	Sistema_Atendimento_Presencial_Dae_Excluir_Vencido("Atendimento", "DAE","Excluir DAE Vencido"),
	Sistema_Atendimento_Presencial_Dae_Listar("Atendimento", "DAE", "Listar DAE"),

	/******** PENDÊNCIAS ***********/
	Sistema_Atendimento_Pendencia_Autorizar_Pendencias("Atendimento", "Pendência","Autorizar Documentação Pendente"),
	Sistema_Atendimento_Pendencia_Add_Atendimento("Atendimento","Pendência","Adicionar Pendência Documentação"),
	Sistema_Atendimento_Pendencia_Add_Financeiro("Atendimento","Pendência", "Adicionar Pendência Financeiro"),
	Sistema_Atendimento_Pendencia_Remove_Documentacao("Atendimento","Pendência","Remover Pendência Documentação"),
	Sistema_Atendimento_Pendencia_Remove("Atendimento", "Pendência", "Remover Pendência"),
	Sistema_Atendimento_Pendencia_Gerenciar("Atendimento", "Pendência", "Gerenciar Pendência"),

	/********* RELATORIO DAE ***********/
	Sistema_Atendimento_Relatorio_Dae_Listar("Atendimento", "Relatório de DAE","Listar"),

	/********* PAINEL ATENDIMENTO ***********/
	Sistema_Atendimento_Painel_Requerimentos_Diarios("Atendimento", "Painel Atendimento", "Requerimentos Diários"),
	Sistema_Atendimento_Painel_Requerimentos_Atrasados("Atendimento", "Painel Atendimento", "Requerimentos Atrasados"),
	Sistema_Atendimento_Painel_Requerimentos_Triagem("Atendimento", "Painel Atendimento", "Requerimentos em Triagem"),
	Sistema_Atendimento_Painel_Requerimentos_Aguardando_Validacao_Documentos("Atendimento", "Painel Atendimento", "Requerimentos Aguardando Validação de Documentos"),
	Sistema_Atendimento_Painel_Requerimentos_Aguardando_Validacao_Shape("Atendimento", "Painel Atendimento", "Requerimentos Aguardando Validação de Shape"),
	Sistema_Atendimento_Painel_Requerimentos_Aguardando_Validacao_Pendencia_Documentacao("Atendimento", "Painel Atendimento", "Requerimentos Aguardando Validação de Pendências de Documentação"),
	Sistema_Atendimento_Painel_Requerimentos_Aguardando_Validacao_Pendencia_Shape("Atendimento", "Painel Atendimento", "Requerimentos Aguardando Validação de Pendências de Shape"),
	Sistema_Atendimento_Painel_Ultimos_Requerimentos("Atendimento", "Painel Atendimento","Últimos Requerimentos"),
	Sistema_Atendimento_Painel_Requerimentos_Futuros("Atendimento","Painel Atendimento","Requerimentos Futuro"),
	Sistema_Atendimento_Painel_Requerimentos_Prioritarios("Atendimento","Painel Atendimento","Requerimentos Prioritários"),
	Sistema_Atendimento_Painel_Requerimentos_Latest_Updated("Atendimento", "Painel Atendimento", "Últimos Atualizados"),


	/********************************************************************** Florestal **********************************************/

	/********* CLASSIFICACAO ***********/
	Sistema_Florestal_Classificacao_Listar("Florestal","Classificação","Listar"),
	Sistema_Florestal_Classificacao_Salvar("Florestal","Classificação","Salvar"),
	Sistema_Florestal_Classificacao_Atualizar("Florestal","Classificação","Atualizar"),

	Sistema_Florestal_Produto_Listar("Florestal","Produto","Listar"),
	Sistema_Florestal_Produto_Salvar("Florestal","Produto","Salvar"),
	Sistema_Florestal_Produto_Atualizar("Florestal","Produto","Atualizar"),

	Sistema_Florestal_Producao_Listar("Florestal","Produção","Listar"),
	Sistema_Florestal_Producao_Salvar("Florestal","Produção","Salvar"),
	Sistema_Florestal_Producao_Atualizar("Florestal","Produção","Atualizar"),

	Sistema_Florestal_Tipologia_Listar("Florestal","Tipologia","Listar"),
	Sistema_Florestal_Tipologia_Salvar("Florestal","Tipologia","Salvar"),
	Sistema_Florestal_Tipologia_Atualizar("Florestal","Tipologia","Atualizar"),

	/********* ATIVIDADE FLORESTAL ***********/
	Sistema_Florestal_Atividade_Florestal_Listar("Florestal","Atividade Florestal","Listar"),
	Sistema_Florestal_Atividade_Florestal_Salvar("Florestal","Atividade Florestal","Salvar"),
	Sistema_Florestal_Atividade_Florestal_Atualizar("Florestal","Atividade Florestal","Atualizar"),

	/********* CONSUMO ***********/
	Sistema_Florestal_Consumo_Listar("Florestal","Consumo","Listar"),
	Sistema_Florestal_Consumo_Salvar("Florestal","Consumo","Salvar"),
	Sistema_Florestal_Consumo_Atualizar("Florestal","Consumo","Atualizar"),

	Sistema_Florestal_Dados_Consumo_Listar("Florestal","Dados Consumo","Listar"),
	Sistema_Florestal_Dados_Consumo_Salvar("Florestal","Dados Consumo","Salvar"),
	Sistema_Florestal_Dados_Consumo_Atualizar("Florestal","Dados Consumo","Atualizar"),

	/********* CADASTRO DE CONSUMO FLORESTAL***********/
	Sistema_Florestal_Cadastro_Consumo_Listar("Florestal","Cadastro Consumo","Listar"),
	Sistema_Florestal_Cadastro_Consumo_Salvar("Florestal","Cadastro Consumo","Salvar"),
	Sistema_Florestal_Cadastro_Consumo_Atualizar("Florestal","Cadastro Consumo","Atualizar"),
	Sistema_Florestal_Cadastro_Consumo_Visualizar("Florestal","Cadastro Consumo","Visualizar"),
	Sistema_Florestal_Cadastro_Consumo_Cancelamento("Florestal","Cadastro Consumo","Solicitar Cancelamento"),
	Sistema_Florestal_Cadastro_Consumo_Homologacao_Cancelamento("Florestal","Cadastro Consumo","Homologar Cancelamento"),
	Sistema_Florestal_Cadastro_Consumo_Imprimir_Comprovante_Cancelamento("Florestal","Cadastro Consumo","Imprimir Comprovante Cancelamento"),
	Sistema_Florestal_Cadastro_Consumo_Visualizar_Historico("Florestal","Cadastro Consumo","Visualizar Histórico"),
	Sistema_Florestal_Cadastro_Consumo_Cancelamento_Solicitacao_Mensagem("Florestal","Cadastro Consumo","Mensagem Solicitação Cancelamento"),
	Sistema_Florestal_Cadastro_Consumo_Imprimir_Dae("Florestal","Cadastro Consumo","Imprimir Dae"),
	Sistema_Florestal_Relatorios("Florestal","Cadastro Consumo", "Gerar Relatórios"),
	Sistema_Florestal_Relatorio_Arrecadacao_Cadastro_Consumo("Florestal","Cadastro Consumo", "Relatório Arrecadação Cadastro Consumo"),
	Sistema_Florestal_Relatorio_Cadastro_Consumo_Vencidos_Sem_Renovacao("Florestal","Cadastro Consumo", "Relatório Cadastro Consumo Vencidos Sem Renovação"),
	Sistema_Florestal_Relatorio_Cadastro_Consumo_Florestal("Florestal","Cadastro Consumo", "Relatório de Cadastro de Consumo Florestal"),
	Sistema_Florestal_Cadastro_Consumo_Atualizar_Inscricao_Estadual("Florestal","Cadastro Consumo","Atualizar Inscrição Estadual"),

	/********* CONFIGURAÇÃO FLORESTAL***********/
	Sistema_Florestal_Configuracao_Salvar("Florestal","Configuração","Salvar"),

	/********* CADASTRO DE USUARIO CAR***********/
	Sistema_Florestal_CAR_Usuario_CAR_Listar("CAR", "Usuário CAR", "Listar"),
	Sistema_Florestal_CAR_Usuario_CAR_Salvar("CAR", "Usuário CAR", "Salvar"),
	Sistema_Florestal_CAR_Usuario_CAR_Atualizar("CAR", "Usuário CAR", "Atualizar"),

	/********* VINCULO DE PROJETO SINAFLOR ***********/
	Sistema_Florestal_SINAFLOR_Vincular_Projeto_Listar("SINAFLOR", "Vínculo Projeto", "Listar"),
	Sistema_Florestal_SINAFLOR_Vincular_Projeto_Salvar("SINAFLOR", "Vínculo Projeto", "Salvar"),

	/********************************************************************** TRANSPORTE **********************************************/

	/********* ESPECIE DE TRANSPORTE ***********/
	Sistema_Transporte_Especie_Transporte_Salvar("Transporte", "Espécie de Transporte","Salvar"),
	Sistema_Transporte_Especie_Transporte_Excluir("Transporte", "Espécie de Transporte","Excluir"),
	Sistema_Transporte_Especie_Transporte_Atualizar("Transporte", "Espécie de Transporte","Atualizar"),
	Sistema_Transporte_Especie_Transporte_Listar("Transporte", "Espécie de Transporte","Listar"),

	/********* VEICULO ***********/
	Sistema_Transporte_Veiculo_Salvar("Transporte", "Veículo","Salvar"),
	Sistema_Transporte_Veiculo_Excluir("Transporte", "Veículo","Excluir"),
	Sistema_Transporte_Veiculo_Atualizar("Transporte", "Veículo","Atualizar"),
	Sistema_Transporte_Veiculo_Listar("Transporte", "Veículo","Listar"),

	/********* ORDEM TRANSPORTE ***********/
	Sistema_Transporte_Ordem_Transporte_Salvar("Transporte", "Ordem de Transporte","Salvar"),
	Sistema_Transporte_Ordem_Transporte_Excluir("Transporte", "Ordem de Transporte","Excluir"),
	Sistema_Transporte_Ordem_Transporte_Atualizar("Transporte", "Ordem de Transporte","Atualizar"),
	Sistema_Transporte_Ordem_Transporte_Listar("Transporte", "Ordem de Transporte","Listar"),
	Sistema_Transporte_Ordem_Transporte_Listar_Todas_Sedes("Transporte", "Ordem de Transporte","Listar de Todas as Sedes"),
	Sistema_Transporte_Ordem_Transporte_Cancelar("Transporte", "Ordem de Transporte","Cancelar"),
	Sistema_Transporte_Ordem_Transporte_Iniciar("Transporte", "Ordem de Transporte","Iniciar"),
	Sistema_Transporte_Ordem_Transporte_Finalizar("Transporte", "Ordem de Transporte","Finalizar"),
	Sistema_Transporte_Ordem_Transporte_Substituir_Motorista("Transporte", "Ordem de Transporte","Substituir Motorista"),
	Sistema_Transporte_Ordem_Transporte_Tornar_Motorista("Transporte", "Ordem de Transporte","Tornar Motorista"),
	Sistema_Transporte_Ordem_Transporte_Mensageiria_Aguardando_Motorista("Transporte", "Ordem de Transporte", "Mensageiria Aguardando Motorista"),
	Sistema_Transporte_Ordem_Transporte_Mensageiria_Aguardando_Transporte("Transporte", "Ordem de Transporte", "Mensageiria Aguardando Transporte"),
	Sistema_Transporte_Ordem_Transporte_Retornar_Status("Transporte", "Ordem de Transporte", "Retornar Status"),

	/********* SOLICITACAO DE TRANSPORTE ***********/
	Sistema_Transporte_Solicitacao_Transporte_Listar("Transporte","Solicitação de Transporte","Listar"),
	Sistema_Transporte_Solicitacao_Transporte_Salvar("Transporte","Solicitação de Transporte","Salvar"),
	Sistema_Transporte_Solicitacao_Transporte_Atualizar("Transporte","Solicitação de Transporte","Atualizar"),
	Sistema_Transporte_Solicitacao_Transporte_Visualizar_Setor_Pai("Transporte","Solicitação de Transporte","Visualizar Solicitações do Setor Pai"),
	Sistema_Transporte_Solicitacao_Transporte_Aprovar("Transporte","Solicitação de Transporte","Aprovar"),
	Sistema_Transporte_Solicitacao_Transporte_Atualizar_RH("Transporte","Solicitação de Transporte","Atualizar pelo RH"),
	Sistema_Transporte_Solicitacao_Transporte_Iniciar("Transporte","Solicitação de Transporte","Iniciar Solicitação"),
	Sistema_Transporte_Solicitacao_Transporte_Finalizar("Transporte","Solicitação de Transporte","Finalizar Solicitação"),
	Sistema_Transporte_Solicitacao_Transporte_Imprimir("Transporte","Solicitação de Transporte","Imprimir Solicitação"),

	/********* TIPO DE OBJETIVO ***********/
	Sistema_Transporte_Tipo_Objetivo_Listar("Transporte","Tipo de Objetivo","Listar"),
	Sistema_Transporte_Tipo_Objetivo_Salvar("Transporte","Tipo de Objetivo","Salvar"),
	Sistema_Transporte_Tipo_Objetivo_Atualizar("Transporte","Tipo de Objetivo","Atualizar"),

	/********* CALENDÁRIO DE MOTORISTAS *********/
	Sistema_Transporte_Calendario_Motorista_Listar("Transporte","Calendario de Motoristas","Listar"),

	/*********************************************************************** MODULO RH  **********************************************************/

	/********* ORGAO ***********/
	Sistema_RH_Orgao_Excluir("RH", "Orgão","Excluir"),
	Sistema_RH_Orgao_Atualizar("RH", "Orgão","Atualizar"),
	Sistema_RH_Orgao_Salvar("RH", "Orgão","Salvar"),
	Sistema_RH_Orgao_Listar("RH", "Orgão","Listar"),

	/********* ESCOLARIDADE ***********/
	Sistema_RH_Escolaridade_Salvar("RH", "Escolaridade","Salvar"),
	Sistema_RH_Escolaridade_Excluir("RH", "Escolaridade","Excluir"),
	Sistema_RH_Escolaridade_Atualizar("RH", "Escolaridade","Atualizar"),
	Sistema_RH_Escolaridade_Listar("RH", "Escolaridade","Listar"),

	/********* FORMACAO PROFISSIONAL ***********/
	Sistema_RH_FormacaoProfissional_Salvar("RH","Formação Profissional","Salvar"),
	Sistema_RH_FormacaoProfissional_Excluir("RH","Formação Profissional","Excluir"),
	Sistema_RH_FormacaoProfissional_Atualizar("RH","Formação Profissional","Atualizar"),
	Sistema_RH_FormacaoProfissional_Listar("RH","Formação Profissional","Listar"),

	/********* SETOR ***********/
	Sistema_RH_Setor_Salvar("RH", "Setor","Salvar"),
	Sistema_RH_Setor_Atualizar("RH", "Setor","Atualizar"),
	Sistema_RH_Setor_Excluir("RH", "Setor","Excluir"),
	Sistema_RH_Setor_Listar("RH", "Setor","Listar"),

	/********* FUNCIONARIO ***********/
	Sistema_RH_Funcionario_Salvar("RH","Funcionário","Salvar"),
	Sistema_RH_Funcionario_Atualizar("RH","Funcionário","Atualizar"),
	Sistema_RH_Funcionario_Excluir("RH","Funcionário","Excluir"),
	Sistema_RH_Funcionario_Listar("RH","Funcionário","Listar"),

	/********* FUNCIONARIO ***********/
	Sistema_RH_Advogado_Salvar("RH","Advogado","Salvar"),
	Sistema_RH_Advogado_Atualizar("RH","Advogado","Atualizar"),
	Sistema_RH_Advogado_Excluir("RH","Advogado","Excluir"),
	Sistema_RH_Advogado_Listar("RH","Advogado","Listar"),

	/********* CARGO ***********/
	Sistema_RH_Cargo_Excluir("RH", "Cargo","Excluir"),
	Sistema_RH_Cargo_Atualizar("RH", "Cargo","Atualizar"),
	Sistema_RH_Cargo_Salvar("RH", "Cargo","Salvar"),
	Sistema_RH_Cargo_Listar("RH", "Cargo","Listar"),

	/********* CONFIGURACAO ***********/
	Sistema_RH_Configuracao_Salvar("RH", "Configuracao","Salvar"),

	/********* DIÁRIA ***********/
	Sistema_RH_Diaria_Excluir("RH","Diária","Excluir"),
	Sistema_RH_Diaria_Atualizar("RH","Diária","Atualizar"),
	Sistema_RH_Diaria_Salvar("RH","Diária","Salvar"),
	Sistema_RH_Diaria_Listar("RH","Diária","Listar"),

	/********* FUNCAO ***********/
	Sistema_RH_Funcao_Excluir("RH","Função","Excluir"),
	Sistema_RH_Funcao_Atualizar("RH","Função","Atualizar"),
	Sistema_RH_Funcao_Salvar("RH","Função","Salvar"),
	Sistema_RH_Funcao_Listar("RH","Função","Listar"),

	/********* Vínculo Empregatício ***********/
	Sistema_RH_Vinculo_Excluir("RH","Vínculo","Excluir"),
	Sistema_RH_Vinculo_Atualizar("RH","Vínculo","Atualizar"),
	Sistema_RH_Vinculo_Salvar("RH","Vínculo","Salvar"),
	Sistema_RH_Vinculo_Listar("RH","Vínculo","Listar"),

	/********* FERIAS ***********/
	Sistema_RH_Ferias_Excluir("RH", "Ferias","Excluir"),
	Sistema_RH_Ferias_Atualizar("RH", "Ferias","Atualizar"),
	Sistema_RH_Ferias_Salvar("RH", "Ferias","Salvar"),
	Sistema_RH_Ferias_Listar("RH", "Ferias","Listar"),

	/********* Tipo de Comissão ***********/
	Sistema_RH_TipoComissao_Excluir("RH","Tipo de Comissão","Excluir"),
	Sistema_RH_TipoComissao_Atualizar("RH","Tipo de Comissão","Atualizar"),
	Sistema_RH_TipoComissao_Salvar("RH","Tipo de Comissão","Salvar"),
	Sistema_RH_TipoComissao_Listar("RH","Tipo de Comissão","Listar"),

	/********* Usufruir Ferias ***********/
	Sistema_RH_UsufruirFerias_Excluir("RH","Usufruir Ferias","Excluir"),
	Sistema_RH_UsufruirFerias_Atualizar("RH","Usufruir Ferias","Atualizar"),
	Sistema_RH_UsufruirFerias_Salvar("RH","Usufruir Ferias","Salvar"),
	Sistema_RH_UsufruirFerias_Listar("RH","Usufruir Ferias","Listar"),

	/********* Situação do Funcionario ***********/
	Sistema_RH_Situacao_Funcionario_Excluir("RH","Situação Funcionário","Excluir"),
	Sistema_RH_Situacao_Funcionario_Atualizar("RH","Situação Funcionário","Atualizar"),
	Sistema_RH_Situacao_Funcionario_Salvar("RH","Situação Funcionário","Salvar"),
	Sistema_RH_Situacao_Funcionario_Listar("RH","Situação Funcionário","Listar"),

	/********* Nível Hierárquico ***********/
	Sistema_RH_Nivel_Hieraquico_Excluir("RH","Nível Hierárquico","Excluir"),
	Sistema_RH_Nivel_Hieraquico_Atualizar("RH","Nível Hierárquico","Atualizar"),
	Sistema_RH_Nivel_Hieraquico_Salvar("RH","Nível Hierárquico","Salvar"),
	Sistema_RH_Nivel_Hieraquico_Listar("RH","Nível Hierárquico","Listar"),

	/******************* RELATÓRIOS *****************/
	Sistema_RH_Relatorio_Gerar("RH","Relatório","Gerar"),

	/********* EMPRESA ***********/
	Sistema_RH_Empresa_Excluir("RH", "Empresa","Excluir"),
	Sistema_RH_Empresa_Atualizar("RH", "Empresa","Atualizar"),
	Sistema_RH_Empresa_Salvar("RH", "Empresa","Salvar"),
	Sistema_RH_Empresa_Listar("RH", "Empresa","Listar"),

	/********* SINDICATO ***********/
	Sistema_RH_Sindicato_Excluir("RH", "Sindicato","Excluir"),
	Sistema_RH_Sindicato_Atualizar("RH", "Sindicato","Atualizar"),
	Sistema_RH_Sindicato_Salvar("RH", "Sindicato","Salvar"),
	Sistema_RH_Sindicato_Listar("RH", "Sindicato","Listar"),

	/********* NIVEL ***********/
	Sistema_RH_Nivel_Vinculacao_Excluir("RH", "Nivel Vinculacao","Excluir"),
	Sistema_RH_Nivel_Vinculacao_Atualizar("RH", "Nivel Vinculacao","Atualizar"),
	Sistema_RH_Nivel_Vinculacao_Salvar("RH", "Nivel Vinculacao","Salvar"),
	Sistema_RH_Nivel_Vinculacao_Listar("RH", "Nivel Vinculacao","Listar"),

	/********* NIVEL ***********/
	Sistema_RH_Habilitacao_Excluir("RH", "Habilitacao","Excluir"),
	Sistema_RH_Habilitacao_Atualizar("RH", "Habilitacao","Atualizar"),
	Sistema_RH_Habilitacao_Salvar("RH", "Habilitacao","Salvar"),
	Sistema_RH_Habilitacao_Listar("RH", "Habilitacao","Listar"),

	/********* MUNICIPIO ACRESCIMO ***********/
	Sistema_RH_Municipio_Acrescimo_Excluir("RH", "Município Acrescimo","Excluir"),
	Sistema_RH_Municipio_Acrescimo_Atualizar("RH", "Município Acrescimo","Atualizar"),
	Sistema_RH_Municipio_Acrescimo_Salvar("RH", "Município Acrescimo","Salvar"),
	Sistema_RH_Municipio_Acrescimo_Listar("RH", "Município Acrescimo","Listar"),

	/********* MUNICIPIO SEM DIARIA **********/
	Sistema_RH_Municipio_Sem_Diaria_Listar("RH", "Município Sem Diária","Listar"),
	Sistema_RH_Municipio_Sem_Diaria_Salvar("RH", "Município Sem Diária","Salvar"),
	Sistema_RH_Municipio_Sem_Diaria_Atualizar("RH", "Município Sem Diária","Atualizar"),
	Sistema_RH_Municipio_Sem_Diaria_Excluir("RH", "Município Sem Diária","Excluir"),

	/********* REGISTRO DE PONTO **********/
	Sistema_RH_Registro_Ponto_Listar("RH", "Registro de Ponto","Listar"),
	Sistema_RH_Registro_Ponto_Salvar("RH", "Registro de Ponto","Salvar"),
	Sistema_RH_Registro_Ponto_Atualizar("RH", "Registro de Ponto","Atualizar"),
	Sistema_RH_Registro_Ponto_Excluir("RH", "Registro de Ponto","Excluir"),
	Sistema_RH_Justificativa_Aprovar("RH", "Justificativa","Aprovar"),
	Sistema_RH_Justificativa_Excluir("RH", "Justificativa","Excluir"),
	Sistema_RH_Justificativa_Salvar("RH", "Justificativa","Salvar"),
	Sistema_RH_Banco_Hora_Transferir("RH","Banco de Horas","Atualizar"),

	/********************************************************************* MODULO FINANCEIRO  ***************************************************/

	/********* IMPRESSÃO ************/
	Sistema_Financeiro_Impressao_Dae("Financeiro", "Impressão", "Imprimir Dae"),

	/********* UFIRCE ***********/
	Sistema_Financeiro_Ufirce_Salvar("Financeiro", "UFIR-CE","Salvar"),
	Sistema_Financeiro_Ufirce_Excluir("Financeiro", "UFIR-CE","Excluir"),
	Sistema_Financeiro_Ufirce_Atualizar("Financeiro", "UFIR-CE","Atualizar"),
	Sistema_Financeiro_Ufirce_Listar("Financeiro", "UFIR-CE","Listar"),

	/********* LETRA ATIVIDADE ***********/
	Sistema_Financeiro_Letra_Atividade_Salvar("Financeiro", "Letra por Atividade","Salvar"),
	Sistema_Financeiro_Letra_Atividade_Excluir("Financeiro", "Letra por Atividade","Excluir"),
	Sistema_Financeiro_Letra_Atividade_Listar("Financeiro", "Letra por Atividade","Listar"),

	/********* CONFIGURAÇÃO DAE ***********/
	Sistema_Financeiro_Configuracao_Dae_Salvar("Financeiro", "Configuracao DAE","Salvar"),
	Sistema_Financeiro_Configuracao_Dae_Atualizar("Financeiro", "Configuracao DAE","Atualizar"),
	Sistema_Financeiro_Configuracao_Dae_Listar("Financeiro", "Configuracao DAE","Listar"),

	/********* SOLICITAÇÃO SERVIÇO LABORATORIAL ***********/
	Sistema_Financeiro_Solicitacao_Servico_Laboratorial_Salvar("Financeiro", "Solicitação Serviço Laboratorial","Salvar"),
	Sistema_Financeiro_Solicitacao_Servico_Laboratorial_Excluir("Financeiro", "Solicitação Serviço Laboratorial","Excluir"),
	Sistema_Financeiro_Solicitacao_Servico_Laboratorial_Atualizar("Financeiro", "Solicitação Serviço Laboratorial","Atualizar"),
	Sistema_Financeiro_Solicitacao_Servico_Laboratorial_Listar("Financeiro", "Solicitação Serviço Laboratorial","Listar"),

	/********* CONSULTA DAE ***********/
	Sistema_Financeiro_Consulta_Dae_Listar("Financeiro", "Consulta Dae","Listar"),
	Sistema_Financeiro_Relatorio_Dae("Financeiro", "Relatório", "Relatório de DAE"),
	Sistema_Financeiro_Visualizar_Usuario_Gerou_Dae("Financeiro","Visualizar","Visualizar Usuário Gerou Dae"),
	Sistema_Financeiro_Ressarcir("Financeiro","Ressarcir","Ressarcir Dae"),

	/********* EMISSÃO DE TAXAS ***********/
	Sistema_Financeiro_Emissao_Taxas_Listar("Financeiro", "Emissão de Taxas","Listar"),
	Sistema_Financeiro_Emissao_Taxas_Salvar_Fumaca("Financeiro", "Emissão de Taxas","Emitir Taxas Fumaça"),

	/********* TIPO TAXA ***********/
	Sistema_Financeiro_TipoTaxa_Salvar("Financeiro", "Tipo Taxa","Salvar"),
	Sistema_Financeiro_TipoTaxa_Excluir("Financeiro", "Tipo Taxa","Excluir"),
	Sistema_Financeiro_TipoTaxa_Atualizar("Financeiro", "Tipo Taxa","Atualizar"),
	Sistema_Financeiro_TipoTaxa_Listar("Financeiro", "Tipo Taxa","listar"),

	/********* TIPO SERVIÇO ***********/
	Sistema_Financeiro_Tipo_Produto_Servico_Salvar("Financeiro", "Tipo de Serviço","Salvar"),
	Sistema_Financeiro_Tipo_Produto_Servico_Excluir("Financeiro", "Tipo de Serviço","Excluir"),
	Sistema_Financeiro_Tipo_Produto_Servico_Atualizar("Financeiro", "Tipo de Serviço","Atualizar"),
	Sistema_Financeiro_Tipo_Produto_Servico_Listar("Financeiro", "Tipo de Serviço","listar"),

	/********* TIPO RECEITA ***********/
	Sistema_Financeiro_Tipo_Receita_Salvar("Financeiro", "Tipo de Receita","Salvar"),
	Sistema_Financeiro_Tipo_Receita_Excluir("Financeiro", "Tipo de Receita","Excluir"),
	Sistema_Financeiro_Tipo_Receita_Atualizar("Financeiro", "Tipo de Receita","Atualizar"),
	Sistema_Financeiro_Tipo_Receita_Listar("Financeiro", "Tipo de Receita","listar"),

	/********* CONFIGURAÇÃO VALOR UFIRCE ***********/
	Sistema_Financeiro_Configuracao_Valor_Ufirce_Salvar("Financeiro", "Configuração Valor Ufirce","Salvar"),
	Sistema_Financeiro_Configuracao_Valor_Ufirce_Excluir("Financeiro", "Configuração Valor Ufirce","Excluir"),
	Sistema_Financeiro_Configuracao_Valor_Ufirce_Atualizar("Financeiro", "Configuração Valor Ufirce","Atualizar"),
	Sistema_Financeiro_Configuracao_Valor_Ufirce_Listar("Financeiro", "Configuração Valor Ufirce","listar"),

	/********* CONTA ***********/
	Sistema_Financeiro_Conta_Salvar("Financeiro", "Conta", "Salvar"),
	Sistema_Financeiro_Conta_Desativar("Financeiro", "Conta", "Desativar"),
	Sistema_Financeiro_Conta_Atualizar("Financeiro", "Conta", "Atualizar"),
	Sistema_Financeiro_Conta_Listar("Financeiro", "Conta", "Listar"),
	Sistema_Financeiro_Conta_Liquidar("Financeiro", "Conta", "Liquidar"),
	Sistema_Financeiro_Conta_Receber("Financeiro", "Conta", "Receber"),

	/********* HABILITAR ALTERAÇÃO DAE ***********/
	Sistema_Financeiro_Alteracao_Dae_Habilitar_Alteracao("Financeiro", "Alteração DAE", "Habilitar Alteração"),
	Sistema_Financeiro_Alteracao_Dae_Alterar("Financeiro", "Alteração DAE", "Alterar"),

	/**************VINCULAR BOLETO**************/
	Sistema_Financeiro_Vincular_Deposito("Financeiro", "Vinculacao Deposito", "Vincular"),

	/********* CALCULADORA ***********/
	Sistema_Financeiro_Calculadora("Financeiro", "Calculadora", "Calcular"),
	Sistema_Financeiro_Calculadora_Impressao("Financeiro", "Calculadora", "Imprimir"),

	/********* EMISSÃO DE TAXA *********/
	Sistema_Financeiro_Emissao_Taxa_Visualizar("Financeiro", "Emissao Taxa", "Visualizar"),
	Sistema_Financeiro_Emissao_Taxa_Imprimir_DAE("Financeiro", "Emissao Taxa", "Imprimir DAE"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_DAE("Financeiro", "Emissao Taxa", "Gerar DAE"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_DAE_AIF("Financeiro", "Emissao Taxa", "Gerar DAE de Auto de Infração"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_DAE_PARCELAMENTO_AIF("Financeiro", "Emissao Taxa", "Gerar DAE de Parcelamento de Auto de Infração"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_DAE_COMPENSACAO("Financeiro", "Emissao Taxa", "Gerar DAE de Compensação"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_DAE_BTZ_FUMACA("Financeiro", "Emissao Taxa", "Gerar DAE de Blitz de Fumaça Negra"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_DAE_ANALISE_LABORATORIAL("Financeiro", "Emissao Taxa", "Gerar DAE de Analise Laboratorial"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_DAE_ESTUDO_AMBIENTAL("Financeiro", "Emissao Taxa", "Gerar DAE de Estudo Ambiental"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_DAE_TERMO("Financeiro", "Emissao Taxa", "Gerar DAE de Termo"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_DAE_PARCELAMENTO_DIVIDA("Financeiro", "Emissao Taxa", "Gerar DAE de Parcelamento de Dívida Ativa"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_DAE_COMPLEMENTAR_RENOVACAO("Financeiro", "Emissao Taxa", "Gerar DAE Complementar de Renovação"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_DAE_MULTA_CONTRATUAL("Financeiro", "Emissao Taxa", "Gerar DAE Multa Contratual"),
	Sistema_Financeiro_Emissao_Taxa_Gerar_Dae_Compensacao_Ambiental_Sem_Divisao("Financeiro","Emissão de Taxa","Gera DAE Compensação Ambiental Sem Divisão"),
	Sistema_Financeiro_Emissao_Taxa_Parcelamento_Termo_Apreensao_Deposito("Financeiro","Emissão de Taxa","Gerar Dae Parcelamento de Termo de Apreensão e Depósito"),
	Sistema_Financeiro_Emissao_Taxa_Compensacao_Ambiental_Indenizacao_Ressarcimento("Financeiro","Emissão de Taxa","Gerar Dae de Indenização/Ressarcimento de Compensação Ambiental"),
	Sistema_Financeiro_Emissao_Taxa_Medida_Compensatoria_Decisao_Judicial("Financeiro","Emissão de Taxa","Gerar DAE Medida Compensatória por Decisão Judicial"),
	Sistema_Financeiro_Emissao_Taxa_Multa_Descumprimento_TAC("Financeiro","Emissão de Taxa","Gerar DAE Multa por Descumprimento de TAC"),
	/********* TCFA ***********/
	Sistema_Financeiro_Tcfa_Listar("Financeiro","Tcfa","Listar"),
	Sistema_Financeiro_Tcfa_Salvar("Financeiro","Tcfa","Salvar"),
	Sistema_Financeiro_Tcfa_Atualizar("Financeiro","Tcfa","Atualizar"),
	Sistema_Financeiro_Tcfa_Gerar_Dae("Financeiro","Tcfa","Gerar Dae"),
	Sistema_Financeiro_Tcfa_Gerar_Segunda_Via_Dae("Financeiro","Tcfa","Gerar Segunda Via Dae"),

	/********* RELATÓRIOS ************/
	Sistema_Financeiro_Relatorios("Financeiro", "Relatórios", "Gerar Relatórios"),
	Sistema_Financeiro_Relatorio_Arrecadacao_Anual_Produto("Financeiro", "Relatório", "Arrecadação Anual por Produto"),

	/********************************************************************* MODULO AUDITORIO ***************************************************/

	Sistema_Auditorio_Calendario_Listar("Auditorio","Calendario","Lista"),
	Sistema_Auditorio_Calendario_Salvar("Auditorio","Calendario","Salvar"),
	Sistema_Auditorio_Calendario_Excluir("Auditorio","Calendario","Excluir"),
	Sistema_Auditorio_Calendario_Visualizar("Auditorio","Calendario","Visualizar Dados Agendamento"),

	/********* AGENDAMENTO ***********/
	Sistema_Auditorio_Agendamento_Listar("Auditorio","Agendamento","Listar"),
	Sistema_Auditorio_Agendamento_Excluir("Auditorio","Agendamento","Excluir"),
	Sistema_Auditorio_Agendamento_Atualizar("Auditorio","Agendamento","Atualizar"),
	Sistema_Auditorio_Agendamento_Salvar("Auditorio","Agendamento","Salvar"),
	Sistema_Auditorio_Agendamento_Coordenador("Auditorio","Agendamento","Coordenador"),
	Sistema_Auditorio_Agendamento_Cancelar("Auditorio","Agendamento","Cancelamento"),

	/********* OBJETO RESERVA ***********/
	Sistema_Auditorio_ObjetoReserva_Listar("Auditorio","Objeto de Reserva","Listar"),
	Sistema_Auditorio_ObjetoReserva_Excluir("Auditorio","Objeto de Reserva","Excluir"),
	Sistema_Auditorio_ObjetoReserva_Atualizar("Auditorio","Objeto de Reserva","Atualizar"),
	Sistema_Auditorio_ObjetoReserva_Salvar("Auditorio","Objeto de Reserva","Salvar"),

	/********* ESPACO ***********/
	Sistema_Auditorio_Espaco_Listar("Auditorio","Espaço","Listar"),
	Sistema_Auditorio_Espaco_Excluir("Auditorio","Espaço","Excluir"),
	Sistema_Auditorio_Espaco_Atualizar("Auditorio","Espaço","Atualizar"),
	Sistema_Auditorio_Espaco_Salvar("Auditorio","Espaço","Salvar"),


	/********************************************************************* MODULO FISCALIZAÇÃO ***************************************************/

	/********* DASHBOARD ***********/
	Sistema_Fiscalizacao_Dashboard_Listar("Fiscalização", "Dashboard","Listar"),
	Sistema_Fiscalizacao_Dashboard_Buscar_Ocorrencias("Fiscalização","Dashboard", "Localizar Ocorrências"),
	Sistema_Fiscalizacao_Dashboard_Buscar_Documentos("Fiscalização","Dashboard", "Localizar Autos / Notificações por pessoa"),
	Sistema_Fiscalizacao_Dashboard_Gerencia("Fiscalização", "Dashboard","Gerência"),
	Sistema_Fiscalizacao_Dashboard_Fiscal("Fiscalização", "Dashboard","Fiscal"),

	/********* CALCULADORA ***********/
	Sistema_Fiscalizacao_Calculadora("Fiscalização", "Calculadora", "Calcular"),
	Sistema_Fiscalizacao_Calculadora_Impressao("Fiscalização", "Calculadora", "Imprimir"),

	/********* CAMPANHA ***********/
	Sistema_Fiscalizacao_Campanha_Excluir("Fiscalização", "Campanha","Excluir"),
	Sistema_Fiscalizacao_Campanha_Atualizar("Fiscalização", "Campanha","Atualizar"),
	Sistema_Fiscalizacao_Campanha_Salvar("Fiscalização", "Campanha","Salvar"),
	Sistema_Fiscalizacao_Campanha_Listar("Fiscalização", "Campanha","Listar"),

	Sistema_Fiscalizacao_Configuracao_Salvar("Fiscalização", "Configuração","Salvar"),

	Sistema_Fiscalizacao_Gerador_Documento_Listar("Fiscalização", "Gerador de Documentos", "Listar"),

	/********* OCORRENCIA ***********/
	Sistema_Fiscalizacao_Ocorrencia_Salvar("Fiscalização","Ocorrência","Salvar"),
	Sistema_Fiscalizacao_Ocorrencia_exibir_detalhes("Fiscalização","Ocorrência","Exibir Detalhes"),
	Sistema_Fiscalizacao_Ocorrencia_exibir_Detalhes_Call_Center("Fiscalização","Ocorrência","Exibir Detalhes - Permissão Call Center"),
	Sistema_Fiscalizacao_Ocorrencia_Listar("Fiscalização","Ocorrência","Listar"),
	Sistema_Fiscalizacao_Ocorrencia_Excluir("Fiscalização","Ocorrência","Excluir"),
	Sistema_Fiscalizacao_Ocorrencia_Atualizar("Fiscalização","Ocorrência","Atualizar"),
	Sistema_Fiscalizacao_Ocorrencia_Visualizar_Denunciante("Fiscalização","Ocorrência", "Visualizar Denunciante"),
	Sistema_Fiscalizacao_Ocorrencia_Ouvidoria("Fiscalização","Ocorrência", "Cadastrar como Ouvidor"),
	Sistema_Fiscalizacao_Ocorrencia_Ouvidoria_Atualizar("Fiscalização","Ocorrência", "Atualizar como Ouvidor"),
	Sistema_Fiscalizacao_Ocorrencia_Interno("Fiscalização","Ocorrência", "Cadastrar Ocorrências Internas"),
	Sistema_Fiscalizacao_Ocorrencia_MP("Fiscalização","Ocorrência", "Cadastrar Ocorrências do MP / Processos Judiciais"),
	Sistema_Fiscalizacao_Ocorrencia_Outras("Fiscalização","Ocorrência", "Cadastrar Ocorrências Presencial, Telefone, Anônimo e Interno"),
	Sistema_Fiscalizacao_Ocorrencia_Resposta_Salvar("Fiscalização","Ocorrência", "Responder Ocorrência"),
	Sistema_Fiscalizacao_Ocorrencia_Responder_Fechada("Fiscalização", "Ocorrência", "Responder Ocorrências Respondidas"),
	Sistema_Fiscalizacao_Ocorrencia_Resposta_Listar("Fiscalização","Ocorrência", "Listar Respostas"),
	Sistema_Fiscalizacao_Ocorrencia_Relatorio("Fiscalização","Ocorrência", "Relatório mensal de Ocorrências"),
	Sistema_Fiscalizacao_Ocorrencia_Relatorio_Status("Fiscalização","Ocorrência", "Relatório mensal de Ocorrências por Status"),
	Sistema_Fiscalizacao_Ocorrencia_Relatorio_Consulta("Fiscalização","Ocorrência", "Relatório de Ocorrências - Consulta"),
	Sistema_Fiscalizacao_Ocorrencia_Processo_Passivo("Fiscalização","Ocorrência", "Cadastrar Processos Passivos"),
	Sistema_Fiscalizacao_Ocorrencia_DOF("Fiscalização","Ocorrência", "Cadastrar Processos DOF"),
	Sistema_Fiscalizacao_Ocorrencia_Upload_Arquivos("Fiscalização","Ocorrência", "Upload de Arquivos na Ocorrência"),
	Sistema_Fiscalizacao_Ocorrencia_Download_Arquivos("Fiscalização","Ocorrência", "Download de Arquivos na Ocorrência"),
	Sistema_Fiscalizacao_Ocorrencia_Visualizar_Lista_Ordem_Fiscalizacao("Fiscalização","Ocorrência", "Listar Ordem de Fiscalização na Ocorrência"),
	Sistema_Fiscalizacao_Ocorrencia_Tipo_Fiscal_Salvar("Fiscalização","Ocorrência","Salvar permissão Fiscal"),
	Sistema_Fiscalizacao_Ocorrencia_Tipo_Gerencia_Salvar("Fiscalização","Ocorrência","Salvar permissão Gerência"),
	Sistema_Fiscalizacao_Ocorrencia_Tipo_Fiscal_Atualizar("Fiscalização","Ocorrência","Atualizar permissão Fiscal"),
	Sistema_Fiscalizacao_Ocorrencia_Tipo_Gerencia_Atualizar("Fiscalização","Ocorrência","Atualizar permissão Gerência"),
	Sistema_Fiscalizacao_Ocorrencia_Tipo_Call_Center_Salvar("Fiscalização","Ocorrência","Salvar permissão Call Center"),
	Sistema_Fiscalizacao_Ocorrencia_Tipo_Call_Center_Atualizar("Fiscalização","Ocorrência","Atualizar permissão Call Center"),

	/********* NOTIFICACAO ***********/
	Sistema_Fiscalizacao_Notificacao_Salvar("Fiscalização","Notificação","Salvar"),
	Sistema_Fiscalizacao_Notificacao_Listar("Fiscalização","Notificação","Listar"),
	Sistema_Fiscalizacao_Notificacao_Excluir("Fiscalização","Notificação","Excluir"),
	Sistema_Fiscalizacao_Notificacao_Atualizar("Fiscalização","Notificação","Atualizar"),
	Sistema_Fiscalizacao_Notificacao_Vincular_Notificacao("Fiscalização","Notificação","Vincular"),
	Sistema_Fiscalizacao_Notificacao_Visualizar_Detalhes_Notificacao("Fiscalização","Notificação","Visualizar Detalhes da Notificação"),

	/********* ORDEM DE FISCALIZACAO ***********/
	Sistema_Fiscalizacao_OrdemFiscalizacao_Listar("Fiscalização","Ordem de Fiscalização","Listar Registros Criados"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Listar_Todos("Fiscalização","Ordem de Fiscalização","Listar Todos os Registros"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Salvar("Fiscalização","Ordem de Fiscalização","Salvar"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Cancelar("Fiscalização","Ordem de Fiscalização","Cancelar"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Atualizar("Fiscalização","Ordem de Fiscalização","Atualizar"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Add_Ocorrencia_Campo("Fiscalização","Ordem de Fiscalização","Adicionar Ocorrência Em Campo"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Adicionar_Ocorrencia("Fiscalização","Ordem de Fiscalização","Adicionar Ocorrência"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Remover_Ocorrencia("Fiscalização","Ordem de Fiscalização","Remover Ocorrência"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Fiscalizar("Fiscalização","Ordem de Fiscalização","Colocar Ordem de Fiscalização em Fiscalização"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Retorno_OF("Fiscalização","Ordem de Fiscalização","Retorno da Ordem de Fiscalização"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Download_Arquivos("Fiscalização","Ordem de Fiscalização","Download de Arquivos da Ordem de Fiscalizacao"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Visualizar_Detalhes("Fiscalização","Ordem de Fiscalização","Visualizar Detalhes da Ordem de Fiscalizacao"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Reabrir_OF("Fiscalização", "Ordem de Fiscalização", "Reabrir Ordem de Fiscalização"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Impressao_Respostas("Fiscalização","Ordem de Fiscalização", "Impressão de Respostas das Ocorrências"),
	Sistema_Fiscalizacao_OrdemFiscalizacao_Relatorios("Fiscalização","Ordem de Fiscalização", "Relatórios"),

	/********* TERMO ***********/
	Sistema_Fiscalizacao_Termo_Excluir("Fiscalização","Termo","Excluir"),
	Sistema_Fiscalizacao_Termo_Atualizar("Fiscalização","Termo","Atualizar"),
	Sistema_Fiscalizacao_Termo_Salvar("Fiscalização","Termo","Salvar"),
	Sistema_Fiscalizacao_Termo_Listar("Fiscalização","Termo","Listar"),
	Sistema_Fiscalizacao_Termo_Vincular_Termo("Fiscalização","Termo","Vincular"),
	Sistema_Fiscalizacao_Termo_Visualizar_Detalhes_Termo("Fiscalização","Termo","Visualizar Detalhes do Termo"),
	Sistema_Fiscalizacao_Termo_Gerar_Dae("Fiscalização","Termo","Gerar DAE"),
	Sistema_Fiscalizacao_Termo_Imprimir_Dae("Fiscalização","Termo","Imprimir DAE"),
	Sistema_Fiscalizacao_Termo_Excluir_Dae("Fiscalização","Termo","Excluir DAE"),
	Sistema_Fiscalizacao_Termo_Segunda_Via_Dae("Fiscalização","Termo","Segunda Via DAE"),

	/********* TIPO INFRACAO ***********/
	Sistema_Fiscalizacao_Tipo_Infracao_Excluir("Fiscalização","Tipo de Infração","Excluir"),
	Sistema_Fiscalizacao_Tipo_Infracao_Atualizar("Fiscalização","Tipo de Infração","Atualizar"),
	Sistema_Fiscalizacao_Tipo_Infracao_Salvar("Fiscalização","Tipo de Infração","Salvar"),
	Sistema_Fiscalizacao_Tipo_Infracao_Listar("Fiscalização","Tipo de Infração","Listar"),

	/********* GRUPO ESPECIALIZAÇÃO FISCALIZAÇÃO ***********/
	Sistema_Fiscalizacao_Grupo_Especializacao_Fiscalizacao_Excluir("Fiscalização","Grupo Especialização Fiscalização","Excluir"),
	Sistema_Fiscalizacao_Grupo_Especializacao_Fiscalizacao_Atualizar("Fiscalização","Grupo Especialização Fiscalização","Atualizar"),
	Sistema_Fiscalizacao_Grupo_Especializacao_Fiscalizacao_Salvar("Fiscalização","Grupo Especialização Fiscalização","Salvar"),
	Sistema_Fiscalizacao_Grupo_Especializacao_Fiscalizacao_Listar("Fiscalização","Grupo Especialização Fiscalização","Listar"),

	/********* AUTO LICENCIAMENTO ***********/
	Sistema_Fiscalizacao_Auto_Licenciamento_Excluir("Fiscalização","Auto Licenciamento","Excluir"),
	Sistema_Fiscalizacao_Auto_Licenciamento_Atualizar("Fiscalização","Auto Licenciamento","Atualizar"),
	Sistema_Fiscalizacao_Auto_Licenciamento_Salvar("Fiscalização","Auto Licenciamento","Salvar"),
	Sistema_Fiscalizacao_Auto_Licenciamento_Listar("Fiscalização","Auto Licenciamento","Listar"),


	/******** LICENCIAMENTO RAMA*************/
	Sistema_Licenciamento_Rama_Listar_Todos("Licenciamento","Rama","Listar Todos"),//desuso
	Sistema_Licenciamento_Rama_Listar_Protocodos("Licenciamento","Rama","Listar Limitados"),//desuso
	Sistema_Licenciamento_Rama_Listar("Licenciamento","Rama","Listar"),//desuso
	Sistema_Licenciamento_Rama_Distribuir("Licenciamento","Rama","Distribuir"),
	Sistema_Licenciamento_Rama_Analise("Licenciamento","Rama","Análise"),
	Sistema_Licenciamento_Rama_Arquivo("Licenciamento","Rama","Arquivo"),
	Sistema_Licenciamento_Rama_Correcao("Licenciamento","Rama","Correção de RAMA"),
	Sistema_Licenciamento_Rama_Visualizar_Relatorio_Tecnico("Licenciamento","Rama","Visualizar Relatório Técnico Produzido"),

	Sistema_Licenciamento_Rama_Criacao("Licenciamento","Rama","Criar RAMA"),
	Sistema_Licenciamento_Rama_Listar_Pendentes("Licenciamento","Rama","RAMA Pendente"),
	Sistema_Licenciamento_Rama_Validacao_Isencao("Licenciamento","Rama","Validação de RAMA"),
	Sistema_Licenciamento_Rama_Listar_Rama("Licenciamento","Rama","Listar RAMA"),
	Sistema_Licenciamento_Rama_Dispensar("Licenciamento","Rama","Dispensar"),
	Sistema_Licenciamento_Rama_Protocoladar_Com_Vinculo("Licenciamento","Rama","Vincular"),
	Sistema_Licenciamento_Rama_Visualizar("Licenciamento","Rama","Visualizar"),
	Sistema_Licenciamento_Rama_Imprimir("Licenciamento","Rama","Imprimir"),
	Sistema_Licenciamento_Rama_Desvincular("Licenciamento","Rama","Desvincular"),
	Sistema_Licenciamento_Rama_Pesquisa_Completa("Licenciamento","Rama","Pesquisar por Técnicos"),
	Sistema_Licenciamento_Rama_Remanejar("Licenciamento","Rama","Remanejar"),
	Sistema_Licenciamento_Rama_Aprovar_Analise("Licenciamento","Rama","Aprovar Análise"),
	Sistema_Licenciamento_Rama_Excluir("Licenciamento","Rama","Excluir RAMA"),
	Sistema_Licenciamento_Rama_Visualizar_Dae("Licenciamento","Rama","Visualizar Dae"),
	Sistema_Licenciamento_Rama_Gerar_Dae("Licenciamento","Rama","Gerar Dae"),
	Sistema_Licenciamento_Rama_Cancelar_DAE("Licenciamento","Rama","Cancelar Dae"),
	Sistema_Licenciamento_Rama_Consulta_Relatorios_Produzidos_tecnico("Licenciamento","Rama","Consultar Relatórios Produzidos"),
	Sistema_Licenciamento_Rama_Correcao_Completa("Licenciamento","Rama","Correção Completa"),

	/********* AUTO INFRACAO ***************/
	Sistema_Fiscalizacao_Auto_Infracao_Excluir("Fiscalização","Auto de Infração","Excluir"),
	Sistema_Fiscalizacao_Auto_Infracao_Atualizar("Fiscalização","Auto de Infração","Atualizar"),
	Sistema_Fiscalizacao_Auto_Infracao_Despacho_Saneador("Fiscalização","Auto de Infração","Despacho Saneador do Auto de Infração"),
	Sistema_Fiscalizacao_Auto_Infracao_Salvar("Fiscalização","Auto de Infração","Salvar"),
	Sistema_Fiscalizacao_Auto_Infracao_Listar("Fiscalização","Auto de Infração","Listar"),
	Sistema_Fiscalizacao_Auto_Infracao_Vincular_Auto_Infracao("Fiscalização","Auto de Infração","Vincular Auto de Infração"),
	Sistema_Fiscalizacao_Auto_Infracao_Visualizar_Detalhes_Auto_Infracao("Fiscalização","Auto de Infração","Visualizar Detalhes do Auto de Infração"),
	Sistema_Fiscalizacao_Auto_Infracao_Gerar_Dae("Fiscalização","Auto de Infração","Gerar DAE"),
	Sistema_Fiscalizacao_Auto_Infracao_Imprimir_Dae("Fiscalização","Auto de Infração","Imprimir DAE"),
	Sistema_Fiscalizacao_Auto_Infracao_Excluir_Dae("Fiscalização","Auto de Infração","Excluir DAE"),
	Sistema_Fiscalizacao_Auto_Infracao_Segunda_Via_Dae("Fiscalização","Auto de Infração","Segunda Via DAE"),
	Sistema_Fiscalizacao_Auto_Infracao_Fundamentacao_Adicionar("Fiscalização","Auto de Infração","Adicionar Fundamentação"),
	Sistema_Fiscalizacao_Auto_Infracao_Fundamentacao_Excluir("Fiscalização","Auto de Infração","Excluir Fundamentação"),
	Sistema_Fiscalizacao_Auto_Infracao_Fundamentacao_Atualizar("Fiscalização","Auto de Infração","Atualizar Fundamentação"),

	/********* AUTO DECLARACAO *************/
	Sistema_Fiscalizacao_Auto_Declaracao_Listar("Fiscalização","Auto Declaração", "Listar"),
	Sistema_Fiscalizacao_Auto_Declaracao_Visualizar_Detalhes("Fiscalização","Auto Declaração", "Visualizar Detalhes"),

	/******** JUSTIFICATIVA ***************/
	Sistema_Fiscalizacao_Justificativa_Justificar_Documentos("Fiscalização","Justificativas","Justificar Edição / Alteração de Documentos"),

	/********* DAE ***********/
	Sistema_Fiscalizacao_DAE_Gerar("Fiscalização", "DAE", "Salvar"),
	Sistema_Fiscalizacao_DAE_Listar("Fiscalização", "DAE", "Listar"),
	Sistema_Fiscalizacao_DAE_Excluir("Fiscalização", "DAE", "Excluir"),
	Sistema_Fiscalizacao_DAE_Imprimir("Fiscalização", "DAE", "Imprimir"),

	/********* RAIA ***********/
	Sistema_Fiscalizacao_Raia_Aprovar("Fiscalização", "RAIA", "Aprovar"),
	Sistema_Fiscalizacao_Raia_Assinar("Fiscalização", "RAIA", "Assinar"),
	Sistema_Fiscalizacao_Raia_Excluir("Fiscalização", "RAIA", "Excluir"),
	Sistema_Fiscalizacao_Raia_Cancelar("Fiscalização", "RAIA", "Cancelar"),
	Sistema_Fiscalizacao_Raia_Modificar_Status("Fiscalização", "RAIA", "Modificar Status"),
	Sistema_Fiscalizacao_Raia_Finalizar("Fiscalização", "RAIA", "Finalizar"),
	Sistema_Fiscalizacao_Raia_Salvar("Fiscalização", "RAIA", "Salvar"),
	Sistema_Fiscalizacao_Raia_Atualizar("Fiscalização", "RAIA", "Atualizar"),
	Sistema_Fiscalizacao_Raia_Listar("Fiscalização", "RAIA", "Listar"),
	Sistema_Fiscalizacao_Raia_Excluir_Arquivos("Fiscalização", "RAIA", "Excluir Arquivos"),
	Sistema_Fiscalizacao_Raia_Upload_Arquivos("Fiscalização", "RAIA", "Upload de arquivos"),
	Sistema_Fiscalizacao_Raia_Visualizar("Fiscalização", "RAIA", "Visualizar"),
	Sistema_Fiscalizacao_Raia_Imprimir("Fiscalização", "RAIA", "Imprimir"),
	Sistema_Fiscalizacao_Raia_Download_File("Fiscalização", "RAIA", "Download de Arquivos"),

	/********* COMUNICACAO DE CRIME ***********/
	Sistema_Fiscalizacao_Comunicacao_Crime_Aprovar("Fiscalização", "Comunicação de Crime", "Aprovar"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Assinar("Fiscalização", "Comunicação de Crime", "Assinar"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Finalizar("Fiscalização", "Comunicação de Crime", "Finalizar"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Listar("Fiscalização", "Comunicação de Crime", "Listar"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Visualizar("Fiscalização","Comunicação de Crime", "Visualizar"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Salvar("Fiscalização", "Comunicação de Crime", "Salvar"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Atualizar("Fiscalização", "Comunicação de Crime", "Atualizar"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Cancelar("Fiscalização", "Comunicação de Crime", "Cancelar"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Modificar_Status("Fiscalização", "Comunicação de Crime", "Modificar Status"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Imprimir("Fiscalização", "Comunicação de Crime", "Imprimir"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Upload_Arquivos("Fiscalização", "Comunicação de Crime", "Upload de arquivos"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Download_File("Fiscalização", "Comunicação de Crime", "Download de Arquivos"),
	Sistema_Fiscalizacao_Comunicacao_Crime_Excluir_Arquivos("Fiscalização", "Comunicação de Crime", "Excluir Arquivos"),

	/************************************************************************ MODULO GEOPROCESSAMENTO **************************************/

	/********* ESTATISTICA ***********/
	Sistema_Geo_Estatistica_Visualizar("Geo-Processamento","Estatísticas","Visualizar"),

	/********* RELATORIOS ***********/
	Sistema_Geo_Relatorios_Index("Geo-Processamento","Relatórios","Listar"),
	Sistema_Geo_Relatorios_Gerar("Geo-Processamento","Relatórios","Gerar"),

	/********* SHAPES ***********/
	Sistema_Geo_Shapes_Salvar("Geo-Processamento","Shapes","Salvar"),
	Sistema_Geo_Shapes_Atualizar("Geo-Processamento","Shapes","Atualizar"),
	Sistema_Geo_Shapes_Excluir("Geo-Processamento","Shapes","Excluir"),
	Sistema_Geo_Shapes_Atendimento_Listar("Geo-Processamento","Shapes","Listar Shapes Atendimento"),
	Sistema_Geo_Shapes_Atendimento_Antigo_Listar("Geo-Processamento","Shapes","Listar Shapes Atendimento Antigo"),
	Sistema_Geo_Shapes_Arquivo_Listar("Geo-Processamento","Shapes","Listar Shapes Arquivo"),
	Sistema_Geo_Shapes_Importados_Listar("Geo-Processamento","Shapes","Listar Shapes Importados"),
	Sistema_Geo_Shapes_Atendimento_Substituir("Geo-Processamento","Shapes","Subistituir Shape Atendimento"),
	Sistema_Geo_Shapes_Atendimento_Adicionar("Geo-Processamento","Shapes","Adicionar Shape Atendimento"),
	Sistema_Geo_Shapes_Arquivo_Adicionar("Geo-Processamento","Shapes","Adicionar Shape Arquivo"),
	Sistema_Geo_Shapes_Arquivo_Adicionar_Shape_Original("Geo-Processamento","Shapes","Adicionar Shape Original"),

	/********* SHAPES EXTRAS ***********/
	Sistema_Geo_Shapes_Extras_Salvar("Geo-Processamento","Shapes Extras","Salvar"),

	/********* ARQUIVOS EXTRAS ***********/
	Sistema_Geo_Arquivos_Extras_Salvar("Geo-Processamento","Arquivos Extras","Salvar"),
	Sistema_Geo_Arquivos_Extras_Excluir("Geo-Processamento","Arquivos Extras","Excluir"),

	/********* CONFIGURACAO ***********/
	Sistema_Geo_Configuracao_Index("Geo-Processamento","Configuração","Visualizar"),
	Sistema_Geo_Configuracao_Atualizar("Geo-Processamento","Configuração","Atualizar"),

	/********* DASHBOARD ***********/
	Sistema_Geo_Dashboard_Index("Geo-Processamento","Dashboard","Index"),

	/********* BASE CARTOGRAFICA TIPOS ***********/
	Sistema_Geo_Base_Cartografica_Tipos_Listar("Geo-Processamento","Base","Listar"),
	Sistema_Geo_Base_Cartografica_Tipos_Salvar("Geo-Processamento","Base","Salvar"),
	Sistema_Geo_Base_Cartografica_Tipos_Atualizar("Geo-Processamento","Base","Atualizar"),

	/********* BASE CARTOGRAFICA SHAPES ***********/

	Sistema_Geo_Base_Cartografica_Shape_Listar("Geo-Processamento","Base","Listar"),
	Sistema_Geo_Base_Cartografica_Shape_Atualizar("Geo-Processamento","Base","Atualizar"),
	Sistema_Geo_Base_Cartografica_Shape_Ativar_Desativar("Geo-Processamento","Base","Ativar e Desativar"),
	Sistema_Geo_Base_Cartografica_Shape_Importar("Geo-Processamento","Base","Importar"),

	/********* BASE CARTOGRAFICA LEGENDAS ***********/
	Sistema_Geo_Base_Cartografica_Legendas_Listar("Geo-Processamento","Legendas","Listar"),
	Sistema_Geo_Base_Cartografica_Legendas_Salvar("Geo-Processamento","Legendas","Salvar"),
	Sistema_Geo_Base_Cartografica_Legendas_Atualizar("Geo-Processamento","Legendas","Atualizar"),
	Sistema_Geo_Base_Cartografica_Legendas_Excluir("Geo-Processamento","Legendas","Excluir"),

	/********* TIPO MAPA TEMATICO ***********/
	Sistema_Geo_Tipo_Mapa_Tematico_Listar("Geo-Processamento","Tipo Mapa Tematico","Listar"),
	Sistema_Geo_Tipo_Mapa_Tematico_Salvar("Geo-Processamento","Tipo Mapa Tematico","Salvar"),
	Sistema_Geo_Tipo_Mapa_Tematico_Atualizar("Geo-Processamento","Tipo Mapa Tematico","Atualizar"),

	/********* MAPA TEMATICO ***********/
	Sistema_Geo_Mapa_Tematico_Menu("Geo-Processamento","Mapa Temático","Menu"),
	Sistema_Geo_Mapa_Tematico_Listar("Geo-Processamento","Mapa Temático","Listar"),
	Sistema_Geo_Mapa_Tematico_Importar("Geo-Processamento","Mapa Temático","Importar"),
	Sistema_Geo_Mapa_Tematico_Atualizar("Geo-Processamento","Mapa Temático","Atualizar"),

	/********** GEO MAPS **********/
	Sistema_Geo_Maps_Index("Geo-Processamento","GeoMaps","Index"),
	Sistema_Geo_Maps_Base_Cartografica("Geo-Processamento","GeoMaps","Base Cartográfica"),
	Sistema_Geo_Maps_Base_Cartografica_Visualizar_Desativados("Geo-Processamento","GeoMaps - Base Cartográfica","Visualizar Desativados"),
	Sistema_Geo_Maps_Mapa_Tematico("Geo-Processamento","GeoMaps","Mapa Temático"),
	Sistema_Geo_Maps_Shape_Processo("Geo-Processamento","GeoMaps","Shape de Processo: Apenas Ativos"),
	Sistema_Geo_Maps_Shape_Processo_Todos("Geo-Processamento","GeoMaps","Shape de Processo: Todos"),
	Sistema_Geo_Maps_Shape_Equipamento("Geo-Processamento","GeoMaps","Shape de Equipamento: Apenas Ativos"),
	Sistema_Geo_Maps_Shape_Equipamento_Todos("Geo-Processamento","GeoMaps","Shape de Equipamento: Todos"),
	Sistema_Geo_Maps_Shape_Extra("Geo-Processamento","GeoMaps","Shape Extra: Apenas Ativos"),
	Sistema_Geo_Maps_Shape_Extra_Todos("Geo-Processamento","GeoMaps","Shape Extra: Todos"),
	Sistema_Geo_Maps_Shape_Processo_Proximos("Geo-Processamento","GeoMaps","Shape de Processo Próximos"),
	Sistema_Geo_Maps_Shape_Processo_Download("Geo-Processamento","GeoMaps","Download de Shape de Processo"),
	Sistema_Geo_Maps_Shape_Processo_Download_Kml("Geo-Processamento","GeoMaps","Download de Shape de Processo em KML"),
	Sistema_Geo_Maps_Shape_Equipamento_Download("Geo-Processamento","GeoMaps","Download de Shape de Equipamento"),
	Sistema_Geo_Maps_Shape_Equipamento_Download_Kml("Geo-Processamento","GeoMaps","Download de Shape de Equipamento em KML"),
	Sistema_Geo_Maps_Shape_Extra_Download("Geo-Processamento","GeoMaps","Download de Shape Extra"),
	Sistema_Geo_Maps_Shape_Extra_Download_Kml("Geo-Processamento","GeoMaps","Download de Shape Extra em KML"),
	Sistema_Geo_Maps_Arquivo_Extra_Download("Geo-Processamento","GeoMaps","Download de Arquivo Extra"),
	Sistema_Geo_Maps_Pesquisa_Basica("Geo-Processamento","GeoMaps","Pesquisa Básica"),
	Sistema_Geo_Maps_Pesquisa_Avancada("Geo-Processamento","GeoMaps","Pesquisa Avançada"),
	Sistema_Geo_Maps_Pesquisa_Aif("Geo-Processamento","GeoMaps","Pesquisa de Auto de Infração"),
	Sistema_Geo_Maps_Pesquisa_Ponto("Geo-Processamento","GeoMaps","Pesquisa de Ponto"),
	Sistema_Geo_Maps_Mapa_Calor("Geo-Processamento","GeoMaps","Mapa de Calor"),
	Sistema_Geo_Maps_Ferramenta_Distancia("Geo-Processamento","GeoMaps","Ferramenta de Distância"),
	Sistema_Geo_Maps_Exibir_Todos_Shapes_Pesquisa("Geo-Processamento","GeoMaps","Exibir Todos os Shapes da Pesquisa"),
	Sistema_Geo_Maps_Download_Todos_Shapes_Pesquisa("Geo-Processamento","GeoMaps","Download de Todos os Shapes da Pesquisa"),

	/********* PROCESSO IMAGEM ***********/
	Sistema_Geo_Processo_Imagem_Upload("Geo-Processamento","Upload Imagens","Upload"),
	Sistema_Geo_Processo_Imagem_Desativar("Geo-Processamento","Upload Imagens","Desativar"),
	Sistema_Geo_Processo_Imagem_Visualizar("Geo-Processamento","Upload Imagens","Visualizar Imagens"),
	Sistema_Geo_Processo_Imagem_Visualizar_Desativadas("Geo-Processamento","Upload Imagens","Visualizar Imagens Desativadas"),
	Sistema_Geo_Processo_Imagem_Upload_Ilimitado("Geo-Processamento","Upload Imagens","Upload Ilimitado"),

	/************************************************************************ MODULO LABORATORIO **************************************/

	/********* CONFIGURACOES ***********/
	Sistema_Laboratorio_Configuracao_Listar("Laboratório", "Configuração", "Listar"),

	/********* APP ALERTS ***********/
	Sistema_Laboratorio_App_Alerts_Salvar("Laboratório", "Boletim", "Salvar"),
	Sistema_Laboratorio_App_Alerts_Atualizar("Laboratório", "Boletim", "Atualizar"),
	Sistema_Laboratorio_App_Alerts_Listar("Laboratório", "Boletim", "Listar"),
	Sistema_Laboratorio_App_Alerts_Desativar("Laboratório", "Boletim", "Desativar"),

	/********* BOLETIM ***********/
	Sistema_Laboratorio_Boletim_Salvar("Laboratório", "Boletim", "Salvar"),
	Sistema_Laboratorio_Boletim_Atualizar("Laboratório", "Boletim", "Atualizar"),
	Sistema_Laboratorio_Boletim_Listar("Laboratório", "Boletim", "Listar"),
	Sistema_Laboratorio_Boletim_Publicar("Laboratório", "Boletim", "Publicar"),
	Sistema_Laboratorio_Boletim_Enviar("Laboratório", "Boletim", "Enviar Boletim"),
	Sistema_Laboratorio_Boletim_Imprimir("Laboratório", "Boletim", "Imprimir"),

	/********* ORDEM COLETA ***********/
	Sistema_Laboratorio_Ordem_Coleta_Salvar("Laboratório", "Ordem de Coleta", "Salvar"),
	Sistema_Laboratorio_Ordem_Coleta_Atualizar("Laboratório", "Ordem de Coleta", "Atualizar"),
	Sistema_Laboratorio_Ordem_Coleta_Listar("Laboratório", "Ordem de Coleta", "Listar"),
	Sistema_Laboratorio_Ordem_Coleta_Cancelar("Laboratório", "Ordem de Coleta", "Cancelar"),
	Sistema_Laboratorio_Ordem_Coleta_Finalizar("Laboratório", "Ordem de Coleta", "Finalizar"),
	Sistema_Laboratorio_Ordem_Coleta_Relatorio("Laboratório", "Ordem de Coleta", "Gerar Relatório"),
	Sistema_Laboratorio_Ordem_Coleta_Relatorio_Finalizado("Laboratório", "Ordem de Coleta", "Gerar Relatório Finalizado"),

	/********* CONVENIOS BOLETIM ***********/
	Sistema_Laboratorio_Convenio_Boletim_Salvar("Laboratório", "Convênios de Boletim", "Salvar"),
	Sistema_Laboratorio_Convenio_Boletim_Excluir("Laboratório", "Convênios de Boletim", "Excluir"),
	Sistema_Laboratorio_Convenio_Boletim_Atualizar("Laboratório", "Convênios de Boletim", "Atualizar"),
	Sistema_Laboratorio_Convenio_Boletim_Listar("Laboratório", "Convênios de Boletim", "Listar"),
	Sistema_Laboratorio_Convenio_Boletim_Ativar("Laboratório", "Convênios de Boletim", "Ativar"),
	Sistema_Laboratorio_Convenio_Boletim_Desativar("Laboratório", "Convênios de Boletim", "Desativar"),

	/********* CONDICOES CLIMATICAS ***********/
	Sistema_Laboratorio_Condicoes_Climaticas_Salvar("Laboratório", "Condições Climaticas", "Salvar"),
	Sistema_Laboratorio_Condicoes_Climaticas_Excluir("Laboratório", "Condições Climaticas", "Excluir"),
	Sistema_Laboratorio_Condicoes_Climaticas_Atualizar("Laboratório", "Condições Climaticas", "Atualizar"),
	Sistema_Laboratorio_Condicoes_Climaticas_Listar("Laboratório", "Condições Climaticas", "Listar"),

	/********* TABELA NMP ***********/
	Sistema_Laboratorio_TabelaNMP_Salvar("Laboratório", "Tabela NMP", "Salvar"),
	Sistema_Laboratorio_TabelaNMP_Excluir("Laboratório", "Tabela NMP", "Excluir"),
	Sistema_Laboratorio_TabelaNMP_Atualizar("Laboratório", "Tabela NMP", "Atualizar"),
	Sistema_Laboratorio_TabelaNMP_Listar("Laboratório", "Tabela NMP", "Listar"),
	Sistema_Laboratorio_TabelaNMP_Ativar("Laboratório", "Tabela NMP", "Ativar"),
	Sistema_Laboratorio_TabelaNMP_Desativar("Laboratório", "Tabela NMP", "Desativar"),

	/********* VOLUME DE DILUICAO ***********/
	Sistema_Laboratorio_Volume_Diluicao_Salvar("Laboratório", "Volume de Diluição", "Salvar"),
	Sistema_Laboratorio_Volume_Diluicao_Excluir("Laboratório", "Volume de Diluição", "Excluir"),
	Sistema_Laboratorio_Volume_Diluicao_Atualizar("Laboratório", "Volume de Diluição", "Atualizar"),
	Sistema_Laboratorio_Volume_Diluicao_Listar("Laboratório", "Volume de Diluição", "Listar"),
	Sistema_Laboratorio_Volume_Diluicao_Ativar("Laboratório", "Volume de Diluição", "Ativar"),
	Sistema_Laboratorio_Volume_Diluicao_Desativar("Laboratório", "Volume de Diluição", "Desativar"),

	/********* FATOR DE IMPEDIMENTO ***********/
	Sistema_Laboratorio_Fator_Impedimento_Salvar("Laboratório", "Fator de Impedimento", "Salvar"),
	Sistema_Laboratorio_Fator_Impedimento_Excluir("Laboratório", "Fator de Impedimento", "Excluir"),
	Sistema_Laboratorio_Fator_Impedimento_Atualizar("Laboratório", "Fator de Impedimento", "Atualizar"),
	Sistema_Laboratorio_Fator_Impedimento_Listar("Laboratório", "Fator de Impedimento", "Listar"),

	/********* ITEM DE RISCO ***********/
	Sistema_Laboratorio_Item_Risco_Salvar("Laboratório", "Item de Risco", "Salvar"),
	Sistema_Laboratorio_Item_Risco_Excluir("Laboratório", "Item de Risco", "Excluir"),
	Sistema_Laboratorio_Item_Risco_Atualizar("Laboratório", "Item de Risco", "Atualizar"),
	Sistema_Laboratorio_Item_Risco_Listar("Laboratório", "Item de Risco", "Listar"),
	Sistema_Laboratorio_Item_Risco_Ativar("Laboratório", "Item de Risco", "Ativar"),
	Sistema_Laboratorio_Item_Risco_Desativar("Laboratório", "Item de Risco", "Desativar"),

	/********* ANÁLISE LABORATORIAL ***********/
	Sistema_Laboratorio_Analise_Laboratorial_Salvar("Laboratório", "Análise Laboratorial", "Salvar"),
	Sistema_Laboratorio_Analise_Laboratorial_Excluir("Laboratório", "Análise Laboratorial", "Excluir"),
	Sistema_Laboratorio_Analise_Laboratorial_Atualizar("Laboratório", "Análise Laboratorial", "Atualizar"),
	Sistema_Laboratorio_Analise_Laboratorial_Listar("Laboratório", "Análise Laboratorial", "Listar"),

	/********* ANALISE ***********/
	Sistema_Laboratorio_Analise_Listar("Laboratório","Analise","Listar"),
	Sistema_Laboratorio_Analise_Salvar("Laboratório","Analise","Salvar"),
	Sistema_Laboratorio_Analise_Atualizar("Laboratório","Analise","Atualizar"),
	Sistema_Laboratorio_Analise_Relatorio("Laboratório","Analise","Relatório das Análises"),

	/********* RESULTADO ANALISE ***********/
	Sistema_Laboratorio_Resultado_Analise_Listar("Laboratório","Resultado Análise","Listar"),
	Sistema_Laboratorio_Resultado_Analise_Salvar("Laboratório","Resultado Análise","Salvar"),
	Sistema_Laboratorio_Resultado_Analise_Atualizar("Laboratório","Resultado Análise","Atualizar"),
	Sistema_Laboratorio_Resultado_Analise_Gerar_Media("Laboratório","Resultado Análise","Gerar Média"),
	Sistema_Laboratorio_Resultado_Analise_Invalidar("Laboratório","Resultado Análise","Invalidar Resultado"),

	/********* REGIAO ***********/
	Sistema_Laboratorio_Regiao_Listar("Laboratório","Região","Listar"),
	Sistema_Laboratorio_Regiao_Salvar("Laboratório","Região","Salvar"),
	Sistema_Laboratorio_Regiao_Atualizar("Laboratório","Região","Atualizar"),

	/********* RegiaoZonaPontos ***********/
	Sistema_Laboratorio_RegiaoZonaPontos_Listar("Laboratório","Zona Coleta","Listar"),
	Sistema_Laboratorio_RegiaoZonaPontos_Salvar("Laboratório","Zona Coleta","Salvar"),
	Sistema_Laboratorio_RegiaoZonaPontos_Atualizar("Laboratório","Zona Coleta","Atualizar"),

	/********* PONTO DE AMOSTRAGEM ***********/
	Sistema_Laboratorio_Ponto_Amostragem_Salvar("Laboratório", "Ponto de Amostragem", "Salvar"),
	Sistema_Laboratorio_Ponto_Amostragem_Atualizar("Laboratório", "Ponto de Amostragem", "Atualizar"),
	Sistema_Laboratorio_Ponto_Amostragem_Listar("Laboratório", "Ponto de Amostragem", "Listar"),
	Sistema_Laboratorio_Ponto_Amostragem_Ativar("Laboratório", "Ponto de Amostragem", "Ativar"),
	Sistema_Laboratorio_Ponto_Amostragem_Desativar("Laboratório", "Ponto de Amostragem", "Desativar"),

	/*********PARAMETRO ************/
	Sistema_Laboratorio_Parametro_Salvar("Laboratório", "Parâmetro","Salvar"),
	Sistema_Laboratorio_Parametro_Excluir("Laboratório", "Parâmetro","Excluir"),
	Sistema_Laboratorio_Parametro_Atualizar("Laboratório", "Parâmetro","Atualizar"),
	Sistema_Laboratorio_Parametro_Listar("Laboratório", "Parâmetro","Listar"),

	/********* ENQUADRAMENTO ***********/
	Sistema_Laboratorio_Enquadramento_Listar("Laboratório","Enquadramento","Listar"),
	Sistema_Laboratorio_Enquadramento_Salvar("Laboratório","Enquadramento","Salvar"),
	Sistema_Laboratorio_Enquadramento_Atualizar("Laboratório","Enquadramento","Atualizar"),
	Sistema_Laboratorio_Enquadramento_Excluir("Laboratório","Enquadramento","Excluir"),

	/************************************************************************ MODULO GERAL *************************************************/
	/***** MODULO FEEDBACK ***********************************************************************/
	Sistema_Geral_Grupo_Feedback_Listar("Geral", "Feedback", "Listar"),
	Sistema_Geral_Grupo_Feedback_Salvar("Geral", "Feedback", "Salvar"),
	Sistema_Geral_Grupo_Feedback_Atualizar("Geral", "Feedback", "Atualizar"),

	/********* TIPO ANEXO ***********************************************************************/
	Sistema_Geral_Tipo_Anexo_Listar("Geral", "Tipo Anexo", "Listar"),
	Sistema_Geral_Tipo_Anexo_Salvar("Geral", "Tipo Anexo","Salvar"),
	Sistema_Geral_Tipo_Anexo_Atualizar("Geral", "Tipo Anexo","Atualizar"),
	Sistema_Geral_Tipo_Anexo_Excluir("Geral", "Tipo Anexo","Excluir"),

	/********* E-MAIL ***********/
	Sistema_Geral_Email_Listar("Geral", "Email", "Listar"),
	Sistema_Geral_Email_Enviar("Geral", "Email", "Enviar"),
	Sistema_Geral_Email_Excluir("Geral", "Email", "Excluir"),

	/********* ENQUETE ***********/
	Sistema_Geral_Enquete_Listar("Geral", "Enquete", "Listar"),
	Sistema_Geral_Enquete_Salvar("Geral", "Enquete", "Salvar"),
	Sistema_Geral_Enquete_Atualizar("Geral", "Enquete", "Atualizar"),
	Sistema_Geral_Enquete_Excluir("Geral", "Enquete", "Excluir"),
	Sistema_Geral_Enquete_Relatorio("Geral", "Enquete", "Relatório"),

	/******** CONSULTORIA ******************/
	Sistema_Geral_Consultoria_Salvar("Geral", "Consultoria","Salvar"),
	Sistema_Geral_Consultoria_Atualizar("Geral", "Consultoria","Atualizar"),
	Sistema_Geral_Consultoria_Listar("Geral", "Consultoria","Listar"),
	Sistema_Geral_Consultoria_Excluir("Geral", "Consultoria","Excluir"),
	Sistema_Geral_Consultoria_Vincular("Geral", "Consultoria","Vincular Consultoria a Interessado"),
	Sistema_Geral_Consultoria_Desvincular("Geral", "Consultoria","Desvincular Consultoria a Interessado"),


	/********* POTENCIAL POLUIDOR ***********/
	Sistema_Geral_Potencial_Poluidor_Salvar("Geral", "Potencial Poluidor","Salvar"),
	Sistema_Geral_Potencial_Poluidor_Excluir("Geral", "Potencial Poluidor","Excluir"),
	Sistema_Geral_Potencial_Poluidor_Atualizar("Geral", "Potencial Poluidor","Atualizar"),
	Sistema_Geral_Potencial_Poluidor_Listar("Geral", "Potencial Poluidor","Listar"),

	/********* MENSAGEM ***********/
	Sistema_Geral_Mensagens_Criar_Mensagem("Geral", "Mensagens", "Criar Mensagens para o Usuário"),


	/********* UNIDADE DE CONSERVAÇÃO ***********/
	Sistema_Geral_Unidade_Conservacao_Salvar("Geral", "Unidade de Conservação", "Salvar"),
	Sistema_Geral_Unidade_Conservacao_Excluir("Geral", "Unidade de Conservação", "Excluir"),
	Sistema_Geral_Unidade_Conservacao_Atualizar("Geral", "Unidade de Conservação", "Atualizar"),
	Sistema_Geral_Unidade_Conservacao_Listar("Geral", "Unidade de Conservação", "Listar"),

	/********* FERIADO ***********/
	Sistema_Geral_Feriado_Salvar("Geral", "Feriado", "Salvar"),
	Sistema_Geral_Feriado_Excluir("Geral", "Feriado", "Excluir"),
	Sistema_Geral_Feriado_Atualizar("Geral", "Feriado", "Atualizar"),
	Sistema_Geral_Feriado_Listar("Geral", "Feriado", "Listar"),

	/********* ATIVIDADE ***********/
	Sistema_Geral_Atividade_Salvar("Geral", "Atividade", "Salvar"),
	Sistema_Geral_Atividade_Excluir("Geral", "Atividade", "Excluir"),
	Sistema_Geral_Atividade_Atualizar("Geral", "Atividade", "Atualizar"),
	Sistema_Geral_Atividade_Listar("Geral", "Atividade", "Listar"),
	Sistema_Geral_Atividade_Ativar_Atividade("Geral", "Atividade", "Ativar / Desativar Atividades de uma Coema"),


	/********* GRUPO ATIVIDADE ***********/
	Sistema_Geral_Grupo_Atividade_Salvar("Geral", "Grupo de Atividades", "Salvar"),
	Sistema_Geral_Grupo_Atividade_Excluir("Geral", "Grupo de Atividades", "Excluir"),
	Sistema_Geral_Grupo_Atividade_Atualizar("Geral", "Grupo de Atividades", "Atualizar"),
	Sistema_Geral_Grupo_Atividade_Listar("Geral", "Grupo de Atividades", "Listar"),
	Sistema_Geral_Grupo_Atividade_Ativar_Atividade("Geral", "Grupo de Atividades", "Ativar / Desativar Grupo de Atividades de uma Coema"),

	/********* UNIDADE DE MEDIDA ***********/
	Sistema_Geral_Unidade_Medida_Salvar("Geral", "Unidade de Medida", "Salvar"),
	Sistema_Geral_Unidade_Medida_Excluir("Geral", "Unidade de Medida", "Excluir"),
	Sistema_Geral_Unidade_Medida_Atualizar("Geral", "Unidade de Medida", "Atualizar"),
	Sistema_Geral_Unidade_Medida_Listar("Geral", "Unidade de Medida", "Listar"),

	/********* EMPREENDIMENTO ***********/
	Sistema_Geral_Empreendimento_Salvar("Geral", "Empreendimento", "Salvar"),
	Sistema_Geral_Empreendimento_Excluir("Geral", "Empreendimento", "Excluir"),
	Sistema_Geral_Empreendimento_Atualizar("Geral", "Empreendimento", "Atualizar"),
	Sistema_Geral_Empreendimento_Listar("Geral", "Empreendimento", "Listar"),

	/********* PARAMETRO ***********/
	Sistema_Geral_Parametro_Atualizar("Geral","Parâmetro","Atualizar"),
	Sistema_Geral_Parametro_Salvar("Geral","Parâmetro","Salvar"),
	Sistema_Geral_Parametro_Visualizar("Geral","Parâmetro","Visualizar"),

	/********* BAIRRO ***********/
	Sistema_Geral_Bairro_Excluir("Geral", "Bairro", "Excluir"),
	Sistema_Geral_Bairro_Atualizar("Geral", "Bairro", "Atualizar"),
	Sistema_Geral_Bairro_Salvar("Geral", "Bairro", "Salvar"),
	Sistema_Geral_Bairro_Listar("Geral", "Bairro", "Listar"),

	/********* CIDADE ***********/
	Sistema_Geral_Cidade_Excluir("Geral", "Cidade", "Excluir"),
	Sistema_Geral_Cidade_Atualizar("Geral", "Cidade", "Atualizar"),
	Sistema_Geral_Cidade_Salvar("Geral", "Cidade", "Salvar"),
	Sistema_Geral_Cidade_Listar("Geral", "Cidade", "Listar"),

	/********* LOGRADOURO ***********/
	Sistema_Geral_Logradouro_Excluir("Geral", "Logradouro", "Excluir"),
	Sistema_Geral_Logradouro_Atualizar("Geral", "Logradouro", "Atualizar"),
	Sistema_Geral_Logradouro_Salvar("Geral", "Logradouro", "Salvar"),
	Sistema_Geral_Logradouro_Listar("Geral", "Logradouro", "Listar"),

	/********* UF ***********/
	Sistema_Geral_UF_Excluir("Geral", "UF", "Excluir"),
	Sistema_Geral_UF_Atualizar("Geral", "UF", "Atualizar"),
	Sistema_Geral_UF_Salvar("Geral", "UF", "Salvar"),
	Sistema_Geral_UF_Listar("Geral", "UF", "Listar"),

	/********* ENDERECO ***********/
	Sistema_Geral_Endereco_Excluir("Geral","Endereço","Excluir"),
	Sistema_Geral_Endereco_Atualizar("Geral","Endereço","Atualizar"),
	Sistema_Geral_Endereco_Salvar("Geral","Endereço","Salvar"),
	Sistema_Geral_Endereco_Listar("Geral","Endereço","Listar"),

	/********* GERAR DOCUMENTO ***********/
	Sistema_Geral_Documento_Gerar_Documento("Geral","Documento","Gerar Documentos"),
	Sistema_Geral_Documento_Reimprimir_Documento("Geral","Documento","Re-Impressão de Documentos"),
	Sistema_Geral_Documento_Listar("Geral","Documento","Listar"),
	Sistema_Impressao_Impressao_Documentos("Geral", "Impressão","Impressão de Documentos"),

	/********* INTERESSADO ***********/
	Sistema_Geral_Interessado_Excluir("Geral", "Interessado", "Excluir"),
	Sistema_Geral_Interessado_Atualizar("Geral", "Interessado", "Atualizar"),
	Sistema_Geral_Interessado_Salvar("Geral", "Interessado", "Salvar"),
	Sistema_Geral_Interessado_Listar("Geral", "Interessado", "Listar"),
	Sistema_Geral_Interessado_Visualizar("Geral", "Interessado", "Visualizar"),

	/********* PESSOA FISICA ***********/
	Sistema_Geral_Pessoa_Fisica_Excluir("Geral", "Pessoa Física", "Excluir"),
	Sistema_Geral_Pessoa_Fisica_Atualizar("Geral", "Pessoa Física", "Atualizar"),
	Sistema_Geral_Pessoa_Fisica_Salvar("Geral", "Pessoa Física", "Salvar"),
	Sistema_Geral_Pessoa_Fisica_Listar("Geral", "Pessoa Física", "Listar"),

	/********* PESSOA JURIDICA ***********/
	Sistema_Geral_Pessoa_Juridica_Excluir("Geral", "Pessoa Jurídica", "Excluir"),
	Sistema_Geral_Pessoa_Juridica_Atualizar("Geral", "Pessoa Jurídica", "Atualizar"),
	Sistema_Geral_Pessoa_Juridica_Salvar("Geral", "Pessoa Jurídica", "Salvar"),
	Sistema_Geral_Pessoa_Juridica_Listar("Geral", "Pessoa Jurídica", "Listar"),

	/********* PORTE EMPRESA ***********/
	Sistema_Geral_Porte_Empresa_Excluir("Geral", "Porte Empresarial", "Excluir"),
	Sistema_Geral_Porte_Empresa_Atualizar("Geral", "Porte Empresarial", "Atualizar"),
	Sistema_Geral_Porte_Empresa_Salvar("Geral", "Porte Empresarial", "Salvar"),
	Sistema_Geral_Porte_Empresa_Listar("Geral", "Porte Empresarial", "Listar"),

	/********* IKON ***********/
	Sistema_Geral_Ikon_Excluir("Geral", "Ikon", "Excluir"),
	Sistema_Geral_Ikon_Atualizar("Geral", "Ikon", "Atualizar"),
	Sistema_Geral_Ikon_Salvar("Geral", "Ikon", "Salvar"),
	Sistema_Geral_Ikon_Listar("Geral", "Ikon", "Listar"),

	/********* PROCURADOR ***********/
	Sistema_Geral_Procurador_Excluir("Geral", "Procurador", "Excluir"),
	Sistema_Geral_Procurador_Atualizar("Geral", "Procurador", "Atualizar"),
	Sistema_Geral_Procurador_Salvar("Geral", "Procurador", "Salvar"),
	Sistema_Geral_Procurador_Listar("Geral", "Procurador", "Listar"),

	/********* TECNICO ***********/
	Sistema_Geral_Tecnico_Excluir("Geral","Técnico","Excluir"),
	Sistema_Geral_Tecnico_Atualizar("Geral","Técnico","Atualizar"),
	Sistema_Geral_Tecnico_Salvar("Geral","Técnico","Salvar"),
	Sistema_Geral_Tecnico_Listar("Geral","Técnico","Listar"),

	/********* TIPO TELEFONE ***********/
	Sistema_Geral_Tipo_Telefone_Excluir("Geral", "Tipo de Telefone", "Excluir"),
	Sistema_Geral_Tipo_Telefone_Atualizar("Geral", "Tipo de Telefone", "Atualizar"),
	Sistema_Geral_Tipo_Telefone_Salvar("Geral", "Tipo de Telefone", "Salvar"),
	Sistema_Geral_Tipo_Telefone_Listar("Geral", "Tipo de Telefone", "Listar"),

	/******** ZONAS ********************/
	Sistema_Geral_Zona_Excluir("Geral", "Zonas", "Excluir"),
	Sistema_Geral_Zona_Atualizar("Geral", "Zonas", "Atualizar"),
	Sistema_Geral_Zona_Salvar("Geral", "Zonas", "Salvar"),
	Sistema_Geral_Zona_Listar("Geral", "Zonas", "Listar"),

	/******** SEDE ********************/
	Sistema_Geral_Sede_Excluir("Geral", "Sede", "Excluir"),
	Sistema_Geral_Sede_Atualizar("Geral", "Sede", "Atualizar"),
	Sistema_Geral_Sede_Salvar("Geral", "Sede", "Salvar"),
	Sistema_Geral_Sede_Listar("Geral", "Sede", "Listar"),

	/********* LIVRO DE VISITAS ***********/
	Sistema_Atendimento_Livro_Visita_Salvar("Atendimento", "Livro de Visitas","Salvar"),
	Sistema_Atendimento_Livro_Visita_Listar("Atendimento", "Livro de Visitas","Listar"),
	Sistema_Atendimento_Livro_Visita_Atualizar("Atendimento", "Livro de Visitas", "Atualizar"),
	Sistema_Atendimento_Livro_Visita_Excluir("Atendimento", "Livro de Visitas", "Excluir"),

	/********* CARTÃO DE VISITAS ***********/
	Sistema_Atendimento_Cartao_Visita_Salvar("Atendimento", "Cartão de Visitas","Salvar"),
	Sistema_Atendimento_Cartao_Visita_Listar("Atendimento", "Cartão de Visitas","Listar"),
	Sistema_Atendimento_Cartao_Visita_Atualizar("Atendimento", "Cartão de Visitas", "Atualizar"),
	Sistema_Atendimento_Cartao_Visita_Excluir("Atendimento", "Cartão de Visitas", "Excluir"),

	/********* COEMA ***********************/
	Sistema_Geral_Coema_Excluir("Geral", "Coema", "Excluir"),
	Sistema_Geral_Coema_Atualizar("Geral", "Coema", "Atualizar"),
	Sistema_Geral_Coema_Salvar("Geral", "Coema", "Salvar"),
	Sistema_Geral_Coema_Listar("Geral", "Coema", "Listar"),


	/********* TIPO DOCUMENTO ***********************************************************************/
	Sistema_Geral_Tipo_Documento_Listar("Geral", "Tipo Documento", "Listar"),
	Sistema_Geral_Tipo_Documento_Salvar("Geral", "Tipo Documento","Salvar"),
	Sistema_Geral_Tipo_Documento_Atualizar("Geral", "Tipo Documento","Atualizar"),
	Sistema_Geral_Tipo_Documento_Excluir("Geral", "Tipo Documento","Excluir"),

	/********* TEMPLATE DOCUMENTO ***********************************************************************/
	Sistema_Geral_Template_Documento_Listar("Geral", "Template de Documentos", "Listar"),
	Sistema_Geral_Template_Documento_Salvar("Geral", "Template de Documentos", "Salvar"),
	Sistema_Geral_Template_Documento_Atualizar("Geral", "Template de Documentos", "Atualizar"),
	Sistema_Geral_Template_Documento_Excluir("Geral", "Template de Documentos", "Excluir"),


	/********* QUESTIONARIO ***********/
	Sistema_Geral_Questionario_Salvar("Geral", "Questionário", "Salvar"),
	Sistema_Geral_Questionario_Listar("Geral", "Questionário", "Listar"),
	Sistema_Geral_Questionario_Atualizar("Geral", "Questionário", "Atualizar"),
	Sistema_Geral_Questionario_Excluir("Geral", "Questionário", "Excluir"),
	Sistema_Geral_Questionario_Visualizar("Geral", "Questionário", "Visualizar"),
	/********* ESTADO ***********/
	Sistema_Geral_Estado_Salvar("Geral", "Estado", "Salvar"),
	Sistema_Geral_Estado_Listar("Geral", "Estado", "Listar"),
	Sistema_Geral_Estado_Atualizar("Geral", "Estado", "Atualizar"),
	Sistema_Geral_Estado_Excluir("Geral", "Estado", "Excluir"),


	/********* MUNICIPIO ***********/
	Sistema_Geral_Municipio_Salvar("Geral", "Municipio", "Salvar"),
	Sistema_Geral_Municipio_Listar("Geral", "Municipio", "Listar"),
	Sistema_Geral_Municipio_Atualizar("Geral", "Municipio", "Atualizar"),
	Sistema_Geral_Municipio_Excluir("Geral", "Municipio", "Excluir"),

	/********* REGIÃO METROPOLITANA ***********/
	Sistema_Geral_Regiao_Metropolitana_Salvar("Geral", "Região Metropolitana", "Salvar"),
	Sistema_Geral_Regiao_Metropolitana_Listar("Geral", "Região Metropolitana", "Listar"),
	Sistema_Geral_Regiao_Metropolitana_Atualizar("Geral", "Região Metropolitana", "Atualizar"),
	Sistema_Geral_Regiao_Metropolitana_Excluir("Geral", "Região Metropolitana", "Excluir"),

	/******************************************************************** LICENCIAMENTO *********************************************/


	/********* PROJETO ESTRATÉGICO ***********/
	Sistema_Licenciamento_Projeto_Estrategico_Salvar("Licenciamento", "Projeto Estratégico","Salvar"),
	Sistema_Licenciamento_Projeto_Estrategico_Listar("Licenciamento", "Projeto Estratégico","Listar"),
	Sistema_Licenciamento_Projeto_Estrategico_Atualizar("Licenciamento", "Projeto Estratégico","Atualizar"),
	Sistema_Licenciamento_Projeto_Estrategico_Excluir("Licenciamento", "Projeto Estratégico","Excluir"),

	/********* TIPO REQUERIMENTO***********/
	Sistema_Licenciamento_Tipo_Requerimento_Salvar("Licenciamento", "Tipo de Requerimento","Salvar"),
	Sistema_Licenciamento_Tipo_Requerimento_Excluir("Licenciamento", "Tipo de Requerimento","Excluir"),
	Sistema_Licenciamento_Tipo_Requerimento_Atualizar("Licenciamento", "Tipo de Requerimento","Atualizar"),
	Sistema_Licenciamento_Tipo_Requerimento_Listar("Licenciamento", "Tipo de Requerimento","Listar"),

	/********* TIPO PROCESSO ***********/
	Sistema_Licenciamento_Tipo_Processo_Salvar("Licenciamento", "Tipo de Processo","Salvar"),
	Sistema_Licenciamento_Tipo_Processo_Excluir("Licenciamento", "Tipo de Processo","Excluir"),
	Sistema_Licenciamento_Tipo_Processo_Atualizar("Licenciamento", "Tipo de Processo","Atualizar"),
	Sistema_Licenciamento_Tipo_Processo_Listar("Licenciamento", "Tipo de Processo","Listar"),

	/********* CHECKLIST ***********/
	Sistema_Licenciamento_CheckList_Salvar("Licenciamento", "Checklist","Salvar"),
	Sistema_Licenciamento_CheckList_Excluir("Licenciamento", "Checklist","Excluir"),
	Sistema_Licenciamento_CheckList_Atualizar("Licenciamento", "Checklist","Atualizar"),
	Sistema_Licenciamento_CheckList_Listar("Licenciamento", "Checklist","Listar"),
	Sistema_Licenciamento_CheckList_Clonar("Licenciamento", "Checklist","Clonar"),

	/********* ITENS CHECKLIST ***********/
	Sistema_Licenciamento_Itens_CheckList_Salvar("Licenciamento", "Itens do Checklist","Salvar"),
	Sistema_Licenciamento_Itens_CheckList_Atualizar("Licenciamento", "Itens do Checklist","Atualizar"),
	Sistema_Licenciamento_Itens_CheckList_Excluir("Licenciamento", "Itens do Checklist","Excluir"),
	Sistema_Licenciamento_Itens_CheckList_Listar("Licenciamento", "Itens do Checklist","Listar"),
	Sistema_Licenciamento_Adicionar_Item_Todos_CheckList("Licenciamento","Itens do Checklist","Adicionar Item em Todos CheckList"),


	/********* Grandeza ***********/
	Sistema_Licenciamento_Grandeza_Salvar("Licenciamento", "Grandeza", "Salvar"),
	Sistema_Licenciamento_Grandeza_Listar("Licenciamento", "Grandeza", "Listar"),
	Sistema_Licenciamento_Grandeza_Atualizar("Licenciamento", "Grandeza","Atualizar"),


	/********* Classificação Grandeza ***********/
	Sistema_Licenciamento_Classificacao_Grandeza_Salvar("Licenciamento", "Classificação de Grandeza", "Salvar"),
	Sistema_Licenciamento_Classificacao_Grandeza_Listar("Licenciamento", "Classificação de Grandeza", "Listar"),
	Sistema_Licenciamento_Classificacao_Grandeza_Atualizar("Licenciamento", "Classificação de Grandeza","Atualizar"),

	/********* Tipo de Estudo ***********/
	Sistema_Licenciamento_Tipo_Estudo_Salvar("Licenciamento", "Tipo de Estudo", "Salvar"),
	Sistema_Licenciamento_Tipo_Estudo_Listar("Licenciamento", "Tipo de Estudo", "Listar"),
	Sistema_Licenciamento_Tipo_Estudo_Atualizar("Licenciamento", "Tipo de Estudo","Atualizar"),

	/********* SUBTIPO PROCESSO ***********/
	Sistema_Licenciamento_Subtipo_Processo_Listar("Licenciamento","Subtipo Processo","Listar"),
	Sistema_Licenciamento_Subtipo_Processo_Salvar("Licenciamento","Subtipo Processo","Salvar"),
	Sistema_Licenciamento_Subtipo_Processo_Atualizar("Licenciamento","Subtipo Processo","Atualizar"),

	/********* CONFIGURACAO TIPO PROCESSO ***********/
	Sistema_Licenciamento_Configuracao_Tipo_Processo_Listar("Licenciamento","Configuracao Tipo Processo","Listar"),
	Sistema_Licenciamento_Configuracao_Tipo_Processo_Salvar("Licenciamento","Configuracao Tipo Processo","Salvar"),
	Sistema_Licenciamento_Configuracao_Tipo_Processo_Atualizar("Licenciamento","Configuracao Tipo Processo","Atualizar"),

	/********* MODALIDADE ***********/
	Sistema_Licenciamento_Modalidade_Listar("Licenciamento","Modalidade","Listar"),
	Sistema_Licenciamento_Modalidade_Salvar("Licenciamento","Modalidade","Salvar"),
	Sistema_Licenciamento_Modalidade_Atualizar("Licenciamento","Modalidade","Atualizar"),

	/******** MIGRAÇÂO CHECKLIST ********/
	Sistema_Licenciamento_Migracao_Checklist_Listar("Licenciamento","Migração Checklist","Listar"),
	Sistema_Licenciamento_Configuracao("Licenciamento","Configuracao","Configuracao"),


	/********* PONTO MONITORAMENTO***********/
	Sistema_Laboratorio_PontoMonitoramento_Listar("Laboratório", "Ponto de Monitoramento", "Listar"),
	Sistema_Laboratorio_PontoMonitoramento_Atualizar("Laboratório", "Ponto de Monitoramento", "Atualizar"),
	Sistema_Laboratorio_PontoMonitoramento_Excluir("Laboratório", "Ponto de Monitoramento", "Excluir"),
	Sistema_Laboratorio_PontoMonitoramento_Salvar("Laboratório", "Ponto de Monitoramento", "Salvar"),


	/********* CNAE ************************/

	Sistema_Licenciamento_Cnae_Listar("Licenciamento","Cnae","Listar"),
	Sistema_Licenciamento_Cnae_Atualizar("Licenciamento","Cnae","Atualizar"),
	Sistema_Licenciamento_Cnae_Excluir("Licenciamento","Cnae","Excluir"),
	Sistema_Licenciamento_Cnae_Salvar("Licenciamento","Cnae","Salvar"),

	/******************************************************************** JULGALMENTO *********************************************/

	/********* JULGALMENTO ***********/
	Sistema_Julgamento_Julgamento_Salvar("Julgamento", "Julgamento", "Salvar"),
	Sistema_Julgamento_Julgamento_Suspender("Julgamento", "Julgamento", "Suspender"),
	Sistema_Julgamento_Julgamento_Continuar("Julgamento", "Julgamento", "Continuar"),
	Sistema_Julgamento_Julgamento_Download_File("Julgamento", "Julgamento", "Download File"),
	Sistema_Julgamento_Julgamento_Visualizar("Julgamento", "Julgamento", "Visualizar"),

	/********* PARECER ***********/
	Sistema_Julgamento_Parecer_Salvar("Julgamento", "Parecer", "Salvar"),
	Sistema_Julgamento_Parecer_Atualizar("Julgamento", "Parecer", "Atualizar"),
	Sistema_Julgamento_Parecer_Visualizar("Julgamento", "Parecer", "Visualizar"),
	Sistema_Julgamento_Parecer_Concluir("Julgamento", "Parecer", "Concluir"),
	Sistema_Julgamento_Parecer_Distribuicao_Automatica("Julgamento", "Parecer", "Distribuição Automática"),
	Sistema_Julgamento_Parecer_Distribuicao_Manual("Julgamento", "Parecer", "Distribuição Manual"),

	/********* DECISAO ***********/
	Sistema_Julgamento_Decisao_Salvar("Julgamento", "Decisão", "Salvar"),
	Sistema_Julgamento_Decisao_Listar("Julgamento", "Decisão", "Listar"),
	Sistema_Julgamento_Decisao_Atualizar("Julgamento", "Decisão", "Atualizar"),
	Sistema_Julgamento_Decisao_Imprimir("Julgamento", "Decisão", "Imprimir"),
	Sistema_Julgamento_Decisao_Finalizar("Julgamento", "Decisão", "Finalizar"),
	Sistema_Julgamento_Decisao_Visualizar("Julgamento", "Decisão", "Visualizar"),
	Sistema_Julgamento_Decisao_Distribuicao_Automatica("Julgamento", "Decisão", "Distribuição Automática"),
	Sistema_Julgamento_Decisao_Distribuicao_Manual("Julgamento", "Decisão", "Distribuição Manual"),

	/********* DISTRIBUICAO  ************/
	Sistema_Julgamento_Distribuicao_Automatica("Julgamento", "Distribuição Automática", "Distribuir"),

	/********* COMUNICACAO ***********/
	Sistema_Julgamento_Comunicacao_Salvar("Julgamento", "Comunicação", "Salvar"),
	Sistema_Julgamento_Comunicacao_Atualizar("Julgamento", "Comunicação", "Atualizar"),

	/********* CONFIGURACAO JULGAMENTO***********/
	Sistema_Julgamento_Configuracao_Salvar("Julgamento", "Configuração", "Salvar"),
	Sistema_Julgamento_Configuracao_Atualizar("Julgamento", "Configuração", "Atualizar"),
	Sistema_Julgamento_Configuracao_Visualizar("Julgamento", "Configuração", "Visualizar"),

	/********* ESTENDER PRAZO DEFESA ***********/
	Sistema_Julgamento_Estender_Prazo_Defesa_Salvar("Julgamento", "Prazo de Defesa", "Salvar"),
	Sistema_Julgamento_Estender_Prazo_Defesa_Listar("Julgamento", "Prazo de Defesa", "Listar"),

	/********* ESTENDER PRAZO ALEGAÇÃO ***********/
	Sistema_Julgamento_Estender_Prazo_Alegacao_Salvar("Julgamento", "Prazo de Alegação", "Salvar"),
	Sistema_Julgamento_Estender_Prazo_Alegacao_Listar("Julgamento", "Prazo de Alegação", "Listar"),

	/********* DAE ***********/
	Sistema_Julgamento_DAE_Gerar("Julgamento", "DAE", "Salvar"),
	Sistema_Julgamento_DAE_Excluir("Julgamento", "DAE", "Excluir"),
	Sistema_Julgamento_DAE_Imprimir("Julgamento", "DAE", "Imprimir"),
	Sistema_Julgamento_DAE_Gerar_Segunda_Via("Julgamento", "DAE", "2ª Via"),

	/********* DAE Avulso ***********/
	Sistema_Julgamento_DAE_Avulso_Gerar("Julgamento", "DAE Avulso", "Salvar"),
	Sistema_Julgamento_DAE_Avulso_Listar("Julgamento", "DAE Avulso", "Listar"),
	Sistema_Julgamento_DAE_Avulso_Excluir("Julgamento", "DAE Avulso", "Excluir"),
	Sistema_Julgamento_DAE_Avulso_Imprimir("Julgamento", "DAE Avulso", "Imprimir"),
	Sistema_Julgamento_DAE_Avulso_Gerar_Segunda_Via("Julgamento", "DAE Avulso","2ª Via"),

	/********* MAJORACAO ***********/
	Sistema_Julgamento_Majoracao_Listar("Julgamento", "Majoração", "Listar"),
	Sistema_Julgamento_Majoracao_Excluir("Julgamento", "Majoração", "Excluir"),
	Sistema_Julgamento_Majoracao_Salvar("Julgamento", "Majoração", "Salvar"),
	Sistema_Julgamento_Majoracao_Atualizar("Julgamento", "Majoração", "Atualizar"),

	/******** RETRATAÇÃO ***********/
	Sistema_Julgamento_Retratacao_Salvar("Julgamento", "Reconsideração", "Salvar"),
	Sistema_Julgamento_Retratacao_Atualizar("Julgamento", "Reconsideração", "Atualizar"),
	Sistema_Julgamento_Retratacao_Finalizar("Julgamento", "Reconsideração", "Finalizar"),

	/******************************************************************** JURIDICO *********************************************/

	/********* TERMO DE PARCELAMENTO DE COMPENSAÇÃO AMBIENTAL ***********************************************************************/
	Sistema_Juridico_Parcelamento_Compensacao_Ambiental_Listar("Jurídico", "Parcelamento de Compensação Ambiental", "Listar"),
	Sistema_Juridico_Parcelamento_Compensacao_Ambiental_Salvar("Jurídico", "Parcelamento de Compensação Ambiental", "Salvar"),
	Sistema_Juridico_Parcelamento_Compensacao_Ambiental_Atualizar("Jurídico", "Parcelamento de Compensação Ambiental", "Atualizar"),
	Sistema_Juridico_Parcelamento_Compensacao_Ambiental_Excluir("Jurídico", "Parcelamento de Compensação Ambiental", "Excluir"),
	Sistema_Juridico_Parcelamento_Compensacao_Ambiental_Imprimir("Jurídico", "Parcelamento de Compensação Ambiental", "Imprimir"),
	Sistema_Juridico_Parcelamento_Compensacao_Ambiental_Finalizar_Termo("Jurídico", "Parcelamento de Compensação Ambiental", "Finalizar Termo"),
	Sistema_Juridico_Parcelamento_Compensacao_Ambiental_Excluir_Dae("Jurídico", "Parcelamento de Compensação Ambiental", "Excluir DAE"),

	/********* TERMO DE PARCELAMENTO DE AUTO DE INFRAÇÃO ***********************************************************************/
	Sistema_Juridico_Parcelamento_Auto_Infracao_Listar("Jurídico", "Parcelamento de Auto de Infração", "Listar"),
	Sistema_Juridico_Parcelamento_Auto_Infracao_Salvar("Jurídico", "Parcelamento de Auto de Infração", "Salvar"),
	Sistema_Juridico_Parcelamento_Auto_Infracao_Atualizar("Jurídico", "Parcelamento de Auto de Infração", "Atualizar"),
	Sistema_Juridico_Parcelamento_Auto_Infracao_Excluir("Jurídico", "Parcelamento de Auto de Infração", "Excluir"),
	Sistema_Juridico_Parcelamento_Auto_Infracao_Imprimir("Jurídico", "Parcelamento de Auto de Infração", "Imprimir"),
	Sistema_Juridico_Parcelamento_Auto_Infracao_Finalizar_Termo("Jurídico", "Parcelamento de Auto de Infração", "Finalizar Termo"),
	Sistema_Juridico_Parcelamento_Auto_Infracao_Excluir_Dae("Jurídico", "Parcelamento de Auto de Infração", "Excluir DAE"),

	/********* ACORDO DE PARCELAMENTO DE DIVIDA ATIVA ***********************************************************************/
	Sistema_Juridico_Parcelamento_Divida_Ativa_Listar("Jurídico", "Parcelamento de Dívida Ativa", "Listar"),
	Sistema_Juridico_Parcelamento_Divida_Ativa_Salvar("Jurídico", "Parcelamento de Dívida Ativa", "Salvar"),
	Sistema_Juridico_Parcelamento_Divida_Ativa_Atualizar("Jurídico", "Parcelamento de Dívida Ativa", "Atualizar"),
	Sistema_Juridico_Parcelamento_Divida_Ativa_Excluir("Jurídico", "Parcelamento de Dívida Ativa", "Excluir"),
	Sistema_Juridico_Parcelamento_Divida_Ativa_Imprimir("Jurídico", "Parcelamento de Dívida Ativa", "Imprimir"),
	Sistema_Juridico_Parcelamento_Divida_Ativa_Finalizar_Acordo("Jurídico", "Parcelamento de Dívida Ativa", "Finalizar Acordo"),
	Sistema_Juridico_Parcelamento_Divida_Ativa_Excluir_Dae("Jurídico", "Parcelamento de Dívida Ativa", "Excluir DAE"),

	/********* GERAÇÃO DE DOCUMENTOS ***********/
	Sistema_Juridico_Documentos_CDA_Gerar("Jurídico", "Documentos de Dívida Ativa", "Gerar Documento CDA"),
	Sistema_Juridico_Documentos_TIDA_Gerar("Jurídico", "Documentos de Dívida Ativa", "Gerar Documento TIDA"),
	Sistema_Juridico_Documentos_TRIDA_Gerar("Jurídico", "Documentos de Dívida Ativa", "Gerar Documento TRIDA"),
	Sistema_Juridico_Documentos_Peticao_Gerar("Jurídico", "Documentos de Dívida Ativa", "Gerar Documento Peticao Inicial"),

	/********* ORIGEM DIVIDA ***********/
	Sistema_Juridico_Origem_Divida_Listar("Jurídico","Origem Divida","Listar"),
	Sistema_Juridico_Origem_Divida_Salvar("Jurídico","Origem Divida","Salvar"),
	Sistema_Juridico_Origem_Divida_Atualizar("Jurídico","Origem Divida","Atualizar"),

	/********* CONFIGURAÇÕES ***********/
	Sistema_Juridico_Configuracao_Divida_Atualizar("Jurídico","Configuração","Atualizar"),

	/********* Indices ***********/
	Sistema_Juridico_Indices_Listar("Jurídico","Índices","Listar"),

	/********* IGPDI ***********/
	Sistema_Juridico_IGPDI_Listar("Jurídico","IGPDI","Listar"),
	Sistema_Juridico_IGPDI_Salvar("Jurídico","IGPDI","Salvar"),
	Sistema_Juridico_IGPDI_Atualizar("Jurídico","IGPDI","Atualizar"),
	Sistema_Juridico_IGPDI_Visualizar_Historico("Jurídico","IGPDI","Visualizar Histórico"),

	/********* DIVIDA ATIVA ***********/
	Sistema_Juridico_Divida_Ativa_Listar("Jurídico","Dívida Ativa","Listar"),
	Sistema_Juridico_Divida_Ativa_Listar_Siga("Jurídico","Dívida Ativa Siga","Listar"),
	Sistema_Juridico_Divida_Ativa_Salvar("Jurídico","Dívida Ativa","Salvar"),
	Sistema_Juridico_Divida_Ativa_Atualizar("Jurídico","Dívida Ativa","Atualizar"),
	Sistema_Juridico_Divida_Ativa_Execucao_Fiscal("Jurídico","Dívida Ativa","Gerar Execução Fiscal"),
	Sistema_Juridico_Divida_Ativa_Migracao("Jurídico","Dívida Ativa","Migrar Dívida"),

	/********* RELATORIOS ***********/
	Sistema_Juridico_Relatorio_Geral_Divida("Jurídico","Relatórios","Relatório Geral da Dívida Ativa"),

	/********* CO RESPONSAVEL ***********/
	Sistema_Juridico_Co_Responsavel_Listar("Jurídico","Co Responsavel","Listar"),
	Sistema_Juridico_Co_Responsavel_Salvar("Jurídico","Co Responsavel","Salvar"),
	Sistema_Juridico_Co_Responsavel_Atualizar("Jurídico","Co Responsavel","Atualizar"),

	/********* PAGAMENTOS ***********/
	Sistema_Juridico_Pagamentos_Visualizar("Jurídico","Pagamentos","Visualizar"),
	Sistema_Juridico_Pagamentos_Quitacao("Jurídico","Pagamentos","Quitação"),
	Sistema_Juridico_Pagamentos_Gerar_Acordo_Parcelamento("Jurídico","Pagamentos","Gerar Acordo de Parcelamento"),
	Sistema_Juridico_Pagamentos_Migrar_Acordo_Parcelamento("Jurídico","Pagamentos","Migrar Acordo de Parcelamento"),
	Sistema_Juridico_Pagamentos_Visualizar_Acordo_Parcelamento("Jurídico","Pagamentos","Visualizar Acordo de Parcelamento"),
	Sistema_Juridico_Pagamentos_Cancelar_Acordo_Parcelamento("Jurídico","Pagamentos","Cancelar Acordo de Parcelamento"),
	Sistema_Juridico_Pagamentos_Imprimir_Acordo_Parcelamento("Jurídico","Pagamentos","Imprimir Acordo de Parcelamento"),
	Sistema_Juridico_Pagamentos_Dae_Imprimir("Jurídico","Pagamentos","Imprimir Dae"),
	Sistema_Juridico_Pagamentos_Dae_Gerar_Segunda_Via("Jurídico","Pagamentos","Gerar segunda via Dae"),
	Sistema_Juridico_Pagamentos_Dae_Cancelar("Jurídico","Pagamentos","Cancelar Dae"),
	Sistema_Juridico_Pagamentos_Mensageria_DAE_Pago("Jurídico","Pagamentos - Mensageria","Dae Quitação Pago"),
	Sistema_Juridico_Pagamentos_Mensageria_DAE_Vencido("Jurídico","Pagamentos - Mensageria","Dae Quitação Vencido"),
	Sistema_Juridico_Pagamentos_Mensageria_Entrada_Acordo_Pago("Jurídico","Pagamentos - Mensageria","Dae Entrada Acordo Pago"),
	Sistema_Juridico_Pagamentos_Mensageria_Entrada_Acordo_Vencido("Jurídico","Pagamentos - Mensageria","Dae Entrada Acordo Vencido"),
	Sistema_Juridico_Pagamentos_Mensageria_Acordo_Cancelado("Jurídico","Pagamentos - Mensageria","Acordo Cancelado"),
	Sistema_Juridico_Pagamentos_Mensageria_Acordo_Finalizado("Jurídico","Pagamentos - Mensageria","Acordo Finalizado"),
	Sistema_Juridico_Pagamentos_Mensageria_Acordo_Em_Atraso("Jurídico","Pagamentos - Mensageria","Acordo em Atraso"),
	Sistema_Juridico_Pagamentos_Mensageria_Acordo_Regularizado("Jurídico","Pagamentos - Mensageria","Acordo Regularizado"),

	/********* CALCULADORA ***********/
	Sistema_Juridico_Calculadora("Jurídico","Calculadora","Calculadora Manual"),

	/********* DIVIDA ATIVA ***********/
	Sistema_Juridico_Livro_Divida_Ativa_Listar("Jurídico","Livro da Dívida Ativa","Listar"),

	/********* SELIC ***********/
	Sistema_Juridico_Selic_Listar("Jurídico","Selic","Listar"),
	Sistema_Juridico_Selic_Salvar("Jurídico","Selic","Salvar"),
	Sistema_Juridico_Selic_Atualizar("Jurídico","Selic","Atualizar"),



	/******************************************************************** SISTEMA PROTOCOLO *********************************************/

	/********* PAUTA ***********************************************************************/
	Sistema_Protocolo_Pauta_Listar("Protocolo", "Pauta", "Listar"),

	/********* DOC ANEXO ***********************************************************************/
	Sistema_Protocolo_Doc_Anexo_Listar("Protocolo", "DocAnexo", "Listar"),
	Sistema_Protocolo_Doc_Anexo_Salvar("Protocolo", "DocAnexo","Salvar"),
	Sistema_Protocolo_Doc_Anexo_Cancelar("Protocolo", "DocAnexo","Cancelar"),
	Sistema_Protocolo_Doc_Anexo_Imprimir("Protocolo", "Doc Anexo","Imprimir"),

	/********* TIPO ANEXO ***********************************************************************/
	Sistema_Protocolo_Tipo_Anexo_Listar("Protocolo", "Tipo Anexo", "Listar"),
	Sistema_Protocolo_Tipo_Anexo_Salvar("Protocolo", "Tipo Anexo","Salvar"),
	Sistema_Protocolo_Tipo_Anexo_Atualizar("Protocolo", "Tipo Anexo","Atualizar"),
	Sistema_Protocolo_Tipo_Anexo_Excluir("Protocolo", "Tipo Anexo","Excluir"),

	/********* PROCESSO ***********************************************************************/
	Sistema_Protocolo_Processo_Listar("Protocolo", "Processo", "Listar"),
	Sistema_Protocolo_Processo_Salvar("Protocolo", "Processo","Salvar"),
	Sistema_Protocolo_Processo_Atualizar("Protocolo", "Processo","Atualizar"),
	Sistema_Protocolo_Processo_Cancelar("Protocolo", "Processo","Cancelar"),

	/********* PROVISORIO ***********************************************************************/
	Sistema_Protocolo_Processo_Provisorio_Listar("Protocolo", "Processo Provisorio", "Listar"),

	/********* DIGITALIZADOS ***********************************************************************/
	Sistema_Protocolo_Digitalizados_Listar("Protocolo", "Digitalizados", "Listar"),

	/********* Doc Avulso ***********************************************************************/
	Sistema_Protocolo_Doc_Avulso_Listar("Protocolo", "Doc Avulso", "Listar"),
	Sistema_Protocolo_Doc_Avulso_Salvar("Protocolo", "Doc Avulso","Salvar"),
	Sistema_Protocolo_Doc_Avulso_Atualizar("Protocolo", "Doc Avulso","Atualizar"),
	Sistema_Protocolo_Doc_Avulso_Excluir("Protocolo", "Doc Avulso","Excluir"),
	Sistema_Protocolo_Doc_Avulso_Cancelar("Protocolo", "Doc Avulso","Cancelar"),
	Sistema_Protocolo_Doc_Avulso_Imprimir("Protocolo", "Doc Avulso","Imprimir"),

	/********* AVISO RECEBIMENTO ***********************************************************************/
	Sistema_Protocolo_Aviso_Recebimento_Listar("Protocolo", "Aviso de Recebimento", "Listar"),
	Sistema_Protocolo_Aviso_Recebimento_Salvar("Protocolo", "Aviso de Recebimento","Salvar"),
	Sistema_Protocolo_Aviso_Recebimento_Atualizar("Protocolo", "Aviso de Recebimento","Atualizar"),
	Sistema_Protocolo_Aviso_Recebimento_Excluir("Protocolo", "Aviso de Recebimento","Excluir"),

	/****** Comunidado Interno ************************************************************************/
	Sistema_Protocolo_Comunicado_Interno_Listar("Protocolo", "Comunicação Interna", "Listar"),
	Sistema_Protocolo_Comunicado_Interno_Salvar("Protocolo", "Comunicação Interna","Salvar"),
	Sistema_Protocolo_Comunicado_Interno_Atualizar("Protocolo", "Comunicação Interna","Atualizar"),
	Sistema_Protocolo_Comunicado_Interno_Cancelar("Protocolo", "Comunicação Interna","Cancelar"),
	Sistema_Protocolo_Comunicado_Interno_Excluir("Protocolo", "Comunicação Interna","Excluir"),

	/****** Consulta de Processo ************************************************************************/
	Sistema_Protocolo_Consultar_Processo_Listar("Protocolo", "Consulta de Processo", "Listar"),


	/********* CONFIGURACAO PROTOCOLO ***********/
	Sistema_Protocolo_Configuracao_Protocolo_Salvar("Protocolo","Configuração Protocolo","Salvar"),

	/********* REMETENTE OFICIO ***********/
	Sistema_Protocolo_Remetente_Oficio_Listar("Protocolo","Remetente de Ofício","Listar"),
	Sistema_Protocolo_Remetente_Oficio_Salvar("Protocolo","Remetente de Ofício","Salvar"),
	Sistema_Protocolo_Remetente_Oficio_Atualizar("Protocolo","Remetente de Ofício","Atualizar"),


	/********* MONITORAMENTO OFICIO ***********/
	Sistema_Protocolo_Monitoramento_Oficio_Listar("Protocolo", "Monitoramento de Ofícios", "Listar"),
	Sistema_Protocolo_Monitoramento_Oficio_Salvar("Protocolo", "Monitoramento de Ofícios", "Salvar"),
	Sistema_Protocolo_Monitoramento_Oficio_Atualizar("Protocolo", "Monitoramento de Ofícios", "Atualizar"),
	Sistema_Protocolo_Monitoramento_Oficio_Excluir("Protocolo", "Monitoramento de Ofícios", "Excluir"),
	Sistema_Protocolo_Monitoramento_Oficio_Visualizar_Detalhes("Protocolo", "Monitoramento de Ofícios", "Visualizar Detalhes"),
	Sistema_Protocolo_Monitoramento_Oficio_Finalizar("Protocolo", "Monitoramento de Ofícios", "Finalizar"),

	/********* SINCRONIA VIPROC ***********/
	Sistema_Protocolo_Sincronia_Viproc_Listar("Protocolo", "Sincronia ViProc", "Listar"),
	Sistema_Protocolo_Sincronia_Viproc_Salvar("Protocolo", "Sincronia ViProc", "Salvar"),
	Sistema_Protocolo_Sincronia_Viproc_Atualizar("Protocolo", "Sincronia ViProc", "Atualizar"),
	Sistema_Protocolo_Sincronia_Viproc_Excluir("Protocolo", "Sincronia ViProc", "Excluir"),
	Sistema_Protocolo_Sincronia_Viproc_Visualizar_Detalhes("Protocolo", "Sincronia ViProc", "Visualizar Detalhes"),

	/********* DESPACHO SANEADOR ************/
	Sistema_Julgamento_Despacho_Saneador_Salvar("Julgamento", "Despacho Saneador", "Salvar"),
	Sistema_Julgamento_Despacho_Saneador_Listar("Julgamento", "Despacho Saneador", "Listar"),
	Sistema_Julgamento_Despacho_Saneador_Imprimir("Julgamento", "Despacho Saneador", "Imprimir"),

	/******** PENDÊNCIAS ***********/
	Sistema_Protocolo_Pendencia_Processo_listar("Protocolo", "Pendência de Processo","Listar"),
	Sistema_Protocolo_Pendencia_Salvar("Protocolo", "Pendência de Processo","Salvar"),
	Sistema_Protocolo_Pendencia_Processo_Atualizar("Protocolo", "Pendência de Processo","Atualizar"),
	Sistema_Protocolo_Pendencia_Processo_Excluir("Protocolo", "Pendência de Processo","Excluir"),
	Sistema_Protocolo_Pendencia_Processo_Visualizar_Detalhes("Protocolo", "Pendência de Processo", "Visualizar Detalhes"),
	Sistema_Protocolo_Pendencia_Processo_Gerenciar("Atendimento", "Pendência", "Gerenciar Pendência"),

	/******************************************************************** SISTEMA PRODUTIVIDADE *********************************************/

	/****** Produtividade ************************************************************************/
	Sistema_Produtividade_Atividade_Produtividade_Listar("Produtividade", "Atividade de Produtividade", "Listar"),
	Sistema_Produtividade_Atividade_Produtividade_Salvar("Produtividade", "Atividade de Produtividade", "Salvar"),
	Sistema_Produtividade_Atividade_Produtividade_Atualizar("Produtividade", "Atividade de Produtividade", "Atualizar"),
	Sistema_Produtividade_Atividade_Produtividade_Excluir("Produtividade", "Atividade de Produtividade", "Excluir"),

	Sistema_Produtividade_Peso_Atividade_Listar("Produtividade", "Peso Atividade", "Listar"),
	Sistema_Produtividade_Peso_Atividade_Salvar("Produtividade", "Peso Atividade", "Salvar"),
	Sistema_Produtividade_Peso_Atividade_Atualizar("Produtividade", "Peso Atividade", "Atualizar"),
	Sistema_Produtividade_Peso_Atividade_Excluir("Produtividade", "Peso Atividade", "Excluir"),

	Sistema_Produtividade_Produtividade_Listar("Produtividade", "Produtividade", "Listar"),
	Sistema_Produtividade_Produtividade_Listar_Todos("Produtividade", "Produtividade", "Listar Todos"),
	Sistema_Produtividade_Produtividade_Salvar("Produtividade", "Produtividade", "Salvar"),
	Sistema_Produtividade_Produtividade_Atualizar("Produtividade", "Produtividade", "Atualizar"),
	Sistema_Produtividade_Produtividade_Excluir("Produtividade", "Produtividade", "Excluir"),
	Sistema_Produtividade_Produtividade_Add_Fechado("Produtividade", "Produtividade", "Adicionar Produtividade Retroativa"),


	/******************************************************************** SISTEMA CONTRATOS *********************************************/

	/********* CONTRATO ***********/
	Sistema_Contrato_Contrato_Listar("Contrato","Contrato","Listar"),
	Sistema_Contrato_Contrato_Salvar("Contrato","Contrato","Salvar"),
	Sistema_Contrato_Contrato_Atualizar("Contrato","Contrato","Atualizar"),
	Sistema_Contrato_Contrato_Controlar_Vigencia("Contrato","Contrato","Controlar Vigência"),
	Sistema_Contrato_Contrato_Alterar_Status_Contrato("Contrato","Contrato","Alterar Status Contrato"),
	Sistema_Contrato_Contrato_Visualizar_Detalhes("Contrato","Contrato","Visualizar Detalhes"),

	/********* OBJETO ***********/
	Sistema_Contrato_Objeto_Contrato_Listar("Contrato","Objeto do Contrato","Listar"),
	Sistema_Contrato_Objeto_Contrato_Salvar("Contrato","Objeto do Contrato","Salvar"),
	Sistema_Contrato_Objeto_Contrato_Atualizar("Contrato","Objeto do Contrato","Atualizar"),

	/********* TIPO CONTRATO ***********/
	Sistema_Contrato_Tipo_Contrato_Listar("Contrato","Tipo Contrato","Listar"),
	Sistema_Contrato_Tipo_Contrato_Salvar("Contrato","Tipo Contrato","Salvar"),
	Sistema_Contrato_Tipo_Contrato_Atualizar("Contrato","Tipo Contrato","Atualizar"),

	/********* TIPO ALTERACAO CONTRATUAL ***********/
	Sistema_Contrato_Tipo_Alteracao_Contratual_Listar("Contrato","Tipo Alteracao Contratual","Listar"),
	Sistema_Contrato_Tipo_Alteracao_Contratual_Salvar("Contrato","Tipo Alteracao Contratual","Salvar"),
	Sistema_Contrato_Tipo_Alteracao_Contratual_Atualizar("Contrato","Tipo Alteracao Contratual","Atualizar"),

	/********* TIPO ALTERACAO CONTRATUAL ***********/
	Sistema_Contrato_Alteracao_Contratual_Salvar("Contrato","Alteracao Contratual","Salvar"),

	/********* FORMA CONTRATUAL ***********/
	Sistema_Contrato_Forma_Contratual_Listar("Contrato","Forma Contratual","Listar"),
	Sistema_Contrato_Forma_Contratual_Salvar("Contrato","Forma Contratual","Salvar"),
	Sistema_Contrato_Forma_Contratual_Atualizar("Contrato","Forma Contratual","Atualizar"),


	/******************************************************************** PLANO DE VIAGEM *********************************************/

	/********* CONFIGURAÇÃO DA SOLICITAÇÃO DE VIAGEM ***********/
	Sistema_Plano_Viagem_Configuracao_Solicitacao_Viagem_Atualizar("Plano de Viagem","Configuração da Solicitação de Viagem","Atualizar"),

	/********* SOLICITAÇÃO DE VIAGEM ***********/
	Sistema_Plano_Viagem_Solicitacao_Viagem_Listar("Plano de Viagem","Solicitação de Viagem","Listar"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Salvar("Plano de Viagem","Solicitação de Viagem","Salvar"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Atualizar("Plano de Viagem","Solicitação de Viagem","Atualizar"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Visualizar_Setor_Pai("Plano de Viagem","Solicitação de Viagem","Visualizar Solicitações do Setor Pai"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Aprovar("Plano de Viagem","Solicitação de Viagem","Aprovar"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Atualizar_RH("Plano de Viagem","Solicitação de Viagem","Atualizar pelo RH"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Iniciar("Plano de Viagem","Solicitação de Viagem","Iniciar Solicitação"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Finalizar("Plano de Viagem","Solicitação de Viagem","Finalizar Solicitação"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Salvar_Rascunho("Plano de Viagem","Solicitação de Viagem","Salvar Rascunho"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Aprovar_Rascunho("Plano de Viagem","Solicitação de Viagem","Aprovar Rascunho"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Visualizar_Outros_Setores("Plano de Viagem","Solicitação de Viagem","Visualizar Solicitações de Outros Setores"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Imprimir("Plano de Viagem","Solicitação de Viagem","Imprimir Solicitação"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Mensageiria_Aguardando_Aprovacao("Plano de Viagem", "Solicitação Viagem", "Enviar Mensagem Aguardando Aprovação"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Mensageiria_Realizada("Plano de Viagem", "Solicitação Viagem", "Enviar Mensagem Realizada"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Mensageiria_Cancelada("Plano de Viagem", "Solicitação Viagem", "Enviar Mensagem Cancelada"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Retornar_Status("Plano de Viagem", "Solicitacao Viagem", "Retornar Status"),

	/********* RELATÓRIO ***********/
	Sistema_Plano_Viagem_Solicitacao_Viagem_Relatorio_Funcionario("Plano de Viagem","Relatório","Gerencial de Viagens"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Relatorio_Ponto("Plano de Viagem","Relatório","Justificativa de Ponto"),
	Sistema_Plano_Viagem_Solicitacao_Viagem_Relatorio_Terceirizado("Plano de Viagem","Relatório","Pagamento de Diárias"),

	/********* LANÇAMENTO ***********/
	Sistema_Plano_Viagem_Lancamento_Listar("Plano de Viagem","Lançamento","Listar"),
	Sistema_Plano_Viagem_Lancamento_Salvar("Plano de Viagem","Lançamento","Salvar"),
	Sistema_Plano_Viagem_Lancamento_Atualizar("Plano de Viagem","Lançamento","Atualizar"),
	Sistema_Plano_Viagem_Lacamento_Estornar("Plano de Viagem", "Lançamento", "Estornar"),
;


	private String modulo;
	private String displayName;
	private String aplicacao;

	/**
	 * Considerar aplicação, módulo e displayname passados como parâmetro
	 * @param modulo
	 * @param displayName
	 */
	private Role( String aplicacao, String modulo, String displayName) {
		this.modulo = modulo;
		this.displayName = displayName;
		this.aplicacao = aplicacao;
	}

	/**
	 * Informa a {@link Role} de acordo com o {@link #name()}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 03/11/2014 15:40:45
	 *
	 * @param name name da {@link Role} a ser consultado
	 *
	 * @return Retorna a {@link Role} de acordo com o {@link #name()}
	 */
    public static Role fromName(final String name){
    	return valueOf(Role.class, name);
    }

	public String getModulo() {
		return this.modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}


	public String getAplicacao() {
		return this.aplicacao;
	}

	/**
	 * Informa a descrição completa do {@link Role}
	 *
	 * @author renearaujo - René Araújo Vasconcelos [renearaujo@gmail.com] - 05/11/2014 15:19:21
	 *
	 * @return Retorna uma {@link String} com o nome da {@link Role#aplicacao}, {@link Role#modulo} e {@link Role#displayName}
	 */
	public String getDescricaoCompleta() {
		return this.aplicacao + " - " + this.modulo + " - " + this.displayName;
	}

	public void setAplicacao(String aplicacao) {
		this.aplicacao = aplicacao;
	}

	public static Role findRole(String displayName){
		Role r = null;
		for(Role role: Role.values()){
			if(role.name().equals(displayName)){
				r = role;
				break;
			}
		}
		return r;
	}

	public static Map<String, Role> getAplicacoes(){
		Map<String, Role> mapModule = new TreeMap<>();
		for(Role role: Role.values()) {
			mapModule.put(role.aplicacao, role);
		}
		return mapModule;
	}

	public static Map<String, Role> getModulos(){
		Map<String, Role> mapModule = new TreeMap<>();
		for(Role role: Role.values()) {
			mapModule.put(role.modulo, role);
		}
		return mapModule;
	}

	public static List<Role> getRolesByAplicacao(String aplicacao){
		Map<String, Role> mapModule = new TreeMap<>();
		List<Role> roles = new ArrayList<>();
		for(Role role: Role.values()){
			if(role.aplicacao.equals(aplicacao)) {
				mapModule.put(role.modulo, role);
			}
		}
		roles.addAll(mapModule.values());
		Collections.sort(roles, new RoleComparator());
		return roles;
	}

	public static List<Role> getRolesByModulo(String aplicacao, String modulo){
		List<Role> roles = new ArrayList<>();
		for(Role role: Role.values()){
			if(role.aplicacao.equals(aplicacao) && role.modulo.equals(modulo)) {
				roles.add(role);
			}
		}
		Collections.sort(roles, new RoleComparator());
		return roles;
	}

	public static String getModuloByRoleAplicacao(Role roleAplicacao){
		String modulo = null;
		for(Role role: Role.values()) {
			if(role.modulo.equals(roleAplicacao.getModulo())){
				modulo = role.modulo;
			}

		}
		return modulo;
	}

	public static String getModuloByRole(Role roleModulo){
		String modulo = null;
		for(Role role: Role.values()) {
			if(role.modulo.equals(roleModulo.getModulo())){
				modulo = role.modulo;
			}

		}
		return modulo;
	}

	public static Map<String,Role> aplicacoesByPerfil(List<String> permissoesArray) {
		Map<String, Role> mapModule = new TreeMap<>();

		for(String permissao: permissoesArray){
			Role role = findRole(permissao);
			mapModule.put(role.aplicacao, role);
		}
		return mapModule;
	}

	public static Map<String,Role> modulosByPerfil(String[] permissoesArray) {
		Map<String, Role> mapModule = new TreeMap<>();

		for(String permissao: permissoesArray){
			Role role = findRole(permissao);
			mapModule.put(role.modulo, role);
		}
		return mapModule;
	}

	public static List<Role> getRolesByAplicacaoPerfil(String aplicacao, String permissoes){
		Map<String, Role> mapModule = new TreeMap<>();
		List<Role> roles = new ArrayList<>();
		for(Role role: Role.values()){
			if(role.aplicacao.equals(aplicacao) && permissoes.contains(role.name())) {
				mapModule.put(role.modulo, role);
			}
		}
		roles.addAll(mapModule.values());
		Collections.sort(roles, new RoleComparator());
		return roles;
	}

	public static List<Role> getRolesByModuloPerfil(String aplicacao, String modulo, String permissoes) {
		List<Role> roles = new ArrayList<>();
		for(Role role: Role.values()){
			if(role.aplicacao.equals(aplicacao) && role.modulo.equals(modulo) && permissoes.contains(role.name())) {
				roles.add(role);
			}
		}
		Collections.sort(roles, new RoleComparator());
		return roles;
	}

}
