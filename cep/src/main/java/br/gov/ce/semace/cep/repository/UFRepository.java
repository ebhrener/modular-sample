package br.gov.ce.semace.cep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.gov.ce.semace.base.model.UF;

public interface UFRepository extends JpaRepository<UF, Long>, JpaSpecificationExecutor<UF> {

}
