package br.gov.ce.semace.cep.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.semace.base.generic.GenericController;
import br.gov.ce.semace.base.model.UF;
import br.gov.ce.semace.base.util.Response;
import br.gov.ce.semace.cep.service.UFService;

@RestController
@RequestMapping("/write/uf")
@RefreshScope
public class UFController extends GenericController<UF, Long>{

	@Autowired
	public UFController(UFService service) {
		super(service);
	}
	
	@Autowired
	protected UFService service;
	
	@GetMapping
	public ResponseEntity<Response> list(@RequestParam(required = false) Map<String, String> filters) {		
		try {
			Page<UF> result = service.list(filters);
			if (result == null) {
				return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList("Sem resultados")), HttpStatus.NOT_FOUND);
			}
			
			return new ResponseEntity<Response>(new Response (Boolean.TRUE, result, Arrays.asList("Sucesso")), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList(e.getMessage())), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
