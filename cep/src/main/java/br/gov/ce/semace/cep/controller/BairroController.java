package br.gov.ce.semace.cep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.semace.base.generic.GenericController;
import br.gov.ce.semace.base.model.Bairro;
import br.gov.ce.semace.cep.service.BairroService;

@RestController
@RequestMapping("/write/bairro")
@RefreshScope
public class BairroController extends GenericController<Bairro, Long>{

	@Autowired
	public BairroController(BairroService service) {
		super(service);
	}
}
