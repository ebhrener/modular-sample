package br.gov.ce.semace.cep.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.gov.ce.semace.base.exception.ValidacaoException;
import br.gov.ce.semace.base.generic.GenericServiceImpl;
import br.gov.ce.semace.base.model.Endereco;
import br.gov.ce.semace.cep.repository.EnderecoRepository;
import br.gov.ce.semace.cep.service.EnderecoService;

@Service
public class EnderecoServiceImpl extends GenericServiceImpl<Endereco, Long> implements EnderecoService  {

	public EnderecoServiceImpl(JpaRepository<Endereco, Long> repository) {
		super(repository);
	}
	
	@Autowired
	private EnderecoRepository repository;

	
	@Override
	public Optional<Endereco> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public Endereco save(Endereco t) throws ValidacaoException {
		return repository.save(t);
	}

	@Override
	public void delete(Endereco t) throws ValidacaoException {
		super.delete(t);
	}
}
