package br.gov.ce.semace.cep.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.gov.ce.semace.base.exception.ValidacaoException;
import br.gov.ce.semace.base.generic.GenericServiceImpl;
import br.gov.ce.semace.base.model.Logradouro;
import br.gov.ce.semace.cep.repository.LogradouroRepository;
import br.gov.ce.semace.cep.service.LogradouroService;

@Service
public class LogradouroServiceImpl extends GenericServiceImpl<Logradouro, Long> implements LogradouroService  {

	public LogradouroServiceImpl(JpaRepository<Logradouro, Long> repository) {
		super(repository);
	}
	
	@Autowired
	private LogradouroRepository repository;

	
	@Override
	public Optional<Logradouro> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public Logradouro save(Logradouro t) throws ValidacaoException {
		return repository.save(t);
	}

	@Override
	public void delete(Logradouro t) throws ValidacaoException {
		super.delete(t);
	}
}
