package br.gov.ce.semace.cep.service.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.gov.ce.semace.base.exception.ValidacaoException;
import br.gov.ce.semace.base.generic.GenericServiceImpl;
import br.gov.ce.semace.base.model.UF;
import br.gov.ce.semace.base.pagination.PageRequestBuilder;
import br.gov.ce.semace.base.specification.UFSpecification;
import br.gov.ce.semace.cep.repository.UFRepository;
import br.gov.ce.semace.cep.service.UFService;

@Service
public class UFServiceImpl extends GenericServiceImpl<UF, Long> implements UFService  {

	private final UFSpecification ufFilterSpecification;
	
	public UFServiceImpl(JpaRepository<UF, Long> repository, UFSpecification ufSpecification) {
		super(repository);
		this.ufFilterSpecification = ufSpecification;
	}
	
	@Autowired
	private UFRepository repository;

	
	@Override
	public Optional<UF> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public UF save(UF t) throws ValidacaoException {
		validate(t);
		return repository.save(t);
	}

	@Override
	public UF update(UF t) throws ValidacaoException {
		Optional<UF> uf = findById(t.getId());
		if (!uf.isPresent()) {
			throw new ValidacaoException("Estado não encontrado");
		}
		validate(t);
		return repository.save(t);
	}

	@Override
	public void delete(UF t) throws ValidacaoException {
		super.delete(t);
	}
	
	@Override
	public void validate(UF t) throws ValidacaoException {
		
		StringBuilder errors = new StringBuilder();
		
		if (t.getSigla() == null || StringUtils.isEmpty(t.getSigla())) {
			errors.append("Sigla é obrigatória.");
			errors.append(System.getProperty("line.separator"));
		}
		
		if (t.getDescricao() == null || StringUtils.isEmpty(t.getDescricao())) {
			errors.append("Descrição é obrigatória.");
			errors.append(System.getProperty("line.separator"));
		}
		
		if (!StringUtils.isEmpty(errors.toString())) {
			throw new ValidacaoException(errors.toString());
		}

	}
	
	@Override
	public Page<UF> list(Map<String, String> filters) {

		PageRequest pageRequest = PageRequestBuilder.getPageRequest(filters);
		Specification<UF> specifications = null;
		if(filters != null && !filters.isEmpty()) {
			Iterator<String> iterator = filters.keySet().iterator();
			String firstKey = iterator.next();
			Class<?> type = getAttributeType(firstKey);
	
			if (type == String.class) {
				specifications = Specification.where(
						ufFilterSpecification.getStringTypeSpecification(firstKey, filters.get(firstKey).toString()));
			} else if (type == Long.class) {
				specifications = Specification.where(
						ufFilterSpecification.getLongTypeSpecification(firstKey, filters.get(firstKey).toString()));
			}
	
			while (iterator.hasNext()) {
				String name = iterator.next();
				type = getAttributeType(name);
				if (type == String.class) {
					specifications = specifications
							.and(ufFilterSpecification.getStringTypeSpecification(name, filters.get(name).toString()));
				} else if (type == Long.class) {
					specifications = specifications
							.and(ufFilterSpecification.getLongTypeSpecification(name, filters.get(name).toString()));
				}
			}
			return repository.findAll(specifications, pageRequest);
		}
		return repository.findAll(pageRequest);
    }
	
	private Class<?> getAttributeType(String attrName) {

		Field[] fields = UF.class.getDeclaredFields();
		for (Field field : fields) {
			if (Modifier.isPrivate(field.getModifiers()) && !Modifier.isStatic(field.getModifiers())) {
				try {
					if (field.getName().equals(attrName)) {
						return field.getType();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}
}
