package br.gov.ce.semace.cep.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.semace.base.generic.GenericController;
import br.gov.ce.semace.base.model.Pais;
import br.gov.ce.semace.base.util.Response;
import br.gov.ce.semace.cep.service.PaisService;

@RestController
@RequestMapping("/write/pais")
@RefreshScope
public class PaisController extends GenericController<Pais, Long>{

	@Autowired
	public PaisController(PaisService service) {
		super(service);
	}
	
	@Autowired
	protected PaisService service;
	
	@GetMapping
	public ResponseEntity<Response> list(@RequestParam(required = false) Map<String, String> filters) {		
		try {
			Page<Pais> result = service.list(filters);
			if (result == null) {
				return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList("Sem resultados")), HttpStatus.NOT_FOUND);
			}
			
			return new ResponseEntity<Response>(new Response (Boolean.TRUE, result, Arrays.asList("Sucesso")), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList(e.getMessage())), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@DeleteMapping(value="/", consumes=MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> findByIds(@RequestBody List<Long> ids) {
		try {
			if (ids == null || ids.isEmpty()) {
				return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList("Necessário pelo menos um id para Delete")), HttpStatus.BAD_REQUEST);
			}
			service.deleteMultiple(ids);
			return new ResponseEntity<Response>(new Response(Boolean.TRUE, ids, Arrays.asList("Sucesso")), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response(Boolean.FALSE, null, Arrays.asList(e.getMessage())), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
