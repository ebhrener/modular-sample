package br.gov.ce.semace.cep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.semace.base.generic.GenericController;
import br.gov.ce.semace.base.model.Logradouro;
import br.gov.ce.semace.cep.service.LogradouroService;

@RestController
@RequestMapping("/write/logradouro")
@RefreshScope
public class LogradouroController extends GenericController<Logradouro, Long>{

	@Autowired
	public LogradouroController(LogradouroService service) {
		super(service);
	}
}
