package br.gov.ce.semace.cep.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.semace.base.model.Cidade;

public interface CidadeRepository extends JpaRepository<Cidade, Long>{

}
