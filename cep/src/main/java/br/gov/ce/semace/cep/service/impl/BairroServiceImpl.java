package br.gov.ce.semace.cep.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.gov.ce.semace.base.exception.ValidacaoException;
import br.gov.ce.semace.base.generic.GenericServiceImpl;
import br.gov.ce.semace.base.model.Bairro;
import br.gov.ce.semace.cep.repository.BairroRepository;
import br.gov.ce.semace.cep.service.BairroService;

@Service
public class BairroServiceImpl extends GenericServiceImpl<Bairro, Long> implements BairroService  {

	public BairroServiceImpl(JpaRepository<Bairro, Long> repository) {
		super(repository);
	}
	
	@Autowired
	private BairroRepository repository;

	
	@Override
	public Optional<Bairro> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public Bairro save(Bairro t) throws ValidacaoException {
		return repository.save(t);
	}

	@Override
	public void delete(Bairro t) throws ValidacaoException {
		super.delete(t);
	}
}
