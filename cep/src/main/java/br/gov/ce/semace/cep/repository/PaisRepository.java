package br.gov.ce.semace.cep.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.gov.ce.semace.base.model.Pais;

public interface PaisRepository extends JpaRepository<Pais, Long>, JpaSpecificationExecutor<Pais> {

	@Modifying
	@Query("DELETE from Pais p where id IN (:ids) ")
	public void deleteByIds(@Param("ids") List<Long> id);
}
