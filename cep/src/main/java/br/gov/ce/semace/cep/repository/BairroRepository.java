package br.gov.ce.semace.cep.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.semace.base.model.Bairro;

public interface BairroRepository extends JpaRepository<Bairro, Long>{

}
