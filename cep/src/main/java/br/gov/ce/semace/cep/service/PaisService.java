package br.gov.ce.semace.cep.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

import br.gov.ce.semace.base.generic.GenericService;
import br.gov.ce.semace.base.model.Pais;

public interface PaisService extends GenericService<Pais, Long> {

	public void deleteMultiple (List<Long> ids);
	
	public Page<Pais> list(Map<String, String> filter);
}
