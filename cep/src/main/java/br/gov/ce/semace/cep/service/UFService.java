package br.gov.ce.semace.cep.service;

import java.util.Map;

import org.springframework.data.domain.Page;

import br.gov.ce.semace.base.generic.GenericService;
import br.gov.ce.semace.base.model.UF;

public interface UFService extends GenericService<UF, Long> {

	public Page<UF> list(Map<String, String> filter);
}
