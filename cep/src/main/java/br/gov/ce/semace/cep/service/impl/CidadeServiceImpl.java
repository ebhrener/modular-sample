package br.gov.ce.semace.cep.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.gov.ce.semace.base.exception.ValidacaoException;
import br.gov.ce.semace.base.generic.GenericServiceImpl;
import br.gov.ce.semace.base.model.Cidade;
import br.gov.ce.semace.cep.repository.CidadeRepository;
import br.gov.ce.semace.cep.service.CidadeService;

@Service
public class CidadeServiceImpl extends GenericServiceImpl<Cidade, Long> implements CidadeService  {

	public CidadeServiceImpl(JpaRepository<Cidade, Long> repository) {
		super(repository);
	}
	
	@Autowired
	private CidadeRepository repository;

	
	@Override
	public Optional<Cidade> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public Cidade save(Cidade t) throws ValidacaoException {
		return repository.save(t);
	}

	@Override
	public void delete(Cidade t) throws ValidacaoException {
		super.delete(t);
	}
}
