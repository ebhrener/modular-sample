package br.gov.ce.semace.cep.service;

import br.gov.ce.semace.base.generic.GenericService;
import br.gov.ce.semace.base.model.Endereco;

public interface EnderecoService extends GenericService<Endereco, Long>{

}
