package br.gov.ce.semace.cep.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.semace.base.model.Logradouro;

public interface LogradouroRepository extends JpaRepository<Logradouro, Long>{

}
