package br.gov.ce.semace.cep.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/status")
public class ServerStatusController {
	
	@Autowired
	private DiscoveryClient discoveryClient;
	
	@GetMapping("/{appName}")
    public List<ServiceInstance> serviceInstancesByApplicationName(@PathVariable String appName ) {
		return this.discoveryClient.getInstances(appName);
	}
}
