package br.gov.ce.semace.cep.service;

import br.gov.ce.semace.base.generic.GenericService;
import br.gov.ce.semace.base.model.Logradouro;

public interface LogradouroService extends GenericService<Logradouro, Long> {

}
