package br.gov.ce.semace.cep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.semace.base.generic.GenericController;
import br.gov.ce.semace.base.model.Cidade;
import br.gov.ce.semace.cep.service.CidadeService;

@RestController
@RequestMapping("/write/cidade")
@RefreshScope
public class CidadeController extends GenericController<Cidade, Long>{

	@Autowired
	public CidadeController(CidadeService service) {
		super(service);
	}
}
