package br.gov.ce.semace.cep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.semace.base.generic.GenericController;
import br.gov.ce.semace.base.model.Endereco;
import br.gov.ce.semace.cep.service.EnderecoService;

@RestController
@RequestMapping("/write/endereco")
@RefreshScope
public class EnderecoController extends GenericController<Endereco, Long>{

	@Autowired
	public EnderecoController(EnderecoService service) {
		super(service);
	}
}
