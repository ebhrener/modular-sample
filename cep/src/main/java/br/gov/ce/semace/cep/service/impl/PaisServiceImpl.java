package br.gov.ce.semace.cep.service.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.gov.ce.semace.base.exception.ValidacaoException;
import br.gov.ce.semace.base.generic.GenericServiceImpl;
import br.gov.ce.semace.base.model.Pais;
import br.gov.ce.semace.base.pagination.PageRequestBuilder;
import br.gov.ce.semace.base.specification.PaisSpecification;
import br.gov.ce.semace.cep.repository.PaisRepository;
import br.gov.ce.semace.cep.service.PaisService;

@Service
public class PaisServiceImpl extends GenericServiceImpl<Pais, Long> implements PaisService  {

	private final PaisSpecification paisFilterSpecification;
	
	public PaisServiceImpl(JpaRepository<Pais, Long> repository, PaisSpecification paisSpecification) {
		super(repository);
		this.paisFilterSpecification = paisSpecification;
	}
	
	@Autowired
	private PaisRepository repository;
	
	@Override
	public Optional<Pais> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public Pais save(Pais t) throws ValidacaoException {
		validate(t);
		return repository.save(t);
	}

	
	@Override
	public Pais update(Pais t) throws ValidacaoException {
		Optional<Pais> pais = findById(t.getId());
		validate(t);
		if (!pais.isPresent()) {
			throw new ValidacaoException("País não encontrado");
		}
		return repository.save(t);
	}

	@Override
	public void validate(Pais t) throws ValidacaoException {
		
		StringBuilder errors = new StringBuilder();
		
		if (t.getCodigo() == null || StringUtils.isEmpty(t.getCodigo())) {
			errors.append("Código é obrigatório.");
			errors.append(System.getProperty("line.separator"));
		} else if (t.getCodigo().toString().length() > 10) {
			errors.append("Código deve ter no máximo 10 caracteres.");
			errors.append(System.getProperty("line.separator"));
		}
		
		if (t.getDescricao() == null || StringUtils.isEmpty(t.getDescricao())) {
			errors.append("Descrição é obrigatória.");
			errors.append(System.getProperty("line.separator"));
		} else if (t.getDescricao().toString().length() > 255) {
			errors.append("Descrição deve ter no máximo 255 caracteres.");
			errors.append(System.getProperty("line.separator"));
		}
		
		if (!StringUtils.isEmpty(errors.toString())) {
			throw new ValidacaoException(errors.toString());
		}

	}
	
	@Transactional
	public void deleteMultiple (List<Long> ids) {
		repository.deleteByIds(ids);
		//repository.deleteAll(ids.stream().map(id -> new Pais(id)).collect(toList()));
	}
	
	public static void main(String[] args) {
		
	}
	
	@Override
	public Page<Pais> list(Map<String, String> filters) {
//		.where(paisFilterSpecification.getStringTypeSpecification("codigo", 
//				filters.containsKey("codigo") ? filters.get("codigo").toString() : null))
//				//codigoMunicipioDigito
//				.and(paisFilterSpecification.getLongTypeSpecification("codigoMunicipioDigito", 
//						filters.containsKey("codigoMunicipioDigito") ? filters.get("codigoMunicipioDigito").toString() : null))
//				//nomeMunicipio
//				.and(paisFilterSpecification.getStringTypeSpecification("nomeMunicipio", 
//				filters.containsKey("nomeMunicipio") ? filters.get("nomeMunicipio").toString() : null))
//				//codigoUF
//				.and(paisFilterSpecification.getLongTypeSpecification("codigoUF", 
//						filters.containsKey("codigoUF") ? filters.get("codigoUF").toString() : null))
//				//nomeUF
//				.and(paisFilterSpecification.getStringTypeSpecification("nomeUF", 
//						filters.containsKey("nomeUF") ? filters.get("nomeUF").toString() : null))
//				//siglaUF
//				.and(paisFilterSpecification.getStringTypeSpecification("siglaUF", 
//						filters.containsKey("siglaUF") ? filters.get("siglaUF").toString() : null))
//				//ativo
//				.and(paisFilterSpecification.getBooleanTypeSpecification("ativo", 
//						filters.containsKey("ativo") ? filters.get("ativo").toString() : null));

		PageRequest pageRequest = PageRequestBuilder.getPageRequest(filters);
		Specification<Pais> specifications = null;
		if(filters != null && !filters.isEmpty()) {
			Iterator<String> iterator = filters.keySet().iterator();
			String firstKey = iterator.next();
			Class<?> type = getAttributeType(firstKey);
	
			if (type == String.class) {
				specifications = Specification.where(
						paisFilterSpecification.getStringTypeSpecification(firstKey, filters.get(firstKey).toString()));
			} else if (type == Long.class) {
				specifications = Specification.where(
						paisFilterSpecification.getLongTypeSpecification(firstKey, filters.get(firstKey).toString()));
			}
	
			while (iterator.hasNext()) {
				String name = iterator.next();
				type = getAttributeType(name);
				if (type == String.class) {
					specifications = specifications
							.and(paisFilterSpecification.getStringTypeSpecification(name, filters.get(name).toString()));
				} else if (type == Long.class) {
					specifications = specifications
							.and(paisFilterSpecification.getLongTypeSpecification(name, filters.get(name).toString()));
				}
			}
			return repository.findAll(specifications, pageRequest);
		}
		return repository.findAll(pageRequest);
    }

	private Class<?> getAttributeType(String attrName) {

		Field[] fields = Pais.class.getDeclaredFields();
		for (Field field : fields) {
			if (Modifier.isPrivate(field.getModifiers()) && !Modifier.isStatic(field.getModifiers())) {
				try {
					if (field.getName().equals(attrName)) {
						return field.getType();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

}
