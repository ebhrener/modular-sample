package br.gov.ce.semace.auth.dto;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(name = "AuthUserDTOMapping", classes = {
		@ConstructorResult(targetClass = AuthUserDTO.class, columns = { 
				@ColumnResult(name = "id", type=Long.class),
				@ColumnResult(name = "login"),
				@ColumnResult(name = "senha"),
				@ColumnResult(name = "is_super_usuario"),
				@ColumnResult(name = "roles") }) })
@NamedNativeQuery(name = "AuthUserDTO", resultSetMapping = "AuthUserDTOMapping", 
query = "select id,login,senha,is_super_usuario,string_agg(cast(up.perfil_id as text), ',') as roles from seguranca.usuario u \n" + 
		"left join seguranca.usuario_perfil up on u.id = up.usuario_id \n" + 
		"where login = :login \n" + 
		"group by 1,2,3,4")
public class AuthUserDTO {

	@Id
	private Long id;
	private String login;
	private String password;
	private boolean isSuperUser;
//	private List<Long> perfis;
	private String perfis;
	public AuthUserDTO(Long id, String login, String password, boolean isSuperUser, String perfis) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
		this.isSuperUser = isSuperUser;
		this.perfis = perfis;
//		setPerfis(perfis, ",");
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
//	public List<Long> getPerfis() {
//		return perfis;
//	}
//	public void setPerfis(List<Long> perfis) {
//		this.perfis = perfis;
//	}
//	public void setPerfis(String perfis, String separator) {
//		this.perfis = new ArrayList<>();
//		if (perfis != null && !perfis.isEmpty()) {
//			String[] array = perfis.split(separator);
//			
//			for (String string : array) {
//				this.perfis.add(Long.parseLong(string));
//			}
//		}
//	}
	public boolean isSuperUser() {
		return isSuperUser;
	}
	public void setSuperUser(boolean isSuperUser) {
		this.isSuperUser = isSuperUser;
	}
	@Override
	public String toString() {
		return "AuthUserDTO [id=" + id + ", login=" + login + ", password=" + password + ", isSuperUser=" + isSuperUser
				+ ", perfis=" + getPerfis() + "]";
	}

	public String getPerfis() {
		return perfis;
	}

	public void setPerfis(String perfis) {
		this.perfis = perfis;
	}
}
