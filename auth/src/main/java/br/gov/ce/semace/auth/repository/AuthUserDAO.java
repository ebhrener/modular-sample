package br.gov.ce.semace.auth.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.gov.ce.semace.auth.dto.AuthUserDTO;

@Repository
public class AuthUserDAO {

	@PersistenceContext
	private EntityManager em;

	public AuthUserDTO findUserByLogin(String login) {
		Query query = em.createNamedQuery("AuthUserDTO");
		query.setParameter("login", login);
		
		return (AuthUserDTO) query.getSingleResult();
	}

}
