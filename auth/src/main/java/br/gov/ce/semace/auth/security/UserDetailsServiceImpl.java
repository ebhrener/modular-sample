package br.gov.ce.semace.auth.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.gov.ce.semace.auth.dto.AuthUserDTO;
import br.gov.ce.semace.auth.repository.AuthUserDAO;

@Service // It has to be annotated with @Service.
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private AuthUserDAO authUserDAO;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		AuthUserDTO authUser = authUserDAO.findUserByLogin(username);
		if (authUser != null) {			
			// Remember that Spring needs roles to be in this format: "ROLE_" + userRole
			// (i.e. "ROLE_ADMIN")
			// So, we need to set it to that format, so we can verify and compare roles
			// (i.e. hasRole("ADMIN")).			
			List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList(authUser.getPerfis());
			
			if (authUser.isSuperUser()) {
				grantedAuthorities.add(new SimpleGrantedAuthority("-999"));
			}
			
			// The "User" class is provided by Spring and represents a model class for user
			// to be returned by UserDetailsService
			// And used by auth manager to verify and check user authentication.
			return new User(authUser.getLogin(), encoder.encode(authUser.getPassword()), grantedAuthorities);
		}

		// If user not found. Throw this exception.
		throw new UsernameNotFoundException("Username: " + username + " not found");
	}
}