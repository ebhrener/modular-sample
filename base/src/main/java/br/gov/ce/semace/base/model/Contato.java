package br.gov.ce.semace.base.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="contato", schema = "geral")
@Getter @Setter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Contato implements Serializable{

	private static final long serialVersionUID = -3754275922634449770L;
	
	@Id @Column(name="contato_id")
	@SequenceGenerator(sequenceName="geral.seq_contato", name="SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQ")
	private Long id;

	private String nome;
	
	private String endereco;
	
	private String numero;
	
	private String bairro;
	
	private String cep;
	
	private String municipio;
	
	private String uf;
	
	private String telefone;
	
	private String documento;
	
}
