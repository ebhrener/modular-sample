package br.gov.ce.semace.base.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="geral", name="pessoa_fisica", uniqueConstraints=@UniqueConstraint(columnNames="cpf"))
@PrimaryKeyJoinColumn(name = "pessoa_id")
@Getter @Setter @NoArgsConstructor
public class PessoaFisica extends Pessoa {

	private static final long serialVersionUID = 506380966349968295L;

	@NotNull
	@Column(unique = true)
	private String cpf;

	private String rg;

	private Character sexo;

	@NotNull
	private String nome;

	@Temporal(TemporalType.DATE)
	@Column(name="data_nascimento")
	private Date dataNascimento;

	private String email;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="contato_id")
	private Contato contato = new Contato();

	private String filiacao;

	@Transient
	private static final String MSG_CAMPO_NAO_INFORMADO = "Não Informado";

	@Transient
	private String documentoRepresentante;

	public PessoaFisica(Pessoa pessoa) {
		super(pessoa);
	}

}