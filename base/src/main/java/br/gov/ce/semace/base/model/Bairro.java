package br.gov.ce.semace.base.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name="bairros", schema="cep")
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode(onlyExplicitlyIncluded=true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Bairro implements Serializable{

	private static final long serialVersionUID = 5072122737126238447L;
	
	@EqualsAndHashCode.Include
	@Id
	@SequenceGenerator(sequenceName="cep.seq_bairros", name="SEQBAIRROS", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQBAIRROS")
	@Column(name="cd_bairro")
	private Long id;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cd_cidade")
	private Cidade cidade = new Cidade();
	
	@Column(name="ds_bairro_nome")
	private String descricao;
	
	
	@OneToMany(mappedBy="bairro", fetch=FetchType.LAZY)
	private List<Logradouro> logradouros = new ArrayList<Logradouro>();
	
}
