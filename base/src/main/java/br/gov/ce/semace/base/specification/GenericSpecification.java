package br.gov.ce.semace.base.specification;


import java.time.chrono.ChronoLocalDate;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

import br.gov.ce.semace.base.filter.Converters;
import br.gov.ce.semace.base.filter.FilterCriteria;
import br.gov.ce.semace.base.filter.FilterSpecifications;

/**
 * 
 * @author Erick Bhrener [erick.silva@gmail.com] 07/09/2018
 *
 * @param <E>
 */
public class GenericSpecification<E> {
	
	/**
	 * {@link FilterSpecifications} for Entity <E> and Field type {@link ChronoLocalDate} (LocalDate)
	 */
	@Autowired
	private FilterSpecifications<E, ChronoLocalDate> dateTypeSpecifications;

	/**
	 * {@link FilterSpecifications} for Entity <E> and Field type {@link String}
	 */
	@Autowired
	private FilterSpecifications<E, String> stringTypeSpecifications;

	/**
	 * {@link FilterSpecifications} for Entity <E> and Field type {@link Integer}
	 * 
	 */
	@Autowired
	private FilterSpecifications<E, Integer> integerTypeSpecifications;

	/**
	 * {@link FilterSpecifications} for Entity <E> and Field type {@link Long}
	 */
	@Autowired
	private FilterSpecifications<E, Long> longTypeSpecifications;
	
	/**
	 * {@link FilterSpecifications} for Entity <E> and Field type {@link Boolean}
	 */
	@Autowired
	private FilterSpecifications<E, Boolean> booleanTypeSpecifications;
	
	
	/**
	 * Converter Functions
	 */
	@Autowired
	private Converters converters;
	
	
	/**
	 * Returns the Specification for Entity {@link E} for the given fieldName and filterValue for the field type Date
	 * 
	 * @param fieldName
	 * @param filterValue
	 * @return
	 */
	public Specification<E> getDateTypeSpecification(String fieldName, String filterValue) {
		return getSpecification(fieldName, filterValue, converters.getFunction(ChronoLocalDate.class), dateTypeSpecifications);
	}

	/**
	 * Returns the Specification for Entity {@link E} for the given fieldName and filterValue for the field type String
	 * @param fieldName
	 * @param filterValue
	 * @return
	 */
	public Specification<E> getStringTypeSpecification(String fieldName, String filterValue) {
		return getSpecification(fieldName, filterValue, converters.getFunction(String.class), stringTypeSpecifications);
	}
	
	/**
	 * Returns the Specification for Entity <E> for the given fieldName and filterValue for the field type Long
	 * 
	 * @param fieldName
	 * @param filterValue
	 * @return
	 */
	public Specification<E> getLongTypeSpecification(String fieldName, String filterValue) {
		return getSpecification(fieldName, filterValue, converters.getFunction(Long.class), longTypeSpecifications);
	}
	
	
	/**
	 * Returns the Specification for Entity <E> for the given fieldName and filterValue for the field type Integer
	 * 
	 * @param fieldName
	 * @param filterValue
	 * @return
	 */
	public Specification<E> getIntegerTypeSpecification(String fieldName, String filterValue) {
		return getSpecification(fieldName, filterValue, converters.getFunction(Integer.class), integerTypeSpecifications);
	}

	/**
	 * Returns the Specification for Entity <E> for the given fieldName and filterValue for the field type Integer
	 * 
	 * @param fieldName
	 * @param filterValue
	 * @return
	 */
	public Specification<E> getBooleanTypeSpecification(String fieldName, String filterValue) {
		return getSpecification(fieldName, filterValue, converters.getFunction(Boolean.class), booleanTypeSpecifications);
	}
	
	/**
	 * Generic method to return {@link Specification} for Entity <E>
	 * 
	 * @param fieldName
	 * @param filterValue
	 * @param converter
	 * @param specifications
	 * @return
	 */
	private <T extends Comparable<T>> Specification<E> getSpecification(String fieldName,
			String filterValue, Function<String, T> converter, FilterSpecifications<E, T> specifications) {

		if (StringUtils.isNotBlank(filterValue)) {
			
			//Form the filter Criteria
			FilterCriteria<T> criteria = new FilterCriteria<>(fieldName, filterValue.toUpperCase(), converter);
			return specifications.getSpecification(criteria.getOperation()).apply(criteria);
		}

		return null;

	}

}
