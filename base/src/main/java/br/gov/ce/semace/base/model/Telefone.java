package br.gov.ce.semace.base.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="telefone", schema="geral")
@Getter @Setter @NoArgsConstructor
public class Telefone implements Serializable{

	private static final long serialVersionUID = -2686407452080740643L;

	@Id @SequenceGenerator(sequenceName="geral.seq_telefone", name="SEQTELEFONE", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQTELEFONE")
	private Long id;

	@ManyToOne
	@JoinColumn(name="tipo_telefone_id")
	private TipoTelefone tipoTelefone = new TipoTelefone();

	@Column
	private String ddd;

	@Column
	private String numero;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="pessoa_id")
	private Pessoa pessoa = new Pessoa();

}