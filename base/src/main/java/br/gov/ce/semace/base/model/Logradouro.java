package br.gov.ce.semace.base.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name="logradouros", schema="cep")
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode(onlyExplicitlyIncluded=true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Logradouro implements Serializable{

	private static final long serialVersionUID = -7646965994755979444L;
	
	@EqualsAndHashCode.Include
	@Id 
	@SequenceGenerator(sequenceName="cep.seq_logradouro", name="SEQLOGRA", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQLOGRA")
	@Column(name="cd_logradouro")
	private Long id; 
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cd_bairro")
	private Bairro bairro = new Bairro();

	@Column(name="cd_tipo_logradouros")
	private String tipoLogradouro;
	
	@Column(name="ds_logradouro_nome")
	private String logradouro;
	
	@Column(name="no_logradouro_cep")
	private String cep;
	
}
