package br.gov.ce.semace.base.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="interessado", schema="geral")
@Getter @Setter
@NoArgsConstructor
public class Interessado implements Serializable{

	private static final long serialVersionUID = 91255214794424663L;

	@Id @Column(name="interessado_id")
	@SequenceGenerator(sequenceName="geral.seq_interessado", name="SEQINTER", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="SEQINTER")
	private Long id;

	private Boolean isento = false;

	@Column(name = "permite_exibicao")
	private Boolean permiteExibicao = false;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="pessoa_id")
	private Pessoa pessoa = new Pessoa();


}
