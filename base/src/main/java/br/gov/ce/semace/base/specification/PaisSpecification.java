package br.gov.ce.semace.base.specification;

import org.springframework.stereotype.Service;

import br.gov.ce.semace.base.model.Pais;

/**
 * 
 * @author Erick Bhrener [erick.silva@gmail.com] 31/01/2019
 *
 * @param <Pais>
 */

@Service
public class PaisSpecification extends GenericSpecification<Pais>{

}
