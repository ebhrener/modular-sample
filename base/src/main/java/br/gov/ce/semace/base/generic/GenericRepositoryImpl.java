package br.gov.ce.semace.base.generic;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class GenericRepositoryImpl<T> implements GenericRepository<T> {

	@PersistenceContext
	protected EntityManager em;
	
	public T getReferenceById(Class<T> clazz, Long id) {
		return em.getReference(clazz, id);
	}
}