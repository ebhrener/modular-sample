package br.gov.ce.semace.base.generic;

import java.io.Serializable;
import java.util.Optional;

import javax.validation.ValidationException;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import br.gov.ce.semace.base.exception.ValidacaoException;

public class GenericServiceImpl<T, K extends Serializable> implements GenericService<T, K> {

	private JpaRepository<T, K> repository;

	public GenericServiceImpl(JpaRepository<T, K> repository) {
		this.repository = repository;
	}

	@Override
	public Iterable<T> findAll() {
		return repository.findAll();
	}

	@Override
	public Optional<T> findById(K id) {
		return repository.findById(id);
	}

	@Override
	@Transactional
	public T save(T t) throws ValidacaoException {
		validate(t);
		return repository.save(t);
	}

	@Override
	@Transactional
	public T update(T t) throws ValidacaoException {
		validate(t);
		return repository.save(t);
	}

	@Override
	@Transactional
	public T update(T t, K id) throws ValidacaoException {

		validate(t);
		
		Optional<T> optional = findById(id);
		if (!optional.isPresent()) {
			throw new ValidationException("Not found");
		}
		
		T entityDB = optional.get();

		return repository.save(entityDB);
	}

	@Override
	@Transactional
	public void delete(T t) throws ValidacaoException {
		repository.delete(t);
	}

	@Override
	public void validate(T t) throws ValidacaoException {

	}
}

