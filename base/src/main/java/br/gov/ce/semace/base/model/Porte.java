package br.gov.ce.semace.base.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "porte", schema = "geral")
@Getter @Setter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Porte implements Serializable {

	private static final long serialVersionUID = -7354708486244336486L;

	@Id
	@SequenceGenerator(sequenceName = "geral.seq_porte", name = "SEQPORTE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQPORTE")
	private Long id;

	@Column(name = "descricao")
	private String descricao;

	@Column(name = "faturamento_minimo")
	private BigDecimal faturamentoMinimo;

	@Column(name = "faturamento_maximo")
	private BigDecimal faturamentoMaximo;

	@Column(name = "area_construida_minima")
	private BigDecimal areaConstruidoMinima;

	@Column(name = "area_construida_maxima")
	private BigDecimal areaConstruidoMaxima;

	@Column(name = "num_funcionario_minimo")
	private Integer numFuncionarioMinimo;

	@Column(name = "num_funcionario_maximo")
	private Integer numFuncionarioMaximo;

	@Column(name = "grau_prioridade")
	private Integer grauPrioridade;


}
