package br.gov.ce.semace.base.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="endereco", schema="geral")
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode(onlyExplicitlyIncluded=true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Endereco implements Serializable, Cloneable {

	private static final long serialVersionUID = -5624844588824452681L;

	@EqualsAndHashCode.Include
	@Id
	@SequenceGenerator(sequenceName="geral.seq_endereco", name="geral.seq_endereco", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="geral.seq_endereco")
	private Long id;

	private String logradouro;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cidade_id")
	private Cidade cidade = new Cidade();

	private String bairro;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="uf_id")
	private UF uf = new UF();

	private String numero;

	private String complemento;

	@Column(name="caixa_postal")
	private String caixaPostal;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pessoa_id")
	private Pessoa pessoa = new Pessoa();

	@Column(name="endereco_padrao")
	private Boolean isEnderecoPadrao = false;

	private String cep;

	private String latitude;

	private String longitude;

}