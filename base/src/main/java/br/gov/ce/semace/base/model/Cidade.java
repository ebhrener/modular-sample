package br.gov.ce.semace.base.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name="municipios", schema="cep")
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode(onlyExplicitlyIncluded=true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Cidade implements Serializable{

	private static final long serialVersionUID = -7659026822803139844L;

	@EqualsAndHashCode.Include
	@Id
	@Column(name="cd_cidade")
	@SequenceGenerator(sequenceName="cep.seq_cidades", name="cep.seq_cidades", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="cep.seq_cidades")
	private Long id;

	@Column(name="ds_cidade_nome")
	private String descricao;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="cd_uf")
	private UF uf = new UF();

	@JsonIgnore
	@OneToMany(mappedBy="cidade", fetch=FetchType.LAZY)
	private List<Bairro> bairros = new ArrayList<>();

	@Column(name="codigo_ibge")
	private Long codigoIbge;

	@Column(name="loc_in_tipo_localidade")
	private String tipoLocalidade;

	@Column(name="loc_nu_sequencial_sub")
	private Integer idPai;

}