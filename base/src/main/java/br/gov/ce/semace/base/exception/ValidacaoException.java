package br.gov.ce.semace.base.exception;

public class ValidacaoException extends Exception {

	private static final long serialVersionUID = 705920653430728112L;

	private String msg;
	
	public ValidacaoException(String msg) {
		super(msg);
		this.msg = msg;
	}
	public ValidacaoException() {
		super("");
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}