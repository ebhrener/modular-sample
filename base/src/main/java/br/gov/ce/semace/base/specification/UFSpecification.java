package br.gov.ce.semace.base.specification;

import org.springframework.stereotype.Service;

import br.gov.ce.semace.base.model.UF;

/**
 * 
 * @author Erick Bhrener [erick.silva@gmail.com] 20/08/2019
 *
 * @param <Pais>
 *
 */

@Service
public class UFSpecification extends GenericSpecification<UF> {

}
