package br.gov.ce.semace.base.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "geral", name = "pessoa")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter @Setter 
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = PessoaFisica.class, name = "pf"), @Type(value = PessoaJuridica.class, name = "pj") })
public class Pessoa implements Serializable{

	private static final long serialVersionUID = 506380966349968295L;

	@Id @Column(name="pessoa_id")
	@SequenceGenerator(sequenceName="geral.seq_pessoa", name="geral.seq_pessoa", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="geral.seq_pessoa")
	private Long id;

	private Boolean ativo = true;

	@JsonIgnore
	@OneToMany(mappedBy="pessoa",cascade=CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Endereco.class)
	private List<Endereco> enderecos = new ArrayList<>();

	@JsonIgnore
	@OneToMany(mappedBy="pessoa",cascade=CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Telefone.class)
	private List<Telefone> telefones = new ArrayList<>();

	public Pessoa(Pessoa pessoa) {
		super();
		this.id = pessoa.id;
		this.ativo = pessoa.ativo;
		this.enderecos = pessoa.enderecos;
		this.telefones = pessoa.telefones;
	}
}