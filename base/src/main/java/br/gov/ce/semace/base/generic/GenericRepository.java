package br.gov.ce.semace.base.generic;

public interface GenericRepository<T> {

	public T getReferenceById(Class<T> clazz, Long id);
}