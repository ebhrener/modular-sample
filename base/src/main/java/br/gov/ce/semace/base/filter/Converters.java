package br.gov.ce.semace.base.filter;


import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

/**
 * 
 * @author Erick Bhrener [erick.bhb@gmail.com] 10/09/2018
 *
 */
@Service
public class Converters {

	private Map<Class<?>, Function<String, ? extends Comparable<?>>> map = new HashMap<>();

	@PostConstruct
	public void init() {
		map.put(String.class, s -> s.toUpperCase());
		map.put(Long.class, Long::valueOf);
		map.put(Integer.class, Integer::valueOf);
		map.put(ChronoLocalDate.class, LocalDate::parse);
		map.put(Boolean.class, Boolean::valueOf);
		// Add more converters
	}

	@SuppressWarnings("unchecked")
	public <T extends Comparable<T>> Function<String, T> getFunction(Class<?> classObj) {
		return (Function<String, T>) map.get(classObj);
	}

}