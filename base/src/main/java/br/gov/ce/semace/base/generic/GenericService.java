package br.gov.ce.semace.base.generic;

import java.util.Optional;

import br.gov.ce.semace.base.exception.ValidacaoException;

public interface GenericService<T, K> {

	Iterable<T> findAll();

	Optional<T> findById(K id);

	T save(T t) throws ValidacaoException;

	T update(T t) throws ValidacaoException;

	T update(T t, K k) throws ValidacaoException;

	void delete(T t) throws ValidacaoException;

	void validate(T t) throws ValidacaoException;
}

