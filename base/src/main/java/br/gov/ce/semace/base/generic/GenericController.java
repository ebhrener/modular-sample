package br.gov.ce.semace.base.generic;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.gov.ce.semace.base.exception.ValidacaoException;
import br.gov.ce.semace.base.util.Response;

public abstract class GenericController<T, K extends Serializable> {

	private GenericService<T, K> service;

	public GenericController(GenericService<T, K> service) {
		this.service = service;
	}

	@GetMapping(value="/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> findAll() {
		
		try {
			return new ResponseEntity<Response>(new Response (Boolean.TRUE, (Object)service.findAll(), Arrays.asList("Sucesso")), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList(e.getMessage())), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> findById(@PathVariable K id) {
		try {
			Optional<T> t = service.findById(id);
			if (!t.isPresent()) {
				return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList("Not found")), HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<Response>(new Response (Boolean.TRUE, (Object)t.get(), Arrays.asList("Sucesso")), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList(e.getMessage())), HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> save(@Valid @RequestBody T t) {
		try {
			return new ResponseEntity<Response>(new Response (Boolean.TRUE, (Object)service.save(t), Arrays.asList("Sucesso")), HttpStatus.OK);
		} catch (ValidacaoException e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList(e.getMsg())), HttpStatus.BAD_REQUEST);
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList(e.getRootCause().getLocalizedMessage())), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList(e.getMessage())), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> update(@Valid @RequestBody T t) {
		try {
			return new ResponseEntity<Response>(new Response (Boolean.TRUE, (Object)service.update(t), Arrays.asList("Sucesso")), HttpStatus.OK);
		} catch (ValidacaoException e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList(e.getMsg())), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList(e.getMessage())), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Response> delete(@PathVariable K id) {
		try {
			Optional<T> t = service.findById(id);
			if (!t.isPresent()) {
				return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList("Not found")), HttpStatus.NOT_FOUND);
			}
			service.delete(t.get());
			return new ResponseEntity<Response>(new Response (Boolean.TRUE, null, Arrays.asList("Sucess")), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList(e.getMessage())), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/validade", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> validate(@Valid @RequestBody T t) {
		try {
			service.validate(t);
			return new ResponseEntity<Response>(new Response (Boolean.TRUE, null, Arrays.asList("Ok")), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Response>(new Response (Boolean.FALSE, null, Arrays.asList(e.getMessage())), HttpStatus.BAD_REQUEST);
		}
	}
}

