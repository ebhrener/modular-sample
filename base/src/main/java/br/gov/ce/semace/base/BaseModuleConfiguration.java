package br.gov.ce.semace.base;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("br.gov.ce.semace")
@EntityScan("br.gov.ce.semace")
public class BaseModuleConfiguration {
	private static final Logger logger = LoggerFactory.getLogger(BaseModuleConfiguration.class);

	@PostConstruct
	public void postConstruct() {
		logger.info("BASE MODULE LOADED!");
	}

}
