package br.gov.ce.semace.base.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="geral", name="pessoa_juridica", uniqueConstraints=@UniqueConstraint(columnNames="cnpj"))
@Getter @Setter
@NoArgsConstructor
public class PessoaJuridica extends Pessoa{

	private static final long serialVersionUID = -1514937360622772274L;

	@Column(name="razao_social")
	private String razaoSocial;

	@Column(name="nome_fantasia")
	private String nomeFantasia;

	@Column(unique = true)
	private String cnpj;

	@Column(name="inscricao_municipal")
	private String inscricaoMunicipal;

	@Column(name="representante_legal")
	private String representanteLegal;

	@Temporal(TemporalType.DATE)
	@Column(name="data_abertura")
	private Date dataAbertura;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="contato_id")
	private Contato contato = new Contato();

	private String email;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="porte_id")
	private Porte porteEmpresa = new Porte();

	@Column(precision=15, scale=2)
	private BigDecimal faturamento;

	@Column(name="cgf")
	private String cgf;

}
