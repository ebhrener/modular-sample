package br.gov.ce.semace.base.model;


import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pais", schema="cep")
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Pais implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = -6480962376795956306L;
	
	public Pais(Long id) {
		super();
		this.id = id;
	}

	@EqualsAndHashCode.Include
	@Id
	@SequenceGenerator(sequenceName = "cep.seq_pais", name = "SEQ_PAIS", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PAIS")
	private Long id;
    
	private String codigo;

	private String descricao;


}
