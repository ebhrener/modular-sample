package br.gov.ce.semace.base.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "estados", schema="cep")
@Getter @Setter 
@NoArgsConstructor 
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UF implements Serializable{

	private static final long serialVersionUID = -2552219816888136458L;

	@EqualsAndHashCode.Include
	@Id @Column(name="cd_uf")
	@SequenceGenerator(sequenceName = "cep.seq_uf", name = "cep.seq_uf", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cep.seq_uf")
	private Long id;
	
	@Column(name ="ds_uf_sigla")
	private String sigla;
	
	@Column(name ="ds_uf_nome")
	private String descricao;

	@JsonIgnore
	@OneToMany(mappedBy="uf", fetch=FetchType.LAZY)
	private List<Cidade> cidades = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name="pais_id")
	private Pais pais = new Pais();

}
